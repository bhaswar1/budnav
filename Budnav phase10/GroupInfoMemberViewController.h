//
//  GroupInfoMemberViewController.h
//  Budnav
//
//  Created by Barsan on 17/09/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"
@interface GroupInfoMemberViewController : GAITrackedViewController

@property(strong,nonatomic) NSMutableDictionary *mydic;
@property(strong,nonatomic) NSDictionary *grpdict;
@property(strong,nonatomic) NSData *mydata2;


@end
