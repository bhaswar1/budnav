//
//  DBManager.m
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 16/06/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

#import "DBManager.h"
#import <sqlite3.h>
#import "Mymodel.h"
#import "AppDelegate.h"
static DBManager *sharedInstance=nil;
static sqlite3 *database=nil;
static sqlite3_stmt *statement=nil;

@implementation DBManager

+(DBManager *)getSharedInstance
{
    if(!sharedInstance)
    {
        sharedInstance=[[super allocWithZone:NULL]init ];
        [sharedInstance createDB];
    }
    return sharedInstance;
}


-(BOOL)createDB
{
    NSString *docsDir;
    NSArray *dirPaths;
    
    dirPaths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    
    databasePath = [[NSString alloc] initWithString:
                    [docsDir stringByAppendingPathComponent: @"contacts.db"]];
    DebugLog(@"databasePath --- %@",databasePath);
    
    BOOL isSuccess = YES;
    
    NSError *error;
    NSFileManager *fmngr = [[NSFileManager alloc] init];
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"contacts.db" ofType:nil];
    if(![fmngr copyItemAtPath:filePath toPath:[NSString stringWithFormat:@"%@/Documents/contacts.db", NSHomeDirectory()] error:&error]) {
        // handle the error
        DebugLog(@"Error creating the database: %@", [error description]);
        
    }
    databasePath = [NSString stringWithFormat:@"%@/Documents/contacts.db", NSHomeDirectory()];
    return isSuccess;
}

//    NSFileManager *fileManager = [NSFileManager defaultManager];
//    NSError *error;
//    
//    NSString *dbPath = [self getDBPath];
//    BOOL success = [fileManager fileExistsAtPath:dbPath];
//    if(!success) {
//        
//        NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"contacts.db"];
//        success = [fileManager copyItemAtPath:defaultDBPath toPath:dbPath error:&error];
//        
//        if (!success)
//            NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
//    }
//    return success;
//}
//
//- (NSString *) getDBPath
//{
//    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
//    NSString *documentsDir = [paths objectAtIndex:0];
//    return [documentsDir stringByAppendingPathComponent:@"contacts.db"];
//}


///////////////////////////////////       Insert Requests from Web to Local DB      //////////////////////////////////////////////
-(void)insertRequests: (NSArray *)array
{
    const char *dbpath = [databasePath UTF8String];
    @try {
           if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *deleteSql =[NSString stringWithFormat:@"delete from requests"];
        
        const char *delete_stmt = [deleteSql UTF8String];
        
        sqlite3_prepare_v2(database, delete_stmt,-1, &statement, NULL);
        DebugLog(@"statement : %d",sqlite3_step(statement));
        
        if (sqlite3_step(statement) == SQLITE_DONE || sqlite3_step(statement) == 101)
        {
            
        }
        else {
        }
//        sqlite3_reset(statement);

        
        int i;
        for (i=0; i< [array count]; i++)
        {
            int business = [[[array objectAtIndex:i]objectForKey:@"business"]intValue];
            NSString *city = [[array objectAtIndex:i]objectForKey:@"city"];
            NSString *country = [[array objectAtIndex:i]objectForKey:@"country"];
            NSString *fullname = [[array objectAtIndex:i]objectForKey:@"fullname"];
            NSString *name = [[array objectAtIndex:i]objectForKey:@"name"];
            NSString *surname = [[array objectAtIndex:i]objectForKey:@"surname"];
            int userid = [[[array objectAtIndex:i]objectForKey:@"id"]intValue];
            
                              
        NSString *insertSQL =[NSString stringWithFormat:@"insert into requests (business,city,country,fullname,id,name,surname) values (\"%d\",\"%@\",\"%@\", \"%@\", \"%d\",\"%@\",\"%@\")",business,city,country,fullname,userid,name,surname];
        
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL);
        DebugLog(@"statement : %d",sqlite3_step(statement));

        if (sqlite3_step(statement) == SQLITE_DONE || sqlite3_step(statement) == 101)
        {
        }
        else {
        }
//        sqlite3_reset(statement);
      }
        sqlite3_close(database);
    }
  }
    @catch (NSException *exception) {
        
    }
}


///////////////////////////////////       Fetch Requests from Local DB      //////////////////////////////////////////////
-(NSArray *)fetchRequests
{
    NSMutableArray *requestsdb_arr = [[NSMutableArray alloc]init];
    
//    [database open];
    
    NSString *query = @"SELECT * FROM requests";
//    sqlite3_stmt *statement;
    
    DebugLog(@"statement: %d",sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, NULL));
    @try
    {
    if (sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
    {
    
    if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, NULL) == SQLITE_OK) {
        
        while (sqlite3_step(statement) == SQLITE_ROW) {
            
            int business = sqlite3_column_int(statement, 0);
            char *city = (char *) sqlite3_column_text(statement, 1);
            char *country = (char *) sqlite3_column_text(statement, 2);
            char *fullname = (char *) sqlite3_column_text(statement, 3);
            char *name = (char *) sqlite3_column_text(statement, 5);
            char *surname = (char *) sqlite3_column_text(statement, 6);
            int userid = sqlite3_column_int(statement, 4);
            
            NSString *fullname_str = [[NSString alloc] initWithUTF8String:fullname];
            NSString *city_str = [[NSString alloc] initWithUTF8String:city];
            NSString *country_str = [[NSString alloc] initWithUTF8String:country];
            NSString *name_str = [[NSString alloc] initWithUTF8String:name];
            NSString *surname_str = [[NSString alloc] initWithUTF8String:surname];
            
            NSMutableDictionary *dict = [NSMutableDictionary new];
            
//            dict = @{
//                     @"business": [NSNumber numberWithInt:business]
//                     };
            [dict setObject:[NSString stringWithFormat:@"%d",business] forKey:@"business"];
            [dict setObject:city_str forKey:@"city"];
            [dict setObject:country_str forKey:@"country"];
            [dict setObject:fullname_str forKey:@"fullname"];
            [dict setObject:[NSString stringWithFormat:@"%d",userid] forKey:@"id"];
            [dict setObject:name_str forKey:@"name"];
            [dict setObject:surname_str forKey:@"surname"];
            
            [requestsdb_arr addObject:dict];
        }
        sqlite3_finalize(statement);
    }
    
    else
        DebugLog(@"Error: %s", sqlite3_errmsg(database));
    //sqlite3_close(database);

    }
    DebugLog(@"requests db: %@",requestsdb_arr);
   
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }

    return (NSArray *)requestsdb_arr;
}



////////////////////////////////////        Insert Map         ///////////////////////////////////////////////////
-(void)insertMap:(NSMutableArray *)array_m
{
    DebugLog(@"db map insert %@",array_m);
//    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        
//        Mymodel *obj=[Mymodel getInstance];
//        obj.db_busy = YES;

    const char *dbpath_m = [databasePath UTF8String];
    @try {
            if (sqlite3_open(dbpath_m, &database) == SQLITE_OK)
        {
            NSString *deleteSql_m =[NSString stringWithFormat:@"delete from maps"];
            
            DebugLog(@"%@",deleteSql_m);
            const char *delete_stmt_m = [deleteSql_m UTF8String];
            
            sqlite3_prepare_v2(database, delete_stmt_m,-1, &statement, NULL);
            DebugLog(@"statement : %d",sqlite3_step(statement));
            
            if (sqlite3_step(statement) == SQLITE_DONE || sqlite3_step(statement) == 101)
            {
            }
            else {
            }
            sqlite3_reset(statement);
            
            
            int i;
            for (i=0; i< [array_m count]; i++)
            {
                int business_m = [[[array_m objectAtIndex:i]objectForKey:@"business"]intValue];
                NSString *lat_m = [[array_m objectAtIndex:i]objectForKey:@"lat"];
                NSString *lng_m = [[array_m objectAtIndex:i]objectForKey:@"lng"];
                NSString *name_m = [[array_m objectAtIndex:i]objectForKey:@"name"];
                NSString *surname_m = [[array_m objectAtIndex:i]objectForKey:@"surname"];
                int userid_m = [[[array_m objectAtIndex:i]objectForKey:@"id"]intValue];
                
                
                NSString *insertSQL_m =[NSString stringWithFormat:@"insert into maps (business,lat,lng,name,surname,id) values (\"%d\",\"%@\",\"%@\", \"%@\",\"%@\", \"%d\")",business_m,lat_m,lng_m,name_m,surname_m,userid_m];
                
                DebugLog(@"%@",insertSQL_m);
                const char *insert_stmt_m = [insertSQL_m UTF8String];
                
                sqlite3_prepare_v2(database, insert_stmt_m,-1, &statement, NULL);
                DebugLog(@"statement : %d",sqlite3_step(statement));
                
                if (sqlite3_step(statement) == SQLITE_DONE || sqlite3_step(statement) == 101)
                {
                }
                else {
                }
                sqlite3_reset(statement);
            }
            sqlite3_close(database);
        }
    }
    @catch (NSException *exception) {
    }
//        dispatch_async(dispatch_get_main_queue(), ^{
//            
//            Mymodel *obj=[Mymodel getInstance];
//            obj.db_busy = NO;
//            
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"db_busy" object:Nil];
//            
//        });
//    });

}



///////////////////////////////////       Fetch Map from Local DB      //////////////////////////////////////////////
-(NSArray *)fetchMap
{
    NSMutableArray *mapdb_arr = [[NSMutableArray alloc]init];
  
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        
//        Mymodel *obj=[Mymodel getInstance];
//        obj.db_busy = YES;
            //    [database open];
    
    NSString *query_m = @"SELECT * FROM maps";
    //    sqlite3_stmt *statement;
    
    @try {
        DebugLog(@"statement: %d",sqlite3_prepare_v2(database, [query_m UTF8String], -1, &statement, NULL));
        
        if (sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
        {
            
            if (sqlite3_prepare_v2(database, [query_m UTF8String], -1, &statement, NULL) == SQLITE_OK) {
                
                while (sqlite3_step(statement) == SQLITE_ROW) {
                    
                    //                DebugLog(@"222222222");
                    int business_m = sqlite3_column_int(statement, 0);
                    char *lat_m = (char *) sqlite3_column_text(statement, 1);
                    char *lng_m = (char *) sqlite3_column_text(statement, 2);
                    char *name_m = (char *) sqlite3_column_text(statement,3);
                    char *surname_m = (char *) sqlite3_column_text(statement, 4);
                    int userid_m = sqlite3_column_int(statement, 5);
                    
                    NSString *lat_str_m = [[NSString alloc] initWithUTF8String:lat_m];
                    NSString *lng_str_m = [[NSString alloc] initWithUTF8String:lng_m];
                    NSString *name_str_m = [[NSString alloc] initWithUTF8String:name_m];
                    NSString *surname_str_m = [[NSString alloc] initWithUTF8String:surname_m];
                    
                    NSMutableDictionary *dict_m = [[NSMutableDictionary alloc]init];
                    [dict_m setObject:[NSString stringWithFormat:@"%d",business_m] forKey:@"business"];
                    [dict_m setObject:lat_str_m forKey:@"lat"];
                    [dict_m setObject:lng_str_m forKey:@"lng"];
                    [dict_m setObject:name_str_m forKey:@"name"];
                    [dict_m setObject:surname_str_m forKey:@"surname"];
                    [dict_m setObject:[NSString stringWithFormat:@"%d",userid_m] forKey:@"id"];
                    
                    [mapdb_arr addObject:dict_m];
                }
                sqlite3_finalize(statement);
            }
            
            else
                DebugLog(@"Error: %s", sqlite3_errmsg(database));
            sqlite3_close(database);
        }
    }
    @catch (NSException *exception) {
        
    }
    @finally {
//         obj.db_busy = NO;
    }
        DebugLog(@"map db count: %lu",(unsigned long)[mapdb_arr count]);
    
//        dispatch_async(dispatch_get_main_queue(), ^{
//            
//            Mymodel *obj=[Mymodel getInstance];
//
//            
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"db_busy" object:Nil];
//            
//        });
//    });
    
    return (NSArray *)mapdb_arr;
}



- (void) insertContacts:(NSMutableArray *)array_c
{
    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        
//        Mymodel *obj=[Mymodel getInstance];
//        obj.db_busy = YES;
    
        DebugLog(@"db contacts insert %@",array_c);

        const char *dbpath_c = [databasePath UTF8String];

    @try {
        if (sqlite3_open(dbpath_c, &database) == SQLITE_OK)
    {
        NSString *deleteSql_c =[NSString stringWithFormat:@"delete from contacts"];
        
        DebugLog(@"%@",deleteSql_c);
        const char *delete_stmt_c = [deleteSql_c UTF8String];
        
        sqlite3_prepare_v2(database, delete_stmt_c,-1, &statement, NULL);
        DebugLog(@"statement : %d",sqlite3_step(statement));
        
        if (sqlite3_step(statement) == SQLITE_DONE || sqlite3_step(statement) == 101)
        {
        }
        else {
        }
        sqlite3_reset(statement);
        
        
        int i;
        for (i=0; i< [array_c count]; i++)
        {
            int business_c = [[[array_c objectAtIndex:i]objectForKey:@"business"]intValue];
            NSString *home_fax_c = [[array_c objectAtIndex:i]objectForKey:@"home_fax"];
            NSString *other_c = [[array_c objectAtIndex:i]objectForKey:@"other"];
            NSString *work_c = [[array_c objectAtIndex:i]objectForKey:@"work"];
            NSString *home_c = [[array_c objectAtIndex:i]objectForKey:@"home"];
            NSString *mobile_c = [[array_c objectAtIndex:i]objectForKey:@"mobile"];
            NSString *numbers_c = [[array_c objectAtIndex:i]objectForKey:@"numbers"];
            NSString *address_c = [[array_c objectAtIndex:i]objectForKey:@"address"];
            NSString *thumb_c = [[array_c objectAtIndex:i]objectForKey:@"thumb"];
            NSString *phone_pre_c = [[array_c objectAtIndex:i]objectForKey:@"phone_pre"];
            NSString *phone_num_c = [[array_c objectAtIndex:i]objectForKey:@"phone_num"];
            int phone_c_c = [[[array_c objectAtIndex:i]objectForKey:@"phone_c"] intValue];
            NSString *mobile_pre_c = [[array_c objectAtIndex:i]objectForKey:@"mobile_pre"];
            NSString *mobile_num_c = [[array_c objectAtIndex:i]objectForKey:@"mobile_num"];
            int mobile_c_c = [[[array_c objectAtIndex:i]objectForKey:@"mobile_c"] intValue];
            
            NSString *name_c = [[array_c objectAtIndex:i]objectForKey:@"name"];
            NSString *surname_c = [[array_c objectAtIndex:i]objectForKey:@"surname"];
            NSString *userid_c = [[array_c objectAtIndex:i]objectForKey:@"id"];
            NSString *fullname_c = [[array_c objectAtIndex:i]objectForKey:@"fullname"];
            NSString *email_c = [[array_c objectAtIndex:i]objectForKey:@"email"];
            int email_c_c = [[[array_c objectAtIndex:i]objectForKey:@"email_c"] intValue];
            NSString *country_c = [[array_c objectAtIndex:i]objectForKey:@"country"];
            NSString *city_c = [[array_c objectAtIndex:i]objectForKey:@"city"];
            NSString *b_phone_pre_c = [[array_c objectAtIndex:i]objectForKey:@"b_phone_pre"];
            NSString *b_phone_num_c = [[array_c objectAtIndex:i]objectForKey:@"b_phone_num"];
            int b_phone_c_c = [[[array_c objectAtIndex:i]objectForKey:@"b_phone_c"] intValue];
             NSString *lat_c = [[array_c objectAtIndex:i]objectForKey:@"lat"];
             NSString *lng_c = [[array_c objectAtIndex:i]objectForKey:@"lng"];
            
            NSString *insertSQL =[NSString stringWithFormat:@"INSERT INTO contacts (business,home_fax,other,work,home,mobile,numbers,address,thumb,phone_pre,phone_num,phone_c,mobile_pre,mobile_num,mobile_c,name,surname,userid,fullname,email,email_c,country,city,b_phone_pre,b_phone_num,b_phone_c,lat,lng) values (\"%d\",\"%@\",\"%@\",\"%@\",\"%@\", \"%@\",\"%@\", \"%@\",\"%@\",\"%@\",\"%@\",\"%d\",\"%@\",\"%@\",\"%d\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%d\",\"%@\",\"%@\",\"%@\",\"%@\",\"%d\", \"%@\",\"%@\")",business_c,home_fax_c,other_c,work_c,home_c,mobile_c,numbers_c,address_c,thumb_c,phone_pre_c,phone_num_c,phone_c_c,mobile_pre_c,mobile_num_c,mobile_c_c,name_c,surname_c,userid_c,fullname_c,email_c,email_c_c,country_c,city_c,b_phone_pre_c,b_phone_num_c,b_phone_c_c,lat_c,lng_c];     // OR REPLACE
            
//            DebugLog(@"%@",insertSQL);
            const char *insert_stmt_c = [insertSQL UTF8String];
            
            sqlite3_prepare_v2(database, insert_stmt_c,-1, &statement, NULL);
            DebugLog(@"statement : %d",sqlite3_step(statement));
            
//            if (sqlite3_step(statement) == SQLITE_DONE || sqlite3_step(statement) == 101)
//            {
//            }
//            else {
//            }
        }
        sqlite3_reset(statement);

        sqlite3_close(database);
    }
}
@catch (NSException *exception) {
    
}
@finally {
    
}
//        dispatch_async(dispatch_get_main_queue(), ^{
//            
//            Mymodel *obj=[Mymodel getInstance];
//            obj.db_busy = NO;
//            
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"db_busy" object:Nil];
//        });
//    });
}


-(NSArray *)fetchContacts
{
    NSMutableArray *contactsdb_arr = [[NSMutableArray alloc]init];

//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        
//        Mymodel *obj=[Mymodel getInstance];
//        obj.db_busy = YES;
    
    
        NSString *query_co = @"SELECT * FROM contacts";
        //    sqlite3_stmt *statement;
    @try {
            if (sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
        {
//            DebugLog(@"statement: %d",sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, NULL));

            if (sqlite3_prepare_v2(database, [query_co UTF8String], -1, &statement, NULL) == SQLITE_OK) {
                
//                DebugLog(@"bbbbbbbbbb maps");
                while (sqlite3_step(statement) == SQLITE_ROW) {
                    
//                    DebugLog(@"222222222");
                    char *lat_co = (char *) sqlite3_column_text(statement, 1);
                    char *lng_co = (char *) sqlite3_column_text(statement, 0);
                    char *home_fax_co = (char *) sqlite3_column_text(statement, 2);
                    char *other_co = (char *) sqlite3_column_text(statement, 4);
                    char *work_co = (char *) sqlite3_column_text(statement, 3);
                    char *mobile_co = (char *) sqlite3_column_text(statement, 5);
                    char *home_co = (char *) sqlite3_column_text(statement, 6);
                    char *numbers_co = (char *) sqlite3_column_text(statement, 7);
//                  char *address = (char *) sqlite3_column_text(statement, 6);
                    char *thumb_co = (char *) sqlite3_column_text(statement, 9);
                    char *phone_pre_co = (char *) sqlite3_column_text(statement, 10);
                    char *phone_num_co = (char *) sqlite3_column_text(statement, 11);
                    int phone_c_co = sqlite3_column_int(statement, 12);
                    char *mobile_pre_co = (char *) sqlite3_column_text(statement, 15);
                    char *mobile_num_co = (char *) sqlite3_column_text(statement, 16);
                    int mobile_c_co = sqlite3_column_int(statement, 17);
                    
                    char *surname_co = (char *) sqlite3_column_text(statement, 13);
                    char *name_co = (char *) sqlite3_column_text(statement, 14);
                    char *userid_co = (char *) sqlite3_column_text(statement, 18);

                    char *fullname_co = (char *) sqlite3_column_text(statement, 19);

                    char *email_co = (char *) sqlite3_column_text(statement, 21);
                    int email_c_co = sqlite3_column_int(statement, 20);
                    char *country_co = (char *) sqlite3_column_text(statement, 22);
                    char *city_co = (char *) sqlite3_column_text(statement, 23);
                    int business_co = sqlite3_column_int(statement, 24);
                    char *b_phone_pre_co = (char *) sqlite3_column_text(statement, 25);
                    char *b_phone_num_co = (char *) sqlite3_column_text(statement, 26);
                    int b_phone_c_co = sqlite3_column_int(statement, 27);
                    
                    
                    
                    NSString *lat_str_co = [[NSString alloc] initWithUTF8String:lat_co];
                    NSString *lng_str_co = [[NSString alloc] initWithUTF8String:lng_co];
                    NSString *home_fax_str_co = [[NSString alloc] initWithUTF8String:home_fax_co];
                    NSString *other_str_co = [[NSString alloc] initWithUTF8String:other_co];
                    NSString *work_str_co = [[NSString alloc] initWithUTF8String:work_co];
                    NSString *mobile_str_co = [[NSString alloc] initWithUTF8String:mobile_co];
                    NSString *home_str_co = [[NSString alloc] initWithUTF8String:home_co];
                    NSString *numbers_str_co = [[NSString alloc] initWithUTF8String:numbers_co];
//                    NSString *address_str = [[NSString alloc] initWithUTF8String:address];
                    NSString *thumb_str_co = [[NSString alloc] initWithUTF8String:thumb_co];                  //
                    NSString *phone_pre_str_co = [[NSString alloc] initWithUTF8String:phone_pre_co];
                    NSString *phone_num_str_co = [[NSString alloc] initWithUTF8String:phone_num_co];
                    NSString *mobile_pre_str_co = [[NSString alloc] initWithUTF8String:mobile_pre_co];
                    NSString *mobile_num_str_co = [[NSString alloc] initWithUTF8String:mobile_num_co];
                    NSString *fullname_str_co = [[NSString alloc] initWithUTF8String:fullname_co];
                    NSString *email_str_co = [[NSString alloc] initWithUTF8String:email_co];
                    NSString *country_str_co = [[NSString alloc] initWithUTF8String:country_co];
                    NSString *city_str_co = [[NSString alloc] initWithUTF8String:city_co];
                    NSString *b_phone_pre_str_co = [[NSString alloc] initWithUTF8String:b_phone_pre_co];
                    NSString *b_phone_num_str_co = [[NSString alloc] initWithUTF8String:b_phone_num_co];
                    NSString *name_str_co = [[NSString alloc] initWithUTF8String:name_co];
                    NSString *surname_str_co = [[NSString alloc] initWithUTF8String:surname_co];
                    NSString *userid_str_co = [[NSString alloc] initWithUTF8String:userid_co];
                    
                    NSMutableDictionary *dict_co = [[NSMutableDictionary alloc]init];
                    [dict_co setObject:[NSString stringWithFormat:@"%d",business_co] forKey:@"business"];
                    [dict_co setObject:name_str_co forKey:@"name"];
                    [dict_co setObject:surname_str_co forKey:@"surname"];
                    [dict_co setObject:userid_str_co forKey:@"id"];
                    [dict_co setObject:home_fax_str_co forKey:@"home fax"];
                    [dict_co setObject:other_str_co forKey:@"other"];
                    [dict_co setObject:work_str_co forKey:@"work"];
                    [dict_co setObject:mobile_str_co forKey:@"mobile"];
                    [dict_co setObject:home_str_co forKey:@"home"];
                    [dict_co setObject:numbers_str_co forKey:@"numbers"];
//                    [dict setObject:address_str forKey:@"numbers"];
                    [dict_co setObject:thumb_str_co forKey:@"thumb"];
                    [dict_co setObject:phone_pre_str_co forKey:@"phone_pre"];
                    [dict_co setObject:phone_num_str_co forKey:@"phone_num"];
                    [dict_co setObject:mobile_pre_str_co forKey:@"mobile_pre"];
                    [dict_co setObject:mobile_num_str_co forKey:@"mobile_num"];
                    [dict_co setObject:fullname_str_co forKey:@"fullname"];
                    [dict_co setObject:email_str_co forKey:@"email"];
                    [dict_co setObject:country_str_co forKey:@"country"];
                    [dict_co setObject:city_str_co forKey:@"city"];
                    [dict_co setObject:b_phone_pre_str_co forKey:@"b_phone_pre"];
                    [dict_co setObject:b_phone_num_str_co forKey:@"b_phone_num"];
                    [dict_co setObject:lat_str_co forKey:@"lat"];
                    [dict_co setObject:lng_str_co forKey:@"lng"];

                    [dict_co setObject:[NSString stringWithFormat:@"%d",phone_c_co] forKey:@"phone_c"];
                    [dict_co setObject:[NSString stringWithFormat:@"%d",mobile_c_co] forKey:@"mobile_c"];
                    [dict_co setObject:[NSString stringWithFormat:@"%d",b_phone_c_co] forKey:@"b_phone_c"];
                    [dict_co setObject:[NSString stringWithFormat:@"%d",email_c_co] forKey:@"email_c"];

                    [contactsdb_arr addObject:dict_co];
                }
//                else
//                {
//                    DebugLog(@"yes payni payni");
//                }
                sqlite3_finalize(statement);
                DebugLog(@"contacts db: %lu",(unsigned long)[contactsdb_arr count]);
            }
            
            else
                DebugLog(@"Error: %s", sqlite3_errmsg(database));
            sqlite3_close(database);
        }
    }
    @catch (NSException *exception) {
        
    }
        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            
//            Mymodel *obj=[Mymodel getInstance];
//            obj.db_busy = NO;
//            
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"db_busy" object:Nil];
//
//        });
//    });
    
    return (NSArray *)contactsdb_arr;
}


///////////////////////////////////////////    Local DB for User Profile    ////////////////////////////////////////////////////////////
- (void) insertProfile:(NSDictionary *)dictprof : (int)userid_p
{
    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//      
//        Mymodel *obj=[Mymodel getInstance];
//        obj.db_busy = YES;
    
    DebugLog(@" db manager profile dict & userid :%d --- %@",userid_p,dictprof);
    
    NSDictionary *dict = [dictprof objectForKey:@"profile"];

    const char *dbpath = [databasePath UTF8String];
    
    //    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

    @try {
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            NSString *deleteSql_pr =[NSString stringWithFormat:@"delete from Profile where userid = '%d'",userid_p];
            
            DebugLog(@"%@",deleteSql_pr);
            const char *delete_stmt_pr = [deleteSql_pr UTF8String];
            
            sqlite3_prepare_v2(database, delete_stmt_pr,-1, &statement, NULL);
            DebugLog(@"statement : %d",sqlite3_step(statement));
            
            if (sqlite3_step(statement) == SQLITE_DONE || sqlite3_step(statement) == 101)
            {
            }
            else {
            }
            sqlite3_reset(statement);
            
            
            int business_p = [[dictprof objectForKey:@"business"]intValue];
            int own_p = [[dictprof objectForKey:@"own"]intValue];
            int friend_p = [[dictprof objectForKey:@"friend"]intValue];
            
            NSString *thumb_p = [dictprof objectForKey:@"image"];
            NSString *phone_pre_p = [dict objectForKey:@"phone_pre"];
            NSString *phone_num_p = [dict objectForKey:@"phone_num"];
            int phone_c_p = [[dict objectForKey:@"phone_c"] intValue];
            NSString *mobile_pre_p = [dict objectForKey:@"mobile_pre"];
            NSString *mobile_num_p = [dict objectForKey:@"mobile_num"];
            int mobile_c_p = [[dict objectForKey:@"mobile_c"] intValue];
            
            NSString *name_p = [dict objectForKey:@"name"];
            NSString *surname_p = [dict objectForKey:@"surname"];
            NSString *email_p = [dict objectForKey:@"email"];
            //            int email_c = [[dict objectForKey:@"email_c"] intValue];
            NSString *country_p = [dict objectForKey:@"country"];
            NSString *city_p = [dict objectForKey:@"city"];
            NSString *b_phone_pre_p = [dict objectForKey:@"b_phone_pre"];
            NSString *b_phone_num_p = [dict objectForKey:@"b_phone_num"];
            int b_phone_c_p = [[dict objectForKey:@"b_phone_c"] intValue];
            
            NSString *website_p = [dict objectForKey:@"website"];
            NSString *street_p = [dict objectForKey:@"street"];
            NSString *zipcode_p = [dict objectForKey:@"zipcode"];
            NSString *housenumber_p = [dict objectForKey:@"housenumber"];
            NSString *lat_p = [dict objectForKey:@"lat"];
            NSString *lng_p = [dict objectForKey:@"lng"];
            NSString *b_company_p = [dict objectForKey:@"b_company"];
            NSString *b_function_p = [dict objectForKey:@"b_function"];
            NSString *b_email_p = [dict objectForKey:@"b_email"];
            //        int b_email_c = [[dict objectForKey:@"b_email_c"] intValue];
            NSString *b_website_p = [dict objectForKey:@"b_website"];
            NSString *b_country_p = [dict objectForKey:@"b_country"];
            NSString *b_zipcode_p = [dict objectForKey:@"b_zipcode"];
            NSString *b_city_p = [dict objectForKey:@"b_city"];
            NSString *b_street_p = [dict objectForKey:@"b_street"];
            NSString *b_housenumber_p = [dict objectForKey:@"b_housenumber"];
            NSString *b_lat_p = [dict objectForKey:@"b_lat"];
            NSString *b_lng_p = [dict objectForKey:@"b_lng"];
            NSString *s_skype_p = [dict objectForKey:@"s_skype"];
            NSString *s_linkedin_p = [dict objectForKey:@"s_linkedin"];
            NSString *s_facebook_p = [dict objectForKey:@"s_facebook"];
            NSString *s_twitter_p = [dict objectForKey:@"s_twitter"];
            NSString *s_youtube_p = [dict objectForKey:@"s_youtube"];
            NSString *s_soundcloud_p = [dict objectForKey:@"s_soundcloud"];
            NSString *s_google_p = [dict objectForKey:@"s_google"];
            NSString *s_pinterest_p = [dict objectForKey:@"s_pinterest"];
            NSString *s_vimeo_p = [dict objectForKey:@"s_vimeo"];
            NSString *s_instagram_p = [dict objectForKey:@"s_instagram"];
            
            
            NSString *insertSQL_p =[NSString stringWithFormat:@"INSERT OR REPLACE into Profile(userid,b_city,b_company,b_country,b_email,b_function,b_housenumber,b_lat,b_lng,b_phone_num,b_phone_pre,b_phone_c,b_street,b_website,b_zipcode,email,housenumber,lat,lng,mobile_num,mobile_pre,mobile_c,name,phone_num,phone_pre,phone_c,street,surname,website,zipcode,business,city,country,friend,own,image,s_facebook,s_google,s_instagram,s_linkedin,s_pinterest,s_skype,s_soundcloud,s_twitter,s_vimeo,s_youtube) values (\"%d\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%d\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\", \"%@\",\"%d\",\"%@\",\"%@\",\"%@\",\"%d\", \"%@\",\"%@\",\"%@\",\"%@\",\"%d\", \"%@\",\"%@\",\"%d\",\"%d\", \"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",userid_p,b_city_p,b_company_p,b_country_p,b_email_p,b_function_p,b_housenumber_p,b_lat_p,b_lng_p,b_phone_num_p,b_phone_pre_p,b_phone_c_p,b_street_p,b_website_p,b_zipcode_p,email_p,housenumber_p,lat_p,lng_p,mobile_num_p,mobile_pre_p,mobile_c_p,name_p,phone_num_p,phone_pre_p,phone_c_p,street_p,surname_p,website_p,zipcode_p,business_p,city_p,country_p,friend_p,own_p,thumb_p,s_facebook_p,s_google_p,s_instagram_p,s_linkedin_p,s_pinterest_p,s_skype_p,s_soundcloud_p,s_twitter_p,s_vimeo_p,s_youtube_p];
            
            DebugLog(@"%@",insertSQL_p);
            const char *insert_stmt_p = [insertSQL_p UTF8String];
            
            sqlite3_prepare_v2(database, insert_stmt_p,-1, &statement, NULL);
            DebugLog(@"statement : %d",sqlite3_step(statement));
            
            if (sqlite3_step(statement) == SQLITE_DONE || sqlite3_step(statement) == 101)
            {
            }
            else {
            }
            sqlite3_reset(statement);
            sqlite3_close(database);
        }
    }
    @catch (NSException *exception) {
        
    }
//        dispatch_async(dispatch_get_main_queue(), ^{
//            
//            Mymodel *obj=[Mymodel getInstance];
//            obj.db_busy = NO;
//            
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"db_busy" object:Nil];
//            
//        });
//    });

}


- (NSDictionary *) fetchProfile: (int)userid_pro
{
    NSMutableDictionary *dict_pr = [[NSMutableDictionary alloc]init];

//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        
//        Mymodel *obj=[Mymodel getInstance];
//        obj.db_busy = YES;
    
    NSString *query_pr = [NSString stringWithFormat:@"SELECT * FROM Profile where userid = '%d'",userid_pro];
    //    sqlite3_stmt *statement;
    DebugLog(@"select query profile: %@",query_pr);
    @try {
        if (sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
        {
            DebugLog(@"statement: %d",sqlite3_prepare_v2(database, [query_pr UTF8String], -1, &statement, NULL));
            
            if (sqlite3_prepare_v2(database, [query_pr UTF8String], -1, &statement, NULL) == SQLITE_OK) {
                
                if (sqlite3_step(statement) == SQLITE_ROW) {
                    
                    int userid_pr = sqlite3_column_int(statement, 0);
                    char *b_city_pr = (char *) sqlite3_column_text(statement, 1);
                    char *b_company_pr = (char *) sqlite3_column_text(statement, 2);
                    char *b_country_pr = (char *) sqlite3_column_text(statement, 3);
                    char *b_email_pr = (char *) sqlite3_column_text(statement, 4);
                    char *b_function_pr = (char *) sqlite3_column_text(statement, 5);
                    char *b_housenumber_pr = (char *) sqlite3_column_text(statement, 6);
                    char *b_lat_pr = (char *) sqlite3_column_text(statement, 7);
                    char *b_lng_pr = (char *) sqlite3_column_text(statement, 8);
                    char *b_phone_num_pr = (char *) sqlite3_column_text(statement, 9);
                    char *b_phone_pre_pr = (char *) sqlite3_column_text(statement, 10);
                    int b_phone_c_pr = sqlite3_column_int(statement, 11);
                    char *b_street_pr = (char *) sqlite3_column_text(statement, 12);
                    char *b_website_pr = (char *) sqlite3_column_text(statement, 13);
                    char *b_zipcode_pr = (char *) sqlite3_column_text(statement, 14);
                    char *email_pr = (char *) sqlite3_column_text(statement, 15);
                    char *housenumber_pr = (char *) sqlite3_column_text(statement, 16);
                    char *lat_pr = (char *) sqlite3_column_text(statement, 17);
                    char *lng_pr = (char *) sqlite3_column_text(statement, 18);
                    char *mobile_num_pr = (char *) sqlite3_column_text(statement, 19);
                    char *mobile_pre_pr = (char *) sqlite3_column_text(statement, 20);
                    int mobile_c_pr = sqlite3_column_int(statement, 21);
                    char *name_pr = (char *) sqlite3_column_text(statement, 22);
                    char *phone_num_pr = (char *) sqlite3_column_text(statement, 23);
                    char *phone_pre_pr = (char *) sqlite3_column_text(statement, 24);
                    int phone_c_pr = sqlite3_column_int(statement, 25);
                    char *street_pr = (char *) sqlite3_column_text(statement, 26);
                    char *surname_pr = (char *) sqlite3_column_text(statement, 27);
                    char *website_pr = (char *) sqlite3_column_text(statement, 28);
                    char *zipcode_pr = (char *) sqlite3_column_text(statement, 29);
                    int business_pr = sqlite3_column_int(statement, 30);
                    char *city_pr = (char *) sqlite3_column_text(statement, 31);
                    char *country_pr = (char *) sqlite3_column_text(statement, 32);
                    int friend_pr = sqlite3_column_int(statement, 33);
                    int own_pr = sqlite3_column_int(statement, 34);
                    char *thumb_pr = (char *) sqlite3_column_text(statement, 35);
                    char *s_facebook_pr = (char *) sqlite3_column_text(statement, 36);
                    char *s_google_pr = (char *) sqlite3_column_text(statement, 37);
                    char *s_instagram_pr = (char *) sqlite3_column_text(statement, 38);
                    char *s_linkedin_pr = (char *) sqlite3_column_text(statement, 39);
                    char *s_pinterest_pr = (char *) sqlite3_column_text(statement, 40);
                    char *s_skype_pr = (char *) sqlite3_column_text(statement, 41);
                    char *s_soundcloud_pr = (char *) sqlite3_column_text(statement, 42);
                    char *s_twitter_pr = (char *) sqlite3_column_text(statement, 43);
                    char *s_vimeo_pr = (char *) sqlite3_column_text(statement, 44);
                    char *s_youtube_pr = (char *) sqlite3_column_text(statement, 45);
                    
                    
                    
                    NSString *lat_str_pr = [[NSString alloc] initWithUTF8String:lat_pr];
                    NSString *lng_str_pr = [[NSString alloc] initWithUTF8String:lng_pr];
                    NSString *b_city_str_pr = [[NSString alloc] initWithUTF8String:b_city_pr];
                    NSString *b_company_str_pr = [[NSString alloc] initWithUTF8String:b_company_pr];
                    NSString *b_country_str_pr = [[NSString alloc] initWithUTF8String:b_country_pr];
                    NSString *b_email_str_pr = [[NSString alloc] initWithUTF8String:b_email_pr];
                    NSString *b_function_str_pr = [[NSString alloc] initWithUTF8String:b_function_pr];
                    NSString *b_housenumber_str_pr = [[NSString alloc] initWithUTF8String:b_housenumber_pr];
                    NSString *b_lat_str_pr = [[NSString alloc] initWithUTF8String:b_lat_pr];
                    NSString *b_lng_str_pr = [[NSString alloc] initWithUTF8String:b_lng_pr];
                    NSString *b_phone_num_str_pr = [[NSString alloc] initWithUTF8String:b_phone_num_pr];
                    NSString *b_phone_pre_str_pr = [[NSString alloc] initWithUTF8String:b_phone_pre_pr];
                    NSString *b_street_str_pr = [[NSString alloc] initWithUTF8String:b_street_pr];
                    NSString *b_website_str_pr = [[NSString alloc] initWithUTF8String:b_website_pr];
                    NSString *b_zipcode_str_pr = [[NSString alloc] initWithUTF8String:b_zipcode_pr];
                    NSString *email_str_pr = [[NSString alloc] initWithUTF8String:email_pr];
                    NSString *housenumber_str_pr = [[NSString alloc] initWithUTF8String:housenumber_pr];
                    NSString *mobile_pre_str_pr = [[NSString alloc] initWithUTF8String:mobile_pre_pr];
                    NSString *mobile_num_str_pr = [[NSString alloc] initWithUTF8String:mobile_num_pr];
                    NSString *street_str_pr = [[NSString alloc] initWithUTF8String:street_pr];
                    NSString *website_str_pr = [[NSString alloc] initWithUTF8String:website_pr];
                    NSString *zipcode_str_pr = [[NSString alloc] initWithUTF8String:zipcode_pr];
                    
                    NSString *thumb_str_pr = [[NSString alloc] initWithUTF8String:thumb_pr];
                    NSString *phone_pre_str_pr = [[NSString alloc] initWithUTF8String:phone_pre_pr];
                    NSString *phone_num_str_pr = [[NSString alloc] initWithUTF8String:phone_num_pr];
                    NSString *country_str_pr = [[NSString alloc] initWithUTF8String:country_pr];
                    NSString *city_str_pr = [[NSString alloc] initWithUTF8String:city_pr];
                    NSString *name_str_pr = [[NSString alloc] initWithUTF8String:name_pr];
                    NSString *surname_str_pr = [[NSString alloc] initWithUTF8String:surname_pr];
                    NSString *s_facebook_str_pr = [[NSString alloc] initWithUTF8String:s_facebook_pr];
                    NSString *s_google_str_pr = [[NSString alloc] initWithUTF8String:s_google_pr];
                    NSString *s_instagram_str_pr = [[NSString alloc] initWithUTF8String:s_instagram_pr];
                    NSString *s_linkedin_str_pr = [[NSString alloc] initWithUTF8String:s_linkedin_pr];
                    NSString *s_pinterest_str_pr = [[NSString alloc] initWithUTF8String:s_pinterest_pr];
                    NSString *s_skype_str_pr = [[NSString alloc] initWithUTF8String:s_skype_pr];
                    NSString *s_soundcloud_str_pr = [[NSString alloc] initWithUTF8String:s_soundcloud_pr];
                    NSString *s_twitter_str_pr = [[NSString alloc] initWithUTF8String:s_twitter_pr];
                    NSString *s_vimeo_str_pr = [[NSString alloc] initWithUTF8String:s_vimeo_pr];
                    NSString *s_youtube_str_pr = [[NSString alloc] initWithUTF8String:s_youtube_pr];
                    
                    
                    [dict_pr setObject:[NSString stringWithFormat:@"%d",business_pr] forKey:@"business"];
                    [dict_pr setObject:[NSString stringWithFormat:@"%d",friend_pr] forKey:@"friend"];
                    [dict_pr setObject:[NSString stringWithFormat:@"%d",own_pr] forKey:@"own"];
                    [dict_pr setObject:name_str_pr forKey:@"name"];
                    [dict_pr setObject:surname_str_pr forKey:@"surname"];
                    [dict_pr setObject:[NSString stringWithFormat:@"%d",userid_pr] forKey:@"id"];
                    [dict_pr setObject:thumb_str_pr forKey:@"thumb"];
                    [dict_pr setObject:phone_pre_str_pr forKey:@"phone_pre"];
                    [dict_pr setObject:phone_num_str_pr forKey:@"phone_num"];
                    [dict_pr setObject:mobile_pre_str_pr forKey:@"mobile_pre"];
                    [dict_pr setObject:mobile_num_str_pr forKey:@"mobile_num"];
                    [dict_pr setObject:email_str_pr forKey:@"email"];
                    [dict_pr setObject:country_str_pr forKey:@"country"];
                    [dict_pr setObject:city_str_pr forKey:@"city"];
                    [dict_pr setObject:b_phone_pre_str_pr forKey:@"b_phone_pre"];
                    [dict_pr setObject:b_phone_num_str_pr forKey:@"b_phone_num"];
                    [dict_pr setObject:lat_str_pr forKey:@"lat"];
                    [dict_pr setObject:lng_str_pr forKey:@"lng"];
                    
                    [dict_pr setObject:[NSString stringWithFormat:@"%d",phone_c_pr] forKey:@"phone_c"];
                    [dict_pr setObject:[NSString stringWithFormat:@"%d",mobile_c_pr] forKey:@"mobile_c"];
                    [dict_pr setObject:[NSString stringWithFormat:@"%d",b_phone_c_pr] forKey:@"b_phone_c"];
                    
                    [dict_pr setObject:b_city_str_pr forKey:@"b_city"];
                    [dict_pr setObject:b_company_str_pr forKey:@"b_company"];
                    [dict_pr setObject:b_country_str_pr forKey:@"b_country"];
                    [dict_pr setObject:b_email_str_pr forKey:@"b_email"];
                    [dict_pr setObject:b_function_str_pr forKey:@"b_function"];
                    [dict_pr setObject:b_housenumber_str_pr forKey:@"b_housenumber"];
                    [dict_pr setObject:phone_num_str_pr forKey:@"phone_num"];
                    [dict_pr setObject:mobile_pre_str_pr forKey:@"mobile_pre"];
                    [dict_pr setObject:mobile_num_str_pr forKey:@"mobile_num"];
                    [dict_pr setObject:b_lat_str_pr forKey:@"b_lat"];
                    [dict_pr setObject:b_lng_str_pr forKey:@"b_lng"];
                    [dict_pr setObject:b_street_str_pr forKey:@"b_street"];
                    [dict_pr setObject:b_website_str_pr forKey:@"b_website"];
                    [dict_pr setObject:b_zipcode_str_pr forKey:@"b_zipcode"];
                    [dict_pr setObject:lat_str_pr forKey:@"lat"];
                    [dict_pr setObject:lng_str_pr forKey:@"lng"];
                    
                    [dict_pr setObject:housenumber_str_pr forKey:@"housenumber"];
                    [dict_pr setObject:street_str_pr forKey:@"street"];
                    [dict_pr setObject:website_str_pr forKey:@"website"];
                    [dict_pr setObject:zipcode_str_pr forKey:@"zipcode"];
                    [dict_pr setObject:lat_str_pr forKey:@"lat"];
                    [dict_pr setObject:lng_str_pr forKey:@"lng"];
                    
                    [dict_pr setObject:s_facebook_str_pr forKey:@"s_facebook"];
                    [dict_pr setObject:s_google_str_pr forKey:@"s_google"];
                    [dict_pr setObject:s_instagram_str_pr forKey:@"s_instagram"];
                    [dict_pr setObject:s_linkedin_str_pr forKey:@"s_linkedin"];
                    [dict_pr setObject:s_pinterest_str_pr forKey:@"s_pinterest"];
                    [dict_pr setObject:s_skype_str_pr forKey:@"s_skype"];
                    [dict_pr setObject:s_soundcloud_str_pr forKey:@"s_soundcloud"];
                    [dict_pr setObject:s_twitter_str_pr forKey:@"s_twitter"];
                    [dict_pr setObject:s_vimeo_str_pr forKey:@"s_vimeo"];
                    [dict_pr setObject:s_youtube_str_pr forKey:@"s_youtube"];
                }
                else
                {
                    DebugLog(@"yes payni payni");
                }
                sqlite3_finalize(statement);
            }
            else
                DebugLog(@"Error profile fetch db");
            sqlite3_close(database);
        }
    }
    @catch (NSException *exception) {
        DebugLog(@"excep: %@",exception.description);
    }
    
    DebugLog(@"profile db: %@",dict_pr);

//        dispatch_async(dispatch_get_main_queue(), ^{
//            
//            Mymodel *obj=[Mymodel getInstance];
//
//    obj.db_busy = NO;
//            
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"db_busy" object:Nil];
//            
//        });
//    });

    return dict_pr;
}


-(void)closeDB
{
//   //sqlite3_close(database);
}
@end