//
//  DBManager.h
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 16/06/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DBManager : NSObject
{
    NSString *databasePath;
}
+(DBManager *)getSharedInstance;
- (void) insertRequests: (NSArray *)array;
- (void) insertMap: (NSMutableArray *)array;
- (void) insertContacts: (NSMutableArray *)array;
- (void) insertProfile: (NSDictionary *)dict : (int)userid;
-(NSArray *)fetchRequests;
-(NSArray *)fetchMap;
-(NSArray *)fetchContacts;
-(NSDictionary *)fetchProfile :(int)userid;
-(void)closeDB;
@end
