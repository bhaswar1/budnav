//
//  GroupInfoAdminViewController.m
//  Budnav
//
//  Created by ios on 17/09/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "GroupInfoAdminViewController.h"
#import "ConGroupSettingsViewController.h"
#import "AddMemberViewController.h"
#import "AppDelegate.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

@interface GroupInfoAdminViewController ()<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UITextFieldDelegate,UIAlertViewDelegate>
{
    UIView *backview,*dividerview1,*dividerview2,*dividerview3,*dividerview4,*dividerview5,*grayview1,*grayview2,*dividerview6,*grayview3,*popupview;
    UIImageView *logo_img,*bck_img,*groupimg,*arrow1,*arrow2,*moreImage;
    UIButton *backbtn,*addmemberbtn,*groupsettingbtn,*menu;
    UILabel *membernolbl,*lbl1,*lbl2,*addmemberlbl,*groupsettinglbl;
    UISwitch *onoff;
    UIScrollView *mainscroll;
    NSString *name;
    NSMutableDictionary *groupdict;
    NSString *groupId,*totalMember;
    UIActivityIndicatorView *act;
    int checklbl,myswitchcheck1,myswitchcheck2;
    BOOL expand;
    id switchcheck1,switchckeck2;
    UITableView *menutable;
    AppDelegate *app;
    UISwitch *mySwitch, *mySwitch2;
    UIActivityIndicatorView *activity;
    
//     UIView *mainview,*blackBack,*searchview,*popupview,*grayView,*change;
//     UIButton *backbutton,*crossbutton,*bigbackbutton;
//     UILabel *back_label,*membercount;;
//     UIImageView *crossimage,*backimage,*moreImage;
//     UILabel *lineDiv, *lbdivider,*nouserlabel;
//      UITextField *searchfield;
}
@end

@implementation GroupInfoAdminViewController
@synthesize groupnamelbl;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"Dictionary.....%@",self.mydic);
      NSLog(@"Group Dictionary.....%@",self.grpdict);
    checklbl=0;
    myswitchcheck1=0;
    myswitchcheck2=0;
    switchcheck1=@"false";
    switchckeck2=@"false";
    
    app = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
     //NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [[NSUserDefaults standardUserDefaults] setObject:[self.mydic objectForKey:@"id"] forKey:@"group_id"];
   // [prefs synchronize];
    
    backview=[[UIView alloc]initWithFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    [backview setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:backview];
    
    UIView *coverview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64)];
    [backview addSubview:coverview];
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, coverview.frame.size.height);
    
    // Add the gradient to the view
    [coverview.layer insertSublayer:gradient atIndex:0];
    
    bck_img = [[UIImageView alloc]initWithFrame:CGRectMake(15,32,12,20)];//(13, 39, 10, 22)];
    bck_img.image=[UIImage imageNamed:@"back3"];
    [self.view addSubview:bck_img];
    
    logo_img = [[UIImageView alloc]init];
    logo_img.frame = CGRectMake(27+([UIScreen mainScreen].bounds.size.width-160)/2, 32, 101, 20);
    logo_img.backgroundColor = [UIColor clearColor];
    
    logo_img.image = [UIImage imageNamed:@"new_logo"];
    [coverview addSubview:logo_img];
    
    backbtn=[[UIButton alloc]initWithFrame:CGRectMake(0.0f,0.0f,74,64)];
    [backbtn setBackgroundColor:[UIColor clearColor]];
    [coverview addSubview:backbtn];
    
    [backbtn addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDown];
    [backbtn addTarget:self action:@selector(changecoloragain) forControlEvents:UIControlEventTouchDragExit];
    [backbtn addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDragEnter];
    [backbtn addTarget:self action:@selector(backtoprev:) forControlEvents:UIControlEventTouchUpInside];
    
    mainscroll=[[UIScrollView alloc]initWithFrame:CGRectMake(0.0f,64.0f,self.view.frame.size.width,self.view.frame.size.height)];
    //mainscroll.backgroundColor=[UIColor lightGrayColor];
    [backview addSubview:mainscroll];
    
    [mainscroll setContentSize:CGSizeMake(self.view.frame.size.width,600.0f)];
    mainscroll.showsVerticalScrollIndicator=NO;
    
    
//    moreImage=[[UIImageView alloc]initWithFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-37, 32, 22,19)];
//    
//    
//    [moreImage setImage:[UIImage imageNamed:@"new_menu"]];
//    [coverview addSubview:moreImage];
//    
//    menu=[[UIButton alloc]initWithFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-55,[[UIApplication sharedApplication]statusBarFrame].size.height, 55,coverview.frame.size.height-[[UIApplication sharedApplication]statusBarFrame].size.height)];//CGRectMake(290, 32, 22, 28)];
//    [menu setBackgroundImage:nil forState:UIControlStateDisabled];
//    [menu setBackgroundImage:nil forState:UIControlStateSelected];
//    [menu setBackgroundImage:nil forState:UIControlStateHighlighted];
//    [menu setUserInteractionEnabled:YES];
//    //[menu setBackgroundColor:[UIColor grayColor]];
//    [menu addTarget:self action:@selector(gomenu) forControlEvents:UIControlEventTouchUpInside];
//    menu.contentMode=UIViewContentModeScaleAspectFill;
//    expand=NO;
//   // menu.enabled=NO;
//    [coverview addSubview:menu];
    
}

//-(void)gomenu
//{
//     NSLog(@"123456789");
//    
//    if (!expand)
//    {
//        
//            popupview=[[UIView alloc]initWithFrame:CGRectMake(0,64, self.view.frame.size.width,100)];
//            popupview.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.9];
//            popupview.opaque=NO;
//            //popupview.layer.cornerRadius=5;
//            //            popupview.layer.shadowOpacity=0.8;
//            //            popupview.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
//            [self.view addSubview:popupview];
//        
//        menutable=[[UITableView alloc]initWithFrame:CGRectMake(0, self.view.frame.origin.y, popupview.frame.size.width, popupview.frame.size.height)];
//        menutable.delegate=self;
//        menutable.tag=2;
//        menutable.dataSource=self;
//        [menutable setBackgroundColor:[UIColor clearColor]];
//        //menutable.separatorInset=UIEdgeInsetsMake(<#CGFloat top#>, <#CGFloat left#>, <#CGFloat bottom#>, <#CGFloat right#>)
//        //menutable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
//        menutable.separatorStyle=UITableViewCellSeparatorStyleNone;
//        menutable.scrollEnabled=NO;
//    
//        menutable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
//        [popupview addSubview:menutable];
//        [menutable reloadData];
//        
//        expand=YES;
//        //backbutton.tag=1;
//        
//    }
//    else if (expand)
//    {
//        popupview.hidden=YES;
//        menu.tag=9;
//       // backbutton.tag=0;
//        expand=NO;
//    }
//  
//
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    static NSString *MyIdentifier = @"MyReuseIdentifier";
//    
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
//    cell=nil;
//    
//    if (cell == nil)
//    {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:MyIdentifier];
//    }
//    
//    if (tableView.tag==2)
//    {
//        cell.backgroundColor=[UIColor clearColor];
//        cell.selectionStyle=UITableViewCellSelectionStyleNone;
//        
//        UILabel *menulabel = [[UILabel alloc]initWithFrame:CGRectMake(0.0f,0.0f, menutable.frame.size.width, 49.0f)];
//        if(indexPath.row==0)
//        {
//            menulabel.text=@"Email Group";
//            UILabel *separatorlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 49.5f, [UIScreen mainScreen].bounds.size.width, 0.5f)];
//            separatorlabel.backgroundColor=[UIColor grayColor];
//            [cell addSubview:separatorlabel];
//        }
//        else
//        {
//            menulabel.text=@"SMS Group";
//        }
//           // [menulabel setText:[NSString stringWithFormat:@"%@",[arraymenu_admin objectAtIndex:indexPath.row]]];
//            [menulabel setTextAlignment:NSTextAlignmentCenter];
//            [menulabel setTextColor:[UIColor whiteColor]];
//            [menulabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:19]];
//            [cell addSubview:menulabel];
//            
//        
//    }
//
//    return cell;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section1
//{
//    if (tableView.tag==2)
//    {
//       
//        return 2;
//        
//    }
//    return 0;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (tableView.tag==2)
//    {
//        return 50.0f;
//    }
//    else
//        return 55.0f;
//}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    act = [[UIActivityIndicatorView alloc] init];
    act.center = self.view.center;
    act.hidesWhenStopped=YES;
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [act setColor:[UIColor blackColor]];
    act.backgroundColor=[UIColor blueColor];
    [self.view addSubview:act];
    act.layer.zPosition=2;
    [NSThread detachNewThreadSelector: @selector(Start) toTarget:self withObject:nil];
    
    
    NSOperationQueue *queue = [NSOperationQueue new];
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(loaddata)
                                        object:nil];
    [queue addOperation:operation];
    
    // UIImageView  *img =[[UIImageView alloc] initWithFrame:CGRectMake(85.0f,coverview.frame.origin.y+coverview.frame.size.height+25,self.view.frame.size.width-170,self.view.frame.size.width-170)];
    // [img sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[groupsarray objectAtIndex:indexPath.row]objectForKey:@"thumb"]]] placeholderImage:[UIImage imageNamed:@"placeholder"] options:/* DISABLES CODE */ (0) == 0?SDWebImageRefreshCached : 0];
    //img.image=[UIImage imageNamed:@"Backup profile pic"];
//    UIImageView  *img =[[UIImageView alloc] initWithFrame:CGRectMake(85.0f,25.0f,self.view.frame.size.width-170,self.view.frame.size.width-170)];
//    
//    img.image=[UIImage imageNamed:@"placeholderimg"];
//   
//    
//    NSString *base64string = [NSString stringWithFormat:@"%@", [self.mydic objectForKey:@"thumb"] ];
//    
//    if (base64string>0)
//    {
//        NSData *imageData = [[NSData alloc] initWithBase64EncodedString:base64string options:0];
//        img.image = [UIImage imageWithData:imageData];
//    }
//    
//    
////    if(self.mydata2.length>0)
////    {
////        img.image = [UIImage imageWithData:[NSData dataWithData:self.mydata2]];
////    }
//    else
//    {
//          img.image=[UIImage imageNamed:@"placeholderimg"];
//    }
//     img.layer.cornerRadius = img.frame.size.width/2;
//    
//    img.contentMode = UIViewContentModeScaleAspectFit;
//    img.clipsToBounds=YES;
//    [mainscroll addSubview:img];
//    
//    
//    
//    
//    groupnamelbl = [[UILabel alloc]initWithFrame:CGRectMake(50.0f,img.frame.origin.y+img.frame.size.height+23,self.view.frame.size.width-100,20)];
//    [groupnamelbl setTextAlignment:NSTextAlignmentCenter];
//    groupnamelbl.textColor=[UIColor blackColor];
//    [groupnamelbl setFont:[UIFont fontWithName:@"ProximaNova-Bold" size:17]];
//    
//    if ((NSNull *)[self.mydic objectForKey:@"name"] == [NSNull null])
//    {
//          groupnamelbl.text=@"";
//       
//    }
//    else
//    {
//       groupnamelbl.text=[self.mydic objectForKey:@"name"];
//    }
//   
//    [mainscroll addSubview:groupnamelbl];
//    
//    membernolbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0f,groupnamelbl.frame.origin.y+groupnamelbl.frame.size.height+13,self.view.frame.size.width-40,20)];
//    [membernolbl setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:15]];
//     [membernolbl setTextAlignment:NSTextAlignmentCenter];
//    
//     if ((NSNull *)[self.mydic objectForKey:@"count"] == [NSNull null])
//     {
//         membernolbl.text=@"";
//     }
//    else
//    {
//      membernolbl.text=[NSString stringWithFormat:@"%@ Members - Personal Group",[self.mydic objectForKey:@"count"]];
//    }
//   
//    [membernolbl setTextColor:[UIColor colorWithRed:(171.0f/255.0f) green:(171.0f/255.0f) blue:(171.0f/255.0f) alpha:1.0f]];
//    [mainscroll addSubview:membernolbl];
//    
//    
//    dividerview1=[[UIView alloc]initWithFrame:CGRectMake(0.0f,membernolbl.frame.origin.y+membernolbl.frame.size.height+23,self.view.frame.size.width,0.5f)];
//    [dividerview1 setBackgroundColor:[UIColor colorWithRed:(204.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f]];
//    [mainscroll addSubview:dividerview1];
//    
//    lbl1=[[UILabel alloc]initWithFrame:CGRectMake(20.0f,dividerview1.frame.origin.y+dividerview1.frame.size.height+15,230,20)];
//    lbl1.text=@"Show group in contact list";
//    [lbl1 setTextAlignment:NSTextAlignmentLeft];
//    [lbl1 setTextColor:[UIColor blackColor]];
//    [lbl1 setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:18]];
//    [mainscroll addSubview:lbl1];
//    
//    
//    
//    
//    UISwitch *mySwitch = [[UISwitch alloc] initWithFrame:CGRectMake(self.view.frame.size.width-70,dividerview1.frame.origin.y+10, 0, 0)];
//    mySwitch.onTintColor = [UIColor colorWithRed:(40.0/255.0) green:(169.0/255.0) blue:(184.0/255.0) alpha:1.0];
//    //[mySwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
//    [mainscroll addSubview:mySwitch];
//    
//    dividerview2=[[UIView alloc]initWithFrame:CGRectMake(0.0f,dividerview1.frame.origin.y+dividerview1.frame.size.height+50,self.view.frame.size.width,0.5f)];
//    [dividerview2 setBackgroundColor:[UIColor colorWithRed:(204.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f]];
//    [mainscroll addSubview:dividerview2];
//    
//    dividerview3=[[UIView alloc]initWithFrame:CGRectMake(0.0f,dividerview2.frame.origin.y+dividerview2.frame.size.height+50,self.view.frame.size.width,0.5f)];
//    [dividerview3 setBackgroundColor:[UIColor colorWithRed:(204.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f]];
//    [mainscroll addSubview:dividerview3];
//    
//    lbl2=[[UILabel alloc]initWithFrame:CGRectMake(20.0f,dividerview2.frame.origin.y+dividerview2.frame.size.height+15,230,20)];
//    lbl2.text=@"Show group on map";
//    [lbl2 setTextAlignment:NSTextAlignmentLeft];
//    [lbl2 setTextColor:[UIColor blackColor]];
//    [lbl2 setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:18]];
//    [mainscroll addSubview:lbl2];
//    
//    
//    UISwitch *mySwitch2 = [[UISwitch alloc] initWithFrame:CGRectMake(self.view.frame.size.width-70,dividerview2.frame.origin.y+10, 0, 0)];
//    mySwitch2.onTintColor = [UIColor colorWithRed:(40.0/255.0) green:(169.0/255.0) blue:(184.0/255.0) alpha:1.0];
//    //[mySwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
//    [mainscroll addSubview:mySwitch2];
//    
//    dividerview4=[[UIView alloc]initWithFrame:CGRectMake(0.0f,dividerview3.frame.origin.y+dividerview3.frame.size.height+35,self.view.frame.size.width,0.5f)];
//    [dividerview4 setBackgroundColor:[UIColor colorWithRed:(204.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f]];
//    [mainscroll addSubview:dividerview4];
//    
//    dividerview5=[[UIView alloc]initWithFrame:CGRectMake(0.0f,dividerview4.frame.origin.y+dividerview4.frame.size.height+50,self.view.frame.size.width,0.5f)];
//    [dividerview5 setBackgroundColor:[UIColor colorWithRed:(204.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f]];
//    [mainscroll addSubview:dividerview5];
//    
//    
//    grayview1=[[UIView alloc]initWithFrame:CGRectMake(0.0,dividerview3.frame.origin.y+dividerview3.frame.size.height,self.view.frame.size.width,35)];
//    [grayview1 setBackgroundColor:[UIColor colorWithRed:(237.0f/255.0f) green:(237.0f/255.0f) blue:(237.0f/255.0f) alpha:1.0f]];
//    [mainscroll addSubview:grayview1];
//    
//    
//        groupsettingbtn=[[UIButton alloc]initWithFrame:CGRectMake(0.0f,dividerview5.frame.origin.y+dividerview5.frame.size.height,self.view.frame.size.width,50.0f)];
//         groupsettingbtn.backgroundColor=[UIColor clearColor];
//    [groupsettingbtn addTarget:self action:@selector(editgroup) forControlEvents:UIControlEventTouchUpInside];
//    //    [addmemberbtn setTitle:@"Add members" forState:UIControlStateNormal];
//    //    //leavegrpbtn.contentMode=UIViewContentModeBottom;
//    //    addmemberbtn.titleLabel.font=[UIFont fontWithName:@"ProximaNova-Regular" size:18.0];
//    //    [addmemberbtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
//        [groupsettingbtn setUserInteractionEnabled:YES];
//        [mainscroll addSubview:groupsettingbtn];
//    
//    addmemberlbl=[[UILabel alloc]initWithFrame:CGRectMake(20.0f,dividerview4.frame.origin.y+dividerview4.frame.size.height+15,150,20.0f)];
//    addmemberlbl.text=@"Add members";
//    [lbl2 setTextAlignment:NSTextAlignmentLeft];
//    [lbl2 setTextColor:[UIColor blackColor]];
//    [addmemberlbl setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:18]];
//    [mainscroll addSubview:addmemberlbl];
//    
//    arrow1 = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width-35,dividerview4.frame.origin.y+20,15,15)];
//    [mainscroll addSubview:arrow1];
//    arrow1.image= [UIImage imageNamed:@"grey_arrow"];
//    arrow1.contentMode=UIViewContentModeScaleAspectFit;
//    
//    groupsettinglbl=[[UILabel alloc]initWithFrame:CGRectMake(20.0f,dividerview5.frame.origin.y+dividerview5.frame.size.height+15,150,20.0f)];
//    groupsettinglbl.text=@"Group settings";
//    [groupsettinglbl setTextAlignment:NSTextAlignmentLeft];
//    [groupsettinglbl setTextColor:[UIColor blackColor]];
//    [groupsettinglbl setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:18]];
//    [mainscroll addSubview:groupsettinglbl];
//    
//    
//    arrow2 = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width-35,dividerview5.frame.origin.y+20,15,15)];
//    [mainscroll addSubview:arrow2];
//    arrow2.image= [UIImage imageNamed:@"grey_arrow"];
//    arrow2.contentMode=UIViewContentModeScaleAspectFit;
//    
//    
//    dividerview6=[[UIView alloc]initWithFrame:CGRectMake(0.0f,dividerview5.frame.origin.y+dividerview5.frame.size.height+50,self.view.frame.size.width,0.5f)];
//    [dividerview6 setBackgroundColor:[UIColor colorWithRed:(204.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f]];
//    [mainscroll addSubview:dividerview6];
//    
//    grayview3=[[UIView alloc]initWithFrame:CGRectMake(0.0,dividerview6.frame.origin.y+dividerview6.frame.size.height,self.view.frame.size.width,self.view.frame.size.height-(dividerview6.frame.origin.y+dividerview6.frame.size.height))];
//    [grayview3 setBackgroundColor:[UIColor colorWithRed:(237.0f/255.0f) green:(237.0f/255.0f) blue:(237.0f/255.0f) alpha:1.0f]];
//    [mainscroll addSubview:grayview3];
//    
//    addmemberbtn=[[UIButton alloc]initWithFrame:CGRectMake(0.0f,dividerview4.frame.origin.y+dividerview4.frame.size.height,self.view.frame.size.width,50.0f)];
//    addmemberbtn.backgroundColor=[UIColor clearColor];
//    [addmemberbtn addTarget:self action:@selector(addmembers) forControlEvents:UIControlEventTouchUpInside];
//    //    [addmemberbtn setTitle:@"Add members" forState:UIControlStateNormal];
//    //    //leavegrpbtn.contentMode=UIViewContentModeBottom;
//    //    addmemberbtn.titleLabel.font=[UIFont fontWithName:@"ProximaNova-Regular" size:18.0];
//    //    [addmemberbtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
//    [addmemberbtn setUserInteractionEnabled:YES];
//    [mainscroll addSubview:addmemberbtn];
    // Do any additional setup after loading the view.
}

-(void)addmembers
{
    NSLog(@"okkkk");
  AddMemberViewController *congrpset=[[AddMemberViewController alloc]init];
    congrpset.group_id=groupId;
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.3;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    transition.subtype = kCATransitionFromRight;
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
    [self.navigationController pushViewController:congrpset animated:NO];
    
  //  [self.navigationController presentViewController:congrpset animated:YES completion:Nil];
    
 
        
}

-(void)editgroup
{
    NSLog(@"ok");
    ConGroupSettingsViewController *congrpset=[[ConGroupSettingsViewController alloc]init];
    //congrpset.group_id= groupId;
    congrpset.group_id=[[NSUserDefaults standardUserDefaults] objectForKey:@"group_id"];
    //congrpset.groupname=groupnamelbl.text;
    //congrpset.membercount=[self.mydic objectForKey:@"count"];
    //name=[NSString stringWithFormat:@"%@ %@",[self.mydic objectForKey:@"name"],[self.mydic objectForKey:@"surname"]];
   // backbutton.tag=0;
   // popupview.hidden=YES;
   // expand=NO;
    
    
    NSMutableDictionary *mdic = [_grpdict valueForKey:@"details"];
    NSMutableArray *marr = [mdic valueForKey:@"members"];
    NSMutableDictionary *mdic2 = [marr objectAtIndex:0];
    
    congrpset.adminname=[NSString stringWithFormat:@"%@ %@",[mdic2 objectForKey:@"name"],[mdic2 objectForKey:@"surname"]];
    DebugLog(@"admin name: %@",congrpset.adminname);
    //DebugLog(@"12345: %@",[group_array valueForKey:@"name"]);
    
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.3;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    transition.subtype = kCATransitionFromRight;
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
    [self.navigationController pushViewController:congrpset animated:NO];
    
    checklbl=1;
    
    
//    //    NSString *groups_url =[NSString stringWithFormat:@"https://budnav.com/ext/?action=group-edit&group_id=%@&access_token=%@&device_id=%@",groupid,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
//    //    DebugLog(@"groups url string ------ %@",groups_url);
//    //
//    //    NSError *error1=nil;
//    //    @try {
//    //        NSData *data1=[NSData dataWithContentsOfURL:[NSURL URLWithString:groups_url]options:NSDataReadingUncached error:&error1];
//    //        if (data1 != nil)
//    //            groupdict=[NSJSONSerialization JSONObjectWithData:data1 //1
//    //                                                      options:kNilOptions
//    //                                                        error:&error1];
//    //        DebugLog(@"-----Groups info ----- %@",groupdict);
//    //    }
//    //    @catch (NSException *exception) {
//    //        DebugLog(@"exception %@",exception);
}

-(void)backtoprev:(id)sender
{
    GroupDetailsViewController *grpDetails = [[GroupDetailsViewController alloc] init];
    
   // grpDetails.groupname = groupnamelbl.text;
  //      AppDelegate *app=[[AppDelegate alloc]init];
  //  grpDetails.groupname=app.groupnameUpdate;
    
    
    DebugLog(@"Group Name POP:%@",grpDetails.groupname);
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)loaddata
{
    
//  NSUserDefaults *prefs1 = [NSUserDefaults standardUserDefaults];
//    groupId=[prefs1 objectForKey:@"group_id"];
    
   // groupId = [self.mydic valueForKey:@"id"];
    
   // totalMember = [self.mydic valueForKey:@"count"];
   // DebugLog(@"TOTAL MEMBERS:%@",totalMember);
    
    groupdict=[[NSMutableDictionary alloc]init];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *groups_url =[NSString stringWithFormat:@"https://budnav.com/ext/?action=group-get&group_id=%@&image=%d&access_token=%@&device_id=%@",[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"group_id"]],true,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
    DebugLog(@"groups url string ------ %@",groups_url);
    
    NSError *error1=nil;
    @try {
        NSData *data1=[NSData dataWithContentsOfURL:[NSURL URLWithString:groups_url]options:NSDataReadingUncached error:&error1];
        if (data1==nil)
        {
            DebugLog(@"no connnnn");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                                            message:nil
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            alert.tag=5;
            [alert show];
            
            
            
        }
        
        if (data1 != nil)
            groupdict=[NSJSONSerialization JSONObjectWithData:data1 //1
                                                      options:kNilOptions
                                                        error:&error1];
        DebugLog(@"-----Groups info ----- %@",groupdict);
        
        [_mydic removeAllObjects];
        _mydic = [[groupdict objectForKey:@"details"] mutableCopy];
        
       
        if ([[[groupdict objectForKey:@"details"] objectForKey:@"type"] intValue]==2)
            
        {
            
            self.screenName = @"Business group info";
            
            [[GAI sharedInstance].defaultTracker send:
             
             [[GAIDictionaryBuilder createEventWithCategory:@"Business group"
               
                                                     action:@"Business group info"
               
                                                      label:nil
               
                                                      value:nil] build]];
            
        }
        
        else if ([[[groupdict objectForKey:@"details"] objectForKey:@"type"] intValue]==1)
            
        {
            
            self.screenName = @"Personal group info";
            
            [[GAI sharedInstance].defaultTracker send:
             
             [[GAIDictionaryBuilder createEventWithCategory:@"Personal group"
               
                                                     action:@"Personal group info"
               
                                                      label:nil
               
                                                      value:nil] build]];
            
        }
        
        
        
    }
    @catch (NSException *exception) {
        DebugLog(@"exception %@",exception);
    }
    [self performSelectorOnMainThread:@selector(datadisplay) withObject:nil waitUntilDone:NO];
}

-(void)datadisplay
{
    [act stopAnimating];
    
    
    
    DebugLog(@"Group DETAILS FROM URL:%@",_mydic);
    
    UIImageView  *img =[[UIImageView alloc] initWithFrame:CGRectMake(85.0f,25.0f,self.view.frame.size.width-170,self.view.frame.size.width-170)];
    
    img.image=[UIImage imageNamed:@"placeholderimg"];
    
    
    NSString *base64string = [NSString stringWithFormat:@"%@", [self.mydic objectForKey:@"image"] ];
    
    if (base64string>0)
    {
        NSData *imageData = [[NSData alloc] initWithBase64EncodedString:base64string options:0];
        img.image = [UIImage imageWithData:imageData];
    }
    
    
    //    if(self.mydata2.length>0)
    //    {
    //        img.image = [UIImage imageWithData:[NSData dataWithData:self.mydata2]];
    //    }
    else
    {
        img.image=[UIImage imageNamed:@"placeholderimg"];
    }
    img.layer.cornerRadius = img.frame.size.width/2;
    
    img.contentMode = UIViewContentModeScaleAspectFit;
    img.clipsToBounds=YES;
    [mainscroll addSubview:img];
    
    
    if (groupnamelbl)
    {
        [groupnamelbl removeFromSuperview];
    }
    
    groupnamelbl = [[UILabel alloc]initWithFrame:CGRectMake(50.0f,img.frame.origin.y+img.frame.size.height+23,self.view.frame.size.width-100,20)];
    [groupnamelbl setTextAlignment:NSTextAlignmentCenter];
    groupnamelbl.textColor=[UIColor blackColor];
    [groupnamelbl setFont:[UIFont fontWithName:@"ProximaNova-Bold" size:17]];
    
    if ((NSNull *)[self.mydic objectForKey:@"name"] == [NSNull null])
    {
        groupnamelbl.text=@"";
        
    }
    else
    {
        DebugLog(@"Group Name:%@",[self.mydic objectForKey:@"name"]);
         DebugLog(@"Group Name SET:%@",groupnamelbl.text);
        groupnamelbl.text=[self.mydic objectForKey:@"name"];
        
       
        app.groupnameUpdate=[self.mydic objectForKey:@"name"];
        DebugLog(@"Appdelegate Set Name:%@",app.groupnameUpdate);
        
    }
    
    [mainscroll addSubview:groupnamelbl];
    
    membernolbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0f,groupnamelbl.frame.origin.y+groupnamelbl.frame.size.height+13,self.view.frame.size.width-40,20)];
    [membernolbl setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:15]];
    [membernolbl setTextAlignment:NSTextAlignmentCenter];
    
    if ([[_mydic objectForKey:@"memberCount"] intValue] == 0)
    {
        membernolbl.text=@"";
    }
    else
    {
       // membernolbl.text=[NSString stringWithFormat:@"%@ Members - Personal Group",totalMember];
        
        if ([[_mydic objectForKey:@"memberCount"] intValue]>1)
        {
            membernolbl.text=[NSString stringWithFormat:@"%@ Members - Personal Group", [_mydic objectForKey:@"memberCount"]];
        }
        else
        {
        membernolbl.text=[NSString stringWithFormat:@"%@ Member - Personal Group", [_mydic objectForKey:@"memberCount"]];
        }
    }
    
    [membernolbl setTextColor:[UIColor colorWithRed:(171.0f/255.0f) green:(171.0f/255.0f) blue:(171.0f/255.0f) alpha:1.0f]];
    [mainscroll addSubview:membernolbl];
    
    
    dividerview1=[[UIView alloc]initWithFrame:CGRectMake(0.0f,membernolbl.frame.origin.y+membernolbl.frame.size.height+23,self.view.frame.size.width,0.5f)];
    [dividerview1 setBackgroundColor:[UIColor colorWithRed:(204.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f]];
    [mainscroll addSubview:dividerview1];
    
    lbl1=[[UILabel alloc]initWithFrame:CGRectMake(20.0f,dividerview1.frame.origin.y+dividerview1.frame.size.height+15,230,20)];
    lbl1.text=@"Show group in contact list";
    [lbl1 setTextAlignment:NSTextAlignmentLeft];
    [lbl1 setTextColor:[UIColor blackColor]];
    [lbl1 setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:18]];
    [mainscroll addSubview:lbl1];
    
    
//    if (mySwitch)
//    {
//         [mySwitch removeFromSuperview];
//    }
//   
//    if (mySwitch2)
//    {
//        [mySwitch2 removeFromSuperview];
//    }
    
    if (!mySwitch)
    {
        
    mySwitch = [[UISwitch alloc] initWithFrame:CGRectMake(self.view.frame.size.width-70,dividerview1.frame.origin.y+10, 0, 0)];
    mySwitch.onTintColor = [UIColor colorWithRed:(40.0/255.0) green:(169.0/255.0) blue:(184.0/255.0) alpha:1.0];
    [mySwitch addTarget:self action:@selector(changeSwitch1) forControlEvents:UIControlEventValueChanged];
    [mainscroll addSubview:mySwitch];
     
        if ([[_mydic objectForKey:@"showContacts"] intValue]==1)
        {
            [mySwitch setOn:YES animated:NO];
            switchcheck1=@"true";
            myswitchcheck1=1;

        }
        else
        {
            [mySwitch setOn:NO animated:NO];
            switchcheck1=@"false";
            myswitchcheck1=0;

        }
      
       // [self urlFired];
    }
    
   //  [self urlFired];

    dividerview2=[[UIView alloc]initWithFrame:CGRectMake(0.0f,dividerview1.frame.origin.y+dividerview1.frame.size.height+50,self.view.frame.size.width,0.5f)];
    [dividerview2 setBackgroundColor:[UIColor colorWithRed:(204.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f]];
    [mainscroll addSubview:dividerview2];
    
    dividerview3=[[UIView alloc]initWithFrame:CGRectMake(0.0f,dividerview2.frame.origin.y+dividerview2.frame.size.height+50,self.view.frame.size.width,0.5f)];
    [dividerview3 setBackgroundColor:[UIColor colorWithRed:(204.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f]];
    [mainscroll addSubview:dividerview3];
    
    lbl2=[[UILabel alloc]initWithFrame:CGRectMake(20.0f,dividerview2.frame.origin.y+dividerview2.frame.size.height+15,230,20)];
    lbl2.text=@"Show group on map";
    [lbl2 setTextAlignment:NSTextAlignmentLeft];
    [lbl2 setTextColor:[UIColor blackColor]];
    [lbl2 setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:18]];
    [mainscroll addSubview:lbl2];
    
    if (!mySwitch2)
    {
        
    mySwitch2 = [[UISwitch alloc] initWithFrame:CGRectMake(self.view.frame.size.width-70,dividerview2.frame.origin.y+10, 0, 0)];
    mySwitch2.onTintColor = [UIColor colorWithRed:(40.0/255.0) green:(169.0/255.0) blue:(184.0/255.0) alpha:1.0];
    [mySwitch2 addTarget:self action:@selector(changeSwitch2) forControlEvents:UIControlEventValueChanged];
    [mainscroll addSubview:mySwitch2];
    
        if ([[_mydic objectForKey:@"showMap"] intValue]==1)
        {
            [mySwitch2 setOn:YES animated:NO];
            switchckeck2=@"true";
            myswitchcheck2=1;

        }
        else
        {
            [mySwitch2 setOn:NO animated:NO];
            switchckeck2=@"false";
            myswitchcheck2=0;
        }
        
       // [self urlFired];
    }
    
 [self urlFired];
    
//    [_mydic removeAllObjects];
//    _mydic = [[groupdict objectForKey:@"details"] mutableCopy];
    

    dividerview4=[[UIView alloc]initWithFrame:CGRectMake(0.0f,dividerview3.frame.origin.y+dividerview3.frame.size.height+35,self.view.frame.size.width,0.5f)];
    [dividerview4 setBackgroundColor:[UIColor colorWithRed:(204.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f]];
    [mainscroll addSubview:dividerview4];
    
    dividerview5=[[UIView alloc]initWithFrame:CGRectMake(0.0f,dividerview4.frame.origin.y+dividerview4.frame.size.height+50,self.view.frame.size.width,0.5f)];
    [dividerview5 setBackgroundColor:[UIColor colorWithRed:(204.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f]];
    [mainscroll addSubview:dividerview5];
    
    
    grayview1=[[UIView alloc]initWithFrame:CGRectMake(0.0,dividerview3.frame.origin.y+dividerview3.frame.size.height,self.view.frame.size.width,35)];
    [grayview1 setBackgroundColor:[UIColor colorWithRed:(237.0f/255.0f) green:(237.0f/255.0f) blue:(237.0f/255.0f) alpha:1.0f]];
    [mainscroll addSubview:grayview1];
    
    
    groupsettingbtn=[[UIButton alloc]initWithFrame:CGRectMake(0.0f,dividerview5.frame.origin.y+dividerview5.frame.size.height,self.view.frame.size.width,50.0f)];
    groupsettingbtn.backgroundColor=[UIColor clearColor];
    [groupsettingbtn addTarget:self action:@selector(editgroup) forControlEvents:UIControlEventTouchUpInside];
    //    [addmemberbtn setTitle:@"Add members" forState:UIControlStateNormal];
    //    //leavegrpbtn.contentMode=UIViewContentModeBottom;
    //    addmemberbtn.titleLabel.font=[UIFont fontWithName:@"ProximaNova-Regular" size:18.0];
    //    [addmemberbtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [groupsettingbtn setUserInteractionEnabled:YES];
    [mainscroll addSubview:groupsettingbtn];
    
    addmemberlbl=[[UILabel alloc]initWithFrame:CGRectMake(20.0f,dividerview4.frame.origin.y+dividerview4.frame.size.height+15,150,20.0f)];
    addmemberlbl.text=@"Add members";
    [lbl2 setTextAlignment:NSTextAlignmentLeft];
    [lbl2 setTextColor:[UIColor blackColor]];
    [addmemberlbl setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:18]];
    [mainscroll addSubview:addmemberlbl];
    
    arrow1 = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width-35,dividerview4.frame.origin.y+20,15,15)];
    [mainscroll addSubview:arrow1];
    arrow1.image= [UIImage imageNamed:@"grey_arrow"];
    arrow1.contentMode=UIViewContentModeScaleAspectFit;
    
    groupsettinglbl=[[UILabel alloc]initWithFrame:CGRectMake(20.0f,dividerview5.frame.origin.y+dividerview5.frame.size.height+15,150,20.0f)];
    groupsettinglbl.text=@"Group settings";
    [groupsettinglbl setTextAlignment:NSTextAlignmentLeft];
    [groupsettinglbl setTextColor:[UIColor blackColor]];
    [groupsettinglbl setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:18]];
    [mainscroll addSubview:groupsettinglbl];
    
    
    arrow2 = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width-35,dividerview5.frame.origin.y+20,15,15)];
    [mainscroll addSubview:arrow2];
    arrow2.image= [UIImage imageNamed:@"grey_arrow"];
    arrow2.contentMode=UIViewContentModeScaleAspectFit;
    
    
    dividerview6=[[UIView alloc]initWithFrame:CGRectMake(0.0f,dividerview5.frame.origin.y+dividerview5.frame.size.height+50,self.view.frame.size.width,0.5f)];
    [dividerview6 setBackgroundColor:[UIColor colorWithRed:(204.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f]];
    [mainscroll addSubview:dividerview6];
    
    grayview3=[[UIView alloc]initWithFrame:CGRectMake(0.0,dividerview6.frame.origin.y+dividerview6.frame.size.height,self.view.frame.size.width,self.view.frame.size.height-(dividerview6.frame.origin.y+dividerview6.frame.size.height))];
    [grayview3 setBackgroundColor:[UIColor colorWithRed:(237.0f/255.0f) green:(237.0f/255.0f) blue:(237.0f/255.0f) alpha:1.0f]];
    [mainscroll addSubview:grayview3];
    
    addmemberbtn=[[UIButton alloc]initWithFrame:CGRectMake(0.0f,dividerview4.frame.origin.y+dividerview4.frame.size.height,self.view.frame.size.width,50.0f)];
    addmemberbtn.backgroundColor=[UIColor clearColor];
    [addmemberbtn addTarget:self action:@selector(addmembers) forControlEvents:UIControlEventTouchUpInside];
    //    [addmemberbtn setTitle:@"Add members" forState:UIControlStateNormal];
    //    //leavegrpbtn.contentMode=UIViewContentModeBottom;
    //    addmemberbtn.titleLabel.font=[UIFont fontWithName:@"ProximaNova-Regular" size:18.0];
    //    [addmemberbtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [addmemberbtn setUserInteractionEnabled:YES];
    [mainscroll addSubview:addmemberbtn];
}

-(void)changeSwitch1
{
    if(mySwitch.on)
    {
     switchcheck1=@"true";
        myswitchcheck1=1;
        [self urlFired];
    }
    else
    {
    switchcheck1=@"false";
        myswitchcheck1=0;
        [self urlFired];
    }
}
-(void)changeSwitch2
{
    if(mySwitch2.on)
    {
    switchckeck2=@"true";
        myswitchcheck2=1;
        [self urlFired];
    }
    else
    {
        switchckeck2=@"false";
        myswitchcheck2=0;
        [self urlFired];
    }
}

-(void)urlFired
{
    [mySwitch setUserInteractionEnabled:NO];
    [mySwitch2 setUserInteractionEnabled:NO];
    
    [self performSelectorOnMainThread:@selector(Start) withObject:nil waitUntilDone:NO];
  
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        
        // Background work
        
         [self performSelectorOnMainThread:@selector(Start) withObject:nil waitUntilDone:NO];
       
        NSString *Newgroups_url =[NSString stringWithFormat:@"https://budnav.com/ext/?action=group-membersettings&group_id=%@&access_token=%@&device_id=%@&show_contacts=%@&show_map=%@",[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"group_id"]],[[NSUserDefaults standardUserDefaults] objectForKey:@"access_token"],[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"],switchcheck1,switchckeck2];
        DebugLog(@"New groups url string ------ %@",Newgroups_url);
         NSError *error1;
        
        NSData *data1=[NSData dataWithContentsOfURL:[NSURL URLWithString:Newgroups_url]options:NSDataReadingUncached error:&error1];
        if (data1==nil)
        {
            DebugLog(@"no connnnn");
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                // Main thread work (UI usually)
                
           
                [mySwitch setUserInteractionEnabled:YES];
                [mySwitch2 setUserInteractionEnabled:YES];
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                                                message:nil
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                alert.tag=5;
                [alert show];
                [act stopAnimating];
                
            }];
            
            
            
            
            
        }
        
        if (data1 != nil)
        {
            
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            // Main thread work (UI usually)
             NSMutableDictionary *newgroupdict=[[NSMutableDictionary alloc]init];
             NSError *error1;
            newgroupdict=[NSJSONSerialization JSONObjectWithData:data1 //1
                                                         options:kNilOptions
                                                           error:&error1];
            DebugLog(@"-----New Groups info ----- %@",newgroupdict);
            [act stopAnimating];
            [mySwitch setUserInteractionEnabled:YES];
            [mySwitch2 setUserInteractionEnabled:YES];
    
        }];
        }
    }];
   
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"OK"])
    {
          [act stopAnimating];
    }
}

- (void) Start
{
    act.hidden = NO;
    [act startAnimating];
}


-(void)changecoloragain
{
    DebugLog(@"CHANGE COLOR");
    //    change.backgroundColor=[UIColor clearColor];
    bck_img.alpha = 1.0f;
    backbtn.alpha = 1.0f;
    //  group_namelbl.alpha=1.0f;
}

-(void)changecolor
{
    DebugLog(@"CHANGE COLOR1");
    //    change.backgroundColor=[UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0];
    bck_img.alpha = 0.5f;
    backbtn.alpha = 0.5f;
    // group_namelbl.alpha=0.5f;
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    bck_img.alpha = 1.0f;
    backbtn.alpha = 1.0f;
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
