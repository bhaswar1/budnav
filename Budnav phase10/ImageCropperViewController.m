//
//  ImageCropperViewController.m
//  Budnav
//
//  Created by Sohini's on 06/02/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "ImageCropperViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

#define SCALE_FRAME_Y 100.0f
#define BOUNDCE_DURATION 0.3f

@interface ImageCropperViewController ()
{
    float x,y;
    CGFloat startpointX;
    CGFloat startpointY;
    CGFloat NewXPosition;
    CGFloat NewYPosition;
    CGFloat NewHeight;
    CGFloat NewWidth;
    BOOL IsPinchExecuted;
    BOOL IsPanExecuted;
    UIImage *RetImage;
    BOOL IsResized;
}

@property (nonatomic, retain) UIImage *originalImage;
@property (nonatomic, retain) UIImage *editedImage;

@property (nonatomic, retain) UIImageView *showImgView;
@property (nonatomic, retain) UIView *overlayView;
@property (nonatomic, retain) UIView *ratioView;

@property (nonatomic, assign) CGRect oldFrame;
@property (nonatomic, assign) CGRect largeFrame;
@property (nonatomic, assign) CGFloat limitRatio;

@property (nonatomic, assign) CGRect latestFrame;


@end

@implementation ImageCropperViewController

- (id)initWithImage:(UIImage *)originalImage cropFrame:(CGRect)cropFrame{// limitScaleRatio:(NSInteger)limitRatio {
    self = [super init];
    if (self) {
        self.cropFrame = cropFrame;
       // self.limitRatio = limitRatio;
        self.originalImage = originalImage;//[self fixOrientation:originalImage];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    IsPanExecuted=NO;
    IsPinchExecuted=NO;
    IsResized=NO;
    // Do any additional setup after loading the view.
    [self initView];
    [self initControlBtn];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return NO;
}

- (void)initView {
    self.view.backgroundColor = [UIColor blackColor];
//    self.showImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    self.showImgView = [[UIImageView alloc] init];//WithFrame:CGRectMake(0, 0, 320, 480)];
    CGFloat imgHeight=self.originalImage.size.height;
    imgHeight=imgHeight*(self.view.frame.size.width/self.originalImage.size.width);
    CGSize PicSize=CGSizeMake(self.view.frame.size.width,imgHeight);
    if (self.originalImage.size.width>self.view.frame.size.width)
    {
        //DebugLog(@"calling photo resize method");
        // call for resize image to fit on the screen
        RetImage=[self imageWithImage:self.originalImage scaledToSize:PicSize];
        //DebugLog(@"After image frame set %f %f",RetImage.size.width,RetImage.size.height);
        [self.showImgView setImage:RetImage];
        IsResized=YES;
    }
    else
    {
        [self.showImgView setImage:self.originalImage];
    }
    [self.showImgView setFrame:CGRectMake((self.view.frame.size.width-self.showImgView.image.size.width)/2, (self.view.frame.size.height-self.showImgView.image.size.height)/2, self.showImgView.image.size.width,self.showImgView.image.size.height)];
//    if (self.originalImage.size.width>self.view.frame.size.width)
//    {
//        x=0;
//        if (self.originalImage.size.height>self.view.frame.size.height)
//        {
//            y=0;
//            self.showImgView.frame=CGRectMake(0, y, self.view.frame.size.width, self.view.frame.size.height);
//        }
//        else
//        {
//            y=(self.view.frame.size.height-self.originalImage.size.height)/2;
//            self.showImgView.frame=CGRectMake(0, y, self.view.frame.size.width, self.originalImage.size.height);
//        }
//    }
//    else
//    {
//        x= (self.view.frame.size.width-self.originalImage.size.width)/2;
//        if (self.originalImage.size.height>self.view.frame.size.height) {
//            y=0;
//            self.showImgView.frame=CGRectMake(x, y, self.originalImage.size.width, self.view.frame.size.height);
//        }
//        else
//        {
//            y=(self.view.frame.size.height-self.originalImage.size.height)/2;
//            self.showImgView.frame=CGRectMake(x, y, self.originalImage.size.width, self.originalImage.size.height);
//        }
//        
//    }

    
    
    [self.showImgView setMultipleTouchEnabled:YES];
    [self.showImgView setUserInteractionEnabled:YES];
    [self.showImgView setImage:self.originalImage];
    self.showImgView.contentMode=UIViewContentModeScaleAspectFit;
//    [self.showImgView setUserInteractionEnabled:YES];
//    [self.showImgView setMultipleTouchEnabled:YES];
    
    //DebugLog(@"%@",NSStringFromCGRect(self.showImgView.frame));
//    CGFloat imgHeight=self.showImgView.image.size.height;
//    imgHeight=imgHeight*(self.view.frame.size.width/self.showImgView.image.size.width);
//    CGSize picSize=CGSizeMake(self.view.frame.size.width, imgHeight);
//    DebugLog(@"Size    %f   %f",self.showImgView.image.size.width,self.showImgView.image.size.height);
//    
//        DebugLog(@"calling photo resize method");
        // call for resize image to fit on the screen
      //  UIImage *RetImage=[self imageWithImage:self.originalImage scaledToSize:picSize];
       // DebugLog(@"After image frame set %f %f",RetImage.size.width,RetImage.size.height);
    
    //self.showImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 150, 320, RetImage.size.height)];
    //[self.showImgView setImage:RetImage];
    [self.view addSubview:self.showImgView];
    // scale to fit the screen
    CGFloat oriWidth = self.cropFrame.size.width;
    CGFloat oriHeight = self.originalImage.size.height * (oriWidth / self.originalImage.size.width);
    CGFloat oriX = self.cropFrame.origin.x + (self.cropFrame.size.width - oriWidth) / 2;
    CGFloat oriY = self.cropFrame.origin.y + (self.cropFrame.size.height - oriHeight) / 2;
    self.oldFrame = CGRectMake(oriX, oriY, oriWidth, oriHeight);
    self.latestFrame = self.oldFrame;
    //self.showImgView.frame = self.oldFrame;
    
    //self.largeFrame = CGRectMake(0, 0, self.limitRatio * self.oldFrame.size.width, self.limitRatio * self.oldFrame.size.height);
    
    
    
    
    self.overlayView = [[UIView alloc] initWithFrame:self.view.bounds];
    self.overlayView.alpha = .5f;
    self.overlayView.backgroundColor = [UIColor blackColor];
    self.overlayView.userInteractionEnabled = YES;
    self.overlayView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:self.overlayView];
    
    self.ratioView = [[UIView alloc] init];//WithFrame:self.cropFrame];
    if (!IsResized)
    {
        //if (self.originalImage.size.height>=self.cropFrame.size.height)
        //{
        self.ratioView.frame=CGRectMake(self.cropFrame.origin.x, self.cropFrame.origin.y, self.cropFrame.size.width, self.cropFrame.size.width);
       // DebugLog(@"ratio view frame: %@",NSStringFromCGRect(self.ratioView.frame));
        //}
        //else
        //{
        //    if (self.originalImage.size.height>self.cropFrame.size.width)
        //    {
        //        self.ratioView.frame=CGRectMake(self.cropFrame.origin.x, self.cropFrame.origin.y, self.cropFrame.size.width, self.cropFrame.size.width);
        //    }
        //    else
        //        self.ratioView.frame=CGRectMake(self.cropFrame.origin.x, self.cropFrame.origin.y, self.originalImage.size.height, self.originalImage.size.height);
        //}
    }
    else
    {
        self.ratioView.frame=CGRectMake((self.view.frame.size.width-RetImage.size.height)/2, (self.view.frame.size.height-RetImage.size.height)/2, RetImage.size.height, RetImage.size.height);
    }
//    else if (self.originalImage.size.width<self.cropFrame.size.width)
//    {
//        DebugLog(@"----------------------");
//        if (self.originalImage.size.height>=self.originalImage.size.height)
//        {
//            
//            self.ratioView.frame=CGRectMake(self.showImgView.frame.origin.x,self.showImgView.frame.origin.y,self.originalImage.size.width,self.originalImage.size.width);
//        }
//        else
//            self.ratioView.frame=CGRectMake(self.showImgView.frame.origin.x,self.showImgView.frame.origin.y,self.originalImage.size.height,self.originalImage.size.height);
//    }
//    self.ratioView.layer.borderColor = [UIColor whiteColor].CGColor;
//    self.ratioView.layer.borderWidth = 1.0f;
//    //self.ratioView.autoresizingMask = UIViewAutoresizingNone;
//    [self.view addSubview:self.ratioView];
//    
//    [self overlayClipping];

    self.ratioView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.ratioView.layer.borderWidth = 1.0f;
    [self.ratioView setMultipleTouchEnabled:YES];
    [self.ratioView setUserInteractionEnabled:YES];
    

   // DebugLog(@"frame of image first highlighted : %@",NSStringFromCGRect(self.ratioView.frame));
    //self.ratioView.autoresizingMask = UIViewAutoresizingNone;
    [self.view addSubview:self.ratioView];
    UIPinchGestureRecognizer *pinchGestureRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchGestureDetect:)];
    [self.ratioView addGestureRecognizer:pinchGestureRecognizer];
    
    // add pan gesture
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panDetect:)];
    [self.ratioView addGestureRecognizer:panGestureRecognizer];

    //[self addGestureRecognizers];
    [self overlayClipping];
}
-(UIImage *)CropImage
{
   // DebugLog(@"Ix:%f Iy:%f Iw:%f Ih:%f",self.showImgView.frame.origin.x,self.showImgView.frame.origin.y,self.showImgView.image.size.width,self.showImgView.image.size.height);
    CGRect NewRect;
    NewRect=self.ratioView.frame;
    if (IsPinchExecuted&&!IsPanExecuted)
    {
        DebugLog(@"1");
        NewRect.origin.x=self.ratioView.frame.origin.x-self.showImgView.frame.origin.x;
        NewRect.origin.y=self.ratioView.frame.origin.y-self.showImgView.frame.origin.y;
        NewRect.size.height=NewHeight;
        NewRect.size.width=NewWidth;
    }
    else if(IsPanExecuted&&!IsPinchExecuted)
    {
        DebugLog(@"2");
        NewRect.origin.x=self.ratioView.frame.origin.x-self.showImgView.frame.origin.x;
        NewRect.origin.y=self.ratioView.frame.origin.y-self.showImgView.frame.origin.y;
        NewRect.size.height=self.ratioView.frame.size.height;
        NewRect.size.width=self.ratioView.frame.size.width;
    }
    else if (IsPanExecuted && IsPinchExecuted)
    {
        DebugLog(@"3");
        NewRect.origin.x=self.ratioView.frame.origin.x-self.showImgView.frame.origin.x;
        NewRect.origin.y=self.ratioView.frame.origin.y-self.showImgView.frame.origin.y;
        NewRect.size.height=NewHeight;
        NewRect.size.width=NewWidth;
    }
    else
    {
        DebugLog(@"4");
        NewRect.origin.x=self.ratioView.frame.origin.x-self.showImgView.frame.origin.x;
        NewRect.origin.y=self.ratioView.frame.origin.y-self.showImgView.frame.origin.y;
        NewRect.size.height=self.ratioView.frame.size.height;
        NewRect.size.width=self.ratioView.frame.size.width;
    }
    
   // DebugLog(@"Selected frame Before Clipping    X=%f    Y=%f   W=%f    H=%f",NewRect.origin.x,NewRect.origin.y,NewRect.size.width,NewRect.size.height);
    //DebugLog(@"image frame height:%f  width:%f",RetImage.size.height,RetImage.size.width);
    CGImageRef imageRef;
    if (self.originalImage.size.width>self.view.frame.size.width)
    {
        imageRef = CGImageCreateWithImageInRect([RetImage CGImage], NewRect);
    }
    else
    {
        imageRef = CGImageCreateWithImageInRect([self.showImgView.image CGImage], NewRect);
    }
    UIImage *newImage   = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    return newImage;
    
}
- (void)panDetect:(UIPanGestureRecognizer *)recognizer
{
    
    UIGestureRecognizerState state = [recognizer state];
    if (state==UIGestureRecognizerStateBegan)
    {
        startpointX=self.ratioView.frame.origin.x;
        startpointY=self.ratioView.frame.origin.y;
        IsPanExecuted=YES;
        DebugLog(@"Start Posiion:::: X=%f    Y=%f",startpointX,startpointY);
        [self overlayClipping];
        
    }
    
    if (state == UIGestureRecognizerStateChanged)
    {
        CGPoint translation = [recognizer translationInView:self.view];
        NewXPosition=translation.x+startpointX;
        NewYPosition=translation.y+startpointY;
        IsPanExecuted=YES;
        DebugLog(@"X Position: %f Y Position: %f",NewXPosition,NewYPosition);
        //        if (NewXPosition>=0 && (NewXPosition+faceView.frame.size.width)<imageV.frame.size.width && NewYPosition>=0 && (NewYPosition+faceView.frame.size.height)<imageV.frame.size.height)
        //        {
        [self.ratioView setFrame:CGRectMake(NewXPosition, NewYPosition, self.ratioView.frame.size.width, self.ratioView.frame.size.height)];
        [self overlayClipping];
        //        }
        
    }
}
-(void)pinchGestureDetect:(UIPinchGestureRecognizer *)recognizer
{
    UIGestureRecognizerState state=[recognizer state];
    if (state==UIGestureRecognizerStateChanged)
    {
        
        recognizer.view.transform=CGAffineTransformScale(recognizer.view.transform, recognizer.scale,recognizer.scale);
        recognizer.scale=1;
        IsPinchExecuted=YES;
        NewHeight=recognizer.view.frame.size.height;
        NewWidth=recognizer.view.frame.size.width;
        [self overlayClipping];
        DebugLog(@"New Height: %f New Width: %f",NewHeight,NewWidth);
    }
}

- (void)initControlBtn {
    
    UIView *blacktopview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 62)];
    [self.view addSubview:blacktopview];
    blacktopview.backgroundColor=[UIColor blackColor];
    
    UIButton *cancelBtn = [[UIButton alloc] initWithFrame:CGRectMake(5, 14.0f, 100, 48)];
    cancelBtn.backgroundColor = [UIColor blackColor];
    cancelBtn.titleLabel.textColor = [UIColor whiteColor];
    [cancelBtn setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelBtn.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Bold" size:18.0f]];
    [cancelBtn.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [cancelBtn.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [cancelBtn.titleLabel setNumberOfLines:0];
    [cancelBtn setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 5.0f, 5.0f, 5.0f)];
    [cancelBtn addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cancelBtn];
    
    UIButton *confirmBtn = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 102.0f, 14.0f, 100, 48)];
    confirmBtn.backgroundColor = [UIColor blackColor];
    confirmBtn.titleLabel.textColor = [UIColor whiteColor];
    [confirmBtn setTitle:@"Upload" forState:UIControlStateNormal];
    [confirmBtn.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Bold" size:18.0f]];
    [confirmBtn.titleLabel setTextAlignment:NSTextAlignmentCenter];
    confirmBtn.titleLabel.textColor = [UIColor whiteColor];
    [confirmBtn.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [confirmBtn.titleLabel setNumberOfLines:0];
    [confirmBtn setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 5.0f, 5.0f, 5.0f)];
    [confirmBtn addTarget:self action:@selector(confirm:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:confirmBtn];
    
    UILabel *blackbottomview = [[UILabel alloc]initWithFrame:CGRectMake(0, self.view.bounds.size.height-60, [UIScreen mainScreen].bounds.size.width, 60)];
    [self.view addSubview:blackbottomview];
    blackbottomview.backgroundColor=[UIColor blackColor];
    blackbottomview.textColor=[UIColor whiteColor];
    blackbottomview.font=[UIFont fontWithName:@"ProximaNova-Regular" size:19];
    blackbottomview.text=@"Adjust size and position";
    blackbottomview.textAlignment=NSTextAlignmentCenter;
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


- (void)cancel:(id)sender {
    if (self.delegate && [self.delegate conformsToProtocol:@protocol(ImageCropperDelegate)]) {
        [self.delegate imageCropperDidCancel:self];
    }
}

- (void)confirm:(id)sender {
    if (self.delegate && [self.delegate conformsToProtocol:@protocol(ImageCropperDelegate)])
    {
        [self.delegate imageCropper:self didFinished:[self CropImage]];
    }
}

- (void)overlayClipping
{
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    CGMutablePathRef path = CGPathCreateMutable();
    // Left side of the ratio view
    CGPathAddRect(path, nil, CGRectMake(0, 0,
                                        self.ratioView.frame.origin.x,
                                        self.overlayView.frame.size.height));
    // Right side of the ratio view
    CGPathAddRect(path, nil, CGRectMake(
                                        self.ratioView.frame.origin.x + self.ratioView.frame.size.width,
                                        0,
                                        self.overlayView.frame.size.width - self.ratioView.frame.origin.x - self.ratioView.frame.size.width,
                                        self.overlayView.frame.size.height));
    // Top side of the ratio view
    CGPathAddRect(path, nil, CGRectMake(0, 0,
                                        self.overlayView.frame.size.width,
                                        self.ratioView.frame.origin.y));
    // Bottom side of the ratio view
    CGPathAddRect(path, nil, CGRectMake(0,
                                        self.ratioView.frame.origin.y + self.ratioView.frame.size.height,
                                        self.overlayView.frame.size.width,
                                        self.overlayView.frame.size.height - (self.ratioView.frame.origin.y + self.ratioView.frame.size.height)));
    maskLayer.path = path;
    self.overlayView.layer.mask = maskLayer;
    CGPathRelease(path);
}
/*
// register all gestures
- (void) addGestureRecognizers
{
    // add pinch gesture
    UIPinchGestureRecognizer *pinchGestureRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchView:)];
    [self.view addGestureRecognizer:pinchGestureRecognizer];
    
    // add pan gesture
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panView:)];
    [self.view addGestureRecognizer:panGestureRecognizer];
}

// pinch gesture handler
- (void) pinchView:(UIPinchGestureRecognizer *)pinchGestureRecognizer
{
    UIView *view = self.ratioView;
    if (pinchGestureRecognizer.state == UIGestureRecognizerStateBegan || pinchGestureRecognizer.state == UIGestureRecognizerStateChanged) {
        [self overlayClipping];
        view.transform = CGAffineTransformScale(view.transform, pinchGestureRecognizer.scale, pinchGestureRecognizer.scale);
        pinchGestureRecognizer.scale = 1;
    }
    else if (pinchGestureRecognizer.state == UIGestureRecognizerStateEnded) {
        [self overlayClipping];
        CGRect newFrame = self.ratioView.frame;
        //newFrame = [self handleScaleOverflow:newFrame];
       // newFrame = [self handleBorderOverflow:newFrame];
        [UIView animateWithDuration:BOUNDCE_DURATION animations:^{
            self.ratioView.frame = newFrame;
            self.latestFrame = newFrame;
        }];
    }
    DebugLog(@"frame of selected part1 : %@",NSStringFromCGRect(self.ratioView.frame));
}

// pan gesture handler
- (void) panView:(UIPanGestureRecognizer *)panGestureRecognizer
{
    UIView *view = self.ratioView;
    if (panGestureRecognizer.state == UIGestureRecognizerStateBegan || panGestureRecognizer.state == UIGestureRecognizerStateChanged) {
        [self overlayClipping];
        // calculate accelerator
        CGFloat absCenterX = self.ratioView.frame.origin.x+self.ratioView.frame.size.width / 2;
        CGFloat absCenterY = self.ratioView.frame.origin.y+self.ratioView.frame.size.height / 2;
        
        
//        CGFloat absCenterX = self.cropFrame.origin.x + self.cropFrame.size.width / 2;
//        CGFloat absCenterY = self.cropFrame.origin.y + self.cropFrame.size.height / 2;
        CGFloat scaleRatio = self.ratioView.frame.size.width / self.originalImage.size.width;
        CGFloat acceleratorX = 1 - ABS(absCenterX - view.center.x) / (scaleRatio * absCenterX);
        CGFloat acceleratorY = 1 - ABS(absCenterY - view.center.y) / (scaleRatio * absCenterY);
        CGPoint translation = [panGestureRecognizer translationInView:view.superview];
        [view setCenter:(CGPoint){view.center.x + translation.x * acceleratorX, view.center.y + translation.y * acceleratorY}];
        [panGestureRecognizer setTranslation:CGPointZero inView:view.superview];
    }
    else if (panGestureRecognizer.state == UIGestureRecognizerStateEnded) {
        // bounce to original frame
        [self overlayClipping];
        CGRect newFrame = self.ratioView.frame;
        //newFrame = [self handleBorderOverflow:newFrame];
        [UIView animateWithDuration:BOUNDCE_DURATION animations:^{
            self.ratioView.frame = newFrame;
            self.latestFrame = newFrame;
        }];
    }
    DebugLog(@"frame of selected part2 : %@",NSStringFromCGRect(self.ratioView.frame));
}*/

- (CGRect)handleScaleOverflow:(CGRect)newFrame {
    // bounce to original frame
    CGPoint oriCenter = CGPointMake(newFrame.origin.x + newFrame.size.width/2, newFrame.origin.y + newFrame.size.height/2);
    if (newFrame.size.width < self.oldFrame.size.width) {
        newFrame = self.oldFrame;
    }
    if (newFrame.size.width > self.largeFrame.size.width) {
        newFrame = self.largeFrame;
    }
    newFrame.origin.x = oriCenter.x - newFrame.size.width/2;
    newFrame.origin.y = oriCenter.y - newFrame.size.height/2;
    return newFrame;
}

- (CGRect)handleBorderOverflow:(CGRect)newFrame {
    // horizontally
    if (newFrame.origin.x > self.cropFrame.origin.x) newFrame.origin.x = self.cropFrame.origin.x;
    if (CGRectGetMaxX(newFrame) < self.cropFrame.origin.x + self.cropFrame.size.width) newFrame.origin.x = self.cropFrame.origin.x + self.cropFrame.size.width - newFrame.size.width;
    // vertically
    if (newFrame.origin.y > self.cropFrame.origin.y) newFrame.origin.y = self.cropFrame.origin.y;
    if (CGRectGetMaxY(newFrame) < self.cropFrame.origin.y + self.cropFrame.size.height) {
        newFrame.origin.y = self.cropFrame.origin.y + self.cropFrame.size.height - newFrame.size.height;
    }
    // adapt horizontally rectangle
    if (self.showImgView.frame.size.width > self.showImgView.frame.size.height && newFrame.size.height <= self.cropFrame.size.height) {
        newFrame.origin.y = self.cropFrame.origin.y + (self.cropFrame.size.height - newFrame.size.height) / 2;
    }
    return newFrame;
}

/*-(UIImage *)getSubImage{
    
    CGRect squareFrame = self.ratioView.frame;
    
    DebugLog(@"original frame of pic : %f %f",self.originalImage.size.width,self.originalImage.size.height);
    DebugLog(@"frame of selected part : %@",NSStringFromCGRect(self.ratioView.frame));
//    CGFloat scaleRatio = self.latestFrame.size.width / self.originalImage.size.width;
//    CGFloat x = (squareFrame.origin.x - self.latestFrame.origin.x) / scaleRatio;
//    CGFloat y = (squareFrame.origin.y - self.latestFrame.origin.y) / scaleRatio;
//    CGFloat x = (squareFrame.origin.x - self.latestFrame.origin.x) / scaleRatio;
//    CGFloat y = (squareFrame.origin.y - self.latestFrame.origin.y) / scaleRatio;
//    CGFloat w = squareFrame.size.width / scaleRatio;
//    CGFloat h = squareFrame.size.height / scaleRatio;
//    if (self.latestFrame.size.width > self.originalImage.size.width) {
//        CGFloat newW = self.originalImage.size.width;
////        CGFloat newH = newW * (self.cropFrame.size.height / self.cropFrame.size.width);
////        x = 0; y = y + (h - newH) / 2;
//        w = newW;// h = newH;
//    }
//    if (self.latestFrame.size.height > self.originalImage.size.height) {
//        CGFloat newH = self.originalImage.size.height;
////        CGFloat newW = newH * (self.cropFrame.size.width / self.cropFrame.size.height);
////        x = x + (w - newW) / 2; y = 0;
//       // w = newH;
//        h = newH;
//    }
//
    CGRect myImageRect = CGRectMake(squareFrame.origin.x-10, squareFrame.origin.y-8, squareFrame.size.width, squareFrame.size.height);
    //CGRect myImageRect = CGRectMake(x, y, w, h);
    DebugLog(@"frame : %@",NSStringFromCGRect(squareFrame));

    CGImageRef imageRef = self.self.showImgView.image.CGImage;
    CGImageRef subImageRef = CGImageCreateWithImageInRect(imageRef, myImageRect);
    CGSize size;
    size.width = myImageRect.size.width;
    size.height = myImageRect.size.height;
    UIGraphicsBeginImageContext(size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextDrawImage(context, myImageRect, subImageRef);
    UIImage* smallImage = [UIImage imageWithCGImage:subImageRef];
    CGImageRelease(subImageRef);
    UIGraphicsEndImageContext();
     DebugLog(@"frame1 : %f %f",smallImage.size.width,smallImage.size.height);
    return smallImage;
    
//    CGRect squareFrame = self.cropFrame;
//    CGFloat scaleRatio = self.latestFrame.size.width / self.originalImage.size.width;
//    CGFloat x = (squareFrame.origin.x - self.latestFrame.origin.x) / scaleRatio;
//    CGFloat y = (squareFrame.origin.y - self.latestFrame.origin.y) / scaleRatio;
//    CGFloat w = squareFrame.size.width / scaleRatio;
//    CGFloat h = squareFrame.size.height / scaleRatio;
//    if (self.latestFrame.size.width < self.cropFrame.size.width) {
//        CGFloat newW = self.originalImage.size.width;
//        CGFloat newH = newW * (self.cropFrame.size.height / self.cropFrame.size.width);
//        x = 0; y = y + (h - newH) / 2;
//        w = newH; h = newH;
//    }
//    if (self.latestFrame.size.height < self.cropFrame.size.height) {
//        CGFloat newH = self.originalImage.size.height;
//        CGFloat newW = newH * (self.cropFrame.size.width / self.cropFrame.size.height);
//        x = x + (w - newW) / 2; y = 0;
//        w = newH; h = newH;
//    }
//    CGRect myImageRect = CGRectMake(x, y, w, h);
//    CGImageRef imageRef = self.originalImage.CGImage;
//    CGImageRef subImageRef = CGImageCreateWithImageInRect(imageRef, myImageRect);
//    CGSize size;
//    size.width = myImageRect.size.width;
//    size.height = myImageRect.size.height;
//    UIGraphicsBeginImageContext(size);
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGContextDrawImage(context, myImageRect, subImageRef);
//    UIImage* smallImage = [UIImage imageWithCGImage:subImageRef];
//    CGImageRelease(subImageRef);
//    UIGraphicsEndImageContext();
//    return smallImage;
}*/

- (UIImage *)fixOrientation:(UIImage *)srcImg {
    if (srcImg.imageOrientation == UIImageOrientationUp) return srcImg;
    CGAffineTransform transform = CGAffineTransformIdentity;
    switch (srcImg.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, srcImg.size.width, srcImg.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, srcImg.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, srcImg.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (srcImg.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, srcImg.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, srcImg.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    CGContextRef ctx = CGBitmapContextCreate(NULL, srcImg.size.width, srcImg.size.height,
                                             CGImageGetBitsPerComponent(srcImg.CGImage), 0,
                                             CGImageGetColorSpace(srcImg.CGImage),
                                             CGImageGetBitmapInfo(srcImg.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (srcImg.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            CGContextDrawImage(ctx, CGRectMake(0,0,srcImg.size.height,srcImg.size.width), srcImg.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,srcImg.size.width,srcImg.size.height), srcImg.CGImage);
            break;
    }
    
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
