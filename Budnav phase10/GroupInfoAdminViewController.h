//
//  GroupInfoAdminViewController.h
//  Budnav
//
//  Created by Barsan on 17/09/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface GroupInfoAdminViewController : GAITrackedViewController

@property(strong,nonatomic) NSMutableDictionary *mydic;
@property(strong,nonatomic) NSDictionary *grpdict;
@property(strong,nonatomic) NSData *mydata2;
@property (assign, nonatomic) BOOL group_update;
@property (strong, nonatomic) UILabel *groupnamelbl;

//@property(nonatomic,strong)UIButton *menu,*invite;
//@property(nonatomic,retain) NSMutableArray *filtered_arr,*search_result,*group_array,*searchresults;
//@property(nonatomic,strong)UITableView *groupstable,*menutable,*searchtablefront,*searchtableback;
//@property(nonatomic,strong)UILabel *group_namelbl;
//@property(nonatomic,strong)UIImageView *groupimage;

@end
