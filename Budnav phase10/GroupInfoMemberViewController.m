//
//  GroupInfoMemberViewController.m
//  Budnav
//
//  Created by ios on 15/09/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "GroupInfoMemberViewController.h"
#import "GroupDetailsViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

@interface GroupInfoMemberViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UIView *backview,*dividerview1,*dividerview2,*dividerview3,*dividerview4,*dividerview5,*grayview1,*grayview2,*popupview;
    UIImageView *logo_img,*bck_img,*groupimg,*moreImage;
    UIButton *backbtn,*leavegrpbtn,*menu;
    UILabel *groupnamelbl,*membernolbl,*lbl1,*lbl2;
    UISwitch *onoff;
    UIScrollView *mainscroll;
    UIActivityIndicatorView *act,*activity;
     NSMutableDictionary *groupdict;
    NSDictionary *groupdictionary;
    BOOL expand;
    UITableView *menutable;
    UISwitch *mySwitch, *mySwitch2;
    int checklbl,myswitchcheck1,myswitchcheck2;
    id switchcheck1,switchckeck2;
    
}
@end

@implementation GroupInfoMemberViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    checklbl=0;
    myswitchcheck1=0;
    myswitchcheck2=0;
    switchcheck1=@"false";
    switchckeck2=@"false";
    
    NSLog(@"Dictionary.....%@",self.mydic);
    
    if (![_mydic isKindOfClass:[NSNull class]] && _mydic!=nil && [_mydic count]>0)
    {
        [[NSUserDefaults standardUserDefaults] setObject:[self.mydic objectForKey:@"id"] forKey:@"group_id"];
    }
    
    
    backview=[[UIView alloc]initWithFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    [backview setBackgroundColor:[UIColor whiteColor]];
    //[backview setBackgroundColor:[UIColor colorWithRed:(237.0f/255.0f) green:(237.0f/255.0f) blue:(237.0f/255.0f) alpha:1.0f]];
    [self.view addSubview:backview];
    
    UIView *coverview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64)];
    [backview addSubview:coverview];
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, coverview.frame.size.height);
    
    // Add the gradient to the view
    [coverview.layer insertSublayer:gradient atIndex:0];
    
    bck_img = [[UIImageView alloc]initWithFrame:CGRectMake(15,32,12,20)];//(13, 39, 10, 22)];
    bck_img.image=[UIImage imageNamed:@"back3"];
    [self.view addSubview:bck_img];
    
    logo_img = [[UIImageView alloc]init];
    logo_img.frame = CGRectMake(27+([UIScreen mainScreen].bounds.size.width-160)/2, 32, 101, 20);
    logo_img.backgroundColor = [UIColor clearColor];
    
    logo_img.image = [UIImage imageNamed:@"new_logo"];
    [coverview addSubview:logo_img];
    
    backbtn=[[UIButton alloc]initWithFrame:CGRectMake(0.0f,0.0f,74,64)];
    [backbtn setBackgroundColor:[UIColor clearColor]];
    [coverview addSubview:backbtn];
    
    [backbtn addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDown];
    [backbtn addTarget:self action:@selector(changecoloragain) forControlEvents:UIControlEventTouchDragExit];
    [backbtn addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDragEnter];
    [backbtn addTarget:self action:@selector(backtoprev:) forControlEvents:UIControlEventTouchUpInside];
    
    
    mainscroll=[[UIScrollView alloc]initWithFrame:CGRectMake(0.0f,64.0f,self.view.frame.size.width,self.view.frame.size.height)];
    //mainscroll.backgroundColor=[UIColor lightGrayColor];
    [backview addSubview:mainscroll];
    
    [mainscroll setContentSize:CGSizeMake(self.view.frame.size.width,590.0f)];
    mainscroll.showsVerticalScrollIndicator=NO;
    
    
//    moreImage=[[UIImageView alloc]initWithFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-37, 32, 22,19)];
//    
//    
//    [moreImage setImage:[UIImage imageNamed:@"new_menu"]];
//    [coverview addSubview:moreImage];
//    
//    menu=[[UIButton alloc]initWithFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-55,[[UIApplication sharedApplication]statusBarFrame].size.height, 55,coverview.frame.size.height-[[UIApplication sharedApplication]statusBarFrame].size.height)];//CGRectMake(290, 32, 22, 28)];
//    [menu setBackgroundImage:nil forState:UIControlStateDisabled];
//    [menu setBackgroundImage:nil forState:UIControlStateSelected];
//    [menu setBackgroundImage:nil forState:UIControlStateHighlighted];
//    [menu setUserInteractionEnabled:YES];
//    //[menu setBackgroundColor:[UIColor grayColor]];
//    [menu addTarget:self action:@selector(gomenu) forControlEvents:UIControlEventTouchUpInside];
//    menu.contentMode=UIViewContentModeScaleAspectFill;
//    expand=NO;
//    // menu.enabled=NO;
//    [coverview addSubview:menu];
}

//-(void)gomenu
//{
//    NSLog(@"123456789");
//    
//    if (!expand)
//    {
//        
//        popupview=[[UIView alloc]initWithFrame:CGRectMake(0,64, self.view.frame.size.width,100)];
//        popupview.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.9];
//        popupview.opaque=NO;
//        //popupview.layer.cornerRadius=5;
//        //            popupview.layer.shadowOpacity=0.8;
//        //            popupview.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
//        [self.view addSubview:popupview];
//        
//        menutable=[[UITableView alloc]initWithFrame:CGRectMake(0, self.view.frame.origin.y, popupview.frame.size.width, popupview.frame.size.height)];
//        menutable.delegate=self;
//        menutable.tag=2;
//        menutable.dataSource=self;
//        [menutable setBackgroundColor:[UIColor clearColor]];
//        //menutable.separatorInset=UIEdgeInsetsMake(<#CGFloat top#>, <#CGFloat left#>, <#CGFloat bottom#>, <#CGFloat right#>)
//        //menutable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
//        menutable.separatorStyle=UITableViewCellSeparatorStyleNone;
//        menutable.scrollEnabled=NO;
//        
//        menutable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
//        [popupview addSubview:menutable];
//        [menutable reloadData];
//        
//        expand=YES;
//        //backbutton.tag=1;
//        
//    }
//    else if (expand)
//    {
//        popupview.hidden=YES;
//        menu.tag=9;
//        // backbutton.tag=0;
//        expand=NO;
//    }
//    
//    
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    static NSString *MyIdentifier = @"MyReuseIdentifier";
//    
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
//    cell=nil;
//    
//    if (cell == nil)
//    {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:MyIdentifier];
//    }
//    
//    if (tableView.tag==2)
//    {
//        cell.backgroundColor=[UIColor clearColor];
//        cell.selectionStyle=UITableViewCellSelectionStyleNone;
//        
//        //            cell.imageView.image=[UIImage imageNamed:[array_imagename objectAtIndex:indexPath.row]];
//        //            //cell.imageView.contentMode=UIViewContentModeCenter;
//        //            cell.textLabel.textColor=[UIColor whiteColor];
//        //            cell.textLabel.text=[arraymenu_admin objectAtIndex:indexPath.row];
//        //            cell.textLabel.textAlignment=NSTextAlignmentCenter;
//        //            cell.textLabel.font=[UIFont fontWithName:@"ProximaNova-Regular" size:14];
//        
//        UILabel *menulabel = [[UILabel alloc]initWithFrame:CGRectMake(0.0f,0.0f, menutable.frame.size.width, 49.0f)];
//        if(indexPath.row==0)
//        {
//            menulabel.text=@"Email Group";
//            UILabel *separatorlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 49.5f, [UIScreen mainScreen].bounds.size.width, 0.5f)];
//            separatorlabel.backgroundColor=[UIColor grayColor];
//            [cell addSubview:separatorlabel];
//        }
//        else
//        {
//            menulabel.text=@"SMS Group";
//        }
//        // [menulabel setText:[NSString stringWithFormat:@"%@",[arraymenu_admin objectAtIndex:indexPath.row]]];
//        [menulabel setTextAlignment:NSTextAlignmentCenter];
//        [menulabel setTextColor:[UIColor whiteColor]];
//        [menulabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:19]];
//        [cell addSubview:menulabel];
//        
//        //            UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(15.0f,15.0f,20.0f,20.0f)];
//        //            image.backgroundColor=[UIColor clearColor];
//        //         //   [image setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[arrayadmin_imagename objectAtIndex:indexPath.row]]]];
//        //            image.contentMode=UIViewContentModeScaleAspectFit;
//        //            [cell addSubview:image];
//        
//        //            UILabel *separatorlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 49.5f, [UIScreen mainScreen].bounds.size.width, 0.5f)];
//        //            separatorlabel.backgroundColor=[UIColor grayColor];
//        //            [cell addSubview:separatorlabel];
//        
//    }
//    
//    return cell;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section1
//{
//    if (tableView.tag==2)
//    {
//        
//        return 2;
//        
//    }
//    return 0;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (tableView.tag==2)
//    {
//        return 50.0f;
//    }
//    else
//        return 55.0f;
//}



-(void)viewDidAppear:(BOOL)animated
    {
        act = [[UIActivityIndicatorView alloc] init];
        act.center = self.view.center;
        act.hidesWhenStopped=YES;
        act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        [act setColor:[UIColor blackColor]];
        act.backgroundColor=[UIColor blueColor];
        [self.view addSubview:act];
        act.layer.zPosition=2;
        [NSThread detachNewThreadSelector: @selector(Start) toTarget:self withObject:nil];
        
        
        NSOperationQueue *queue = [NSOperationQueue new];
        NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                            initWithTarget:self
                                            selector:@selector(loaddata)
                                            object:nil];
        [queue addOperation:operation];
    
    
    // Do any additional setup after loading the view.
}

- (void)loaddata
{
    
    
    groupdict=[[NSMutableDictionary alloc]init];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *groups_url =[NSString stringWithFormat:@"https://budnav.com/ext/?action=group-get&group_id=%@&image=%d&access_token=%@&device_id=%@",[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"group_id"]],true,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
    DebugLog(@"groups url string ------ %@",groups_url);
    
    NSError *error1=nil;
    @try {
        NSData *data1=[NSData dataWithContentsOfURL:[NSURL URLWithString:groups_url]options:NSDataReadingUncached error:&error1];
        if (data1==nil)
        {
            DebugLog(@"no connnnn");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                                            message:nil
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            alert.tag=5;
            [alert show];
            
            
            
        }
        
        if (data1 != nil)
            groupdict=[NSJSONSerialization JSONObjectWithData:data1 //1
                                                      options:kNilOptions
                                                        error:&error1];
        DebugLog(@"-----Groups info ----- %@",groupdict);
        
        [_mydic removeAllObjects];
        _mydic = [[groupdict objectForKey:@"details"] mutableCopy];
        
        if ([[[groupdict objectForKey:@"details"] objectForKey:@"type"] intValue]==2)
        {
            
            self.screenName = @"Business group info";
            
            [[GAI sharedInstance].defaultTracker send:
             
             [[GAIDictionaryBuilder createEventWithCategory:@"Business group"
               
                                                     action:@"Business group info"
               
                                                      label:nil
               
                                                      value:nil] build]];
        }
        
        else if ([[[groupdict objectForKey:@"details"] objectForKey:@"type"] intValue]==1)
        {
            
            self.screenName = @"Personal group info";
            
            [[GAI sharedInstance].defaultTracker send:
             
             [[GAIDictionaryBuilder createEventWithCategory:@"Personal group"
               
                                                     action:@"Personal group info"
               
                                                      label:nil
               
                                                      value:nil] build]];
            
        }
    }
    @catch (NSException *exception) {
        DebugLog(@"exception %@",exception);
    }
    [self performSelectorOnMainThread:@selector(datadisplay) withObject:nil waitUntilDone:NO];
}

-(void)datadisplay
{
    [act stopAnimating];
    
    DebugLog(@"Group DETAILS FROM URL:%@",_mydic);
    
    UIImageView  *img =[[UIImageView alloc] initWithFrame:CGRectMake(85.0f,25.0f,self.view.frame.size.width-170,self.view.frame.size.width-170)];
    
        // [img sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[groupsarray objectAtIndex:indexPath.row]objectForKey:@"thumb"]]] placeholderImage:[UIImage imageNamed:@"placeholder"] options:/* DISABLES CODE */ (0) == 0?SDWebImageRefreshCached : 0];
        //img.image=[UIImage imageNamed:@"Backup profile pic"];
    NSString *base64string = [NSString stringWithFormat:@"%@", [self.mydic objectForKey:@"image"] ];
    
    if (base64string>0)
    {
        NSData *imageData = [[NSData alloc] initWithBase64EncodedString:base64string options:0];
        img.image = [UIImage imageWithData:imageData];
    }
    
//        if(self.mydata2.length>0)
//        {
//            img.image = [UIImage imageWithData:[NSData dataWithData:self.mydata2]];
//        }
        else
        {
              img.image=[UIImage imageNamed:@"placeholderimg"];
        }
    
        img.layer.cornerRadius = img.frame.size.width/2;
        img.clipsToBounds=YES;
    
        [mainscroll addSubview:img];
    
    
        groupnamelbl = [[UILabel alloc]initWithFrame:CGRectMake(50.0f,img.frame.origin.y+img.frame.size.height+23,self.view.frame.size.width-100,20)];
       // groupnamelbl.text=[self.mydic objectForKey:@"name"];
        [groupnamelbl setTextAlignment:NSTextAlignmentCenter];
        groupnamelbl.textColor=[UIColor blackColor];
        [groupnamelbl setFont:[UIFont fontWithName:@"ProximaNova-Bold" size:17]];
    
        if ((NSNull *)[self.mydic objectForKey:@"name"] == [NSNull null])
        {
            groupnamelbl.text=@"";
    
        }
        else
        {
            groupnamelbl.text=[self.mydic objectForKey:@"name"];
        }
    
        [mainscroll addSubview:groupnamelbl];
    
        membernolbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0f,groupnamelbl.frame.origin.y+groupnamelbl.frame.size.height+13,self.view.frame.size.width-40,20)];
        [membernolbl setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:15]];
       // membernolbl.text=[NSString stringWithFormat:@"%@ Members - Personal Group",[self.mydic objectForKey:@"count"]];
        [membernolbl setTextAlignment:NSTextAlignmentCenter];
    
//        if ((NSNull *)[self.mydic objectForKey:@"count"] == [NSNull null])
//        {
//            membernolbl.text=@"";
//        }
    if ([[_mydic objectForKey:@"memberCount"] intValue] == 0)
    {
        membernolbl.text=@"";
    }
        else
        {
            if ([[_mydic objectForKey:@"memberCount"] intValue]>1)
            {
            membernolbl.text=[NSString stringWithFormat:@"%@ Members - Personal Group", [_mydic objectForKey:@"memberCount"]];
            }
            else
            {
                membernolbl.text=[NSString stringWithFormat:@"%@ Member - Personal Group", [_mydic objectForKey:@"memberCount"]];
            }
        }
    
        [membernolbl setTextColor:[UIColor colorWithRed:(171.0f/255.0f) green:(171.0f/255.0f) blue:(171.0f/255.0f) alpha:1.0f]];
        [mainscroll addSubview:membernolbl];
    
    
        dividerview1=[[UIView alloc]initWithFrame:CGRectMake(0.0f,membernolbl.frame.origin.y+membernolbl.frame.size.height+23,self.view.frame.size.width,0.5f)];
        [dividerview1 setBackgroundColor:[UIColor colorWithRed:(204.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f]];
        [mainscroll addSubview:dividerview1];
    
        lbl1=[[UILabel alloc]initWithFrame:CGRectMake(20.0f,dividerview1.frame.origin.y+dividerview1.frame.size.height+15,230,20)];
        lbl1.text=@"Show group in contact list";
        [lbl1 setTextAlignment:NSTextAlignmentLeft];
        [lbl1 setTextColor:[UIColor blackColor]];
        [lbl1 setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:18]];
        [mainscroll addSubview:lbl1];
    
    
    if (!mySwitch)
    {
       
        mySwitch = [[UISwitch alloc] initWithFrame:CGRectMake(self.view.frame.size.width-70,dividerview1.frame.origin.y+10, 0, 0)];
        mySwitch.onTintColor = [UIColor colorWithRed:(40.0/255.0) green:(169.0/255.0) blue:(184.0/255.0) alpha:1.0];
        [mySwitch addTarget:self action:@selector(changeSwitch1) forControlEvents:UIControlEventValueChanged];
        [mainscroll addSubview:mySwitch];
        
        if ([[_mydic objectForKey:@"showContacts"] intValue]==1)
        {
            [mySwitch setOn:YES animated:NO];
            switchcheck1=@"true";
            myswitchcheck1=1;
        }
        else
        {
            [mySwitch setOn:NO animated:NO];
            switchcheck1=@"false";
            myswitchcheck1=0;
        }
        
    }
    
        dividerview2=[[UIView alloc]initWithFrame:CGRectMake(0.0f,dividerview1.frame.origin.y+dividerview1.frame.size.height+50,self.view.frame.size.width,0.5f)];
        [dividerview2 setBackgroundColor:[UIColor colorWithRed:(204.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f]];
        [mainscroll addSubview:dividerview2];
    
        dividerview3=[[UIView alloc]initWithFrame:CGRectMake(0.0f,dividerview2.frame.origin.y+dividerview2.frame.size.height+50,self.view.frame.size.width,0.5f)];
        [dividerview3 setBackgroundColor:[UIColor colorWithRed:(204.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f]];
        [mainscroll addSubview:dividerview3];
    
        lbl2=[[UILabel alloc]initWithFrame:CGRectMake(20.0f,dividerview2.frame.origin.y+dividerview2.frame.size.height+15,230,20)];
        lbl2.text=@"Show group on map";
        [lbl2 setTextAlignment:NSTextAlignmentLeft];
        [lbl2 setTextColor:[UIColor blackColor]];
        [lbl2 setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:18]];
        [mainscroll addSubview:lbl2];
    
    if (!mySwitch2)
    {
    
        mySwitch2 = [[UISwitch alloc] initWithFrame:CGRectMake(self.view.frame.size.width-70,dividerview2.frame.origin.y+10, 0, 0)];
        mySwitch2.onTintColor = [UIColor colorWithRed:(40.0/255.0) green:(169.0/255.0) blue:(184.0/255.0) alpha:1.0];
        [mySwitch2 addTarget:self action:@selector(changeSwitch2) forControlEvents:UIControlEventValueChanged];
        [mainscroll addSubview:mySwitch2];
        
        if ([[_mydic objectForKey:@"showMap"] intValue]==1)
        {
            [mySwitch2 setOn:YES animated:NO];
            switchckeck2=@"true";
            myswitchcheck2=1;
        }
        else
        {
            [mySwitch2 setOn:NO animated:YES];
            switchckeck2=@"false";
            myswitchcheck2=0;
        }
        
    }
    
    [self urlFired];
    
        dividerview4=[[UIView alloc]initWithFrame:CGRectMake(0.0f,dividerview3.frame.origin.y+dividerview3.frame.size.height+35,self.view.frame.size.width,0.5f)];
        [dividerview4 setBackgroundColor:[UIColor colorWithRed:(204.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f]];
        [mainscroll addSubview:dividerview4];
    
        dividerview5=[[UIView alloc]initWithFrame:CGRectMake(0.0f,dividerview4.frame.origin.y+dividerview4.frame.size.height+50,self.view.frame.size.width,0.5f)];
        [dividerview5 setBackgroundColor:[UIColor colorWithRed:(204.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f]];
        [mainscroll addSubview:dividerview5];
    
    
        grayview1=[[UIView alloc]initWithFrame:CGRectMake(0.0,dividerview3.frame.origin.y+dividerview3.frame.size.height,self.view.frame.size.width,35)];
        [grayview1 setBackgroundColor:[UIColor colorWithRed:(237.0f/255.0f) green:(237.0f/255.0f) blue:(237.0f/255.0f) alpha:1.0f]];
        [mainscroll addSubview:grayview1];
    
    
        leavegrpbtn=[[UIButton alloc]initWithFrame:CGRectMake(50.0f,dividerview4.frame.origin.y+dividerview4.frame.size.height,self.view.frame.size.width-100,50.0f)];
        // leavegrpbtn.backgroundColor=[UIColor grayColor];
        [leavegrpbtn setTitle:@"Leave group" forState:UIControlStateNormal];
        //leavegrpbtn.contentMode=UIViewContentModeBottom;
        leavegrpbtn.titleLabel.font=[UIFont fontWithName:@"ProximaNova-Regular" size:19.0];
        [leavegrpbtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [leavegrpbtn setUserInteractionEnabled:YES];
    [leavegrpbtn addTarget:self action:@selector(leavegroup) forControlEvents:UIControlEventTouchUpInside];
        [mainscroll addSubview:leavegrpbtn];
    
        if([UIScreen mainScreen].bounds.size.width==320)
        {
            grayview2=[[UIView alloc]initWithFrame:CGRectMake(0.0,dividerview5.frame.origin.y+dividerview5.frame.size.height,self.view.frame.size.width,50)];
            [grayview2 setBackgroundColor:[UIColor colorWithRed:(237.0f/255.0f) green:(237.0f/255.0f) blue:(237.0f/255.0f) alpha:1.0f]];
            [mainscroll addSubview:grayview2];
        }
        else
        {
            grayview2=[[UIView alloc]initWithFrame:CGRectMake(0.0,dividerview5.frame.origin.y+dividerview5.frame.size.height,self.view.frame.size.width,110)];
            [grayview2 setBackgroundColor:[UIColor colorWithRed:(237.0f/255.0f) green:(237.0f/255.0f) blue:(237.0f/255.0f) alpha:1.0f]];
            [mainscroll addSubview:grayview2];
        }

    
    
}

-(void)leavegroup
{
    NSLog(@"okkkkk");
    activity=[[UIActivityIndicatorView alloc] init];
    activity.center = self.view.center;
    [activity startAnimating];
    activity.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [activity setColor:[UIColor blackColor]];
    [self.view addSubview:activity];
//
//    [UIView animateWithDuration:0 animations:^{
//        
//        expand = NO;
//        popupview.hidden=YES;
//        [SVProgressHUD showWithStatus:@"Procesing......"];
//        
//    }completion:^(BOOL finished) {
    
    
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *groups_url =[NSString stringWithFormat:@"https://budnav.com/ext/?action=group-leave&group_id=%@&access_token=%@&device_id=%@",[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"group_id"]],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
        DebugLog(@"groups url string ------ %@",groups_url);
        
        NSError *error1=nil;
        
        NSData *data2=[NSData dataWithContentsOfURL:[NSURL URLWithString:groups_url]];
        
        //        NSData *data2=[NSData dataWithContentsOfURL:[NSURL URLWithString:groups_url]options:NSDataReadingUncached error:&error1];
        if (data2==nil)
        {
            DebugLog(@"no connnnn");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                                            message:nil
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            alert.tag=5;
            [alert show];
            
            
            
        }
        else
        {
            groupdictionary=[NSJSONSerialization JSONObjectWithData:data2 //1
                                                            options:kNilOptions
                                                              error:&error1];
            
            DebugLog(@"Leave Group:%@",groupdictionary);
            
            
            NSString *check=[NSString stringWithFormat:@"%@",[groupdictionary valueForKey:@"success"]];
            if ([check isEqualToString:@"1"])
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"You have left the group successfully!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
                //[SVProgressHUD dismiss];
                ConGroupsViewController *grp=[[ConGroupsViewController alloc]init];
                
//                CATransition* transition = [CATransition animation];
//                
//                transition.duration = 0.3;
//                transition.type = kCATransitionPush;
//                transition.subtype = kCATransitionFade;
//                transition.subtype = kCATransitionFromRight;
//                [[self navigationController].view.layer addAnimation:transition forKey:nil];
                
                [self.navigationController dismissViewControllerAnimated:NO completion:nil];
                [self.navigationController pushViewController:grp animated:NO];
                
//                CATransition *transition = [CATransition animation];
//                
//                transition.duration = 0.5f;
//                // transition.type = kCATransitionPush;
//                transition.type = kCATransitionFade;
//                // transition.subtype = kCATransitionFromLeft;
//                
//                [[self navigationController].view.layer addAnimation:transition forKey:nil];
//               // [self.navigationController popViewControllerAnimated:YES];
//                
//                
//                [self.navigationController pushViewController:grp animated:YES];
            
            }
//
//            else if ([check isEqualToString:@"0"])
//            {
//                if (isAtLeast8)
//                {
//                    UIAlertController * alert=   [UIAlertController
//                                                  alertControllerWithTitle:@"Warning"
//                                                  message:[NSString stringWithFormat:@"%@",[groupdictionary valueForKey:@"error"]]
//                                                  preferredStyle:UIAlertControllerStyleAlert];
//                    
//                    UIAlertAction* cancel = [UIAlertAction
//                                             actionWithTitle:@"Ok"
//                                             style:UIAlertActionStyleDefault
//                                             handler:^(UIAlertAction * action)
//                                             {
//                                                 [alert dismissViewControllerAnimated:YES completion:nil];
//                                                 
//                                             }];
//                    
//                    [alert addAction:cancel];
//                    
//                    [self presentViewController:alert animated:YES completion:nil];
//                }
//                
//                if (isAtLeast7)
//                {
//                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Warning" message:[NSString stringWithFormat:@"%@",[groupdictionary valueForKey:@"error"]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//                    
//                    [alert show];
//                }
//                
//            }
//            
//            
        }
//        
//        
//    }];
}

 
- (void) Start
{
    act.hidden = NO;
    [act startAnimating];
}

-(void)changeSwitch1
{
    if(mySwitch.on)
    {
        switchcheck1=@"true";
        myswitchcheck1=1;
        [self urlFired];
    }
    else
    {
        switchcheck1=@"false";
        myswitchcheck1=0;
        [self urlFired];
    }
}
-(void)changeSwitch2
{
    if(mySwitch2.on)
    {
        switchckeck2=@"true";
        myswitchcheck2=1;
        [self urlFired];
    }
    else
    {
        switchckeck2=@"false";
        myswitchcheck2=0;
        [self urlFired];
    }
}

-(void)urlFired
{
    [mySwitch setUserInteractionEnabled:NO];
    [mySwitch2 setUserInteractionEnabled:NO];
    
    [self performSelectorOnMainThread:@selector(Start) withObject:nil waitUntilDone:NO];
    
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        
        // Background work
        
        [self performSelectorOnMainThread:@selector(Start) withObject:nil waitUntilDone:NO];
        
        NSString *Newgroups_url =[NSString stringWithFormat:@"https://budnav.com/ext/?action=group-membersettings&group_id=%@&access_token=%@&device_id=%@&show_contacts=%@&show_map=%@",[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"group_id"]],[[NSUserDefaults standardUserDefaults] objectForKey:@"access_token"],[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"],switchcheck1,switchckeck2];
        DebugLog(@"New groups url string ------ %@",Newgroups_url);
        NSError *error1;
        
        NSData *data1=[NSData dataWithContentsOfURL:[NSURL URLWithString:Newgroups_url]options:NSDataReadingUncached error:&error1];
        if (data1==nil)
        {
            DebugLog(@"no connnnn");
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                // Main thread work (UI usually)
                
                
                [mySwitch setUserInteractionEnabled:YES];
                [mySwitch2 setUserInteractionEnabled:YES];
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                                                message:nil
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                alert.tag=5;
                [alert show];
                [act stopAnimating];
                
            }];
            
            
            
            
            
        }
        
        if (data1 != nil)
        {
            
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                // Main thread work (UI usually)
                NSMutableDictionary *newgroupdict=[[NSMutableDictionary alloc]init];
                NSError *error1;
                newgroupdict=[NSJSONSerialization JSONObjectWithData:data1 //1
                                                             options:kNilOptions
                                                               error:&error1];
                DebugLog(@"-----New Groups info ----- %@",newgroupdict);
                [act stopAnimating];
                [mySwitch setUserInteractionEnabled:YES];
                [mySwitch2 setUserInteractionEnabled:YES];
                
            }];
        }
    }];
    
}

-(void)backtoprev:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)changecoloragain
{
    DebugLog(@"CHANGE COLOR");
    //    change.backgroundColor=[UIColor clearColor];
    bck_img.alpha = 1.0f;
    backbtn.alpha = 1.0f;
    //  group_namelbl.alpha=1.0f;
}

-(void)changecolor
{
    DebugLog(@"CHANGE COLOR1");
    //    change.backgroundColor=[UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0];
    bck_img.alpha = 0.5f;
    backbtn.alpha = 0.5f;
    // group_namelbl.alpha=0.5f;
    
}


-(void)viewDidDisappear:(BOOL)animated
{
    bck_img.alpha = 1.0f;
    backbtn.alpha = 1.0f;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
