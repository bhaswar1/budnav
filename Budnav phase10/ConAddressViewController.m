
//
//  ConAddressViewController.m
//  Contacter
//  Created by Bhaswar's MacBook Air on 06/05/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
#import "ConAddressViewController.h"
#import "ConOptionsPicViewController.h"
@interface ConAddressViewController ()
{
    UIScrollView *scviewbudget;
    UITextField *ziptext,*citytext,*numbertext,*streettext;
    UIAlertView *alert;
    UIImageView *viewimg1, *countryimg;
    UILabel *countrylabel;
    UITableView *countrycodetable;
    NSArray *countriesList;
    UIToolbar *numberToolbar;
}
@end

@implementation ConAddressViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
    
    scviewbudget = [[UIScrollView alloc] initWithFrame:CGRectMake(0,-20, 320,self.view.frame.size.height+20) ];
    scviewbudget.backgroundColor =[UIColor clearColor];
    scviewbudget.scrollEnabled = YES;
    scviewbudget.userInteractionEnabled = YES;
    [scviewbudget setContentSize:CGSizeMake(scviewbudget.frame.size.width,scviewbudget.frame.size.height+120)];
    //    scviewbudget.scrollEnabled=NO;
    [self.view addSubview:scviewbudget];
    scviewbudget.showsVerticalScrollIndicator = NO;
    scviewbudget.contentOffset=CGPointMake(0, 0);
    
    UILabel *startlb = [[UILabel alloc]initWithFrame:CGRectMake(0, 110, self.view.bounds.size.width, 35)];
    [scviewbudget addSubview:startlb];
    startlb.backgroundColor=[UIColor clearColor];
    startlb.text=@"Your Address";
    startlb.textColor=[UIColor colorWithRed:40.0f/255.0f green:82.0f/255.0f blue:135.0f/255.0f alpha:1];
    startlb.font=[UIFont fontWithName:@"ProximaNova-Bold" size:27.5f];
    startlb.textAlignment=NSTextAlignmentCenter;
    
    viewimg1=[[UIImageView alloc]initWithFrame:CGRectMake(15, 165, 290, 42.5f)];
    
    viewimg1.image=[UIImage imageNamed:@"input-filed.png"];
    
    [scviewbudget addSubview:viewimg1];
    viewimg1.userInteractionEnabled=YES;
    
    //==============Keyboard type numpad Done button============//
    
    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           //                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneButton)],
                           nil];
    [numberToolbar sizeToFit];
    
    UIImageView *zipimage=[[UIImageView alloc]initWithFrame:CGRectMake(15, 217, 100, 42.5f)];
    zipimage.image=[UIImage imageNamed:@"zip-input-field.png"];
    [scviewbudget addSubview:zipimage];
    zipimage.userInteractionEnabled=YES;
    
    ziptext= [[UITextField alloc]initWithFrame:CGRectMake(0, 4, 100, 37)];
    [zipimage addSubview:ziptext];
    ziptext.delegate=self;
    ziptext.backgroundColor=[UIColor clearColor];
    ziptext.placeholder=@"Zip Code";
    ziptext.text=@"";  //121212
    ziptext.textColor=[UIColor grayColor];
    ziptext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    ziptext.tintColor=[UIColor grayColor];
    ziptext.keyboardType=UIKeyboardTypeNumberPad;
    ziptext.textAlignment=NSTextAlignmentCenter;
    ziptext.keyboardAppearance=UIKeyboardAppearanceDark;
    ziptext.inputAccessoryView = numberToolbar;

    
    UIImageView *cityimage=[[UIImageView alloc]initWithFrame:CGRectMake(125, 217, 180, 42.5f)];
    
    cityimage.image=[UIImage imageNamed:@"country-input-field.png"];
    
    [scviewbudget addSubview:cityimage];
    cityimage.userInteractionEnabled=YES;
    
    citytext= [[UITextField alloc]initWithFrame:CGRectMake(10, 4, 165, 37)];
    [cityimage addSubview:citytext];
    citytext.delegate=self;
    citytext.backgroundColor=[UIColor clearColor];
    citytext.placeholder=@"City";
    citytext.text=@"";  //Kolkata
    citytext.textColor=[UIColor grayColor];
    citytext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    citytext.tintColor=[UIColor grayColor];
    citytext.keyboardType=UIKeyboardTypeDefault;
    citytext.keyboardAppearance=UIKeyboardAppearanceDark;
    
    
    
    UIImageView *streetimage=[[UIImageView alloc]initWithFrame:CGRectMake(15, 270, 180, 42.5f)];
    
    streetimage.image=[UIImage imageNamed:@"country-input-field.png"];
    
    [scviewbudget addSubview:streetimage];
    streetimage.userInteractionEnabled=YES;
    
    streettext= [[UITextField alloc]initWithFrame:CGRectMake(10, 4, 165, 37)];
    [streetimage addSubview:streettext];
    streettext.delegate=self;
    streettext.backgroundColor=[UIColor clearColor];
    streettext.placeholder=@"Street";
    streettext.text=@"";                    //Demo Street
    streettext.textColor=[UIColor grayColor];
    streettext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    streettext.tintColor=[UIColor grayColor];
    streettext.keyboardType=UIKeyboardTypeDefault;
    streettext.keyboardAppearance=UIKeyboardAppearanceDark;
    
    
    UIImageView *numberimage=[[UIImageView alloc]initWithFrame:CGRectMake(205, 270, 100, 42.5f)];
    
    numberimage.image=[UIImage imageNamed:@"zip-input-field.png"];
    
    [scviewbudget addSubview:numberimage];
    numberimage.userInteractionEnabled=YES;
    
    
    numbertext= [[UITextField alloc]initWithFrame:CGRectMake(7, 4, 90, 37)];
    [numberimage addSubview:numbertext];
    numbertext.delegate=self;
    numbertext.backgroundColor=[UIColor clearColor];
    numbertext.placeholder=@"Number";
    numbertext.text=@"";                            //9999999998
    numbertext.textColor=[UIColor grayColor];
    numbertext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:15.5f];
    numbertext.tintColor=[UIColor grayColor];
    numbertext.keyboardType=UIKeyboardTypeNumbersAndPunctuation;
    numbertext.keyboardAppearance=UIKeyboardAppearanceDark;
    
    
    UIButton *nextbt = [UIButton buttonWithType:UIButtonTypeCustom];
    nextbt.frame = CGRectMake(15, 340, 290, 45);
    nextbt.layer.cornerRadius=5;
    [nextbt setBackgroundImage:[UIImage imageNamed:@"next.png"] forState:UIControlStateNormal];
    nextbt.backgroundColor=[UIColor clearColor];
    [nextbt addTarget:self action:@selector(gotonext:) forControlEvents:UIControlEventTouchUpInside];
    [scviewbudget addSubview:nextbt];
    
    UIButton *helpbt = [UIButton buttonWithType:UIButtonTypeCustom];
    helpbt.frame = CGRectMake(15, self.view.bounds.size.height-40, 290, 30);
    helpbt.layer.cornerRadius=5;
    helpbt.titleLabel.font=[UIFont fontWithName:@"ProximaNova-Regular" size:18];
    [helpbt setTitle:@"Back" forState:UIControlStateNormal];
    [helpbt setTitleColor:[UIColor colorWithRed:17.0f/255.0f green:61.0f/255.0f blue:125.0f/255.0f alpha:1] forState:UIControlStateNormal];
    helpbt.backgroundColor=[UIColor clearColor];
    [helpbt addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    [scviewbudget addSubview:helpbt];
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    
    DebugLog(@"ASCHE==============>ISO = %@\nCountry = %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"placemarkISOCode"],[[NSUserDefaults standardUserDefaults] objectForKey:@"placemarkCode"]);
    
    NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"countriesflags" ofType:@"json"]];
    NSError *localError = nil;
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
    
    if (localError != nil) {
        DebugLog(@"%@", [localError userInfo]);
    }
    countriesList = (NSArray *)parsedObject;
    
//    DebugLog(@"FLAGS======================>%@",countriesList);
    
    for (check=0; check < countriesList.count; check++){
        
        if ([[[countriesList objectAtIndex:check]objectForKey:@"code2l"] isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"placemarkISOCode"]]) {
            
            flagCode = [[countriesList objectAtIndex:check]objectForKey:@"flag_128"];
            
        }
//        DebugLog(@"FLAG_CODE===============>%@",flagCode);
    }
    
    
    countryimg=[[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 35, 22)];
    
    countryimg.image=[UIImage imageNamed:flagCode];
    
    [viewimg1 addSubview:countryimg];
    countryimg.userInteractionEnabled=YES;
    
    countrylabel = [[UILabel alloc]initWithFrame:CGRectMake(60, 12, 225, 22)];
    [viewimg1 addSubview:countrylabel];
    countrylabel.backgroundColor=[UIColor clearColor];
    
    if([[[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkISOCode"] isKindOfClass:[NSNull class]] || [[[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkISOCode"] length] == 0){
        
        countrylabel.text = @"Select country";
        
    }else{
        
        countrylabel.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"placemarkCode"];
        
    }
    countrylabel.textColor=[UIColor grayColor];
    countrylabel.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
    
    
    
    countrycodetable=[[UITableView alloc]initWithFrame:CGRectMake(5, 20, (self.view.bounds.size.width)-10,(self.view.bounds.size.height)-30)];
    [scviewbudget addSubview:countrycodetable];
    countrycodetable.backgroundColor=[UIColor whiteColor];
    countrycodetable.dataSource=self;
    countrycodetable.delegate=self;
    countrycodetable.separatorStyle=UITableViewCellSeparatorStyleNone;
    countrycodetable.hidden=YES;
    countrycodetable.showsVerticalScrollIndicator=NO;
    countrycodetable.layer.borderColor=[[UIColor grayColor]CGColor];
    countrycodetable.layer.borderWidth=1;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(toggletable)];
    
    [viewimg1 addGestureRecognizer:tap];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)goBack: (id)sender
{
    CATransition *transition = [CATransition animation];
    
    transition.duration = 0.4f;
    
    transition.type = kCATransitionFade;
    
    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)gotonext:(id)sender
{
        NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    
    if ([[countrylabel.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
    {
        
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your country"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        
    }else if ([countrylabel.text isEqualToString:@"Select country"])
        
    {
        
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your country"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        
    }else if ([[ziptext.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
        {
            
            alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your Zip Code"
                                               message:Nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
        }else if ([ziptext.text isEqualToString:@"Zip Code"])
        {
            
            alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your Zip Code"
                                               message:Nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
            
        }else if (!([ziptext.text rangeOfCharacterFromSet:notDigits].location == NSNotFound))
        {
            
            alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your Zip Code"
                                               message:Nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
        }
        else if ([[citytext.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
        {
            
            alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your City"
                                               message:Nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
    
        }else if ([citytext.text isEqualToString:@"City"])
        {
            
            alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your City"
                                               message:Nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
            
        }
        else if ([[streettext.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
        {
            
            alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your Street"
                                               message:Nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
        }else if ([streettext.text isEqualToString:@"Street"])
        {
            
            alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your Street"
                                               message:Nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
            
        }else if ([numbertext.text isEqualToString:@"Number"])
        {
            
            alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your House Number"
                                               message:Nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
            
        }
        else if ([[numbertext.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
        {
            
            alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your House Number"
                                               message:Nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
            
        }else if ([[numbertext.text stringByTrimmingCharactersInSet:whitespace] length] <1 || [[numbertext.text stringByTrimmingCharactersInSet:whitespace] length] > 12 || !([numbertext.text rangeOfCharacterFromSet:notDigits].location == NSNotFound))
        {
            
            alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your House Number"
                                               message:Nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
        
        }else{
            
            ConOptionsPicViewController *pre= [[ConOptionsPicViewController alloc]init];
            
            [[NSUserDefaults standardUserDefaults] setObject:countrylabel.text forKey:@"r_country"];
            [[NSUserDefaults standardUserDefaults] setObject:ziptext.text forKey:@"r_zipcode"];
            [[NSUserDefaults standardUserDefaults] setObject:citytext.text forKey:@"r_city"];
            [[NSUserDefaults standardUserDefaults] setObject:streettext.text forKey:@"r_street"];
            [[NSUserDefaults standardUserDefaults] setObject:numbertext.text forKey:@"r_housenumber"];
            
            
//            CATransition* transition = [CATransition animation];
//            
//            transition.duration = 0.4;
//            transition.type = kCATransitionPush;
//            transition.subtype = kCATransitionFade;
//            
//            [[self navigationController].view.layer addAnimation:transition forKey:nil];
            
            [self.navigationController pushViewController:pre animated:YES];
   
        }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [UIView animateWithDuration:0.5f animations:^{
        scviewbudget.contentOffset = CGPointMake(0, -20);
    }];
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField { //Keyboard becomes visible
    
    // DebugLog(@"in editing");
    if (self.view.frame.size.height < 500)
    if(textField == streettext|| textField==numbertext){
        [UIView animateWithDuration:0.5f animations:^{
            scviewbudget.contentOffset = CGPointMake(0, 120);
        }];
    }
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [countriesList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [countrycodetable dequeueReusableCellWithIdentifier:CellIdentifier];
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell.backgroundColor=[UIColor clearColor];
    
    UIImageView *cellimg=[[UIImageView alloc]initWithFrame:CGRectMake(10, 7, 35, 22)];
    cellimg.image=[UIImage imageNamed:[[countriesList objectAtIndex:indexPath.row] objectForKey:@"flag_32"]];
    cellimg.clipsToBounds=YES;
    cellimg.contentMode=UIViewContentModeScaleToFill;
    [cell addSubview:cellimg];
    
    UILabel *name=[[UILabel alloc]initWithFrame:CGRectMake(60, 0, 230, 35)];
    name.backgroundColor=[UIColor clearColor];
    name.text=[NSString stringWithFormat:@"%@",[[countriesList objectAtIndex:indexPath.row] objectForKey:@"name"]];
    name.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
    name.textColor=[UIColor blackColor];
    [cell addSubview:name];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    countrylabel.text = @"";
    countryimg.image = nil;
    countrylabel.text = [NSString stringWithFormat:@"%@",[[countriesList objectAtIndex:indexPath.row]objectForKey:@"name"]];
    countryimg.image = [UIImage imageNamed:[[countriesList objectAtIndex:indexPath.row] objectForKey:@"flag_128"]];
    countrycodetable.hidden=YES;
    
    CATransition *animation = [CATransition animation];
    
    [animation setType:kCATransitionFade];
    
    animation.duration = 0.2f;
    
    [countrycodetable.layer addAnimation:animation forKey:nil];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 35.0f;
}

-(void)toggletable
{
    if (countrycodetable.hidden==YES)
    {
        countrycodetable.hidden=NO;
        CATransition *animation = [CATransition animation];
        
        [animation setType:kCATransitionReveal];
        
        [animation setSubtype:kCATransitionReveal];
        
        animation.duration = 0.7f;
        
        [countrycodetable.layer addAnimation:animation forKey:nil];
    }
    else
    {
        countrycodetable.hidden=YES;
        CATransition *animation = [CATransition animation];
        
        [animation setType:kCATransitionFade];
        
        [animation setSubtype:kCATransitionFade];
        
        animation.duration = 0.75f;
        
        [countrycodetable.layer addAnimation:animation forKey:nil];
        
    }
}

-(void)viewDidDisappear:(BOOL)animated
{
    countrylabel.text = @"";
    countryimg.image = nil;
}

-(void)doneButton{
    
    [ziptext resignFirstResponder];
}
@end