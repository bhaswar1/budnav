//
//  ConNewProfileViewController.m
//  Budnav
//
//  Created by intel on 03/06/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "ConNewProfileViewController.h"
#import "ConNewContactsViewController.h"
#import "ConPictureProfViewController.h"
#import "SocialWebViewController.h"
#import "ConNewRequestsViewController.h"
#import "ConRequestViewController.h"
#import "ConAddFriendViewController.h"
#import "DBManager.h"
#import "AppDelegate.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"
#import "ConLocateGroupViewController.h"
#import "ConFullSettingsViewController.h"
#import "ConPersonalProfileViewController.h"


@interface ConNewProfileViewController ()
{
    int switchprof, allowpersonal, nobusiness, profile_insertion, profile_fetch, move;
    double latitude,longitude;
    AppDelegate *del;
    NSUserDefaults *prefs;
    NSString *user_id, *base64String, *fullname, *mail, *googleMapUrlString, *appleMapUrlString;
    UIView *coverView, *mainview, *tapbgview, *mapview, *addressback, *addressView, *headingView, *designationView, *b_phoneView, *b_emailView, *b_addressView, *socialView, *landphoneView, *websiteView, *b_websiteView;
    CAGradientLayer *gradient;
    UIImageView *logo_img, *bck_img, *moreImage, *profile_image, *detailimg, *businessImg, *removeImg, *call_img, *sms_img, *email_img, *location_img;
    UIView *change, *grayback, *mailback, *blackView, *mobileView, *emailView, *b_companyView, *locationView;
    UILabel *backlbl, *labelHead, *prof_name, *location, *mobile_num, *emailId, *addressView_div, *lineDivMenu, *land_num, *website, *b_website, *removecon_label;
    UIButton *backBtn, *rightMenu, *crossbt, *navigatebt, *navigatebtapple, *togglebt, *removeconbt, *rightMenuBack, *call_btn, *sms_btn, *email_btn, *location_btn;
    UIActivityIndicatorView *act;
    NSMutableDictionary *dict_profile;
    NSDictionary *json1;
    UIAlertView *alert;
    UIImage *profilepic;
    UIScrollView *scrollView;
    NSMutableArray *mobileArray;
    UITapGestureRecognizer *overlayTap;
    UITextView *address;
    NSMutableArray *socialImage;
    BOOL map;
    BOOL rightmenuOpen, business_con, map_business_con;
}
@end

@implementation ConNewProfileViewController

- (void)viewDidLoad
{
    switchprof=0;
    [super viewDidLoad];
    
    NSLog(@"From ConNewProfileViewController");
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"error"];
    self.tabBarController.tabBar.hidden = YES;
    // Do any additional setup after loading the view.
    
    prefs = [NSUserDefaults standardUserDefaults];
    DebugLog(@"user id %@",[prefs objectForKey:@"userid"]);
    
    self.view.backgroundColor=[UIColor whiteColor];
    self.tabBarController.tabBar.translucent = YES;
    
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.navigationController.navigationBarHidden=YES;
    
    
    coverView = [[UIView alloc]init];
    coverView.frame = CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width,64);
    coverView.userInteractionEnabled=YES;
    [self.view addSubview:coverView];
    
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, coverView.frame.size.height);
    
    // Add the gradient to the view
    [coverView.layer insertSublayer:gradient atIndex:0];
    
    
    logo_img = [[UIImageView alloc]init];
    logo_img.frame = CGRectMake(27+([UIScreen mainScreen].bounds.size.width-160)/2, 32, 101, 20);
    logo_img.backgroundColor = [UIColor clearColor];
    
    logo_img.image = [UIImage imageNamed:@"new_logo"];
    [coverView addSubview:logo_img];
    
    
    if ([_other isEqualToString:@"yes"])
    {
        user_id =_uid;
        DebugLog(@"ID ID:%@",user_id);
        
    }
    else
    {
        user_id= [prefs objectForKey:@"userid"];
        
    }
    
    if ([user_id intValue] == [[prefs objectForKey:@"userid"] intValue])
    {
        DebugLog(@"IFFFFFF=============>");
        self.tabBarController.tabBar.frame = CGRectMake(0, [[UIScreen mainScreen]bounds].size.height-44, [UIScreen mainScreen].bounds.size.width, 44);
        self.tabBarController.tabBar.hidden = NO;
        ////////////////////////////////////////// Newly Added /////////////////////////////////////////////
        if ([_mapReturn isEqualToString:@"mapBack"]) {
            
            [self.tabBarController.tabBar setHidden:YES];
            map = TRUE;
            
            bck_img=[[UIImageView alloc]initWithFrame:CGRectMake(15, 32, 12, 20)];//(9,33,10,20)];//(13, 39, 10, 22)];
            bck_img.image=[UIImage imageNamed:@"back3"];
            [coverView addSubview:bck_img];
            
            labelHead = [[UILabel alloc]initWithFrame:CGRectMake(32, 31, 50, 20)];
            labelHead.text = @"Map";
            labelHead.backgroundColor = [UIColor clearColor];
            labelHead.textColor = [UIColor whiteColor];
            
            UIButton *backBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
            backBtn1.frame = CGRectMake(0, 0, logo_img.frame.origin.x
                                        -1, 64);
            backBtn1.backgroundColor=[UIColor clearColor];
            [backBtn1 addTarget:self action:@selector(changecolor1) forControlEvents:UIControlEventTouchDown];
            [backBtn1 addTarget:self action:@selector(changecoloragain1) forControlEvents:UIControlEventTouchDragExit];
            [backBtn1 addTarget:self action:@selector(changecolor1) forControlEvents:UIControlEventTouchDragEnter];
            
            [backBtn1 addTarget:self action:@selector(gobackoption) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:backBtn1];
            
        }
        else
        {
            
            change = [[UIView alloc]initWithFrame:CGRectMake(8,32,115,30)];
            [change setBackgroundColor:[UIColor clearColor]];
            [coverView addSubview:change];
            
            
            bck_img=[[UIImageView alloc]initWithFrame:CGRectMake(7, 0, 12, 20)];//(9,33,10,20)];//(13, 39, 10, 22)];
            bck_img.image=[UIImage imageNamed:@"back3"];
            [change addSubview:bck_img];
            
            backlbl=[[UILabel alloc]initWithFrame:CGRectMake(bck_img.frame.origin.x+bck_img.frame.size.width+5, 3, 50, 20)];//(bck_img.frame.origin.x+bck_img.frame.size.width+5, 28, 54, 30)];
            backlbl.text=@"Back";
            backlbl.textColor=[UIColor whiteColor];
            backlbl.backgroundColor=[UIColor clearColor];
            backlbl.userInteractionEnabled=YES;
            
            
            backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            backBtn.frame = CGRectMake(0, 0, logo_img.frame.origin.x
                                       -1, 64);
            backBtn.backgroundColor=[UIColor clearColor];
            [backBtn setTitle:@"" forState:UIControlStateNormal];
            [backBtn addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDown];
            [backBtn addTarget:self action:@selector(changecoloragain) forControlEvents:UIControlEventTouchDragExit];
            [backBtn addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDragEnter];
            
            
            [backBtn addTarget:self action:@selector(gobackoption) forControlEvents:UIControlEventTouchUpInside];
            [coverView addSubview:backBtn];
            
        }
        
    }else{ //////////////////////////////////// Newly Added ////////////////////////////////////
        
        DebugLog(@"ELSEEEE=============>");
        
        [self.tabBarController.tabBar setHidden:YES]; //===========OTHER PROFILE REMOVE FOOTER BUTTON BAR==========//
        if ([_mapReturn isEqualToString:@"mapBack"]) {
            
            map = TRUE;
            
            bck_img=[[UIImageView alloc]initWithFrame:CGRectMake(15, 32, 12, 20)];//(9,33,10,20)];//(13, 39, 10, 22)];
            bck_img.image=[UIImage imageNamed:@"back3"];
            [coverView addSubview:bck_img];
            
            labelHead = [[UILabel alloc]initWithFrame:CGRectMake(32, 31, 50, 20)];
            labelHead.text = @"Map";
            labelHead.backgroundColor = [UIColor clearColor];
            labelHead.textColor = [UIColor whiteColor];
            
            UIButton *backBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
            backBtn1.frame = CGRectMake(0, 0, logo_img.frame.origin.x
                                        -1, 64);
            backBtn1.backgroundColor=[UIColor clearColor];
            [backBtn1 addTarget:self action:@selector(changecolor1) forControlEvents:UIControlEventTouchDown];
            [backBtn1 addTarget:self action:@selector(changecoloragain1) forControlEvents:UIControlEventTouchDragExit];
            [backBtn1 addTarget:self action:@selector(changecolor1) forControlEvents:UIControlEventTouchDragEnter];
            
            [backBtn1 addTarget:self action:@selector(gobackoption) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:backBtn1];
            
        }else{
            change = [[UIView alloc]initWithFrame:CGRectMake(8,32,115,30)];
            [change setBackgroundColor:[UIColor clearColor]];
            [coverView addSubview:change];
            
            bck_img=[[UIImageView alloc]initWithFrame:CGRectMake(7, 0, 12, 20)];//(9,33,10,20)];//(13, 39, 10, 22)];
            bck_img.image=[UIImage imageNamed:@"back3"];
            [change addSubview:bck_img];
            
            backlbl=[[UILabel alloc]initWithFrame:CGRectMake(bck_img.frame.origin.x+bck_img.frame.size.width+5, 3, 50, 20)];
            backlbl.text=@"Back";
            backlbl.textColor=[UIColor whiteColor];
            backlbl.contentMode=UIViewContentModeScaleAspectFit;
            backlbl.backgroundColor=[UIColor clearColor];
            
            backBtn=[UIButton buttonWithType:UIButtonTypeCustom];//WithFrame:CGRectMake(bck_img.frame.origin.x, backlbl.frame.origin.y, backlbl.frame.origin.x+backlbl.frame.size.width-bck_img.frame.origin.x, backlbl.frame.size.height)];
            backBtn.frame=CGRectMake(0, 0, logo_img.frame.origin.x
                                     -1, 64);
            [backBtn setTitle:@"" forState:UIControlStateNormal];
            backBtn.backgroundColor=[UIColor clearColor];
            [backBtn setTitle:@"" forState:UIControlStateNormal];
            [backBtn addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDown];
            [backBtn addTarget:self action:@selector(changecoloragain) forControlEvents:UIControlEventTouchDragExit];
            [backBtn addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDragEnter];
            [backBtn addTarget:self action:@selector(gobackoption) forControlEvents:UIControlEventTouchUpInside];
            [coverView addSubview:backBtn];
            
            
            
        }
    }
    
    moreImage=[[UIImageView alloc]initWithFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-37, 32, 22,19)];
    [moreImage setImage:[UIImage imageNamed:@"new_menu"]];
    [coverView addSubview:moreImage];
    
    rightMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    rightMenu.enabled=YES;
    [rightMenu setFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-55,[[UIApplication sharedApplication]statusBarFrame].size.height, 55,coverView.frame.size.height-[[UIApplication sharedApplication]statusBarFrame].size.height)];
    [rightMenu setBackgroundImage:nil forState:UIControlStateNormal];
    [rightMenu setBackgroundImage:nil forState:UIControlStateSelected];
    [rightMenu setBackgroundImage:nil forState:UIControlStateHighlighted];
    [rightMenu setBackgroundColor:[UIColor clearColor]];
    [rightMenu addTarget:self action:@selector(rightMenu:) forControlEvents:UIControlEventTouchUpInside];
    [coverView addSubview:rightMenu];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestreceivedaction) name:@"Requestreceived_push" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(acceptedrequestaction) name:@"Accepted_push" object:nil];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AcceptedRequest) name:@"Accepted_request" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AddFriend) name:@"NewConnection" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ConnectionType) name:@"NewConnectionType" object:nil];
    
    // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AcceptedRequest) name:@"TypeChange" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shareLocation) name:@"ShareLocation" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdateInformation) name:@"UpdateInfo" object:nil];
    
}

-(void)UpdateInformation
{
    ConPersonalProfileViewController *PVC = [[ConPersonalProfileViewController alloc]init];
    PVC.toUpdateInfo = @"YES";
    [self.navigationController pushViewController:PVC animated:YES];
   // [[NSNotificationCenter defaultCenter] postNotificationName:@"Update Info" object:Nil];
    
}


-(void)shareLocation
{
    ConLocateGroupViewController *conLocate = [[ConLocateGroupViewController alloc]init];
    conLocate.group_id=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"sharedLocGrpId"]];
    [self.navigationController pushViewController:conLocate animated:YES];
}


-(void)ConnectionType
{
    ConNewRequestsViewController *mng = [[ConNewRequestsViewController alloc]init];
    mng.userid = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userforrequest"] intValue];
    //    CATransition* transition = [CATransition animation];
    //
    //    transition.duration = 0.4;
    //    transition.type = kCATransitionPush;
    //    transition.subtype = kCATransitionFade;
    //
    //    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:mng animated:YES];
}


-(void)AddFriend
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDel.newUser)
    {
        
        ConAddFriendViewController *con = [[ConAddFriendViewController alloc]init];
        
        //    CATransition* transition = [CATransition animation];
        //
        //    transition.duration = 0.4;
        //    transition.type = kCATransitionPush;
        //    transition.subtype = kCATransitionFade;
        
        appDel.newUser = NO;
        
        //    [[self navigationController].view.layer addAnimation:transition forKey:nil];
        [[self navigationController] pushViewController:con animated:YES];
        
    }
}

-(void)AcceptedRequest
{
    ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
    // con.other=@"yes";
    con.request = YES;
    [self.tabBarController.tabBar setHidden:YES];
    // con.uid=[NSString stringWithFormat:@"%d",[userid intValue]];
    //                [self.navigationController presentViewController:con animated:NO completion:nil];
    
    //    CATransition* transition = [CATransition animation];
    //
    //    transition.duration = 0.4;
    //    transition.type = kCATransitionPush;
    //    transition.subtype = kCATransitionFade;
    //
    //    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
    
}

-(void)requestreceivedaction
{
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
    con.request = TRUE;
    con.other=@"yes";
    con.uid=[NSString stringWithFormat:@"%@",del.pushtoprofileid];
    
    //    CATransition* transition = [CATransition animation];
    //
    //    transition.duration = 0.4;
    //    transition.type = kCATransitionPush;
    //    transition.subtype = kCATransitionFade;
    //
    //    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}

-(void)acceptedrequestaction
{
    ConRequestViewController *con = [[ConRequestViewController alloc]init];
    
    //    CATransition* transition = [CATransition animation];
    //
    //    transition.duration = 0.4;
    //    transition.type = kCATransitionPush;
    //    transition.subtype = kCATransitionFade;
    //
    //    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}



-(void)viewDidAppear:(BOOL)animated
{
    DebugLog(@"MAPRETURN:%@",_mapReturn);
    
    if (![_mapReturn isKindOfClass:[NSNull class]] && _mapReturn!=nil && ![_mapReturn isEqualToString:@"(null)"] && [_mapReturn isEqualToString:@"mapBack"])
        
    {
        
        if ([_business_friend isEqualToString:@"yes"])
            
        {
            
            self.screenName = @"Map business contact";
            
            [[GAI sharedInstance].defaultTracker send:
             
             [[GAIDictionaryBuilder createEventWithCategory:@"Map"
               
                                                     action:@"Map business contact"
               
                                                      label:nil
               
                                                      value:nil] build]];
            
        }
        
        else
            
        {
            
            self.screenName = @"Map personal contact";
            
            [[GAI sharedInstance].defaultTracker send:
             
             [[GAIDictionaryBuilder createEventWithCategory:@"Map"
               
                                                     action:@"Map personal contact"
               
                                                      label:nil
               
                                                      value:nil] build]];
            
        }
    }
    _phonearr = [[NSMutableArray alloc]init];
    socialView = [[UIView alloc] init];
    socialImage = [[NSMutableArray alloc] init];
    DebugLog(@"CONTACT DETAILS:%@",_Condetails_dict);
    
    if (_request) {
        map = YES;
    }
    
    if (_Condetails_dict !=nil) {
        
        if ([[_Condetails_dict objectForKey:@"business"] intValue]==1) {
            
            self.screenName = @"Contacts";
            
            [[GAI sharedInstance].defaultTracker send:
             
             [[GAIDictionaryBuilder createEventWithCategory:@"Contacts"
               
                                                     action:@"Business contact"
               
                                                      label:nil
               
                                                      value:nil] build]];
            
            business_con = TRUE;
        }
        
        else
        {
            
            self.screenName = @"Contacts";
            
            [[GAI sharedInstance].defaultTracker send:
             
             [[GAIDictionaryBuilder createEventWithCategory:@"Contacts"
               
                                                     action:@"Personal contact"
               
                                                      label:nil
               
                                                      value:nil] build]];
            
            business_con = FALSE;
            
        }
    }
    
    nobusiness=0;
    move=0;
    profile_insertion=0;
    profile_fetch=0;
    allowpersonal=0;
    
    [b_addressView removeFromSuperview];
    [b_companyView removeFromSuperview];
    [b_emailView removeFromSuperview];
    [b_phoneView removeFromSuperview];
    [designationView removeFromSuperview];
    [b_websiteView removeFromSuperview];
    [socialView removeFromSuperview];
    
    
    act = [[UIActivityIndicatorView alloc] init];//WithFrame:CGRectMake(135, 190, 40, 40)];
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    act.center=self.view.center;
    [act setColor:[UIColor blackColor]];
    [self.view addSubview:act];
    [act startAnimating];
    
    
    
    headingView = [[UIView alloc] initWithFrame:CGRectMake(0, 65, [UIScreen mainScreen].bounds.size.width, 101)];
    [headingView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:headingView];
    
    profile_image = [[UIImageView alloc] initWithFrame:CGRectMake(12, 12, 76, 76)];
    profile_image.backgroundColor = [UIColor clearColor];
    profile_image.layer.cornerRadius = profile_image.frame.size.width/2;
    profile_image.clipsToBounds = YES;
    profile_image.image = [UIImage imageNamed:@"Backup profile pic"];
    [headingView addSubview:profile_image];
    
    
    profile_image.contentMode= UIViewContentModeScaleAspectFill;
    profile_image.clipsToBounds=YES;
    profile_image.userInteractionEnabled=YES;
    UITapGestureRecognizer *tapprofpic = [[UITapGestureRecognizer alloc]
                                          initWithTarget:self
                                          action:@selector(detailpic:)];
    
    [profile_image addGestureRecognizer:tapprofpic];
    //    //    }
    
    
    prof_name = [[UILabel alloc] initWithFrame:CGRectMake(profile_image.frame.origin.x+profile_image.frame.size.width+24, 17, [UIScreen mainScreen].bounds.size.width-120, 35)];
    [prof_name setBackgroundColor:[UIColor clearColor]];
    prof_name.font = [UIFont fontWithName:@"ProximaNova-Bold" size:17];
    prof_name.numberOfLines = 2;
    //prof_name.text = [NSString stringWithFormat:@"%@",[_Condetails_dict objectForKey:@"fullname"]];
    prof_name.lineBreakMode = NSLineBreakByWordWrapping;
    prof_name.textColor = [UIColor blackColor];
    prof_name.textAlignment = NSTextAlignmentLeft;
    [headingView addSubview:prof_name];
    [prof_name adjustsFontSizeToFitWidth];
    
    locationView = [[UIView alloc] init];
    locationView.frame = CGRectMake(profile_image.frame.origin.x+profile_image.frame.size.width+24, 52, [UIScreen mainScreen].bounds.size.width-120, 35);
    locationView.backgroundColor = [UIColor clearColor];
    [headingView addSubview:locationView];
    
    
    
    location = [[UILabel alloc] init];
    location.frame = CGRectMake(12, 5, [UIScreen mainScreen].bounds.size.width-100, 17);
    location.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
    location.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
    //    location.text = [[NSString stringWithFormat:@"%@, %@",[_Condetails_dict objectForKey:@"city"],[_Condetails_dict objectForKey:@"country"]] uppercaseString];
    [locationView addSubview:location];
    
    
    UILabel *heading_div = [[UILabel alloc] initWithFrame:CGRectMake(0, 100.5f, [UIScreen mainScreen].bounds.size.width, 0.5f)];
    [heading_div setBackgroundColor:[UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f]];
    [headingView addSubview:heading_div];
    
    UIView *actionView = [[UIView alloc] init];
    actionView.frame = CGRectMake(0, 166, [UIScreen mainScreen].bounds.size.width, 56);
    [self.view addSubview:actionView];
    
    UILabel *actionView_div = [[UILabel alloc] init];
    actionView_div.frame = CGRectMake(0, 55.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
    actionView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
    [actionView addSubview:actionView_div];
    
    call_img = [[UIImageView alloc] init];
    call_img.frame = CGRectMake(([UIScreen mainScreen].bounds.size.width/4-23)/2, 16.5, 23, 23);
    call_img.image = [UIImage imageNamed:@"Call"];
    [actionView addSubview:call_img];
    
    UILabel *call_div = [[UILabel alloc] init];
    call_div.frame = CGRectMake(call_img.frame.origin.x+call_img.frame.size.width+([UIScreen mainScreen].bounds.size.width/4-23)/2-0.5f, 0, 0.5f, 56);
    call_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
    [actionView addSubview:call_div];
    
    call_btn = [[UIButton alloc] init];
    call_btn.tag = 1;
    call_btn.frame = CGRectMake(0, 0, call_img.frame.origin.x+call_img.frame.size.width+35-0, 56);
    [call_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    //    [call_btn addTarget:self action:@selector(phone_color) forControlEvents:UIControlEventTouchDown];
    //    [call_btn addTarget:self action:@selector(phone_colorAgain) forControlEvents:UIControlEventTouchDragExit];
    //    [call_btn addTarget:self action:@selector(phone_color) forControlEvents:UIControlEventTouchDragEnter];
    [call_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
    [call_btn addTarget:self action:@selector(phonefunc:) forControlEvents:UIControlEventTouchUpInside];
    [call_btn setBackgroundColor:[UIColor clearColor]];
    [actionView addSubview:call_btn];
    
    sms_img = [[UIImageView alloc] init];
    sms_img.frame = CGRectMake(call_div.frame.origin.x+call_div.frame.size.width+([UIScreen mainScreen].bounds.size.width/4-23)/2, 16.5, 23, 23);
    sms_img.image = [UIImage imageNamed:@"SMS"];
    [actionView addSubview:sms_img];
    
    UILabel *sms_div = [[UILabel alloc] init];
    sms_div.frame = CGRectMake(sms_img.frame.origin.x+sms_img.frame.size.width+([UIScreen mainScreen].bounds.size.width/4-23)/2-0.5f, 0, 0.5f, 56);
    sms_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
    [actionView addSubview:sms_div];
    
    sms_btn = [[UIButton alloc] init];
    sms_btn.frame = CGRectMake(call_div.frame.origin.x+call_div.frame.size.width, 0, sms_img.frame.origin.x+sms_img.frame.size.width+40-(call_div.frame.origin.x+call_div.frame.size.width+5), 56);
    [sms_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    //    [sms_btn addTarget:self action:@selector(sms_color) forControlEvents:UIControlEventTouchDown];
    //    [sms_btn addTarget:self action:@selector(sms_colorAgain) forControlEvents:UIControlEventTouchDragExit];
    //    [sms_btn addTarget:self action:@selector(sms_color) forControlEvents:UIControlEventTouchDragEnter];
    [sms_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
    [sms_btn addTarget:self action:@selector(smsfunc) forControlEvents:UIControlEventTouchUpInside];
    [sms_btn setBackgroundColor:[UIColor clearColor]];
    [actionView addSubview:sms_btn];
    
    email_img = [[UIImageView alloc] init];
    email_img.frame = CGRectMake(sms_div.frame.origin.x+sms_div.frame.size.width+([UIScreen mainScreen].bounds.size.width/4-23)/2, 16.5, 23, 23);
    email_img.image = [UIImage imageNamed:@"Email"];
    [actionView addSubview:email_img];
    
    UILabel *email_div = [[UILabel alloc] init];
    email_div.frame = CGRectMake(email_img.frame.origin.x+email_img.frame.size.width+([UIScreen mainScreen].bounds.size.width/4-23)/2-0.5f, 0, 0.5f, 56);
    email_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
    [actionView addSubview:email_div];
    
    email_btn = [[UIButton alloc] init];
    email_btn.frame = CGRectMake(sms_div.frame.origin.x+sms_div.frame.size.width, 0, email_img.frame.origin.x+email_img.frame.size.width+40-(sms_div.frame.origin.x+sms_div.frame.size.width+5), 56);
    [email_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    //    [email_btn addTarget:self action:@selector(email_color) forControlEvents:UIControlEventTouchDown];
    //    [email_btn addTarget:self action:@selector(email_colorAgain) forControlEvents:UIControlEventTouchDragExit];
    //    [email_btn addTarget:self action:@selector(email_color) forControlEvents:UIControlEventTouchDragEnter];
    [email_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
    [email_btn addTarget:self action:@selector(mailFun:) forControlEvents:UIControlEventTouchUpInside];
    [email_btn setBackgroundColor:[UIColor clearColor]];
    [actionView addSubview:email_btn];
    
    location_img = [[UIImageView alloc] init];
    location_img.frame = CGRectMake(email_div.frame.origin.x+email_div.frame.size.width+([UIScreen mainScreen].bounds.size.width/4-23)/2, 16.5, 23, 23);
    location_img.image = [UIImage imageNamed:@"Navigate"];
    [actionView addSubview:location_img];
    
    location_btn = [[UIButton alloc] init];
    location_btn.frame = CGRectMake(email_div.frame.origin.x+email_div.frame.size.width, 0, location_img.frame.origin.x+location_img.frame.size.width+40-(email_div.frame.origin.x+email_div.frame.size.width+5), 56);
    [location_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    //    [location_btn addTarget:self action:@selector(loc_color) forControlEvents:UIControlEventTouchDown];
    //    [location_btn addTarget:self action:@selector(loc_colorAgain) forControlEvents:UIControlEventTouchDragExit];
    //    [location_btn addTarget:self action:@selector(loc_color) forControlEvents:UIControlEventTouchDragEnter];
    [location_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
    [location_btn addTarget:self action:@selector(openmap:) forControlEvents:UIControlEventTouchUpInside];
    [location_btn setBackgroundColor:[UIColor clearColor]];
    [actionView addSubview:location_btn];
    
    mainview = [[UIView alloc]initWithFrame:CGRectMake(0, 222, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    [self.view addSubview:mainview];
    mainview.backgroundColor=[UIColor clearColor];
    
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width,self.view.frame.size.height) ];
    scrollView.backgroundColor =[UIColor clearColor];
    scrollView.scrollEnabled = YES;
    scrollView.userInteractionEnabled = YES;
    // [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width,scrollView.frame.size.height+55)];
    //    scrollView.contentSize = CGSizeMake(1, scrollView.frame.size.height+255);
    scrollView.showsVerticalScrollIndicator=NO;
    [mainview addSubview:scrollView];
    scrollView.contentOffset=CGPointMake(0, 0);
    
    
    if([UIScreen mainScreen].bounds.size.height==568)
    {
        [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width,scrollView.frame.size.height+70)];
    }
    else
        [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width,scrollView.frame.size.height+55)];
    
    mobileView = [[UIView alloc] init];
    
    landphoneView = [[UIView alloc] init];
    if (!map) {
        
        if ([_Condetails_dict objectForKey:@"name"] != false && ![[_Condetails_dict objectForKey:@"name"] isEqualToString:@"0"] && [[_Condetails_dict objectForKey:@"name"] length] > 1 && ![[_Condetails_dict objectForKey:@"name"] isEqualToString:@"(null)"] && ![[_Condetails_dict objectForKey:@"name"] isEqualToString:@""]) {
            prof_name.text = [NSString stringWithFormat:@"%@ %@",[_Condetails_dict objectForKey:@"name"], [_Condetails_dict objectForKey:@"surname"]];
        }
        
        if (!business_con) {
            
            
            if ([_Condetails_dict objectForKey:@"city"] != false && ![[_Condetails_dict objectForKey:@"city"] isEqualToString:@"0"] && [[_Condetails_dict objectForKey:@"city"] length] > 1 && ![[_Condetails_dict objectForKey:@"city"] isEqualToString:@"(null)"] && ![[_Condetails_dict objectForKey:@"city"] isEqualToString:@""] && [_Condetails_dict objectForKey:@"country"] != false && ![[_Condetails_dict objectForKey:@"country"] isEqualToString:@"0"] && [[_Condetails_dict objectForKey:@"country"] length] > 1 && ![[_Condetails_dict objectForKey:@"country"] isEqualToString:@"(null)"] && ![[_Condetails_dict objectForKey:@"country"] isEqualToString:@""]) {
                
                UIImageView *loc_img = [[UIImageView alloc] init];
                loc_img.frame = CGRectMake(0, 8, 7, 9);
                loc_img.image = [UIImage imageNamed:@"Loction"];
                [locationView addSubview:loc_img];
                
                location.text = [[NSString stringWithFormat:@"%@, %@",[_Condetails_dict objectForKey:@"city"],[_Condetails_dict objectForKey:@"country"]] uppercaseString];
                
            }
            
            if ([_Condetails_dict objectForKey:@"mobile_num"] != false && ![[_Condetails_dict objectForKey:@"mobile_num"] isEqualToString:@"0"] && [[_Condetails_dict objectForKey:@"mobile_num"] length] > 1 && ![[_Condetails_dict objectForKey:@"mobile_num"] isEqualToString:@"(null)"] && ![[_Condetails_dict objectForKey:@"mobile_num"] isEqualToString:@""])
            {
                mobileView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 62);
                mobileView.backgroundColor = [UIColor clearColor];
                [scrollView addSubview:mobileView];
                
                UILabel *mobileView_div = [[UILabel alloc] init];
                mobileView_div.frame = CGRectMake(0, 61.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
                mobileView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
                [mobileView addSubview:mobileView_div];
                
                UILabel *mobileLabel = [[UILabel alloc] init];
                mobileLabel.frame = CGRectMake(20, 10, 100, 22);
                mobileLabel.text = @"MOBILE";
                mobileLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
                mobileLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
                [mobileView addSubview:mobileLabel];
                
                mobile_num = [[UILabel alloc] init];
                mobile_num.frame = CGRectMake(20, 32, 150, 22);
                //mobile_num.text = [NSString stringWithFormat:@"%@ %@",[_Condetails_dict objectForKey:@"mobile_pre"],[_Condetails_dict objectForKey:@"mobile_num"]];
                mobile_num.textColor = [UIColor blackColor];
                mobile_num.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
                [mobileView addSubview:mobile_num];
                
                
                UIImageView *whatsapp_img = [[UIImageView alloc] init];
                whatsapp_img.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-50, 20, 22, 22);
                whatsapp_img.image = [UIImage imageNamed:@"Whatsapp"];
              //  [mobileView addSubview:whatsapp_img];
                
                UIButton *whatsapp_btn = [[UIButton alloc] init];
                whatsapp_btn.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-65, 10, 52, 42);
                [whatsapp_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
                [whatsapp_btn setBackgroundColor:[UIColor clearColor]];
                [whatsapp_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
                [whatsapp_btn addTarget:self action:@selector(whatsapp_func) forControlEvents:UIControlEventTouchUpInside];
              //  [mobileView addSubview:whatsapp_btn];
                
                
                UIButton *mobileView_btn = [[UIButton alloc] init];
                mobileView_btn.tag = 1;
                mobileView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-65, 62);
                [mobileView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
                [mobileView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
                [mobileView_btn addTarget:self action:@selector(phonefunc:) forControlEvents:UIControlEventTouchUpInside];
                [mobileView addSubview:mobileView_btn];
                
                mobile_num.text = [NSString stringWithFormat:@"%@ %@",[_Condetails_dict objectForKey:@"mobile_pre"],[_Condetails_dict objectForKey:@"mobile_num"]];
                [_phonearr addObject:mobile_num.text];
                //[_phonearr addObject:[NSString stringWithFormat:@"%@ %@",[_Condetails_dict objectForKey:@"phone_pre"],[_Condetails_dict objectForKey:@"phone_num"]]];
            }
            
            
            
            if ([_Condetails_dict objectForKey:@"phone_num"] != false && ![[_Condetails_dict objectForKey:@"phone_num"] isEqualToString:@"0"] && [[_Condetails_dict objectForKey:@"phone_num"] length] > 1 && ![[_Condetails_dict objectForKey:@"phone_num"] isEqualToString:@"(null)"] && ![[_Condetails_dict objectForKey:@"phone_num"] isEqualToString:@""]) {
                
                landphoneView.frame = CGRectMake(0, mobileView.frame.origin.y+mobileView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 62);
                landphoneView.backgroundColor = [UIColor clearColor];
                [scrollView addSubview:landphoneView];
                
                UILabel *landphoneView_div = [[UILabel alloc] init];
                landphoneView_div.frame = CGRectMake(0, 61.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
                landphoneView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
                [landphoneView addSubview:landphoneView_div];
                
                UILabel *landLabel = [[UILabel alloc] init];
                landLabel.frame = CGRectMake(20, 10, 100, 22);
                landLabel.text = @"HOME";
                landLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
                landLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
                [landphoneView addSubview:landLabel];
                
                
                //[landphonelb addTarget:self action:@selector(landphoneFun:) forControlEvents:UIControlEventTouchUpInside];
                
                land_num = [[UILabel alloc] init];
                land_num.frame = CGRectMake(20, 32, 180, 22);
                land_num.text = [NSString stringWithFormat:@"%@ %@",[_Condetails_dict objectForKey:@"phone_pre"],[_Condetails_dict objectForKey:@"phone_num"]];
                land_num.textColor = [UIColor blackColor];
                land_num.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
                [landphoneView addSubview:land_num];
                
                [_phonearr addObject:[NSString stringWithFormat:@"%@ %@",[_Condetails_dict objectForKey:@"phone_pre"],[_Condetails_dict objectForKey:@"phone_num"]]];
                
                UIButton *landView_btn = [[UIButton alloc] init];
                if ([_phonearr count]==2) {
                    landView_btn.tag = 2;
                }
                else
                {
                    landView_btn.tag = 1;
                }
                
                landView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 62);
                [landView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
                [landView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
                [landView_btn addTarget:self action:@selector(phonefunc:) forControlEvents:UIControlEventTouchUpInside];
                [landphoneView addSubview:landView_btn];
                
            }
            else
            {
                landphoneView.frame = CGRectMake(0, mobileView.frame.origin.y+mobileView.frame.size.height, 0,0);
            }
            
            
            
            emailView = [[UIView alloc] init];
            emailView.frame = CGRectMake(0, landphoneView.frame.origin.y+landphoneView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 62);
            emailView.backgroundColor = [UIColor clearColor];
            [scrollView addSubview:emailView];
            
            UILabel *emailView_div = [[UILabel alloc] init];
            emailView_div.frame = CGRectMake(0, 61.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
            emailView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
            [emailView addSubview:emailView_div];
            
            UILabel *emailLabel = [[UILabel alloc] init];
            emailLabel.frame = CGRectMake(20, 10, 100, 22);
            emailLabel.text = @"EMAIL";
            emailLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
            emailLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
            [emailView addSubview:emailLabel];
            
            emailId = [[UILabel alloc] init];
            emailId.frame = CGRectMake(20, 32, [UIScreen mainScreen].bounds.size.width, 22);
            //emailId.text = [NSString stringWithFormat:@"%@",[_Condetails_dict objectForKey:@"email"]];
            emailId.textColor = [UIColor blackColor];
            emailId.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
            [emailView addSubview:emailId];
            //    mail = emailId.text;
            
            UIButton *emailView_btn = [[UIButton alloc] init];
            emailView_btn.tag = 2;
            emailView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 62);
            [emailView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
            [emailView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
            [emailView_btn addTarget:self action:@selector(mailFun:) forControlEvents:UIControlEventTouchUpInside];
            [emailView addSubview:emailView_btn];
            
            if ([_Condetails_dict objectForKey:@"email"] != false && ![[_Condetails_dict objectForKey:@"email"] isEqualToString:@"0"] && [[_Condetails_dict objectForKey:@"email"] length] > 1 && ![[_Condetails_dict objectForKey:@"email"] isEqualToString:@"(null)"] && ![[_Condetails_dict objectForKey:@"email"] isEqualToString:@""]) {
                
                emailId.text = [NSString stringWithFormat:@"%@",[_Condetails_dict objectForKey:@"email"]];
                mail = emailId.text;
            }
            
            /////////////////////////////////////////////// WEBSITE /////////////////////////////////////////
            
            websiteView = [[UIView alloc] init];
            
            if ([_Condetails_dict objectForKey:@"website"] != false && ![[_Condetails_dict objectForKey:@"website"] isEqualToString:@"0"] && [[_Condetails_dict objectForKey:@"website"] length] > 1 && ![[_Condetails_dict objectForKey:@"website"] isEqualToString:@"(null)"] && ![[_Condetails_dict objectForKey:@"website"] isEqualToString:@""]) {
                
                websiteView.frame = CGRectMake(0, emailView.frame.origin.y+emailView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 62);
                websiteView.backgroundColor = [UIColor clearColor];
                [scrollView addSubview:websiteView];
                
                UILabel *websiteView_div = [[UILabel alloc] init];
                websiteView_div.frame = CGRectMake(0, 61.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
                websiteView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
                [websiteView addSubview:websiteView_div];
                
                UILabel *websiteLabel = [[UILabel alloc] init];
                websiteLabel.frame = CGRectMake(20, 10, 100, 22);
                websiteLabel.text = @"WEBSITE";
                websiteLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
                websiteLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
                [websiteView addSubview:websiteLabel];
                
                
                //[landphonelb addTarget:self action:@selector(landphoneFun:) forControlEvents:UIControlEventTouchUpInside];
                
                website = [[UILabel alloc] init];
                website.frame = CGRectMake(20, 32, [UIScreen mainScreen].bounds.size.width, 22);
                website.text = [NSString stringWithFormat:@"%@",[_Condetails_dict objectForKey:@"website"]];
                website.textColor = [UIColor blackColor];
                website.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
                [websiteView addSubview:website];
                
                UIButton *websiteView_btn = [[UIButton alloc] init];
                websiteView_btn.tag = 1;
                websiteView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 62);
                [websiteView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
                [websiteView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
                [websiteView_btn addTarget:self action:@selector(websiteFun:) forControlEvents:UIControlEventTouchUpInside];
                [websiteView addSubview:websiteView_btn];
                
            }
            else
            {
                websiteView.frame = CGRectMake(0, emailView.frame.origin.y+emailView.frame.size.height, 0,0);
                
                
                
            }
            
            
            
            
            
            //////////////////////////////////////////////////////////////////////////////////////////////////
            
            //    [address sizeToFit];
            
            if ([_Condetails_dict objectForKey:@"street"] != false && ![[_Condetails_dict objectForKey:@"street"] isEqualToString:@"0"] && [[_Condetails_dict objectForKey:@"street"] length] > 1 && ![[_Condetails_dict objectForKey:@"street"] isEqualToString:@"(null)"] && ![[_Condetails_dict objectForKey:@"street"] isEqualToString:@""])
            {
                
                addressView = [[UIView alloc] init];
                addressView.frame = CGRectMake(0, websiteView.frame.origin.y+websiteView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 106);
                addressView.backgroundColor = [UIColor clearColor];
                [scrollView addSubview:addressView];
                
                addressView_div = [[UILabel alloc] init];
                addressView_div.frame = CGRectMake(0, 105.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
                addressView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
                [addressView addSubview:addressView_div];
                
                UILabel *addressLabel = [[UILabel alloc] init];
                addressLabel.frame = CGRectMake(20, 10, 100, 22);
                addressLabel.text = @"ADDRESS";
                addressLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
                addressLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
                [addressView addSubview:addressLabel];
                
                [self performSelectorOnMainThread:@selector(loadtextview) withObject:nil waitUntilDone:YES];
            }
            
        }
    }
    
    [self performSelectorOnMainThread:@selector(stopLoader) withObject:nil waitUntilDone:YES];
    NSOperationQueue *queue=[NSOperationQueue new];
    NSInvocationOperation *op=[[NSInvocationOperation alloc] initWithTarget:self selector:@selector(loaddata) object:nil];
    [queue addOperation:op];
}



-(void)loadtextview
{
    @try {
        address = [[UITextView alloc] initWithFrame:CGRectMake(15, 32, addressView.frame.size.width-15, 70)];
        //        address.frame = ;
        address.backgroundColor = [UIColor clearColor];
        address.editable = NO;
        address.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
        //        [addressView addSubview:address];
        
        [addressView addSubview:address];
        
        UIButton *addressView_btn = [[UIButton alloc] init];
        addressView_btn.tag = 2;
        addressView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 106);
        [addressView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [addressView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
        [addressView_btn addTarget:self action:@selector(openmap:) forControlEvents:UIControlEventTouchUpInside];
        [addressView addSubview:addressView_btn];
        
        
        
        NSString *addressValue = [NSString stringWithFormat:@"%@<br>%@ %@<br>%@",[_Condetails_dict objectForKey:@"street"],[_Condetails_dict objectForKey:@"zipcode"],[_Condetails_dict objectForKey:@"city"],[_Condetails_dict objectForKey:@"country"]];
        //
        [address setValue:addressValue forKey:@"contentToHTMLString"];
        address.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
        
        address.textColor = [UIColor blackColor];
        
    }
    @catch (NSException *exception) {
        NSLog(@"textview exception: %@",exception.description);
    }
    @finally {
        
    }
    
}


-(void)viewDidDisappear:(BOOL)animated
{
    [mobileView removeFromSuperview];
    [landphoneView removeFromSuperview];
    [emailView removeFromSuperview];
    [websiteView removeFromSuperview];
    [addressView removeFromSuperview];
    [socialView removeFromSuperview];
    
    [b_companyView removeFromSuperview];
    [designationView removeFromSuperview];
    [b_emailView removeFromSuperview];
    [b_websiteView removeFromSuperview];
    [b_addressView removeFromSuperview];
    [socialView removeFromSuperview];
    [socialImage removeAllObjects];
    
    b_phoneView.backgroundColor = [UIColor clearColor];
    mobileView.backgroundColor = [UIColor clearColor];
    b_emailView.backgroundColor = [UIColor clearColor];
    emailView.backgroundColor = [UIColor clearColor];
    b_addressView.backgroundColor = [UIColor clearColor];
    addressView.backgroundColor = [UIColor clearColor];
}

-(void)loaddata
{
    
    [self performSelectorOnMainThread:@selector(startLoader) withObject:nil waitUntilDone:NO];
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"firstProfile"] isEqualToString:@"yes"])
    {
        NSError *error;
        NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=profile&id=%@&access_token=%@&device_id=%@&image=true",user_id,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
        
        DebugLog(@"profile url: %@",urlString1);
        
        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
        
        if (signeddataURL1 != nil)
        {
            json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                    options:kNilOptions
                                                      error:&error];
            DebugLog(@"PROFILE JSON:%@",json1);
        }
        else
        {
            DebugLog(@"no connnn profile");
            
            //            [self performSelectorOnMainThread:@selector(stopLoader) withObject:nil waitUntilDone:NO];
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                [self servererror];
                //                alert = [[UIAlertView alloc] initWithTitle:@"Error in Profile Server Connection!"
                //                                                   message:nil
                //                                                  delegate:self
                //                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                //                [alert show];
                
            }];
            //            [self performSelectorOnMainThread:@selector(servererror) withObject:nil waitUntilDone:YES];
        }
        
        NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
        DebugLog(@"err  %@",errornumber);
        
        if (![errornumber isEqualToString:@"0"])
        {
            [act removeFromSuperview];
            NSString *err_str = [json1 objectForKey:@"error"];
            alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                               message:err_str
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            //[alert show];
        }
        
        else
            
        {
            
            dict_profile = [[NSMutableDictionary alloc]init];
            
            _profdict= [json1 objectForKey:@"details"];
            
            dict_profile = [_profdict objectForKey:@"profile"];
            
            [self performSelectorOnMainThread:@selector(stopLoader) withObject:nil waitUntilDone:YES];
            
            [self performSelectorOnMainThread:@selector(startDBInserta:)
                                   withObject:nil
                                waitUntilDone:YES];
            
            //        [[DBManager getSharedInstance]insertProfile:profdict :[user_id intValue]];
            
            
            
            [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"firstProfile"];
            DebugLog(@"dict pro%@",dict_profile);
            
            //            if (![[NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]] isKindOfClass:[NSNull class]] && ![[NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]] isEqualToString:@""] && ![[NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]] isEqualToString:@"(null)"] && [[NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]] length]>0) {
            //
            //                fullname= [NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]];
            //                prof_name.text = fullname;
            //
            //            }
            
            //            [prof_name removeFromSuperview];
            
            base64String= [_profdict objectForKey:@"image"];
            
            if ([user_id intValue] == [[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]intValue])
            {
                switchprof=0;
                //[self loadPersonalViews];
            }
            else
            {
                if ([[_profdict objectForKey:@"friend"]intValue] == 1)
                {
                    switchprof=0;
                    // [self loadPersonalViews];
                }
                
                else if ([[dict_profile objectForKey:@"business"] intValue]==1)
                {
                    switchprof=1;
                    allowpersonal=1;
                    business_con = TRUE;
                    [self loadBusinessView];
                }
                DebugLog(@"fields are: %d , %d",[[_profdict objectForKey:@"own"]intValue], switchprof);
            }
            
            [self performSelectorOnMainThread:@selector(displaydata) withObject:nil waitUntilDone:NO];
        }
        
    }
    
    
    /////////////////////////////////////////////   Load from Local DB   ////////////////////////////////////////////////////////////////////
    
    else
    {
        
        allowpersonal=0;
        DebugLog(@"eittttto etatei ");
        NSDictionary *localDBProfile = [[NSDictionary alloc]init];
        
        localDBProfile = [[DBManager getSharedInstance]fetchProfile:[user_id intValue]];
        
        DebugLog(@"this gives: %@",localDBProfile);
        DebugLog(@"BUSINESS BUSINESS:%@",[localDBProfile objectForKey:@"business"]);
        if ([[localDBProfile objectForKey:@"business"] intValue]==1) {
            map_business_con = TRUE;
        }
        
        NSArray * allKeys = [localDBProfile allKeys];
        DebugLog(@"Count : %lu", (unsigned long)[allKeys count]);
        
        if ([allKeys count] == 0 || [[localDBProfile objectForKey:@"name"] isKindOfClass:[NSNull class]] || [[localDBProfile objectForKey:@"name"] length]==0 || [[localDBProfile objectForKey:@"name"] isEqualToString:@"(null)"])
        {
            DebugLog(@"this gives null profile");
            
            [self reloadDataFromWeb];
        }
        else
        {
            
            
            
            if ([user_id intValue] == [[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"] intValue] && [[[NSUserDefaults standardUserDefaults]objectForKey:@"profile_edited"] isEqualToString:@"yes"])
                
                // [self reloadDataFromWeb];
                
                dict_profile = [[NSMutableDictionary alloc]init];
            dict_profile = [localDBProfile mutableCopy];
            
            [self performSelectorOnMainThread:@selector(stopLoader) withObject:nil waitUntilDone:YES];
            
            //            UIImageView *prof_img = [[UIImageView alloc]initWithFrame:CGRectMake(15, 75-64, 90, 90)];
            //            [grayback addSubview:prof_img];
            [self performSelector:@selector(reloadDataFromWeb)];
            
        }
        
        if (![[NSUserDefaults standardUserDefaults] boolForKey:@"error"])
        {
            fullname= [NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]];
            if ([_profdict objectForKey:@"image"] == nil) {
                base64String= [_profdict objectForKey:@"thumb"];
            }
            else
                base64String= [_profdict objectForKey:@"image"];
            //            grayback = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 110.0f)];
            //            [grayback setBackgroundColor:[UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f]];
            //            [mainscroll addSubview:grayback];
            //            prof_name.frame = CGRectMake(0, 0, 0, 0);
            //            [grayback addSubview:prof_name];
            [self performSelectorOnMainThread:@selector(displaydata) withObject:nil waitUntilDone:YES];
        }
        else
        {
            [act removeFromSuperview];
            UIAlertView *showalert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Cannot load data. Please log in again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [showalert show];
        }
        
    }
    
    
}

-(void)displaydata
{
    [self performSelectorOnMainThread:@selector(stopLoader) withObject:nil waitUntilDone:NO];
    
    UIImageView *fb_img, *google_img, *link_img, *inst_img, *pin_img, *skype_img, *vimeo_img, *twitter_img, *soundcloud_img, *youtube_img;
    fb_img = [[UIImageView alloc] init];
    google_img = [[UIImageView alloc] init];
    link_img = [[UIImageView alloc] init];
    inst_img = [[UIImageView alloc] init];
    pin_img = [[UIImageView alloc] init];
    skype_img = [[UIImageView alloc] init];
    vimeo_img = [[UIImageView alloc] init];
    twitter_img = [[UIImageView alloc] init];
    soundcloud_img = [[UIImageView alloc] init];
    youtube_img = [[UIImageView alloc] init];
    int fb, google, inst, link, pint, sky, sound, twt, vim, you,count=0;
    // float upperView_y=21.5, lowerView_y=75;
    DebugLog(@"DISPLAY DATA:%@",dict_profile);
    [act removeFromSuperview];
    
    if (map) {
        
        
        if ([base64String length] > 6)
        {
            NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
            profilepic = [UIImage imageWithData:decodedData];
            
            profile_image.image = profilepic;
        }
        
        if (![[dict_profile objectForKey:@"name"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"name"]isEqualToString:@""] && ![[dict_profile objectForKey:@"name"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"name"] length]>0 && ![[dict_profile objectForKey:@"surname"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"surname"]isEqualToString:@""] && ![[dict_profile objectForKey:@"surname"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"surname"] length]>0)
            
        {
            
            
            
            fullname= [NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]];
            
            prof_name.text = fullname;
            
            DebugLog(@"Profile Name:%@",prof_name.text);
            
            
            
        }
        
        
        
        else
            
        {
            
            //prof_name.text=[NSString stringWithFormat:@"%@ %@",[dict_profile objectForKey:@"name"],[dict_profile objectForKey:@"surname"]];
            
            //            prof_name.text = @"";
            
            DebugLog(@"PROFILE NAME:%@",[dict_profile objectForKey:@"name"]);
            
        }
        
        if (map_business_con) {
            
            
            [self loadBusinessView];
            socialView.frame = CGRectMake(0,b_addressView.frame.origin.y+b_addressView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 118);
            socialView.backgroundColor = [UIColor clearColor];
            [scrollView addSubview:socialView];
            
        }
        else
        {
            if ([dict_profile objectForKey:@"city"] != false && ![[dict_profile objectForKey:@"city"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"city"] length] > 1 && ![[dict_profile objectForKey:@"city"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"city"] isEqualToString:@""] && [dict_profile objectForKey:@"country"] != false && ![[dict_profile objectForKey:@"country"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"country"] length] > 1 && ![[dict_profile objectForKey:@"country"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"country"] isEqualToString:@""]) {
                
                UIImageView *loc_img = [[UIImageView alloc] init];
                loc_img.frame = CGRectMake(0, 8, 7, 9);
                loc_img.image = [UIImage imageNamed:@"Loction"];
                [locationView addSubview:loc_img];
                location.text = [[NSString stringWithFormat:@"%@, %@",[dict_profile objectForKey:@"city"],[dict_profile objectForKey:@"country"]] uppercaseString];
                
            }
            
            
            if ([dict_profile objectForKey:@"mobile_num"] != false && ![[dict_profile objectForKey:@"mobile_num"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"mobile_num"] length] > 1 && ![[dict_profile objectForKey:@"mobile_num"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"mobile_num"] isEqualToString:@""])
            {
                mobileView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 62);
                mobileView.backgroundColor = [UIColor clearColor];
                [scrollView addSubview:mobileView];
                
                UILabel *mobileView_div = [[UILabel alloc] init];
                mobileView_div.frame = CGRectMake(0, 61.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
                mobileView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
                [mobileView addSubview:mobileView_div];
                
                UILabel *mobileLabel = [[UILabel alloc] init];
                mobileLabel.frame = CGRectMake(20, 10, 100, 22);
                mobileLabel.text = @"MOBILE";
                mobileLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
                mobileLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
                [mobileView addSubview:mobileLabel];
                
                mobile_num = [[UILabel alloc] init];
                mobile_num.frame = CGRectMake(20, 32, 150, 22);
                //mobile_num.text = [NSString stringWithFormat:@"%@ %@",[_Condetails_dict objectForKey:@"mobile_pre"],[_Condetails_dict objectForKey:@"mobile_num"]];
                mobile_num.textColor = [UIColor blackColor];
                mobile_num.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
                [mobileView addSubview:mobile_num];
                
                UIImageView *whatsapp_img = [[UIImageView alloc] init];
                whatsapp_img.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-50, 20, 22, 22);
                whatsapp_img.image = [UIImage imageNamed:@"Whatsapp"];
              //  [mobileView addSubview:whatsapp_img];
                
                UIButton *whatsapp_btn = [[UIButton alloc] init];
                whatsapp_btn.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-65, 10, 52, 42);
                [whatsapp_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
                [whatsapp_btn setBackgroundColor:[UIColor clearColor]];
                [whatsapp_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
                [whatsapp_btn addTarget:self action:@selector(whatsapp_func) forControlEvents:UIControlEventTouchUpInside];
              //  [mobileView addSubview:whatsapp_btn];
                
                
                UIButton *mobileView_btn = [[UIButton alloc] init];
                mobileView_btn.tag = 1;
                mobileView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-65, 62);
                [mobileView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
                [mobileView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
                [mobileView_btn addTarget:self action:@selector(phonefunc:) forControlEvents:UIControlEventTouchUpInside];
                [mobileView addSubview:mobileView_btn];
                
                mobile_num.text = [NSString stringWithFormat:@"%@ %@",[dict_profile objectForKey:@"mobile_pre"],[dict_profile objectForKey:@"mobile_num"]];
                [_phonearr addObject:mobile_num.text];
                
                mobile_num.text = [NSString stringWithFormat:@"%@ %@",[dict_profile objectForKey:@"mobile_pre"],[dict_profile objectForKey:@"mobile_num"]];
                [_phonearr addObject:[NSString stringWithFormat:@"%@ %@",[dict_profile objectForKey:@"mobile_pre"],[dict_profile objectForKey:@"mobile_num"]]];
                
            }
            if ([dict_profile objectForKey:@"phone_num"] != false && ![[dict_profile objectForKey:@"phone_num"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"phone_num"] length] > 1 && ![[dict_profile objectForKey:@"phone_num"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"phone_num"] isEqualToString:@""]) {
                
                landphoneView.frame = CGRectMake(0, mobileView.frame.origin.y+mobileView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 62);
                landphoneView.backgroundColor = [UIColor clearColor];
                [scrollView addSubview:landphoneView];
                
                UILabel *landphoneView_div = [[UILabel alloc] init];
                landphoneView_div.frame = CGRectMake(0, 61.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
                landphoneView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
                [landphoneView addSubview:landphoneView_div];
                
                UILabel *landLabel = [[UILabel alloc] init];
                landLabel.frame = CGRectMake(20, 10, 100, 22);
                landLabel.text = @"HOME";
                landLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
                landLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
                [landphoneView addSubview:landLabel];
                
                
                //[landphonelb addTarget:self action:@selector(landphoneFun:) forControlEvents:UIControlEventTouchUpInside];
                land_num = [[UILabel alloc] init];
                land_num.frame = CGRectMake(20, 32, 180, 22);
                land_num.text = [NSString stringWithFormat:@"%@ %@",[dict_profile objectForKey:@"phone_pre"],[dict_profile objectForKey:@"phone_num"]];
                land_num.textColor = [UIColor blackColor];
                land_num.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
                [landphoneView addSubview:land_num];
                
                [_phonearr addObject:[NSString stringWithFormat:@"%@ %@",[dict_profile objectForKey:@"phone_pre"],[dict_profile objectForKey:@"phone_num"]]];
                
                UIButton *landView_btn = [[UIButton alloc] init];
                if ([_phonearr count]==2) {
                    landView_btn.tag = 2;
                }
                else{
                    landView_btn.tag = 1;
                }
                
                landView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 62);
                [landView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
                [landView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
                [landView_btn addTarget:self action:@selector(phonefunc:) forControlEvents:UIControlEventTouchUpInside];
                [landphoneView addSubview:landView_btn];
                
            }
            else
            {
                landphoneView.frame = CGRectMake(0, mobileView.frame.origin.y+mobileView.frame.size.height, 0,0);
            }
            
            
            
            if ([dict_profile objectForKey:@"email"] != false && ![[dict_profile objectForKey:@"email"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"email"] length] > 1 && ![[dict_profile objectForKey:@"email"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"email"] isEqualToString:@""]) {
                
                emailView = [[UIView alloc] init];
                emailView.frame = CGRectMake(0, landphoneView.frame.origin.y+landphoneView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 62);
                emailView.backgroundColor = [UIColor clearColor];
                [scrollView addSubview:emailView];
                
                UILabel *emailView_div = [[UILabel alloc] init];
                emailView_div.frame = CGRectMake(0, 61.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
                emailView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
                [emailView addSubview:emailView_div];
                
                UILabel *emailLabel = [[UILabel alloc] init];
                emailLabel.frame = CGRectMake(20, 10, 100, 22);
                emailLabel.text = @"EMAIL";
                emailLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
                emailLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
                [emailView addSubview:emailLabel];
                
                emailId = [[UILabel alloc] init];
                emailId.frame = CGRectMake(20, 32, [UIScreen mainScreen].bounds.size.width, 22);
                //emailId.text = [NSString stringWithFormat:@"%@",[_Condetails_dict objectForKey:@"email"]];
                emailId.textColor = [UIColor blackColor];
                emailId.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
                [emailView addSubview:emailId];
                mail = emailId.text;
                
                UIButton *emailView_btn = [[UIButton alloc] init];
                emailView_btn.tag = 2;
                emailView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 62);
                [emailView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
                [emailView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
                [emailView_btn addTarget:self action:@selector(mailFun:) forControlEvents:UIControlEventTouchUpInside];
                [emailView addSubview:emailView_btn];
                
                emailId.text = [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"email"]];
                mail = emailId.text;
            }
            
            
            /////////////////////////////////////////////// WEBSITE /////////////////////////////////////////
            
            websiteView = [[UIView alloc] init];
            
            if ([dict_profile objectForKey:@"website"] != false && ![[dict_profile objectForKey:@"website"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"website"] length] > 1 && ![[dict_profile objectForKey:@"website"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"website"] isEqualToString:@""]) {
                
                websiteView.frame = CGRectMake(0, emailView.frame.origin.y+emailView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 62);
                websiteView.backgroundColor = [UIColor clearColor];
                [scrollView addSubview:websiteView];
                
                UILabel *websiteView_div = [[UILabel alloc] init];
                websiteView_div.frame = CGRectMake(0, 61.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
                websiteView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
                [websiteView addSubview:websiteView_div];
                
                UILabel *websiteLabel = [[UILabel alloc] init];
                websiteLabel.frame = CGRectMake(20, 10, 100, 22);
                websiteLabel.text = @"WEBSITE";
                websiteLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
                websiteLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
                [websiteView addSubview:websiteLabel];
                
                
                //[landphonelb addTarget:self action:@selector(landphoneFun:) forControlEvents:UIControlEventTouchUpInside];
                
                website = [[UILabel alloc] init];
                website.frame = CGRectMake(20, 32, 180, 22);
                website.text = [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"website"]];
                website.textColor = [UIColor blackColor];
                website.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
                [websiteView addSubview:website];
                
                UIButton *websiteView_btn = [[UIButton alloc] init];
                websiteView_btn.tag = 1;
                websiteView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 62);
                [websiteView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
                [websiteView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
                [websiteView_btn addTarget:self action:@selector(websiteFun:) forControlEvents:UIControlEventTouchUpInside];
                [websiteView addSubview:websiteView_btn];
                
            }
            else
            {
                websiteView.frame = CGRectMake(0, emailView.frame.origin.y+emailView.frame.size.height, 0,0);
            }
            
            
            
            
            
            //////////////////////////////////////////////////////////////////////////////////////////////////
            
            
            //    [address sizeToFit];
            
            if ([dict_profile objectForKey:@"street"] != false && ![[dict_profile objectForKey:@"street"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"street"] length] > 1 && ![[dict_profile objectForKey:@"street"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"street"] isEqualToString:@""])
            {
                
                addressView = [[UIView alloc] init];
                addressView.frame = CGRectMake(0, websiteView.frame.origin.y+websiteView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 106);
                addressView.backgroundColor = [UIColor clearColor];
                [scrollView addSubview:addressView];
                
                addressView_div = [[UILabel alloc] init];
                addressView_div.frame = CGRectMake(0, 105.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
                addressView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
                [addressView addSubview:addressView_div];
                
                UILabel *addressLabel = [[UILabel alloc] init];
                addressLabel.frame = CGRectMake(20, 10, 100, 22);
                addressLabel.text = @"ADDRESS";
                addressLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
                addressLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
                [addressView addSubview:addressLabel];
                
                [self performSelectorOnMainThread:@selector(displaydataloadtextview) withObject:nil waitUntilDone:YES];
                
                UIButton *addressView_btn = [[UIButton alloc] init];
                addressView_btn.tag = 2;
                addressView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 106);
                [addressView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
                [addressView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
                [addressView_btn addTarget:self action:@selector(openmap:) forControlEvents:UIControlEventTouchUpInside];
                [addressView addSubview:addressView_btn];
                
            }
            
            socialView.frame = CGRectMake(0,addressView.frame.origin.y+addressView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 118);
            socialView.backgroundColor = [UIColor clearColor];
            [scrollView addSubview:socialView];
            
        }
        
    }
    else
    {
        
        if (business_con) {
            
            if ([dict_profile objectForKey:@"b_city"] != false && ![[dict_profile objectForKey:@"b_city"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_city"] length] > 1 && ![[dict_profile objectForKey:@"b_city"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"b_city"] isEqualToString:@""] && [dict_profile objectForKey:@"b_country"] != false && ![[dict_profile objectForKey:@"_country"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_country"] length] > 1 && ![[dict_profile objectForKey:@"b_country"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"b_bcountry"] isEqualToString:@""]) {
                
                UIImageView *loc_img = [[UIImageView alloc] init];
                loc_img.frame = CGRectMake(0, 8, 7, 9);
                loc_img.image = [UIImage imageNamed:@"Loction"];
                [locationView addSubview:loc_img];
                location.text = [[NSString stringWithFormat:@"%@, %@",[dict_profile objectForKey:@"b_city"],[dict_profile objectForKey:@"b_country"]] uppercaseString];
                
            }
            
            [self loadBusinessView];
            socialView.frame = CGRectMake(0,b_addressView.frame.origin.y+b_addressView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 118);
            socialView.backgroundColor = [UIColor clearColor];
            [scrollView addSubview:socialView];
        }
        else
        {
            if ([base64String length] > 6)
            {
                NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
                profilepic = [UIImage imageWithData:decodedData];
                
                profile_image.image = profilepic;
            }
            
            socialView.frame = CGRectMake(0,addressView.frame.origin.y+addressView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 118);
            socialView.backgroundColor = [UIColor clearColor];
            [scrollView addSubview:socialView];
        }
        
        
        
    }
    
    [act removeFromSuperview];
    
    
    
    
    if ([dict_profile objectForKey:@"s_facebook"] != false && ![[dict_profile objectForKey:@"s_facebook"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"s_facebook"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_facebook"] isEqualToString:@""]) {
        
        fb = 1;
        [socialImage addObject:@"Facebook"];
        
        count++;
    }
    
    if ([dict_profile objectForKey:@"s_google"] != false && ![[dict_profile objectForKey:@"s_google"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"s_google"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_google"] isEqualToString:@""]) {
        
        google = 1;
        [socialImage addObject:@"Google+"];
        count++;
        
    }
    if ([dict_profile objectForKey:@"s_linkedin"] != false && ![[dict_profile objectForKey:@"s_linkedin"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"s_linkedin"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_linkedin"] isEqualToString:@""]) {
        
        link = 1;
        [socialImage addObject:@"Linkedin"];
        count++;
    }
    if ([dict_profile objectForKey:@"s_instagram"] != false && ![[dict_profile objectForKey:@"s_instagram"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"s_facebook"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_instagram"] isEqualToString:@""]) {
        
        inst = 1;
        [socialImage addObject:@"Instagram"];
        count++;
    }
    if ([dict_profile objectForKey:@"s_pinterest"] != false && ![[dict_profile objectForKey:@"s_pinterest"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"s_pinterest"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_pinterest"] isEqualToString:@""]) {
        
        pint = 1;
        [socialImage addObject:@"Pinterest"];
        count++;
    }
    if ([dict_profile objectForKey:@"s_skype"] != false && ![[dict_profile objectForKey:@"s_skype"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"s_skype"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_skype"] isEqualToString:@""]) {
        
        sky = 1;
        [socialImage addObject:@"Skype"];
        count++;
    }
    if ([dict_profile objectForKey:@"s_vimeo"] != false && ![[dict_profile objectForKey:@"s_vimeo"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"s_vimeo"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_vimeo"] isEqualToString:@""]) {
        
        vim = 1;
        [socialImage addObject:@"Vimeo"];
        count++;
    }
    
    if ([dict_profile objectForKey:@"s_twitter"] != false && ![[dict_profile objectForKey:@"s_twitter"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"s_twitter"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_twitter"] isEqualToString:@""]) {
        
        twt = 1;
        [socialImage addObject:@"Twitter"];
        count++;
    }
    
    if ([dict_profile objectForKey:@"s_soundcloud"] != false && ![[dict_profile objectForKey:@"s_soundcloud"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"s_soundcloud"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_soundcloud"] isEqualToString:@""]) {
        
        sound = 1;
        [socialImage addObject:@"Soundcloud"];
        
        count++;
    }
    if ([dict_profile objectForKey:@"s_youtube"] != false && ![[dict_profile objectForKey:@"s_youtube"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"s_youtube"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_youtube"] isEqualToString:@""]) {
        
        you = 1;
        [socialImage addObject:@"YouTube"];
        count++;
    }
    
    float x_axis = 18*(float)([UIScreen mainScreen].bounds.size.width/320), y_axis;
    for (int i=0; i<[socialImage count]; i++) {
        
        if (i<=5) {
            y_axis = 18;
        }
        else
        {
            y_axis = 68;
        }
        DebugLog(@"IMAGE:%@",[socialImage objectAtIndex:i]);
        UIImageView *social_Imgview = [[UIImageView alloc] init];
        //social_Imgview.backgroundColor = [UIColor greenColor];
        social_Imgview.frame = CGRectMake(x_axis, y_axis, 32, 32);
        social_Imgview.image = [UIImage imageNamed:[socialImage objectAtIndex:i]];
        [socialView addSubview:social_Imgview];
        
        UIButton *social_btn = [[UIButton alloc] init];
        social_btn.frame = CGRectMake(social_Imgview.frame.origin.x-5, social_Imgview.frame.origin.y-5, social_Imgview.frame.size.width+10, social_Imgview.frame.size.height+10);
        social_btn.tag = i;
        [social_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [social_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
        [social_btn addTarget:self action:@selector(socialFunc:) forControlEvents:UIControlEventTouchUpInside];
        [socialView addSubview:social_btn];
        if (i==5) {
            
            x_axis = 18*(float)([UIScreen mainScreen].bounds.size.width/320);
        }
        else{
            x_axis = social_Imgview.frame.origin.x + social_Imgview.frame.size.width+18*(float)([UIScreen mainScreen].bounds.size.width/320);
        }
    }
    
    
    DebugLog(@"SOCIAL MEDIA:%@",socialImage);
    [act removeFromSuperview];
    [self performSelectorOnMainThread:@selector(stopLoader) withObject:nil waitUntilDone:NO];
}


-(void)displaydataloadtextview
{
    @try {
        address = [[UITextView alloc] initWithFrame:CGRectMake(15, 32, addressView.frame.size.width-15, 70)];
        //            address.frame = ;
        address.backgroundColor = [UIColor clearColor];
        
        address.editable = NO;
        address.textColor = [UIColor blackColor];
        address.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
        [addressView addSubview:address];
        NSString *addressValue = [NSString stringWithFormat:@"%@<br>%@ %@<br>%@",[dict_profile objectForKey:@"street"],[dict_profile objectForKey:@"zipcode"],[dict_profile objectForKey:@"city"],[dict_profile objectForKey:@"country"]];
        
        [address setValue:addressValue forKey:@"contentToHTMLString"];
        address.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}

-(void)socialFunc:(UIButton *)sender
{
    if ([[socialImage objectAtIndex:sender.tag] isEqualToString:@"Facebook"]) {
        [self facebookfunc];
    }
    if ([[socialImage objectAtIndex:sender.tag] isEqualToString:@"Google+"]) {
        [self gplusfunc];
    }
    if ([[socialImage objectAtIndex:sender.tag] isEqualToString:@"Linkedin"]) {
        [self linkedinfunc];
    }
    if ([[socialImage objectAtIndex:sender.tag] isEqualToString:@"Instagram"]) {
        [self instagramfunc];
    }
    if ([[socialImage objectAtIndex:sender.tag] isEqualToString:@"Pinterest"]) {
        [self pinterestfunc];
    }
    if ([[socialImage objectAtIndex:sender.tag] isEqualToString:@"Skype"]) {
        [self skypefunc];
    }
    if ([[socialImage objectAtIndex:sender.tag] isEqualToString:@"Vimeo"]) {
        [self vimeofunc];
    }
    if ([[socialImage objectAtIndex:sender.tag] isEqualToString:@"Twitter"]) {
        [self twitterfunc];
    }
    if ([[socialImage objectAtIndex:sender.tag] isEqualToString:@"Soundcloud"]) {
        [self soundcloudfunc];
    }
    if ([[socialImage objectAtIndex:sender.tag] isEqualToString:@"YouTube"]) {
        [self youtubefunc];
    }
}


-(void)phone_color
{
    call_img.alpha = 0.5f;
    //call_btn.alpha = 0.5f;
    // [call_btn setBackgroundColor:[UIColor grayColor]];
    
}
-(void)phone_colorAgain
{
    call_img.alpha = 1.0f;
    // call_btn.alpha = 1.0f;
    // [call_btn setBackgroundColor:[UIColor clearColor]];
}
-(void)sms_color
{
    sms_img.alpha = 0.5f;
    //sms_btn.alpha = 0.5f;
    //[sms_btn setBackgroundColor:[UIColor grayColor]];
}
-(void)sms_colorAgain
{
    sms_img.alpha = 1.0f;
    //sms_btn.alpha = 1.0f;
    //[sms_btn setBackgroundColor:[UIColor clearColor]];
}
-(void)email_color
{
    email_img.alpha = 0.5f;
    //email_btn.alpha = 0.5f;
    //[email_btn setBackgroundColor:[UIColor grayColor]];
}
-(void)email_colorAgain
{
    email_img.alpha = 1.0f;
    // email_btn.alpha = 1.0f;
    //[email_btn setBackgroundColor:[UIColor clearColor]];
}
-(void)loc_color
{
    location_img.alpha = 0.5f;
    //location_btn.alpha = 0.5f;
    //[location_btn setBackgroundColor:[UIColor grayColor]];
}
-(void)loc_colorAgain
{
    location_img.alpha = 1.0f;
    //location_btn.alpha = 1.0f;
    // [location_btn setBackgroundColor:[UIColor clearColor]];
}

-(void)changecoloragain
{
    backlbl.alpha = 1.0f;
    labelHead.alpha = 1.0f;
    bck_img.alpha = 1.0f;
}
-(void)changecolor
{
    backlbl.alpha = 0.5f;
    labelHead.alpha = 1.0f;
    bck_img.alpha = 0.5f;
    
}

-(void)changecoloragain1
{
    DebugLog(@"CHANGE COLOR");
    labelHead.alpha = 1.0f;
    bck_img.alpha = 1.0f;
}

-(void)changecolor1
{
    DebugLog(@"CHANGE COLOR1");
    labelHead.alpha = 0.5f;
    bck_img.alpha = 0.5f;
    
}

-(void)gobackoption
{
    labelHead.alpha = 0.5f;
    bck_img.alpha = 0.5f;
    
    
    //    CATransition *transition = [CATransition animation];
    //
    //    transition.duration = 0.4f;
    //
    //    transition.type = kCATransitionFade;
    //
    //    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    [self.navigationController popViewControllerAnimated:YES];
    //[self.navigationController popToViewController:[navarr objectAtIndex:index] animated:YES];
    
}

-(void)startDBInserta:(NSNotification *)notification
{
    
    if (profile_insertion == 0)
    {
        profile_insertion =1;
        DebugLog(@"startdbinserta called");
        [[DBManager getSharedInstance]insertProfile:_profdict :[user_id intValue]];
    }
}

-(void) reloadDataFromWeb
{
    DebugLog(@"reload profile from web");
    prefs = [NSUserDefaults standardUserDefaults];
    [mainview addSubview:act];
    act.center= self.view.center;
    act.tintColor=[UIColor grayColor];
    [act startAnimating];
    
    //    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    NSString *errornumber;
    
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=profile&id=%@&access_token=%@&device_id=%@&image=true",user_id,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
    
    DebugLog(@"profile url: %@",urlString1);
    
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL11a =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    
    if (signeddataURL11a == nil)
    {
        DebugLog(@"no connnn profile bm");
        
        
        [self performSelectorOnMainThread:@selector(servererror) withObject:nil waitUntilDone:YES];
        [self reloadFromLocalDB];
    }
    //    DebugLog(@"json returns: %@",json1);
    else
    {
        //            [alert show];
        
        
        NSError *error;
        json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL11a //1
                 
                                                options:kNilOptions
                 
                                                  error:&error];
        errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
        
        DebugLog(@"err  %@",errornumber);
        
        
        if (![errornumber isEqualToString:@"0"])
        {
            [self performSelectorOnMainThread:@selector(stopLoader) withObject:nil waitUntilDone:YES];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"error"];
            [act removeFromSuperview];
            NSString *err_str = [json1 objectForKey:@"error"];
            
            alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                     
                                               message:err_str
                     
                                              delegate:self
                     
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            //            [alert show];
        }
        else
        {
            dict_profile = [NSMutableDictionary new];
            
            _profdict = [NSMutableDictionary new];
            //
            //            dict_profile = [NSMutableDictionary new];
            
            
            
            _profdict = [json1 objectForKey:@"details"];
            
            dict_profile = [_profdict objectForKey:@"profile"];
            
            [self performSelectorOnMainThread:@selector(stopLoader) withObject:nil waitUntilDone:YES];
            
            if ([user_id intValue] == [[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]intValue])
                switchprof=0;
            else
            {
                if ([[dict_profile objectForKey:@"friend"]intValue] == 1)
                {
                    switchprof=0;
                }
                
                else if ([[dict_profile objectForKey:@"business"] intValue]==1)
                {
                    switchprof=1;
                }
                DebugLog(@"fields are: %d , %d",[[dict_profile objectForKey:@"own"]intValue], switchprof);
            }
        }
        
        
    }
    //        dispatch_async(dispatch_get_main_queue(), ^{
    
    if (signeddataURL11a != nil && [errornumber isEqualToString:@"0"] && [dict_profile count] > 0)
    {
        [[DBManager getSharedInstance]insertProfile:_profdict :[user_id intValue]];
        
    }
    
    
}

-(void) reloadFromLocalDB
{
    [act removeFromSuperview];
    
    NSDictionary *localDBProfile = [[NSDictionary alloc]init];
    
    localDBProfile = [[DBManager getSharedInstance]fetchProfile:[user_id intValue]];
    
    DebugLog(@"this gives: %@",localDBProfile);
    
    if ([localDBProfile isKindOfClass:[NSNull class]] || localDBProfile == (id)[NSNull null])
    {
        //        [self reloadDataFromWeb];
        [self performSelectorOnMainThread:@selector(servererror) withObject:nil waitUntilDone:YES];
        
    }
    else{
        
        dict_profile = [[NSMutableDictionary alloc]init];
        
        dict_profile = [[NSMutableDictionary alloc]init];
        
        dict_profile = [[NSMutableDictionary alloc]init];
        
        dict_profile = [localDBProfile mutableCopy];
        _profdict = [dict_profile mutableCopy];
        
        [self performSelectorOnMainThread:@selector(stopLoader) withObject:nil waitUntilDone:YES];
        NSString *fullname1;
        
        if (![[NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]] isKindOfClass:[NSNull class]] && ![[NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]] isEqualToString:@""] && ![[NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]] isEqualToString:@"(null)"] && [[NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]] length]>0)
        {
            
            fullname1= [NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]];
            prof_name.text = fullname1;
        }
        
        if ([fullname1 isKindOfClass:[NSNull class]] || [fullname1 isEqualToString:@""] || [fullname1 isEqualToString:@"(null) (null)"] || [fullname1 length]==0) {
            
            if (![_Condetails_dict isKindOfClass:[NSNull class]])
            {
                if (![[_Condetails_dict objectForKey:@"name"] isKindOfClass:[NSNull class]] && ![[_Condetails_dict objectForKey:@"name"] isEqualToString:@""] && ![[_Condetails_dict objectForKey:@"name"] isEqualToString:@"(null) (null)"] && ![[_Condetails_dict objectForKey:@"name"] length]==0)
                {
                    fullname1= [NSString stringWithFormat:@"%@ %@",[[_Condetails_dict objectForKey:@"name"] capitalizedString],[[_Condetails_dict objectForKey:@"surname"] capitalizedString]];
                    prof_name.text = fullname1;
                }
                else
                    prof_name.text = @"";
            }
            else
                prof_name.text = @"";
            
            DebugLog(@"PROFILE NAME1:%@",prof_name.text);
            
        }
        else
        {
            prof_name.text=fullname1;
        }
        
        base64String= [localDBProfile objectForKey:@"thumb"];
        
        if ([base64String length] >6)
        {
            NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
            
            //    NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
            
            profilepic = [UIImage imageWithData:decodedData];
            
            profile_image.image=profilepic;
            
            profile_image.contentMode= UIViewContentModeScaleAspectFill;
            
            profile_image.clipsToBounds=YES;
        }
        
        profile_image.userInteractionEnabled=YES;
        
        UITapGestureRecognizer *propictap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(detailpic:)];
        
        [profile_image addGestureRecognizer:propictap];
        
        if ([user_id intValue] == [[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]intValue])
        {
            switchprof=0;
            // [self loadPersonalViews];
        }
        else
        {
            if ([[dict_profile objectForKey:@"friend"]intValue] == 1)
            {
                switchprof=0;
                //[self loadPersonalViews];
            }
            
            else if ([[dict_profile objectForKey:@"business"] intValue]==1)
            {
                switchprof=1;
                [self loadBusinessView];
            }
            DebugLog(@"fields are: %d , %d",[[dict_profile objectForKey:@"own"]intValue], switchprof);
        }
        
    }
    
}

-(void)loadPersonalViews
{
    [act removeFromSuperview];
    [b_companyView removeFromSuperview];
    [designationView removeFromSuperview];
    [b_phoneView removeFromSuperview];
    [b_emailView removeFromSuperview];
    [b_websiteView removeFromSuperview];
    [b_addressView removeFromSuperview];
    [socialView removeFromSuperview];
    
    [mobileView removeFromSuperview];
    [landphoneView removeFromSuperview];
    [emailView removeFromSuperview];
    [websiteView removeFromSuperview];
    [addressView removeFromSuperview];
    [self viewDidAppear:YES];
    
}

-(void)loadBusinessView
{
    [mobileView removeFromSuperview];
    [landphoneView removeFromSuperview];
    [emailView removeFromSuperview];
    [addressView removeFromSuperview];
    [websiteView removeFromSuperview];
    //[socialView removeFromSuperview];
    
    
    
    _phonearr=[[NSMutableArray alloc]init];
    
    [act removeFromSuperview];
    
    b_companyView = [[UIView alloc] init];
    
    
    
    base64String= [_profdict objectForKey:@"image"];
    if ([base64String length] > 6)
    {
        NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
        profilepic = [UIImage imageWithData:decodedData];
        
        profile_image.image = profilepic;
    }
    
    
    if ([dict_profile objectForKey:@"b_city"] != false && ![[dict_profile objectForKey:@"b_city"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_city"] length] > 1 && ![[dict_profile objectForKey:@"b_city"] isEqualToString:@"(null)"] && [dict_profile objectForKey:@"b_country"] != false && ![[dict_profile objectForKey:@"b_country"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_country"] length] > 1 && ![[dict_profile objectForKey:@"b_country"] isEqualToString:@"(null)"])
    {
        UIImageView *loc_img = [[UIImageView alloc] init];
        loc_img.frame = CGRectMake(0, 8, 7, 9);
        loc_img.image = [UIImage imageNamed:@"Loction"];
        [locationView addSubview:loc_img];
        DebugLog(@"IN LOAD BUSINESS:%@",[[NSString stringWithFormat:@"%@, %@",[dict_profile objectForKey:@"b_city"],[dict_profile objectForKey:@"b_country"]] uppercaseString]);
        
        location.text = [[NSString stringWithFormat:@"%@, %@",[dict_profile objectForKey:@"b_city"],[dict_profile objectForKey:@"b_country"]] uppercaseString];
        
    }
    
    
    
    if ([dict_profile objectForKey:@"b_company"] != false && ![[dict_profile objectForKey:@"b_company"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_company"] length] > 1 && ![[dict_profile objectForKey:@"b_company"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"b_company"] isEqualToString:@""] && ![[dict_profile objectForKey:@"b_company"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"b_company"] isEqual:[NSNull class]])
    {
        b_companyView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 62);
        b_companyView.backgroundColor = [UIColor clearColor];
        [scrollView addSubview:b_companyView];
        
        
        UILabel *b_companyView_div = [[UILabel alloc] init];
        b_companyView_div.frame = CGRectMake(0, 61.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
        b_companyView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        [b_companyView addSubview:b_companyView_div];
        
        UILabel *companyLabel = [[UILabel alloc] init];
        companyLabel.frame = CGRectMake(20, 10, 100, 22);
        companyLabel.text = @"COMPANY";
        companyLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        companyLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
        [b_companyView addSubview:companyLabel];
        
        UILabel *company_name = [[UILabel alloc] init];
        company_name.frame = CGRectMake(20, 32, 150, 22);
        company_name.textColor = [UIColor blackColor];
        company_name.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
        [b_companyView addSubview:company_name];
        company_name.text = [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_company"]];
        
        
        DebugLog(@"COMPANY:%@",[dict_profile objectForKey:@"b_company"]);
        
    }
    else
    {
        b_companyView.frame = CGRectMake(0, 0, 0, 0);
    }
    
    designationView = [[UIView alloc] init];
    
    
    if ([dict_profile objectForKey:@"b_function"] != false && ![[dict_profile objectForKey:@"b_function"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_function"] length] > 1 && ![[dict_profile objectForKey:@"b_function"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"b_function"] isEqualToString:@""] && ![[dict_profile objectForKey:@"b_function"] isKindOfClass:[NSNull class]])
    {
        designationView.frame = CGRectMake(0, b_companyView.frame.origin.y+b_companyView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 62);
        designationView.backgroundColor = [UIColor clearColor];
        [scrollView addSubview:designationView];
        
        UILabel *designationView_div = [[UILabel alloc] init];
        designationView_div.frame = CGRectMake(0, 61.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
        designationView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        [designationView addSubview:designationView_div];
        
        UILabel *desigLabel = [[UILabel alloc] init];
        desigLabel.frame = CGRectMake(20, 10, 100, 22);
        desigLabel.text = @"DESIGNATION";
        desigLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        desigLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
        [designationView addSubview:desigLabel];
        
        UILabel *desig = [[UILabel alloc] init];
        desig.frame = CGRectMake(20, 32, 270, 22);
        desig.textColor = [UIColor blackColor];
        desig.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
        [designationView addSubview:desig];
        
        desig.text = [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_function"]];
        DebugLog(@"COMPANY:%@",[dict_profile objectForKey:@"b_function"]);
    }
    else
    {
        designationView.frame = CGRectMake(0, b_companyView.frame.origin.y+b_companyView.frame.size.height, 0, 0);
    }
    
    b_phoneView = [[UIView alloc] init];
    
    
    if ([dict_profile objectForKey:@"b_phone_num"] != false && ![[dict_profile objectForKey:@"b_phone_num"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_phone_num"] length] > 1 && ![[dict_profile objectForKey:@"b_phone_num"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"b_phone_num"] isEqualToString:@""] && ![[dict_profile objectForKey:@"b_phone_num"] isKindOfClass:[NSNull class]])
    {
        b_phoneView.frame = CGRectMake(0, designationView.frame.origin.y+designationView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 62);
        b_phoneView.backgroundColor = [UIColor clearColor];
        [scrollView addSubview:b_phoneView];
        
        UILabel *b_phoneView_div = [[UILabel alloc] init];
        b_phoneView_div.frame = CGRectMake(0, 61.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
        b_phoneView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        [b_phoneView addSubview:b_phoneView_div];
        
        UILabel *b_phoneLabel = [[UILabel alloc] init];
        b_phoneLabel.frame = CGRectMake(20, 10, 100, 22);
        b_phoneLabel.text = @"PHONE";
        b_phoneLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        b_phoneLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
        [b_phoneView addSubview:b_phoneLabel];
        
        UILabel *b_phone = [[UILabel alloc] init];
        b_phone.frame = CGRectMake(20, 32, 200, 22);
        b_phone.backgroundColor = [UIColor clearColor];
        b_phone.textColor = [UIColor blackColor];
        b_phone.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
        [b_phoneView addSubview:b_phone];
        
        UIImageView *whatsapp_img = [[UIImageView alloc] init];
        whatsapp_img.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-50, 20, 22, 22);
        whatsapp_img.image = [UIImage imageNamed:@"Whatsapp"];
      //  [b_phoneView addSubview:whatsapp_img];
        
        
        UIButton *whatsapp_btn = [[UIButton alloc] init];
        whatsapp_btn.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-65, 10, 52, 42);
        [whatsapp_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [whatsapp_btn setBackgroundColor:[UIColor clearColor]];
        whatsapp_btn.userInteractionEnabled = YES;
        whatsapp_btn.enabled = YES;
        whatsapp_btn.layer.zPosition = 5;
        [whatsapp_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
        [whatsapp_btn addTarget:self action:@selector(whatsapp_func) forControlEvents:UIControlEventTouchUpInside];
      //  [b_phoneView addSubview:whatsapp_btn];
        
        b_phone.text = [NSString stringWithFormat:@"%@ %@",[dict_profile objectForKey:@"b_phone_pre"],[dict_profile objectForKey:@"b_phone_num"]];
        [_phonearr addObject:[NSString stringWithFormat:@"%@ %@",[dict_profile objectForKey:@"b_phone_pre"],[dict_profile objectForKey:@"b_phone_num"]]];
        
        
        UIButton *b_phoneView_btn = [[UIButton alloc] init];
        b_phoneView_btn.tag = 1;
        b_phoneView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-65, 62);
        [b_phoneView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [b_phoneView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
        [b_phoneView_btn addTarget:self action:@selector(phonefunc:) forControlEvents:UIControlEventTouchUpInside];
        [b_phoneView addSubview:b_phoneView_btn];
    }
    else
    {
        b_phoneView.frame = CGRectMake(0, designationView.frame.origin.y+designationView.frame.size.height, 0, 0);
    }
    
    b_emailView = [[UIView alloc] init];
    
    
    if ([dict_profile objectForKey:@"b_email"] != false && ![[dict_profile objectForKey:@"b_email"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_email"] length] > 1 && ![[dict_profile objectForKey:@"b_email"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"b_email"] isEqualToString:@""] && ![[dict_profile objectForKey:@"b_email"] isKindOfClass:[NSNull class]])
    {
        b_emailView.frame = CGRectMake(0, b_phoneView.frame.origin.y+b_phoneView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 62);
        b_emailView.backgroundColor = [UIColor clearColor];
        [scrollView addSubview:b_emailView];
        
        UILabel *b_emailView_div = [[UILabel alloc] init];
        b_emailView_div.frame = CGRectMake(0, 61.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
        b_emailView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        [b_emailView addSubview:b_emailView_div];
        
        UILabel *b_emailLabel = [[UILabel alloc] init];
        b_emailLabel.frame = CGRectMake(20, 10, 100, 22);
        b_emailLabel.text = @"EMAIL";
        b_emailLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        b_emailLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
        [b_emailView addSubview:b_emailLabel];
        
        UILabel *b_email = [[UILabel alloc] init];
        b_email.frame = CGRectMake(20, 32, [UIScreen mainScreen].bounds.size.width, 22);
        b_email.backgroundColor = [UIColor clearColor];
        b_email.textColor = [UIColor blackColor];
        b_email.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
        [b_emailView addSubview:b_email];
        mail = [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_email"]];
        DebugLog(@"BUSINESS MAIL:%@",mail);
        b_email.text = [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_email"]];
        
        UIButton *b_emailView_btn = [[UIButton alloc] init];
        b_emailView_btn.tag = 3;
        b_emailView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 62);
        [b_emailView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [b_emailView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
        [b_emailView_btn addTarget:self action:@selector(mailFun:) forControlEvents:UIControlEventTouchUpInside];
        [b_emailView addSubview:b_emailView_btn];
    }
    else
    {
        b_emailView.frame = CGRectMake(0, b_phoneView.frame.origin.y+b_phoneView.frame.size.height, 0, 0);
    }
    
    /////////////////////////////////////////////// WEBSITE /////////////////////////////////////////
    
    b_websiteView = [[UIView alloc] init];
    
    if ([dict_profile objectForKey:@"b_website"] != false && ![[dict_profile objectForKey:@"b_website"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_website"] length] > 1 && ![[dict_profile objectForKey:@"b_website"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"b_website"] isEqualToString:@""]) {
        
        b_websiteView.frame = CGRectMake(0, b_emailView.frame.origin.y+b_emailView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 62);
        b_websiteView.backgroundColor = [UIColor clearColor];
        [scrollView addSubview:b_websiteView];
        
        UILabel *b_websiteView_div = [[UILabel alloc] init];
        b_websiteView_div.frame = CGRectMake(0, 61.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
        b_websiteView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        [b_websiteView addSubview:b_websiteView_div];
        
        UILabel *b_websiteLabel = [[UILabel alloc] init];
        b_websiteLabel.frame = CGRectMake(20, 10, 100, 22);
        b_websiteLabel.text = @"WEBSITE";
        b_websiteLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        b_websiteLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
        [b_websiteView addSubview:b_websiteLabel];
        
        
        //[landphonelb addTarget:self action:@selector(landphoneFun:) forControlEvents:UIControlEventTouchUpInside];
        
        b_website = [[UILabel alloc] init];
        b_website.frame = CGRectMake(20, 32, [UIScreen mainScreen].bounds.size.width, 22);
        b_website.text = [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_website"]];
        b_website.textColor = [UIColor blackColor];
        b_website.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
        [b_websiteView addSubview:b_website];
        
        UIButton *b_websiteView_btn = [[UIButton alloc] init];
        b_websiteView_btn.tag = 1;
        b_websiteView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 62);
        [b_websiteView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [b_websiteView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
        //[websiteView_btn addTarget:self action:@selector(phonefunc:) forControlEvents:UIControlEventTouchUpInside];
        [b_websiteView addSubview:b_websiteView_btn];
        
    }
    else
    {
        b_websiteView.frame = CGRectMake(0, b_emailView.frame.origin.y+b_emailView.frame.size.height, 0,0);
    }
    
    
    
    
    NSString *b_addressValue;
    b_addressView = [[UIView alloc] init];
    
    
    if ([dict_profile objectForKey:@"b_street"] != false && ![[dict_profile objectForKey:@"b_street"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"b_street"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_street"] length]!=0 && ![[dict_profile objectForKey:@"b_street"] isEqualToString:@""] && ![[dict_profile objectForKey:@"b_street"] isEqualToString:@"0"])
        
    {
        b_addressValue = [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_street"]];
        
    }
    else
    {
        b_addressValue = @"";
        
    }
    if ([dict_profile objectForKey:@"b_housenumber"] != false && ![[dict_profile objectForKey:@"b_housenumber"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_housenumber"] length]!=0 && ![[dict_profile objectForKey:@"b_housenumber"] isEqualToString:@""] && ![[dict_profile objectForKey:@"b_housenumber"] isEqualToString:@"0"])
    {
        b_addressValue = [b_addressValue stringByAppendingString:[NSString stringWithFormat:@"%@<br>",[dict_profile objectForKey:@"b_housenumber"]]];
    }
    else
    {
        b_addressValue = [b_addressValue stringByAppendingString:[NSString stringWithFormat:@"<br>"]];
    }
    
    if ([dict_profile objectForKey:@"b_zipcode"] != false && ![[dict_profile objectForKey:@"b_zipcode"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_zipcode"] length]!=0 && ![[dict_profile objectForKey:@"b_zipcode"] isEqualToString:@""] && ![[dict_profile objectForKey:@"b_zipcode"] isEqualToString:@"0"])
    {
        
        nobusiness=1;
        
        b_addressValue = [b_addressValue stringByAppendingString:[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_zipcode"]]];
        
    }
    if ([dict_profile objectForKey:@"b_city"] != false && ![[dict_profile objectForKey:@"b_city"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"b_city"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_city"] length]!=0 && ![[dict_profile objectForKey:@"b_city"] isEqualToString:@""] && ![[dict_profile objectForKey:@"b_city"] isEqualToString:@"0"])
    {
        b_addressValue = [b_addressValue stringByAppendingString:[NSString stringWithFormat:@" %@<br>",[dict_profile objectForKey:@"b_city"]]];
        
    }
    
    if ([dict_profile objectForKey:@"b_country"] != false && ![[dict_profile objectForKey:@"b_country"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_country"] length]!=0 && ![[dict_profile objectForKey:@"b_country"] isEqualToString:@""] && ![[dict_profile objectForKey:@"b_country"] isEqualToString:@"0"])
    {
        b_addressValue = [b_addressValue stringByAppendingString:[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_country"]]];
        
    }
    
    if ([b_addressValue isEqualToString:@""] || [b_addressValue isEqualToString:@"<br>"]) {
        b_addressView.frame = CGRectMake(0, b_websiteView.frame.origin.y+b_websiteView.frame.size.height, 0, 0);
    }
    else
    {
        b_addressView.frame = CGRectMake(0, b_websiteView.frame.origin.y+b_websiteView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 106);
        b_addressView.backgroundColor = [UIColor clearColor];
        [scrollView addSubview:b_addressView];
        
        UILabel *b_addressView_div = [[UILabel alloc] init];
        b_addressView_div.frame = CGRectMake(0, 105.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
        b_addressView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        [b_addressView addSubview:b_addressView_div];
        
        UILabel *b_addressLabel = [[UILabel alloc] init];
        b_addressLabel.frame = CGRectMake(20, 10, 100, 22);
        b_addressLabel.text = @"ADDRESS";
        b_addressLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        b_addressLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
        [b_addressView addSubview:b_addressLabel];
        
        UITextView *b_address = [[UITextView alloc] init];
        b_address.frame = CGRectMake(15, 32, [UIScreen mainScreen].bounds.size.width-15, 70);
        b_address.backgroundColor = [UIColor clearColor];
        [b_address setValue:b_addressValue forKey:@"contentToHTMLString"];
        b_address.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
        
        //b_address.text = [NSString stringWithFormat:@"%@ %@ %@",[_Condetails_dict objectForKey:@"b_street"],[_Condetails_dict objectForKey:@"b_city"],[_Condetails_dict objectForKey:@"b_country"]];
        //    b_address.lineBreakMode = YES;
        //    b_address.lineBreakMode = NSLineBreakByWordWrapping;
        //    b_address.numberOfLines = 3;
        //    b_address.textColor = [UIColor blackColor];
        b_address.editable = NO;
        [b_addressView addSubview:b_address];
        
        UIButton *addressView_btn = [[UIButton alloc] init];
        addressView_btn.tag = 3;
        addressView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 106);
        [addressView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [addressView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
        [addressView_btn addTarget:self action:@selector(openmap:) forControlEvents:UIControlEventTouchUpInside];
        [b_addressView addSubview:addressView_btn];
        
    }
    
    socialView.frame = CGRectMake(0,b_addressView.frame.origin.y+b_addressView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 118);
    
}

-(void)detailpic: (UIGestureRecognizer *)sender
{
    ConPictureProfViewController *con = [[ConPictureProfViewController alloc]init];
    con.profilepic = profilepic;
    //    con.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:con animated:NO completion:nil];
}



-(void)closedetailpic: (UIGestureRecognizer *)sender
{
    
    CATransition *animation = [CATransition animation];
    
    
    
    [animation setType:kCATransitionFade];
    
    
    
    [animation setSubtype:kCATransitionFade];
    
    
    
    animation.duration = 0.7f;
    
    
    
    [detailimg.layer addAnimation:animation forKey:nil];
    
    
    
    if(detailimg.hidden==NO)
        
    {
        
        detailimg.hidden=YES;
        
    }
    
    [tapbgview removeFromSuperview];
    
}

-(void)crossmap
{
    [crossbt removeFromSuperview];
    [mapview removeFromSuperview];
    [tapbgview removeFromSuperview];
    [navigatebt removeFromSuperview];
    [navigatebtapple removeFromSuperview];
}

-(void)phonefunc:(UIButton*)sender
{
    
    call_img.alpha=1.0f;
    UIAlertView *callAlert;
    DebugLog(@"call phonesarr: %@",_phonearr);
    if ([_phonearr count] == 0)
    {
        callAlert = [[UIAlertView alloc] initWithTitle:@"No Number found for this user!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        callAlert.tag = 9;
        b_phoneView.backgroundColor = [UIColor clearColor];
        mobileView.backgroundColor = [UIColor clearColor];
        [callAlert show];
    }
    else
    {
        UIDevice *device = [UIDevice currentDevice];
        if ([[device model] isEqualToString:@"iPhone"] ) {
            
            NSString *phoneNumber = [[[_phonearr objectAtIndex:sender.tag-1] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"] invertedSet]] componentsJoinedByString:@""];
            phoneNumber = [@"tel://" stringByAppendingString:phoneNumber];
            //phoneNumber = [@"telprompt://" stringByAppendingString:@"+919477404186"];
            DebugLog(@"PHONE NUMBER: %@",phoneNumber);
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
            //[[UIApplication sharedApplication] openURL:[NSURL URLWithString: [NSString stringWithFormat:@"tel:%@",[_phonearr objectAtIndex:0]]]];
        } else {
            callAlert=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            callAlert.tag = 99;
            b_phoneView.backgroundColor = [UIColor clearColor];
            mobileView.backgroundColor = [UIColor clearColor];
            [callAlert show];
        }
    }
}

-(void)smsfunc
{
    //sms_btn.alpha=0.0f;
    sms_img.alpha=1.0f;
    DebugLog(@"I Want to Send a SMS");
    UIAlertView *smsAlert1;
    if ([_phonearr count] == 0)
    {
        smsAlert1 = [[UIAlertView alloc] initWithTitle:@"No Number found for this user!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        smsAlert1.tag = 6;
        [smsAlert1 show];
    }
    else
    {
        UIDevice *device = [UIDevice currentDevice];
        if ([[device model] isEqualToString:@"iPhone"]) {
            
            MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
            if([MFMessageComposeViewController canSendText])
            {
                [self presentViewController:controller animated:YES completion:Nil];
                controller.body = @"";
                NSString *smsstring= [NSString stringWithFormat:@"%@",[_phonearr objectAtIndex:0]];
                controller.recipients = [NSArray arrayWithObjects:smsstring, nil];
                controller.messageComposeDelegate = self;
                //[self presentModalViewController:controller animated:YES];
            }
        }
        else
        {
            smsAlert1=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            smsAlert1.tag = 66;
            [smsAlert1 show];
        }
    }
    
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    if (result == MessageComposeResultCancelled) {
        
    } else if (result == MessageComposeResultSent) {
        
    }
    
}



-(void)whatsapp_func
{
    DebugLog(@"profile name field text: %@",prof_name.text);
    // Fetch the address book
    CFErrorRef *errorab = nil;
    
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, errorab);
    
    // Search for the person named "Appleseed" in the address book
    //    CFArrayRef people = ABAddressBookCopyPeopleWithName(addressBook, CFSTR("Appleseed"));
    
    CFArrayRef people = ABAddressBookCopyPeopleWithName(addressBook,
                                                        (__bridge CFStringRef)prof_name.text);
    
    // Display "Appleseed" information if found in the address book
    if ((people != nil) && (CFArrayGetCount(people) > 0))
    {
        for (int m = 0; m < (CFArrayGetCount(people)); m++){
            DebugLog(@"count e dichhe: %ld == %@", (CFArrayGetCount(people)),people);
            ABRecordRef person = CFArrayGetValueAtIndex(people, 0);
            
            ABRecordID recordID = ABRecordGetRecordID(person);
            
            DebugLog(@"ab id: %d",recordID);
            DebugLog(@"phone array; %@", _phonearr);
            
            ABMultiValueRef phoneNumberProperty = ABRecordCopyValue(person, kABPersonPhoneProperty);
            NSArray *_phoneNumbers = (__bridge NSArray*)ABMultiValueCopyArrayOfAllValues(phoneNumberProperty);
            
            NSMutableArray *mutableph= [_phoneNumbers mutableCopy];
            int pp;
            for (pp =0; pp< [mutableph count]; pp++)
            {
                if ([[mutableph objectAtIndex:pp] isKindOfClass:[NSNull class]] || [mutableph objectAtIndex:pp] == (id)[NSNull null] || [[mutableph objectAtIndex:pp] length] < 3)
                {
                    [mutableph removeObjectAtIndex:pp];
                    pp= pp-1;
                }
            }
            NSMutableSet* set1 = [NSMutableSet setWithArray:mutableph];
            NSMutableSet* set2 = [NSMutableSet setWithArray:_phonearr];
            [set1 intersectSet:set2]; //this will give you only the obejcts that are in both sets
            
            NSArray* result = [set1 allObjects];
            DebugLog(@"result pelo: %@",result);
            
            if ([_phonearr count] > 0)
            {
                NSString * urlWhats = [NSString stringWithFormat:@"whatsapp://send?abid=%d&text=",recordID];
                NSURL * whatsappURL = [NSURL URLWithString:[urlWhats stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
                    [[UIApplication sharedApplication] openURL: whatsappURL];
                } else {
                    UIAlertView * alert1 = [[UIAlertView alloc] initWithTitle:@"WhatsApp not installed." message:@"Your device has no WhatsApp installed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert1 show];
                }
                break;
            }
        }
    }
    else
    {
        // Show an alert if "Appleseed" is not in Contacts
    }
    //CFRelease(addressBook);
    //CFRelease(people);
}

-(void)mailFun:(UIButton *)sender {
    
    //    if (sender.tag == 3) {
    //        b_emailView.backgroundColor = [UIColor lightGrayColor];
    //    }
    //    if (sender.tag == 2) {
    //        emailView.backgroundColor = [UIColor lightGrayColor];
    //    }
    //email_btn.alpha=0.0f;
    email_img.alpha=1.0f;
    if ([mail length]>0 && ![mail isEqualToString:@"(null)"] && ![mail isKindOfClass:[NSNull class]] && ![mail isEqualToString:@""]) {
        
        mailback.backgroundColor = [UIColor colorWithRed:(238.0f/255.0f) green:(238.0f/255.0f) blue:(238.0f/255.0f) alpha:1.0f];
        
        mailComposer = [[MFMailComposeViewController alloc]init];
        mailComposer.mailComposeDelegate = self;
        NSArray *recipentsArray = [NSArray arrayWithObject:mail];
        [mailComposer setToRecipients:recipentsArray];
        [mailComposer setMessageBody:@"" isHTML:NO];
        [self.navigationController presentViewController:mailComposer animated:YES completion:nil];
    }
    else{
        DebugLog(@"EMAIL:%@",mail);
        alert = [[UIAlertView alloc] initWithTitle:@"No Email Id found for this user!"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        b_emailView.backgroundColor = [UIColor clearColor];
        emailView.backgroundColor = [UIColor clearColor];
        
    }
}

#pragma mark - mail compose delegate

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (result) {
        DebugLog(@"Result : %d",result);
    }
    if (error) {
        DebugLog(@"Error : %@",error);
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
    mailback.backgroundColor = [UIColor clearColor];
}

-(void)openmap: (UIButton *)sender
{
    //    if (sender.tag == 3) {
    //        b_addressView.backgroundColor = [UIColor lightGrayColor];
    //    }
    //    if (sender.tag == 2) {
    //        addressView.backgroundColor = [UIColor lightGrayColor];
    //    }
    //location_btn.alpha=0.0f;
    location_img.alpha=1.0f;
    addressback.backgroundColor = [UIColor colorWithRed:(238.0f/255.0f) green:(238.0f/255.0f) blue:(238.0f/255.0f) alpha:1.0f];
    
    if (switchprof == 0)
    {
        latitude=[[dict_profile objectForKey:@"lat"] doubleValue];
        longitude=[[dict_profile objectForKey:@"lng"] doubleValue];
    }
    else
    {
        latitude=[[dict_profile objectForKey:@"b_lat"] doubleValue];
        longitude=[[dict_profile objectForKey:@"b_lng"] doubleValue];
    }
    DebugLog(@"lati=== %f",latitude);
    DebugLog(@"long=== %f",longitude);
    
    
    [self generateRoute];
}

- (void)generateRoute {
    CLLocationCoordinate2D end = {latitude, longitude};
    
    MKMapItem *destination_mapitem = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:end addressDictionary:nil]];
    
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
    
    request.source = [MKMapItem mapItemForCurrentLocation];
    
    request.destination = destination_mapitem;
    
    [request setTransportType:MKDirectionsTransportTypeAny]; // This can be limited to automobile and walking directions.
    
    [request setRequestsAlternateRoutes:YES];
    
    MKDirections *directions = [[MKDirections alloc] initWithRequest:request];
    
    [directions calculateDirectionsWithCompletionHandler:
     
     ^(MKDirectionsResponse *response, NSError *error) {
         
         if (error) {
             DebugLog(@"error generate route----  %@",error);
             
         } else {
             [self showRoute:response];
         }
     }];
    [self navigateinapple];
}

-(void)navigateinapple
{
    CLLocationCoordinate2D endingCoord = CLLocationCoordinate2DMake(latitude, longitude);
    MKPlacemark *endLocation = [[MKPlacemark alloc] initWithCoordinate:endingCoord addressDictionary:nil];
    MKMapItem *endingItem = [[MKMapItem alloc] initWithPlacemark:endLocation];
    NSMutableDictionary *launchOptions = [[NSMutableDictionary alloc] init];
    [launchOptions setObject:MKLaunchOptionsDirectionsModeDriving forKey:MKLaunchOptionsDirectionsModeKey];
    [endingItem openInMapsWithLaunchOptions:launchOptions];
}

-(void)showRoute:(MKDirectionsResponse *)response
{
    for (MKRoute *route in response.routes)
    {
        [map_View addOverlay:route.polyline level:MKOverlayLevelAboveLabels];
        
        for (MKRouteStep *step in route.steps)
        {
            DebugLog(@" here %@", step.instructions);
        }
    }
}


- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id < MKOverlay >)overlay
{
    MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
    renderer.strokeColor = [UIColor blueColor];
    renderer.lineWidth = 4.0;
    return renderer;
}


- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    CLLocation *location = map_View.userLocation.location;
    DebugLog(@"lat current: %f - long current: %f", location.coordinate.latitude, location.coordinate.longitude);
}


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    // Handle any custom annotations.
    
    if ([annotation isKindOfClass:[MKPointAnnotation class]])
    {
        // Try to dequeue an existing pin view first.
        
        MKPinAnnotationView *pinView = (MKPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomPinAnnotationView"];
        //        if (!pinView)
        //        {
        pinView = [[MKPinAnnotationView alloc] init];
        //            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomPinAnnotationView"];
        
        pinView.animatesDrop = YES;
        pinView.canShowCallout = YES;
        pinView.image = [UIImage imageNamed:@"locator1.png"];
        pinView.calloutOffset = CGPointMake(0, 0);
        
        
        UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        
        pinView.rightCalloutAccessoryView = rightButton;
        
        
        UIImageView *iconView = [[UIImageView alloc] init];
        
        iconView.frame = CGRectMake(0.0f, 0.0f,20, 20);
        
        pinView.leftCalloutAccessoryView = iconView;
        
        iconView.backgroundColor=[UIColor whiteColor];
        
        
        if (annotation == mapView.userLocation)
            
        {
            pinView.image= [UIImage imageNamed:@"locatorown.png"];
            
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomPinAnnotationView"];
        }
        
        DebugLog(@"(int)annotation.subtitle===%@",annotation.subtitle);
        
        iconView.image= profilepic;
        
        if (switchprof == 0)
            pinView.image = [UIImage imageNamed:@"locator1.png"];
        else
            pinView.image = [UIImage imageNamed:@"locator2.png"];
        
        //        }
        return pinView;
    }
    return nil;
}





-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    DebugLog(@"annotation selected");
}

- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)aUserLocation {
    
    MKCoordinateRegion region;
    
    MKCoordinateSpan span;
    
    span.latitudeDelta = 0.005;
    
    span.longitudeDelta = 0.005;
    
    CLLocationCoordinate2D map_location;
    
    map_location.latitude =  latitude;       //aUserLocation.coordinate.latitude;
    
    map_location.longitude = longitude;      //aUserLocation.coordinate.longitude;
    
    region.span = span;
    
    region.center = map_location;
    
    googleMapUrlString = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%f,%f&daddr=%f,%f", aUserLocation.location.coordinate.latitude,aUserLocation.location.coordinate.longitude, map_location.latitude, map_location.longitude];
    
    DebugLog(@"url fired map: %@",googleMapUrlString);
    
    //    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapUrlString]];
    appleMapUrlString = [NSString stringWithFormat:@"http://maps.apple.com/maps?saddr=%f,%f&daddr=%f,%f", aUserLocation.location.coordinate.latitude,aUserLocation.location.coordinate.longitude, map_location.latitude, map_location.longitude];
    [self generateRoute];
}

-(void)rightMenu:(UIButton *)sender{
    
    DebugLog(@"rightMenu: %x", rightmenuOpen);
    
    [overlayTap setEnabled:NO];
    //    menuBground.backgroundColor = [UIColor clearColor];
    
    if(!rightmenuOpen) {
        
        rightmenuOpen = TRUE;
        blackView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0)];
        [blackView setBackgroundColor:[UIColor blackColor]];
        [blackView setAlpha:0.9f];
        blackView.layer.zPosition = 3;
        [blackView setUserInteractionEnabled:YES];
        [headingView addSubview:blackView];
        
        //===========================SHOW BUSINESS CARD==========================//
        
        
        //        businessImg = [[UIImageView alloc]initWithFrame:CGRectMake(15, 0, 61.0f/3.0f, 0)];
        //        businessImg.image=[UIImage imageNamed:@"business_profile"];
        //        [blackView addSubview:businessImg];
        //
        //        togglebt = [UIButton buttonWithType:UIButtonTypeCustom];
        //        togglebt.frame = CGRectMake(36, 0, 180, 40);
        //        togglebt.backgroundColor=[UIColor clearColor];
        //        togglebt.layer.zPosition = 4.0f;
        //        if (switchprof == 0)
        //            [togglebt setTitle:@"View Business Card" forState:UIControlStateNormal];
        //        else
        //            [togglebt setTitle:@"View Personal Card" forState:UIControlStateNormal];
        //
        //        [togglebt setTitleColor:[UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f] forState:UIControlStateNormal];
        //        togglebt.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Normal" size:16];
        //        [togglebt addTarget:self action:@selector(togglefunc:) forControlEvents:UIControlEventTouchUpInside];
        //        [blackView addSubview:togglebt];
        //
        //        DebugLog(@"show business udhao: %d .... %d", [[dict_profile objectForKey:@"friend"]intValue], [[dict_profile objectForKey:@"business"] intValue]);
        //
        //        //=================================LINE=================================//
        //
        //        lineDivMenu = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0)];
        //        lineDivMenu.backgroundColor = [UIColor colorWithRed:(102.0f/255.0f) green:(102.0f/255.0f) blue:(102.0f/255.0f) alpha:1.0f];
        //        [blackView addSubview:lineDivMenu];
        
        //===========================REMOVE CONNECTION==========================//
        
        removeImg = [[UIImageView alloc]initWithFrame:CGRectMake(15, 0, 61.0f/3.0f, 0)];
        removeImg.image=[UIImage imageNamed:@"remove_profile"];
        [blackView addSubview:removeImg];
        
        removecon_label = [[UILabel alloc] init];
        //removecon_label.frame = CGRectMake(36, 8, 180, 40);
        removecon_label.text = @"Remove Connection";
        removecon_label.textAlignment = NSTextAlignmentCenter;
        removecon_label.textColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f];
        removecon_label.font = [UIFont fontWithName:@"ProximaNova-Normal" size:17];
        [blackView addSubview:removecon_label];
        
        
        removeconbt = [UIButton buttonWithType:UIButtonTypeCustom];
        removeconbt.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40);
        [removeconbt setTitle:@"" forState:UIControlStateNormal];
        [removeconbt setTitleColor:[UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f] forState:UIControlStateNormal];
        removeconbt.backgroundColor=[UIColor clearColor];
        [removeconbt addTarget:self action:@selector(removeConnection:) forControlEvents:UIControlEventTouchUpInside];
        [blackView addSubview:removeconbt];
        
        [UIView animateWithDuration:0.1 animations:^{
            
            [blackView setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 55)];
            
            // [businessImg setFrame:CGRectMake(16, 20, 61.0f/3.5f, 54.0f/3.0f)];
            
            // [togglebt setFrame:CGRectMake(36, 10, 180, 40)];
            
            // [lineDivMenu setFrame:CGRectMake(0, blackView.frame.size.height/2, [UIScreen mainScreen].bounds.size.width, 1)];
            
            [removeImg setFrame:CGRectMake(16, 18, 61.0f/3.5f, 54.0f/3.0f)];
            removecon_label.frame = CGRectMake(36, 8, 180, 40);
            removeconbt.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 55);
            
        }
         
                         completion:^(BOOL finished){
                             
                             rightmenuOpen = YES;
                             [overlayTap setEnabled:YES];
                             
                         }];
        
    } else {
        rightmenuOpen = FALSE;
        
        [UIView animateWithDuration:0.3 animations:^{
            
            //            coverView.backgroundColor = [UIColor blackColor];
            
            [blackView setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0)];
            
            
            [removeImg setFrame:CGRectMake(15, 0, 61.0f/2.0f, 0)];
            removecon_label.frame = CGRectMake(36, 8, 180, 0);
            removeconbt.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0);
            
            [togglebt removeFromSuperview];
            [removecon_label removeFromSuperview];
            [removeconbt removeFromSuperview];
            
        }
         
                         completion:^(BOOL finished){
                             
                             //rightmenuOpen = YES;
                             [overlayTap setEnabled:YES];
                             
                         }];
        
    }
    
}

-(void)togglefunc: (UIButton *)sender
{
    DebugLog(@"TOGGLE FUNC CaLl HoChChE");
    [togglebt removeFromSuperview];
    [removecon_label removeFromSuperview];
    [removeconbt removeFromSuperview];
    if (switchprof == 0)
    {
        [UIView animateWithDuration:0.3 animations:^{
            
            // Add the gradient to the view
            [coverView.layer insertSublayer:gradient atIndex:0];
            
            
            [blackView setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0)];
            
            
            [removeImg setFrame:CGRectMake(15, 0, 61.0f/2.0f, 0)];
            removecon_label.frame = CGRectMake(36, 8, 180, 0);
            removeconbt.frame = CGRectMake(36, 0, 180, 0);
            
        }
         
                         completion:^(BOOL finished){
                             
                             rightmenuOpen = FALSE;
                             
                         }];
        
        switchprof = 1;
        //[business_view removeFromSuperview];
        [self loadBusinessView];
        //        [self loadViews];
    }
    else
    {
        [UIView animateWithDuration:0.3 animations:^{
            
            // Add the gradient to the view
            [coverView.layer insertSublayer:gradient atIndex:0];
            
            
            [blackView setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0)];
            
            
            [removeImg setFrame:CGRectMake(15, 0, 61.0f/2.0f, 0)];
            removecon_label.frame = CGRectMake(36, 8, 180, 0);
            removeconbt.frame = CGRectMake(36, 0, 180, 0);
            
        }
                         completion:^(BOOL finished){
                             
                             rightmenuOpen = FALSE;
                             
                         }];
        
        switchprof = 0;
        [self loadPersonalViews];
        //        [self loadViews];
    }
}

-(void)changeButtonBackGroundColor:(id)sender{
    
    [rightMenuBack setBackgroundColor:[UIColor colorWithRed:(30.0f/255.0f) green:(30.0f/255.0f) blue:(30.0f/255.0f) alpha:1.0f]];
    
}
-(void)resetButtonBackGroundColor:(id)sender{
    
    [rightMenuBack setBackgroundColor:[UIColor blackColor]];
}

-(void)removeFromMap
{
    int index = 0;
    prefs = [[NSUserDefaults standardUserDefaults] init];
    NSMutableArray *removeArray = [[NSMutableArray alloc] init];
    removeArray = [prefs objectForKey:@"mapdata"];
    
    DebugLog(@"ARRAY:%@",removeArray);
    
    for (NSDictionary *dict in removeArray) {
        
        
        if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"id"]]  isEqualToString: user_id]) {
            [removeArray removeObjectAtIndex:index];
            DebugLog(@"REMOVED FROM MAP");
            break;
            
        }
        
        index++;
    }
    [prefs setObject:removeArray forKey:@"mapdata"];
    DebugLog(@"ARRAY:%@",removeArray);
    
}

-(void)removeConnection: (id)sender
{
    [UIView animateWithDuration:0.3 animations:^{
        
        // coverView.backgroundColor = [UIColor blackColor];
        
        // Add the gradient to the view
        [coverView.layer insertSublayer:gradient atIndex:0];
        
        
        
        [blackView setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0)];
        
        
        [removeImg setFrame:CGRectMake(15, 0, 61.0f/2.0f, 0)];
        removecon_label.frame = CGRectMake(36, 8, 180, 0);
        removeconbt.frame = CGRectMake(36, 0, 180, 0);
        
    }
     
     //        }];
     
                     completion:^(BOOL finished){
                         
                         rightmenuOpen = NO;
                         
                     }];
    [togglebt setHidden:YES];
    [removeconbt setHidden:YES];
    
    alert = [[UIAlertView alloc] initWithTitle:@"Remove this contact?"
                                       message:nil
                                      delegate:self
                             cancelButtonTitle:@"Cancel"  otherButtonTitles:@"Ok", nil];
    alert.tag=123;
    [alert show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    DebugLog(@"ALERT . TAG====%ld",(long)alertView.tag);
    if (alertView.tag == 123) {
        
        if(buttonIndex == 0)
        {
            // Do something
        }
        else
        {
            [UIView animateWithDuration:0.0f animations:^{
                
                act = [[UIActivityIndicatorView alloc] init];
                act.center=self.view.center;
                [act startAnimating];
                act.hidden=NO;
                act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
                [act setColor:[UIColor blackColor]];
                [mainview addSubview:act];
                
            }completion:^(BOOL finished){
                
                prefs = [NSUserDefaults standardUserDefaults];
                NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=removeconnection&id=%d&access_token=%@&device_id=%@&business=true",[user_id intValue],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
                
                DebugLog(@"deny url: %@",urlString1);
                NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
                NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
                
                if (signeddataURL1 != nil)
                {
                    NSError *error=nil;
                    NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                              options:kNilOptions
                                                                                error:&error];
                    DebugLog(@"deny json returns: %@",json_deny);
                    if ([[json_deny objectForKey:@"success"]intValue] == 1)
                    {
                        [act removeFromSuperview];
                        alert = [[UIAlertView alloc] initWithTitle:@"Connection Successfully Removed!"
                                                           message:nil
                                                          delegate:self
                                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                        [alert show];
                        [removecon_label removeFromSuperview];
                        [removeconbt removeFromSuperview];
                        
                        //[self performSelectorInBackground:@selector(removeFromMap) withObject:nil];
                        
                        ConNewContactsViewController *refresh = [[ConNewContactsViewController alloc]init];
                        [refresh reloadAgain];
                        
                        //                togglebt.userInteractionEnabled = NO;
                        
                        ConNewRequestsViewController *mng = [[ConNewRequestsViewController alloc]init];
                        //                mng.imported_arr = [filtered_arr objectAtIndex:indexPath.row];
                        mng.userid= [user_id intValue];
                        DebugLog(@"selected user id : %d",mng.userid);
                        
                        //                        CATransition* transition = [CATransition animation];
                        //
                        //                        transition.duration = 0.4;
                        //                        transition.type = kCATransitionPush;
                        //                        transition.subtype = kCATransitionFade;
                        //
                        //                        [[self navigationController].view.layer addAnimation:transition forKey:nil];
                        
                        [self.navigationController pushViewController:mng animated:YES];
                    }
                    else
                    {
                        [act removeFromSuperview];
                        alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                                           message:[NSString stringWithFormat:@"%@",[json_deny objectForKey:@"error"]]
                                                          delegate:self
                                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                        [alert show];
                    }
                }
                else
                {
                    [act removeFromSuperview];
                    alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                                       message:nil
                                                      delegate:self
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                    [alert show];
                }
                
            }];
            
        }
    }
}

-(void)facebookfunc
{
    
    NSString *fb_app_str = [[NSString alloc]init];
    
    fb_app_str = [dict_profile objectForKey:@"s_facebook"];
    
    //    NSString *string1=[NSString
    //                       stringWithFormat:@"http://graph.facebook.com/%@",[dict_profile objectForKey:@"s_facebook"]];
    //
    //    DebugLog(@"facebook graph %@",string1);
    //
    //    NSError *localErr;
    //
    //
    //
    //    NSData *result =  [NSData dataWithContentsOfURL:[NSURL URLWithString:string1] options:NSDataReadingUncached error:&localErr];
    //
    //
    //
    //    if (result != nil)
    //
    //    {
    //
    //        NSDictionary *graphjson =[NSJSONSerialization JSONObjectWithData:result options:0 error:&localErr];
    //
    //        DebugLog(@"GRAPH:%@",graphjson);
    //        if (![graphjson isKindOfClass:[NSNull class]])
    //
    //        {
    //
    //            if ([[graphjson allKeys]containsObject:@"id"])
    //
    //            {
    //
    //                fb_app_str = [graphjson objectForKey:@"id"];
    //
    //            }
    //
    //        }
    //
    //    }
    //
    //
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"fb://profile/%@",fb_app_str]];  //[dict_profile objectForKey:@"s_facebook"]
    
    [[UIApplication sharedApplication] openURL:url];
    
    
    
    if ([[UIApplication sharedApplication] canOpenURL:url]){
        
        DebugLog(@"facebook url :%@",url);
        [[UIApplication sharedApplication] openURL:url];
        
    }
    
    else {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://facebook.com/%@",[dict_profile objectForKey:@"s_facebook"]]]];  //100001925673906
        
        DebugLog(@"http://facebook.com/%@",[dict_profile objectForKey:@"s_facebook"]);
        
    }
    
}


-(void)gplusfunc
{
    NSString *string = [dict_profile objectForKey:@"s_google"];
    NSArray *stringArray = [string componentsSeparatedByString: @"com/"];
    NSString *gplusstr = [stringArray objectAtIndex:1];
    NSURL *GooglePlus = [NSURL URLWithString:[NSString stringWithFormat:@"gplus://plus.google.com/%@",gplusstr]];
    DebugLog(@"GOO+ URL=============>%@",GooglePlus);
    
    if ([[UIApplication sharedApplication] canOpenURL:GooglePlus])  //[NSURL URLWithString:@"gplus://plus.google.com/u/0/105819873801211194735/"]
        [[UIApplication sharedApplication] openURL:GooglePlus];
    else
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://plus.google.com/%@",gplusstr]]];
}

-(void)pinterestfunc
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"pinterest://user/%@",[dict_profile objectForKey:@"s_pinterest"]]]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"pinterest://user/%@",[dict_profile objectForKey:@"s_pinterest"]]]];
    } else {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://pinterest.com"]];
        DebugLog(@"PIN URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"http://pinterest.com/%@",[dict_profile objectForKey:@"s_pinterest"]]]);
    }
}

-(void)twitterfunc
{
    NSURL *urlApp = [NSURL URLWithString: [NSString stringWithFormat:@"twitter://user?screen_name=%@", [dict_profile objectForKey:@"s_twitter"]]];
    
    if ([[UIApplication sharedApplication] canOpenURL:urlApp]) {
        
        [[UIApplication sharedApplication] openURL:urlApp];
    } else {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://twitter.com/%@",[dict_profile objectForKey:@"s_twitter"]]]];
        DebugLog(@"TWIT URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"http://twitter.com/%@",[dict_profile objectForKey:@"s_twitter"]]]);
    }
}


-(void)youtubefunc
{
    NSString* social   = [NSString stringWithFormat:@"http://www.youtube.com/user/%@",[dict_profile objectForKey:@"s_youtube"]];
    NSString* socialApp = [NSString stringWithFormat:@"youtube://user/%@",[dict_profile objectForKey:@"s_youtube"]];
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:socialApp]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:socialApp]];
        
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:social]];
    }
}



-(void)instagramfunc
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"instagram://user?username=%@",[dict_profile objectForKey:@"s_instagram"]]]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"instagram://user?username=%@",[dict_profile objectForKey:@"s_instagram"]]]];
        
        DebugLog(@"INSTA URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"instagram://user?username=%@",[dict_profile objectForKey:@"s_instagram"]]]);
    } else {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.instagram.com"]];
        DebugLog(@"INSTA URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"https://www.instagram.com/%@",[dict_profile objectForKey:@"s_instagram"]]]);
    }
}


-(void)linkedinfunc
{
    NSString *linkedinstra = [dict_profile objectForKey:@"s_linkedin"];
    //    NSArray *linkedinarr= [linkedinstra componentsSeparatedByString:@"="];
    //    DebugLog(@"Linkedin:%@",linkedinarr);
    //    NSString *linkedinstr = [linkedinarr objectAtIndex:1];
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"linkedin://profile/%@",linkedinstra]]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"linkedin://profile/%@",linkedinstra]]];
        
        DebugLog(@"LINKED URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"linkedin://profile/%@",linkedinstra]]);
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.linkedin.com"]];
        DebugLog(@"LINKED URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_linkedin"]]]);
    }
}


-(void)skypefunc
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"skype:%@",[dict_profile objectForKey:@"s_skype"]]]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"skype:%@",[dict_profile objectForKey:@"s_skype"]]]];
        
        DebugLog(@"SKYPE URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"skype:%@",[dict_profile objectForKey:@"s_skype"]]]);
    } else {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.skype.com/%@",[dict_profile objectForKey:@"s_skype"]]]];
        
        DebugLog(@"SKYPE URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"https://www.skype.com/%@",[dict_profile objectForKey:@"s_skype"]]]);
    }
}
-(void)skypeAction{
    
    if ([dict_profile objectForKey:@"s_skype"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_skype"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_skype"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_skype"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_skype"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_skype"] isEqualToString:@"(null)"]) {
        
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"skype:%@",[dict_profile objectForKey:@"s_skype"]]]]) {
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"skype:%@",[dict_profile objectForKey:@"s_skype"]]]];
            
            DebugLog(@"SKYPE URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"skype:%@",[dict_profile objectForKey:@"s_skype"]]]);
        } else {
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.skype.com/%@",[dict_profile objectForKey:@"s_skype"]]]];
            
            DebugLog(@"SKYPE URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"https://www.skype.com/%@",[dict_profile objectForKey:@"s_skype"]]]);
        }
        
    }else{
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.skype.com"]]];
        
    }
    
    
}


-(void)soundcloudfunc
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.soundcloud.com/%@",[dict_profile objectForKey:@"s_soundcloud"]]]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.soundcloud.com/%@",[dict_profile objectForKey:@"s_soundcloud"]]]];
        
        DebugLog(@"SOUNDC URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"https://www.soundcloud.com/%@",[dict_profile objectForKey:@"s_soundcloud"]]]);
        
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.soundcloud.com"]];
        
        DebugLog(@"SOUNDC URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"https://www.soundcloud.com/%@",[dict_profile objectForKey:@"s_soundcloud"]]]);
    }
}



-(void)vimeofunc
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.vimeo.com/%@",[dict_profile objectForKey:@"s_vimeo"]]]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.vimeo.com/%@",[dict_profile objectForKey:@"s_vimeo"]]]];
        
        DebugLog(@"VIMEO URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"https://www.vimeo.com/%@",[dict_profile objectForKey:@"s_vimeo"]]]);
        
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.vimeo.com"]];
        
        DebugLog(@"VIMEO URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"https://www.vimeo.com/%@",[dict_profile objectForKey:@"s_vimeo"]]]);
    }
}

-(UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

-(void)websiteFun: (id)sender
{
    DebugLog(@"dcdc");
    // SocialWebViewController *social= [[SocialWebViewController alloc]init];
    if(map)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"website"]]]];
    }
    else
    {
        
        if (switchprof == 0)
        {
            //        social.type_social=[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"website"]];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[_Condetails_dict objectForKey:@"website"]]]];
            
            
        }
        else
            //        social.type_social=[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_website"]];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[_Condetails_dict objectForKey:@"b_website"]]]];
        
        //    CATransition* transition = [CATransition animation];
        //
        //    transition.duration = 0.4;
        //    transition.type = kCATransitionPush;
        //    transition.subtype = kCATransitionFade;
        //
        //    [[self navigationController].view.layer addAnimation:transition forKey:nil];
        //
        //    [self.navigationController pushViewController:social animated:YES];
    }
}

-(void)servererror
{
    [act removeFromSuperview];
    alert = [[UIAlertView alloc] initWithTitle:@"Error in Profile Server Connection!"
                                       message:nil delegate:self
                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
    [alert show];
}

-(void)startLoader
{
    act = [[UIActivityIndicatorView alloc] init];//WithFrame:CGRectMake(135, 190, 40, 40)];
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    act.center=mainscroll.center;
    [act setColor:[UIColor blackColor]];
    [mainscroll addSubview:act];
    [act startAnimating];
}
-(void)stopLoader
{
    [act removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
