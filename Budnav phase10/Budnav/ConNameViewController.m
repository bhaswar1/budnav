//
//  ConNameViewController.m
//  Budnav
//
//  Created by intel on 14/05/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "ConNameViewController.h"
#import "SVProgressHUD.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

@interface ConNameViewController ()
{
    UIButton *save_btn;
    UIImageView *bck_img;
}

@end

@implementation ConNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor=[UIColor colorWithRed:(238.0f/255.0f) green:(238.0f/255.0f) blue:(238.0f/255.0f) alpha:1.0f];
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, [[UIScreen mainScreen]bounds].size.height-44, [UIScreen mainScreen].bounds.size.width, 44)];
    
    del.tabBarController.tabBar.hidden = YES;
    self.navigationController.navigationBarHidden=YES;
    
    topbar = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64)];
    [self.view addSubview:topbar];
    //topbar.backgroundColor=[UIColor blackColor];
    topbar.layer.zPosition=2;
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
    
    // Add the gradient to the view
    [topbar.layer insertSublayer:gradient atIndex:0];
    
    
    UIImageView *logo_img = [[UIImageView alloc]init];
    logo_img.frame = CGRectMake(27+([UIScreen mainScreen].bounds.size.width-160)/2, 32, 101, 20);
    logo_img.backgroundColor = [UIColor clearColor];
    
    logo_img.image = [UIImage imageNamed:@"new_logo"];
    [topbar addSubview:logo_img];
    
    
    UILabel *topbartitle = [[UILabel alloc]initWithFrame:CGRectMake(60, 32, [UIScreen mainScreen].bounds.size.width-120, 20)];
    topbartitle.backgroundColor=[UIColor clearColor];
    //[topbar addSubview:topbartitle];
    topbartitle.text=@"CHANGE PASSWORD";
    topbartitle.textAlignment=NSTextAlignmentCenter;
    topbartitle.font=[UIFont fontWithName:@"ProximaNova-Regular" size:15.5f];
    topbartitle.textColor=[UIColor whiteColor];
    
    
    bck_img = [[UIImageView alloc]initWithFrame:CGRectMake(15,32,12,20)];//(13, 39, 10, 22)];
    bck_img.image=[UIImage imageNamed:@"back3"];
    [topbar addSubview:bck_img];
    
    
    // CGRect frame = CGRectMake(110,165,[UIScreen mainScreen].bounds.size.width-220,100);
    
    act = [[UIActivityIndicatorView alloc] init];
    act.center = self.view.center;
    [act startAnimating];
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [act setColor:[UIColor blackColor]];
    [self.view addSubview:act];
    
    act.layer.zPosition = 4;
    
  
    UIButton *helpbt = [UIButton buttonWithType:UIButtonTypeCustom];
    helpbt.frame = CGRectMake(0, 0, logo_img.frame.origin.x-1, 64);
    helpbt.backgroundColor=[UIColor clearColor];
    [helpbt setTitle:nil forState:UIControlStateNormal];
    
    [helpbt addTarget:self action:@selector(backColor) forControlEvents:UIControlEventTouchDown];
    [helpbt addTarget:self action:@selector(backColorAgain) forControlEvents:UIControlEventTouchDragExit];
    [helpbt addTarget:self action:@selector(backColor) forControlEvents:UIControlEventTouchDragEnter];
    [helpbt addTarget:self action:@selector(goback) forControlEvents:UIControlEventTouchUpInside];
    [topbar addSubview:helpbt];
    
    
    save_btn = [[UIButton alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-90, 25, 90, 39)];
    [save_btn setTitle:@"Save" forState:UIControlStateNormal];
    save_btn.backgroundColor = [UIColor clearColor];
    [save_btn.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Bold" size:20.0]];
    // [save_btn.titleLabel.text] = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    [save_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [topbar addSubview:save_btn];
    [save_btn addTarget:self action:@selector(changeColor) forControlEvents:UIControlEventTouchDown];
    [save_btn addTarget:self action:@selector(changeColorAgain) forControlEvents:UIControlEventTouchDragExit];
    [save_btn addTarget:self action:@selector(changeColor) forControlEvents:UIControlEventTouchDragEnter];

    [save_btn addTarget:self action:@selector(save:) forControlEvents:UIControlEventTouchUpInside];
    
    nametext= [[UITextField alloc]initWithFrame:CGRectMake(0, 65, [UIScreen mainScreen].bounds.size.width, 50)];
    [self.view addSubview:nametext];
    nametext.delegate=self;
    nametext.backgroundColor=[UIColor whiteColor];
    nametext.placeholder=@"Name";
    nametext.layer.sublayerTransform = CATransform3DMakeTranslation(20.0f, 0.0f, 0.0f);
    nametext.text = [[del.profdict objectForKey:@"profile" ] objectForKey:@"name"];  //bhaswar.mukherjee@esolzmail.com
    nametext.textColor=[UIColor blackColor];
    nametext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    nametext.tintColor=[UIColor grayColor];
    nametext.keyboardType=UIKeyboardTypeDefault;
    nametext.keyboardAppearance=UIKeyboardAppearanceDark;
    
    
    
    UIView *name_div = [[UIView alloc] initWithFrame:CGRectMake(0, 114, [UIScreen mainScreen].bounds.size.width, 1.0f)];
    //sep.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"divider.png"]];
    name_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f];
    [self.view addSubview:name_div];
    
    surnametext= [[UITextField alloc]initWithFrame:CGRectMake(0, 116, [UIScreen mainScreen].bounds.size.width, 50)];
    [self.view addSubview:surnametext];
    surnametext.delegate=self;
    surnametext.backgroundColor=[UIColor whiteColor];
    surnametext.placeholder=@"Surname";
    surnametext.layer.sublayerTransform = CATransform3DMakeTranslation(20.0f, 0.0f, 0.0f);
    surnametext.text = [[del.profdict objectForKey:@"profile" ] objectForKey:@"surname"];  //123456
    surnametext.textColor=[UIColor blackColor];
    surnametext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    surnametext.tintColor=[UIColor grayColor];
    surnametext.keyboardType=UIKeyboardTypeDefault;
    surnametext.keyboardAppearance=UIKeyboardAppearanceDark;
    
    
    UIView *surname_div = [[UIView alloc] initWithFrame:CGRectMake(0, 165, [UIScreen mainScreen].bounds.size.width, 1.0f)];
    //sep.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"divider.png"]];
    surname_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f];
    [self.view addSubview:surname_div];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    
    [act stopAnimating];
    
    self.screenName = @"Edit name";
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createEventWithCategory:@"Account settings"
                                             action:@"Edit name"
                                              label:nil
                                              value:nil] build]];

}

-(void)goback//: (id)sender
{
    //[self dismissViewControllerAnimated:YES completion:nil];
    
    CATransition *transition = [CATransition animation];
    
    transition.duration = 0.4f;
    
    transition.type = kCATransitionFade;
    
    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    [self.navigationController popViewControllerAnimated:YES];
    //    ConAccountSettingsViewController *con = [[ConAccountSettingsViewController alloc]init];
    //    [self.navigationController pushViewController:con animated:NO];
}

-(NSString*) encodeToPercentEscapeString:(NSString *)string {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                              (CFStringRef) string,
                                                              NULL,
                                                              (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
}


-(void)save: (id)sender
{
    [nametext resignFirstResponder];
    [surnametext resignFirstResponder];
    
        
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *name = [self encodeToPercentEscapeString:nametext.text];
    NSString *surname = [self encodeToPercentEscapeString:surnametext.text];
    //    NSString *confpass = [self encodeToPercentEscapeString:retypetext.text];
    
    [UIView animateWithDuration:0.0 animations:^{
         [SVProgressHUD showWithStatus:@"Saving......"];
        
    }
                     completion:^(BOOL finished){
                        
                         if ([[nametext.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
                         {
                             alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your Name!"
                                                                message:Nil
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                             
                              save_btn.alpha = 1.0f;
                             [SVProgressHUD dismiss];
                             [alert show];
                             
                            
                             
                         }
                         
                         else
                         {
                             NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                             //        NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=editpassword&oldpass=%@&newpass=%@&access_token=%@&device_id=%@",name, surname,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
                             NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=editprofile&name=%@&surname=%@&access_token=%@&device_id=%@",name, surname,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
                             
                             DebugLog(@"deny url: %@",urlString1);
                             NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
                             
                             NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
                             if (signeddataURL1 != nil)
                             {
                                 NSError *error=nil;
                                 NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                                           options:kNilOptions
                                                                                             error:&error];
                                 DebugLog(@"deny json returns: %@",json_deny);
                                 NSString *errornumber_reqcount= [NSString stringWithFormat:@"%@",[json_deny objectForKey:@"errorno"]];
                                 if (![errornumber_reqcount isEqualToString:@"0"])
                                 {
                                     NSString *err_str_reqcount = [json_deny objectForKey:@"error"];
                                     alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                                                        message:err_str_reqcount
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                                     
                                     save_btn.alpha = 1.0f;
                                     [SVProgressHUD dismiss];
                                     [alert show];
                                     
                                 }
                                 
                                 if ([[json_deny objectForKey:@"success"]intValue] == 1)
                                 {
                                     save_btn.alpha = 1.0f;
                                     [SVProgressHUD dismiss];
                                     
                                     alert = [[UIAlertView alloc] initWithTitle:@"Changes have been Successfully Saved!"
                                                                        message:nil
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                                     [alert show];
                                     
                                     CATransition *transition = [CATransition animation];
                                     
                                     transition.duration = 0.4f;
                                     
                                     transition.type = kCATransitionFade;
                                     
                                     [[self navigationController].view.layer addAnimation:transition forKey:nil];
                                     [self.navigationController popViewControllerAnimated:YES];
                                     
                                 }
                             }
                         }
                         
                     }];

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


-(void)changeColor
{
    save_btn.alpha = 0.5f;
}

-(void)changeColorAgain
{
    save_btn.alpha = 1.0f;
}

-(void)backColorAgain
{
    bck_img.alpha = 1.0f;
}
-(void)backColor
{
    bck_img.alpha = 0.5f;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
