//
//  ConPersonalProfileViewController.m
//  Budnav
//
//  Created by intel on 14/05/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "ConPersonalProfileViewController.h"
#import "VPImageCropperViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "AppDelegate.h"
#import "ConAccountSettingsViewController.h"
#import "DBManager.h"
#import "SVProgressHUD.h"
#import "ConFullSettingsViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

#define ORIGINAL_MAX_WIDTH 640.0f
#define ORIGINAL_MAX_HEIGHT 550.0f

@interface ConPersonalProfileViewController ()<VPImageCropperDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

{
    UIView *topbar, *flagView;
    UILabel *flag_name;
    NSArray *countriesList, *isdcode_list;
    UITableView *countrycodetable, *isdcode_table;
    UIImageView *flag_image, *profile_image;
    UIScrollView *scrollView;
    NSString *base64String, *companyName, *job_title, *mobile, *email_address, *web, *countryName, *zipCode, *street_name, *pathss;
    UIButton *save_btn, *pre_mob, *pre_home;
    UITextField *street, *mobilePhone, *homePhone, *email, *website, *zipcode, *city;
    UIAlertView *alert;
    AppDelegate *del;
    NSUserDefaults *prefs;
    UIImage *portraitImg1;
    UIImageView *bck_img;
    int picuploaded;
    BOOL pre_mob_btn, pre_home_btn;
    UIActivityIndicatorView *act;
    NSMutableDictionary *json1;
    
}

@end

@implementation ConPersonalProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"from ConPersonalProfileViewController.......");
    picuploaded=0;
    
    NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"countriesflags" ofType:@"json"]];
    NSError *localError = nil;
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
    
    if (localError != nil) {
        DebugLog(@"%@", [localError userInfo]);
    }
    countriesList = (NSArray *)parsedObject;
    
    NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
    countriesList = [countriesList sortedArrayUsingDescriptors:sortDescriptors];
    
    
    ///////////////////////////////////// ISD CODE ARRAY ///////////////////////////////////////////////
    NSData *data1 = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"countries" ofType:@"json"]];
    NSError *localError1 = nil;
    NSDictionary *parsedObject1 = [NSJSONSerialization JSONObjectWithData:data1 options:0 error:&localError1];
    
    if (localError1 != nil) {
    }
    isdcode_list = (NSArray *)parsedObject1;
    
    NSSortDescriptor *sort12 = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    
    isdcode_list = [isdcode_list sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort12]];
    
    for (check=0; check < isdcode_list.count; check++) {
        
        if ([[[isdcode_list objectAtIndex:check]objectForKey:@"code"] isEqualToString: [[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkISOCode"]]) {
            isdCode = [NSString stringWithFormat:@"%@",[[isdcode_list objectAtIndex:check]objectForKey:@"dial_code"]];
        }
    }
    
    DebugLog(@"ISDCODE ARRAY:%@",isdcode_list);
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    
    
    self.view.backgroundColor = [UIColor colorWithRed:(238.0f/255.0f) green:(238.0f/255.0f) blue:(238.0f/255.0f) alpha:1.0f];
    del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, [[UIScreen mainScreen]bounds].size.height-44, [UIScreen mainScreen].bounds.size.width, 44)];
    
    del.tabBarController.tabBar.hidden = YES;
    self.navigationController.navigationBarHidden=YES;
    
    DebugLog(@"PROFILE DETAILS:%@",[del.profdict objectForKey:@"profile"]);
    
    
    topbar = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64)];
    [self.view addSubview:topbar];
    //topbar.backgroundColor=[UIColor blackColor];
    topbar.layer.zPosition=2;
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
    
    // Add the gradient to the view
    [topbar.layer insertSublayer:gradient atIndex:0];
    
    
    UIImageView *logo_img = [[UIImageView alloc]init];
    logo_img.frame = CGRectMake(27+([UIScreen mainScreen].bounds.size.width-160)/2, 32, 101, 20);
    logo_img.backgroundColor = [UIColor clearColor];
    
    logo_img.image = [UIImage imageNamed:@"new_logo"];
    [topbar addSubview:logo_img];
    
    bck_img=[[UIImageView alloc]initWithFrame:CGRectMake(15,32,12,20)];//(13, 39, 10, 22)];
    bck_img.image=[UIImage imageNamed:@"back3"];
    [topbar addSubview:bck_img];
 
    UIButton *back_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    back_btn.frame = CGRectMake(0, 0, logo_img.frame.origin.x
                                -1, 64);
    back_btn.backgroundColor=[UIColor clearColor];
    [back_btn setTitle:nil forState:UIControlStateNormal];
    
    [back_btn addTarget:self action:@selector(backColor) forControlEvents:UIControlEventTouchDown];
    [back_btn addTarget:self action:@selector(backColorAgain) forControlEvents:UIControlEventTouchDragExit];
    [back_btn addTarget:self action:@selector(backColor) forControlEvents:UIControlEventTouchDragEnter];
    
    [back_btn addTarget:self action:@selector(goback) forControlEvents:UIControlEventTouchUpInside];
    [topbar addSubview:back_btn];
    
    
    save_btn = [[UIButton alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-90, 25, 90, 39)];
    [save_btn setTitle:@"Save" forState:UIControlStateNormal];
    save_btn.backgroundColor = [UIColor clearColor];
    [save_btn.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Bold" size:20.0]];
    // [save_btn.titleLabel.text] = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    [save_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [topbar addSubview:save_btn];
    [save_btn addTarget:self action:@selector(changeColor) forControlEvents:UIControlEventTouchDown];
    [save_btn addTarget:self action:@selector(changeColorAgain) forControlEvents:UIControlEventTouchDragExit];
    [save_btn addTarget:self action:@selector(changeColor) forControlEvents:UIControlEventTouchDragEnter];
    
    [save_btn addTarget:self action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
    
   
    UIView *headingView = [[UIView alloc] initWithFrame:CGRectMake(0, 65, [UIScreen mainScreen].bounds.size.width, 101)];
    [headingView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:headingView];
    
    profile_image = [[UIImageView alloc] initWithFrame:CGRectMake(20, 11.5, 77, 77)];
    profile_image.backgroundColor = [UIColor clearColor];
    profile_image.layer.cornerRadius = profile_image.frame.size.width/2;
    profile_image.image = [UIImage imageNamed:@"profileImage"];
    [headingView addSubview:profile_image];
    
    base64String = [del.profdict objectForKey:@"image"];
    if ([base64String length] > 6)
    {
        NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
        //    NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
        
        UIImage *profilepic = [UIImage imageWithData:decodedData];
        
        profile_image.image = profilepic;
        //profile_image.contentMode= UIViewContentModeScaleAspectFill;
        //profile_image.clipsToBounds=YES;
    }
    
    profile_image.contentMode= UIViewContentModeScaleAspectFill;
    profile_image.clipsToBounds=YES;
    profile_image.userInteractionEnabled=YES;
    UITapGestureRecognizer *tapprofpic = [[UITapGestureRecognizer alloc]
                                          initWithTarget:self
                                          action:@selector(changeprofpic:)];
    
    [profile_image addGestureRecognizer:tapprofpic];
    //    }
    
    
    UILabel *headingLabel = [[UILabel alloc] initWithFrame:CGRectMake(profile_image.frame.origin.x+profile_image.frame.size.width+20, 35, [UIScreen mainScreen].bounds.size.width-130, 35)];
    [headingLabel setBackgroundColor:[UIColor clearColor]];
    headingLabel.text = @"This profile can be seen \nby personal connections.";
    headingLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:16];
    //    headingLabel.lineBreakMode = YES;
    headingLabel.numberOfLines = 2;
    headingLabel.textColor = [UIColor blackColor];
    headingLabel.textAlignment = NSTextAlignmentLeft;
    [headingView addSubview:headingLabel];
    [headingLabel adjustsFontSizeToFitWidth];
    
    
    
    UILabel *heading_div = [[UILabel alloc] initWithFrame:CGRectMake(0, 101, [UIScreen mainScreen].bounds.size.width, 0.6f)];
    [heading_div setBackgroundColor:[UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f]];
    [headingView addSubview:heading_div];
    
    for (check=0; check < countriesList.count; check++){
        
        if ([[[countriesList objectAtIndex:check]objectForKey:@"code2l"] isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"placemarkISOCode"]]) {
            
            flagCode = [[countriesList objectAtIndex:check]objectForKey:@"flag_128"];
            
        }
        //        DebugLog(@"FLAG_CODE===============>%@",flagCode);
    }
    
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 167, [UIScreen mainScreen].bounds.size.width,self.view.frame.size.height) ];
    scrollView.backgroundColor =[UIColor clearColor];
    scrollView.scrollEnabled = YES;
    scrollView.userInteractionEnabled = YES;
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width,scrollView.frame.size.height+255)];
    scrollView.contentSize = CGSizeMake(1, scrollView.frame.size.height+255);
    scrollView.showsVerticalScrollIndicator=NO;
    [self.view addSubview:scrollView];
    scrollView.contentOffset=CGPointMake(0, 0);
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    self.screenName = @"Edit personal profile";
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createEventWithCategory:@"Settings"
                                             action:@"Edit personal profile"
                                              label:nil
                                              value:nil] build]];

//    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 167, [UIScreen mainScreen].bounds.size.width,self.view.frame.size.height) ];
//    scrollView.backgroundColor =[UIColor clearColor];
//    scrollView.scrollEnabled = YES;
//    scrollView.userInteractionEnabled = YES;
//    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width,scrollView.frame.size.height+255)];
//    scrollView.contentSize = CGSizeMake(1, scrollView.frame.size.height+255);
//    scrollView.showsVerticalScrollIndicator=NO;
//    [self.view addSubview:scrollView];
//    scrollView.contentOffset=CGPointMake(0, 0);
    
    ///////////////////////////////////// ISD CODE ////////////////////////////////////////////////////////////////
    pre_mob = [UIButton buttonWithType:UIButtonTypeCustom];
    pre_mob.frame = CGRectMake(0, 0, 70, 50);
    //pre_mob.backgroundColor = [UIColor clearColor];
    [pre_mob setBackgroundColor:[UIColor whiteColor]];
    pre_mob.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    //pre_mob.titleLabel.textColor = [UIColor blackColor];
    [pre_mob setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    //[pre_mob setValue:[UIColor colorWithRed:(146.0f/255.0f) green:(146.0f/255.0f) blue:(146.0f/255.0f) alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    //pre_mob.layer.sublayerTransform = CATransform3DMakeTranslation(20.f, 0.0f, 0.0f);
    [scrollView addSubview:pre_mob];
    //pre_mob.layer.zPosition = 4;
    pre_mob.tag = 2;
    pre_mob.userInteractionEnabled = YES;
    [pre_mob addTarget:self action:@selector(select:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *premobile_div = [[UILabel alloc] initWithFrame:CGRectMake(70, pre_mob.frame.origin.y, 0.6f, 50)];
    [premobile_div setBackgroundColor:[UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f]];
    [scrollView addSubview:premobile_div];
    
    
    
    mobilePhone = [[UITextField alloc] initWithFrame:CGRectMake(71, 0, [UIScreen mainScreen].bounds.size.width-71, 50)];
    mobilePhone.backgroundColor = [UIColor whiteColor];
    mobilePhone.delegate = self;
    mobilePhone.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    mobilePhone.textColor = [UIColor blackColor];
    mobilePhone.keyboardAppearance = YES;
    mobilePhone.keyboardAppearance = UIKeyboardAppearanceDark;
    mobilePhone.placeholder = @"Mobile phone";
    [mobilePhone setValue:[UIColor colorWithRed:(146.0f/255.0f) green:(146.0f/255.0f) blue:(146.0f/255.0f) alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    mobilePhone.layer.sublayerTransform = CATransform3DMakeTranslation(20.f, 0.0f, 0.0f);
    [scrollView addSubview:mobilePhone];
    

    UILabel *mobile_div = [[UILabel alloc] initWithFrame:CGRectMake(0, 50, [UIScreen mainScreen].bounds.size.width, 0.6f)];
    [mobile_div setBackgroundColor:[UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f]];
    [scrollView addSubview:mobile_div];
    
    
    ///////////////////////////////////////////ISD CODE ///////////////////////////////////////////////////////
    pre_home = [UIButton buttonWithType:UIButtonTypeCustom];
    pre_home.frame = CGRectMake(0, mobile_div.frame.origin.y+mobile_div.frame.size.height, 70, 50);
    [pre_home setBackgroundColor:[UIColor whiteColor]];
    pre_home.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    [pre_home setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    //[pre_mob setValue:[UIColor colorWithRed:(146.0f/255.0f) green:(146.0f/255.0f) blue:(146.0f/255.0f) alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    //pre_mob.layer.sublayerTransform = CATransform3DMakeTranslation(20.f, 0.0f, 0.0f);
    [scrollView addSubview:pre_home];
    pre_home.layer.zPosition = 3;
    pre_home.tag = 3;
    pre_home.userInteractionEnabled = YES;
    [pre_home addTarget:self action:@selector(select:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *prehome_div = [[UILabel alloc] initWithFrame:CGRectMake(70, pre_home.frame.origin.y, 0.6f, 50)];
    [prehome_div setBackgroundColor:[UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f]];
    [scrollView addSubview:prehome_div];
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    homePhone = [[UITextField alloc] initWithFrame:CGRectMake(71, mobile_div.frame.origin.y+mobile_div.frame.size.height, [UIScreen mainScreen].bounds.size.width-71, 50)];
    homePhone.backgroundColor = [UIColor whiteColor];
    homePhone.delegate = self;
    homePhone.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    homePhone.textColor = [UIColor blackColor];
    homePhone.keyboardAppearance = UIKeyboardAppearanceDark;
    homePhone.placeholder = @"Home phone";
    [homePhone setValue:[UIColor colorWithRed:(146.0f/255.0f) green:(146.0f/255.0f) blue:(146.0f/255.0f) alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    homePhone.layer.sublayerTransform = CATransform3DMakeTranslation(20.f, 0.0f, 0.0f);
    [scrollView addSubview:homePhone];
    
    UILabel *homePhn_div = [[UILabel alloc] initWithFrame:CGRectMake(0, homePhone.frame.origin.y+homePhone.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.6f)];
    [homePhn_div setBackgroundColor:[UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f]];
    [scrollView addSubview:homePhn_div];
    
    
    email = [[UITextField alloc] initWithFrame:CGRectMake(0, homePhn_div.frame.origin.y+homePhn_div.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50)];
    email.backgroundColor = [UIColor whiteColor];
    email.delegate = self;
    email.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    email.textColor = [UIColor blackColor];
    email.keyboardAppearance = UIKeyboardAppearanceDark;
    email.autocapitalizationType = UITextAutocapitalizationTypeNone;
    email.placeholder = @"Email address";
    [email setValue:[UIColor colorWithRed:(146.0f/255.0f) green:(146.0f/255.0f) blue:(146.0f/255.0f) alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    email.layer.sublayerTransform = CATransform3DMakeTranslation(20.f, 0.0f, 0.0f);
    [scrollView addSubview:email];
    
    UILabel *email_div = [[UILabel alloc] initWithFrame:CGRectMake(0, email.frame.origin.y+email.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.6f)];
    [email_div setBackgroundColor:[UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f]];
    [scrollView addSubview:email_div];
    
    
    website = [[UITextField alloc] initWithFrame:CGRectMake(0, email_div.frame.origin.y+email_div.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50)];
    website.backgroundColor = [UIColor whiteColor];
    website.delegate = self;
    website.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    website.textColor = [UIColor blackColor];
    website.keyboardAppearance = UIKeyboardAppearanceDark;
    website.autocapitalizationType = UITextAutocapitalizationTypeNone;
    website.placeholder = @"Website";
    [website setValue:[UIColor colorWithRed:(146.0f/255.0f) green:(146.0f/255.0f) blue:(146.0f/255.0f) alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    website.layer.sublayerTransform = CATransform3DMakeTranslation(20.f, 0.0f, 0.0f);
    [scrollView addSubview:website];
    
    UILabel *website_div = [[UILabel alloc] initWithFrame:CGRectMake(0, website.frame.origin.y+website.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.6f)];
    [website_div setBackgroundColor:[UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f]];
    [scrollView addSubview:website_div];
    
    
    
    flagView = [[UIView alloc] initWithFrame:CGRectMake(0, website_div.frame.origin.y+website_div.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50)];
    flagView.backgroundColor=[UIColor whiteColor];
    [scrollView addSubview:flagView];
    
    flag_image = [[UIImageView alloc] initWithFrame:CGRectMake(20, 15, 25, 20)];
    flag_image.backgroundColor = [UIColor clearColor];
    flag_image.image = [UIImage imageNamed:flagCode];
    [flagView addSubview:flag_image];
    
    flag_name = [[UILabel alloc] init];
    flag_name.backgroundColor = [UIColor clearColor];
    flag_name.textColor = [UIColor colorWithRed:(0.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0];
    
    if ([[del.profdict objectForKey:@"profile"] objectForKey:@"country"] != false && ![[[del.profdict objectForKey:@"profile"] objectForKey:@"country"] isEqualToString:@"0"] && [[[del.profdict objectForKey:@"profile"] objectForKey:@"country"] length] > 1)
    {
        flag_name.text= [NSString stringWithFormat:@"%@",[[del.profdict objectForKey:@"profile"] objectForKey:@"country"]];
        flag_name.frame = CGRectMake(flag_image.frame.origin.x+flag_image.frame.size.width+15, 15, [[UIScreen mainScreen] bounds].size.width-120, 20);
        NSString *countrystr = [[NSString stringWithFormat:@"%@",[[del.profdict objectForKey:@"profile"] objectForKey:@"country"]] lowercaseString];
        countrystr =[NSString stringWithFormat:@"%@_32.png",countrystr];
        flag_image.image=[UIImage imageNamed:countrystr];
        
//        NSString *flagCodeaa;
//        for (check=0; check < countriesList.count; check++){
//            
//            if ([[[countriesList objectAtIndex:check]objectForKey:@"name"] isEqualToString:[personal_dict objectForKey:@"country"]]) {
//                
//                flagCodeaa = [[countriesFlags objectAtIndex:check]objectForKey:@"flag_128"];
//            }
//        }
        
    }
    else {
        flag_image.image=[UIImage imageNamed:flagCode];
        
        flag_name.text=[[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkCode"];
        flag_name.frame = CGRectMake(flag_image.frame.origin.x+flag_image.frame.size.width+15, 15, [[UIScreen mainScreen] bounds].size.width-120, 20);
    }
    
    [flagView addSubview:flag_name];
    
//    if([[[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkISOCode"] isKindOfClass:[NSNull class]] || [[[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkISOCode"] length] == 0){
//        
//        flag_name.frame = CGRectMake(20, 15, [[UIScreen mainScreen] bounds].size.width-120, 20);
//        flag_name.text = @"Select country";
//        
//    }else{
//        
//        flag_name.frame = CGRectMake(flag_image.frame.origin.x+flag_image.frame.size.width+15, 15, [[UIScreen mainScreen] bounds].size.width-120, 20);
//        flag_name.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"placemarkCode"];
//    }
    
    //    country.text=@"hgsdjg";
    
    
    UIImageView *down = [[UIImageView alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-36, website_div.frame.origin.y+website_div.frame.size.height+17, 16, 16)];
    [down setBackgroundColor:[UIColor clearColor]];
    down.image=[UIImage imageNamed:@"dnwarr.png"];
    [scrollView addSubview:down];
    down.userInteractionEnabled=YES;
    
    UIButton *down_btn = [[UIButton alloc] initWithFrame:CGRectMake(0, website_div.frame.origin.y+website_div.frame.size.height+11, [UIScreen mainScreen].bounds.size.width, 26)];
    [down_btn addTarget:self action:@selector(select:) forControlEvents:UIControlEventTouchUpInside];
    [down_btn setBackgroundColor:[UIColor clearColor]];
    [scrollView addSubview:down_btn];
    
    
    //    UITextField *flag = [[UITextField alloc] initWithFrame:CGRectMake(0, website_div.frame.origin.y+website_div.frame.size.height+1, [UIScreen mainScreen].bounds.size.width, 50)];
    //    flag.backgroundColor = [UIColor whiteColor];
    //    flag.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    //    flag.textColor = [UIColor blackColor];
    //    flag.placeholder = @"Flag";
    //    [flag setValue:[UIColor colorWithRed:(146.0f/255.0f) green:(146.0f/255.0f) blue:(146.0f/255.0f) alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    //    flag.layer.sublayerTransform = CATransform3DMakeTranslation(20.f, 0.0f, 0.0f);
    //    [self.view addSubview:flag];
    
    UILabel *flag_div = [[UILabel alloc] initWithFrame:CGRectMake(0, flagView.frame.origin.y+flagView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.6)];
    [flag_div setBackgroundColor:[UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f]];
    [scrollView addSubview:flag_div];
    
    
    zipcode = [[UITextField alloc] initWithFrame:CGRectMake(0, flag_div.frame.origin.y+flag_div.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50)];
    zipcode.backgroundColor = [UIColor whiteColor];
    zipcode.delegate = self;
    zipcode.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    zipcode.textColor = [UIColor blackColor];
    zipcode.keyboardAppearance = UIKeyboardAppearanceDark;
    zipcode.placeholder = @"Zipcode";
    [zipcode setValue:[UIColor colorWithRed:(146.0f/255.0f) green:(146.0f/255.0f) blue:(146.0f/255.0f) alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    zipcode.layer.sublayerTransform = CATransform3DMakeTranslation(20.f, 0.0f, 0.0f);
    [scrollView addSubview:zipcode];
    
    UILabel *zipcode_div = [[UILabel alloc] initWithFrame:CGRectMake(0, zipcode.frame.origin.y+zipcode.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.6f)];
    [zipcode_div setBackgroundColor:[UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f]];
    [scrollView addSubview:zipcode_div];
    
    
    city = [[UITextField alloc] initWithFrame:CGRectMake(0, zipcode_div.frame.origin.y+zipcode_div.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50)];
    city.backgroundColor = [UIColor whiteColor];
    city.delegate = self;
    city.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    city.textColor = [UIColor blackColor];
    city.keyboardAppearance = UIKeyboardAppearanceDark;
    city.placeholder = @"City";
    [city setValue:[UIColor colorWithRed:(146.0f/255.0f) green:(146.0f/255.0f) blue:(146.0f/255.0f) alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    city.layer.sublayerTransform = CATransform3DMakeTranslation(20.f, 0.0f, 0.0f);
    [scrollView addSubview:city];
    
    UILabel *city_div = [[UILabel alloc] initWithFrame:CGRectMake(0, city.frame.origin.y+city.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.6f)];
    [city_div setBackgroundColor:[UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f]];
    [scrollView addSubview:city_div];
    
    
    street = [[UITextField alloc] initWithFrame:CGRectMake(0, city_div.frame.origin.y+city_div.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50)];
    street.backgroundColor = [UIColor whiteColor];
    street.delegate = self;
    street.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    street.textColor = [UIColor blackColor];
    street.keyboardAppearance = UIKeyboardAppearanceDark;
    street.placeholder = @"Address";
    [street setValue:[UIColor colorWithRed:(146.0f/255.0f) green:(146.0f/255.0f) blue:(146.0f/255.0f) alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    street.layer.sublayerTransform = CATransform3DMakeTranslation(20.f, 0.0f, 0.0f);
    [scrollView addSubview:street];
    
    UILabel *street_div = [[UILabel alloc] initWithFrame:CGRectMake(0, street.frame.origin.y+street.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.6f)];
    [street_div setBackgroundColor:[UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f]];
    [scrollView addSubview:street_div];
    
    
    if ([_toUpdateInfo isEqualToString:@"YES"])
    {
        _toUpdateInfo = @"";
        [self loadData];
    }
    else
    {
    
        [self displayData];
    }
}

-(void)loadData
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        [self startLoader];
        
    }];
    
    [[NSOperationQueue new] addOperationWithBlock:^{
        
    del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    prefs = [NSUserDefaults standardUserDefaults];
    
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=profile&id=%@&access_token=%@&device_id=%@&image=true", [prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
    
    DebugLog(@"own profile url: %@",urlString1);
    
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    
    if (signeddataURL1 != nil)
    {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
          
        NSError *error = nil;
        
        json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                 
                                                options:kNilOptions
                 
                                                  error:&error];
        DebugLog(@"own json returns: %@",json1);
        
        NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
        DebugLog(@"err  %@",errornumber);
        
        if (![errornumber isEqualToString:@"0"])
        {
            DebugLog(@"if if");
            NSString *err_str = [json1 objectForKey:@"error"];
            alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                               message:err_str
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
        }
        else
        {
            del.personal_dict = [[NSMutableDictionary alloc]init];
            del.business_dict = [[NSMutableDictionary alloc]init];
            del.social_dict = [[NSMutableDictionary alloc]init];
            
            del.profdict= [[json1 objectForKey:@"details"] mutableCopy];
            
            [[NSUserDefaults standardUserDefaults] setObject:del.profdict forKey:@"loadedProfDict"];
            DebugLog(@"FROM WEB:%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"loadedProfDict"]);
            NSDictionary *dict_profile = [del.profdict objectForKey:@"profile"];
            DebugLog(@"array gives: %@",dict_profile);
            for( NSString *aKey in [dict_profile allKeys])
            {
                NSString *newString = [aKey substringToIndex:2];
                if ([newString isEqualToString:@"b_"])
                {
                    [del.business_dict setValue:[dict_profile valueForKey:[NSString stringWithFormat:@"%@",aKey]] forKey:[NSString stringWithFormat:@"%@",aKey]];
                    
                }
                else if ([newString isEqualToString:@"s_"])
                {
                    [del.social_dict setValue:[dict_profile valueForKey:[NSString stringWithFormat:@"%@",aKey]] forKey:[NSString stringWithFormat:@"%@",aKey]];
                    
                }
                else
                {
                    [del.personal_dict setValue:[dict_profile valueForKey:[NSString stringWithFormat:@"%@",aKey]] forKey:[NSString stringWithFormat:@"%@",aKey]];
                    
                    
                }
            }
            
            [self displayData];
            
        }
            }];
    }
        else
        {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                alert = [[UIAlertView alloc] initWithTitle:@"Error!" message:@"Server Failed to Response" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }];
        }
        
     }];
    
}

-(void)startLoader
{
    act = [[UIActivityIndicatorView alloc] init];//WithFrame:CGRectMake(135, 190, 40, 40)];
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    act.center = scrollView.center;
    [act setColor:[UIColor blackColor]];
    [scrollView addSubview:act];
    [act startAnimating];
}
-(void)stopLoader
{
    [act removeFromSuperview];
}

-(void)displayData
{
    if ([[del.profdict objectForKey:@"profile"] objectForKey:@"mobile_pre"] != false){
        
        //pre_mob.titleLabel.text= [NSString stringWithFormat:@"%@",[[del.profdict objectForKey:@"profile"] objectForKey:@"mobile_pre"]];
        [pre_mob setTitle:[NSString stringWithFormat:@"%@",[[del.profdict objectForKey:@"profile"] objectForKey:@"mobile_pre"]] forState:UIControlStateNormal];
    }
    else
    {
        [pre_mob setTitle:@"" forState:UIControlStateNormal];
    }
    
    if ([[del.profdict objectForKey:@"profile"] objectForKey:@"mobile_num"] != false){
        
        mobilePhone.text= [NSString stringWithFormat:@"%@",[[del.profdict objectForKey:@"profile"] objectForKey:@"mobile_num"]];
    }
    else{
        mobilePhone.text=@"";
    }
    
    if ([[del.profdict objectForKey:@"profile"] objectForKey:@"phone_pre"] != false){
        
        //pre_home.titleLabel.text= [NSString stringWithFormat:@"%@",[[del.profdict objectForKey:@"profile"] objectForKey:@"phone_pre"]];
        [pre_home setTitle:[NSString stringWithFormat:@"%@",[[del.profdict objectForKey:@"profile"] objectForKey:@"phone_pre"]] forState:UIControlStateNormal];
    }
    else{
        [pre_home setTitle:@"" forState:UIControlStateNormal];
    }
    
    if ([[del.profdict objectForKey:@"profile"] objectForKey:@"phone_num"] != false){
        
        homePhone.text= [NSString stringWithFormat:@"%@",[[del.profdict objectForKey:@"profile"] objectForKey:@"phone_num"]];
    }
    else{
        homePhone.text=@"";
    }
    
    if ([[del.profdict objectForKey:@"profile"] objectForKey:@"email"] != false){
        
        email.text= [NSString stringWithFormat:@"%@",[[del.profdict objectForKey:@"profile"] objectForKey:@"email"]];
    }
    else{
        
        email.text=@"";
    }
    
    if ([[del.profdict objectForKey:@"profile"] objectForKey:@"website"] != false){
        
        website.text= [NSString stringWithFormat:@"%@",[[del.profdict objectForKey:@"profile"] objectForKey:@"website"]];
    }
    else{
        
        website.text=@"";
    }
    
    if ([[del.profdict objectForKey:@"profile"] objectForKey:@"city"] != false){
        
        city.text= [NSString stringWithFormat:@"%@",[[del.profdict objectForKey:@"profile"] objectForKey:@"city"]];
    }
    else{
        
        city.text=@"";
    }
    
    if ([[del.profdict objectForKey:@"profile"] objectForKey:@"street"] != false){
        
        street.text= [NSString stringWithFormat:@"%@",[[del.profdict objectForKey:@"profile"] objectForKey:@"street"]];
    }
    else{
        street.text=@"";
    }
    
    if ([[del.profdict objectForKey:@"profile"] objectForKey:@"zipcode"] != false){
        
        zipcode.text= [NSString stringWithFormat:@"%@",[[del.profdict objectForKey:@"profile"] objectForKey:@"zipcode"]];
    }
    else{
        
        zipcode.text=@"";
    }
    
//    if ([personal_dict objectForKey:@"housenumber"] != false){
//        
//        numbertext.text= [NSString stringWithFormat:@"%@",[personal_dict objectForKey:@"housenumber"]];
//    }
//    else{
//        
//        numbertext.text=@"";
//    }

}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [countriesList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    if (tableView == isdcode_table) {
        
        UITableViewCell *cell = [isdcode_table dequeueReusableCellWithIdentifier:CellIdentifier];
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor=[UIColor clearColor];
        
        UILabel *name=[[UILabel alloc]initWithFrame:CGRectMake(80, 14, 150, 22)];
        name.backgroundColor=[UIColor clearColor];
        name.text=[NSString stringWithFormat:@"%@",[[isdcode_list objectAtIndex:indexPath.row] objectForKey:@"name"]];
        name.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
        name.textColor=[UIColor blackColor];
        name.textAlignment=NSTextAlignmentLeft;
        [cell addSubview:name];
        
        
        UILabel *code=[[UILabel alloc]initWithFrame:CGRectMake(10, 14, 50, 22)];
        code.backgroundColor=[UIColor clearColor];
        code.text=[NSString stringWithFormat:@"%@",[[isdcode_list objectAtIndex:indexPath.row] objectForKey:@"dial_code"]];
        code.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
        code.textColor=[UIColor blackColor];
        code.textAlignment=NSTextAlignmentCenter;
        [cell addSubview:code];
        
        UILabel *separatorlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 49.5, [UIScreen mainScreen].bounds.size.width, 0.5)];
        separatorlabel.backgroundColor=[UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f];
        [cell addSubview:separatorlabel];
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return cell;
    }
    else
    {
    
        
    UITableViewCell *cell = [countrycodetable dequeueReusableCellWithIdentifier:CellIdentifier];
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell.backgroundColor=[UIColor clearColor];
    
    UIImageView *cellimg=[[UIImageView alloc]initWithFrame:CGRectMake(10, 14, 35, 22)];
    cellimg.image=[UIImage imageNamed:[[countriesList objectAtIndex:indexPath.row] objectForKey:@"flag_32"]];
    cellimg.clipsToBounds=YES;
    cellimg.contentMode=UIViewContentModeScaleToFill;
    [cell addSubview:cellimg];
    
    UILabel *name=[[UILabel alloc]initWithFrame:CGRectMake(60, 0, 230, 50)];
    name.backgroundColor=[UIColor clearColor];
    name.text=[NSString stringWithFormat:@"%@",[[countriesList objectAtIndex:indexPath.row] objectForKey:@"name"]];
    name.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
    name.textColor=[UIColor blackColor];
    [cell addSubview:name];
    
    UILabel *separatorlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 49.5, [UIScreen mainScreen].bounds.size.width, 0.5)];
    separatorlabel.backgroundColor=[UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f];
    [cell addSubview:separatorlabel];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    countrylabel.text = @"";
    //    countryimg.image = nil;
    
    if (tableView == isdcode_table) {
        
        if (pre_mob_btn) {
            
            //pre_mob.titleLabel.text = [NSString stringWithFormat:@"%@",[[isdcode_list objectAtIndex:indexPath.row]objectForKey:@"dial_code"]];
            [pre_mob setTitle:[NSString stringWithFormat:@"%@",[[isdcode_list objectAtIndex:indexPath.row]objectForKey:@"dial_code"]] forState:UIControlStateNormal];
            DebugLog(@"BTN TEXT:%@",pre_mob.titleLabel.text);
            pre_mob.titleLabel.textColor = [UIColor blackColor];
            pre_home_btn = NO;
            isdcode_table.hidden=YES;
        }
        if (pre_home_btn) {
            
            //pre_home.titleLabel.text = [NSString stringWithFormat:@"%@",[[isdcode_list objectAtIndex:indexPath.row]objectForKey:@"dial_code"]];
            [pre_home setTitle:[NSString stringWithFormat:@"%@",[[isdcode_list objectAtIndex:indexPath.row]objectForKey:@"dial_code"]] forState:UIControlStateNormal];
            DebugLog(@"BTN TEXT:%@",pre_home.titleLabel.text);
            pre_home.titleLabel.textColor = [UIColor blackColor];
            pre_home_btn = NO;
            isdcode_table.hidden=YES;
            
        }
       
        
    }
    
    else
    {
    
    flag_name.frame = CGRectMake(flag_image.frame.origin.x+flag_image.frame.size.width+15, 15, [[UIScreen mainScreen] bounds].size.width-120, 20);
    flag_name.text = [NSString stringWithFormat:@"%@",[[countriesList objectAtIndex:indexPath.row]objectForKey:@"name"]];
    flag_name.textColor = [UIColor blackColor];
    flag_image.image = [UIImage imageNamed:[[countriesList objectAtIndex:indexPath.row] objectForKey:@"flag_128"]];
    countrycodetable.hidden=YES;
    
        
    }
    // backbutton.tag = 1;
    
    //    CATransition *animation = [CATransition animation];
    //
    //    [animation setType:kCATransitionFade];
    //
    //    animation.duration = 0.2f;
    //
    //    [countrycodetable.layer addAnimation:animation forKey:nil];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 50;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    DebugLog(@"Dismiss keyboard");
    [textField resignFirstResponder];
    return YES;
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField.tag==4)
    {
        if (self.view.bounds.size.height<=480) {
            //mainview.contentOffset=CGPointMake(0, 100);
            //            mainview.frame= CGRectMake(0, -50, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
        }
    }
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [scrollView setContentOffset:CGPointMake(0, 0)];
    [textField resignFirstResponder];
    if (textField.tag==4)
    {
        if (self.view.bounds.size.height<=480) {
            // mainview.contentOffset=CGPointMake(0, 0);
            //            mainview.frame= CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
        }
    }
    
    return YES;
}


-(void)goback
{
    del.tabBarController.tabBar.hidden = NO;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)save
{
    DebugLog(@"gotonext");
    
    [mobilePhone resignFirstResponder];
    [homePhone resignFirstResponder];
    [email resignFirstResponder];
    [website resignFirstResponder];
    [zipcode resignFirstResponder];
    [city resignFirstResponder];
    [street resignFirstResponder];
    
    
    [UIView animateWithDuration:0.0
                     animations:^{
                         
                         [SVProgressHUD showWithStatus:@"Saving......"];
                         
                         
                     }
                     completion:^(BOOL finished){
                         
                         NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
                         NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
                         
//                         NSString *mobile_pre_en = [self encodeToPercentEscapeString:isdlabel.text];
//                         NSString *phone_pre_en = [self encodeToPercentEscapeString:isdphone.text];
//                         NSString *bphone_pre_en = [self encodeToPercentEscapeString:bisdlabel.text];
                         
                         //DebugLog(@"Encoded text: %@ %@ %@", mobile_pre_en, phone_pre_en, bphone_pre_en);
                         
                         
                       if ([[mobilePhone.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
                         {
                             alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your Mobile Number"
                                                                message:Nil
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                             save_btn.alpha = 1.0f;
                             [alert show];
                             
                         }
                         
                         else if([[homePhone.text stringByTrimmingCharactersInSet:whitespace] length]>0)
                         {
                             if ([[homePhone.text stringByTrimmingCharactersInSet:whitespace] length] <8 || [[homePhone.text stringByTrimmingCharactersInSet:whitespace] length] > 10 || !([homePhone.text rangeOfCharacterFromSet:notDigits].location == NSNotFound))
                         {
                             alert = [[UIAlertView alloc] initWithTitle:@"Please Enter a Valid Phone Number"
                                                                message:Nil
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                             save_btn.alpha = 1.0f;
                             [alert show];
                             [SVProgressHUD dismiss];
                             
                         }
                         }
                         
                         if ([[email.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
                         {
                             alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your Email"
                                                                message:Nil
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                             
                             save_btn.alpha = 1.0f;
                             [alert show];
                             [SVProgressHUD dismiss];
                             
                         }
                         else if(![self NSStringIsValidEmail:email.text])
                         {
                             email.text =nil;
                             alert =[[UIAlertView alloc]initWithTitle:@"Email Id is Invalid" message:Nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                             
                             save_btn.alpha = 1.0f;
                             [alert show];
                             [SVProgressHUD dismiss];
                             
                             return;
                         }
                         else if ([[website.text stringByTrimmingCharactersInSet:whitespace] length] > 0 && ![self validateUrl:website.text])
                         {
                             alert = [[UIAlertView alloc] initWithTitle:@"Please Enter a valid Website"
                                                                message:Nil
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                             save_btn.alpha = 1.0f;
                             [alert show];
                             [SVProgressHUD dismiss];
                             
                         }
                         
                         else
                         {
                             DebugLog(@"FLAG:%@",flag_name.text);
                             
                             prefs = [NSUserDefaults standardUserDefaults];
                             NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=editprofile&email=%@&website=%@&mobile_num=%@&phone_num=%@&street=%@&zipcode=%@&city=%@&country=%@&access_token=%@&device_id=%@",[email.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[website.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[mobilePhone.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[homePhone.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[street.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[zipcode.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[city.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[flag_name.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
                             
                             DebugLog(@"deny url: %@",urlString1);
                             NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
                             
                             NSError *error= nil;
                             NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
                             
                             if (signeddataURL1 != nil) {
                                 
                                 NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                                           options:kNilOptions
                                                                                             error:&error];
                                 DebugLog(@"deny json returns: %@",json_deny);
                                 
                                 if ([[json_deny objectForKey:@"success"]intValue] == 1)
                                 {
                                     [self editpic];
                                     [del ownDetails];
                                     save_btn.alpha = 1.0f;
                                     
                                     alert = [[UIAlertView alloc] initWithTitle:@"Changes have been Successfully Saved!"
                                                                        message:nil
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                                     [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"profile_edited"];
                                     
                                     [alert show];
                                     [SVProgressHUD dismiss];
                                     
//                                     CATransition *transition = [CATransition animation];
//                                     
//                                     transition.duration = 0.4f;
//                                     
//                                     transition.type = kCATransitionFade;
//                                     
                                     del.tabBarController.tabBar.hidden = NO;
//                                     [[self navigationController].view.layer addAnimation:transition forKey:nil];
                                     [self.navigationController popViewControllerAnimated:YES];
                                     
                                 }
                                 
                             }
                             else
                             {
                                 [SVProgressHUD dismiss];
                                 save_btn.alpha = 1.0f;
                                 alert = [[UIAlertView alloc] initWithTitle:@"Error in server connection!"
                                                                    message:nil
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                                 
                                 [alert show];
                             }
                             
                             
                         }
                     }];
    
}

-(void)select:(UIButton *)sender
{
    
    if ((int)sender.tag == 2) {
        
        pre_mob_btn = YES;
        isdcode_table=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-64)];
        [self.view addSubview:isdcode_table];
        [self.view bringSubviewToFront:isdcode_table];
        
        isdcode_table.backgroundColor=[UIColor whiteColor];
        isdcode_table.dataSource=self;
        isdcode_table.delegate=self;
        isdcode_table.separatorStyle=UITableViewCellSeparatorStyleNone;
        isdcode_table.hidden=NO;
        isdcode_table.showsVerticalScrollIndicator=NO;
        isdcode_table.layer.borderColor=[[UIColor grayColor]CGColor];
        isdcode_table.layer.borderWidth=1;
        isdcode_table.layer.zPosition=3;
        
        
    }
    else
    {
        if ((int)sender.tag==3) {
            
            pre_home_btn = YES;
            isdcode_table=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-64)];
            [self.view addSubview:isdcode_table];
            [self.view bringSubviewToFront:isdcode_table];
            
            isdcode_table.backgroundColor=[UIColor whiteColor];
            isdcode_table.dataSource=self;
            isdcode_table.delegate=self;
            isdcode_table.separatorStyle=UITableViewCellSeparatorStyleNone;
            isdcode_table.hidden=NO;
            isdcode_table.showsVerticalScrollIndicator=NO;
            isdcode_table.layer.borderColor=[[UIColor grayColor]CGColor];
            isdcode_table.layer.borderWidth=1;
            isdcode_table.layer.zPosition=3;
        }
    else
    {
    countrycodetable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, self.view.bounds.size.width,self.view.bounds.size.height-64)];
    [self.view addSubview:countrycodetable];
    countrycodetable.backgroundColor=[UIColor whiteColor];
    countrycodetable.dataSource=self;
    countrycodetable.delegate=self;
    countrycodetable.separatorStyle=UITableViewCellSeparatorStyleNone;
    //    countrycodetable.hidden=YES;
    countrycodetable.showsVerticalScrollIndicator=NO;
    countrycodetable.layer.borderColor=[[UIColor grayColor]CGColor];
    countrycodetable.layer.borderWidth=1;
    }
    //backbutton.tag=2;
    //    nxt.hidden=YES;
    //    next.hidden=YES;
    }
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (textField == mobilePhone)
    {
        if (scrollView.contentOffset.y < 0)
            [scrollView setContentOffset:CGPointMake(0, 0)];
    }
    if (textField == homePhone)
    {
        if (scrollView.contentOffset.y < 50)
            [scrollView setContentOffset:CGPointMake(0, 0)];
    }
    if (textField == email)
    {
        if (scrollView.contentOffset.y < 100)
            [scrollView setContentOffset:CGPointMake(0, 0)];
    }
    if (textField == website)
    {
        if (scrollView.contentOffset.y < 150)
            [scrollView setContentOffset:CGPointMake(0,51)];
    }
    if (textField == zipcode)
    {
        if (scrollView.contentOffset.y < 200)
            [scrollView setContentOffset:CGPointMake(0, 153)];
    }
    if (textField == city)
    {
        if (scrollView.contentOffset.y < 250)
            [scrollView setContentOffset:CGPointMake(0, 204)];
    }
    if (textField == street)
    {
        if (scrollView.contentOffset.y < 300)
            [scrollView setContentOffset:CGPointMake(0, 255)];
    }
   
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)changeprofpic: (UIGestureRecognizer *)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose Your Action"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Open Camera", @"Choose From Gallery",nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [actionSheet showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
            
        case 0:
        {
            DebugLog(@"here camera");
            [self camera_func];
        }
            break;
            
        case 1:
        {
            [self gallery_func];
        }
            break;
    }
}

-(void)camera_func
{
    DebugLog(@"camera func");
    if ([self isCameraAvailable] && [self doesCameraSupportTakingPhotos]) {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypeCamera;
        if ([self isFrontCameraAvailable]) {
            controller.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        }
        NSMutableArray *mediaTypes = [[NSMutableArray alloc] init];
        [mediaTypes addObject:(__bridge NSString *)kUTTypeImage];
        controller.mediaTypes = mediaTypes;
        controller.delegate = self;
        [self presentViewController:controller
                           animated:YES
                         completion:^(void){
                             DebugLog(@"Picker View Controller is presented");
                         }];
    }
}

-(void)gallery_func
{
    DebugLog(@"gallery func");
    if ([self isPhotoLibraryAvailable]) {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        NSMutableArray *mediaTypes = [[NSMutableArray alloc] init];
        [mediaTypes addObject:(__bridge NSString *)kUTTypeImage];
        controller.mediaTypes = mediaTypes;
        controller.delegate = self;
        [self presentViewController:controller
                           animated:YES
                         completion:^(void){
                             DebugLog(@"Picker View Controller is presented");
                         }];
    }
    
}

- (BOOL) isCameraAvailable{
    return [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
}

- (BOOL) isRearCameraAvailable{
    return [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear];
}

- (BOOL) isFrontCameraAvailable {
    return [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront];
}

- (BOOL) doesCameraSupportTakingPhotos {
    return [self cameraSupportsMedia:(__bridge NSString *)kUTTypeImage sourceType:UIImagePickerControllerSourceTypeCamera];
}

- (BOOL) isPhotoLibraryAvailable{
    return [UIImagePickerController isSourceTypeAvailable:
            UIImagePickerControllerSourceTypePhotoLibrary];
}
- (BOOL) canUserPickVideosFromPhotoLibrary{
    return [self
            cameraSupportsMedia:(__bridge NSString *)kUTTypeMovie sourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}
- (BOOL) canUserPickPhotosFromPhotoLibrary{
    return [self
            cameraSupportsMedia:(__bridge NSString *)kUTTypeImage sourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}

- (BOOL) cameraSupportsMedia:(NSString *)paramMediaType sourceType:(UIImagePickerControllerSourceType)paramSourceType{
    __block BOOL result = NO;
    if ([paramMediaType length] == 0) {
        return NO;
    }
    NSArray *availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:paramSourceType];
    [availableMediaTypes enumerateObjectsUsingBlock: ^(id obj, NSUInteger idx, BOOL *stop) {
        NSString *mediaType = (NSString *)obj;
        if ([mediaType isEqualToString:paramMediaType]){
            result = YES;
            *stop= YES;
        }
    }];
    return result;
}

- (BOOL) validateUrl: (NSString *) candidate {
    NSString *urlRegEx =
    @"(http|https)://((\\w\\w\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    
    NSString *urlRegEx1 =  @"((\\w\\w\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    
    if ([urlTest evaluateWithObject:candidate]) {
        
        return [urlTest evaluateWithObject:candidate];
    }
    else
    {
        NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx1];
        if ([urlTest evaluateWithObject:candidate]) {
            return [urlTest evaluateWithObject:candidate];
        }
        else
        {
            return false;
        }
        
    }
}


-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    /*BOOL stricterFilter = YES;
     NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
     NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
     NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
     NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
     return [emailTest evaluateWithObject:checkString];*/
    checkString = [checkString lowercaseString];
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:checkString];
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    DebugLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        DebugLog(@"currentLocation.coordinate.longitude=========>%f",currentLocation.coordinate.longitude);
        DebugLog(@"currentLocation.coordinate.longitude=========>%f",currentLocation.coordinate.latitude);
    }
    //    [locationManager stopUpdatingLocation];
    
    DebugLog(@"Resolving the Address");
    
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemrk, NSError *error) {
        DebugLog(@"Found placemarks: %@, error: %@", placemrk, error);
        if (error == nil && [placemrk count] > 0) {
            placemark = [placemrk lastObject];
            
            DebugLog(@"ADDRESS===================>%@ %@\n%@ %@\n%@\n%@\n%@",placemark.subThoroughfare, placemark.thoroughfare,
                     placemark.postalCode, placemark.locality,
                     placemark.administrativeArea,
                     placemark.country,placemark.ISOcountryCode);
            
            [[NSUserDefaults standardUserDefaults] setObject:placemark.ISOcountryCode forKey:@"placemarkISOCode"];
            [[NSUserDefaults standardUserDefaults] setObject:placemark.country forKey:@"placemarkCode"];
            
            [locationManager stopUpdatingLocation];
//            [act stopAnimating];
//            [act setHidden:YES];
        } else {
            DebugLog(@"%@", error.debugDescription);
        }
    }];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    DebugLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

-(void)editpic
{
    prefs = [NSUserDefaults standardUserDefaults];
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=editpicture&access_token=%@&device_id=%@",[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
    
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    
    [request setHTTPShouldHandleCookies:NO];
    
    [request setURL:[NSURL URLWithString:newString1]];
    
    [request setTimeoutInterval:10];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *imageData = UIImagePNGRepresentation(portraitImg1);
    NSString *base64Stringa = [imageData base64EncodedStringWithOptions:0];
    
    if ( [imageData length] > 0)
        //   if (pic_send ==1)
    {
        DebugLog(@"portrait img data is not nil");
        //if ([base64Stringa length] > 6)
        //        {
        //            NSData *decodedData1 = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
        //            //    NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
        //
        //            UIImage *profilepic1 = [UIImage imageWithData:decodedData1];
        //
        //            prof_img.image=profilepic1;
        //            prof_img.contentMode= UIViewContentModeScaleAspectFill;
        //            prof_img.clipsToBounds=YES;
        //        }
        
        //[profdict objectForKey:@"image"];
        
        //        NSString *boundary = [NSString stringWithFormat:@"%0.9u",arc4random()];
        //
        //        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        //
        //        [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
        //
        //        NSMutableData *body = [NSMutableData data];
        //
        //        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        //
        //        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image\"; filename=\".png\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        //
        //        [body appendData:[[NSString stringWithFormat:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        //
        //        [body appendData:[NSData dataWithData:UIImagePNGRepresentation(portraitImg1)]];
        //
        //        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        //
        //        [request setHTTPBody:body];
        
        NSString *params = [[NSString alloc] initWithFormat:@"image=%@",[base64Stringa stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"]];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        
        NSURLResponse *response = nil;
        NSError *error= nil;
        NSData *signeddataURL1 = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                  options:kNilOptions
                                                                    error:&error];
        DebugLog(@"deny json returns: %@",json_deny);
        
        if ([[json_deny objectForKey:@"success"]intValue] == 1)
        {
            
            profile_image.image = portraitImg1;
            
            [del.profdict setObject:[NSString stringWithFormat:@"%@",base64Stringa] forKey:@"image"];
            
            [[NSUserDefaults standardUserDefaults] setObject:del.profdict forKey:@"loadedProfDict"];
            //            if ([base64Stringa length] > 6)
            //            {
            //                NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
            //                //    NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
            //
            //                UIImage *profilepic = [UIImage imageWithData:decodedData];
            //
            //                prof_img.image=profilepic;
            //                prof_img.contentMode= UIViewContentModeScaleAspectFill;
            //                prof_img.clipsToBounds=YES;
            //            }
            //
            
//            ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
//            con.other=@"no";
//            con.uid=[prefs objectForKey:@"id"];
//            
//            CATransition* transition = [CATransition animation];
//            
//            transition.duration = 0.4;
//            transition.type = kCATransitionPush;
//            transition.subtype = kCATransitionFade;
//            
//            [[self navigationController].view.layer addAnimation:transition forKey:nil];
//            
//            [self.navigationController pushViewController:con animated:YES];
        }
    }
    else
    {
//        ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
//        con.other=@"no";
//        con.uid=[prefs objectForKey:@"id"];
//        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
//        
//        [self.navigationController pushViewController:con animated:YES];
    }
}

- (void)imageCropper:(VPImageCropperViewController *)cropperViewController didFinished:(UIImage *)editedImage {
    profile_image.image = editedImage;
    portraitImg1 =editedImage;
    profile_image.contentMode=UIViewContentModeScaleAspectFill;
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
        // TO DO
    }];
    
    //    NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(editedImage)];
    //    //    NSData *plainData = [plainString dataUsingEncoding:NSUTF8StringEncoding];
    //
    //    UIImage* image = [UIImage imageWithData:imageData];
    //
    //    base64String = [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:0];
    //
    ////    base64String = [imageData base64EncodedStringWithOptions:0];
    //    DebugLog(@"%@", base64String); // Zm9
    //
    //    NSString *encodedText = [self encodeToPercentEscapeString:base64String];
    //    DebugLog(@"Encoded text: %@", encodedText);
    //
    //    //    NSString *strImageData = [base64String stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    //
    //    [[NSUserDefaults standardUserDefaults]setObject:encodedText forKey:@"r_prof_image"];
    
    NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(editedImage)];
    //    NSData *plainData = [plainString dataUsingEncoding:NSUTF8StringEncoding];
    base64String = [imageData base64EncodedStringWithOptions:0];
    //    DebugLog(@"%@", base64String); // Zm9
    DebugLog(@"Encoded text: %@", base64String);
    
    //    NSString *encodedText = [self encodeToPercentEscapeString:base64String];
    
    //    NSString *strImageData = [base64String stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    
    // [[NSUserDefaults standardUserDefaults]setObject:base64String forKey:@"r_prof_image"];
    
    
    picuploaded=1;
}

- (void)imageCropperDidCancel:(VPImageCropperViewController *)cropperViewController {
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (UIImage *)imageByScalingToMaxSize:(UIImage *)sourceImage {
    if (sourceImage.size.height < ORIGINAL_MAX_HEIGHT) return sourceImage;
    CGFloat btWidth = 0.0f;
    CGFloat btHeight = 0.0f;
    //    if (sourceImage.size.height > sourceImage.size.width) {
    //        btHeight = ORIGINAL_MAX_HEIGHT;
    //        btWidth = sourceImage.size.height * (ORIGINAL_MAX_HEIGHT / sourceImage.size.width);
    //    } else {
    //        btHeight = ORIGINAL_MAX_HEIGHT;
    //        btWidth = sourceImage.size.width * (ORIGINAL_MAX_HEIGHT / sourceImage.size.height);
    //    }
    if (sourceImage.size.height > sourceImage.size.width) {
        btHeight = ORIGINAL_MAX_HEIGHT;
        btWidth = sourceImage.size.width * (ORIGINAL_MAX_HEIGHT / sourceImage.size.height);
    } else {
        btHeight = ORIGINAL_MAX_HEIGHT;
        btWidth = sourceImage.size.width * (ORIGINAL_MAX_HEIGHT / sourceImage.size.height);
    }
    
    CGSize targetSize = CGSizeMake(btWidth, btHeight);
    return [self imageByScalingAndCroppingForSourceImage:sourceImage targetSize:targetSize];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^() {
        
        UIImage *portraitImg = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        portraitImg = [self imageByScalingToMaxSize:portraitImg];
        
        pathss = [NSTemporaryDirectory() stringByAppendingPathComponent:@"image.png"];
        NSData *imageData = UIImagePNGRepresentation(portraitImg);
        //you can also use UIImageJPEGRepresentation(img,1); for jpegs
        [imageData writeToFile:pathss atomically:YES];
        
        // present the cropper view controller
        VPImageCropperViewController *imgCropperVC = [[VPImageCropperViewController alloc] initWithImage:portraitImg cropFrame:CGRectMake(35, (self.view.frame.size.height- (self.view.frame.size.width-70))/2, self.view.frame.size.width-70, self.view.frame.size.width-70) limitScaleRatio:3.0];
        imgCropperVC.delegate = self;
        [self presentViewController:imgCropperVC animated:YES completion:^{
            // TO DO
        }];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:^(){
    }];
}

#pragma mark image scale utility

- (UIImage *)imageByScalingAndCroppingForSourceImage:(UIImage *)sourceImage targetSize:(CGSize)targetSize {
    DebugLog(@"called scaling&croppingforsize");
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    if (CGSizeEqualToSize(imageSize, targetSize) == NO)
    {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor > heightFactor)
            scaleFactor = widthFactor; // scale to fit height
        else
            scaleFactor = heightFactor; // scale to fit width
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else
            if (widthFactor < heightFactor)
            {
                thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
            }
    }
    UIGraphicsBeginImageContext(targetSize); // this will crop
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil) DebugLog(@"could not scale image");
    
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    return newImage;
}


-(void)changeColorAgain
{
    save_btn.alpha = 1.0f;
}
-(void)changeColor
{
    save_btn.alpha = 0.5f;
}

-(void)backColorAgain
{
    bck_img.alpha = 1.0f;
}
-(void)backColor
{
    bck_img.alpha = 0.5f;
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
//    if ([[touch view] isKindOfClass:[Tiles class]]) {
        NSLog(@"[touch view].tag = %@  %ld",[touch view], [touch view].tag);
//    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
