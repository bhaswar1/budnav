//
//  ConAddressSignupViewController.h
//  Budnav
//
//  Created by Sohini's on 24/03/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <AddressBook/AddressBook.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "GAITrackedViewController.h"

@interface ConAddressSignupViewController : GAITrackedViewController<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource, UIScrollViewDelegate>
{
    UIView *coverview,*divider1,*divider2,*divider3,*divider4;
    UITextField *city,*zipcode,*street;
    UILabel *country,*details,*bck,*nxt;
    UIImageView *down,*back,*next,*flag;
    UIButton *backbutton,*nextbutton;
    UIPickerView *countrypicker;
    UIToolbar *numberToolbar;
    int check;
    NSArray *countriesList;
    NSString *flagCode, *err_str;
    UITableView *countrycodetable;
    NSMutableData *responseData;
    UIScrollView *mainview;
}
@property (nonatomic, strong) NSString *Code;
@property(nonatomic, readwrite) NSMutableDictionary *add_contacts_dict, *ns;

@end
