//
//  ConAddFriendViewController.h
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 03/06/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConParentTopBarViewController.h"
#import <MessageUI/MessageUI.h>
#import "GAITrackedViewController.h"

@interface ConAddFriendViewController : GAITrackedViewController<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate,UIActionSheetDelegate,UISearchBarDelegate>
{
    UIActionSheet *anActionSheet;
    UISearchBar *searchBar;
}
@property (nonatomic, readwrite) UIView *mainview;
@property (nonatomic, readwrite) NSMutableArray *selectedIndexPaths, *sendarray;
@property (nonatomic, readwrite) NSString *searchfriends;
@property (nonatomic, readwrite) UIButton *invite_send;
+(void)chngpostion;
@end
