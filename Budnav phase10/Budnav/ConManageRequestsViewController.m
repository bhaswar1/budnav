//
//  ConManageRequestsViewController.m
//  Contacter
//  Created by Bhaswar's MacBook Air on 24/05/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
#import "ConManageRequestsViewController.h"
#import "AppDelegate.h"
#import "ConProfileOwnViewController.h"
#import "ConAccountSettingsViewController.h"
#import "ConInviteViewController.h"
#import "SVProgressHUD.h"
#import "ConSyncLoaderViewController.h"
#import "ConRequestViewController.h"
#import "ConQROptionsViewController.h"
#import "ConQROptionsViewController.h"
#import "ConNewProfileViewController.h"
#import "ConAddFriendViewController.h"
#import "ConNewRequestsViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"
#import "ConLocateGroupViewController.h"

@interface ConManageRequestsViewController ()
{
    int move,status,userid;
    UIView *mainview;
    NSString *currentstatus_str, *table_type;
    NSDictionary *import_dict;
    UITableView *tab_request;
    NSMutableArray *connection_type_arr;
    UIButton *acceptchange, *denybt, *currentstatus;
    NSDictionary *json1;
}
@end

@implementation ConManageRequestsViewController
@synthesize imported_arr,userid;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    move=0;
    status=0;
    connection_type_arr= [[NSMutableArray alloc]init];
    connection_type_arr= [ @[@"Friend", @"Business"] mutableCopy ];
    
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    self.navigationController.navigationBarHidden=YES;
    
    mainview = [[UIView alloc]initWithFrame:CGRectMake(0, 65, [UIScreen mainScreen].bounds.size.width, self.view.frame.size.height-60)];
    [self.view addSubview:mainview];
    mainview.backgroundColor=[UIColor whiteColor];
    
    //    [SVProgressHUD showWithStatus:@"Loading"];
    UIActivityIndicatorView *act = [[UIActivityIndicatorView alloc] init];
    act.center = self.view.center;
    [act startAnimating];
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [act setColor:[UIColor blackColor]];
    [mainview addSubview:act];
    
    DebugLog(@"reload dictionary called here");
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=profile&id=%d&access_token=%@&device_id=%@&image=true",userid,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
        
        DebugLog(@"profile url: %@",urlString1);
        
        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
        
        if (signeddataURL1 == nil)
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            //            [alert show];
            //            [SVProgressHUD dismiss];
            [act removeFromSuperview];
        }
        else
        {
            NSError *error=nil;
            
            json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                     
                                                    options:kNilOptions
                     
                                                      error:&error];
            DebugLog(@"1st json returns: %@",json1);
            
            NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
            DebugLog(@"err  %@",errornumber);
            
            if (![errornumber isEqualToString:@"0"])
            {
                DebugLog(@"if if");
                NSString *err_str = [json1 objectForKey:@"error"];
                alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                                   message:err_str
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                //        [alert show];
                //        [SVProgressHUD dismiss];
                [act removeFromSuperview];
            }
            else
                import_dict = [json1 objectForKey:@"details"];
            
            //    [self reloadTheDictionary];
            
            //    CGRect frame = CGRectMake(110,165,100,100);
            //    UIActivityIndicatorView *act = [[UIActivityIndicatorView alloc] initWithFrame:frame];
            //    [act startAnimating];
            //    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
            //    [act setColor:[UIColor grayColor]];
            //    [mainview addSubview:act];
            
            //    userid = [[import_dict objectForKey:@"id"]intValue];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIImageView *prof_img = [[UIImageView alloc]initWithFrame:CGRectMake(15, 75-60, 90, 90)];
            [mainview addSubview:prof_img];
            
            base64String= [import_dict objectForKey:@"image"];
            if ([base64String length] >6)
            {
                NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
                //    NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
                UIImage *profilepic = [UIImage imageWithData:decodedData];
                prof_img.image=profilepic;
                //        prof_img.contentMode= UIViewContentModeScaleAspectFit;
                prof_img.clipsToBounds=YES;
            }
            
            NSDictionary *profdict= [json1 objectForKey:@"details"];
            NSDictionary *dict_profile = [profdict objectForKey:@"profile"];
            
            fullname= [NSString stringWithFormat:@"%@ %@",[dict_profile objectForKey:@"name"],[dict_profile objectForKey:@"surname"]];
            UILabel *prof_name = [[UILabel alloc]init]; //WithFrame:CGRectMake(112, 72, 196, 60)];
            [mainview addSubview:prof_name];
            prof_name.backgroundColor=[UIColor clearColor];
            prof_name.textColor=[UIColor blackColor];
            prof_name.text=fullname;
            //    prof_name.font=[UIFont fontWithName:@"Mark Simonson - Proxima Nova Alt Condensed Bold" size:19];
            prof_name.font=[UIFont fontWithName:@"ProximaNova-Bold" size:20];
            prof_name.numberOfLines=0;
            
            CGSize maximumSize = CGSizeMake(196, 60);
            
            UIFont *myFont = [UIFont fontWithName:@"ProximaNova-Bold" size:20];
            CGSize myStringSize = [fullname sizeWithFont:myFont
                                   
                                       constrainedToSize:maximumSize
                                   
                                           lineBreakMode:NSLineBreakByWordWrapping];
            prof_name.frame=CGRectMake(115, 75-60, 196, myStringSize.height);
            
            NSString *location;
            if ([[dict_profile objectForKey:@"city"] isKindOfClass:[NSNull class]] || [dict_profile objectForKey:@"city"] == (id)[NSNull null] || [[dict_profile objectForKey:@"city"] length] ==0)
            {
                location= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"country"]];
            }
            else
                location= [NSString stringWithFormat:@"%@,%@",[dict_profile objectForKey:@"city"],[dict_profile objectForKey:@"country"]];
            
            UILabel *locationlb = [[UILabel alloc]initWithFrame:CGRectMake(115, prof_name.frame.origin.y + prof_name.frame.size.height+5, 210, 20)];
            [mainview addSubview:locationlb];
            locationlb.backgroundColor=[UIColor clearColor];
            locationlb.text=location;
            locationlb.font=[UIFont fontWithName:@"ProximaNova-Regular" size:12];
            locationlb.textAlignment=NSTextAlignmentLeft;
            
            [self loadViews];
            //            [SVProgressHUD dismiss];
            [act removeFromSuperview];
        });
    });
    [del showTabValues:YES];
}


-(void)loadViews
{
    [tab_request removeFromSuperview];
    [currentstatus removeFromSuperview];
    [acceptchange removeFromSuperview];
    [denybt removeFromSuperview];
    
    tab_request=[[UITableView alloc]initWithFrame:CGRectMake(10, 145, 80, 60)];
    [mainview addSubview:tab_request];
    tab_request.backgroundColor=[UIColor clearColor];
    tab_request.dataSource=self;
    tab_request.delegate=self;
    tab_request.separatorStyle=UITableViewCellSeparatorStyleNone;
    tab_request.showsVerticalScrollIndicator=NO;
    tab_request.hidden=YES;
    tab_request.layer.borderWidth=1;
    tab_request.layer.borderColor=[[UIColor grayColor]CGColor];
    tab_request.layer.cornerRadius=5;
    tab_request.scrollEnabled=NO;
    
    currentstatus = [UIButton buttonWithType:UIButtonTypeCustom];
    //    currentstatus.frame = CGRectMake(15, 120, 75, 25);
    [mainview addSubview:currentstatus];
    currentstatus.layer.cornerRadius=5;
    currentstatus.backgroundColor=[UIColor clearColor];
    currentstatus.layer.borderWidth=1;
    currentstatus.layer.borderColor=[[UIColor grayColor]CGColor];
    [currentstatus setTitleColor:[UIColor colorWithRed:97.0f/255.0f green:132.0f/255.0f blue:83.0f/255.0f alpha:1] forState:UIControlStateNormal];
    
    
    
    if ([[import_dict objectForKey:@"business"]intValue] == 0 && [[import_dict objectForKey:@"friend"]intValue] == 0)
    {
        if ([[import_dict objectForKey:@"requestReceived"]intValue] == 1)
        {
            DebugLog(@"5555");
            if ([[import_dict objectForKey:@"requestBusiness"]intValue] == 1)
            {
                DebugLog(@"6666");
                currentstatus.titleLabel.text=@"Accept Business";
                [currentstatus setTitle:@"Accept Business" forState:UIControlStateNormal];
                currentstatus_str = @"Accept Business";
                CGSize maximumSize1 = CGSizeMake(240, 25);
                
                UIFont *myFont1 = [UIFont fontWithName:@"ProximaNova-Bold" size:20];
                CGSize myStringSize1 = [currentstatus_str sizeWithFont:myFont1
                                        
                                                     constrainedToSize:maximumSize1
                                        
                                                         lineBreakMode:NSLineBreakByWordWrapping];
                currentstatus.frame=CGRectMake(15, 120, myStringSize1.width, 25);
                [currentstatus setTitleColor:[UIColor colorWithRed:97.0f/255.0f green:132.0f/255.0f blue:83.0f/255.0f alpha:1] forState:UIControlStateNormal];
                [currentstatus addTarget:self action:@selector(accept:) forControlEvents:UIControlEventTouchUpInside];
            }
            else
            {
                DebugLog(@"7777");
                currentstatus.titleLabel.text=@"Accept";
                [currentstatus setTitle:@"Accept" forState:UIControlStateNormal];
                currentstatus_str = @"Accept";
                CGSize maximumSize1 = CGSizeMake(240, 25);
                
                UIFont *myFont1 = [UIFont fontWithName:@"ProximaNova-Bold" size:20];
                CGSize myStringSize1 = [currentstatus_str sizeWithFont:myFont1
                                        
                                                     constrainedToSize:maximumSize1
                                        
                                                         lineBreakMode:NSLineBreakByWordWrapping];
                currentstatus.frame=CGRectMake(10, 120, myStringSize1.width, 25);
                [currentstatus setTitleColor:[UIColor colorWithRed:97.0f/255.0f green:132.0f/255.0f blue:83.0f/255.0f alpha:1] forState:UIControlStateNormal];
                [currentstatus addTarget:self action:@selector(accept:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            denybt= [UIButton buttonWithType:UIButtonTypeCustom];
            denybt.frame = CGRectMake(currentstatus.frame.origin.x+currentstatus.frame.size.width+10, 120, 65, 25);
            [mainview addSubview:denybt];
            denybt.layer.cornerRadius=5;
            denybt.backgroundColor=[UIColor clearColor];
            denybt.titleLabel.text=@"Deny";
            [denybt setTitle:@"Deny" forState:UIControlStateNormal];
            [denybt setTitleColor:[UIColor colorWithRed:126.0f/255.0f green:85.0f/255.0f blue:85.0f/255.0f alpha:1] forState: UIControlStateNormal];
            denybt.layer.borderColor=[[UIColor grayColor]CGColor];
            denybt.layer.borderWidth=1;
            [denybt addTarget:self action:@selector(deny:) forControlEvents:UIControlEventTouchUpInside];
            
        }
        
        //    if ([[import_dict objectForKey:@"business"]intValue] == 0 && [[import_dict objectForKey:@"friends"]intValue] == 0)
        //    {
        DebugLog(@"1111");
        if ([[import_dict objectForKey:@"requestReceived"]intValue] == 0)
        {
            DebugLog(@"2222");
            if ([[import_dict objectForKey:@"requestSent"]intValue] == 1)
            {
                DebugLog(@"3333");
                currentstatus.titleLabel.text=@"Cancel Request";
                [currentstatus setTitle:@"Cancel Request" forState:UIControlStateNormal];
                currentstatus_str = @"Cancel Request";
                CGSize maximumSize1 = CGSizeMake(240, 25);
                
                UIFont *myFont1 = [UIFont fontWithName:@"ProximaNova-Bold" size:20];
                CGSize myStringSize1 = [currentstatus_str sizeWithFont:myFont1
                                        
                                                     constrainedToSize:maximumSize1
                                        
                                                         lineBreakMode:NSLineBreakByWordWrapping];
                currentstatus.frame=CGRectMake(10, 120, myStringSize1.width, 25);
                [currentstatus setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                [currentstatus addTarget:self action:@selector(cancelRequest:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            else
            {
                DebugLog(@"4444");
                currentstatus.titleLabel.text=@"Add";
                [currentstatus setTitle:@"Add" forState:UIControlStateNormal];
                currentstatus_str = @"Add";
                CGSize maximumSize1 = CGSizeMake(240, 25);
                
                UIFont *myFont1 = [UIFont fontWithName:@"ProximaNova-Bold" size:20];
                CGSize myStringSize1 = [currentstatus_str sizeWithFont:myFont1
                                        
                                                     constrainedToSize:maximumSize1
                                        
                                                         lineBreakMode:NSLineBreakByWordWrapping];
                currentstatus.frame=CGRectMake(10, 120, myStringSize1.width, 25);
                if (myStringSize1.width < 80)
                    currentstatus.frame=CGRectMake(10, 120, 80, 25);
                [currentstatus setTitleColor:[UIColor colorWithRed:97.0f/255.0f green:132.0f/255.0f blue:83.0f/255.0f alpha:1] forState:UIControlStateNormal];
                [currentstatus addTarget:self action:@selector(addOptions:) forControlEvents:UIControlEventTouchUpInside];
            }
        }
        
        else
        {
            
        }
        
    }
    
    else if ([[import_dict objectForKey:@"business"]intValue] == 1)
    {
        [currentstatus setTitle:@"Business" forState:UIControlStateNormal];
        currentstatus_str = @"Business";
        CGSize maximumSize1 = CGSizeMake(240, 25);
        
        UIFont *myFont1 = [UIFont fontWithName:@"ProximaNova-Bold" size:20];
        CGSize myStringSize1 = [currentstatus_str sizeWithFont:myFont1
                                
                                             constrainedToSize:maximumSize1
                                
                                                 lineBreakMode:NSLineBreakByWordWrapping];
        currentstatus.frame=CGRectMake(10, 120, myStringSize1.width, 25);
        [currentstatus setTitleColor:[UIColor colorWithRed:17.0f/255.0f green:61.0f/255.0f blue:125.0f/255.0f alpha:1] forState:UIControlStateNormal];
        [currentstatus addTarget:self action:@selector(addOptions_Business:) forControlEvents:UIControlEventTouchUpInside];
        
        
        if ([[import_dict objectForKey:@"requestReceived"]intValue] == 1)
        {
            DebugLog(@"5555");
            if ([[import_dict objectForKey:@"requestBusiness"]intValue] == 0)
            {
                DebugLog(@"6666");
                acceptchange = [UIButton buttonWithType:UIButtonTypeCustom];
                [mainview addSubview:acceptchange];
                acceptchange.layer.borderWidth=1;
                acceptchange.layer.borderColor=[[UIColor grayColor]CGColor];
                acceptchange.layer.cornerRadius=5;
                acceptchange.titleLabel.text=@"Accept Change to Friends";
                [acceptchange setTitle:@"Accept Change to Friends" forState:UIControlStateNormal];
                [acceptchange setTitleColor:[UIColor colorWithRed:97.0f/255.0f green:132.0f/255.0f blue:83.0f/255.0f alpha:1] forState: UIControlStateNormal];
                NSString *acceptchangestr = @"Accept Change to Friends";
                CGSize maximumSize1 = CGSizeMake(240, 25);
                
                UIFont *myFont1 = [UIFont fontWithName:@"ProximaNova-Regular" size:18];
                CGSize myStringSize1 = [acceptchangestr sizeWithFont:myFont1
                                        
                                                   constrainedToSize:maximumSize1
                                        
                                                       lineBreakMode:NSLineBreakByWordWrapping];
                acceptchange.frame=CGRectMake(currentstatus.frame.origin.x + currentstatus.frame.size.width+5, 120, myStringSize1.width+5, 25);
                [acceptchange addTarget:self action:@selector(accept:) forControlEvents:UIControlEventTouchUpInside];
                
                
                denybt= [UIButton buttonWithType:UIButtonTypeCustom];
                denybt.frame = CGRectMake(acceptchange.frame.origin.x, 150, 65, 25);
                [mainview addSubview:denybt];
                denybt.layer.cornerRadius=5;
                denybt.backgroundColor=[UIColor clearColor];
                denybt.titleLabel.text=@"Deny";
                [denybt setTitle:@"Deny" forState:UIControlStateNormal];
                [denybt setTitleColor:[UIColor colorWithRed:126.0f/255.0f green:85.0f/255.0f blue:85.0f/255.0f alpha:1] forState: UIControlStateNormal];
                denybt.layer.borderColor=[[UIColor grayColor]CGColor];
                denybt.layer.borderWidth=1;
                [denybt addTarget:self action:@selector(deny:) forControlEvents:UIControlEventTouchUpInside];
            }
        }
        
        else if ([[import_dict objectForKey:@"requestSent"]intValue] == 1)
        {
            acceptchange = [UIButton buttonWithType:UIButtonTypeCustom];
            [mainview addSubview:acceptchange];
            acceptchange.layer.borderWidth=1;
            acceptchange.layer.borderColor=[[UIColor grayColor]CGColor];
            acceptchange.layer.cornerRadius=5;
            acceptchange.titleLabel.text=@"Cancel Request";
            [acceptchange setTitle:@"Cancel Request" forState:UIControlStateNormal];
            [acceptchange setTitleColor:[UIColor blackColor] forState: UIControlStateNormal];
            NSString *acceptchangestr = @"Cancel Request";
            CGSize maximumSize1 = CGSizeMake(240, 25);
            
            UIFont *myFont1 = [UIFont fontWithName:@"ProximaNova-Regular" size:18];
            CGSize myStringSize1 = [acceptchangestr sizeWithFont:myFont1
                                    
                                               constrainedToSize:maximumSize1
                                    
                                                   lineBreakMode:NSLineBreakByWordWrapping];
            acceptchange.frame=CGRectMake(currentstatus.frame.origin.x + currentstatus.frame.size.width+5, 120, myStringSize1.width+5, 25);
            [acceptchange setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [acceptchange addTarget:self action:@selector(cancelRequest:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    
    else if ([[import_dict objectForKey:@"friend"]intValue] == 1)
    {
        [currentstatus setTitle:@"Friends" forState:UIControlStateNormal];
        currentstatus_str = @"Friends";
        CGSize maximumSize1 = CGSizeMake(240, 25);
        
        UIFont *myFont1 = [UIFont fontWithName:@"ProximaNova-Bold" size:20];
        CGSize myStringSize1 = [currentstatus_str sizeWithFont:myFont1
                                
                                             constrainedToSize:maximumSize1
                                
                                                 lineBreakMode:NSLineBreakByWordWrapping];
        currentstatus.frame=CGRectMake(10, 120, myStringSize1.width, 25);
        if (myStringSize1.width < 80)
            currentstatus.frame=CGRectMake(10, 120, 80, 25);
        [currentstatus setTitleColor:[UIColor colorWithRed:97.0f/255.0f green:132.0f/255.0f blue:83.0f/255.0f alpha:1] forState:UIControlStateNormal];
        [currentstatus addTarget:self action:@selector(addOptions_Friends:) forControlEvents:UIControlEventTouchUpInside];
        
        if ([[import_dict objectForKey:@"requestReceived"]intValue] == 1)
        {
            DebugLog(@"5555");
            if ([[import_dict objectForKey:@"requestBusiness"]intValue] == 1)
            {
                DebugLog(@"6666");
                acceptchange = [UIButton buttonWithType:UIButtonTypeCustom];
                [mainview addSubview:acceptchange];
                acceptchange.layer.borderWidth=1;
                acceptchange.layer.borderColor=[[UIColor grayColor]CGColor];
                acceptchange.layer.cornerRadius=5;
                acceptchange.titleLabel.text=@"Accept Change to Business";
                [acceptchange setTitle:@"Accept Change to Business" forState:UIControlStateNormal];
                [acceptchange setTitleColor:[UIColor colorWithRed:126.0f/255.0f green:85.0f/255.0f blue:85.0f/255.0f alpha:1] forState: UIControlStateNormal];
                NSString *acceptchangestr = @"Accept Change to Business";
                CGSize maximumSize1 = CGSizeMake(240, 25);
                
                UIFont *myFont1 = [UIFont fontWithName:@"ProximaNova-Regular" size:18];
                CGSize myStringSize1 = [acceptchangestr sizeWithFont:myFont1
                                        
                                                   constrainedToSize:maximumSize1
                                        
                                                       lineBreakMode:NSLineBreakByWordWrapping];
                acceptchange.frame=CGRectMake(currentstatus.frame.origin.x + currentstatus.frame.size.width+5, 120, myStringSize1.width+5, 25);
                [acceptchange addTarget:self action:@selector(accept:) forControlEvents:UIControlEventTouchUpInside];
                
                
                denybt= [UIButton buttonWithType:UIButtonTypeCustom];
                denybt.frame = CGRectMake(acceptchange.frame.origin.x, 150, 65, 25);
                [mainview addSubview:denybt];
                denybt.layer.cornerRadius=5;
                denybt.backgroundColor=[UIColor clearColor];
                denybt.titleLabel.text=@"Deny";
                [denybt setTitle:@"Deny" forState:UIControlStateNormal];
                [denybt setTitleColor:[UIColor colorWithRed:126.0f/255.0f green:85.0f/255.0f blue:85.0f/255.0f alpha:1] forState: UIControlStateNormal];
                denybt.layer.borderColor=[[UIColor grayColor]CGColor];
                denybt.layer.borderWidth=1;
                [denybt addTarget:self action:@selector(deny:) forControlEvents:UIControlEventTouchUpInside];
            }
        }
        
        else if ([[import_dict objectForKey:@"requestSent"]intValue] == 1)
        {
            acceptchange = [UIButton buttonWithType:UIButtonTypeCustom];
            [mainview addSubview:acceptchange];
            acceptchange.layer.borderWidth=1;
            acceptchange.layer.borderColor=[[UIColor grayColor]CGColor];
            acceptchange.layer.cornerRadius=5;
            acceptchange.titleLabel.text=@"Cancel Request";
            [acceptchange setTitle:@"Cancel Request" forState:UIControlStateNormal];
            [acceptchange setTitleColor:[UIColor blackColor] forState: UIControlStateNormal];
            NSString *acceptchangestr = @"Cancel Request";
            CGSize maximumSize1 = CGSizeMake(240, 25);
            
            UIFont *myFont1 = [UIFont fontWithName:@"ProximaNova-Regular" size:18];
            CGSize myStringSize1 = [acceptchangestr sizeWithFont:myFont1
                                    
                                               constrainedToSize:maximumSize1
                                    
                                                   lineBreakMode:NSLineBreakByWordWrapping];
            acceptchange.frame=CGRectMake(currentstatus.frame.origin.x + currentstatus.frame.size.width+5, 120, myStringSize1.width+5, 25);
            [acceptchange setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [acceptchange addTarget:self action:@selector(cancelRequest:) forControlEvents:UIControlEventTouchUpInside];
        }
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

+(void)chngpostion
{
    DebugLog(@"Change pos requests page");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Data" object:Nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getData:) name:@"Data" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(navtopage:) name:@"pagenav" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestreceivedaction) name:@"Requestreceived_push" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(acceptedrequestaction) name:@"Accepted_push" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AcceptedRequest) name:@"Accepted_request" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AddFriend) name:@"NewConnection" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ConnectionType) name:@"NewConnectionType" object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AcceptedRequest) name:@"TypeChange" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shareLocation) name:@"ShareLocation" object:nil];
    
}

-(void)shareLocation
{
    ConLocateGroupViewController *conLocate = [[ConLocateGroupViewController alloc]init];
    conLocate.group_id=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"sharedLocGrpId"]];
    [self.navigationController pushViewController:conLocate animated:YES];
}

-(void)ConnectionType
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDel.newrequestRedirect)
    {
    
    ConNewRequestsViewController *mng = [[ConNewRequestsViewController alloc]init];
       mng.userid = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userforrequest"] intValue];
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    appDel.newrequestRedirect = NO;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:mng animated:YES];
        
    }
}


-(void)AddFriend
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDel.newUser)
    {

    ConAddFriendViewController *con = [[ConAddFriendViewController alloc]init];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
        
        appDel.newUser = NO;
    
 //   [[self navigationController].view.layer addAnimation:transition forKey:nil];
    [[self navigationController] pushViewController:con animated:YES];
        
    }
    
}

-(void)AcceptedRequest
{
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (del.profileRedirect)
    {
    
    ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
    // con.other=@"yes";
    con.request = YES;
    [self.tabBarController.tabBar setHidden:YES];
    // con.uid=[NSString stringWithFormat:@"%d",[userid intValue]];
    //                [self.navigationController presentViewController:con animated:NO completion:nil];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
        
        del.profileRedirect = NO;
    
 //   [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
        
    }
    
}

-(void)requestreceivedaction
{
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
    ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
    con.other=@"yes";
    con.uid=[NSString stringWithFormat:@"%@",del.pushtoprofileid];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}

-(void)acceptedrequestaction
{
    ConRequestViewController *con = [[ConRequestViewController alloc]init];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}

-(void)navtopage: (NSNotification *)notification
{
    DebugLog(@"navtopage");
    NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
    if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"4"])
    {
        ConAccountSettingsViewController *con = [[ConAccountSettingsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"6"])
    {
        ConInviteViewController *con = [[ConInviteViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"5"])
    {
        ConSyncLoaderViewController *con = [[ConSyncLoaderViewController alloc]init];
        //        [self.navigationController pushViewController:con animated:NO];
        
        [self presentViewController:con animated:NO completion:nil];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"2"])
    {
//        ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
        ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"3"])
    {
        ConQROptionsViewController *con = [[ConQROptionsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    
    
    [mainview setFrame:CGRectMake(0, mainview.frame.origin.y, mainview.frame.size.width, mainview.frame.size.height)];
    //    [prefs setObject:@"111" forKey:@"whichpage"];
}


-(void)getData:(NSNotification *)notification {
    
    if(move == 0) {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-40, mainview.frame.origin.y, mainview.frame.size.width, mainview.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             move=1;
                         }];
    } else {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake(0, (mainview.frame.origin.y),mainview.frame.size.width, mainview.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             move=0;
                         }];
    }
}

-(void)viewDidDisappear:(BOOL)animated
{
    move=0;
    [mainview removeFromSuperview];
    [super viewDidDisappear:YES];
}

-(void)accept: (id)sender
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=acceptrequest&id=%d&access_token=%@&device_id=%@",userid,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
    
    DebugLog(@"accept url: %@",urlString1);
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    
    NSError *error=nil;
    NSDictionary *json_accept = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                options:kNilOptions
                                                                  error:&error];
    DebugLog(@"accept json returns: %@",json_accept);
    if ([[json_accept objectForKey:@"success"]intValue] == 1)
    {
        //    [self loadrequestsdata];
        //    tab_request =Nil;
        //    [tab_request reloadData];
        
        alert = [[UIAlertView alloc] initWithTitle:@"Request Successfully Accepted!"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        [self reloadTheDictionary];
        
        //        ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
        //        con.other=@"yes";
        //        con.uid=[NSString stringWithFormat:@"%d",[[import_dict objectForKey:@"id"] intValue]];
        //        [self.navigationController pushViewController:con animated:NO];
    }
}

-(void)deny: (id)sender
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=denyrequest&id=%d&access_token=%@&device_id=%@",userid,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
    
    DebugLog(@"deny url: %@",urlString1);
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    
    NSError *error=nil;
    NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                              options:kNilOptions
                                                                error:&error];
    DebugLog(@"deny json returns: %@",json_deny);
    
    if ([[json_deny objectForKey:@"success"]intValue] == 1)
    {
        //        [self loadrequestsdata];
        //        tab_request =Nil;
        //        [tab_request reloadData];
        
        alert = [[UIAlertView alloc] initWithTitle:@"Request Successfully Denied!"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        [self reloadTheDictionary];
        
        //        ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
        //        con.other=@"yes";
        //        con.uid=[NSString stringWithFormat:@"%d",[[import_dict objectForKey:@"id"] intValue]];
        //        [self.navigationController pushViewController:con animated:NO];
    }
    
}

-(void)addOptions: (id)sender
{
    connection_type_arr= [ @[@"Friend", @"Business"] mutableCopy ];
    [tab_request reloadData];
    table_type= @"addOptions";
    if (tab_request.hidden== YES)
        [tab_request setHidden:NO];
    else
        [tab_request setHidden:YES];
}

-(void)addOptions_Friends: (id)sender
{
    connection_type_arr= [ @[@"Business", @"Remove"] mutableCopy ];
    [tab_request reloadData];
    table_type= @"nowFriend";
    if (tab_request.hidden== YES)
        [tab_request setHidden:NO];
    else
        [tab_request setHidden:YES];
}

-(void)addOptions_Business: (id)sender
{
    connection_type_arr= [@[@"Friend", @"Remove"] mutableCopy];
    [tab_request reloadData];
    table_type= @"nowBusiness";
    if (tab_request.hidden== YES)
        [tab_request setHidden:NO];
    else
        [tab_request setHidden:YES];
}

-(void)cancelRequest: (id)sender
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=withdrawrequest&id=%d&access_token=%@&device_id=%@",userid,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
    
    DebugLog(@"deny url: %@",urlString1);
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    
    NSError *error=nil;
    NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                              options:kNilOptions
                                                                error:&error];
    DebugLog(@"deny json returns: %@",json_deny);
    
    if ([[json_deny objectForKey:@"success"]intValue] == 1)
    {
        //        [self loadrequestsdata];
        //        tab_request =Nil;
        //        [tab_request reloadData];
        
        alert = [[UIAlertView alloc] initWithTitle:@"Request Successfully Cancelled!"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        alert.delegate=self;
        //        UIImage *theImage = [UIImage imageNamed:@"nexta.png"];
        //        UIView *view=[alert valueForKey:@"_backgroundImageView"];
        //        //Set frame according to adustable
        //        UIImageView *image=[[UIImageView alloc] initWithImage:theImage];
        //        [image setFrame:CGRectMake(0, 0, 280, 130)];
        //        [view addSubview:image];
        [alert show];
        [self reloadTheDictionary];
        
        //        ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
        //        con.other=@"yes";
        //        con.uid=[NSString stringWithFormat:@"%d",[[import_dict objectForKey:@"id"] intValue]];
        //        [self.navigationController pushViewController:con animated:NO];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tab_request dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell.backgroundColor=[UIColor clearColor];
    
    UILabel *lab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
    [cell addSubview:lab];
    lab.backgroundColor=[UIColor clearColor];
    lab.text=[connection_type_arr objectAtIndex:indexPath.row];
    lab.textAlignment=NSTextAlignmentCenter;
    lab.textColor=[UIColor blueColor];
    
    UIView *sep = [[UIView alloc]initWithFrame:CGRectMake(0, 29.5f, 80, 1)];
    sep.backgroundColor=[UIColor grayColor];
    [cell addSubview:sep];
    
    if (indexPath.row == 0)
    {
        if ([table_type isEqualToString:@"nowFriend"])
            lab.textColor= [UIColor colorWithRed:17.0f/255.0f green:61.0f/255.0f blue:125.0f/255.0f alpha:1];
        
        else if ([table_type isEqualToString:@"nowBusiness"])
            lab.textColor= [UIColor colorWithRed:97.0f/255.0f green:132.0f/255.0f blue:83.0f/255.0f alpha:1];
        
        else
            lab.textColor= [UIColor colorWithRed:97.0f/255.0f green:132.0f/255.0f blue:83.0f/255.0f alpha:1];
    }
    else
    {
        if ([table_type isEqualToString:@"nowFriend"] || [table_type isEqualToString:@"nowBusiness"])
            lab.textColor= [UIColor colorWithRed:126.0f/255.0f green:85.0f/255.0f blue:85.0f/255.0f alpha:1];
        
        else
            lab.textColor= [UIColor colorWithRed:17.0f/255.0f green:61.0f/255.0f blue:125.0f/255.0f alpha:1];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        if ([table_type isEqualToString:@"addOptions"] || [table_type isEqualToString:@"nowBusiness"])
        {
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=addrequest&id=%d&access_token=%@&device_id=%@",userid,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
            
            DebugLog(@"deny url: %@",urlString1);
            NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
            
            NSError *error=nil;
            NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                      options:kNilOptions
                                                                        error:&error];
            DebugLog(@"deny json returns: %@",json_deny);
            
            if ([[json_deny objectForKey:@"success"]intValue] == 1)
            {
                
                alert = [[UIAlertView alloc] initWithTitle:@"Request Successfully Sent!"
                                                   message:nil
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                [alert show];
                [self reloadTheDictionary];
                
            }
            
        }
        else
        {
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=addrequest&id=%d&access_token=%@&device_id=%@&business=true",userid,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
            
            DebugLog(@"deny url: %@",urlString1);
            NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
            
            NSError *error=nil;
            NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                      options:kNilOptions
                                                                        error:&error];
            DebugLog(@"deny json returns: %@",json_deny);
            
            if ([[json_deny objectForKey:@"success"]intValue] == 1)
            {
                
                alert = [[UIAlertView alloc] initWithTitle:@"Request Successfully Sent!"
                                                   message:nil
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                [alert show];
                [self reloadTheDictionary];
            }
            
        }
    }
    
    else
    {
        if ([table_type isEqualToString:@"nowFriend"] || [table_type isEqualToString:@"nowBusiness"])
        {
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=removeconnection&id=%d&access_token=%@&device_id=%@&business=true",userid,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
            
            DebugLog(@"deny url: %@",urlString1);
            NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
            
            NSError *error=nil;
            NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                      options:kNilOptions
                                                                        error:&error];
            DebugLog(@"deny json returns: %@",json_deny);
            
            if ([[json_deny objectForKey:@"success"]intValue] == 1)
            {
                
                alert = [[UIAlertView alloc] initWithTitle:@"Connection Successfully Removed!"
                                                   message:nil
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                [alert show];
                [self reloadTheDictionary];
            }
            else
            {
                alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                                   message:[NSString stringWithFormat:@"%@",[import_dict objectForKey:@"error"]]
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                [alert show];
                [self reloadTheDictionary];
            }
            
        }
        else
        {
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=addrequest&id=%d&access_token=%@&device_id=%@&business=true",userid,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
            
            DebugLog(@"deny url: %@",urlString1);
            NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
            
            NSError *error=nil;
            NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                      options:kNilOptions
                                                                        error:&error];
            DebugLog(@"deny json returns: %@",json_deny);
            
            if ([[json_deny objectForKey:@"success"]intValue] == 1)
            {
                
                alert = [[UIAlertView alloc] initWithTitle:@"Request Successfully Sent!"
                                                   message:nil
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                [alert show];
                [self reloadTheDictionary];
            }
            
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 30;
}

-(void) reloadTheDictionary
{
    DebugLog(@"reload dictionary called here");
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=profile&id=%d&access_token=%@&device_id=%@&image=true",userid,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
    
    DebugLog(@"profile url: %@",urlString1);
    
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    
    if (signeddataURL1 == nil)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        [SVProgressHUD dismiss];
    }
    else
    {
        NSError *error=nil;
        
        json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                 
                                                options:kNilOptions
                 
                                                  error:&error];
        //    DebugLog(@"json returns: %@",json1);
        
        NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
        DebugLog(@"err  %@",errornumber);
        
        if (![errornumber isEqualToString:@"0"])
        {
            DebugLog(@"if if");
            NSString *err_str = [json1 objectForKey:@"error"];
            alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                               message:err_str
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
            [SVProgressHUD dismiss];
        }
        else
            import_dict = [json1 objectForKey:@"details"];
    }
    [self loadViews];
    
}
@end