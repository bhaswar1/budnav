//
//  ConNewContactsViewController.h
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 16/07/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConParentTopBarViewController.h"
#import <MessageUI/MessageUI.h>
#import <AddressBook/AddressBook.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITrackedViewController.h"

@interface ConNewContactsViewController : GAITrackedViewController<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate,UIActionSheetDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,UISearchBarDelegate,MKMapViewDelegate,NSURLConnectionDelegate, NSURLConnectionDataDelegate>
{
    //   UIView *mainview;
    UIActionSheet *anActionSheet;
    UISearchBar *searchBar;
    MKMapView *map_View;
    UILabel *name,*surname,*locationlb;
    UIImageView *selectedrowbg,*divider;
    UIButton *phonebt,*chatbt,*smsbt,*mapBtn;
}
@property (nonatomic, strong) UIView *mainview;
@property (nonatomic, readwrite) NSMutableArray *selectedIndexPaths, *backuparr;
@property (nonatomic) UITableView *tab_contact;
@property(nonatomic, readwrite) NSMutableDictionary *add_contacts_dict, *ns;
@property(nonatomic, retain) NSMutableArray *check_app_cont1, *addtophonebook, *existingphn, *appdelwebarray;
@property(nonatomic, readwrite) NSDictionary *getbackupdict;
@property(nonatomic, readwrite) NSArray *backarrnumbers;
@property(nonatomic, readwrite) NSString *pass,*phnstr,*finalph;
+(void)chngpostion;
//-(void) loadrequestsfromweb;
-(void) loadContactsFromWeb;
-(void)reloadAgain;
typedef void(^myCompleton)(BOOL);

@end


//    googleMapUrlString = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%f,%f&daddr=%f,%f", userlat,userlong, latitude, longitude];
//    appleMapUrlString = [NSString stringWithFormat:@"http://maps.apple.com/maps?saddr=%f,%f&daddr=%f,%f", userlat,userlong, latitude, longitude];
//    ConNavigateViewController *con = [[ConNavigateViewController alloc]init];
//    con.fireurl = googleMapUrlString;
//    [self.navigationController pushViewController:con animated:NO];
