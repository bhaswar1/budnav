//
//  SocialWebViewController.h
//  Contacter
//
//  Created by Anirban Tah on 14/05/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface SocialWebViewController : GAITrackedViewController<UIWebViewDelegate,UIAlertViewDelegate>

@property (strong, nonatomic) NSString *type_social;
@end
