#import "RFScreenshot.h"
#import "ConPreSignUpViewController.h"
#import "ConStartViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "SVProgressHUD.h"
#import "ConSignupViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

#define LEFT_FRAME CGRectMake(0, 0, CGRectGetMidX(self.view.frame), CGRectGetHeight(self.view.frame))
#define RIGHT_FRAME CGRectMake(CGRectGetMidX(self.view.frame), 0, CGRectGetMidX(self.view.frame), CGRectGetHeight(self.view.frame))
#define kDefaultAnimationDuration 1.0

@interface ConPreSignUpViewController (){
    
    NSTimer *timerr;
    UIActivityIndicatorView *act;
    UIButton *loginbt;
}
@property (nonatomic, strong) UIImageView* imageView;
@end
@implementation ConPreSignUpViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
    UIImageView *viewimg=[[UIImageView alloc]initWithFrame:CGRectMake(20, 120, 280, 188)];
    viewimg.image=[UIImage imageNamed:@"preconsignup.png"];
    [self.view addSubview:viewimg];
    
    //Activity Indicator============================//
    act = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(135,410,50,50)];
    [act startAnimating];
    [act setHidden:NO];
    //    [act setBackgroundColor:[UIColor grayColor]];
    act.layer.cornerRadius = 5.0f;
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [act setColor:[UIColor grayColor]];
    [self.view addSubview:act];
    
    
    loginbt = [UIButton buttonWithType:UIButtonTypeCustom];
    loginbt.frame = CGRectMake(15, 335, 290, 40);
    loginbt.layer.cornerRadius=5;
    [loginbt setTitle:@"Start" forState:UIControlStateNormal];
    loginbt.titleLabel.textColor=[UIColor whiteColor];
    loginbt.backgroundColor=[UIColor colorWithRed:141.0f/255.0f green:195.0f/255.0f blue:128.0f/255.0f alpha:1];
    [loginbt addTarget:self action:@selector(gonext:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:loginbt];
    [loginbt setUserInteractionEnabled:NO];
    loginbt.titleLabel.font=[UIFont fontWithName:@"ProximaNova-Regular" size:20];
    
    UIButton *helpbt = [UIButton buttonWithType:UIButtonTypeCustom];
    helpbt.frame = CGRectMake(15, self.view.bounds.size.height-65, 290, 30);
    helpbt.layer.cornerRadius=5;
    helpbt.titleLabel.font=[UIFont fontWithName:@"ProximaNova-Regular" size:18];
    [helpbt setTitle:@"Back" forState:UIControlStateNormal];
    [helpbt setTitleColor:[UIColor colorWithRed:17.0f/255.0f green:61.0f/255.0f blue:125.0f/255.0f alpha:1] forState:UIControlStateNormal];
    helpbt.backgroundColor=[UIColor clearColor];
    [helpbt addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:helpbt];
    
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate=self;
    locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    locationManager.distanceFilter=kCLDistanceFilterNone;
    
    [locationManager startUpdatingLocation];
    geocoder = [[CLGeocoder alloc] init];

    NSUInteger code = [CLLocationManager authorizationStatus];
    if (code == kCLAuthorizationStatusNotDetermined && ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)] || [locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]))
    {
        // choose one request according to your business.
        if([[NSBundle mainBundle]
            objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"]){
            DebugLog(@"NSLocationAlwaysUsageDescription");
            [locationManager requestAlwaysAuthorization];
            
        } else if([[NSBundle mainBundle]
                   objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
            DebugLog(@"NSLocationWhenInUseUsageDescription");
            [locationManager requestWhenInUseAuthorization];
            
        } else {
            DebugLog(@"Info.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription");
           }
        }
                  
                  
//    
//      
//    [SVProgressHUD showWithStatus:@"Fetching Your Current Location"];

    timerr = [NSTimer scheduledTimerWithTimeInterval:15.0
                                              target:self
                                            selector:@selector(targetMethod:)
                                            userInfo:nil
                                             repeats:NO];
}

-(void)targetMethod:(NSTimer *)timer{
    
    [locationManager stopUpdatingLocation];
    [SVProgressHUD dismiss];
    [act stopAnimating];
    [act setHidden:YES];
    [loginbt setUserInteractionEnabled:YES];
    
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    DebugLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        DebugLog(@"currentLocation.coordinate.longitude=========>%f",currentLocation.coordinate.longitude);
        DebugLog(@"currentLocation.coordinate.longitude=========>%f",currentLocation.coordinate.latitude);
    }
    //    [locationManager stopUpdatingLocation];
    
    DebugLog(@"Resolving the Address");
    
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemrk, NSError *error) {
        DebugLog(@"Found placemarks: %@, error: %@", placemrk, error);
        if (error == nil && [placemrk count] > 0) {
            placemark = [placemrk lastObject];
            
DebugLog(@"ADDRESS===================>%@ %@\n%@ %@\n%@\n%@\n%@",placemark.subThoroughfare, placemark.thoroughfare,
                  placemark.postalCode, placemark.locality,
                  placemark.administrativeArea,
                  placemark.country,placemark.ISOcountryCode);
            
            [[NSUserDefaults standardUserDefaults] setObject:placemark.ISOcountryCode forKey:@"placemarkISOCode"];
            [[NSUserDefaults standardUserDefaults] setObject:placemark.country forKey:@"placemarkCode"];
            
            [locationManager stopUpdatingLocation];
            [act stopAnimating];
            [act setHidden:YES];
            [loginbt setUserInteractionEnabled:YES];
            [SVProgressHUD dismiss];
        } else {
            DebugLog(@"%@", error.debugDescription);
        }
    }];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    DebugLog(@"didFailWithError: %@", error);

    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [SVProgressHUD dismiss];
    [super viewDidDisappear:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)gonext:(id)sender
{
//    ConStartViewController *pre= [[ConStartViewController alloc]init];
    ConSignupViewController *pre = [[ConSignupViewController alloc]init];
    [self.navigationController pushViewController:pre animated:NO];
}

-(void)goBack: (id)sender
{
    
//    CATransition *transition = [CATransition animation];
//    
//    transition.duration = 0.4f;
//    
//    transition.type = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

@end