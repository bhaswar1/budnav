//  ConScanRequestViewController.m
//  Contacter
//  Created by Bhaswar's MacBook Air on 26/06/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
#import "ConScanRequestViewController.h"
#import "ConNewRequestsViewController.h"
#import "ConProfileOwnViewController.h"
#import "SVProgressHUD.h"
#import "ConQROptionsViewController.h"
#import "iToast.h"
#import "AppDelegate.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

@interface ConScanRequestViewController ()
{
    UIAlertView *alert;
}
@end

@implementation ConScanRequestViewController
@synthesize request_type,userid;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    
    self.navigationController.navigationBarHidden=YES;
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    [SVProgressHUD show];
    self.view.backgroundColor = [UIColor clearColor];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *urlString1;
    
    if ([request_type isEqualToString:@"business"])
        
        urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=addrequest&id=%d&access_token=%@&device_id=%@&business=true",[userid intValue],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
    
    else
        
        urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=addrequest&id=%d&access_token=%@&device_id=%@",[userid intValue],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
    
    DebugLog(@"deny url: %@",urlString1);
    
    
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    
    if (signeddataURL1 == nil)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        //            [alert show];
        [SVProgressHUD dismiss];
        
//        CATransition *transition = [CATransition animation];
//        
//        transition.duration = 0.4f;
//        
//        transition.type = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        NSError *error=nil;
        NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                  options:kNilOptions
                                                                    error:&error];
        DebugLog(@"scan request json returns: %@",json_deny);
        
        //            [[[[iToast makeText:[NSString stringWithFormat:@" %@ ",[json_deny objectForKey:@"success"]]]
        //               setGravity:iToastGravityTop] setDuration:iToastDurationShort] show];
        
        if ([[json_deny objectForKey:@"success"]intValue] == 1)
        {
            
            
            NSDictionary *details_dict = [json_deny objectForKey:@"details"];
            
            if ([[details_dict objectForKey:@"switch"] intValue] ==0) {
                
                //                    [[[[iToast makeText:[NSString stringWithFormat:@"switch %d ",[[json_deny objectForKey:@"switch"] intValue]]]
                //                       setGravity:iToastGravityTop] setDuration:iToastDurationShort] show];
                
                [SVProgressHUD dismiss];
                
                ConNewRequestsViewController *con = [[ConNewRequestsViewController alloc]init];
                
                con.userid= [userid intValue];
                
//                CATransition* transition = [CATransition animation];
//                
//                transition.duration = 0.4;
//                transition.type = kCATransitionPush;
//                transition.subtype = kCATransitionFade;
//                
//                [[self navigationController].view.layer addAnimation:transition forKey:nil];
                
                [self.navigationController pushViewController:con animated:YES];
                
                alert = [[UIAlertView alloc] initWithTitle:@"Success!"
                                                   message:@"Request Successfully Sent"
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                [alert show];
                
            }
            else{
                
                //                    [[[[iToast makeText:[NSString stringWithFormat:@"switch %d ",[[json_deny objectForKey:@"switch"] intValue]]]
                //                       setGravity:iToastGravityTop] setDuration:iToastDurationShort] show];
                
                [SVProgressHUD dismiss];
                
                alert = [[UIAlertView alloc] initWithTitle:@"Success!"
                                                   message:@"Request Successfully Sent"
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                [alert show];
                
                [SVProgressHUD dismiss];
                
                
                ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
                
                con.other=@"yes";
                con.uid=userid;
                
//                CATransition* transition = [CATransition animation];
//                
//                transition.duration = 0.4;
//                transition.type = kCATransitionPush;
//                transition.subtype = kCATransitionFade;
//                
//                [[self navigationController].view.layer addAnimation:transition forKey:nil];
                
                [self.navigationController pushViewController:con animated:YES];
            }
        }
        
        else
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                               message:[json_deny objectForKey:@"error"]
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
            
            [SVProgressHUD dismiss];
            
            ConQROptionsViewController *con = [[ConQROptionsViewController alloc]init];
            
//            CATransition* transition = [CATransition animation];
//            
//            transition.duration = 0.4;
//            transition.type = kCATransitionPush;
//            transition.subtype = kCATransitionFade;
//            
//            [[self navigationController].view.layer addAnimation:transition forKey:nil];
            
            [self.navigationController pushViewController:con animated:YES];
        }
    }
    //    [del showTabValues:YES];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [self.view removeFromSuperview];
}
@end