//
//  GroupInfoViewController.m
//  Budnav
//
//  Created by ios on 15/09/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "GroupInfoViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

@interface GroupInfoViewController ()
{
    UIView *backview,*dividerview1,*dividerview2,*dividerview3,*dividerview4,*dividerview5,*grayview1,*grayview2;
    UIImageView *logo_img,*bck_img,*groupimg;
    UIButton *backbtn,*leavegrpbtn;
    UILabel *groupnamelbl,*membernolbl,*lbl1,*lbl2;
    UISwitch *onoff;
    
}
@end

@implementation GroupInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"Dictionary.....%@",self.mydic);
    backview=[[UIView alloc]initWithFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    [backview setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:backview];
    
    UIView *coverview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64)];
    [self.view addSubview:coverview];
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, coverview.frame.size.height);
    
    // Add the gradient to the view
    [coverview.layer insertSublayer:gradient atIndex:0];
    
    bck_img = [[UIImageView alloc]initWithFrame:CGRectMake(15,32,12,20)];//(13, 39, 10, 22)];
    bck_img.image=[UIImage imageNamed:@"back3"];
    [self.view addSubview:bck_img];
    
    logo_img = [[UIImageView alloc]init];
    logo_img.frame = CGRectMake(27+([UIScreen mainScreen].bounds.size.width-160)/2, 32, 101, 20);
    logo_img.backgroundColor = [UIColor clearColor];
    
    logo_img.image = [UIImage imageNamed:@"new_logo"];
    [coverview addSubview:logo_img];
    
    backbtn=[[UIButton alloc]initWithFrame:CGRectMake(0.0f,0.0f,74,64)];
    [backbtn setBackgroundColor:[UIColor clearColor]];
    [coverview addSubview:backbtn];
    [backbtn addTarget:self action:@selector(backtoprev:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIImageView  *img =[[UIImageView alloc] initWithFrame:CGRectMake(85.0f,coverview.frame.origin.y+coverview.frame.size.height+25,self.view.frame.size.width-170,self.view.frame.size.width-170)];
    // [img sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[groupsarray objectAtIndex:indexPath.row]objectForKey:@"thumb"]]] placeholderImage:[UIImage imageNamed:@"placeholder"] options:/* DISABLES CODE */ (0) == 0?SDWebImageRefreshCached : 0];
    //img.image=[UIImage imageNamed:@"Backup profile pic"];
    img.layer.cornerRadius = img.frame.size.width/2;
    img.image = [UIImage imageWithData:[NSData dataWithData:self.mydata2]];
    img.clipsToBounds=YES;
    [backview addSubview:img];
    
    
    groupnamelbl = [[UILabel alloc]initWithFrame:CGRectMake(50.0f,img.frame.origin.y+img.frame.size.height+23,self.view.frame.size.width-100,20)];
    groupnamelbl.text=[self.mydic objectForKey:@"name"];
    [groupnamelbl setTextAlignment:NSTextAlignmentCenter];
    groupnamelbl.textColor=[UIColor blackColor];
    [groupnamelbl setFont:[UIFont fontWithName:@"ProximaNova-Bold" size:17]];
    [backview addSubview:groupnamelbl];
    
    membernolbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0f,groupnamelbl.frame.origin.y+groupnamelbl.frame.size.height+13,self.view.frame.size.width-40,20)];
     [membernolbl setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:15]];
     membernolbl.text=[NSString stringWithFormat:@"%@ Members - Personal Group",[self.mydic objectForKey:@"count"]];
    [membernolbl setTextAlignment:NSTextAlignmentCenter];
    [membernolbl setTextColor:[UIColor colorWithRed:(171.0f/255.0f) green:(171.0f/255.0f) blue:(171.0f/255.0f) alpha:1.0f]];
   [backview addSubview:membernolbl];
    
    
    dividerview1=[[UIView alloc]initWithFrame:CGRectMake(0.0f,membernolbl.frame.origin.y+membernolbl.frame.size.height+23,self.view.frame.size.width,0.5f)];
    [dividerview1 setBackgroundColor:[UIColor colorWithRed:(153.0f/255.0f) green:(153.0f/255.0f) blue:(153.0f/255.0f) alpha:0.5f]];
    [backview addSubview:dividerview1];
    
    lbl1=[[UILabel alloc]initWithFrame:CGRectMake(20.0f,dividerview1.frame.origin.y+dividerview1.frame.size.height+15,230,20)];
    lbl1.text=@"Show group in contact list";
    [lbl1 setTextAlignment:NSTextAlignmentLeft];
    [lbl1 setTextColor:[UIColor blackColor]];
    [lbl1 setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:18]];
    [backview addSubview:lbl1];
    
    

    
    UISwitch *mySwitch = [[UISwitch alloc] initWithFrame:CGRectMake(self.view.frame.size.width-70,dividerview1.frame.origin.y+10, 0, 0)];
    mySwitch.onTintColor = [UIColor colorWithRed:(40.0/255.0) green:(169.0/255.0) blue:(184.0/255.0) alpha:1.0];
    //[mySwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:mySwitch];
    
    dividerview2=[[UIView alloc]initWithFrame:CGRectMake(0.0f,dividerview1.frame.origin.y+dividerview1.frame.size.height+50,self.view.frame.size.width,0.5f)];
    [dividerview2 setBackgroundColor:[UIColor colorWithRed:(153.0f/255.0f) green:(153.0f/255.0f) blue:(153.0f/255.0f) alpha:1.0f]];
    [backview addSubview:dividerview2];
    
    dividerview3=[[UIView alloc]initWithFrame:CGRectMake(0.0f,dividerview2.frame.origin.y+dividerview2.frame.size.height+50,self.view.frame.size.width,0.5f)];
    [dividerview3 setBackgroundColor:[UIColor colorWithRed:(153.0f/255.0f) green:(153.0f/255.0f) blue:(153.0f/255.0f) alpha:0.5f]];
    [backview addSubview:dividerview3];
    
    lbl2=[[UILabel alloc]initWithFrame:CGRectMake(20.0f,dividerview2.frame.origin.y+dividerview2.frame.size.height+15,230,20)];
    lbl2.text=@"Show group on map";
    [lbl2 setTextAlignment:NSTextAlignmentLeft];
    [lbl2 setTextColor:[UIColor blackColor]];
    [lbl2 setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:18]];
    [backview addSubview:lbl2];
    
    
    UISwitch *mySwitch2 = [[UISwitch alloc] initWithFrame:CGRectMake(self.view.frame.size.width-70,dividerview2.frame.origin.y+10, 0, 0)];
    mySwitch2.onTintColor = [UIColor colorWithRed:(40.0/255.0) green:(169.0/255.0) blue:(184.0/255.0) alpha:1.0];
    //[mySwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:mySwitch2];
    
    dividerview4=[[UIView alloc]initWithFrame:CGRectMake(0.0f,dividerview3.frame.origin.y+dividerview3.frame.size.height+40,self.view.frame.size.width,0.5f)];
    [dividerview4 setBackgroundColor:[UIColor colorWithRed:(153.0f/255.0f) green:(153.0f/255.0f) blue:(153.0f/255.0f) alpha:1.0f]];
    [backview addSubview:dividerview4];
    
    dividerview5=[[UIView alloc]initWithFrame:CGRectMake(0.0f,dividerview4.frame.origin.y+dividerview4.frame.size.height+50,self.view.frame.size.width,0.5f)];
    [dividerview5 setBackgroundColor:[UIColor colorWithRed:(153.0f/255.0f) green:(153.0f/255.0f) blue:(153.0f/255.0f) alpha:1.0f]];
    [backview addSubview:dividerview5];
    
    
    grayview1=[[UIView alloc]initWithFrame:CGRectMake(0.0,dividerview3.frame.origin.y+dividerview3.frame.size.height,self.view.frame.size.width,40)];
    [grayview1 setBackgroundColor:[UIColor colorWithRed:(237.0f/255.0f) green:(237.0f/255.0f) blue:(237.0f/255.0f) alpha:1.0f]];
    [backview addSubview:grayview1];
    
    
    leavegrpbtn=[[UIButton alloc]initWithFrame:CGRectMake(50.0f,dividerview4.frame.origin.y+dividerview4.frame.size.height,self.view.frame.size.width-100,50.0f)];
   // leavegrpbtn.backgroundColor=[UIColor grayColor];
    [leavegrpbtn setTitle:@"Leave group" forState:UIControlStateNormal];
    //leavegrpbtn.contentMode=UIViewContentModeBottom;
    leavegrpbtn.titleLabel.font=[UIFont fontWithName:@"ProximaNova-Regular" size:19.0];
    [leavegrpbtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [leavegrpbtn setUserInteractionEnabled:YES];
    [backview addSubview:leavegrpbtn];
    
    
    grayview2=[[UIView alloc]initWithFrame:CGRectMake(0.0,dividerview5.frame.origin.y+dividerview5.frame.size.height,self.view.frame.size.width,80)];
    [grayview2 setBackgroundColor:[UIColor colorWithRed:(237.0f/255.0f) green:(237.0f/255.0f) blue:(237.0f/255.0f) alpha:1.0f]];
    [backview addSubview:grayview2];
    
    
    
    
    
    // Do any additional setup after loading the view.
}

//- (void)changeSwitch:(id)sender{
//    
//    if([sender isOn]){
//        NSLog(@"Switch is ON");
//    } else{
//        NSLog(@"Switch is OFF");
//    }
//    
//}

-(void)backtoprev:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
