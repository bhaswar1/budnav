//
//  ConSignupViewController.m
//  Budnav
//
//  Created by intel on 23/03/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#define ORIGINAL_MAX_HEIGHT 450.0f
#import "ConSignupViewController.h"
#import "ConHomeViewController.h"
#import "ConSMSVerification.h"
#import "VPImageCropperViewController.h"
#import "SVProgressHUD.h"
#import <CoreLocation/CoreLocation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "ConAddressSignupViewController.h"

@interface ConSignupViewController ()<UITextFieldDelegate,UIImagePickerControllerDelegate,VPImageCropperDelegate,UINavigationControllerDelegate,CLLocationManagerDelegate,UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate,UIWebViewDelegate>
{
    UIView *topBar;
    UIScrollView *mainview;
    UIImageView *bck_imgv, *Signup_imgv, *profile_imgv;
    UILabel *backLabel, *signupLabel;
    UIButton *backButton, *signupButton;
    UITextField *firstName,*lastName,*countryCode,*contact,*email,*password;
    UITextView *terms;
    UITapGestureRecognizer *tap;
    NSString *pathss,*groups_url,*checkin;
    //    CGSize targetSize;
    UIImage *portraitImg1;
    NSMutableDictionary *groupdict1, *checkmobile_response;
    NSMutableData *responseData;
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    NSString *dialCode;
    UIAlertView *alert,*alert1;
    long ccCheck;
    UIActivityIndicatorView *activ;
    NSDictionary *jsonparsed;
    UIToolbar *numberToolbar;
    NSArray *countriesList;
    int check;
    UITableView *countrycodetable;
    UIImagePickerController *controller;
    NSArray *sortedArray;
    BOOL terms_press;
    UIWebView *webView;
    
}
@end

@implementation ConSignupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"from ConSignupViewController");
    
    self.screenName = @"Sign up profile";
    
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createEventWithCategory:@"Home"
                                             action:@"Sign up profile"
                                              label:nil
                                              value:nil] build]];
    // Do any additional setup after loading the view.
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate=self;
    locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    locationManager.distanceFilter=kCLDistanceFilterNone;
    
    [locationManager startUpdatingLocation];
    geocoder = [[CLGeocoder alloc] init];
    
    NSUInteger code = [CLLocationManager authorizationStatus];
    if (code == kCLAuthorizationStatusNotDetermined && ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)] || [locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]))
    {
        // choose one request according to your business.
        if([[NSBundle mainBundle]
            objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"]){
            DebugLog(@"NSLocationAlwaysUsageDescription");
            [locationManager requestAlwaysAuthorization];
            
        } else if([[NSBundle mainBundle]
                   objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
            DebugLog(@"NSLocationWhenInUseUsageDescription");
            [locationManager requestWhenInUseAuthorization];
            
        } else {
            DebugLog(@"Info.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription");
        }
    }
    
    NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"countries" ofType:@"json"]];
    NSError *localError = nil;
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
    
    countriesList = (NSArray *)parsedObject;
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    
    sortedArray = [countriesList sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];
    
    for (check=0; check < sortedArray.count; check++) {
        
        if ([[[sortedArray objectAtIndex:check]objectForKey:@"code"] isEqualToString: [[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkISOCode"]]) {
            
            dialCode = [NSString stringWithFormat:@"%@",[[sortedArray objectAtIndex:check]objectForKey:@"dial_code"]];
        }
        //        else
        //            dialCode = @"+93";
    }
    
    
    //}
    //
    //- (void)viewDidAppear:(BOOL)animated
    //{
    UIImage* sourceImage = [UIImage imageNamed:@"back3"];
    
    UIImage* flippedImage = [UIImage imageWithCGImage:sourceImage.CGImage
                                                scale:sourceImage.scale
                                          orientation:UIImageOrientationUpMirrored];
    
    topBar = [[UIView alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.origin.x , [UIScreen mainScreen].bounds.origin.y, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.origin.y+64)];
    topBar.layer.zPosition=3;
    //[topBar setBackgroundColor:[UIColor blackColor]];
    [self.view addSubview:topBar];
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
    
    // Add the gradient to the view
    [topBar.layer insertSublayer:gradient atIndex:0];
    
    
    
    bck_imgv = [[UIImageView alloc]initWithFrame:CGRectMake(15, 32, 12, 20)];//(9,33,10,20)];//(13, 39, 10, 22)];
    bck_imgv.image=[UIImage imageNamed:@"back3"];
    [topBar addSubview:bck_imgv];
    
    backLabel = [[UILabel alloc] initWithFrame:CGRectMake(bck_imgv.frame.origin.x+bck_imgv.frame.size.width+5, 31, 50, 20)];
    backLabel.text = @"Back";
    //    [backLabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:18]];
    backLabel.textColor = [UIColor whiteColor];
    [topBar addSubview:backLabel];
    
    
    Signup_imgv = [[UIImageView alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-27, 32, 12, 20)];//(9,33,10,20)];//(13, 39, 10, 22)];
    Signup_imgv.image = flippedImage;
    [topBar addSubview:Signup_imgv];
    
    signupLabel = [[UILabel alloc] initWithFrame:CGRectMake(Signup_imgv.frame.origin.x-66.8, 31, 61.8, 20)];
    signupLabel.text = @"Sign up";
    //    [signupLabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:18]];
    signupLabel.textColor = [UIColor whiteColor];
    [topBar addSubview:signupLabel];
    
    
    backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 0, 150, 64);
    backButton.backgroundColor = [UIColor clearColor];
    [backButton setBackgroundImage:nil forState:UIControlStateNormal];
    backButton.tag=1;
    [topBar addSubview:backButton];
    backButton.userInteractionEnabled = YES;
    
    
    signupButton = [UIButton buttonWithType:UIButtonTypeCustom];
    signupButton.frame = CGRectMake(signupLabel.frame.origin.x, signupLabel.frame.origin.y, Signup_imgv.frame.origin.x+Signup_imgv.frame.size.width-signupLabel.frame.origin.x, 30);
    signupButton.backgroundColor = [UIColor clearColor];
    [topBar addSubview:signupButton];
    signupButton.userInteractionEnabled = YES;
    
    //    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"back"])
    {
        mainview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, topBar.frame.origin.y+topBar.frame.size.height, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height-(topBar.frame.origin.y+topBar.frame.size.height))];
        mainview.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:mainview];
        
        profile_imgv = [[UIImageView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.origin.x, 0,115, 117)];
        
        if (portraitImg1) {
            profile_imgv.image = portraitImg1;
        }
        else
            profile_imgv.image = [UIImage imageNamed:@"Add profile picture"];
        
//        profile_imgv.layer.borderWidth = 1.0f;
//        profile_imgv.layer.borderColor = [[UIColor colorWithRed:204/255.0f green:204/255.0f blue:204/255.0f alpha:1] CGColor];
        [mainview addSubview:profile_imgv];
        
        tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(grouppic:)];
        [profile_imgv addGestureRecognizer:tap];
        profile_imgv.userInteractionEnabled=YES;
        
        UIView *clearView=[[UIView alloc]initWithFrame: CGRectMake(0, 0, 15, self.view.frame.size.height/12.8)] ;
        
        
        firstName = [[UITextField alloc] init];
        firstName.frame = CGRectMake([UIScreen mainScreen].bounds.origin.x+profile_imgv.frame.size.width, 3, self.view.frame.size.width-profile_imgv.frame.origin.x-100, 55);
        firstName.delegate = self;
        [firstName setTextColor:[UIColor blackColor]];
        [firstName setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:18]];
        [firstName setKeyboardAppearance:UIKeyboardAppearanceDark];
        [firstName setAutocorrectionType:UITextAutocorrectionTypeNo];
        [firstName setAutocapitalizationType:UITextAutocapitalizationTypeWords];
        [firstName setTintColor:[UIColor grayColor]];
        [firstName setLeftViewMode:UITextFieldViewModeAlways];
        firstName.leftView=clearView;
        
        firstName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"First Name" attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:204/255.0f green:204/255.0f blue:204/255.0f alpha:1]}];
        [mainview addSubview:firstName];
        
        
        UIView *borderView=[[UIView alloc]initWithFrame:CGRectMake(firstName.frame.origin.x, firstName.frame.origin.y+firstName.frame.size.height, firstName.frame.size.width, 0.5f)];
        [borderView setBackgroundColor:[UIColor colorWithRed:204/255.0f green:204/255.0f blue:204/255.0f alpha:0.8]];
        
        
        [mainview addSubview:borderView];
        
        
        
        lastName = [[UITextField alloc] init];
        lastName.frame = CGRectMake(self.view.frame.origin.x+profile_imgv.frame.size.width, firstName.frame.origin.y+firstName.frame.size.height+2.5, self.view.frame.size.width-profile_imgv.frame.origin.x-100, 55);
        lastName.delegate = self;
        [lastName setTextColor:[UIColor blackColor]];
        [lastName setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:18]];
        [lastName setKeyboardAppearance:UIKeyboardAppearanceDark];
        [lastName setAutocorrectionType:UITextAutocorrectionTypeNo];
        [lastName setAutocapitalizationType:UITextAutocapitalizationTypeWords];
        [lastName setTintColor:[UIColor grayColor]];
        [lastName setLeftViewMode:UITextFieldViewModeAlways];
        
        UIView *clearView2=[[UIView alloc]initWithFrame: CGRectMake(0, 0, 15, self.view.frame.size.height/12.8)] ;
        
        lastName.leftView=clearView2;
        
        lastName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Last Name" attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:204/255.0f green:204/255.0f blue:204/255.0f alpha:0.8]}];
        [mainview addSubview:lastName];
        
        
//        UIView *borderView2=[[UIView alloc]initWithFrame:CGRectMake(lastName.frame.origin.x, lastName.frame.origin.y+lastName.frame.size.height+0.5f, lastName.frame.size.width, 0.5f)];
        
         UIView *borderView2=[[UIView alloc]initWithFrame:CGRectMake(0, lastName.frame.origin.y+lastName.frame.size.height+0.5f, [UIScreen mainScreen].bounds.size.width, 0.5f)];
        [borderView2 setBackgroundColor:[UIColor colorWithRed:204/255.0f green:204/255.0f blue:204/255.0f alpha:0.8]];
        
        
        [mainview addSubview:borderView2];
        numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
        numberToolbar.barStyle = UIBarStyleBlackTranslucent;
        numberToolbar.items = [NSArray arrayWithObjects:
                               //                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                               [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                               [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneButton)],
                               nil];
        [numberToolbar sizeToFit];
        
        countryCode=[[UITextField alloc]initWithFrame:CGRectMake(0, profile_imgv.frame.origin.y+profile_imgv.frame.size.height+2, 114, 55)];
        [countryCode setTextColor:[UIColor blackColor]];
        countryCode.backgroundColor = [UIColor clearColor];
        countryCode.inputAccessoryView=numberToolbar;
        [countryCode setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:20.0f]];
        countryCode.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"CC" attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:204/255.0f green:204/255.0f blue:204/255.0f alpha:0.8]}];
        
        UIImageView *phone=[[UIImageView alloc]initWithFrame:CGRectMake(15, 6, 15, 15)];
        
        [phone setImage:[UIImage imageNamed:@"mobile_profile"]];
        
        
        UIView *leftView3=[[UIView alloc]initWithFrame: CGRectMake(0, 0, 43, 30)] ;
        [leftView3 addSubview:phone];
        countryCode.leftViewMode=UITextFieldViewModeAlways;
        countryCode.leftView = leftView3;
        countryCode.tag=91;
        
        if([[[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkISOCode"] isKindOfClass:[NSNull class]] || [[[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkISOCode"] length] == 0)
            countryCode.text=@"+93";
        else
            countryCode.text=[NSString stringWithFormat:@"%@",dialCode];
        
        countryCode.delegate=self;
        
        [mainview addSubview:countryCode];
        countryCode.userInteractionEnabled=YES;
        
        UIButton *downbutton = [UIButton buttonWithType:UIButtonTypeCustom];
        downbutton.frame = CGRectMake(0, 0, 110, 55);
        [downbutton setTitle:nil forState:UIControlStateNormal];
        downbutton.backgroundColor=[UIColor clearColor];
        downbutton.userInteractionEnabled=YES;
        [downbutton addTarget:self action:@selector(showtable:) forControlEvents:UIControlEventTouchUpInside];
        [countryCode addSubview:downbutton];
        
        
        UIView *borderView4=[[UIView alloc]initWithFrame:CGRectMake(countryCode.frame.origin.x+countryCode.frame.size.width, lastName.frame.origin.y+lastName.frame.size.height+0.5f,0.5f, countryCode.frame.size.height )];
        [borderView4 setBackgroundColor:[UIColor colorWithRed:204/255.0f green:204/255.0f blue:204/255.0f alpha:0.8]];
        
        
        [mainview addSubview:borderView4];
        
        
        contact = [[UITextField alloc] init];
        contact.frame = CGRectMake(countryCode.frame.origin.x+countryCode.frame.size.width+0.5, lastName.frame.origin.y+lastName.frame.size.height+3, self.view.frame.size.width-countryCode.frame.origin.x-100, 55);
        contact.delegate = self;
        [contact setTextColor:[UIColor blackColor]];
        contact.inputAccessoryView=numberToolbar;
        [contact setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:18]];
        [contact setKeyboardType:UIKeyboardTypePhonePad];
        [contact setKeyboardAppearance:UIKeyboardAppearanceDark];
        [contact setLeftViewMode:UITextFieldViewModeAlways];
        
        UIView *clearView3=[[UIView alloc]initWithFrame: CGRectMake(0, 0, 15, 50)] ;
        
        contact.leftView=clearView3;
        contact.tag=100;
        contact.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter Phone No." attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:204/255.0f green:204/255.0f blue:204/255.0f alpha:0.8]}];
        [mainview addSubview:contact];
        
        UIView *borderView3=[[UIView alloc]initWithFrame:CGRectMake(0, contact.frame.origin.y+contact.frame.size.height-0.5, self.view.frame.size.width, 0.5f)];
        [borderView3 setBackgroundColor:[UIColor colorWithRed:204/255.0f green:204/255.0f blue:204/255.0f alpha:0.8]];
        
        [mainview addSubview:borderView3];
        
        
        
        email = [[UITextField alloc] init];
        email.frame = CGRectMake(countryCode.frame.origin.x, contact.frame.origin.y+contact.frame.size.height+2, self.view.frame.size.width,  55);
        email.delegate = self;
        [email setTextColor:[UIColor blackColor]];
        email.backgroundColor = [UIColor clearColor];
        email.tag=3;
        [email setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:18]];
        [email setKeyboardAppearance:UIKeyboardAppearanceDark];
        [email setAutocorrectionType:UITextAutocorrectionTypeNo];
        [email setAutocapitalizationType:UITextAutocapitalizationTypeNone];
        email.keyboardType = UIKeyboardTypeEmailAddress;
        [email setTintColor:[UIColor grayColor]];
        
        [email setLeftViewMode:UITextFieldViewModeAlways];
        
        UIView *clearView4=[[UIView alloc]initWithFrame: CGRectMake(0, 0, 18,50)] ;
        
        email.leftView=clearView4;
        
        email.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:204/255.0f green:204/255.0f blue:204/255.0f alpha:0.8]}];
        
        UIImageView *emailImg=[[UIImageView alloc]initWithFrame:CGRectMake(15, 8, 15, 12)];
        
        [emailImg setImage:[UIImage imageNamed:@"email_profile"]];
        
        
        UIView *leftView4=[[UIView alloc]initWithFrame: CGRectMake(0, 0, 44, 30)] ;
        [leftView4 addSubview:emailImg];
        email.leftViewMode=UITextFieldViewModeAlways;
        email.leftView = leftView4;
        
        
        [mainview addSubview:email];
        
        UIView *borderView5=[[UIView alloc]initWithFrame:CGRectMake(email.frame.origin.x, email.frame.origin.y+email.frame.size.height+0.5f, email.frame.size.width, 0.5f)];
        [borderView5 setBackgroundColor:[UIColor colorWithRed:204/255.0f green:204/255.0f blue:204/255.0f alpha:0.8]];
        
        
        [mainview addSubview:borderView5];
        
        
        password = [[UITextField alloc] init];
        password.frame = CGRectMake(email.frame.origin.x, email.frame.origin.y+email.frame.size.height+2.5f, self.view.frame.size.width, 55);
        password.delegate = self;
        [password setTextColor:[UIColor blackColor]];
        [password setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:18]];
        [password setKeyboardAppearance:UIKeyboardAppearanceDark];
        [password setAutocorrectionType:UITextAutocorrectionTypeNo];
        [password setTintColor:[UIColor grayColor]];
        
        
        [password setLeftViewMode:UITextFieldViewModeAlways];
        
        UIView *clearView5=[[UIView alloc]initWithFrame: CGRectMake(0, 0, 15, 50)] ;
        
        password.leftView=clearView5;
        
        password.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:204/255.0f green:204/255.0f blue:204/255.0f alpha:0.8]}];
        
        UIImageView *pass=[[UIImageView alloc]initWithFrame:CGRectMake(16, 5, 12, 15)];
        
        [pass setImage:[UIImage imageNamed:@"password_profile"]];
        
        
        UIView *leftView5=[[UIView alloc]initWithFrame: CGRectMake(0, 0, 44, 30)] ;
        [leftView5 addSubview:pass];
        password.leftViewMode=UITextFieldViewModeAlways;
        password.leftView = leftView5;
        password.tag=4;
        [password setSecureTextEntry:YES];
        
        
        
        [mainview addSubview:password];
        
        UIView *borderView6=[[UIView alloc]initWithFrame:CGRectMake(password.frame.origin.x, password.frame.origin.y+password.frame.size.height, password.frame.size.width, 0.5f)];
        [borderView6 setBackgroundColor:[UIColor colorWithRed:204/255.0f green:204/255.0f blue:204/255.0f alpha:0.8]];
        
        
        [mainview addSubview:borderView6];
        
        
        terms=[[UITextView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/10.66, password.frame.origin.y+password.frame.size.height+30, self.view.frame.size.width/1.23, self.view.frame.size.height/4.3)];
        
        terms.textAlignment=NSTextAlignmentCenter;
        [terms setTextColor:[UIColor colorWithRed:135.0/255.0 green:135.0/255.0 blue:135.0/255.0 alpha:1.0]];
        [terms setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
        [terms setEditable:NO];
        [terms setBackgroundColor: [UIColor clearColor]];
        
        
        [terms setText:@"By clicking Sign up, you agree to our Terms and that you have read our Data Use Policy, including our Cookie Use."];
        
        NSRange range1 = [terms.text rangeOfString:@"Terms"];
        NSRange range2 = [terms.text rangeOfString:@"Data Use Policy"];
        NSRange range3 = [terms.text rangeOfString:@"Cookie Use"];
        
        
        
        NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
        
        paragraph.alignment = NSTextAlignmentCenter;
        
        
        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:terms.text];
        [attributedText setAttributes: @{NSFontAttributeName :[UIFont fontWithName:@"ProximaNova-Regular" size:17],
                                         NSForegroundColorAttributeName : [UIColor colorWithRed:135.0/255.0 green:135.0/255.0 blue:135.0/255.0 alpha:1.0],NSParagraphStyleAttributeName:paragraph} range:NSMakeRange(0,terms.text.length)];
        
        [attributedText addAttributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)} range:range1];
        [attributedText addAttributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)} range:range2];
        [attributedText addAttributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)} range:range3];
        
        terms.attributedText=attributedText;
        [mainview addSubview:terms];
        
        UIButton *terms_btn = [[UIButton alloc] init];
        terms_btn.frame = CGRectMake(self.view.frame.size.width/10.66, password.frame.origin.y+password.frame.size.height+30, self.view.frame.size.width/1.23, 100);
        [terms_btn setBackgroundColor:[UIColor clearColor]];
        [terms_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [terms_btn addTarget:self action:@selector(terms_condition) forControlEvents:UIControlEventTouchUpInside];
        [mainview addSubview:terms_btn];
        
        
    }
    countrycodetable=[[UITableView alloc]init];
    //    countrycodetable=[[UITableView alloc]initWithFrame:CGRectMake(5, 20, (self.view.bounds.size.width)-10,(self.view.bounds.size.height)-30)];
    countrycodetable.frame= CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, self.view.bounds.size.height-64);
    [mainview addSubview:countrycodetable];
    countrycodetable.backgroundColor=[UIColor whiteColor];
    countrycodetable.dataSource=self;
    countrycodetable.delegate=self;
    countrycodetable.separatorStyle=UITableViewCellSeparatorStyleNone;
    countrycodetable.hidden=YES;
    countrycodetable.showsVerticalScrollIndicator=NO;
    countrycodetable.layer.borderColor=[[UIColor grayColor]CGColor];
    countrycodetable.layer.borderWidth=1;
    countrycodetable.layer.zPosition=2;
    
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    signupLabel.alpha = 1.0f;
    Signup_imgv.alpha = 1.0f;
    
    [backButton addTarget:self action:@selector(changecolor1) forControlEvents:UIControlEventTouchDown];
    [backButton addTarget:self action:@selector(changecoloragain1) forControlEvents:UIControlEventTouchDragExit];
    [backButton addTarget:self action:@selector(changecolor1) forControlEvents:UIControlEventTouchDragEnter];
    
    
    [backButton addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    
    [signupButton addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDown];
    [signupButton addTarget:self action:@selector(changecoloragain) forControlEvents:UIControlEventTouchDragExit];
    [signupButton addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDragEnter];
    
    [signupButton addTarget:self action:@selector(signup) forControlEvents:UIControlEventTouchUpInside];
}


-(void)showtable:(id)sender
{
    DebugLog(@"yepee");
    if (countrycodetable.hidden==YES)
    {
        countrycodetable.hidden=NO;
        [Signup_imgv setHidden:YES];
        [signupLabel setHidden:YES];
        backButton.tag=2;
        //        downbutton.transform = CGAffineTransformMakeRotation(M_PI);
    }
    else
    {
        countrycodetable.hidden=YES;
    }
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [sortedArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [countrycodetable dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    // cell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"profile-bg.png"]];
    cell.backgroundColor=[UIColor clearColor];
    
    UILabel *name=[[UILabel alloc]initWithFrame:CGRectMake(10, 0, 200, 50)];
    name.backgroundColor=[UIColor clearColor];
    name.text=[NSString stringWithFormat:@"%@",[[sortedArray objectAtIndex:indexPath.row] objectForKey:@"name"]];
    name.font=[UIFont fontWithName:@"ProximaNova-Regular" size:20];
    name.textColor=[UIColor blackColor];
    name.textAlignment=NSTextAlignmentLeft;
    [cell addSubview:name];
    
    UILabel *code=[[UILabel alloc]initWithFrame:CGRectMake(250, 0, 60, 50)];
    code.backgroundColor=[UIColor clearColor];
    code.text=[NSString stringWithFormat:@"%@",[[sortedArray objectAtIndex:indexPath.row] objectForKey:@"dial_code"]];
    code.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
    code.textColor=[UIColor colorWithRed:135.0/255.0 green:135.0/255.0 blue:135.0/255.0 alpha:1.0];
    code.textAlignment=NSTextAlignmentRight;
    [cell addSubview:code];
    
    UILabel *separatorlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 49.5, 320, 0.5)];
    separatorlabel.backgroundColor=[UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f];
    [cell addSubview:separatorlabel];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    countryCode.text = [NSString stringWithFormat:@"%@",[[sortedArray objectAtIndex:indexPath.row]objectForKey:@"dial_code"]];
    //    isdlabel.text = [NSString stringWithFormat:@"%@",[[sortedArray objectAtIndex:indexPath.row]objectForKey:@"dial_code"]];
    countrycodetable.hidden=YES;
    [Signup_imgv setHidden:NO];
    [signupLabel setHidden:NO];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 50;
}


-(void)grouppic: (UIGestureRecognizer *)sender
{
    [firstName resignFirstResponder];
    [lastName resignFirstResponder];
    [email resignFirstResponder];
    [password resignFirstResponder];
    [contact resignFirstResponder];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose Your Action"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Open Camera", @"Choose From Gallery",nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [actionSheet showInView:self.view];
    
}


-(void)camera_func
{
    DebugLog(@"camera func");
    if ([self isCameraAvailable] && [self doesCameraSupportTakingPhotos]) {
        controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypeCamera;
        if ([self isFrontCameraAvailable]) {
            controller.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        }
        NSMutableArray *mediaTypes = [[NSMutableArray alloc] init];
        [mediaTypes addObject:(__bridge NSString *)kUTTypeImage];
        controller.mediaTypes = mediaTypes;
        controller.delegate = self;
        
        {
            [self presentViewController:controller
                               animated:YES
                             completion:^(void){
                                 DebugLog(@"Picker View Controller is presented");
                             }];
        }
    }
}



#pragma mark camera utility
- (BOOL) isCameraAvailable{
    return [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
}

- (BOOL) isRearCameraAvailable{
    return [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear];
}

- (BOOL) isFrontCameraAvailable {
    return [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront];
}

- (BOOL) doesCameraSupportTakingPhotos {
    return [self cameraSupportsMedia:(__bridge NSString *)kUTTypeImage sourceType:UIImagePickerControllerSourceTypeCamera];
}

- (BOOL) isPhotoLibraryAvailable{
    return [UIImagePickerController isSourceTypeAvailable:
            UIImagePickerControllerSourceTypePhotoLibrary];
}
- (BOOL) canUserPickVideosFromPhotoLibrary{
    return [self
            cameraSupportsMedia:(__bridge NSString *)kUTTypeMovie sourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}
- (BOOL) canUserPickPhotosFromPhotoLibrary{
    return [self
            cameraSupportsMedia:(__bridge NSString *)kUTTypeImage sourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}

- (BOOL) cameraSupportsMedia:(NSString *)paramMediaType sourceType:(UIImagePickerControllerSourceType)paramSourceType{
    __block BOOL result = NO;
    if ([paramMediaType length] == 0) {
        return NO;
    }
    NSArray *availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:paramSourceType];
    [availableMediaTypes enumerateObjectsUsingBlock: ^(id obj, NSUInteger idx, BOOL *stop) {
        NSString *mediaType = (NSString *)obj;
        if ([mediaType isEqualToString:paramMediaType]){
            result = YES;
            *stop= YES;
        }
    }];
    return result;
}

-(NSString*) encodeToPercentEscapeString:(NSString *)string {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                              (CFStringRef) string,
                                                              NULL,
                                                              (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
}




-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
            
        case 0:
        {
            DebugLog(@"here camera");
            [self performSelector:@selector(camera_func)];
        }
            break;
            
        case 1:
        {
            [self performSelector:@selector(gallery_func)];
        }
            break;
    }
}

-(void)gallery_func
{
    DebugLog(@"gallery func");
    if ([self isPhotoLibraryAvailable]) {
        controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        NSMutableArray *mediaTypes = [[NSMutableArray alloc] init];
        [mediaTypes addObject:(__bridge NSString *)kUTTypeImage];
        controller.mediaTypes = mediaTypes;
        controller.delegate = self;
        //        controller.allowsEditing=YES;
        [self presentViewController:controller
                           animated:YES
                         completion:^(void){
                             DebugLog(@"Picker View Controller is presented");
                         }];
    }
    
}

- (void)imageCropper:(VPImageCropperViewController *)cropperViewController didFinished:(UIImage *)editedImage {
    
    DebugLog(@"edited img: %f %f",editedImage.size.width, editedImage.size.height);
    
    portraitImg1 = editedImage;
    
    
    
    profile_imgv.image=portraitImg1;
    
    //    zipimage.image = editedImage;
    //    zipimage.contentMode=UIViewContentModeScaleAspectFill;
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
        // TO DO
    }];
    
}

- (void)imageCropperDidCancel:(VPImageCropperViewController *)cropperViewController {
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
    }];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^() {
        UIImage *portraitImg = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        portraitImg = [self imageByScalingToMaxSize:portraitImg];
        // present the cropper view controller
        VPImageCropperViewController *imgCropperVC = [[VPImageCropperViewController alloc] initWithImage:portraitImg cropFrame:CGRectMake(35, (self.view.frame.size.height- (self.view.frame.size.width-70))/2, self.view.frame.size.width-70, self.view.frame.size.width-70) limitScaleRatio:3.0];
        imgCropperVC.delegate = self;
        //        [mainview removeFromSuperview];
        [self presentViewController:imgCropperVC animated:YES completion:^{
            // TO DO
        }];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:^(){
    }];
}

#pragma mark image scale utility

- (UIImage *)imageByScalingToMaxSize:(UIImage *)sourceImage {
    if (sourceImage.size.height < ORIGINAL_MAX_HEIGHT) return sourceImage;
    CGFloat btWidth = 0.0f;
    CGFloat btHeight = 0.0f;
    if (sourceImage.size.height > sourceImage.size.width) {
        btHeight = ORIGINAL_MAX_HEIGHT;
        btWidth = sourceImage.size.width * (ORIGINAL_MAX_HEIGHT / sourceImage.size.height);
    } else {
        btHeight = ORIGINAL_MAX_HEIGHT;
        btWidth = sourceImage.size.width * (ORIGINAL_MAX_HEIGHT / sourceImage.size.height);
    }
    CGSize targetSize = CGSizeMake(btWidth, btHeight);
    return [self imageByScalingAndCroppingForSourceImage:sourceImage targetSize:targetSize];
}


- (UIImage *)imageByScalingAndCroppingForSourceImage:(UIImage *)sourceImage targetSize:(CGSize)targetSize {
    DebugLog(@"called scaling&croppingforsize %f %f",targetSize.width,targetSize.height);
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    
    
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    if (CGSizeEqualToSize(imageSize, targetSize) == NO)
    {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor > heightFactor)
            scaleFactor = widthFactor; // scale to fit height
        else
            scaleFactor = heightFactor; // scale to fit width
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else
            if (widthFactor < heightFactor)
            {
                thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
            }
    }
    UIGraphicsBeginImageContext(targetSize); // this will crop
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil) DebugLog(@"could not scale image");
    
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - UINavigationControllerDelegate
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}


- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
}


- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    DebugLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        DebugLog(@"currentLocation.coordinate.longitude=========>%f",currentLocation.coordinate.longitude);
        DebugLog(@"currentLocation.coordinate.longitude=========>%f",currentLocation.coordinate.latitude);
    }
    //    [locationManager stopUpdatingLocation];
    
    DebugLog(@"Resolving the Address");
    
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemrk, NSError *error) {
        DebugLog(@"Found placemarks: %@, error: %@", placemrk, error);
        if (error == nil && [placemrk count] > 0) {
            placemark = [placemrk lastObject];
            
            DebugLog(@"ADDRESS===================>%@ %@\n%@ %@\n%@\n%@\n%@",placemark.subThoroughfare, placemark.thoroughfare,
                     placemark.postalCode, placemark.locality,
                     placemark.administrativeArea,
                     placemark.country,placemark.ISOcountryCode);
            
            [[NSUserDefaults standardUserDefaults] setObject:placemark.ISOcountryCode forKey:@"placemarkISOCode"];
            [[NSUserDefaults standardUserDefaults] setObject:placemark.country forKey:@"placemarkCode"];
            
            [locationManager stopUpdatingLocation];
        } else {
            DebugLog(@"%@", error.debugDescription);
        }
    }];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    DebugLog(@"didFailWithError: %@", error);
    //    UIAlertView *errorAlert = [[UIAlertView alloc]
    //                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //    [errorAlert show];
    
}




#pragma mark- method to change to base64 string
- (NSString*)base64forData:(NSData*)theData {
    
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}


-(void)doneButton{
    
    // NSString *numberFromTheKeyboard = ziptext.text;
    [contact resignFirstResponder];
    [countryCode resignFirstResponder];
}


- (void)goBack:(UIButton *)button
{
    
    if (terms_press) {
        backLabel.alpha = 1.0f;
        bck_imgv.alpha = 1.0;
        [webView removeFromSuperview];
    }
    else
    {
        if (button.tag==1){
            
            //            CATransition *transition = [CATransition animation];
            //
            //            transition.duration = 0.4f;
            //
            //            transition.type = kCATransitionFade;
            //
            //            [[self navigationController].view.layer addAnimation:transition forKey:nil];
            [self.navigationController popViewControllerAnimated:YES];
        }
        
        if (button.tag==2)
        {
            
            countrycodetable.hidden=YES;
            backButton.tag=1;
            [Signup_imgv setHidden:NO];
            [signupLabel setHidden:NO];
            backLabel.alpha = 1.0f;
            bck_imgv.alpha = 1.0f;
            
        }
    }
    
    //    ConHomeViewController *homePage = [[ConHomeViewController alloc] init];
    //    [self.navigationController popToViewController:homePage animated:NO];
}


- (void)signup
{
    DebugLog(@"country code %@",countryCode.text);
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    
    if ([[firstName.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your First Name"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        signupLabel.alpha = 1.0f;
        Signup_imgv.alpha = 1.0f;
        
    }
    else if ([[lastName.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your Last Name"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        signupLabel.alpha = 1.0f;
        Signup_imgv.alpha = 1.0f;
        
        
    }
    
    else if ([[countryCode.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your Country Code"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        signupLabel.alpha = 1.0f;
        Signup_imgv.alpha = 1.0f;
        
    }
    
    else if ([[countryCode.text stringByTrimmingCharactersInSet:whitespace] length] <2 )
    {
        
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter a Valid Country Code"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        signupLabel.alpha = 1.0f;
        Signup_imgv.alpha = 1.0f;
        
    }
    
    else if ([[contact.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your Mobile Number"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        signupLabel.alpha = 1.0f;
        Signup_imgv.alpha = 1.0f;
        
    }
    
    else if ([[contact.text stringByTrimmingCharactersInSet:whitespace] length] <8 || [[contact.text stringByTrimmingCharactersInSet:whitespace] length] > 12 || !([contact.text rangeOfCharacterFromSet:notDigits].location == NSNotFound))
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter a Valid Mobile Number"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        signupLabel.alpha = 1.0f;
        Signup_imgv.alpha = 1.0f;
        
    }
    
    else if ([[email.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your Email"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        signupLabel.alpha = 1.0f;
        Signup_imgv.alpha = 1.0f;
        
    }else if (![self validateEmailWithString:email.text]){
        
        alert = [[UIAlertView alloc] initWithTitle:@"Email is not valid"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        signupLabel.alpha = 1.0f;
        Signup_imgv.alpha = 1.0f;
        
        
    }
    else if ([[password.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your Password"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        signupLabel.alpha = 1.0f;
        Signup_imgv.alpha = 1.0f;
        
    }
    
    else if ([[password.text stringByTrimmingCharactersInSet:whitespace] length] > 0 && [[password.text stringByTrimmingCharactersInSet:whitespace] length] < 6)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Password should have minimum 6 characters"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        signupLabel.alpha = 1.0f;
        Signup_imgv.alpha = 1.0f;
        
    }
    
//    else if (portraitImg1==NULL)
//    {
//        NSLog(@"working");
//        alert = [[UIAlertView alloc] initWithTitle:@"Please upload a profile picture"
//                                           message:Nil
//                                          delegate:self
//                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
//        [alert show];
//        signupLabel.alpha = 1.0f;
//        Signup_imgv.alpha = 1.0f;
//    }
    
    else
    {
        activ=[[UIActivityIndicatorView alloc]init];
        activ.center=self.view.center;
        [activ startAnimating];
        activ.hidesWhenStopped=YES;
        activ.hidden=NO;
        activ.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        [activ setColor:[UIColor blackColor]];
        [self.view addSubview:activ];
        
        
        NSOperationQueue *queue = [NSOperationQueue new];
        [queue addOperationWithBlock:^{
            @try {
                NSString *loginstring = [NSString stringWithFormat:@"%@/?action=checkemail&email=%@",mydomainurl,email.text];
                DebugLog(@"%@",loginstring);
                
                NSError *localErr;
                NSData *dataURL =  [NSData dataWithContentsOfURL:[NSURL URLWithString:loginstring] options:NSDataReadingUncached error:&localErr];
                if (dataURL != nil)
                {
                    if(localErr!=nil)
                    {
                        DebugLog(@" nil");
                    }
                    jsonparsed = [NSJSONSerialization JSONObjectWithData:dataURL //1
                                                                 options:kNilOptions
                                                                   error:&localErr];
                    
                    NSString *errornumber= [NSString stringWithFormat:@"%@",[jsonparsed objectForKey:@"errorno"]];
                    DebugLog(@"error number  %@",errornumber);
                    
                    if (![errornumber isEqualToString:@"0"])
                    {
                        [self performSelectorOnMainThread:@selector(failure) withObject:nil waitUntilDone:YES];
                    }
                    else
                    {
                        //                        [self performSelectorOnMainThread:@selector(yesSuccess)
                        //                                               withObject:nil
                        //                                            waitUntilDone:YES];
                        
                        [self performSelectorOnMainThread:@selector(checkNumberExist)
                                               withObject:nil
                                            waitUntilDone:YES];
                    }
                }
                else
                {
                    signupLabel.alpha = 1.0f;
                    Signup_imgv.alpha = 1.0f;
                    DebugLog(@"server not responding");
                    UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@"Error!"
                                                                     message:@"Server not responding"
                                                                    delegate:self
                                                           cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                    
                    [alert2 show];
                    [activ stopAnimating];
                    [activ removeFromSuperview];
                }
            }
            @catch (NSException *exception) {
                
                DebugLog(@"exception occurred");
                UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@"Error!"
                                                                 message:@"Exception occurred"
                                                                delegate:self
                                                       cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                
                [alert2 show];
                [activ stopAnimating];
                [activ removeFromSuperview];
            }
            
        }];
        
    }
    
}

-(void)checkNumberExist
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        //        [self startLoading:self.view];
        
    }];
    
    [[NSOperationQueue new] addOperationWithBlock:^{
        
        NSMutableDictionary *result=[[NSMutableDictionary alloc]init];
        checkmobile_response = [[NSMutableDictionary alloc] init];
        NSString *urlString = [NSString stringWithFormat:@"https://budnav.com/ext/?action=checkMobile&mobile_pre=%@&mobile_num=%@&device_id=%@",countryCode.text,contact.text,[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"]];
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"CheckMobile URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            // [self stopLoading];
            
            if(urlData==nil)
            {
                
                alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"error in internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
            }
            else
            {
                NSMutableDictionary *result=[[NSMutableDictionary alloc]init];
                result = [NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                DebugLog(@"Notification result is:%@",result);
                
                
                
                NSString *errornumber= [NSString stringWithFormat:@"%@",[result objectForKey:@"errorno"]];
                DebugLog(@"error number  %@",errornumber);
                
                if ([[result objectForKey:@"success"] intValue] == 1)
                {
                    checkmobile_response = [result objectForKey:@"details"];
                    
                    if ([[[result objectForKey:@"details"] objectForKey:@"exists"] intValue]==1)
                    {
                        if ([[result objectForKey:@"details"] objectForKey:@"confirmed"])
                        {
                            [SVProgressHUD showErrorWithStatus:@"Please Use Another Phone Number!"];
                            [activ stopAnimating];
                            [activ removeFromSuperview];
                            [firstName setEnabled:YES];
                            [lastName setEnabled:YES];
                            [countryCode setEnabled:YES];
                            [contact setEnabled:YES];
                            [email setEnabled:YES];
                            [password setEnabled:YES];
                            Signup_imgv.alpha = 1;
                            signupLabel.alpha = 1;
                        }
                        else
                        {
                            [self performSelectorOnMainThread:@selector(yesSuccess)
                                                   withObject:nil
                                                waitUntilDone:YES];
                        }
                    }
                    else
                    {
                        //                    ConAddressSignupViewController *add = [[ConAddressSignupViewController alloc] init];
                        //
                        //                    add.Code = @"";
                        //                    [self.navigationController pushViewController:add animated:YES];
                        [self performSelectorOnMainThread:@selector(yesSuccess)
                                               withObject:nil
                                            waitUntilDone:YES];
                    }
                }
                else
                {
                    alert = [[UIAlertView alloc] initWithTitle:[result objectForKey:@"error"]
                                                       message:Nil
                                                      delegate:self
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                    [alert show];
                    [activ removeFromSuperview];
                }
            }
        }];
        
    }];
    
}

-(void)yesSuccess{
    
    NSDictionary *dict_details = [jsonparsed objectForKey:@"details"];
    
    DebugLog(@" dict detaisls %@",dict_details);
    checkin=[dict_details objectForKey:@"exists"];
    DebugLog(@"checkin : %@",checkin);
    
    if([checkin intValue]== 1)
    {
        [SVProgressHUD showErrorWithStatus:@"This Email already exists!"];
        [activ stopAnimating];
        [activ removeFromSuperview];
        [firstName setEnabled:YES];
        [lastName setEnabled:YES];
        [countryCode setEnabled:YES];
        [contact setEnabled:YES];
        [email setEnabled:YES];
        [password setEnabled:YES];
        Signup_imgv.alpha = 1;
        signupLabel.alpha = 1;
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setObject:firstName.text forKey:@"firstname"];
        [[NSUserDefaults standardUserDefaults] setObject:lastName.text forKey:@"lastname"];
        [[NSUserDefaults standardUserDefaults] setObject:password.text forKey:@"password"];
        [[NSUserDefaults standardUserDefaults] setObject:email.text forKey:@"email"];
        [[NSUserDefaults standardUserDefaults] setObject:contact.text forKey:@"phone"];
        [[NSUserDefaults standardUserDefaults] setObject:countryCode.text forKey:@"countrycode"];
        
        NSString *base64String;
       
        if ([profile_imgv.image isEqual:[UIImage imageNamed:@"Add profile picture"]])
        {
            NSLog(@"Profile Image:%@",profile_imgv.image);
            base64String = @"";
        }
        else
        {
            NSLog(@"Profile Image:%@",profile_imgv.image);
        
            NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(profile_imgv.image)];
            base64String = [imageData base64EncodedStringWithOptions:0];
            //    DebugLog(@"%@", base64String); // Zm9
            DebugLog(@"Encoded text: %@", base64String);
        }
        DebugLog(@"Encoded text: %@", base64String);
        
        [[NSUserDefaults standardUserDefaults]setObject:base64String forKey:@"r_prof_image"];
        
        //////////////////////////////// SMS VERIFICATION URL ///////////////////////////////////////////
        //action=smsverification&mobile_pre=+31&mobile_num=42529524
        
        NSString *pre_num = countryCode.text;
        NSString *number = contact.text;
        NSUserDefaults *prefsa = [NSUserDefaults standardUserDefaults];
        NSError *error;
        NSDictionary *json;
        NSString *urlString =[NSString stringWithFormat:@"https://budnav.com/ext/?action=registersendsms&mobile_pre=%@&mobile_num=%@",pre_num,number];
        DebugLog(@"SMS VERIFICATION URL:%@",urlString);
        
        NSString *newString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSData *signeddataURL =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString]];
        if (signeddataURL == nil)
        {
            signupLabel.alpha = 1.0f;
            Signup_imgv.alpha = 1.0f;
            signupButton.enabled = YES;
            alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                               message:Nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
            [activ removeFromSuperview];
        }
        else
        {
            json = [NSJSONSerialization JSONObjectWithData:signeddataURL
                                                   options:kNilOptions
                                                     error:&error];
            DebugLog(@"SMS JSON:%@",json);
            NSString *error_msg = [json objectForKey:@"error"];
            
            if ([[json objectForKey:@"success"] intValue] == 1)
            {
                if ([[checkmobile_response objectForKey:@"exists"] intValue]==1)
                {
                    ConSMSVerification *sign_up = [[ConSMSVerification alloc] init];
                    [self.navigationController pushViewController:sign_up animated:NO];
                }
                else
                {
                    ConAddressSignupViewController *add = [[ConAddressSignupViewController alloc] init];
                    add.Code = @"";
                    [self.navigationController pushViewController:add animated:YES];
                }
                
                //                ConSMSVerification *sign_up = [[ConSMSVerification alloc] init];
                //
                //                if ([[json objectForKey:@"details"] count]>0)
                //                {
                //
                //                if ([[[json objectForKey:@"details"] allKeys] containsObject:@"code"] && ![[[json objectForKey:@"details"] objectForKey:@"code"] isKindOfClass:[NSNull class]] && [[json objectForKey:@"details"] objectForKey:@"code"]!=nil && ![[[json objectForKey:@"details"] objectForKey:@"code"] isEqualToString:@"(null)"] && ![[[json objectForKey:@"details"] objectForKey:@"code"] isEqualToString:@""])
                //
                //                {
                //
                //                    sign_up.defaultCode = [[json objectForKey:@"details"] objectForKey:@"code"];
                //
                //                }
                //            }
                
                //                    CATransition* transition = [CATransition animation];
                //
                //                    transition.duration = 0.4;
                //                    transition.type = kCATransitionPush;
                //                    transition.subtype = kCATransitionFade;
                //                    transition.subtype = kCATransitionFromRight;
                //
                //                    [[self navigationController].view.layer addAnimation:transition forKey:nil];
                
                //      [self.navigationController pushViewController:sign_up animated:NO];
                // }
            }
            else
            {
                signupLabel.alpha = 1.0f;
                Signup_imgv.alpha = 1.0f;
                signupButton.enabled = YES;
                alert = [[UIAlertView alloc] initWithTitle:error_msg
                                                   message:Nil
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                [alert show];
                [activ removeFromSuperview];
                
            }
        }
        
        
    }
}


-(void)failure{
    
    NSString *err_str = [jsonparsed objectForKey:@"error"];
    alert1 = [[UIAlertView alloc] initWithTitle:@"Error!"
                                        message:err_str
                                       delegate:self
                              cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
    
    [alert1 show];
    [activ stopAnimating];
    [activ removeFromSuperview];
    Signup_imgv.alpha = 1;
    signupLabel.alpha = 1;
    
}


-(void)viewDidDisappear:(BOOL)animated
{
    [firstName setEnabled:YES];
    [lastName setEnabled:YES];
    [countryCode setEnabled:YES];
    [contact setEnabled:YES];
    [email setEnabled:YES];
    [password setEnabled:YES];
    
    [activ removeFromSuperview];
    
    //    [super viewDidDisappear:YES];
}



- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    if(textField.tag==91)
    {
        
        textField.text=@"+";
        
    }
    if (textField.tag==4)
    {
        if (self.view.bounds.size.height<=480) {
            mainview.contentOffset=CGPointMake(0, 100);
            //            mainview.frame= CGRectMake(0, -50, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
        }
    }
    if (textField.tag==3)
    {
        if (self.view.bounds.size.height<=480) {
            mainview.contentOffset=CGPointMake(0, 100);
            //            mainview.frame= CGRectMake(0, -50, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
        }
    }
    
    
    return YES;
}


- (BOOL)textField:(UITextView *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField.tag==91)
    {
        
        if (range.location < 1 || range.location >2) {
            return NO;
        }
        
    }
    else if(textField.tag==100)
    {
        if(range.location >12)
        {
            return NO;
        }
        
    }
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    
    if(textField.tag==91)
    {
        if(textField.text.length==1)
        {
            textField.text=@"";
        }
    }
    return YES;
}




- (BOOL)validateEmailWithString:(NSString*)checkString
{
    DebugLog(@"hjkiloppppp");
    
    BOOL stricterFilter = YES;
    
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:checkString];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    mainview.contentOffset=CGPointMake(0, 0);
    return YES;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)changecoloragain1
{
    DebugLog(@"CHANGE COLOR");
    //    change.backgroundColor=[UIColor clearColor];
    backLabel.alpha = 1.0f;
    bck_imgv.alpha = 1.0f;
}

-(void)changecolor1
{
    DebugLog(@"CHANGE COLOR1");
    //    change.backgroundColor=[UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0];
    backLabel.alpha = 0.5f;
    bck_imgv.alpha = 0.5f;
    
}

-(void)changecoloragain
{
    DebugLog(@"CHANGE COLOR");
    //    change.backgroundColor=[UIColor clearColor];
    signupLabel.alpha = 1.0f;
    Signup_imgv.alpha = 1.0f;
}

-(void)changecolor
{
    DebugLog(@"CHANGE COLOR1");
    //    change.backgroundColor=[UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0];
    signupLabel.alpha = 0.5f;
    Signup_imgv.alpha = 0.5f;
    
}

-(void)terms_condition
{
    backButton.userInteractionEnabled =  YES;
    terms_press = TRUE;
    DebugLog(@"Terms & Condition");
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://budnav.com/dev/?p=legal"]];
    
    webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 64, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    [webView setDelegate:self];
    
    NSString *urlAddress = @"https://budnav.com/?p=legal";
    NSURL *url = [NSURL URLWithString:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [webView loadRequest:requestObj];
    
    [self.view addSubview:webView];
    
    
}

@end
