#import <AddressBookUI/AddressBookUI.h>
#import "AppDelegate.h"
#import "ConNewContactsViewController.h"
#import "ConAddBackUpViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

@interface ConAddBackUpViewController()<NSURLConnectionDelegate,NSURLConnectionDataDelegate>
{
}
@end

@implementation ConAddBackUpViewController

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize navigationController,tabBarController,dbstatus,arr_contacts_ad,writetodbcount,recom_search_array,recom_search_string, pushtoprofileid,con_array, uniquearray, part_array;

-(void)viewDidLoad
{
}

-(void)reloadNewContactsFromWeb
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    NSError *error = nil;
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=contacts&amount=-1&id=%@&access_token=%@&device_id=%@&thumb=true",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
    
    DebugLog(@"eshe gechhe addbackup from loadrequests from web contacts profile url: %@",urlString1);
    
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    
    if (signeddataURL1 == nil)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        //            [alert show];
    }
    else
    {
        json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                options:kNilOptions
                                                  error:&error];
        
        NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
        
        if (![errornumber isEqualToString:@"0"])
        {
            NSString *err_str = [json1 objectForKey:@"error"];
            alert = [[UIAlertView alloc] initWithTitle:@"Error in Retrieving Data!"
                                               message:err_str
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            //                [alert show];
        }
        else
        {
            app_contacts = [[NSMutableArray alloc]init];
            
            if ([[json1 objectForKey:@"details"] isKindOfClass:[NSNull class]] || [json1 objectForKey:@"details"] == (id)[NSNull null] ||[[json1 objectForKey:@"details"] count] ==0)
            {
            }
            else
            {
                NSDictionary *contacts_temp_dict = [json1 objectForKey:@"details"];
                
                for (NSDictionary *dict in [contacts_temp_dict objectForKey:@"contacts"])
                {
                    [app_contacts addObject:dict];
                }
                loadcheck=1;
            }
            
            
            CFErrorRef *errorab = nil;
            
            ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, errorab);
            
            __block BOOL accessGranted = NO;
            if (ABAddressBookRequestAccessWithCompletion != NULL) { // we're on iOS 6
                dispatch_semaphore_t sema = dispatch_semaphore_create(0);
                ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
                    accessGranted = granted;
                    dispatch_semaphore_signal(sema);
                });
                dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
                //        dispatch_release(sema);
            }
            else { // we're on iOS 5 or older
                accessGranted = YES;
            }
            if (accessGranted) {
                new_con_array = [[NSMutableArray alloc]init];
                //                NSMutableArray *contactpickarr = [[NSMutableArray alloc]init];
                
                ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
                //contains details for all the contacts
                //contains details for all the contacts
                CFArrayRef ContactInfoArray = ABAddressBookCopyArrayOfAllPeople(addressBookRef);
                
                //get the total number of count of the users contact
                CFIndex numberofPeople = CFArrayGetCount(ContactInfoArray);
                DebugLog(@"%ld",numberofPeople);
                con_array = [[NSMutableArray alloc]init];
                
                //iterate through each record and add the value in the array
                for (int ik =0; ik<numberofPeople; ik++) {
                    ABRecordRef ref = CFArrayGetValueAtIndex(ContactInfoArray, ik);
                    ABMultiValueRef names = (__bridge ABMultiValueRef)((__bridge NSString*)ABRecordCopyValue(ref, kABPersonFirstNameProperty));
                    ABMultiValueRef lastnames = (__bridge ABMultiValueRef)((__bridge NSString*)ABRecordCopyValue(ref, kABPersonLastNameProperty));
                    
                    //        DebugLog(@"name from address book = %@ %@",names,lastnames);  // works fine.
                    NSString *fullname;
                    NSString *fnm = [NSString stringWithFormat:@"%@",names];
                    NSString *lnm = [NSString stringWithFormat:@"%@",lastnames];
                    //        DebugLog(@"fnm : %@",fnm);
                    if ([fnm length] ==0 || [fnm isKindOfClass:[NSNull class]] || fnm == (id)[NSNull null] || [fnm isEqualToString:@"(null)"])
                        fullname = [NSString stringWithFormat:@"%@",lastnames];
                    
                    else if ([lnm length] ==0 || [lnm isKindOfClass:[NSNull class]] || lnm == (id)[NSNull null] || [lnm isEqualToString:@"(null)"])
                        fullname = [NSString stringWithFormat:@"%@",names];
                    else
                        fullname = [NSString stringWithFormat:@"%@ %@",names,lastnames];
                    
                    //        NSString *contactName = (__bridge NSString *)(names);
                    
                    ABRecordID recordID = ABRecordGetRecordID(ref);
                    int useridphn = recordID;
                    NSString *idphn = [NSString stringWithFormat:@"%d",useridphn];
                    
                    [ns setObject:idphn forKey:@"userid"];
                    
                    
                    ABMultiValueRef emailProperty = ABRecordCopyValue(ref, kABPersonEmailProperty);
                    
                    NSArray *emailArray = (__bridge NSArray *)ABMultiValueCopyArrayOfAllValues(emailProperty);
                    NSString *newstringemail= [emailArray componentsJoinedByString:@","];
                    
                    ns = [[NSMutableDictionary alloc]init];
                    
                    [ns setObject:fullname forKey:@"name"];
                    //            [ns setObject:fullname forKey:@"fullname"];
                    [ns setValue:newstringemail forKey:@"email"];
                    
                    if ([newstringemail isKindOfClass:[NSNull class]] || newstringemail == (id)[NSNull null] || [newstringemail length] < 4)
                        [ns setValue:@"" forKey:@"email"];
                    
                    
                    ABMultiValueRef phoneNumberProperty = ABRecordCopyValue(ref, kABPersonPhoneProperty);
                    NSArray *_phoneNumbers = (__bridge NSArray*)ABMultiValueCopyArrayOfAllValues(phoneNumberProperty);
                    
                    NSMutableArray *mutableph= [_phoneNumbers mutableCopy];
                    int pp;
                    //                            DebugLog(@"mutable first: %@",mutableph);
                    for (pp =0; pp< [mutableph count]; pp++)
                    {
                        if ([[mutableph objectAtIndex:pp] isKindOfClass:[NSNull class]] || [mutableph objectAtIndex:pp] == (id)[NSNull null] || [[mutableph objectAtIndex:pp] length] < 3)
                        {
                            [mutableph removeObjectAtIndex:pp];
                            pp= pp-1;
                        }
                    }
                    //                            DebugLog(@"mutable second: %@",mutableph);
                    NSString *newstringph= [mutableph componentsJoinedByString:@","];
                    
                    [ns setValue:newstringph forKey:@"phone"];
                    
                    
                    ABMultiValueRef stt = ABRecordCopyValue(ref, kABPersonAddressProperty);
                    if (ABMultiValueGetCount(stt) > 0) {
                        CFDictionaryRef dict = ABMultiValueCopyValueAtIndex(stt, 0);
                        //                    DebugLog(@"dict address contacts: %@",dict);
                        NSString *streetp = CFDictionaryGetValue(dict, kABPersonAddressStreetKey);
                        NSString *cityp = CFDictionaryGetValue(dict, kABPersonAddressCityKey);
                        NSString *zipcdep = CFDictionaryGetValue(dict, kABPersonAddressZIPKey);
                        //                    NSString *statep = CFDictionaryGetValue(dict, kABPersonAddressStateKey);
                        NSString *countryp = CFDictionaryGetValue(dict, kABPersonAddressCountryKey);
                        
                        if ([streetp isKindOfClass:[NSNull class]] || streetp == (id)[NSNull null])
                            [ns setValue:@"" forKey:@"street"];
                        else
                            [ns setValue:streetp forKey:@"street"];
                        
                        if ([cityp isKindOfClass:[NSNull class]] || cityp == (id)[NSNull null])
                            [ns setValue:@"" forKey:@"city"];
                        else
                            [ns setValue:cityp forKey:@"city"];
                        
                        if ([zipcdep isKindOfClass:[NSNull class]] || zipcdep == (id)[NSNull null])
                            [ns setValue:@"" forKey:@"zip"];
                        else
                            [ns setValue:zipcdep forKey:@"street"];
                        
                        if ([countryp isKindOfClass:[NSNull class]] || countryp == (id)[NSNull null])
                            [ns setValue:@"" forKey:@"country"];
                        else
                            [ns setValue:countryp forKey:@"country"];
                        
                        [ns setValue:@"" forKey:@"number"];
                    }
                    else
                    {
                        [ns setValue:@"" forKey:@"number"];
                        [ns setValue:@"" forKey:@"city"];
                        [ns setValue:@"" forKey:@"street"];
                        [ns setValue:@"" forKey:@"zip"];
                        [ns setValue:@"" forKey:@"country"];
                    }
                    //            }
                    
                    //            [ns setObject:addressarr forKey:@"address"];
                    [con_array addObject:ns];
                }
                
            }
            //                }
            //    DebugLog(@"array content = %@", contactpickarr);
            //                    DebugLog(@"array content part 2 = %@", con_array);
            
            
            
            uniquearray = [[[NSSet setWithArray:con_array] allObjects] mutableCopy];
            
            //                    DebugLog(@"check contacts app has: %@",uniquearray);
            if ([uniquearray isKindOfClass:[NSArray class]]) {
                DebugLog(@"=====================> YES");
            } else {
                DebugLog(@"=====================> NO");
            }
            
            for (int ij =0; ij< [uniquearray count]; ij++)
            {
                NSString *newString = [[[[uniquearray objectAtIndex:ij] objectForKey:@"name"] substringToIndex:1] lowercaseString];
                
                NSCharacterSet *strCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"];//1234567890_"];
                
                strCharSet = [strCharSet invertedSet];
                //And you can then use a string method to find if your string contains anything in the inverted set:
                NSString *strphones = [[uniquearray objectAtIndex:ij] objectForKey:@"phone"];
                NSArray *numbersarray = [strphones componentsSeparatedByString:@","];
                //                    DebugLog(@"numbers array count: %lu",(unsigned long)[numbersarray count]);
                
                NSRange r = [newString rangeOfCharacterFromSet:strCharSet];
                if (r.location != NSNotFound || [numbersarray count] == 0) {
                    DebugLog(@"the string contains illegal characters");
                    
                    [uniquearray removeObjectAtIndex:ij];
                    ij= ij-1;
                }
            }
            
            
            //===================================================REMOVE BACKUP======================================//
            
            
            @try {
                NSError *err1 = nil;
                number_of_iterations=0;
                DebugLog(@"unique array count: %lu",(unsigned long)[uniquearray count]);
                
                part_array = [[NSMutableArray alloc]init];
                if ([uniquearray count] <= 300)
                {
                    //                            part_array = [uniquearray mutableCopy];
                    number_of_iterations=1;
                }
                else
                {
                    if ([uniquearray count] % 300 == 0)
                    {
                        number_of_iterations = (int)[uniquearray count]/300;
                    }
                    else
                    {
                        number_of_iterations = (int)[uniquearray count]/300 +1 ;
                    }
                }
                DebugLog(@"number of iterations are: %d", number_of_iterations);
                
                for (int numb =0; numb < number_of_iterations; numb++)
                {
                    part_array = [[NSMutableArray alloc]init];
                    
                    if (numb == number_of_iterations -1)
                    {
                        NSArray *tempArray = [uniquearray subarrayWithRange:NSMakeRange(numb*300, [uniquearray count]-(300*numb))];
                        [part_array addObjectsFromArray:tempArray];
                    }
                    else
                    {
                        NSArray *tempArray = [uniquearray subarrayWithRange:NSMakeRange(numb*300, 299)];
                        [part_array addObjectsFromArray:tempArray];
                    }
                    
                    DebugLog(@"number of elements in part array are: %lu", (unsigned long)[part_array count]);
                    
                    //=========================================================ADD BACKUP===========================================================//
                    
                    NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:(NSArray *)part_array options:NSJSONWritingPrettyPrinted error:&err1];
                    NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
                    NSString *encodedText = [self encodeToPercentEscapeString:jsonString];
                    
                    //                            DebugLog(@"jsonData as string:\n%@", jsonString);
                    
                    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=adddoublefilter&access_token=%@&device_id=%@",[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];   //&contacts=%@  ,[jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                    
                    DebugLog(@"addbackuppart url: %@",urlString1);
                    //            NSError *errorjs= nil;
                    
                    //        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
                    
                    //            NSString *firid = [NSString stringWithFormat:@"800"];
                    
                    responseData = [NSMutableData data];
                    
                    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString1]];
                    
                    NSString *params = [[NSString alloc] initWithFormat:@"contacts=%@",encodedText]; //[jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                    [request setHTTPMethod:@"POST"];
                    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
                    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                    //            [[NSURLConnection alloc] initWithRequest:request delegate:self];
                    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
                    
                    if(theConnection)
                    {
                        DebugLog(@"ncinccn");
                        responseData = [NSMutableData data];
                    }
                    else
                    {
                        DebugLog(@"theConnection is null");
                    }
                }
            }
            @catch (NSException *exception) {
                DebugLog(@"Exception: %@", exception.description);
            }
            @finally {
            }
            
        }
    }
}


#pragma NSURLConnectionDelegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    responseData = [NSMutableData new];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data
{
    //    [responseData appendData:data];
    responseData = [data mutableCopy];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    DebugLog(@"connection failed");
    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"backupstatus"];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    DebugLog(@"ccnnnipk");
    NSError *err;
    NSString *datatostring = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    if (responseData != nil)
    {
        NSDictionary  *json1aa=[NSJSONSerialization JSONObjectWithData:responseData options:0 error:&err];
        
        DebugLog(@"this now has json string url: %@",json1aa);
        
        //            if ([[json1aa objectForKey:@"success"] intValue] ==1)
        if([datatostring rangeOfString:@"\"success\":true"].location != NSNotFound || [datatostring rangeOfString:@"\"error\":\"\""].location != NSNotFound)
        {
            //             [SVProgressHUD showSuccessWithStatus:@"Success!"];
            connectiondoublefiler =connectiondoublefiler+1;
            
            DebugLog(@"connectiondoublefilter & number of iterations: %d .... %d",connectiondoublefiler,number_of_iterations);
            if (connectiondoublefiler == number_of_iterations)
            {
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                NSError *error;
                
                NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=getdoublefilter&amount=-1&id=%@&access_token=%@&device_id=%@&thumb=true",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
                
                DebugLog(@"profile url: %@",urlString1);
                
                NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
                
                NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
                
                if (signeddataURL1 == nil)
                {
                    alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                                       message:nil
                                                      delegate:self
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                }
                else
                {
                    json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                            options:kNilOptions
                                                              error:&error];
                    
                    DebugLog(@"get Double Filter : %@",json1);
                    con_array = [[NSMutableArray alloc]init];
                    NSDictionary *getdoub_dict = [json1 objectForKey:@"details"];
                    for (NSDictionary *contactdict in [getdoub_dict objectForKey:@"contacts"])
                    {
                        //////////////////////////////////////////   Compare App & Device Contacts   ////////////////////////////////////////////////////////////
                        
                        [con_array addObject:contactdict];
                    }
                    check_app_cont = [[NSMutableArray alloc]init];
                    testarr = [con_array mutableCopy];
                    int apps,dev;
                    int test;
                    test=0;
                    NSString *conmobile; //*conmain,*coniphone,*conpager,*conother;
                    NSDictionary *testdict;
                    
                    for (dev=0; dev< [testarr count]; dev++)
                    {
                        conmobile = [[testarr objectAtIndex:dev]objectForKey:@"mobile_num"];
                        
                        testdict = [testarr objectAtIndex:dev];
                        
                        for (apps=0; apps< [app_contacts count]; apps++)
                        {
                            //            if ([[con_array objectAtIndex:dev]objectForKey:@"home"])
                            //                {
                            NSString *homestr = [[[[app_contacts objectAtIndex:apps]objectForKey:@"mobile_num"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
                            
                            NSString *landphnstr = [[[[app_contacts objectAtIndex:apps]objectForKey:@"phone_num"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
                            
                            NSString *b_phn = [[[[app_contacts objectAtIndex:apps]objectForKey:@"b_phone_num"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
                            
                            if ( ([conmobile longLongValue] != 0 && [homestr longLongValue] != 0 && [conmobile rangeOfString:homestr].location != NSNotFound) || ([conmobile longLongValue] != 0 && [b_phn longLongValue] != 0 && [conmobile rangeOfString:b_phn].location != NSNotFound) || ([conmobile longLongValue] != 0 && [landphnstr longLongValue] != 0 && [conmobile rangeOfString:landphnstr].location != NSNotFound))
                            {
                                test=0;
                                [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"entered"];
                                break;
                            }
                            else
                            {
                                test=1;
                                [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"entered"];
                                continue;
                            }
                        }
                        if (test ==1)
                            [check_app_cont addObject:testdict];
                    }
                    
                    uniquearray = check_app_cont;
                    
                    if ([check_app_cont count] >0 && [con_array count] > 0)
                    {
                        con_array = [NSMutableArray arrayWithArray:[app_contacts arrayByAddingObjectsFromArray:uniquearray]];
                    }
                    else if ([app_contacts count] > 0 && [con_array count] == 0)
                    {
                        con_array =[app_contacts mutableCopy];
                    }
                    else if ([app_contacts count] == 0 && [con_array count] > 0)
                    {
                    }
                    //            DebugLog(@"now con array is: %@",con_array);
                    //-------------------------------------------------------------------------------------------------------------
                    
                    
                    
                    
                    
                    add_contacts_dict =[[NSMutableDictionary alloc]init];
                    //    NSMutableArray *numericcontactsarray = [[NSMutableArray alloc]init];
                    
                    for (int ij =0; ij< [con_array count]; ij++)
                    {
                        
                        NSString *newString = [[[[con_array objectAtIndex:ij] objectForKey:@"name"] substringToIndex:1] lowercaseString];
                        
                        NSCharacterSet *strCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"];//1234567890_"];
                        
                        strCharSet = [strCharSet invertedSet];
                        //And you can then use a string method to find if your string contains anything in the inverted set:
                        
                        NSRange r = [newString rangeOfCharacterFromSet:strCharSet];
                        if (r.location != NSNotFound) {
                        }
                        else
                        {
                            if (add_contacts_dict[newString])
                            {
                                NSMutableArray *checkarr =[[NSMutableArray alloc]init];
                                for (NSDictionary *check in [add_contacts_dict objectForKey:newString] )
                                {
                                    [checkarr addObject:check];
                                }
                                [checkarr addObject:[con_array objectAtIndex:ij]];
                                NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)];
                                NSArray *sortedArray=[checkarr sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];
                                
                                [add_contacts_dict removeObjectForKey:newString];
                                [add_contacts_dict setObject:sortedArray forKey:newString];
                            }
                            else
                            {
                                NSMutableArray *checkarr1 =[[NSMutableArray alloc]init];
                                [checkarr1 addObject:[con_array objectAtIndex:ij]];
                                //            [add_contacts_dict setObject:[con_array objectAtIndex:ij] forKey:newString];
                                [add_contacts_dict setObject:checkarr1 forKey:newString];
                            }
                        }
                    }
                    final_con_array= [[NSMutableArray alloc]init];
                    for (NSDictionary *dict in add_contacts_dict)
                        [final_con_array addObject:dict];
                    
                    
                    
#pragma mark -- this is the Beginning of NSLOCALIZEDINDEXEDCOLLATION SECTION - Bhaswar 16/7/2014
                    
                    // create a dictionary to store an array of objects for each section
                    NSMutableDictionary *tempSections = [NSMutableDictionary dictionary];
                    
                    // iterate through each dictionaey in the list, and put them into the correct section
                    for (NSDictionary *item in con_array)
                    {
                        // get the index of the section (Assuming the table index is showing A-#)
                        NSInteger indexName = [[UILocalizedIndexedCollation currentCollation] sectionForObject:[item valueForKey:@"name"] collationStringSelector:@selector(description)];
                        
                        NSNumber *keyName = [NSNumber numberWithInteger:indexName];
                        
                        // if an array doesnt exist for the key, create one
                        NSMutableArray *arrayName = [tempSections objectForKey:keyName];
                        if (arrayName == nil)
                        {
                            arrayName = [NSMutableArray array];
                        }
                        
                        // add the dictionary to the array (add the actual value as we need this object to sort the array later)
                        [arrayName addObject: item];   //[item valueForKey:@"fullname"]];
                        
                        
                        // put the array with new object in, back into the dictionary for the correct key
                        [tempSections setObject:arrayName forKey:keyName];
                    }
                    
                    /* now to do the sorting of each index */
                    
                    NSMutableDictionary *sortedSections = [NSMutableDictionary dictionary];
                    
                    // sort each index array (A..Z)
                    [tempSections enumerateKeysAndObjectsUsingBlock:^(id key, id array, BOOL *stop)
                     {
                         // sort the array - again, we need to tell it which selctor to sort each object by
                         NSArray *sortedArray = [[UILocalizedIndexedCollation currentCollation] sortedArrayFromArray:array collationStringSelector:@selector(description)];
                         NSSortDescriptor *sort =[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)];
                         sortedArray=[sortedArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];
                         [sortedSections setObject:[NSMutableArray arrayWithArray:sortedArray] forKey:key];
                     }];
                    
                    // set the global sectioned dictionary
                    indexedSections = sortedSections;
                    //            DebugLog(@"indexedsections: %@",indexedSections);
                    
                    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];;
                    del.arr_contacts_ad= [con_array mutableCopy];
                    
                    DebugLog(@"conarray mutablecopy count: %ld", (unsigned long)[arr_contacts_ad count]);
                    //                        [del loadContactsIntoDB:con_array];
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //1
                    NSString *documentsDirectory = [paths objectAtIndex:0];
                    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"data.plist"];
                    
                    NSMutableArray *data ;
                    data= [con_array mutableCopy];
                    [data writeToFile: path atomically:YES];
                    
                    appdelwebarray = [[NSMutableArray alloc]init];
                    appdelwebarray = [con_array mutableCopy];
                    
                    DebugLog(@"app delegate web array: %@",appdelwebarray);
                }
            }
        }
        else
        {
            NSString *err_str = [json1aa objectForKey:@"error"];
            DebugLog(@"error backing up: %@",err_str);
        }
        //            [SVProgressHUD showSuccessWithStatus:@"Success!"];
    }
    else
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        alert.tag=7;
        //        [alert show];
    }
}


#pragma encode string function
-(NSString*) encodeToPercentEscapeString:(NSString *)string {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                              (CFStringRef) string,
                                                              NULL,
                                                              (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
}

@end