//  ConNewContactsViewController.m
//  Created by Bhaswar's MacBook Air on 16/07/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
#import "ConNewContactsViewController.h"
#import <AddressBookUI/AddressBookUI.h>
#import "AppDelegate.h"
#import "ConProfileOwnViewController.h"
#import "ConNewProfileViewController.h"
#import "ConManageRequestsViewController.h"
#import "ConNewRequestsViewController.h"
#import "iToast.h"
#import "ConAccountSettingsViewController.h"
#import "ConInviteViewController.h"
#import "DBManager.h"
#import "ConNavigateViewController.h"
#import "ConDeviceProfileViewController.h"
#import "SVProgressHUD.h"
#import "ConSyncLoaderViewController.h"
#import "MDRadialProgressView.h"
#import "MDRadialProgressTheme.h"
#import "MDRadialProgressLabel.h"
#import "ConRequestViewController.h"
#import "ConAddBackUpViewController.h"
#import "ConQROptionsViewController.h"
#import "ConAddFriendViewController.h"
#import "ConNewDeviceViewController.h"
#import "GroupDetailsViewController.h"
#import "AppDelegate.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"
#import "ConLocateGroupViewController.h"
#import "iToast.h"
#import "UIView+Toast.h"
#import "ConFullSettingsViewController.h"
#import "ConPersonalProfileViewController.h"


@interface ConNewContactsViewController () {
    NSDictionary *cpeoples,*json1, *indexedSections, *jsonRemove;
    NSArray *cpeopleSectionTitles,*dictkeysarr;
    NSArray *cpeopleIndexTitles, *anintit;
    //    UITableView *tab_contact;
    NSMutableArray *con_array,*final_con_array,*filtered_arr, *part_array, *uniquearray, *backup_array;
    NSMutableDictionary *add_contacts_dict;
    NSMutableArray *app_contacts, *check_app_cont, *phonesarr, *new_con_array;
    UIAlertView *alert;
    int currentRow,currentSection,height,move;
    NSArray *searchresults;
    NSCharacterSet *whitespace;
    NSString *st, *con_count;
    UILabel *nouserlb;
    NSMutableArray *testarr;
    NSArray *array_phone, *array_app;
    NSString *googleMapUrlString,*appleMapUrlString;
    UIView *mapview,*curtainraiser, *blackBack;
    double latitude,longitude, userlat, userlong;
    int loadcheck,contacts_insertion, contacts_fetch, loadfromwebagain;
    UIActivityIndicatorView *act;
    NSTimer *timerrload, *timerresist, *timerr, *timeragain, *timer;
    BOOL filter;
    MDRadialProgressView *radialView3;
    UILabel *syncstatus;
    UIView *viewnew;
    int typeofgesture,successTag,number_of_iterations, connectionsuccess, connectiondoublefiler;
    NSMutableData *responseData;
    ABAddressBookRef addressBookRef;
    NSIndexPath *scrollToPath;
    UIButton *addContact;
    UITextField *txtSearchField, *searchField;
    UIImageView *mapPin;
    CGFloat scrollPosition;
    NSMutableArray *newcontactsapp;
    UIRefreshControl *refreshcontrolnew;
    NSMutableAttributedString *refreshString;
    NSString *decodedString;
    NSData *decodedData;
    UIImage *profilepic;
    UIImageView *recommended;
    BOOL checkIfFinished;
    float time_interval;
    AppDelegate *appDel;
}
@end
@implementation ConNewContactsViewController
@synthesize mainview,tab_contact,backuparr,add_contacts_dict,addtophonebook,getbackupdict,backarrnumbers,phnstr,ns, appdelwebarray;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    blackBack = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64)];
    //[blackBack setBackgroundColor:[UIColor blackColor]];
    [self.view addSubview:blackBack];
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
    
    // Add the gradient to the view
    [blackBack.layer insertSublayer:gradient atIndex:0];
    
    
    
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 15, [UIScreen mainScreen].bounds.size.width-41, 55)];
    searchBar.delegate=self;
    searchBar.tintColor=[UIColor whiteColor];
    // searchBar.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:1.0f];
    searchBar.backgroundColor = [UIColor clearColor];
    searchBar.barTintColor= [UIColor clearColor];
    searchBar.backgroundImage=[UIImage new];
    
    searchField = [searchBar valueForKey:@"_searchField"];
    searchField.textColor = [UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:1.0f];
    
    searchField.backgroundColor = [UIColor colorWithRed:(20.0f/255.0f) green:(102.0f/255.0f) blue:(110.0f/255.0f) alpha:1.0f];
    
    searchField.keyboardAppearance = UIKeyboardAppearanceDark;
    
    
    NSAttributedString *str = [[NSAttributedString alloc] initWithString:@"Search" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:(153.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f] }];
    searchField.attributedPlaceholder = str;
    searchBar.autocorrectionType=UITextAutocorrectionTypeNo;
    searchBar.keyboardAppearance=UIKeyboardAppearanceDark;
    [blackBack addSubview:searchBar];
    searchBar.hidden=NO;
    
    [searchBar setImage:[UIImage imageNamed:@"searchIcon"]
       forSearchBarIcon:UISearchBarIconSearch
                  state:UIControlStateNormal];
    
    recommended = [[UIImageView alloc] initWithFrame:CGRectMake(searchBar.frame.origin.x+searchBar.frame.size.width+3, 32, 28, 20)];
    recommended.image = [UIImage imageNamed:@"recommended"];
    [recommended setBackgroundColor:[UIColor clearColor]];
    
    [blackBack addSubview:recommended];
    
    
    addContact = [[UIButton alloc]init];
    //    [addContact setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-55, 25, (95.0f/2.0f), (60.0f/2.0f))];
    [addContact setFrame:CGRectMake(searchBar.frame.origin.x+searchBar.frame.size.width, 28, 60, 36)];
    
    [addContact setBackgroundColor:[UIColor clearColor]];
    [addContact addTarget:self action:@selector(changealpha) forControlEvents:UIControlEventTouchDragEnter];
    [addContact addTarget:self action:@selector(changealpha) forControlEvents:UIControlEventTouchDragExit];
    [addContact addTarget:self action:@selector(changealpha1) forControlEvents:UIControlEventTouchDown];
    [addContact addTarget:self action:@selector(goPlus:) forControlEvents:UIControlEventTouchUpInside];
    [blackBack addSubview:addContact];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"firstContacts"] isEqualToString:@"no"])
    {
        AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [del showTabStatus:YES];
        [del showTabValues:YES];
        [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    }
    else
    {
        AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [del showTabStatus:NO];
    }
    self.navigationController.navigationBarHidden=YES;
    scrollToPath = [NSIndexPath indexPathForRow:0 inSection:0];
    checkIfFinished = NO;
    time_interval = 1.5f;
    
    mainview = [[UIView alloc]initWithFrame:CGRectMake(0, 65, [UIScreen mainScreen].bounds.size.width, self.view.frame.size.height-60)];
    [self.view addSubview:mainview];
    mainview.backgroundColor=[UIColor whiteColor];
    
    //    CGRect frame = CGRectMake(110,225,100,100);
    act = [[UIActivityIndicatorView alloc] init];
    act.center = self.view.center;
    [act startAnimating];
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [act setColor:[UIColor blackColor]];
    [mainview addSubview:act];
    act.layer.zPosition=2;
    //    [SVProgressHUD showWithStatus:@"Loading.."];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    self.view.frame=CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height-44);
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    
    [self.view endEditing:YES];
    
    DebugLog(@"why coming to this??");
    connectionsuccess = 0;
    connectiondoublefiler = 0;
    move=0;
    loadcheck=0;
    contacts_insertion=0;
    contacts_fetch=0;
    loadfromwebagain =0;
    
    addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //1
    NSString *documentsDirectory = [paths objectAtIndex:0]; //2
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"data.plist"]; //3
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath: path]) //4
    {
        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"data" ofType:@"plist"]; //5
        [fileManager copyItemAtPath:bundle toPath: path error:&error]; //6
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"firstContacts"] isEqualToString:@"no"])
    {
        AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [del showTabStatus:YES];
        [del showTabValues:YES];
        [del.tabBarController.tabBar setFrame:CGRectMake(0,del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    }
    self.navigationController.navigationBarHidden=YES;
    
    filter=NO;
    currentRow=-1;
    height=0;
    currentSection=-1;
    
  
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    
    
    
    if ([[prefs objectForKey:@"firstContacts"] isEqualToString:@"yes"])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
        [UIApplication sharedApplication].idleTimerDisabled = YES;
        
        viewnew = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
        viewnew.backgroundColor=[UIColor blackColor];
        [self.view addSubview:viewnew];
        viewnew.layer.zPosition=3;
        
        UIView *whiteview = [[UIView alloc]initWithFrame:CGRectMake(55, 150, [UIScreen mainScreen].bounds.size.width-110, 210)];
        [self.view addSubview:whiteview];
        whiteview.backgroundColor=[UIColor clearColor];
        //
        // UIImageView *logo_img = [[UIImageView alloc]initWithFrame:CGRectMake((self.view.frame.size.width-92.5)/2, 31, 92.5f, 16)];
        //        logo_img.image=[UIImage imageNamed:@"logomod.png"];
        //        UIImageView *logo_img = [[UIImageView alloc] initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-252)/2, [UIScreen mainScreen].bounds.size.height/9.2, 252, 87)];     //16/09/15
        //        logo_img.image=[UIImage imageNamed:@"Logo"];
        //        [viewnew addSubview:logo_img];
        
        UIImageView *logo_img=[[UIImageView alloc]init];
        logo_img.frame = CGRectMake(27+([UIScreen mainScreen].bounds.size.width-160)/2, 32, 101, 20);  //16/09/15
        logo_img.image=[UIImage imageNamed:@"new_logo"];
        [viewnew addSubview:logo_img];
        
        
        radialView3 = [self progressViewWithFrame:CGRectMake(0, 140, 200, 200)];
        radialView3.progressTotal = 100;
        radialView3.progressCounter = 0;
        radialView3.theme.completedColor = [UIColor whiteColor];//colorWithRed:132/255.0 green:23/255.0 blue:23/255.0 alpha:1.0
        radialView3.theme.incompletedColor = [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]; //lightGrayColor
        radialView3.theme.thickness = 9.5f;
        radialView3.theme.sliceDividerHidden = YES;
        radialView3.theme.centerColor = [UIColor clearColor];
        //        radialView3.label.text
        radialView3.center= self.view.center;
        [viewnew addSubview:radialView3];
        
        syncstatus = [[UILabel alloc]initWithFrame:CGRectMake(0, self.view.bounds.size.height-90, [UIScreen mainScreen].bounds.size.width, 40)];
        syncstatus.text=@"Welcome!";
        syncstatus.textColor=[UIColor whiteColor];
        syncstatus.textAlignment=NSTextAlignmentCenter;
        syncstatus.font= [UIFont fontWithName:@"ProximaNova-Light" size:18];
        [viewnew addSubview:syncstatus];
        
        timerr = [NSTimer scheduledTimerWithTimeInterval:time_interval
                                                  target:self
                                                selector:@selector(targetMethodFirstTime:)
                                                userInfo:nil
                                                 repeats:YES];
        [act stopAnimating];
        
        NSError *error = nil;
        NSString *urlString =[NSString stringWithFormat:@"https://budnav.com/ext/?action=contacts&amount=-1&id=%@&access_token=%@&device_id=%@&thumb=true",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
        
        DebugLog(@"contacts -- now the url is : %@",urlString);
        
        NSString *newString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSData *signeddataURL =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString]];
        
        if (signeddataURL == nil)
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
            [act removeFromSuperview];
        }
        else
        {
            json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL //1
                                                    options:kNilOptions
                                                      error:&error];
            DebugLog(@"JSON FIRST HIT:%@",json1);
            
            NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
            
            if (![errornumber isEqualToString:@"0"])
            {
                NSString *err_str = [json1 objectForKey:@"error"];
                alert = [[UIAlertView alloc] initWithTitle:@"Error in Retrieving Data!"
                                                   message:err_str
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                [alert show];
                [act removeFromSuperview];
            }
            else
            {
                app_contacts = [[NSMutableArray alloc]init];
                //        //DebugLog(@"details req= %@",[json1 objectForKey:@"details"]);
                if ([[json1 objectForKey:@"details"] isKindOfClass:[NSNull class]] || [json1 objectForKey:@"details"] == (id)[NSNull null] ||[[json1 objectForKey:@"details"] count] ==0)
                {
                    [act removeFromSuperview];
                    //            [SVProgressHUD dismiss];
                }
                else
                {
                    ////////////////////////////   App Contacts Found   /////////////////////////////////
                    
                    //            if ([string rangeOfString:@"123"].location == NSNotFound) {
                    //            } else {}
                    
                    NSDictionary *contacts_temp_dict = [json1 objectForKey:@"details"];
                    
                    DebugLog(@"CONTACTS TEMP DICT:%@",contacts_temp_dict);
                    newcontactsapp = [[NSMutableArray alloc]init];
                    NSString *result;
                    int con_count1;
                    con_count1 = 0;
                    //                        if (con_count1 > 0)
                    //                        {
                    for (NSDictionary *dict in [contacts_temp_dict objectForKey:@"contacts"])
                    {
                        [newcontactsapp addObject:dict]; //[dict objectForKey:@"fullname"]
                    }
                    result = [newcontactsapp componentsJoinedByString:@","];
                    
                    if (con_count1 == 1)
                    {
                        //                                [[[[iToast makeText:[NSString stringWithFormat:@" %@ is a new contact",result]]
                        //                                   setGravity:iToastGravityTop] setDuration:iToastDurationLong] show];
                    }
                    else
                    {
                        //                                [[[[iToast makeText:[NSString stringWithFormat:@" %@ are new contacts",result]]
                        //                                   setGravity:iToastGravityTop] setDuration:iToastDurationLong] show];
                    }
                    
                    DebugLog(@"newcontactsapp: %@",newcontactsapp);
                    //////////////////////////////////////    add to phonebook    ////////////////////////////////////
                    //                    if ([[prefs objectForKey:@"copycontacts"] isEqualToString:@"no"])
                    
                    //                        }   //con_count closing brace
                    for (NSDictionary *dict in [contacts_temp_dict objectForKey:@"contacts"])
                    {
                        [app_contacts addObject:dict];
                    }
                    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%ld",[app_contacts count]] forKey:@"app_contacts_count"];
                }
            }
        }
        //--------------------------------------------------------------------------------------------------------------
        
        ////////////////////////////////////// Using Getbackup To Fetch Device Contacts ///////////////////////////////////////////
        
        NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=getbackup&id=%@&access_token=%@&device_id=%@&thumb=true",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
        
        DebugLog(@"contacts -- now the url is : %@",urlString1);
        
        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
        
        if (signeddataURL1 == nil)
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            //                [alert show];
            [act removeFromSuperview];
            //            [SVProgressHUD dismiss];
        }
        else
        {
            json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                    options:kNilOptions
                                                      error:&error];
            DebugLog(@"JSON FIRST HIT:%@",json1);
            
            NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
            
            if (![errornumber isEqualToString:@"0"])
            {
                NSString *err_str = [json1 objectForKey:@"error"];
                alert = [[UIAlertView alloc] initWithTitle:@"Error in Retrieving Data!"
                                                   message:err_str
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                [alert show];
                [act removeFromSuperview];
            }
            else
            {
                backup_array = [[NSMutableArray alloc]init];
                if ([[json1 objectForKey:@"details"] isKindOfClass:[NSNull class]] || [json1 objectForKey:@"details"] == (id)[NSNull null] ||[[json1 objectForKey:@"details"] count] ==0)
                {
                    [act removeFromSuperview];
                }
                else
                {
                    
                    NSDictionary *contacts_temp_dict = [json1 objectForKey:@"details"];
                    
                    DebugLog(@"CONTACTS TEMP DICT:%@",contacts_temp_dict);
                    // newcontactsapp = [[NSMutableArray alloc]init];
                    //                    NSString *result;
                    int con_count1;
                    con_count1 = 0;
                    for (NSDictionary *dict in [contacts_temp_dict objectForKey:@"contacts"])
                    {
                        [backup_array addObject:dict]; //[dict objectForKey:@"fullname"]
                    }
                    //result = [newcontactsapp componentsJoinedByString:@","];
                    
                    if (con_count1 == 1)
                    {
                        //                                [[[[iToast makeText:[NSString stringWithFormat:@" %@ is a new contact",result]]
                        //                                   setGravity:iToastGravityTop] setDuration:iToastDurationLong] show];
                    }
                    else
                    {
                        //                                [[[[iToast makeText:[NSString stringWithFormat:@" %@ are new contacts",result]]
                        //                                   setGravity:iToastGravityTop] setDuration:iToastDurationLong] show];
                    }
                    
                    DebugLog(@"Back Up Contacts: %@",backup_array);
                    //////////////////////////////////////    add to phonebook    ////////////////////////////////////
                    //                    if ([[prefs objectForKey:@"copycontacts"] isEqualToString:@"no"])
                    
                    //                        }   //con_count closing brace
                    //                    for (NSDictionary *dict in [contacts_temp_dict objectForKey:@"contacts"])
                    //                    {
                    //                        [app_contacts addObject:dict];
                    //                    }
                    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%ld",[backup_array count]] forKey:@"backup_count"];
                }
            }
        }
        
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        
        con_array = [[NSMutableArray alloc]init];
        
        for (NSDictionary *appDict in newcontactsapp) {
            [con_array addObject:appDict];
        }
        
        for (NSDictionary *backupDict in backup_array) {
            
            //for (NSDictionary *tempDict in newcontactsapp) {
            
            //                if ([[tempDict objectForKey:@"fullname"] isEqualToString:[backupDict objectForKey:@"fullname"]] && [[tempDict objectForKey:@"mobile_num"] isEqualToString:[backupDict objectForKey:@"phone"]]) {
            //
            //                    DebugLog(@"Budnav contacts exist with same backup contact name");
            //                }
            //                else{
            [con_array addObject:backupDict];
            //                }
            //            }
        }
        
        
       // DebugLog(@"Combined Array:%ld %@",[con_array count],con_array);
        
        [self getfilterbackup];
        
        
//        //////////////////////////////////// Added on 11.5.16 ///////////////////////////////////////////////////////////
//        
//        appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//        
//        if ([appDel.signup isEqualToString:@"Signup"])
//        {
//            appDel.signup = @"";
//            
//            alert = [[UIAlertView alloc] initWithTitle:@"Welcome to Budnav!" message:@"Your contacts are now secured and accessible from any computer, tablet and smartphone with your login credentials!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
//            
//            //        UIViewController *vc = [self visibleViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
//            //
//            //
//            //        [vc.view makeToast:@"Weolcome To Budnav" duration:3 position:CSToastPositionTop title:nil];
//        }
//        
//        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        
        
        //        [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"firstContacts"];
    }
    
    //////////////////////////////////////    After 1st time, Load data from Local Database    //////////////////////////////////////////////
    else
    {
        //        NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=contacts&amount=-1&id=%@&access_token=%@&device_id=%@&thumb=true",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
        //
        //        DebugLog(@"contacts -- now the url is : %@",urlString1);
        //
        //        cpeopleIndexTitles = @[@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z", @"#"];
        //
        //        anintit= @[@"a", @"b", @"c", @"d", @"e", @"f", @"g", @"h", @"i", @"j", @"k", @"l", @"m", @"n", @"o", @"p", @"q", @"r", @"s", @"t", @"u", @"v", @"w", @"x", @"y", @"z", @"#"];
        //
        //        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //1
        //        NSString *documentsDirectory = [paths objectAtIndex:0]; //2
        //        NSString *path = [documentsDirectory stringByAppendingPathComponent:@"data.plist"]; //3
        //
        //        NSMutableArray *savedStock = [[NSMutableArray alloc] initWithContentsOfFile: path];
        //
        //        DebugLog(@"savedstock has: %@",savedStock);
        //        //load from savedStock example int value
        //        //        int value;
        //
        //        [act startAnimating];
        //        act.layer.zPosition=2;
        //
        //        dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        //        dispatch_async(q, ^{
        //
        //            AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        //
        //            DebugLog(@"elshe");
        //
        //
        //            con_array = [[NSMutableArray alloc]init];
        //
        //            NSData *dataRepresentingSavedArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"Contactslocaldb"];
        //            if (dataRepresentingSavedArray != nil)
        //            {
        //                NSArray *oldSavedArray = [NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
        //                if (oldSavedArray != nil)
        //                    con_array = [[NSMutableArray alloc] initWithArray:oldSavedArray];
        //                else
        //                    con_array = [[NSMutableArray alloc] init];
        //            }
        //
        //            DebugLog(@"xxxxxxxxxx");
        //            if ([con_array count]== 0)
        //            {
        //                con_array = [savedStock mutableCopy];
        //                DebugLog(@"yyyyyyyyyy");
        //            }
        //            if ([con_array count]== 0)
        //            {
        //                con_array = [del.newarr_contacts_ad mutableCopy];
        //                DebugLog(@"zzzzzzzzzz");
        //            }
        //            DebugLog(@"conarray bms %@",con_array);
        //            check_app_cont = [[NSMutableArray alloc]init];
        //            testarr = [[NSMutableArray alloc]init];
        //            testarr = [con_array mutableCopy];
        //            int test;
        //            test=0;
        //
        //            DebugLog(@"con array error khao: %@",con_array);
        //            if ([con_array count] == 0)
        //            {
        //                [SVProgressHUD showErrorWithStatus:@"Back Up Could Not be restored due to Server Connection Problem"];
        //            }
        //
        //
        //            add_contacts_dict =[[NSMutableDictionary alloc]init];
        //            //    NSMutableArray *numericcontactsarray = [[NSMutableArray alloc]init];
        //
        //            for (int ij =0; ij< [con_array count]; ij++)
        //            {
        //                DebugLog(@"Crash Log:%@",[con_array objectAtIndex:ij]);
        //
        //                NSString *newString = [[[[con_array objectAtIndex:ij] objectForKey:@"fullname"] substringToIndex:1] lowercaseString];
        //
        //                NSCharacterSet *strCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"];//1234567890_"];
        //
        //                strCharSet = [strCharSet invertedSet];
        //                //And you can then use a string method to find if your string contains anything in the inverted set:
        //
        //                NSRange r = [newString rangeOfCharacterFromSet:strCharSet];
        //                if (r.location != NSNotFound) {
        //                }
        //                else
        //                {
        //                    if (add_contacts_dict[newString])
        //                    {
        //                        NSMutableArray *checkarr =[[NSMutableArray alloc]init];
        //                        for (NSDictionary *check in [add_contacts_dict objectForKey:newString])
        //                        {
        //                            [checkarr addObject:check];
        //                        }
        //                        [checkarr addObject:[con_array objectAtIndex:ij]];
        //                        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"fullname" ascending:YES selector:@selector(caseInsensitiveCompare:)];
        //                        NSArray *sortedArray=[checkarr sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];
        //
        //                        [add_contacts_dict removeObjectForKey:newString];
        //                        [add_contacts_dict setObject:sortedArray forKey:newString];
        //                    }
        //                    else
        //                    {
        //                        NSMutableArray *checkarr1 =[[NSMutableArray alloc]init];
        //                        if (![[con_array objectAtIndex:ij] isKindOfClass:[NSNull class]]) {
        //
        //                            [checkarr1 addObject:[con_array objectAtIndex:ij]];//////////////////// Crashing /////////////////////////
        //                            //            [add_contacts_dict setObject:[con_array objectAtIndex:ij] forKey:newString];
        //                            [add_contacts_dict setObject:checkarr1 forKey:newString];
        //
        //                        }
        //
        //                    }
        //                }
        //            }
        //
        //            final_con_array= [[NSMutableArray alloc]init];
        //            for (NSDictionary *dict in add_contacts_dict)
        //                [final_con_array addObject:dict];
        
        NSData *contactsWithoutIndexed = [[NSUserDefaults standardUserDefaults] objectForKey:@"Contactslocaldb"];
        
        con_array = [NSKeyedUnarchiver unarchiveObjectWithData:contactsWithoutIndexed];
        
#pragma mark -- this is the Beginning of NSLOCALIZEDINDEXEDCOLLATION SECTION - Bhaswar 16/7/2014
        
        
        NSData *indexedSavedArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"IndexedContactslocaldb"];
        
        NSArray *oldSavedArray = [NSKeyedUnarchiver unarchiveObjectWithData:indexedSavedArray];
        indexedSections = [oldSavedArray objectAtIndex:0];
//        DebugLog(@"indexed sections:%ld...... %@", (unsigned long)[oldSavedArray count], oldSavedArray);
//        
//        DebugLog(@"indexed sections & count:%ld...... %@", (unsigned long)[indexedSections count], indexedSections);
        
#pragma mark -- this is the End of NSLOCALIZEDINDEXEDCOLLATION SECTION - Bhaswar 16/7/2014
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [tab_contact removeFromSuperview];
            tab_contact=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, mainview.frame.size.height-46)];
            [mainview addSubview:tab_contact];
            tab_contact.backgroundColor=[UIColor clearColor];
            tab_contact.dataSource=self;
            tab_contact.delegate=self;
            tab_contact.separatorStyle=UITableViewCellSeparatorStyleNone;
            tab_contact.showsVerticalScrollIndicator=NO;
            
            //                tab_contact.scrollEnabled = NO;
            //
            //                timer = [NSTimer scheduledTimerWithTimeInterval:0.2
            //                                                              target:self
            //                                                            selector:@selector(resistInteraction:)
            //                                                            userInfo:nil
            //                                                             repeats:YES];
            
            
            ///////////////// Pull To Refresh //////////////////////
            UITableViewController *tableViewController = [[UITableViewController alloc] init];
            tableViewController.tableView = tab_contact;
            
            refreshcontrolnew = [[UIRefreshControl alloc] init];
            [refreshString addAttributes:@{NSForegroundColorAttributeName : [UIColor grayColor]} range:NSMakeRange(0, refreshString.length)];
            refreshcontrolnew.attributedTitle = refreshString;
            [refreshcontrolnew addTarget:self action:@selector(reloadAgain) forControlEvents:UIControlEventValueChanged];
            tableViewController.refreshControl = refreshcontrolnew;
            refreshcontrolnew.tintColor=[UIColor grayColor];
            
            [act removeFromSuperview];
            
            
            AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            
            if (!filter)
            {
                if ([con_array count] > 0)
                {
                    if ([[UIDevice currentDevice].systemVersion floatValue] <= 7.1f)
                    {
                        scrollToPath = [NSIndexPath indexPathForRow:del.scrollrow inSection:del.scrollsection];
                        DebugLog(@"scrollpath= %@",scrollToPath);
                        
                        if ([[indexedSections objectForKey:[NSNumber numberWithInteger:del.scrollsection]] count] > 0)
                            [tab_contact scrollToRowAtIndexPath:scrollToPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
                    }
                    else
                    {
                        tab_contact.contentOffset = CGPointMake(0, del.scrolloffsety);
                    }
                }
            }
            
            if ([tab_contact respondsToSelector:@selector(setSectionIndexColor:)]) {
                //                    tab_contact.sectionIndexColor = [UIColor colorWithRed:153.0f/255.0f green:153.0f/255.0f blue:153.0f/255.0f alpha:1];
                tab_contact.sectionIndexColor = [UIColor colorWithRed:40.0f/255.0f green:170.0f/255.0f blue:185.0f/255.0f alpha:1];
                tab_contact.sectionIndexTrackingBackgroundColor = [UIColor whiteColor];
            }
//            [tab_contact reloadData];
            
            curtainraiser = [[UIView alloc]initWithFrame:tab_contact.frame];
            curtainraiser.backgroundColor=[UIColor clearColor];
            [mainview addSubview:curtainraiser];
            [curtainraiser removeFromSuperview];
            tab_contact.scrollEnabled=YES;
            
            searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, -1, [UIScreen mainScreen].bounds.size.width, 51)];
            searchBar.delegate=self;
            searchBar.tintColor=[UIColor darkGrayColor];
            searchBar.backgroundColor=[UIColor clearColor];
            //                [searchBar setSearchBarStyle:UISearchBarStyleMinimal];
            //                searchBar.barTintColor= [UIColor colorWithRed:238.0f/255.0f green:238.0f/255.0f blue:238.0f/255.0f alpha:1];
            [searchBar setImage:[UIImage imageNamed:@"searchIcon"]
               forSearchBarIcon:UISearchBarIconSearch
                          state:UIControlStateNormal];
            
            searchBar.barTintColor= [UIColor clearColor];
            searchBar.backgroundImage=[UIImage new];
            
            searchBar.keyboardAppearance = UIKeyboardAppearanceDark;
            searchBar.placeholder= @"Search Contacts";
            //[mainview addSubview:searchBar];
            searchBar.hidden=YES;
            
            nouserlb= [[UILabel alloc]initWithFrame:CGRectMake(0, 50, [UIScreen mainScreen].bounds.size.width, 30)];
            [mainview addSubview:nouserlb];
            nouserlb.text=@"No results";
            nouserlb.backgroundColor=[UIColor clearColor];
            nouserlb.textAlignment=NSTextAlignmentCenter;
            nouserlb.textColor=[UIColor grayColor];
            nouserlb.hidden=YES;
            
            del.scrollsection=0;
            del.scrollrow= 0;
            //                [SVProgressHUD dismiss];
            [self performSelectorInBackground:@selector(yesSuccess)
                                   withObject:nil];
        });
        //  });
        //        NSTimer *timerr;
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"firstContacts"];
    
    
   
    
}

-(void)resistInteraction: (NSTimer *)timer1
{
    [curtainraiser removeFromSuperview];
    tab_contact.scrollEnabled=YES;
    [timer1 invalidate];
}


-(void)targetMethod: (NSTimer *)timer1
{
    [self loadContactsFromWeb];
    [timer1 invalidate];
}

-(void) stoploader: (NSTimer *)sender
{
    [act stopAnimating];
}

- (NSString *)getImageFilename:(NSString *)cpeople
{
    NSString *imageFilename = [[cpeople lowercaseString] stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    imageFilename = [imageFilename stringByAppendingString:@".jpg"];
    
    return imageFilename;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    return nil;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tab_contact dequeueReusableCellWithIdentifier:CellIdentifier];
    //    cell = Nil;
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    else
    {
        NSArray* subviews = [cell.contentView subviews];
        for (UIView* view in subviews)
        {
            [view removeFromSuperview];
        }
    }
    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    if (!filter)
    {
        
        // DebugLog(@"INDEX SECTION:%ld",indexPath.section);
        id object = [[indexedSections objectForKey:[NSNumber numberWithInteger:indexPath.section]] objectAtIndex:indexPath.row];
        
        NSString *cpeople;
        
        //        if ([[object objectForKey:@"id"]intValue] > 0)   //crashes budnav
        //           cpeople= [object objectForKey:@"fullname"];
        //        else
        cpeople= [object objectForKey:@"fullname"];
        
        NSArray *myWords = [cpeople componentsSeparatedByString:@" "];
        
        NSString *firstname = [[myWords objectAtIndex:0] capitalizedString];
        if ([[myWords objectAtIndex:0] isKindOfClass:[NSNull class]] || [myWords objectAtIndex:0] == (id)[NSNull null] ||[[myWords objectAtIndex:0] length] ==0)
        {
            firstname = @"";
        }
        
        NSString *lastname,*location;
        lastname=@"";
        
        UIImageView *typeimg = [[UIImageView alloc]init];
        [cell.contentView addSubview:typeimg];
        
        UIImageView *typeimg1 = [[UIImageView alloc]init];
        [cell.contentView addSubview:typeimg1];
        
        DebugLog(@"OBJECT:%@",object);
        
        
        int type = [[object objectForKey:@"business"] intValue];
        if (type == 1)
        {
            typeimg.image=[UIImage imageNamed:@"userclose.png"];
            typeimg1.image=[UIImage imageNamed:@"userclose.png"];
            
            lastname =[[NSString stringWithFormat:@"%@",[object objectForKey:@"surname"]] capitalizedString];
            
            if ([[object objectForKey:@"b_city"] isKindOfClass:[NSNull class]] || [object objectForKey:@"b_city"] == (id)[NSNull null] || [[object objectForKey:@"b_city"] length]==0)
            {
                if ([[object objectForKey:@"b_country"] isKindOfClass:[NSNull class]] || [object objectForKey:@"b_country"] == (id)[NSNull null] || [[object objectForKey:@"b_country"] length]==0)
                    location=@"";
                else
                    location= [NSString stringWithFormat:@"%@",[object objectForKey:@"b_country"]];
            }
            else
            {
                if ([[object objectForKey:@"b_country"] isKindOfClass:[NSNull class]] || [object objectForKey:@"b_country"] == (id)[NSNull null] || [[object objectForKey:@"b_country"] length]==0)
                    location= [NSString stringWithFormat:@"%@",[object objectForKey:@"b_city"]];
                else
                    location= [NSString stringWithFormat:@"%@, %@",[object objectForKey:@"b_city"],[object objectForKey:@"b_country"]];
            }
            
            
        }
        
        else
        {
            
            for (int k=1;k< [myWords count];k++)
            {
                if (k==1)
                {
                    if ([[myWords objectAtIndex:k] isKindOfClass:[NSNull class]] || [myWords objectAtIndex:k] == (id)[NSNull null] ||[[myWords objectAtIndex:k] length] ==0 || [[myWords objectAtIndex:k] isEqualToString:@"(null)"])
                    {
                        lastname = @"";
                    }
                    else
                        lastname= [[NSString stringWithFormat:@"%@",[myWords objectAtIndex:k]] capitalizedString];
                }
                else
                    lastname= [[NSString stringWithFormat:@"%@ %@",lastname,[myWords objectAtIndex:k]] capitalizedString];
            }
            if([[object objectForKey:@"id"] intValue] != 0)
            {
                typeimg.image=[UIImage imageNamed:@"usericon.png"];
                typeimg1.image=[UIImage imageNamed:@"usericon.png"];
                
                //                lastname =[NSString stringWithFormat:@"%@",[object objectForKey:@"surname"]];
                
                if ([[object objectForKey:@"city"] isKindOfClass:[NSNull class]] || [object objectForKey:@"city"] == (id)[NSNull null] || [[object objectForKey:@"city"] length]==0)
                {
                    if ([[object objectForKey:@"country"] isKindOfClass:[NSNull class]] || [object objectForKey:@"country"] == (id)[NSNull null] || [[object objectForKey:@"country"] length]==0)
                        location=@"";
                    else
                        location= [NSString stringWithFormat:@"%@",[object objectForKey:@"country"]];
                }
                else
                {
                    if ([[object objectForKey:@"country"] isKindOfClass:[NSNull class]] || [object objectForKey:@"country"] == (id)[NSNull null] || [[object objectForKey:@"country"] length]==0)
                        location= [NSString stringWithFormat:@"%@",[object objectForKey:@"city"]];
                    else
                        location= [NSString stringWithFormat:@"%@, %@",[object objectForKey:@"city"],[object objectForKey:@"country"]];
                }
                
            }
            else
            {
                //                typeimg.image=[UIImage imageNamed:@"icon3.png"];
                //                typeimg1.image=[UIImage imageNamed:@"icon3.png"];
                
                location = @"";
            }
        }
        name = Nil;
        name = [[UILabel alloc]init];
        [cell.contentView addSubview:name];
        name.backgroundColor=[UIColor clearColor];
        name.text=firstname;
        name.font=[UIFont fontWithName:@"ProximaNova-Bold" size:17];
        name.textAlignment=NSTextAlignmentLeft;
        
        surname = [[UILabel alloc]init];
        [cell.contentView addSubview:surname];
        surname.backgroundColor=[UIColor clearColor];
        surname.text=lastname;
        surname.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
        surname.textAlignment=NSTextAlignmentLeft;
        
        mapPin = [[UIImageView alloc]init];
        mapPin.image = [UIImage imageNamed:@"Map Marker Contacts Screen copy2x.png"];
        mapPin.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:mapPin];
        
        locationlb = [[UILabel alloc]init];
        [cell.contentView addSubview:locationlb];
        locationlb.backgroundColor=[UIColor clearColor];
        locationlb.text=location;
        locationlb.font=[UIFont fontWithName:@"ProximaNova-Regular" size:13];
        locationlb.textAlignment=NSTextAlignmentLeft;
        locationlb.textColor = [UIColor colorWithRed:153.0f/255.0f green:153.0f/255.0f blue:153.0f/255.0f alpha:1];
        
        selectedrowbg = [[UIImageView alloc]init];
        [cell.contentView addSubview:selectedrowbg];
        selectedrowbg.image= [UIImage imageNamed:@"row.png"];
        selectedrowbg.backgroundColor=[UIColor lightGrayColor];
        selectedrowbg.clipsToBounds=YES;
        selectedrowbg.userInteractionEnabled=YES;
        
        phonebt = [UIButton buttonWithType:UIButtonTypeCustom];
        [phonebt setBackgroundImage:[UIImage imageNamed:@"phone_con.png"] forState:UIControlStateNormal];
        phonebt.backgroundColor=[UIColor clearColor];
        phonebt.tag = indexPath.section*1000 +indexPath.row;
        
        [phonebt addTarget:self action:@selector(phonefun:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:phonebt];
        
        chatbt = [UIButton buttonWithType:UIButtonTypeCustom];
        [chatbt setBackgroundImage:[UIImage imageNamed:@"chat.png"] forState:UIControlStateNormal];
        chatbt.backgroundColor=[UIColor clearColor];
        chatbt.tag = indexPath.section*1000 +indexPath.row;
        [chatbt addTarget:self action:@selector(chatfun:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:chatbt];
        
        smsbt = [UIButton buttonWithType:UIButtonTypeCustom];
        [smsbt setBackgroundImage:[UIImage imageNamed:@"message_con.png"] forState:UIControlStateNormal];
        smsbt.backgroundColor=[UIColor clearColor];
        smsbt.tag = indexPath.section*1000 +indexPath.row;
        [smsbt addTarget:self action:@selector(messagefun:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:smsbt];
        
        mapBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [mapBtn setBackgroundImage:[UIImage imageNamed:@"Nav button-1.png"] forState:UIControlStateNormal];
        mapBtn.backgroundColor=[UIColor clearColor];
        mapBtn.tag = indexPath.section*1000 +indexPath.row;
        [mapBtn addTarget:self action:@selector(mapFunc:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:mapBtn];
        
        
        divider = [[UIImageView alloc]init];
        [cell.contentView addSubview:divider];
        //divider.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f];
        divider.backgroundColor = [UIColor colorWithRed:(204.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f];                 //16/09/15
        
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        BOOL isSelected = [self.selectedIndexPaths containsObject:indexPath];
        if (isSelected)
        {
            CGSize maximumSize = CGSizeMake(160, 50);
            
            UIFont *myFont = [UIFont fontWithName:@"ProximaNova-Bold" size:17];
            CGSize myStringSize = [firstname sizeWithFont:myFont
                                   
                                        constrainedToSize:maximumSize
                                   
                                            lineBreakMode:NSLineBreakByWordWrapping];
            if ([location isEqualToString:@""])
            {
                name.frame=CGRectMake(20, 0, myStringSize.width, 48);
                surname.frame=CGRectMake(name.frame.origin.x+name.frame.size.width+4, 0, 210, 48);
            }
            else
            {
                name.frame=CGRectMake(20, 0, myStringSize.width, 28);
                surname.frame=CGRectMake(name.frame.origin.x+name.frame.size.width+4, 0, 210, 28);
            }
            
            
            if ([location isKindOfClass:[NSNull class]] || location == (id)[NSNull null] || [location length] == 0 || [location  isEqualToString:@""]) {
                
                
                //                DebugLog(@"XXXXXXXXXXXXXXXXXXXXXX");
                //                DebugLog(@"LOCATION == %@   Name == %@    Surname == %@",location,name.text,surname.text);
                mapPin.frame = CGRectMake(0, 0, 0, 0);
                
            }else{
                
                //                DebugLog(@"BOXBOXBOXBOX");
                //                DebugLog(@"LOCATION == %@   Name == %@    Surname == %@",location,name.text,surname.text);
                mapPin.frame = CGRectMake(20, 29, (18.0f/2.0f), 11);//(25.0f/2.0f));
                
            }
            locationlb.frame=CGRectMake(31, 25, 300, 20);
            //        name.frame= CGRectMake(20, 50, 240, 50);
            //            typeimg.frame= CGRectMake(262, 62, 36, 0);
            selectedrowbg.frame= CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-10, 50);
            phonebt.frame = CGRectMake(40, 15, 20.5f, 20);
            chatbt.frame = CGRectMake(112, 15, 20.5f, 20);
            
            
            if ([[object objectForKey:@"email"] isKindOfClass:[NSNull class]] || [object objectForKey:@"email"] == (id)[NSNull null] || [[object objectForKey:@"email"] isEqualToString:@"(null)"] || [[object objectForKey:@"email"] length] == 0)
                smsbt.frame = CGRectMake(184, 18, 0, 0);
            else
                smsbt.frame = CGRectMake(184, 18, 20.5f, 14);
            
            if ([[object objectForKey:@"id"] intValue] == 0 && [[object objectForKey:@"userid"] intValue] == 0) {
                mapBtn.frame = CGRectMake(256, 18, 0, 0);
            }
            else
                mapBtn.frame = CGRectMake(256, 18, (40/2), (40/2));
            // divider.frame= CGRectMake(0, 99, [UIScreen mainScreen].bounds.size.width, 1);
            divider.frame= CGRectMake(0, 99, [UIScreen mainScreen].bounds.size.width, 0.5f);   //16/09/15
            //typeimg1.frame= CGRectMake(262, 12, 36, 0);
            
            UISwipeGestureRecognizer *swipeLeftRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGestureLeft:)];
            [swipeLeftRight setDirection:UISwipeGestureRecognizerDirectionLeft];
            [cell addGestureRecognizer:swipeLeftRight];
        }
        else
        {
            CGSize maximumSize = CGSizeMake(160, 50);
            
            UIFont *myFont = [UIFont fontWithName:@"ProximaNova-Bold" size:17];
            CGSize myStringSize = [firstname sizeWithFont:myFont
                                   
                                        constrainedToSize:maximumSize
                                            lineBreakMode:NSLineBreakByWordWrapping];
            
            if ([location isEqualToString:@""])
            {
                name.frame=CGRectMake(20, 0, myStringSize.width, 50);
                surname.frame=CGRectMake(name.frame.origin.x+name.frame.size.width+4, 0, 210, 50);
            }
            else
            {
                name.frame=CGRectMake(20, 0, myStringSize.width, 30);
                surname.frame=CGRectMake(name.frame.origin.x+name.frame.size.width+4, 0, 210, 30);
            }
            
            //======================================== MAP PIN check================///
            
            if ([location isKindOfClass:[NSNull class]] || location == (id)[NSNull null] || [location length] == 0 || [location isEqualToString:@""]) {
                
                //                DebugLog(@"IFfffffffff>>>>>>>>>>");
                //                DebugLog(@"LOCATION == %@   Name == %@    Surname == %@",location,name.text,surname.text);
                mapPin.frame = CGRectMake(0, 0, 0, 0);
                
            }else{
                //                DebugLog(@"ELSE<<<<<<<<<<<<<<<<<<<<");
                //                DebugLog(@"LOCATION %@    Name == %@    Surname == %@",location,name.text,surname.text);
                
                mapPin.frame = CGRectMake(20, 29, (18.0f/2.0f), 11);//(25.0f/2.0f));
            }
            
            locationlb.frame=CGRectMake(31, 25, 300, 20);
            //        name.frame= CGRectMake(20, 0, 240, 50);
            //            typeimg.frame= CGRectMake(262, 12, 36, 25.5f);
            selectedrowbg.frame= CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-10, 0);
            // divider.frame= CGRectMake(0, 49, [UIScreen mainScreen].bounds.size.width, 1);
            divider.frame= CGRectMake(0, 49, [UIScreen mainScreen].bounds.size.width,0.5);       //16/09/15
            
        }
        
        cell.tag = indexPath.section*1000 +indexPath.row;
        
        UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longTap:)];
        [cell addGestureRecognizer:longPressGesture];
        
        UISwipeGestureRecognizer *swipeLeftRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGesture:)];
        [swipeLeftRight setDirection:UISwipeGestureRecognizerDirectionRight];
        [cell addGestureRecognizer:swipeLeftRight];
    }
    
    /////////////////////////////////////////////////         Filter = 1          ///////////////////////////////////////////////////////
    else
    {
        
        DebugLog(@"SEARCH CONTACT:%@",filtered_arr);
        
        
        CGSize maximumSize = CGSizeMake(215, 50);
        NSString *firstname = [[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"fullname"];
        
        NSArray *myWords = [firstname componentsSeparatedByString:@" "];
        
        NSString *fname= [[myWords objectAtIndex:0] capitalizedString];
        NSString *lastname;
        UIFont *myFont = [UIFont fontWithName:@"ProximaNova-Bold" size:17];
        CGSize myStringSize = [fname sizeWithFont:myFont
                               
                                constrainedToSize:maximumSize
                               
                                    lineBreakMode:NSLineBreakByWordWrapping];
        name = [[UILabel alloc]init];
        if([[[filtered_arr objectAtIndex:indexPath.row]objectForKey:@"id"] intValue] != 0)
            name.frame = CGRectMake(20, 0, myStringSize.width, 30);
        else
            name.frame = CGRectMake(20, 0, myStringSize.width, 50);
        [cell.contentView addSubview:name];
        name.backgroundColor=[UIColor clearColor];
        name.text=fname;
        name.font=[UIFont fontWithName:@"ProximaNova-Bold" size:17];
        name.textAlignment=NSTextAlignmentLeft;
        
        surname = [[UILabel alloc]init];
        if([[[filtered_arr objectAtIndex:indexPath.row]objectForKey:@"id"] intValue] != 0)
            surname.frame= CGRectMake(name.frame.origin.x+name.frame.size.width+4, 0, 270-name.frame.origin.x-name.frame.size.width, 30);
        else
            surname.frame= CGRectMake(name.frame.origin.x+name.frame.size.width+4, 0, 270-name.frame.origin.x-name.frame.size.width, 50);
        
        [cell.contentView addSubview:surname];
        surname.backgroundColor=[UIColor clearColor];
        for (int k=1;k< [myWords count];k++)
        {
            if (k==1)
            {
                if ([[myWords objectAtIndex:k] isKindOfClass:[NSNull class]] || [myWords objectAtIndex:k] == (id)[NSNull null] ||[[myWords objectAtIndex:k] length] ==0 || [[myWords objectAtIndex:k] isEqualToString:@"(null)"])
                {
                    lastname = @"";
                }
                else
                    lastname= [[NSString stringWithFormat:@"%@",[myWords objectAtIndex:k]] capitalizedString];
                
            }
            else
                lastname= [[NSString stringWithFormat:@"%@ %@",lastname,[myWords objectAtIndex:k]] capitalizedString];
        }
        surname.text=lastname;
        surname.font=[UIFont fontWithName:@"ProximaNova-Light" size:17];
        surname.textAlignment=NSTextAlignmentLeft;
        
        NSString *location;
        
        if([[[filtered_arr objectAtIndex:indexPath.row]objectForKey:@"id"] intValue] != 0)
        {
            int type1 = [[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"business"] intValue];
            
            if (type1==1) {
                
                if ([[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"b_city"] isKindOfClass:[NSNull class]] || [[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"b_city"] == (id)[NSNull null] || [[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"b_city"] length] ==0)
                {
                    if ([[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"b_country"] isKindOfClass:[NSNull class]] || [[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"b_country"] == (id)[NSNull null] || [[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"b_country"] length] ==0)
                        location=@"";
                    else
                        location= [NSString stringWithFormat:@"%@",[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"b_country"]];
                }
                else
                {
                    if ([[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"b_country"] isKindOfClass:[NSNull class]] || [[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"b_country"] == (id)[NSNull null] || [[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"b_country"] length] ==0)
                        location= [NSString stringWithFormat:@"%@",[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"b_city"]];
                    else
                        location= [NSString stringWithFormat:@"%@, %@",[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"b_city"],[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"b_country"]];
                }
            }
            else
            {
                
                if ([[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"city"] isKindOfClass:[NSNull class]] || [[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"city"] == (id)[NSNull null] || [[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"city"] length] ==0)
                {
                    if ([[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"country"] isKindOfClass:[NSNull class]] || [[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"country"] == (id)[NSNull null] || [[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"country"] length] ==0)
                        location=@"";
                    else
                        location= [NSString stringWithFormat:@"%@",[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"country"]];
                }
                else
                {
                    if ([[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"country"] isKindOfClass:[NSNull class]] || [[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"country"] == (id)[NSNull null] || [[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"country"] length] ==0)
                        location= [NSString stringWithFormat:@"%@",[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"city"]];
                    else
                        location= [NSString stringWithFormat:@"%@, %@",[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"city"],[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"country"]];
                }
                
            }
            
            
            //=========================================MAP pin check=====================//
            
            if ([location isKindOfClass:[NSNull class]] || location == (id)[NSNull null] || [location length] == 0 || [location isEqualToString:@""]) {
                
                
                //                mapPin = [[UIImageView alloc]init];
                mapPin.frame = CGRectMake(0, 0, 0, 0);
                
            }else{
                
                mapPin = [[UIImageView alloc]init];
                mapPin.frame = CGRectMake(20, 29, (18.0f/2.0f),11);// (25.0f/2.0f));
                mapPin.image = [UIImage imageNamed:@"Map Marker Contacts Screen copy2x.png"];
                mapPin.contentMode=UIViewContentModeScaleAspectFit;
                mapPin.backgroundColor = [UIColor clearColor];
                [cell.contentView addSubview:mapPin];
                
                
            }
            
            locationlb = [[UILabel alloc]initWithFrame:CGRectMake(31, 25, 210, 20)];
            [cell.contentView addSubview:locationlb];
            locationlb.backgroundColor = [UIColor clearColor];
            locationlb.text = location;
            locationlb.textColor = [UIColor colorWithRed:153.0f/255.0f green:153.0f/255.0f blue:153.0f/255.0f alpha:1];
            locationlb.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
            locationlb.textAlignment = NSTextAlignmentLeft;
        }
        
        
        
        UIImageView *prof_img = [[UIImageView alloc]init];
        //        [cell.contentView addSubview:prof_img];
        prof_img.frame= CGRectMake(10, 5, 40, 40);
        NSString *base64String= [[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"thumb"];
        if ([base64String length] >6)
        {
            NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
            //    NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
            UIImage *profilepic = [UIImage imageWithData:decodedData];
            prof_img.image=profilepic;
            prof_img.contentMode= UIViewContentModeScaleAspectFill;
            prof_img.clipsToBounds=YES;
        }
        divider = [[UIImageView alloc]initWithFrame:CGRectMake(0, 49, [UIScreen mainScreen].bounds.size.width, 1)];
        [cell.contentView addSubview:divider];
        divider.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f];
        //        divider.image= [UIImage imageNamed:@"divider_contact.png"];
        
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *firstVisibleIndexPath = [[tab_contact indexPathsForVisibleRows] objectAtIndex:0];
    DebugLog(@"first visible cell's section: %li, row: %li", (long)firstVisibleIndexPath.section, (long)firstVisibleIndexPath.row);
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    del.scrollsection = firstVisibleIndexPath.section;
    del.scrollrow = firstVisibleIndexPath.row;
    
    [[NSUserDefaults standardUserDefaults]setInteger:del.scrollsection forKey:@"savedSection"];
    [[NSUserDefaults standardUserDefaults]setInteger:indexPath.row forKey:@"savedrow"];
    
    [[tableView cellForRowAtIndexPath:indexPath] setBackgroundColor:[UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f]];
    [searchBar resignFirstResponder];
    
    DebugLog(@"offset now: %f == %f", tab_contact.contentOffset.x, tab_contact.contentOffset.y);
    
    del.scrolloffsety = tab_contact.contentOffset.y;
    
    if (filter==0)
    {
        id object = [[indexedSections objectForKey:[NSNumber numberWithInteger:indexPath.section]] objectAtIndex:indexPath.row];
        NSString *userid = [object objectForKey:@"id"];
        
        if ([userid isKindOfClass:[NSNull class]] || userid == (id)[NSNull null] || [userid intValue] == 0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                //ConDeviceProfileViewController *modelview = [[ConDeviceProfileViewController alloc]init];
                //                [self.navigationController pushViewController:modelview animated:NO];
                ConNewDeviceViewController *modelview = [[ConNewDeviceViewController alloc]init];
                modelview.ContactDetails_Dict = object;
                
                
                //                CATransition* transition = [CATransition animation];
                //
                //                transition.duration = 0.4;
                //                transition.type = kCATransitionPush;
                //                transition.subtype = kCATransitionFade;
                //
                //                [[self navigationController].view.layer addAnimation:transition forKey:nil];
                
                [self.navigationController pushViewController:modelview animated:YES];
            });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                scrollPosition=tableView.contentOffset.y;
                
                //                ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
                ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
                con.Condetails_dict = object;
                con.other=@"yes";
                [self.tabBarController.tabBar setHidden:YES];
                con.uid=[NSString stringWithFormat:@"%d",[userid intValue]];
                //                [self.navigationController presentViewController:con animated:NO completion:nil];
                
                //                CATransition* transition = [CATransition animation];
                //
                //                transition.duration = 0.4;
                //                transition.type = kCATransitionPush;
                //                transition.subtype = kCATransitionFade;
                //
                //                [[self navigationController].view.layer addAnimation:transition forKey:nil];
                
                [self.navigationController pushViewController:con animated:YES];
            });
        }
    }
    else
    {
        NSDictionary *details_Dict = [filtered_arr objectAtIndex:indexPath.row];
        
        NSString *userid = [[filtered_arr objectAtIndex:indexPath.row]objectForKey:@"id"];
        
        if ([userid isKindOfClass:[NSNull class]] || userid == (id)[NSNull null] || [userid intValue] == 0)
        {
            scrollPosition=tableView.contentOffset.y;
            //ConDeviceProfileViewController *modelview = [[ConDeviceProfileViewController alloc]init];
            ConNewDeviceViewController *modelview = [[ConNewDeviceViewController alloc]init];
            
            modelview.ContactDetails_Dict = details_Dict;
            [self.tabBarController.tabBar setHidden:YES];
            [searchBar resignFirstResponder];
            
            
            //            CATransition* transition = [CATransition animation];
            //
            //            transition.duration = 0.4;
            //            transition.type = kCATransitionPush;
            //            transition.subtype = kCATransitionFade;
            //
            //            [[self navigationController].view.layer addAnimation:transition forKey:nil];
            
            [self.navigationController pushViewController:modelview animated:YES];
        }
        else
        {
            scrollPosition=tableView.contentOffset.y;
            //ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
            ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
            con.Condetails_dict = details_Dict;
            con.other=@"yes";
            
            con.uid=[NSString stringWithFormat:@"%d",[userid intValue]];
            [searchBar resignFirstResponder];
            
            
            //            CATransition* transition = [CATransition animation];
            //
            //            transition.duration = 0.4;
            //            transition.type = kCATransitionPush;
            //            transition.subtype = kCATransitionFade;
            //
            //            [[self navigationController].view.layer addAnimation:transition forKey:nil];
            
            [self.navigationController pushViewController:con animated:YES];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (filter==0)
    {
        BOOL isSelected = [self.selectedIndexPaths containsObject:indexPath];
        
        //        CGFloat maxHeight = 100.0f;
        CGFloat minHeight = 50.0f;
        
        CGFloat constrainHeight = isSelected?minHeight:minHeight;  //isSelected?maxHeight:
        return constrainHeight;
    }
    else
        return 50;
}


- (void)addOrRemoveSelectedIndexPath:(NSIndexPath *)indexPath
{
    if (!self.selectedIndexPaths) {
        self.selectedIndexPaths = [NSMutableArray new];
    }
    BOOL containsIndexPath = [self.selectedIndexPaths containsObject:indexPath];
    
    if (containsIndexPath) {
        [self.selectedIndexPaths removeObject:indexPath];
        if (typeofgesture ==1)
            [tab_contact reloadRowsAtIndexPaths:@[indexPath]
                               withRowAnimation:UITableViewRowAnimationFade];
        else
            [tab_contact reloadRowsAtIndexPaths:@[indexPath]
                               withRowAnimation:UITableViewRowAnimationLeft];
    }else{
        self.selectedIndexPaths = [[NSMutableArray alloc]init];
        [tab_contact reloadData];
        
        [self.selectedIndexPaths addObject:indexPath];
        if (typeofgesture ==1)
            [tab_contact reloadRowsAtIndexPaths:@[indexPath]
                               withRowAnimation:UITableViewRowAnimationFade];
        else
            [tab_contact reloadRowsAtIndexPaths:@[indexPath]
                               withRowAnimation:UITableViewRowAnimationRight];
    }
}

+(void)chngpostion
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Data" object:Nil];
}

-(void)viewWillAppear:(BOOL)animated{
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"firstContacts"] isEqualToString:@"no"])
    {
        AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [del showTabStatus:YES];
//        [del showTabValues:YES];
        [del.tabBarController.tabBar setFrame:CGRectMake(0,del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getData:) name:@"Data" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(navtopage:) name:@"pagenav" object:nil];
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(contactload:) name:@"contactload" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(memoryfunction) name:@"applicationDidReceiveMemoryWarning" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestreceivedaction) name:@"Requestreceived_push" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(acceptedrequestaction) name:@"Accepted_push" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshContactsTable) name:@"RefreshContacts" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AcceptedRequest) name:@"Accepted_request" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AddFriend) name:@"NewConnection" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(GroupRequest) name:@"RequestFromGroup" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AcceptedRequest) name:@"TypeChange" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ConnectionType) name:@"NewConnectionType" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shareLocation) name:@"ShareLocation" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdateInformation) name:@"UpdateInfo" object:nil];
    
}

-(void)UpdateInformation
{
    ConPersonalProfileViewController *PVC = [[ConPersonalProfileViewController alloc]init];
    PVC.toUpdateInfo = @"YES";
    [self.navigationController pushViewController:PVC animated:YES];
   // [[NSNotificationCenter defaultCenter] postNotificationName:@"Update Info" object:Nil];
    
}


-(void)shareLocation
{
    ConLocateGroupViewController *conLocate = [[ConLocateGroupViewController alloc]init];
    conLocate.group_id=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"sharedLocGrpId"]];
    [self.navigationController pushViewController:conLocate animated:YES];
}

-(void)ConnectionType
{
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDel.newrequestRedirect)
    {
        ConNewRequestsViewController *mng = [[ConNewRequestsViewController alloc]init];
        mng.userid = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userforrequest"] intValue];
        //    CATransition* transition = [CATransition animation];
        //
        //    transition.duration = 0.4;
        //    transition.type = kCATransitionPush;
        //    transition.subtype = kCATransitionFade;
        
        appDel.newrequestRedirect = NO;
        //   [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:mng animated:YES];
        
    }
}

-(void)AcceptedRequestChange
{
    ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
    // con.other=@"yes";
    con.request = YES;
    
    [self.tabBarController.tabBar setHidden:YES];
    // con.uid=[NSString stringWithFormat:@"%d",[userid intValue]];
    //                [self.navigationController presentViewController:con animated:NO completion:nil];
    
    //    CATransition* transition = [CATransition animation];
    //
    //    transition.duration = 0.4;
    //    transition.type = kCATransitionPush;
    //    transition.subtype = kCATransitionFade;
    //
    //    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}



-(void)GroupRequest
{
    ConRequestViewController *grpdtls=[[ConRequestViewController alloc]init];
    //                CATransition* transition = [CATransition animation];
    //
    //                transition.duration = 0.4;
    //                transition.type = kCATransitionPush;
    //                transition.subtype = kCATransitionFade;
    //                transition.subtype = kCATransitionFromRight;
    //
    //                [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:grpdtls animated:NO];
}

-(void)AddFriend
{
    appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
//    alert = [[UIAlertView alloc] initWithTitle:@"" message:@"IN Contact" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    [alert show];
    
//    if (appDel.newUser)
//    {
    
        ConAddFriendViewController *con = [[ConAddFriendViewController alloc]init];
        
        //    CATransition* transition = [CATransition animation];
        //
        //    transition.duration = 0.4;
        //    transition.type = kCATransitionPush;
        //    transition.subtype = kCATransitionFade;
        
        appDel.newUser = NO;
        
        //    [[self navigationController].view.layer addAnimation:transition forKey:nil];
        [[self navigationController] pushViewController:con animated:YES];
        
   // }
    
}
-(void)AcceptedRequest
{
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (del.profileRedirect)
    {
        ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
        // con.other=@"yes";
        con.request = YES;
        con.other = @"yes";
        con.uid = [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"other_user"]];
        DebugLog(@"USER ID:%@",con.uid);
        [self.tabBarController.tabBar setHidden:YES];
        // con.uid=[NSString stringWithFormat:@"%d",[userid intValue]];
        //                [self.navigationController presentViewController:con animated:NO completion:nil];
        
        //        CATransition* transition = [CATransition animation];
        //
        //        transition.duration = 0.4;
        //        transition.type = kCATransitionPush;
        //        transition.subtype = kCATransitionFade;
        
        del.profileRedirect = NO;
        
        //        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
        
    }
    else
    {
        
    }
    
}

-(void)refreshContactsTable
{
    [tab_contact removeFromSuperview];
    tab_contact=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, mainview.frame.size.height-46)];
    [mainview addSubview:tab_contact];
    tab_contact.backgroundColor=[UIColor clearColor];
    tab_contact.dataSource=self;
    tab_contact.delegate=self;
    tab_contact.separatorStyle=UITableViewCellSeparatorStyleNone;
    tab_contact.showsVerticalScrollIndicator=NO;
    
    if ([tab_contact respondsToSelector:@selector(setSectionIndexColor:)]) {
        tab_contact.sectionIndexColor = [UIColor colorWithRed:151.0f/255.0f green:26.0f/255.0f blue:26.0f/255.0f alpha:1];
        tab_contact.sectionIndexTrackingBackgroundColor = [UIColor whiteColor];
    }
    [tab_contact reloadData];
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //1
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"data.plist"];
    
    NSMutableArray *data = [[NSMutableArray alloc]init];
    if (loadfromwebagain == 0)
    {
        data= [con_array mutableCopy];
        [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:con_array] forKey:@"Contactslocaldb"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else
    {
        data = [new_con_array mutableCopy];
        [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:new_con_array] forKey:@"Contactslocaldb"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    
    [data writeToFile: path atomically:YES];
    
    DebugLog(@"refresh contacts table bhaswar");
}

-(void)requestreceivedaction
{
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //    ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
    ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
    con.other=@"yes";
    con.uid=[NSString stringWithFormat:@"%@",del.pushtoprofileid];
    
    //    CATransition* transition = [CATransition animation];
    //
    //    transition.duration = 0.4;
    //    transition.type = kCATransitionPush;
    //    transition.subtype = kCATransitionFade;
    //
    //    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}

-(void)acceptedrequestaction
{
    ConRequestViewController *con = [[ConRequestViewController alloc]init];
    
    //    CATransition* transition = [CATransition animation];
    //
    //    transition.duration = 0.4;
    //    transition.type = kCATransitionPush;
    //    transition.subtype = kCATransitionFade;
    //
    //    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    
    [self.navigationController pushViewController:con animated:YES];
}


-(void)navtopage: (NSNotification *)notification
{
    DebugLog(@"navtopage");
    NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
    if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"4"])
    {
        ConAccountSettingsViewController *con = [[ConAccountSettingsViewController alloc]init];
        
        //        CATransition* transition = [CATransition animation];
        //
        //        transition.duration = 0.4;
        //        transition.type = kCATransitionPush;
        //        transition.subtype = kCATransitionFade;
        //
        //        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"6"])
    {
        ConInviteViewController *con = [[ConInviteViewController alloc]init];
        
        //        CATransition* transition = [CATransition animation];
        //
        //        transition.duration = 0.4;
        //        transition.type = kCATransitionPush;
        //        transition.subtype = kCATransitionFade;
        //
        //        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"5"])
    {
        ConSyncLoaderViewController *con = [[ConSyncLoaderViewController alloc]init];
        //        [self.navigationController pushViewController:con animated:NO];
        
        [self presentViewController:con animated:NO completion:nil];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"2"])
    {
        //        ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
        ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
        
        //        CATransition* transition = [CATransition animation];
        //
        //        transition.duration = 0.4;
        //        transition.type = kCATransitionPush;
        //        transition.subtype = kCATransitionFade;
        //
        //        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"3"])
    {
        ConQROptionsViewController *con = [[ConQROptionsViewController alloc]init];
        
        //        CATransition* transition = [CATransition animation];
        //
        //        transition.duration = 0.4;
        //        transition.type = kCATransitionPush;
        //        transition.subtype = kCATransitionFade;
        //
        //        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    
    
    //    [prefs setObject:@"111" forKey:@"whichpage"];
}


-(void)getData:(NSNotification *)notification {
    
    if(move == 0) {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-40, (mainview.frame.origin.y), mainview.frame.size.width, mainview.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             move=1;
                         }];
        
    } else {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake(0, (mainview.frame.origin.y), mainview.frame.size.width, mainview.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             move=0;
                         }];
    }
}

-(void)viewDidDisappear:(BOOL)animated
{
    addContact.alpha = 1.0f;
    recommended.alpha = 1.0f;
    move=0;
    searchBar.text = @"";
    searchField.text = @"";
    [self.view endEditing:YES];
    //    [searchBar removeFromSuperview];
    //    [searchField removeFromSuperview];
    [searchField resignFirstResponder];
    [searchBar resignFirstResponder];
//    tab_contact = Nil;
//    [mainview removeFromSuperview];
    con_array = nil;
    new_con_array = nil;
    //    ns = nil;
    self.navigationController.delegate = nil;
    
    //if you have any table views these would also need to be set to nil
//    tab_contact.delegate = nil;
//    tab_contact.dataSource = nil;
    self.selectedIndexPaths = [NSMutableArray new];
    [super viewDidDisappear:YES];
    free((__bridge void *)(con_array));
}

-(void)phonefun: (UIButton *)sender
{
    
    phonesarr=[[NSMutableArray alloc]init];
    NSString *mobile_str,*business_str, *landphnstr; //, *home_str,*main_str, *other_str, *work_str;
    //   [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:2135554321"]];
    NSInteger section = (sender.tag)/1000;
    NSInteger row = (sender.tag)%1000;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
    
    id object = [[indexedSections objectForKey:[NSNumber numberWithInteger:indexPath.section]] objectAtIndex:indexPath.row];
    
    if ([[object objectForKey:@"id"] intValue]> 0)
    {
        mobile_str = [NSString stringWithFormat:@"%@%@",[object objectForKey:@"mobile_pre"],[object objectForKey:@"mobile_num"]];
        
        business_str = [NSString stringWithFormat:@"%@%@",[object objectForKey:@"b_phone_pre"],[object objectForKey:@"b_phone_num"]];
        
        landphnstr = [NSString stringWithFormat:@"%@%@",[object objectForKey:@"phone_pre"],[object objectForKey:@"phone_num"]];
        
        if ([mobile_str length] > 3)
        {
            [phonesarr addObject:mobile_str];
            UIDevice *device = [UIDevice currentDevice];
            if ([[device model] isEqualToString:@"iPhone"] ) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString: [NSString stringWithFormat:@"tel:%@",mobile_str]]];
            } else {
                UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [notPermitted show];
            }
            
        }
        
        else if ([landphnstr length] > 3)
        {
            [phonesarr addObject:landphnstr];
            UIDevice *device = [UIDevice currentDevice];
            if ([[device model] isEqualToString:@"iPhone"] ) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString: [NSString stringWithFormat:@"tel:%@",landphnstr]]];
            } else {
                UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [notPermitted show];
            }
        }
        
        else if ([business_str length] > 3)
        {
            [phonesarr addObject:business_str];
            
            UIDevice *device = [UIDevice currentDevice];
            if ([[device model] isEqualToString:@"iPhone"] ) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString: [NSString stringWithFormat:@"tel:%@",business_str]]];
            } else {
                UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [notPermitted show];
            }
        }
        else
        {
            alert = [[UIAlertView alloc] initWithTitle:@"No Number found for this user!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
        }
        
    }
    else
    {
        NSArray *arrr;
        
        DebugLog(@"Device Contact:%@",object);
        
        // NSString *devicenos = [object objectForKey:@"mobile_num"];
        
        mobile_str = [NSString stringWithFormat:@"%@",[object objectForKey:@"mobile"]];
        
        
        
        business_str = [NSString stringWithFormat:@"%@",[object objectForKey:@"work"]];
        
        
        
        landphnstr = [NSString stringWithFormat:@"%@",[object objectForKey:@"home"]];
        
        
        
        if (![mobile_str isKindOfClass:[NSNull class]] && ![mobile_str isEqualToString:@""] && ![mobile_str isEqualToString:@"(null)"] && mobile_str!=nil)
            
        {
            
            arrr = [mobile_str componentsSeparatedByString:@","];
            
        }
        
        else if (![business_str isKindOfClass:[NSNull class]] && ![business_str isEqualToString:@""] && ![business_str isEqualToString:@"(null)"] && business_str!=nil)
            
        {
            
            arrr = [business_str componentsSeparatedByString:@","];
            
        }
        
        else
            
        {
            
            arrr = [landphnstr componentsSeparatedByString:@","];
            
        }
        phonesarr = [arrr mutableCopy];
        
        if ([phonesarr count] == 0)
        {
            alert = [[UIAlertView alloc] initWithTitle:@"No Number found for this user!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
        }
        else
        {
            UIDevice *device = [UIDevice currentDevice];
            if ([[device model] isEqualToString:@"iPhone"] ) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString: [NSString stringWithFormat:@"tel:%@",[phonesarr objectAtIndex:0]]]];
            } else {
                UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [notPermitted show];
            }
        }
    }
}

-(void)chatfun: (UIButton *)sender
{
    NSString *mobile_str,*business_str, *landphnstr;//, *home_str,*main_str, *other_str, *work_str;
    phonesarr=[[NSMutableArray alloc]init];
    NSInteger section = (sender.tag)/1000;
    NSInteger row = (sender.tag)%1000;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
    id object = [[indexedSections objectForKey:[NSNumber numberWithInteger:indexPath.section]] objectAtIndex:indexPath.row];
    
    if ([[object objectForKey:@"id"] intValue]> 0)
    {
        mobile_str = [NSString stringWithFormat:@"%@%@",[object objectForKey:@"mobile_pre"],[object objectForKey:@"mobile_num"]];
        business_str = [NSString stringWithFormat:@"%@%@",[object objectForKey:@"b_phone_pre"],[object objectForKey:@"b_phone_num"]];
        landphnstr = [NSString stringWithFormat:@"%@%@",[object objectForKey:@"phone_pre"],[object objectForKey:@"phone_num"]];
        
        if ([mobile_str length] > 3)
            [phonesarr addObject:mobile_str];
        else if ([landphnstr length] > 3)
            [phonesarr addObject:landphnstr];
        else if ([business_str length] > 3)
            [phonesarr addObject:business_str];
    }
    else
    {
        NSArray *arrr;
        
        DebugLog(@"Device Contact:%@",object);
        
        // NSString *devicenos = [object objectForKey:@"mobile_num"];
        
        mobile_str = [NSString stringWithFormat:@"%@",[object objectForKey:@"mobile"]];
        
        
        
        business_str = [NSString stringWithFormat:@"%@",[object objectForKey:@"work"]];
        
        
        
        landphnstr = [NSString stringWithFormat:@"%@",[object objectForKey:@"home"]];
        
        
        
        if (![mobile_str isKindOfClass:[NSNull class]] && ![mobile_str isEqualToString:@""] && ![mobile_str isEqualToString:@"(null)"] && mobile_str!=nil)
            
        {
            
            arrr = [mobile_str componentsSeparatedByString:@","];
            
        }
        
        else if (![business_str isKindOfClass:[NSNull class]] && ![business_str isEqualToString:@""] && ![business_str isEqualToString:@"(null)"] && business_str!=nil)
            
        {
            
            arrr = [business_str componentsSeparatedByString:@","];
            
        }
        
        else
            
        {
            
            arrr = [landphnstr componentsSeparatedByString:@","];
            
        }
        phonesarr = [arrr mutableCopy];
    }
    if ([phonesarr count] == 0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"No Number found for this user!"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
    }
    else
    {
        UIDevice *device = [UIDevice currentDevice];
        if ([[device model] isEqualToString:@"iPhone"]) {
            
            MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
            
            if([MFMessageComposeViewController canSendText])
            {
                [self presentViewController:controller animated:YES completion:Nil];
                
                controller.body = @"";
                
                NSString *smsstring= [NSString stringWithFormat:@"%@",[phonesarr objectAtIndex:0]];
                controller.recipients = [NSArray arrayWithObjects:smsstring, nil];
                
                controller.messageComposeDelegate = self;
                //[self presentModalViewController:controller animated:YES];
            }
        }
        else
        {
            UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [notPermitted show];
        }
    }
    
}

-(void)messagefun: (UIButton *)sender
{
    phonesarr=[[NSMutableArray alloc]init];
    //    NSString *mobile_str,*business_str, *home_str,*main_str, *other_str;
    //   [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:2135554321"]];
    NSInteger section = (sender.tag)/1000;
    NSInteger row = (sender.tag)%1000;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
    
    id object = [[indexedSections objectForKey:[NSNumber numberWithInteger:indexPath.section]] objectAtIndex:indexPath.row];
    
    if ([[object objectForKey:@"id"] intValue]> 0)
    {
        NSString *app_mail = [object objectForKey:@"email"];
        if ([app_mail isKindOfClass:[NSNull class]] || app_mail == (id)[NSNull null] || [app_mail length] == 0 || [app_mail isEqualToString:@"(null)"])
        {
        }
        else
            [phonesarr addObject:app_mail];
        if ([phonesarr count] == 0)
        {
            alert = [[UIAlertView alloc] initWithTitle:@"No Email found for this user!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
        }
        else
        {
            anActionSheet = [[UIActionSheet alloc] initWithTitle:@"Options" delegate:self
                                               cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil, nil];
            
            for (int i = 0; i < [phonesarr count]; i++)
                [anActionSheet addButtonWithTitle:[phonesarr objectAtIndex:i]];
            
            anActionSheet.cancelButtonIndex = [phonesarr count];
            [anActionSheet addButtonWithTitle:@"Cancel"];
            anActionSheet.actionSheetStyle = UIActionSheetStyleDefault;
            [anActionSheet showInView:mainview];
            anActionSheet.tag=3;
        }
    }
    else
    {
        NSString *emailfromdev= [object objectForKey:@"email"];
        phonesarr= [[emailfromdev componentsSeparatedByString:@","] mutableCopy];
        if ([phonesarr count] == 0)
        {
            alert = [[UIAlertView alloc] initWithTitle:@"No Email found for this user!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
        }
        else
        {
            anActionSheet = [[UIActionSheet alloc] initWithTitle:@"Options" delegate:self
                                               cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil, nil];
            
            for (int i = 0; i < [phonesarr count]; i++)
                [anActionSheet addButtonWithTitle:[phonesarr objectAtIndex:i]];
            
            anActionSheet.cancelButtonIndex = [phonesarr count];
            [anActionSheet addButtonWithTitle:@"Cancel"];
            anActionSheet.actionSheetStyle = UIActionSheetStyleDefault;
            [anActionSheet showInView:mainview];
            anActionSheet.tag=3;
        }
    }
}

-(void) longTap:(UILongPressGestureRecognizer *)sender{
    if ([sender state] == UIGestureRecognizerStateBegan) {
        //int x= (int)sender.view.tag;
        typeofgesture = 1;
        
        NSInteger section = (sender.view.tag)/1000;
        NSInteger row = (sender.view.tag)%1000;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
        [self addOrRemoveSelectedIndexPath:indexPath];
    }
}

-(void) swipeGesture:(UISwipeGestureRecognizer *)sender{
    //int x= (int)sender.view.tag;
    typeofgesture = 2;
    
    NSInteger section = (sender.view.tag)/1000;
    NSInteger row = (sender.view.tag)%1000;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
    [self addOrRemoveSelectedIndexPath:indexPath];
}

-(void) swipeGestureLeft:(UISwipeGestureRecognizer *)sender{
    //int x= (int)sender.view.tag;
    typeofgesture = 2;
    
    NSInteger section = (sender.view.tag)/1000;
    NSInteger row = (sender.view.tag)%1000;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
    if ([self.selectedIndexPaths containsObject:indexPath])
        [self addOrRemoveSelectedIndexPath:indexPath];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag==1)
    {
        if (buttonIndex != [phonesarr count])
        {
            UIDevice *device = [UIDevice currentDevice];
            if ([[device model] isEqualToString:@"iPhone"] ) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString: [NSString stringWithFormat:@"tel:%@",[phonesarr objectAtIndex:buttonIndex]]]];
            } else {
                UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [notPermitted show];
            }
        }
    }
    if (actionSheet.tag==2)
    {
        if (buttonIndex != [phonesarr count])
        {
            UIDevice *device = [UIDevice currentDevice];
            if ([[device model] isEqualToString:@"iPhone"]) {
                
                MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
                
                if([MFMessageComposeViewController canSendText])
                {
                    [self presentViewController:controller animated:YES completion:Nil];
                    
                    controller.body = @"";
                    
                    NSString *smsstring= [NSString stringWithFormat:@"%@",[phonesarr objectAtIndex:buttonIndex]];
                    controller.recipients = [NSArray arrayWithObjects:smsstring, nil];
                    
                    controller.messageComposeDelegate = self;
                    
                    //[self presentModalViewController:controller animated:YES];
                }
            }
            else
            {
                UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [notPermitted show];
            }
            
        }
    }
    
    if (actionSheet.tag==3)
    {
        if (buttonIndex != [phonesarr count])
        {
            if ([MFMailComposeViewController canSendMail]==YES)
            {
                MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
                controller.mailComposeDelegate = self;
                if (self.title)
                    [controller setSubject:@""];
                
                NSMutableString *emailBody = [[NSMutableString alloc] initWithString:@"<html><body>"];
                
                //            [emailBody appendString:[NSString stringWithFormat:@"<p>via PXC </p>"]];
                //
                //            [emailBody appendString:@"</body></html>"];
                NSArray *toRecipients = [NSArray arrayWithObjects:[phonesarr objectAtIndex:buttonIndex],nil];
                [controller setToRecipients:toRecipients];
                
                [controller setMessageBody:emailBody isHTML:YES];
                
                [self.navigationController presentViewController:controller animated:YES completion:nil];
#if !__has_feature(objc_arc)
                [controller release];
                [emailBody release];
#endif
            }
            
            else
            {
                NSString *deviceType        = [UIDevice currentDevice].model;
                alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                   message:[NSString stringWithFormat:@"Your %@ must have an email account set up",deviceType ]
                                                  delegate:nil
                                         cancelButtonTitle:@"Ok"
                                         otherButtonTitles:nil];
                [alert show];
#if !__has_feature(objc_arc)
                [alert release];
#endif
            }
        }
    }
    
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    if (result == MessageComposeResultCancelled) {
        
    } else if (result == MessageComposeResultSent) {
        
    }
    
}


- (BOOL)  ShouldBeginEditing:(UISearchBar *)searchBarlocal
{
    searchBarlocal.showsCancelButton=YES;
    
    searchBar.frame = CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width, 51);
    
    [addContact setHidden:YES];    //    searchview.hidden=NO;
    return YES;
}


-(void)press_selectedbg:(UIGestureRecognizer *)sender
{
    if ([sender state] == UIGestureRecognizerStateBegan) {
        //        int x= (int)sender.view.tag;
        NSInteger section = (sender.view.tag)/1000;
        NSInteger row = (sender.view.tag)%1000;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
        [self addOrRemoveSelectedIndexPath:indexPath];
    }
}



//////////////////////////////    After loading data from database,fire url & update database    //////////////////////////////////////////
-(void) loadContactsFromWeb
{
    if (self.view.window)
    {
        
        NSOperationQueue* operationQueue = [[NSOperationQueue alloc] init];
        [operationQueue addOperationWithBlock:^{
            // Perform long-running tasks without blocking main thread
            
            AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            
            
        }];
        
        DebugLog(@"came here came here");
        loadfromwebagain =1;
        new_con_array = [appdelwebarray mutableCopy];
        [self performSelectorInBackground:@selector(startDBInsert)
                               withObject:nil];
        
        //                if (loadcheck == 1)
        [self loaddatafromlocaldb];
//        [tab_contact reloadData];
    }
}


-(void) loaddatafromlocaldb
{
}

- (void)generateRoute {
    
    CLLocationCoordinate2D end = {latitude, longitude};
    MKMapItem *destination_mapitem = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:end addressDictionary:nil]];
    
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
    request.source = [MKMapItem mapItemForCurrentLocation];
    request.destination = destination_mapitem;
    [request setTransportType:MKDirectionsTransportTypeAny]; // This can be limited to automobile and walking directions.
    [request setRequestsAlternateRoutes:YES];
    MKDirections *directions = [[MKDirections alloc] initWithRequest:request];
    
    [directions calculateDirectionsWithCompletionHandler:
     ^(MKDirectionsResponse *response, NSError *error) {
         if (error) {
             // Handle Error
             //             [[[[iToast makeText:[NSString stringWithFormat:@"error generate route %@",error]]
             //                setGravity:iToastGravityCenter] setDuration:iToastDurationLong] show];
             
         } else {
             [self showRoute:response];
         }
     }];
}


- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {
    CLLocation *location = map_View.userLocation.location;
    DebugLog(@"lat current: %f - long current: %f", location.coordinate.latitude, location.coordinate.longitude);
}

-(void)showRoute:(MKDirectionsResponse *)response
{
    for (MKRoute *route in response.routes)
    {
        [map_View addOverlay:route.polyline level:MKOverlayLevelAboveLabels];
        for (MKRouteStep *step in route.steps)
        {
            DebugLog(@" here %@", step.instructions);
        }
    }
}


- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)aUserLocation {
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.005;
    span.longitudeDelta = 0.005;
    CLLocationCoordinate2D location;
    location.latitude =  latitude;       //aUserLocation.coordinate.latitude;
    location.longitude = longitude;      //aUserLocation.coordinate.longitude;
    region.span = span;
    region.center = location;
    
    userlat= aUserLocation.location.coordinate.latitude;
    userlong= aUserLocation.location.coordinate.longitude;
    
    [self generateRoute];
}

//==============Map Button===============//
-(void)mapFunc:(UIButton *)sender{
    
    NSInteger section = (sender.tag)/1000;
    NSInteger row = (sender.tag)%1000;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
    
    NSString *sectionTitle = [dictkeysarr objectAtIndex:indexPath.section];
    NSArray *sectionContactsArray = [add_contacts_dict objectForKey:sectionTitle];
    
    latitude = [[[sectionContactsArray objectAtIndex:indexPath.row]objectForKey:@"lat"] floatValue];
    longitude = [[[sectionContactsArray objectAtIndex:indexPath.row]objectForKey:@"lng"] floatValue];
    
    CLLocationCoordinate2D endingCoord = CLLocationCoordinate2DMake(latitude, longitude);
    MKPlacemark *endLocation = [[MKPlacemark alloc] initWithCoordinate:endingCoord addressDictionary:nil];
    MKMapItem *endingItem = [[MKMapItem alloc] initWithPlacemark:endLocation];
    
    NSMutableDictionary *launchOptions = [[NSMutableDictionary alloc] init];
    [launchOptions setObject:MKLaunchOptionsDirectionsModeDriving forKey:MKLaunchOptionsDirectionsModeKey];
    [endingItem openInMapsWithLaunchOptions:launchOptions];
}

-(void)startDBInsert
{
    DebugLog(@"start contacts db insert");
    //    if (contacts_insertion == 0)
    //    {
    //        contacts_insertion=1;
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (loadfromwebagain == 0)
    {
        del.writetodbcount = [con_array count];
        [del loadContactsIntoDB:con_array];
    }
    else
    {
        del.writetodbcount = (int)[new_con_array count];
        [del loadContactsIntoDB:new_con_array];
    }
    //    }
    //    [data release];
}

-(void)contactload: (NSNotification *)notification
{
    [self loaddatafromlocaldb];
}

+ (NSNumber *)recordIdFromPersonRecord:(ABRecordRef)personRecord
{
    ABRecordID recordId = ABRecordGetRecordID(personRecord);
    return [NSNumber numberWithInt:(int)recordId];
}

-(void)yesSuccess
{
    [act removeFromSuperview];
    
//    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    [del showTabValues:YES];
    
    //    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"reloadinAppDelegate"] isEqualToString:@"yes"])
    //    {
    [self loadContactsFromWeb];
    //    }
    [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"reloadinAppDelegate"];
    DebugLog(@"reload table in contacts page");
}


#pragma Mark: TableView Delegate Methods

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    //       return [[UILocalizedIndexedCollation currentCollation] sectionForSectionIndexTitleAtIndex:index];
    if (!filter)
    {
        [[[iToast makeText:[NSString stringWithFormat:@" %@",title]]
          setGravity:iToastGravityCenter] show];
        
        UILocalizedIndexedCollation *currentCollation = [UILocalizedIndexedCollation currentCollation];
        return [currentCollation sectionForSectionIndexTitleAtIndex:index];
        //   }
    }
    else
        return 0;
}


- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    //   return [[UILocalizedIndexedCollation currentCollation] sectionIndexTitles];
    
    //        return [[NSArray arrayWithObject:UITableViewIndexSearch] arrayByAddingObjectsFromArray:
    if (!filter)
        return [[UILocalizedIndexedCollation currentCollation] sectionIndexTitles];
    else
        return Nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (!filter)
    {
        int total;
        total =1;
        //        DebugLog(@"e4eee %ld",[[[UILocalizedIndexedCollation currentCollation] sectionTitles] count]);
        return [[[UILocalizedIndexedCollation currentCollation] sectionTitles] count]; //[final_con_array count];
    }
    else
        return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!filter)
        return [[indexedSections objectForKey:[NSNumber numberWithInteger:section]] count];
    else
        return [filtered_arr count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    BOOL showSection = [[indexedSections objectForKey:[NSNumber numberWithInteger:section]] count]!=0;
    if (showSection)
        //only show the section title if there are rows in the section
        return  [[[UILocalizedIndexedCollation currentCollation] sectionTitles] objectAtIndex:section];
    else
        return 0;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}

- (MDRadialProgressView *)progressViewWithFrame:(CGRect)frame
{
    MDRadialProgressView *view = [[MDRadialProgressView alloc] initWithFrame:frame];
    // Only required in this demo to align vertically the progress views.
    view.center = CGPointMake(self.view.center.x, view.center.y+40);
    return view;
}

-(void)targetMethodFirstTime: (NSTimer *)timer
{
    radialView3.progressCounter=radialView3.progressCounter+1;
    
    if (radialView3.progressCounter == 96)
    {
        if (checkIfFinished == NO)
        {
            NSLog(@"drrrdddddd");
            [timer invalidate];
        }
    }
    
    if (radialView3.progressCounter == 100)
    {
        [timer invalidate];
        AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [del showTabStatus:YES];
        [del showTabValues:YES];
        [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
        [viewnew removeFromSuperview];
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
        [UIApplication sharedApplication].idleTimerDisabled = NO;
        
        //////////////////////////////////// Added on 11.5.16 ///////////////////////////////////////////////////////////
        
        appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        if ([appDel.signup isEqualToString:@"Signup"])
        {
            appDel.signup = @"";
            
            alert = [[UIAlertView alloc] initWithTitle:@"Welcome to Budnav!" message:@"Your contacts are now secured and accessible from any computer, tablet and smartphone with your login credentials!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            //        UIViewController *vc = [self visibleViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
            //
            //
            //        [vc.view makeToast:@"Weolcome To Budnav" duration:3 position:CSToastPositionTop title:nil];
        }
        
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        
    }
}

-(void)memoryfunction
{
}

- (void)dealloc
{
    self.navigationController.delegate = nil;
    //if you have any table views these would also need to be set to nil
    tab_contact.delegate = nil;
    tab_contact.dataSource = nil;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [searchBar resignFirstResponder];
}

-(NSString*) encodeToPercentEscapeString:(NSString *)string {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                              (CFStringRef) string,
                                                              NULL,
                                                              (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
}


#pragma NSURLConnectionDelegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    responseData = [NSMutableData new];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data
{
    //    [responseData appendData:data];
    responseData = [data mutableCopy];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    DebugLog(@"connection failed");
    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"backupstatus"];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    DebugLog(@"connection finished loading %d", successTag);
    DebugLog(@"response data - %@", [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
    
    NSString *datatostring = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSError *err;
    
    NSDictionary *urgentdict = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&err];
    DebugLog(@"urgent dict gives: %@",urgentdict);
    
    //    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString1]];
    
    if (successTag == 200) {
        
        if (responseData != nil)
        {
            NSMutableDictionary  *json1aa=[urgentdict mutableCopy];
            
            DebugLog(@"this now has json string url: %@",json1aa);
            
            //           if ([json1aa objectForKey:@"success"])
            if([datatostring rangeOfString:@"\"success\":true"].location != NSNotFound)
            {
                connectionsuccess = connectionsuccess + 1;
            }
            DebugLog(@"connectionsuccess: %d ,,,, number of iterations: %d",connectionsuccess,number_of_iterations);
            if (connectionsuccess == number_of_iterations) {
                
                [self getfilterbackup];
            }
        }
        
        else
        {
            DebugLog(@"error in server connection");
        }
        //            [SVProgressHUD showSuccessWithStatus:@"Success!"];
    }
    
    else{
        
        NSString *datatostring = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        
        if (responseData != nil)
        {
            NSDictionary  *json1aa=[NSJSONSerialization JSONObjectWithData:responseData options:0 error:&err];
            
            DebugLog(@"this now has json string url: %@",json1aa);
            
            //            if ([[json1aa objectForKey:@"success"] intValue] ==1)
            if([datatostring rangeOfString:@"\"success\":true"].location != NSNotFound || [datatostring rangeOfString:@"\"error\":\"\""].location != NSNotFound)
            {
                //             [SVProgressHUD showSuccessWithStatus:@"Success!"];
                //                connectiondoublefiler =connectiondoublefiler+1;
                
                
            }
            else
            {
                NSString *err_str = [json1aa objectForKey:@"error"];
                DebugLog(@"error backing up: %@",err_str);
            }
            //            [SVProgressHUD showSuccessWithStatus:@"Success!"];
        }
        else
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            alert.tag=7;
            //        [alert show];
        }
    }
    
}

-(void)goPlus:(id)sender
{
    DebugLog(@"Plus clicked");
    [searchBar resignFirstResponder];
    
    ConAddFriendViewController *con = [[ConAddFriendViewController alloc]init];
    
    //    CATransition* transition = [CATransition animation];
    //
    //    transition.duration = 0.4;
    //    transition.type = kCATransitionPush;
    //    transition.subtype = kCATransitionFade;
    //
    //    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    [[self navigationController] pushViewController:con animated:YES];
    
}

-(void)getfilterbackup
{
    successTag = 100;
    //=========================================================GET FILTER BACKUP===============================================================//
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:@"yes" forKey:@"backgroundContactsRefresh"];
    
    
    
#pragma mark -- this is the Beginning of NSLOCALIZEDINDEXEDCOLLATION SECTION - Bhaswar 16/7/2014
    
    // create a dictionary to store an array of objects for each section
    NSMutableDictionary *tempSections = [NSMutableDictionary dictionary];
    
    // iterate through each dictionaey in the list, and put them into the correct section
    for (NSDictionary *item in con_array)
    {
        // get the index of the section (Assuming the table index is showing A-#)
        NSInteger indexName = [[UILocalizedIndexedCollation currentCollation] sectionForObject:[item valueForKey:@"fullname"] collationStringSelector:@selector(description)];
        
        NSNumber *keyName = [NSNumber numberWithInteger:indexName];
        
        // if an array doesnt exist for the key, create one
        NSMutableArray *arrayName = [tempSections objectForKey:keyName];
        if (arrayName == nil)
        {
            arrayName = [NSMutableArray array];
        }
        
        // add the dictionary to the array (add the actual value as we need this object to sort the array later)
        [arrayName addObject: item];   //[item valueForKey:@"fullname"]];
        
        // put the array with new object in, back into the dictionary for the correct key
        [tempSections setObject:arrayName forKey:keyName];
    }
    
    /* now to do the sorting of each index */
    
    NSMutableDictionary *sortedSections = [NSMutableDictionary dictionary];
    
    // sort each index array (A..Z)
    [tempSections enumerateKeysAndObjectsUsingBlock:^(id key, id array, BOOL *stop)
     {
         // sort the array - again, we need to tell it which selctor to sort each object by
         NSArray *sortedArray = [[UILocalizedIndexedCollation currentCollation] sortedArrayFromArray:array collationStringSelector:@selector(description)];
         NSSortDescriptor *sort =[NSSortDescriptor sortDescriptorWithKey:@"fullname" ascending:YES selector:@selector(caseInsensitiveCompare:)];
         sortedArray=[sortedArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];
         [sortedSections setObject:[NSMutableArray arrayWithArray:sortedArray] forKey:key];
     }];
    
    // set the global sectioned dictionary
    indexedSections = sortedSections;
    //            DebugLog(@"indexedsections: %@",indexedSections);
    
    //                                                            AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //                                del.arr_contacts_ad= [app_contacts mutableCopy];
    //                                del.newarr_contacts_ad = [app_contacts mutableCopy];
    
    NSMutableArray *fresh_ban = [[NSMutableArray alloc]init];
    [fresh_ban addObject:indexedSections];
    
    [[NSUserDefaults standardUserDefaults]setObject:[NSKeyedArchiver archivedDataWithRootObject:fresh_ban] forKey:@"IndexedContactslocaldb"];
    NSData *data_SavedArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"IndexedContactslocaldb"];
    if (data_SavedArray != nil)
    {
        NSArray *oldSavedArray = [NSKeyedUnarchiver unarchiveObjectWithData:data_SavedArray];
        DebugLog(@"CONTACT LOCAL DB DB:%@",oldSavedArray);
    }
    
    DebugLog(@"CONTACT LOCAL indexedsections:%@",indexedSections);
    DebugLog(@"CONTACT LOCAL fresh_ban:%@",fresh_ban);
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:con_array ] forKey:@"Contactslocaldb"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    // DebugLog(@"conarray mutablecopy count: %ld", (unsigned long)[del.arr_contacts_ad count]);
    //                        [del loadContactsIntoDB:con_array];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //1
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"data.plist"];
    
    NSMutableArray *data = [[NSMutableArray alloc]init] ;
    data= [con_array mutableCopy];
    DebugLog(@"bbbbb data: %lu",(unsigned long)[data count]);
    [data writeToFile: path atomically:YES];
    
    
    [tab_contact removeFromSuperview];
    tab_contact=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, mainview.frame.size.height-46)];
    [mainview addSubview:tab_contact];
    tab_contact.backgroundColor=[UIColor clearColor];
    tab_contact.dataSource=self;
    tab_contact.delegate=self;
    tab_contact.separatorStyle=UITableViewCellSeparatorStyleNone;
    tab_contact.showsVerticalScrollIndicator=NO;
    
    if ([tab_contact respondsToSelector:@selector(setSectionIndexColor:)]) {
        tab_contact.sectionIndexColor = [UIColor colorWithRed:151.0f/255.0f green:26.0f/255.0f blue:26.0f/255.0f alpha:1];
        tab_contact.sectionIndexTrackingBackgroundColor = [UIColor whiteColor];
    }
    [tab_contact reloadData];
    
    //
    curtainraiser = [[UIView alloc]initWithFrame:tab_contact.frame];
    curtainraiser.backgroundColor=[UIColor clearColor];
    [mainview addSubview:curtainraiser];
    
    //tab_contact.scrollEnabled=NO;
    [curtainraiser removeFromSuperview];
    tab_contact.scrollEnabled=YES;
    //
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, -1, [UIScreen mainScreen].bounds.size.width, 51)];
    searchBar.delegate=self;
    searchBar.tintColor=[UIColor darkGrayColor];
    searchBar.backgroundColor=[UIColor clearColor];
    //                                searchBar.barTintColor= [UIColor colorWithRed:238.0f/255.0f green:238.0f/255.0f blue:238.0f/255.0f alpha:1];
    
    [searchBar setImage:[UIImage imageNamed:@"searchIcon"]
       forSearchBarIcon:UISearchBarIconSearch
                  state:UIControlStateNormal];
    
    searchBar.barTintColor= [UIColor clearColor];
    searchBar.backgroundImage=[UIImage new];
    //                                [searchBar setSearchBarStyle:UISearchBarStyleMinimal];
    searchBar.keyboardAppearance = UIKeyboardAppearanceDark;
    searchBar.placeholder= @"Search Contacts";
    //[mainview addSubview:searchBar];
    searchBar.hidden=YES;
    
    nouserlb= [[UILabel alloc]initWithFrame:CGRectMake(0, 50, [UIScreen mainScreen].bounds.size.width, 30)];
    [mainview addSubview:nouserlb];
    nouserlb.text=@"No results";
    nouserlb.backgroundColor=[UIColor clearColor];
    nouserlb.textAlignment=NSTextAlignmentCenter;
    nouserlb.textColor=[UIColor colorWithRed:238.0f/255.0f green:238.0f/255.0f blue:238.0f/255.0f alpha:1];
    nouserlb.hidden=YES;
    //                [SVProgressHUD dismiss];
    //                [self performSelectorInBackground:@selector(yesSuccess) withObject:nil];
    
    [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"backgroundContactsRefresh"];
    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"reloadinAppDelegate"];
    
    checkIfFinished = YES;
    
    time_interval = 0.1f;
    
    [timerr invalidate];
    timerr = nil;
    
    [self performSelectorOnMainThread:@selector(objFirstViewController) withObject:nil waitUntilDone:YES];
    
    //   [[NSNotificationCenter defaultCenter] postNotificationName:@"ContactsLoaded" object:Nil];
}

-(void)objFirstViewController//:(NSNotification *)notification {
{
    time_interval = 0.1f;
    timeragain = [NSTimer scheduledTimerWithTimeInterval:0.1f
                                                  target:self
                                                selector:@selector(targetMethodFirstTime:)
                                                userInfo:nil
                                                 repeats:YES];
    
}




- (BOOL)searchBarDidBeginEditing:(UISearchBar *)searchBarlocal
{
    searchBarlocal.showsCancelButton=YES;
    //    searchview.hidden=NO;
    [searchBarlocal becomeFirstResponder];
    //    tab_contact.userInteractionEnabled=NO;
    //    tab_contact.scrollEnabled=NO;
    
    return YES;
}

- (BOOL)searchBarDidEndEditing:(UISearchBar *)searchBarlocal
{
    //    searchview.hidden=YES;
    searchBarlocal.showsCancelButton=YES;
    filter= NO;
    [searchBarlocal resignFirstResponder];
    tab_contact.userInteractionEnabled=YES;
    tab_contact.scrollEnabled=YES;
    
    searchBarlocal.text=@"";
    return YES;
}

- (BOOL)searchBar:(UISearchBar *)searchBarlocal shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return YES;
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBarlocal
{
    searchBarlocal.showsCancelButton = NO;
    [searchBarlocal resignFirstResponder];
    tab_contact.userInteractionEnabled=YES;
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBarlocal
{
    //    searchview.hidden=YES;
    [addContact setHidden:NO];
    searchBar.frame = CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width-55, 51);
    
    [searchBarlocal resignFirstResponder];
    
    txtSearchField.layer.borderColor = [[UIColor clearColor]CGColor];
    
    searchBarlocal.text=@"";
    searchBarlocal.showsCancelButton=NO;
    filter= NO;
    [tab_contact reloadData];
    tab_contact.hidden=NO;
    nouserlb.hidden=YES;
    tab_contact.userInteractionEnabled=YES;
    tab_contact.scrollEnabled=YES;
    
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    filter = NO;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    //if we only try and resignFirstResponder on textField or searchBar,
    //the keyboard will not dissapear (at least not on iPad)!
    filter =NO;
    [self performSelector:@selector(searchBarCancelButtonClicked:) withObject:searchBar afterDelay: 0.05];
    return YES;
}
- (void)searchBar:(UISearchBar *)searchBar2 textDidChange:(NSString *)searchText {
    
    //    searchview.hidden=NO;
    whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    st = [searchText stringByTrimmingCharactersInSet:whitespace];
    //Remove all objects first.
    //    [filtered_arr removeAllObjects];
    if ([st length] == 0)
    {
        filter=NO;
        [searchBar2 performSelector:@selector(resignFirstResponder) withObject:nil afterDelay:0];
    }
    
    if([st length] >= 1) {
        [filtered_arr removeAllObjects];
        filter = YES;
        
        NSPredicate *resultPredicate = [NSPredicate
                                        predicateWithFormat:@"fullname contains[c] %@",searchText];
        //      [NSPredicate predicateWithFormat:@"(first CONTAINS[c] %@) OR (last CONTAINS[c] %@)", keyword, keyword]
        searchresults = [con_array filteredArrayUsingPredicate:resultPredicate];
        
        filtered_arr=[[NSMutableArray alloc] initWithArray:searchresults];
        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"fullname" ascending:YES selector:@selector(caseInsensitiveCompare:)];
        filtered_arr=[[filtered_arr sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]] mutableCopy];
        
        [tab_contact reloadData];
        
        if ([filtered_arr count] ==0)
        {
            [filtered_arr removeAllObjects];
            tab_contact.hidden=YES;
            nouserlb.hidden=NO;
        }
        else
        {
            nouserlb.hidden=YES;
            tab_contact.hidden=NO;
        }
    }
    else {
        //        filter = NO;
        nouserlb.hidden=YES;
        tab_contact.hidden=NO;
        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"fullname" ascending:YES];
        filtered_arr=[[con_array sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]] mutableCopy];
        //        [filtered_arr removeAllObjects];
        [tab_contact reloadData];
    }
}

- (void)reloadAgain
{
    NSMutableArray *now_Bhaswar_array;
    tab_contact.userInteractionEnabled = NO;
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    @autoreleasepool {
        
        [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"backgroundContactsRefresh"];
        connectiondoublefiler =0;
        
        NSError *error = nil;
        NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=contacts&amount=-1&id=%@&access_token=%@&device_id=%@&thumb=true",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
        
        DebugLog(@"eshe gechhe from loadrequests from web contacts profile url: %@",urlString1);
        
        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
        
        if (signeddataURL1 == nil)
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        }
        else
        {
            json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                    options:kNilOptions
                                                      error:&error];
            
            NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
            
            if (![errornumber isEqualToString:@"0"])
            {
                NSString *err_str = [json1 objectForKey:@"error"];
                alert = [[UIAlertView alloc] initWithTitle:@"Error in Retrieving Data!"
                                                   message:err_str
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                //                [alert show];
            }
            else
            {
                app_contacts = [[NSMutableArray alloc]init];
                
                if ([[json1 objectForKey:@"details"] isKindOfClass:[NSNull class]] || [json1 objectForKey:@"details"] == (id)[NSNull null] ||[[json1 objectForKey:@"details"] count] ==0)
                {
                    DebugLog(@"PULL CONTACTS:%@",json1);
                }
                else
                {
                    
                    DebugLog(@"PULL TO REFRESH:%@",[json1 objectForKey:@"details"]);
                    
                    NSDictionary *contacts_temp_dict = [json1 objectForKey:@"details"];
                    newcontactsapp = [[NSMutableArray alloc]init];
                    int con_count1;
                    con_count1 = 0;
                    //                        if (con_count1 > 0)
                    //                        {
                    for (NSDictionary *dictc in [contacts_temp_dict objectForKey:@"contacts"])
                    {
                        [newcontactsapp addObject:dictc];  //[dictc objectForKey:@"fullname"]
                    }
                    
                    //    int finalwrite;
                    //for (finalwrite =0; finalwrite < [newcontactsapp count]; finalwrite++)
                    //                    {
                    //                        int traverselooptocheck;
                    //                        traverselooptocheck=0;
                    //
                    //                        ///////////////// Newly Added //////////////////
                    //                        NSDictionary *finalwritedict = [newcontactsapp objectAtIndex:finalwrite];
                    //
                    //                        NSString *petFirstName = [finalwritedict objectForKey:@"name"];
                    //                        NSString *petLastName = [finalwritedict objectForKey:@"surname"];
                    //                        NSString *petfullname= [finalwritedict objectForKey:@"fullname"];
                    //                        NSString *workphone = @"";
                    //                        NSString *mobilephone = @"";
                    //                        NSString *homephone = @"";
                    //                        NSString *strretp = @"";
                    //                        NSString *cityp= @"";
                    //                        NSString *housenumberp = @"";
                    //                        NSString *countryp = @"";
                    //                        NSString *zipCode = @"";
                    //                        NSString *petEmail = @"";
                    //
                    //
                    //                        // NSString *cityp= @"";
                    //                        // NSString *countryp = @"";
                    //
                    //
                    //                        ////////////////// Base64 Conversion //////////////////////
                    //
                    //
                    //                        NSString *base64String1= [[newcontactsapp objectAtIndex:finalwrite] objectForKey:@"thumb"];
                    //                        if ([base64String1 length] > 6)
                    //                        {
                    //                            decodedData = [[NSData alloc] initWithBase64EncodedString:base64String1 options:0];
                    //                            //decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
                    //                            profilepic = [UIImage imageWithData:decodedData];
                    //
                    //                        }
                    //
                    //
                    //
                    //                        NSData *petImageData = UIImageJPEGRepresentation(profilepic, 0.7f);
                    //                        //NSData *petImageData = UIImageJPEGRepresentation([UIImage imageNamed:@"Background"], 0.7f);
                    //
                    //                        //        NSString *apphme, *appmobl, *appwrk, *devhme, *devmob, *devwrk;
                    //
                    //                        if ([[finalwritedict objectForKey:@"b_phone_num"] length] > 2)
                    //                            workphone = [NSString stringWithFormat:@"%@%@",[finalwritedict objectForKey:@"b_phone_pre"],[finalwritedict objectForKey:@"b_phone_num"]];
                    //                        if ([[finalwritedict objectForKey:@"mobile_num"] length] > 2)
                    //                            mobilephone = [NSString stringWithFormat:@"%@%@",[finalwritedict objectForKey:@"mobile_pre"],[finalwritedict objectForKey:@"mobile_num"]];
                    //                        if ([[finalwritedict objectForKey:@"phone_num"] length] > 2)
                    //                            homephone = [NSString stringWithFormat:@"%@%@",[finalwritedict objectForKey:@"phone_pre"],[finalwritedict objectForKey:@"phone_num"]];
                    //                        if ([[finalwritedict objectForKey:@"city"] length] > 2)
                    //                            cityp = [NSString stringWithFormat:@"%@",[finalwritedict objectForKey:@"city"]];
                    //                        if ([[finalwritedict objectForKey:@"country"] length] > 2)
                    //                            countryp = [NSString stringWithFormat:@"%@",[finalwritedict objectForKey:@"country"]];
                    //
                    //
                    //                        if ([[finalwritedict objectForKey:@"street"]length]>2) {
                    //                            strretp = [finalwritedict objectForKey:@"street"];
                    //
                    //                        }
                    //
                    //                        //                        if ([[finalwritedict objectForKey:@"housenumber"] length]>2) {
                    //                        //                            housenumberp = [finalwritedict objectForKey:@"housenumber"];
                    //                        //
                    //                        //                        }
                    //                        if (![[finalwritedict objectForKey:@"housenumber"] isKindOfClass:[NSNull class]] &&  ![[finalwritedict objectForKey:@"housenumber"] isEqualToString:@"(null)"] && ![[finalwritedict objectForKey:@"housenumber"] isEqualToString:@""] && [[finalwritedict objectForKey:@"housenumber"] length] > 0) {
                    //                            housenumberp = [finalwritedict objectForKey:@"housenumber"];
                    //                            NSLog(@"HOUSENUMBER:%@",housenumberp);
                    //
                    //                            strretp = [NSString stringWithFormat:@"%@,%@",strretp,housenumberp];
                    //                        }
                    //
                    //
                    //                        if ([[finalwritedict objectForKey:@"zipcode"] length]>2) {
                    //                            zipCode = [NSString stringWithFormat:@"%@", [finalwritedict objectForKey:@"zipcode"] ];
                    //
                    //                        }
                    //
                    //                        if ([[finalwritedict objectForKey:@"email"] length]>2) {
                    //                            petEmail = [finalwritedict objectForKey:@"email"];
                    //
                    //                        }
                    //
                    //
                    //
                    //
                    //
                    //                        ABRecordRef pet = ABPersonCreate();
                    //                        //                NSString *firstname=
                    //                        ABRecordSetValue(pet, kABPersonFirstNameProperty, (__bridge CFStringRef)petFirstName, nil);
                    //                        ABRecordSetValue(pet, kABPersonLastNameProperty, (__bridge CFStringRef)petLastName, nil);
                    //
                    //
                    //                        ABMutableMultiValueRef phoneNumbers = ABMultiValueCreateMutable(kABMultiStringPropertyType);
                    //                        ABMultiValueAddValueAndLabel(phoneNumbers, (__bridge CFStringRef)workphone, kABWorkLabel, NULL);
                    //                        ABMultiValueAddValueAndLabel(phoneNumbers, (__bridge CFStringRef)homephone, kABHomeLabel, NULL);
                    //                        ABMultiValueAddValueAndLabel(phoneNumbers, (__bridge CFStringRef)mobilephone, kABPersonPhoneMobileLabel, NULL);
                    //                        ABRecordSetValue(pet, kABPersonPhoneProperty, phoneNumbers, nil);
                    //                        ABPersonSetImageData(pet, (__bridge CFDataRef)petImageData, nil);
                    //
                    //
                    //                        /////////////////////// Email & Address Adding /////////////////
                    //
                    //
                    //                        ABMutableMultiValueRef emails = ABMultiValueCreateMutable(kABMultiStringPropertyType);
                    //                        ABMultiValueAddValueAndLabel(emails,  (__bridge CFStringRef)petEmail, kABWorkLabel, NULL);
                    //
                    //                        ABRecordSetValue(pet, kABPersonEmailProperty, emails, nil);
                    //                        //                    DebugLog(@"ADDRESS PROPERTY:%@",emails);
                    //
                    //                        //CFRelease(emails);
                    //
                    //
                    //                        //////////////////////// Adding Address //////////////////////////////
                    //
                    //                        ABMutableMultiValueRef multiAddress1 = ABMultiValueCreateMutable(kABMultiDictionaryPropertyType);
                    //
                    //                        NSMutableDictionary *addressDictionary2 = [[NSMutableDictionary alloc] init];
                    //                        [addressDictionary2 setObject:strretp forKey:(NSString *) kABPersonAddressStreetKey];
                    //                        [addressDictionary2 setObject:zipCode forKey:(NSString *)kABPersonAddressZIPKey];
                    //
                    //                        [addressDictionary2 setObject:cityp forKey:(NSString *)kABPersonAddressCityKey];
                    //
                    //                        [addressDictionary2 setObject:countryp forKey:(NSString *)kABPersonAddressCountryKey];
                    //                        //[addressDictionary setObject:@"us" forKey:(NSString *)kABPersonAddressCountryCodeKey];
                    //
                    //
                    //                        ABMultiValueAddValueAndLabel(multiAddress1, (__bridge CFTypeRef)(addressDictionary2), kABHomeLabel, NULL);
                    //
                    //
                    //                        //(@"home address added");
                    //
                    //                        ABRecordSetValue(pet, kABPersonAddressProperty, multiAddress1, nil);
                    //
                    //                        NSArray *allADContacts = (__bridge NSArray *)ABAddressBookCopyArrayOfAllPeople(addressBookRef);
                    //                        for (id record in allADContacts){
                    //                            ABRecordRef thisContact = (__bridge ABRecordRef)record;
                    //
                    //                            if (ABRecordCopyCompositeName(thisContact) != NULL)
                    //                            {
                    //                                if (CFStringCompare(ABRecordCopyCompositeName(thisContact),
                    //                                                    (__bridge CFStringRef)(petfullname), 0) == kCFCompareEqualTo){
                    //                                    //                                DebugLog(@"The contact already exists!  %@", (__bridge CFStringRef)(petfullname));
                    //                                    traverselooptocheck =1;
                    //                                }
                    //                                else
                    //                                {
                    //                                }
                    //                            }
                    //                        }
                    //
                    //                        if (traverselooptocheck == 0)
                    //                        {
                    //                            ABAddressBookAddRecord(addressBookRef, pet, nil);
                    //                            ABAddressBookSave(addressBookRef, nil);
                    //                        }
                    //                    }
                    
                    DebugLog(@"new app contacts:%@ %lu",newcontactsapp,(unsigned long)[newcontactsapp count]);
                    DebugLog(@"new app contacts count:%lu",(unsigned long)[newcontactsapp count]);
                }
                
                
                
                NSString *previous_app_contacts_str = [[NSUserDefaults standardUserDefaults]objectForKey:@"app_contacts_count"];
                long previous_app_contacts = [previous_app_contacts_str longLongValue];
                
                
                DebugLog(@"koto elo: %ld == %@ == %@",previous_app_contacts, [[NSUserDefaults standardUserDefaults]objectForKey:@"app_contacts_count"], previous_app_contacts_str);
                
                del.Bhaswar_array = [[NSMutableArray alloc]init];
                NSData *dataRepresentingSavedArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"Contactslocaldb"];
                if (dataRepresentingSavedArray != nil)
                {
                    NSArray *oldSavedArray = [NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
                    if (oldSavedArray != nil)
                        con_array = [[NSMutableArray alloc] initWithArray:oldSavedArray];
                    else
                        con_array = [[NSMutableArray alloc] init];
                }
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //1
                NSString *documentsDirectory = [paths objectAtIndex:0]; //2
                NSString *path = [documentsDirectory stringByAppendingPathComponent:@"data.plist"]; //3
                
                NSMutableArray *savedStock = [[NSMutableArray alloc] initWithContentsOfFile: path];
                if ([con_array count]== 0)
                {
                    con_array = [savedStock mutableCopy];
                    DebugLog(@"yyyyyyyyyy");
                }
                //                if ([del.Bhaswar_array count]== 0)
                //                {
                //                    del.Bhaswar_array = [del.newarr_contacts_ad mutableCopy];
                //                    DebugLog(@"zzzzzzzzzz");
                //                }
                
                DebugLog(@"array ta hochhe: %@",con_array);
                
                // for (long ijk =0; ijk < previous_app_contacts; ijk++)
                // {
                for (int i=0; i<[con_array count]; i++) {
                    
                    if (![[[con_array objectAtIndex:i] objectForKey:@"id"] isKindOfClass:[NSString class]]) {
                        
                        DebugLog(@"ID ID:%@",[[con_array objectAtIndex:i] objectForKey:@"id"]);
                        [con_array removeObjectAtIndex:i];
                        i--;
                    }
                }
                
                //[con_array removeObjectAtIndex:0];
                //   }
                
                DebugLog(@"array ta hochhe ebar: %@",con_array);
                
                // DebugLog(@"finally ami kothay giye daralam er agey: %@",del.Bhaswar_array);
                
                now_Bhaswar_array = [[NSMutableArray alloc]init];
                now_Bhaswar_array = [newcontactsapp mutableCopy];
                
                /////////////////////// Checking Budnav Contact with Backup Contact ////////////////////////////
                int index = 0;
                
                
                
                now_Bhaswar_array = [[newcontactsapp arrayByAddingObjectsFromArray:con_array] mutableCopy];
                
                if ([newcontactsapp count] == 0)
                {
                    now_Bhaswar_array = [con_array mutableCopy];
                }
                
                DebugLog(@"finally ami kothay giye daralam: %@",now_Bhaswar_array);
                [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%ld",(unsigned long)[newcontactsapp count]] forKey:@"app_contacts_count"];
            }
            con_array = [[NSMutableArray alloc]init];
            con_array = now_Bhaswar_array;
            
            
            
#pragma mark -- this is the Beginning of NSLOCALIZEDINDEXEDCOLLATION SECTION - Bhaswar 16/7/2014
            
            // create a dictionary to store an array of objects for each section
            NSMutableDictionary *tempSections = [NSMutableDictionary dictionary];
            
            // iterate through each dictionaey in the list, and put them into the correct section
            for (NSDictionary *item in con_array)
            {
                // get the index of the section (Assuming the table index is showing A-#)
                NSInteger indexName = [[UILocalizedIndexedCollation currentCollation] sectionForObject:[item valueForKey:@"fullname"] collationStringSelector:@selector(description)];
                
                NSNumber *keyName = [NSNumber numberWithInteger:indexName];
                
                // if an array doesnt exist for the key, create one
                NSMutableArray *arrayName = [tempSections objectForKey:keyName];
                if (arrayName == nil)
                {
                    arrayName = [NSMutableArray array];
                }
                
                // add the dictionary to the array (add the actual value as we need this object to sort the array later)
                [arrayName addObject: item];   //[item valueForKey:@"fullname"]];
                
                // put the array with new object in, back into the dictionary for the correct key
                [tempSections setObject:arrayName forKey:keyName];
            }
            
            /* now to do the sorting of each index */
            
            NSMutableDictionary *sortedSections = [NSMutableDictionary dictionary];
            
            // sort each index array (A..Z)
            [tempSections enumerateKeysAndObjectsUsingBlock:^(id key, id array, BOOL *stop)
             {
                 // sort the array - again, we need to tell it which selctor to sort each object by
                 NSArray *sortedArray = [[UILocalizedIndexedCollation currentCollation] sortedArrayFromArray:array collationStringSelector:@selector(description)];
                 
                 NSSortDescriptor *sort =[NSSortDescriptor sortDescriptorWithKey:@"fullname" ascending:YES selector:@selector(caseInsensitiveCompare:)];
                 sortedArray=[[sortedArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]] mutableCopy];
                 [sortedSections setObject:[NSMutableArray arrayWithArray:sortedArray] forKey:key];
             }];
            
            // set the global sectioned dictionary
            indexedSections = sortedSections;
            DebugLog(@"indexed sections & count:%ld...... %@ ", (unsigned long)[indexedSections count], indexedSections);
            
#pragma mark -- this is the End of NSLOCALIZEDINDEXEDCOLLATION SECTION - Bhaswar 16/7/2014
            
            NSMutableArray *fresh_ban = [[NSMutableArray alloc]init];
            [fresh_ban addObject:indexedSections];
            
            [[NSUserDefaults standardUserDefaults]setObject:[NSKeyedArchiver archivedDataWithRootObject:fresh_ban] forKey:@"IndexedContactslocaldb"];
            //            NSData *data_SavedArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"Contactslocaldb"];
            //            if (data_SavedArray != nil)
            //            {
            //                NSArray *oldSavedArray = [NSKeyedUnarchiver unarchiveObjectWithData:data_SavedArray];
            //                DebugLog(@"CONTACT LOCAL DB DB:%@",oldSavedArray);
            //            }
            
            
            [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:con_array] forKey:@"Contactslocaldb"];
            
            //            [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:indexedSections] forKey:@"IndexedContactslocaldb"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            //                        [del loadContactsIntoDB:con_array];
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //1
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString *path = [documentsDirectory stringByAppendingPathComponent:@"data.plist"];
            
            NSMutableArray *data = [[NSMutableArray alloc]init] ;
            data= [con_array mutableCopy];
            DebugLog(@"bbbbb data: %lu",(unsigned long)[data count]);
            [data writeToFile: path atomically:YES];
            
        }
    }
    
    //    [del reloadNewContactsFromWeb];
    [refreshcontrolnew endRefreshing];
    DebugLog(@"TABLE CONTACTS:%@",indexedSections);
    [tab_contact reloadData];
    tab_contact.userInteractionEnabled = YES;
    
    //    [self backupPullTo];
}

-(void)changealpha
{
    addContact.alpha = 0.5f;
    recommended.alpha = 0.5f;
}

-(void)changealpha1
{
    addContact.alpha = 0.5f;
    recommended.alpha = 0.5f;
}

- (void)pushViewController:(UIViewController *)viewController
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.45;
    transition.timingFunction = [CAMediaTimingFunction
                                 functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    transition.fillMode = kCAFillModeForwards;
    transition.delegate = self;
    [self.view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)backupPullTo
{
    ////////////////////////////////////// Using Getbackup To Fetch Device Contacts ///////////////////////////////////////////
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSError *error = nil;
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=getbackup&id=%@&access_token=%@&device_id=%@&thumb=true",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
    
    DebugLog(@"contacts -- now the url is : %@",urlString1);
    
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    
    if (signeddataURL1 == nil)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        //                [alert show];
        [act removeFromSuperview];
        //            [SVProgressHUD dismiss];
    }
    else
    {
        json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                options:kNilOptions
                                                  error:&error];
        DebugLog(@"Back Up Pull To:%@",json1);
        
        NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
        
        if (![errornumber isEqualToString:@"0"])
        {
            NSString *err_str = [json1 objectForKey:@"error"];
            alert = [[UIAlertView alloc] initWithTitle:@"Error in Retrieving Data!"
                                               message:err_str
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
            [act removeFromSuperview];
        }
        else
        {
            backup_array = [[NSMutableArray alloc]init];
            if ([[json1 objectForKey:@"details"] isKindOfClass:[NSNull class]] || [json1 objectForKey:@"details"] == (id)[NSNull null] ||[[json1 objectForKey:@"details"] count] ==0)
            {
                [act removeFromSuperview];
            }
            else
            {
                
                NSDictionary *contacts_temp_dict = [json1 objectForKey:@"details"];
                
                DebugLog(@"(Pull) Backup CONTACTS TEMP DICT:%@",contacts_temp_dict);
                // newcontactsapp = [[NSMutableArray alloc]init];
                //                    NSString *result;
                int con_count1;
                con_count1 = 0;
                for (NSDictionary *dict in [contacts_temp_dict objectForKey:@"contacts"])
                {
                    [backup_array addObject:dict]; //[dict objectForKey:@"fullname"]
                }
                //result = [newcontactsapp componentsJoinedByString:@","];
                
                if (con_count1 == 1)
                {
                    //                                [[[[iToast makeText:[NSString stringWithFormat:@" %@ is a new contact",result]]
                    //                                   setGravity:iToastGravityTop] setDuration:iToastDurationLong] show];
                }
                else
                {
                    //                                [[[[iToast makeText:[NSString stringWithFormat:@" %@ are new contacts",result]]
                    //                                   setGravity:iToastGravityTop] setDuration:iToastDurationLong] show];
                }
                
                DebugLog(@"Back Up Contacts: %@",backup_array);
                //////////////////////////////////////    add to phonebook    ////////////////////////////////////
                //                    if ([[prefs objectForKey:@"copycontacts"] isEqualToString:@"no"])
                
                //                        }   //con_count closing brace
                //                    for (NSDictionary *dict in [contacts_temp_dict objectForKey:@"contacts"])
                //                    {
                //                        [app_contacts addObject:dict];
                //                    }
                [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%ld",[backup_array count]] forKey:@"backup_count"];
            }
        }
    }
}


//////////////////////////////////// Added on 11.5.16 ///////////////////////////////////////////////////////////

- (UIViewController *)visibleViewController:(UIViewController *)rootViewController
{
    if (rootViewController.presentedViewController == nil)
    {
        return rootViewController;
    }
    if ([rootViewController.presentedViewController isKindOfClass:[UINavigationController class]])
    {
        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
        
        return [self visibleViewController:lastViewController];
    }
    if ([rootViewController.presentedViewController isKindOfClass:[UITabBarController class]])
    {
        UITabBarController *tabBarController = (UITabBarController *)rootViewController.presentedViewController;
        UIViewController *selectedViewController = tabBarController.selectedViewController;
        
        return [self visibleViewController:selectedViewController];
    }
    
    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
    
    return [self visibleViewController:presentedViewController];
    
}


@end