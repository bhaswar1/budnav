//
//  ConPasswordChangeViewController.h
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 30/05/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface ConPasswordChangeViewController : GAITrackedViewController<UITextFieldDelegate,UIAlertViewDelegate>
{
    UIView *topbar;
    UITextField *oldpasstext, *newpasstext, *retypetext;
    UIAlertView *alert;
    UIActivityIndicatorView *act;
}
@end
