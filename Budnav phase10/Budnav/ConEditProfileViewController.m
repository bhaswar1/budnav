//  ConEditProfileViewController.m
//  Contacter
//  Created by Bhaswar's MacBook Air on 29/05/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
#import "VPImageCropperViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "ConEditProfileViewController.h"
#import "AppDelegate.h"
#import "ConProfileOwnViewController.h"
#import "ConAccountSettingsViewController.h"
#import "DBManager.h"
#import "SVProgressHUD.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

#define ORIGINAL_MAX_WIDTH 640.0f
#define ORIGINAL_MAX_HEIGHT 550.0f

@interface ConEditProfileViewController ()<VPImageCropperDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    UIView *mainview,*topbar,*businessview;
    UITextField *firstname,*lastname,*email,*yourmobile, *ziptext,*citytext,*streettext,*numbertext, *yourphone, *websitetext, *bphonetext,*bemailtext,*bwebsitetext,*bziptext,*bcitytext,*bstreettext,*bcountrytext,*bnumbertext,*bcompanytext,*bdesigtext;
    UILabel *countrycode,*isdlabel,*countrylabel, *isdphone,*countryphone, *bcountrycode,*bisdlabel,*bcountrylabel, *bisdphone,*bcountryphone;
    UITableView *countrycodetable,*countryflagtable, *countryphonetable, *bcountryflagtable;
    NSArray *countriesList, *countriesFlags;
    UIButton *downbutton,*downbtphone,*personalbt,*businessbt, *downbtbusiness, *downbtphonebusiness, *helpbt;
    UIImageView *countryimg,*countryflagimg, *bcountryimg,*bcountryflagimg;
    UIImageView *prof_img;
    NSMutableDictionary *json,*json1,*profdict;
    NSMutableDictionary *personal_dict,*business_dict,*social_dict;
    int business,picuploaded,check,profile_insertion;
    UIActivityIndicatorView *act, *act1;
    UIImage *portraitImg1;
    NSString *pathss,*dialCode,*flagCode;
    NSOperationQueue *operationQueue;
    NSUserDefaults *prefs;
    BOOL firstTime;
    
}
@end

@implementation ConEditProfileViewController
@synthesize controll_check;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)loadscreen
{
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(toggletable)];
    [countryflagimg addGestureRecognizer:tap];
  
    
    prefs = [NSUserDefaults standardUserDefaults];
    NSError *error = nil;
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=profile&id=%@&access_token=%@&device_id=%@&image=true", [prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
    
    DebugLog(@"profile url: %@",urlString1);
    
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    
    if (signeddataURL1 != nil)
    {
        json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                 
                                                options:kNilOptions
                 
                                                  error:&error];
        DebugLog(@"json returns: %@",json1);
        
        NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
        DebugLog(@"err  %@",errornumber);
        
        if (![errornumber isEqualToString:@"0"])
        {
            DebugLog(@"if if");
            NSString *err_str = [json1 objectForKey:@"error"];
            alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                               message:err_str
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        else
        {
            personal_dict = [[NSMutableDictionary alloc]init];
            business_dict = [[NSMutableDictionary alloc]init];
            social_dict = [[NSMutableDictionary alloc]init];
            
            profdict= [[json1 objectForKey:@"details"] mutableCopy];
            
            [[NSUserDefaults standardUserDefaults] setObject:profdict forKey:@"loadedProfDict"];
            DebugLog(@"FROM WEB:%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"loadedProfDict"]);
            NSDictionary *dict_profile = [profdict objectForKey:@"profile"];
            DebugLog(@"array gives: %@",dict_profile);
            for( NSString *aKey in [dict_profile allKeys])
            {
                //            DebugLog(aKey);
                NSString *newString = [aKey substringToIndex:2];
                //            DebugLog(@"%@", newString);
                if ([newString isEqualToString:@"b_"])
                {
                    [business_dict setValue:[dict_profile valueForKey:[NSString stringWithFormat:@"%@",aKey]] forKey:[NSString stringWithFormat:@"%@",aKey]];
                    //[[NSUserDefaults standardUserDefaults] setObject:business_dict forKey:@"loadedBusiness"];
                    
                }
                else if ([newString isEqualToString:@"s_"])
                {
                    [social_dict setValue:[dict_profile valueForKey:[NSString stringWithFormat:@"%@",aKey]] forKey:[NSString stringWithFormat:@"%@",aKey]];
                    //[[NSUserDefaults standardUserDefaults] setObject:social_dict forKey:@"loadedSocial"];
                    
                }
                else
                {
                    [personal_dict setValue:[dict_profile valueForKey:[NSString stringWithFormat:@"%@",aKey]] forKey:[NSString stringWithFormat:@"%@",aKey]];
                    
                    //[[NSUserDefaults standardUserDefaults] setObject:personal_dict forKey:@"loadedData"];
                    
                }
            }
             [self loadViews];
            
            mainview.userInteractionEnabled = YES;
            
//            [act removeFromSuperview];
            
            [self performSelectorOnMainThread:@selector(startDBInserta:)
                                   withObject:nil
                                waitUntilDone:YES];
        }
    }
    else
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    ///////////////////////////////////////////     Business View     ////////////////////////////////////////////////////////////
    
//    mainview.userInteractionEnabled = YES;
//        
//    [act removeFromSuperview];
    
    
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"searchafterreg"];
    
    self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
    
    //    CGRect frame = CGRectMake(110,165,100,100);
    
    
    topbar = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64)];
    [self.view addSubview:topbar];
    //topbar.backgroundColor=[UIColor blackColor];
//    topbar.layer.zPosition=2;
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
    
    // Add the gradient to the view
    [topbar.layer insertSublayer:gradient atIndex:0];
    
    
    UILabel *topbartitle = [[UILabel alloc]initWithFrame:CGRectMake(60, 32, [UIScreen mainScreen].bounds.size.width-120, 20)];
    topbartitle.backgroundColor=[UIColor clearColor];
    [topbar addSubview:topbartitle];
    topbartitle.text=@"EDIT PROFILE";
    topbartitle.textAlignment=NSTextAlignmentCenter;
    topbartitle.font=[UIFont fontWithName:@"ProximaNova-Regular" size:18];
    topbartitle.textColor=[UIColor whiteColor];
    
    
    UIImageView *bck_img=[[UIImageView alloc]initWithFrame:CGRectMake(15,32,12,20)];//(13, 39, 10, 22)];
    bck_img.image=[UIImage imageNamed:@"back3"];
    [topbar addSubview:bck_img];
    
    helpbt = [UIButton buttonWithType:UIButtonTypeCustom];
    helpbt.frame = CGRectMake(0, 27, 60, 37);    // 12,22,11,19
    helpbt.backgroundColor=[UIColor clearColor];
    [helpbt setTitle:nil forState:UIControlStateNormal];
    helpbt.tag = 1;
    [helpbt addTarget:self action:@selector(goback:) forControlEvents:UIControlEventTouchUpInside];
    [topbar addSubview:helpbt];
    
//    UIView *backbuttonextended = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 85, 60)];
//    backbuttonextended.backgroundColor=[UIColor clearColor];
//    [self.view addSubview:backbuttonextended];
//    backbuttonextended.userInteractionEnabled=YES;
//    
//    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goback:)];
//    [backbuttonextended addGestureRecognizer:tap1];
    
    act = [[UIActivityIndicatorView alloc] init];//WithFrame:frame];
    act.center=self.view.center;
    [act startAnimating];
    [self.view bringSubviewToFront:act];
    act.layer.zPosition = 4;
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [act setColor:[UIColor blackColor]];
    [self.view addSubview:act];
    //}
    //
    //
    //-(void)viewDidAppear:(BOOL)animated
    //{
    //    [super viewDidAppear:YES];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    
    profile_insertion=0;
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, [[UIScreen mainScreen]bounds].size.height-44, [UIScreen mainScreen].bounds.size.width, 44)];
    del.tabBarController.tabBar.hidden = YES;
    self.navigationController.navigationBarHidden=YES;
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    
    
    //    self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
    //   // self.view.backgroundColor = [UIColor grayColor];
    //
    //    topbar = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 60)];
    //    [self.view addSubview:topbar];
    //    topbar.backgroundColor=[UIColor blackColor];
    //    topbar.layer.zPosition=2;
    //
    //    UILabel *topbartitle = [[UILabel alloc]initWithFrame:CGRectMake(60, 12, 200, 50)];
    //    topbartitle.backgroundColor=[UIColor clearColor];
    //    [topbar addSubview:topbartitle];
    //    topbartitle.text=@"EDIT PROFILE";
    //    topbartitle.textAlignment=NSTextAlignmentCenter;
    //    topbartitle.font=[UIFont fontWithName:@"ProximaNova-Regular" size:18];
    //    topbartitle.textColor=[UIColor whiteColor];
    //
    //    //    UIButton *menubt = [UIButton buttonWithType:UIButtonTypeCustom];
    //    //    menubt.frame = CGRectMake(15, 22, 26, 25);
    //    //    [menubt setBackgroundImage:[UIImage imageNamed:@"back-button-ac.png"] forState:UIControlStateNormal];
    //    //    [menubt addTarget:self action:@selector(goback:) forControlEvents:UIControlEventTouchUpInside];
    //    //    [topbar addSubview:menubt];
    //
    //    UIImageView *bck_img=[[UIImageView alloc]initWithFrame:CGRectMake(17,27,8,17)];//(13, 39, 10, 22)];
    //    bck_img.image=[UIImage imageNamed:@"back2"];
    //    [topbar addSubview:bck_img];
    //
    //    UIButton *helpbt = [UIButton buttonWithType:UIButtonTypeCustom];
    //    helpbt.frame = CGRectMake(12, 24.5f, 11, 21);    // 12,22,11,19
    //    helpbt.backgroundColor=[UIColor clearColor];
    //    [helpbt setTitle:nil forState:UIControlStateNormal];
    //    [helpbt addTarget:self action:@selector(goback) forControlEvents:UIControlEventTouchUpInside];
    //    [topbar addSubview:helpbt];
    //
    //    UIView *backbuttonextended = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 85, 60)];
    //    backbuttonextended.backgroundColor=[UIColor clearColor];
    //    [self.view addSubview:backbuttonextended];
    //    backbuttonextended.userInteractionEnabled=YES;
    //
    //    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goback)];
    //    [backbuttonextended addGestureRecognizer:tap1];
    //
    //    act = [[UIActivityIndicatorView alloc] init];//WithFrame:frame];
    //    act.center=self.view.center;
    //    [act startAnimating];
    //    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    //    [act setColor:[UIColor blackColor]];
    //    [self.view addSubview:act];
    
    
    mainview= [[UIView alloc]initWithFrame:CGRectMake(0, 65, self.view.bounds.size.width, self.view.bounds.size.height-65)];
    mainview.backgroundColor=[UIColor clearColor];
    [self.view addSubview:mainview];
    
    
    scviewbudget = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width,self.view.frame.size.height) ];
    scviewbudget.backgroundColor =[UIColor clearColor];
    scviewbudget.scrollEnabled = YES;
    scviewbudget.userInteractionEnabled = YES;
    [scviewbudget setContentSize:CGSizeMake(scviewbudget.frame.size.width,scviewbudget.frame.size.height+255)];
    scviewbudget.contentSize = CGSizeMake(1, scviewbudget.frame.size.height+255);
    scviewbudget.showsVerticalScrollIndicator=NO;
    [mainview addSubview:scviewbudget];
    scviewbudget.contentOffset=CGPointMake(0, 0);
    
    //    [self performSelector:@selector(loadscreen) withObject:nil afterDelay:0.5f];
    
    
    business=0;
    picuploaded=0;
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate=self;
    locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    locationManager.distanceFilter=kCLDistanceFilterNone;
    
    [locationManager startUpdatingLocation];
    geocoder = [[CLGeocoder alloc] init];
    
    //    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //    [del showTabStatus:YES];
    //    //    [del showTabValues:YES];
    //    [del.tabBarController.tabBar setFrame:CGRectMake(0, [[UIScreen mainScreen]bounds].size.height-54, 320, 54)];
    //    self.navigationController.navigationBarHidden=YES;
    //    [del showTabValues:YES];
    
    
    
    [self pageDesign];
    
    //[scviewbudget addSubview:act];
    
    //    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"firstedit"] isEqualToString:@"yes"])
    DebugLog(@"LOADED DATA:%@  %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"loadedProfDict"], [[[NSUserDefaults standardUserDefaults] objectForKey:@"loadedProfDict"] class]);
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"loadedProfDict"] isKindOfClass:[NSNull class]] || [[NSUserDefaults standardUserDefaults] objectForKey:@"loadedProfDict"]==(id)[NSNull null] || !([[NSUserDefaults standardUserDefaults] objectForKey:@"loadedProfDict"])) {
        
        mainview.userInteractionEnabled = NO;
        NSOperationQueue *queue = [NSOperationQueue new];
        NSInvocationOperation *op = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(loadscreen) object:nil];
        [queue addOperation:op];
        
    }
    else
    {
        
        DebugLog(@"LOADED DATA:%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"loadedProfDict"]);
        personal_dict = [[NSMutableDictionary alloc]init];
        business_dict = [[NSMutableDictionary alloc]init];
        social_dict = [[NSMutableDictionary alloc]init];\
        
        profdict = [[NSMutableDictionary alloc]init];
        
        profdict = [[[NSUserDefaults standardUserDefaults] objectForKey:@"loadedProfDict"] mutableCopy];
        
        NSDictionary *dict_profile = [profdict objectForKey:@"profile"];
        DebugLog(@"array gives: %@",dict_profile);
        for( NSString *aKey in [dict_profile allKeys])
        {
            // DebugLog(@"AKEY:%@",aKey);
            NSString *newString = [aKey substringToIndex:2];
            //DebugLog(@"%@", newString);
            if ([newString isEqualToString:@"b_"])
            {
                [business_dict setValue:[dict_profile valueForKey:[NSString stringWithFormat:@"%@",aKey]] forKey:[NSString stringWithFormat:@"%@",aKey]];
                //[[NSUserDefaults standardUserDefaults] setObject:business_dict forKey:@"loadedBusiness"];
                
            }
            else if ([newString isEqualToString:@"s_"])
            {
                [social_dict setValue:[dict_profile valueForKey:[NSString stringWithFormat:@"%@",aKey]] forKey:[NSString stringWithFormat:@"%@",aKey]];
                //[[NSUserDefaults standardUserDefaults] setObject:social_dict forKey:@"loadedSocial"];
                
            }
            else
            {
                [personal_dict setValue:[dict_profile valueForKey:[NSString stringWithFormat:@"%@",aKey]] forKey:[NSString stringWithFormat:@"%@",aKey]];
                
                //[[NSUserDefaults standardUserDefaults] setObject:personal_dict forKey:@"loadedData"];
                
            }
        }
        
        
        [self loadViews];
        [act removeFromSuperview];
        
    }
    
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    DebugLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        DebugLog(@"currentLocation.coordinate.longitude=========>%f",currentLocation.coordinate.longitude);
        DebugLog(@"currentLocation.coordinate.longitude=========>%f",currentLocation.coordinate.latitude);
    }
    //    [locationManager stopUpdatingLocation];
    
    DebugLog(@"Resolving the Address");
    
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemrk, NSError *error) {
        DebugLog(@"Found placemarks: %@, error: %@", placemrk, error);
        if (error == nil && [placemrk count] > 0) {
            placemark = [placemrk lastObject];
            
            DebugLog(@"ADDRESS===================>%@ %@\n%@ %@\n%@\n%@\n%@",placemark.subThoroughfare, placemark.thoroughfare,
                  placemark.postalCode, placemark.locality,
                  placemark.administrativeArea,
                  placemark.country,placemark.ISOcountryCode);
            
            [[NSUserDefaults standardUserDefaults] setObject:placemark.ISOcountryCode forKey:@"placemarkISOCode"];
            [[NSUserDefaults standardUserDefaults] setObject:placemark.country forKey:@"placemarkCode"];
            
            [locationManager stopUpdatingLocation];
            [act stopAnimating];
            [act setHidden:YES];
        } else {
            DebugLog(@"%@", error.debugDescription);
        }
    }];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    DebugLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

-(void)loadViews
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
   
    
    firstname.text=[personal_dict objectForKey:@"name"];
    lastname.text=[personal_dict objectForKey:@"surname"];
    base64String = [profdict objectForKey:@"image"];
    if ([base64String length] > 6)
    {
        NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
        //    NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
        
        UIImage *profilepic = [UIImage imageWithData:decodedData];
        
        prof_img.image=profilepic;
        prof_img.contentMode= UIViewContentModeScaleAspectFill;
        prof_img.clipsToBounds=YES;
    }
    
    if ([personal_dict objectForKey:@"mobile_num"] != false){
        
        yourmobile.text= [NSString stringWithFormat:@"%@",[personal_dict objectForKey:@"mobile_num"]];
    }
    else{
        yourmobile.text=@"";
    }
    
    if ([personal_dict objectForKey:@"phone_num"] != false){
        
        yourphone.text= [NSString stringWithFormat:@"%@",[personal_dict objectForKey:@"phone_num"]];
    }
    else{
        yourphone.text=@"";
    }
    
    if ([personal_dict objectForKey:@"email"] != false){
        
        email.text= [NSString stringWithFormat:@"%@",[personal_dict objectForKey:@"email"]];
    }
    else{
        
        email.text=@"";
    }
    
    if ([personal_dict objectForKey:@"website"] != false){
        
        websitetext.text= [NSString stringWithFormat:@"%@",[personal_dict objectForKey:@"website"]];
    }
    else{
        
        websitetext.text=@"";
    }
    
    if ([personal_dict objectForKey:@"city"] != false){
        
        citytext.text= [NSString stringWithFormat:@"%@",[personal_dict objectForKey:@"city"]];
    }
    else{
        
        citytext.text=@"";
    }
    
    if ([personal_dict objectForKey:@"street"] != false){
        
        streettext.text= [NSString stringWithFormat:@"%@",[personal_dict objectForKey:@"street"]];
    }
    else{
        streettext.text=@"";
    }
    
    if ([personal_dict objectForKey:@"zipcode"] != false){
        
        ziptext.text= [NSString stringWithFormat:@"%@",[personal_dict objectForKey:@"zipcode"]];
    }
    else{
        
        ziptext.text=@"";
    }
    
    if ([personal_dict objectForKey:@"housenumber"] != false){
        
        numbertext.text= [NSString stringWithFormat:@"%@",[personal_dict objectForKey:@"housenumber"]];
    }
    else{
        
        numbertext.text=@"";
    }
    
    if ([personal_dict objectForKey:@"mobile_pre"] != false && ![[personal_dict objectForKey:@"mobile_pre"] isEqualToString:@"0"] && [[personal_dict objectForKey:@"mobile_pre"] length] > 1 && ![[personal_dict objectForKey:@"mobile_pre"] isKindOfClass:[NSNull class]] && ![[personal_dict objectForKey:@"mobile_pre"] isEqualToString:@""] && ![[personal_dict objectForKey:@"mobile_pre"] isEqualToString:@"(null)"]){
        
        DebugLog(@"mobile pre substring: %@",[[personal_dict objectForKey:@"mobile_pre"] substringToIndex:1]);
        
        if ([[[personal_dict objectForKey:@"mobile_pre"] substringToIndex:1] isEqualToString:@"+"]){
            DebugLog(@"MOBILE PRE1:%@",[personal_dict objectForKey:@"mobile_pre"]);
            isdlabel.text= [NSString stringWithFormat:@"%@",[personal_dict objectForKey:@"mobile_pre"]];
        }
        else{
            
            DebugLog(@"MOBILE PRE:%@",[personal_dict objectForKey:@"mobile_pre"]);
            isdlabel.text= [NSString stringWithFormat:@"+%@",[personal_dict objectForKey:@"mobile_pre"]];
        }
        
        for(NSDictionary *data in countriesList)
        {
            if([isdlabel.text isEqualToString:[NSString stringWithFormat:@"%@",[data objectForKey:@"dial_code"]]]){
                DebugLog(@"peyche common:%@",[data objectForKey:@"dial_code"]);
                int index = (int)[countriesList indexOfObject:data];
                //Login
                countrycode.text = [NSString stringWithFormat:@"%@",[[countriesList objectAtIndex:index]objectForKey:@"code"]];
                //                isdlabel.text=[NSString stringWithFormat:@"%@",[data objectForKey:@"dial_code"]];
            }
        }
    }
    else {
        if (![[[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkISOCode"] isEqualToString:@"(null)"]) {
            
            countrycode.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkISOCode"];
            isdlabel.text=[NSString stringWithFormat:@"%@",dialCode];
            
        }
        else
        {
            DebugLog(@"NULL PAINI :%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkISOCode"]);
        }
        
    }
    
    if ([personal_dict objectForKey:@"phone_pre"] != false && ![[personal_dict objectForKey:@"phone_pre"] isEqualToString:@"0"] && [[personal_dict objectForKey:@"phone_pre"] length] > 1 && ![[personal_dict objectForKey:@"phone_pre"] isKindOfClass:[NSNull class]] && ![[personal_dict objectForKey:@"phone_pre"] isEqualToString:@""] && ![[personal_dict objectForKey:@"phone_pre"] isEqualToString:@"(null)"]){
        
        isdphone.text= [NSString stringWithFormat:@"%@",[personal_dict objectForKey:@"phone_pre"]];
        //        if ([countriesList containsObject:[personal_dict objectForKey:@"phone_pre"]])
        //        {
        //            DebugLog(@"Found");
        //            int index = [countriesList indexOfObject:isdlabel.text];
        //            countrycode.text = [NSString stringWithFormat:@"%@",[[countriesList objectAtIndex:index]objectForKey:@"code"]];
        //        }
        for( NSDictionary *data in countriesList)
        {
            if([isdphone.text isEqualToString:[NSString stringWithFormat:@"%@",[data objectForKey:@"dial_code"]]]){
                DebugLog(@"peyche phone common1!%@",[data objectForKey:@"dial_code"]);
                int index = (int)[countriesList indexOfObject:data];
                //Login
                if (![[[countriesList objectAtIndex:index]objectForKey:@"code"] isEqualToString:@"(null)"]) {
                    
                    countryphone.text = [NSString stringWithFormat:@"%@",[[countriesList objectAtIndex:index]objectForKey:@"code"]];
                    
                }
                
                //                isdphone.text=[NSString stringWithFormat:@"%@",[data objectForKey:@"dial_code"]];
            }
        }
    }
    else {
        
        if (![[[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkISOCode"] isEqualToString:@"(null)"] && ![[[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkISOCode"] isEqualToString:@""] && ![[[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkISOCode"] isKindOfClass:[NSNull class]] && [[[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkISOCode"] length]>0) {
            
            DebugLog(@"ISD CODE:%@ %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkISOCode"],dialCode);
            countryphone.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkISOCode"];
            isdphone.text=[NSString stringWithFormat:@"%@",dialCode];
            
        }
        
    }
    
    if ([personal_dict objectForKey:@"country"] != false && ![[personal_dict objectForKey:@"country"] isEqualToString:@"0"] && [[personal_dict objectForKey:@"country"] length] > 1)
    {
        countrylabel.text= [NSString stringWithFormat:@"%@",[personal_dict objectForKey:@"country"]];
        NSString *countrystr = [[NSString stringWithFormat:@"%@",[personal_dict objectForKey:@"country"]] lowercaseString];
        countrystr =[NSString stringWithFormat:@"%@_32.png",countrystr];
        countryimg.image=[UIImage imageNamed:countrystr];
        
        NSString *flagCodeaa;
        for (check=0; check < countriesFlags.count; check++){
            
            if ([[[countriesFlags objectAtIndex:check]objectForKey:@"name"] isEqualToString:[personal_dict objectForKey:@"country"]]) {
                
                flagCodeaa = [[countriesFlags objectAtIndex:check]objectForKey:@"flag_128"];
            }
        }
        
        DebugLog(@"answer bm = %@",flagCodeaa);
        DebugLog(@"COUNTRY IMAGE====================>%@",countrystr);
        bcountryimg.image=[UIImage imageNamed:flagCodeaa];
    }
    else {
        countryimg.image=[UIImage imageNamed:flagCode];
        
        countrylabel.text=[[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkCode"];
    }
    
    //    else        ///////////// Business Data is Loading here
    //    {
    if ([business_dict objectForKey:@"b_company"] != false)
    {
        bcompanytext.text= [NSString stringWithFormat:@"%@",[business_dict objectForKey:@"b_company"]];
    }
    else
        bcompanytext.text=@"";
    
    if ([business_dict objectForKey:@"b_function"] != false)
    {
        bdesigtext.text= [NSString stringWithFormat:@"%@",[business_dict objectForKey:@"b_function"]];
    }
    else
        bdesigtext.text=@"";
    
    if ([business_dict objectForKey:@"b_phone_num"] != false)
        bphonetext.text= [NSString stringWithFormat:@"%@",[business_dict objectForKey:@"b_phone_num"]];
    else
        bphonetext.text=@"";
    
    if ([business_dict objectForKey:@"b_email"] != false)
        bemailtext.text= [NSString stringWithFormat:@"%@",[business_dict objectForKey:@"b_email"]];
    else
        bemailtext.text=@"";
    
    if ([business_dict objectForKey:@"b_website"] != false)
        bwebsitetext.text= [NSString stringWithFormat:@"%@",[business_dict objectForKey:@"b_website"]];
    else
        bwebsitetext.text=@"";
    
    if ([business_dict objectForKey:@"b_city"] != false)
    {
        bcitytext.text= [NSString stringWithFormat:@"%@",[business_dict objectForKey:@"b_city"]];
    }
    else
        bcitytext.text=@"";
    
    if ([business_dict objectForKey:@"b_street"] != false)
    {
        bstreettext.text= [NSString stringWithFormat:@"%@",[business_dict objectForKey:@"b_street"]];
    }
    else
        bstreettext.text=@"";
    
    if ([business_dict objectForKey:@"b_zipcode"] != false)
    {
        bziptext.text= [NSString stringWithFormat:@"%@",[business_dict objectForKey:@"b_zipcode"]];
    }
    else
        bziptext.text=@"";
    
    if ([business_dict objectForKey:@"b_housenumber"] != false)
    {
        bnumbertext.text= [NSString stringWithFormat:@"%@",[business_dict objectForKey:@"b_housenumber"]];
    }
    else
        bnumbertext.text=@"";
    
    if ([business_dict objectForKey:@"b_phone_pre"] != false && ![[business_dict objectForKey:@"b_phone_pre"] isEqualToString:@"0"] && [[business_dict objectForKey:@"b_phone_pre"] length] > 1 && ![[business_dict objectForKey:@"b_phone_pre"] isEqualToString:@""] && ![[business_dict objectForKey:@"b_phone_pre"] isEqualToString:@"(null)"] && ![[business_dict objectForKey:@"b_phone_pre"] isKindOfClass:[NSNull class]])
    {
        bisdlabel.text= [NSString stringWithFormat:@"%@",[business_dict objectForKey:@"b_phone_pre"]];
        //        if ([countriesList containsObject:[personal_dict objectForKey:@"phone_pre"]])
        //        {
        //            DebugLog(@"Found");
        //            int index = [countriesList indexOfObject:isdlabel.text];
        //            countrycode.text = [NSString stringWithFormat:@"%@",[[countriesList objectAtIndex:index]objectForKey:@"code"]];
        //        }
        for( NSDictionary *data in countriesList)
        {
            if([bisdlabel.text isEqualToString:[data objectForKey:@"dial_code"]]){
                int index = (int)[countriesList indexOfObject:data];
                //Login
                bcountrycode.text = [NSString stringWithFormat:@"%@",[[countriesList objectAtIndex:index]objectForKey:@"code"]];
            }
        }
    }
    else
    {
        
        if (![[[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkISOCode"] isEqualToString:@"(null)"] && ![[[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkISOCode"] isEqualToString:@""] && ![[[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkISOCode"] isKindOfClass:[NSNull class]] && [[[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkISOCode"] length]>0) {
            
            DebugLog(@"ISD CODE:%@ %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkISOCode"],dialCode);
            bisdlabel.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkISOCode"];
            bcountrycode.text=[NSString stringWithFormat:@"%@",dialCode];
            
        }
        
        
        //        bisdlabel.text=[NSString stringWithFormat:@"%@",dialCode];
        //        bcountrycode.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkISOCode"];
    }
    if ([business_dict objectForKey:@"b_country"] != false && ![[business_dict objectForKey:@"b_country"] isEqualToString:@"0"] && [[business_dict objectForKey:@"b_country"] length] > 1 && ![[business_dict objectForKey:@"b_country"] isKindOfClass:[NSNull class]] && ![[business_dict objectForKey:@"b_country"] isEqualToString:@""] && ![[business_dict objectForKey:@"b_country"] isEqualToString:@"(null)"])
    {
        bcountrylabel.text= [NSString stringWithFormat:@"%@",[business_dict objectForKey:@"b_country"]];
        NSString *countrystr = [[NSString stringWithFormat:@"%@",[business_dict objectForKey:@"b_country"]] lowercaseString];
        countrystr =[NSString stringWithFormat:@"%@_32.png",countrystr];
        
        NSString *flagCodeaa;
        for (check=0; check < countriesFlags.count; check++){
            
            if ([[[countriesFlags objectAtIndex:check]objectForKey:@"name"] isEqualToString:[business_dict objectForKey:@"b_country"]]) {
                
                flagCodeaa = [[countriesFlags objectAtIndex:check]objectForKey:@"flag_128"];
                
            }
        }
        
        DebugLog(@"answer bm = %@",flagCodeaa);
        DebugLog(@"COUNTRY IMAGE====================>%@",countrystr);
        bcountryimg.image=[UIImage imageNamed:flagCodeaa];
    }
    else {
        
        bcountryimg.image=[UIImage imageNamed:flagCode];
        
        bcountrylabel.text=[[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkCode"];
        
    }
        [act removeFromSuperview];
         }];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) goback:(UIButton *)button
{
   
    //    ConAccountSettingsViewController *con = [[ConAccountSettingsViewController alloc]init];
    //    [self.navigationController pushViewController:con animated:NO];
    if (button.tag==1) {
        
        if (controll_check) {
            
            controll_check = FALSE;
            
//            CATransition *transition = [CATransition animation];
//            
//            transition.duration = 0.4f;
//            
//            transition.type = kCATransitionFade;
//            
//            [[self navigationController].view.layer addAnimation:transition forKey:nil];
            [self.navigationController popViewControllerAnimated:YES];
            
        }
        else
        {
//            CATransition* transition = [CATransition animation];
//            
//            transition.duration = 0.4;
            //transition.type = kCATransitionPush;
           // transition.type = kCATransitionFade;
            //transition.subtype = kCATransitionFromLeft;
            
          //  [[self navigationController].view.layer addAnimation:transition forKey:nil];
            
            [self.navigationController popViewControllerAnimated:YES];
        //[self dismissViewControllerAnimated:YES completion:nil];
        }
        
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
       // [self.navigationController popViewControllerAnimated:YES];
    }
    if (button.tag==2)
    {
        countrycodetable.hidden = YES;
        countryflagtable.hidden = YES;
        countryphonetable.hidden = YES;
        bcountryflagtable.hidden = YES;
        
        helpbt.tag = 1;
    }
    
    //  [self.navigationController popViewControllerAnimated:NO];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [UIView animateWithDuration:0.5f animations:^{
        scviewbudget.contentOffset = CGPointMake(0, -20);
    }];
    return YES;
}

-(void)toggletable
{
    if (business == 0)
    {
        countryflagimg.userInteractionEnabled=YES;
        [countrycodetable setHidden:YES];
        [countryphonetable setHidden:YES];
        
        if (countryflagtable.hidden==YES)
        {
            countryflagtable.hidden=NO;
            helpbt.tag = 2;
        }
        else
        {
            countryflagtable.hidden=YES;
        }
        
        downbutton = [UIButton buttonWithType:UIButtonTypeCustom];
        downbutton.frame = CGRectMake(51, 171, 22, 20);
        [downbutton setBackgroundImage:[UIImage imageNamed:@"dnwarr.png"] forState:UIControlStateNormal];
        downbutton.userInteractionEnabled = YES;
        downbutton.backgroundColor=[UIColor clearColor];
        [downbutton addTarget:self action:@selector(showtable:) forControlEvents:UIControlEventTouchUpInside];
        [scviewbudget addSubview:downbutton];
        
        downbtphone = [UIButton buttonWithType:UIButtonTypeCustom];
        downbtphone.frame = CGRectMake(51, 224, 22, 20);
        [downbtphone setBackgroundImage:[UIImage imageNamed:@"dnwarr.png"] forState:UIControlStateNormal];
        downbtphone.backgroundColor=[UIColor clearColor];
        [downbtphone addTarget:self action:@selector(showtablephone:) forControlEvents:UIControlEventTouchUpInside];
        [scviewbudget addSubview:downbtphone];
    }
    else
    {
        // DebugLog(@"called2");
        if (bcountryflagtable.hidden==YES)
        {
            //  DebugLog(@"called3");
            [bcountryflagtable setHidden:NO];
            bcountryflagimg.userInteractionEnabled=YES;
            helpbt.tag = 2;
        }
        else
        {
            // DebugLog(@"called4");
            bcountryflagtable.hidden=YES;
            bcountryflagimg.userInteractionEnabled=YES;
        }
    }
}

-(void)showtable: (id)sender
{
    if (business == 0)
    {
        //DebugLog(@"called1");
        if (countrycodetable.hidden==YES)
        {
            NSLog(@"etay bm1");
            [countrycodetable setHidden:NO];
            //        downbutton.transform = CGAffineTransformMakeRotation(M_PI);
            countryflagimg.userInteractionEnabled=YES;
            if(countryphonetable.hidden==NO)
                [countryphonetable setHidden:YES];
            
            helpbt.tag =2;
        
        }
        else
        {
            NSLog(@"etay bm2");

            countrycodetable.hidden=YES;
            downbutton = [UIButton buttonWithType:UIButtonTypeCustom];
            downbutton.frame = CGRectMake(51, 171, 22, 20);
            [downbutton setBackgroundImage:[UIImage imageNamed:@"dnwarr.png"] forState:UIControlStateNormal];
            downbutton.backgroundColor=[UIColor clearColor];
            [downbutton addTarget:self action:@selector(showtable:) forControlEvents:UIControlEventTouchUpInside];
            [scviewbudget addSubview:downbutton];
            
            countryflagimg.userInteractionEnabled=YES;
        }
        [countryflagtable setHidden:YES];
        if(countryphonetable.hidden==NO)
            [countryphonetable setHidden:YES];
        
        downbtphone = [UIButton buttonWithType:UIButtonTypeCustom];
        downbtphone.frame = CGRectMake(51, 224, 22, 20);
        [downbtphone setBackgroundImage:[UIImage imageNamed:@"dnwarr.png"] forState:UIControlStateNormal];
        downbtphone.backgroundColor=[UIColor clearColor];
        [downbtphone addTarget:self action:@selector(showtablephone:) forControlEvents:UIControlEventTouchUpInside];
        [scviewbudget addSubview:downbtphone];
        
        //        downbtphone.hidden=YES;
        //        downbutton.hidden=YES;
    }
    else
    {
        if (countrycodetable.hidden==YES)
        {
            NSLog(@"etay bm3");
            [countrycodetable setHidden:NO];
            bcountryflagimg.userInteractionEnabled=YES;
            [bcountryflagtable setHidden:YES];
            downbtphone.hidden=YES;
            downbutton.hidden=YES;
            
            helpbt.tag =2;
            
        }
        else
        {
            countrycodetable.hidden=YES;
            bcountryflagimg.userInteractionEnabled=YES;
            
        }
        
    }
}

-(void)showtablephone: (id)sender
{
    if (business == 0)
    {
        if (countryphonetable.hidden==YES)
        {
            countryphonetable.hidden=NO;
            //        downbtphone.transform = CGAffineTransformMakeRotation(M_PI);
            countryflagimg.userInteractionEnabled=YES;
            helpbt.tag =2;
            
        }
        else
        {
            countryphonetable.hidden=YES;
            downbtphone = [UIButton buttonWithType:UIButtonTypeCustom];
            downbtphone.frame = CGRectMake(51, 171, 22, 20);
            [downbtphone setBackgroundImage:[UIImage imageNamed:@"dnwarr.png"] forState:UIControlStateNormal];
            downbtphone.backgroundColor=[UIColor clearColor];
            [downbtphone addTarget:self action:@selector(showtablephone:) forControlEvents:UIControlEventTouchUpInside];
            [scviewbudget addSubview:downbtphone];
            
            countryflagimg.userInteractionEnabled=YES;
            helpbt.tag = 2;
        }
        
        [countryflagtable setHidden:YES];
        if (countrycodetable.hidden == NO)
            [countrycodetable setHidden:YES];
        downbutton = [UIButton buttonWithType:UIButtonTypeCustom];
        downbutton.frame = CGRectMake(51, 171, 22, 20);
        [downbutton setBackgroundImage:[UIImage imageNamed:@"dnwarr.png"] forState:UIControlStateNormal];
        downbutton.backgroundColor=[UIColor clearColor];
        [downbutton addTarget:self action:@selector(showtable:) forControlEvents:UIControlEventTouchUpInside];
        [scviewbudget addSubview:downbutton];
    }
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView == countrycodetable || tableView == countryphonetable)
        return [countriesList count];
    else
        return [countriesFlags count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    if (tableView == countrycodetable || tableView == countryphonetable)
    {
        UITableViewCell *cell = [countrycodetable dequeueReusableCellWithIdentifier:CellIdentifier];
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        // cell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"profile-bg.png"]];
        cell.backgroundColor=[UIColor clearColor];
        
        UILabel *name=[[UILabel alloc]initWithFrame:CGRectMake(10, 0, 200, 25)];
        name.backgroundColor=[UIColor clearColor];
        name.text=[NSString stringWithFormat:@"%@",[[countriesList objectAtIndex:indexPath.row] objectForKey:@"name"]];
        name.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
        name.textColor=[UIColor blackColor];
        name.textAlignment=NSTextAlignmentLeft;
        [cell addSubview:name];
        
        
        UILabel *code=[[UILabel alloc]initWithFrame:CGRectMake(250, 0, 50, 25)];
        code.backgroundColor=[UIColor clearColor];
        code.text=[NSString stringWithFormat:@"%@",[[countriesList objectAtIndex:indexPath.row] objectForKey:@"code"]];
        code.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
        code.textColor=[UIColor blackColor];
        code.textAlignment=NSTextAlignmentCenter;
        [cell addSubview:code];
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return cell;
    }
    else
    {
        UITableViewCell *cell = [countryflagtable dequeueReusableCellWithIdentifier:CellIdentifier];
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor=[UIColor clearColor];
        
        UIImageView *cellimg=[[UIImageView alloc]initWithFrame:CGRectMake(10, 7, 35, 22)];
        cellimg.image=[UIImage imageNamed:[[countriesFlags objectAtIndex:indexPath.row] objectForKey:@"flag_32"]];
        cellimg.clipsToBounds=YES;
        [cell addSubview:cellimg];
        
        UILabel *name=[[UILabel alloc]initWithFrame:CGRectMake(60, 0, 230, 35)];
        name.backgroundColor=[UIColor clearColor];
        name.text=[NSString stringWithFormat:@"%@",[[countriesFlags objectAtIndex:indexPath.row] objectForKey:@"name"]];
        name.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
        name.textColor=[UIColor blackColor];
        [cell addSubview:name];
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return cell;
        
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (business == 0)
    {
        if (tableView == countrycodetable)
        {
            
            countrycode.text = [NSString stringWithFormat:@"%@",[[countriesList objectAtIndex:indexPath.row]objectForKey:@"code"]];
            isdlabel.text = [NSString stringWithFormat:@"%@",[[countriesList objectAtIndex:indexPath.row]objectForKey:@"dial_code"]];
            [countrycodetable setHidden:YES];
            
            downbutton = [UIButton buttonWithType:UIButtonTypeCustom];
            downbutton.frame = CGRectMake(51, 171, 22, 20);
            [downbutton setBackgroundImage:[UIImage imageNamed:@"dnwarr.png"] forState:UIControlStateNormal];
            downbutton.backgroundColor=[UIColor clearColor];
            [downbutton addTarget:self action:@selector(showtable:) forControlEvents:UIControlEventTouchUpInside];
            [scviewbudget addSubview:downbutton];
        }
        else if (tableView == countryphonetable)
        {
            countryphone.text = [NSString stringWithFormat:@"%@",[[countriesList objectAtIndex:indexPath.row]objectForKey:@"code"]];
            isdphone.text = [NSString stringWithFormat:@"%@",[[countriesList objectAtIndex:indexPath.row]objectForKey:@"dial_code"]];
            [countryphonetable setHidden:YES];
            
            downbtphone = [UIButton buttonWithType:UIButtonTypeCustom];
            downbtphone.frame = CGRectMake(51, 224, 22, 20);
            [downbtphone setBackgroundImage:[UIImage imageNamed:@"dnwarr.png"] forState:UIControlStateNormal];
            downbtphone.backgroundColor=[UIColor clearColor];
            [downbtphone addTarget:self action:@selector(showtablephone:) forControlEvents:UIControlEventTouchUpInside];
            [scviewbudget addSubview:downbtphone];
        }
        
        else
        {
            countrylabel.text = [NSString stringWithFormat:@"%@",[[countriesFlags objectAtIndex:indexPath.row]objectForKey:@"name"]];
            countryimg.image = [UIImage imageNamed:[[countriesFlags objectAtIndex:indexPath.row] objectForKey:@"flag_128"]];
            countryflagtable.hidden=YES;
           
        }
        [countryflagtable setHidden:YES];
        [countrycodetable setHidden:YES];
        [countryphonetable setHidden:YES];
    }
    else
    {
        if (tableView == countrycodetable)
        {
            bcountrycode.text = [NSString stringWithFormat:@"%@",[[countriesList objectAtIndex:indexPath.row]objectForKey:@"code"]];
            bisdlabel.text = [NSString stringWithFormat:@"%@",[[countriesList objectAtIndex:indexPath.row]objectForKey:@"dial_code"]];
            [countrycodetable setHidden:YES];
            
            bcountryflagimg.userInteractionEnabled=YES;
        }
        else
        {
            bcountrylabel.text = [NSString stringWithFormat:@"%@",[[countriesFlags objectAtIndex:indexPath.row]objectForKey:@"name"]];
            bcountryimg.image = [UIImage imageNamed:[[countriesFlags objectAtIndex:indexPath.row] objectForKey:@"flag_128"]];
            bcountryflagtable.hidden=YES;
            
        }
        
        
    }
    helpbt.tag = 1;
    countryflagimg.userInteractionEnabled=YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == countrycodetable || tableView == countryphonetable)
        return 25.0f;
    else
        return 35;
}

-(void)gotonext:(id)sender
{
    DebugLog(@"gotonext");
    
    [UIView animateWithDuration:0.0
                     animations:^{
                         
                         [SVProgressHUD showWithStatus:@"Saving......"];
  
                         
                     }
                     completion:^(BOOL finished){
                         
                         NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
                         NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
                         
                         NSString *mobile_pre_en = [self encodeToPercentEscapeString:isdlabel.text];
                         NSString *phone_pre_en = [self encodeToPercentEscapeString:isdphone.text];
                         NSString *bphone_pre_en = [self encodeToPercentEscapeString:bisdlabel.text];
                         
                         DebugLog(@"Encoded text: %@ %@ %@", mobile_pre_en, phone_pre_en, bphone_pre_en);
                         
                         if ([websitetext.text length] > 0)
                         {
                             if ([websitetext.text hasPrefix:@"http://"]) {
                                 
                             } else {
                                 websitetext.text = [NSString stringWithFormat:@"http://%@",websitetext.text];
                             }
                         }
                         
                         if ([bwebsitetext.text length] > 0)
                         {
                             if ([bwebsitetext.text hasPrefix:@"http://"]) {
                                 
                             } else {
                                 bwebsitetext.text = [NSString stringWithFormat:@"http://%@",bwebsitetext.text];
                             }
                         }
                         
                         
                         if ([[firstname.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
                         {
                             alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your First Name"
                                                                message:Nil
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                             [alert show];
                         }
                         else if ([[lastname.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
                         {
                             alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your Last Name"
                                                                message:Nil
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                             [alert show];
                             
                         }
                         else if ([[yourmobile.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
                         {
                             alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your Mobile Number"
                                                                message:Nil
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                             [alert show];
                         }
                         
                         else if ([[yourmobile.text stringByTrimmingCharactersInSet:whitespace] length] <8 || [[yourmobile.text stringByTrimmingCharactersInSet:whitespace] length] > 10 || !([yourmobile.text rangeOfCharacterFromSet:notDigits].location == NSNotFound))
                         {
                             alert = [[UIAlertView alloc] initWithTitle:@"Please Enter a Valid Mobile Number"
                                                                message:Nil
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                             [alert show];
                         }
                         
                         else if ([[email.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
                         {
                             alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your Email"
                                                                message:Nil
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                             [alert show];
                         }
                         
                         else if(![self NSStringIsValidEmail:email.text])
                         {
                             email.text =nil;
                             alert =[[UIAlertView alloc]initWithTitle:@"Email Id is Invalid" message:Nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                             
                             [alert show];
                             return;
                         }
                         
                         else if ([[bemailtext.text stringByTrimmingCharactersInSet:whitespace] length] > 0 && ![self NSStringIsValidEmail:bemailtext.text])
                         {
                             alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your Business email"
                                                                message:Nil
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                             [alert show];
                         }
                         else if ([[websitetext.text stringByTrimmingCharactersInSet:whitespace] length] > 0 && ![self validateUrl:websitetext.text])
                         {
                             alert = [[UIAlertView alloc] initWithTitle:@"Please Enter a valid Website"
                                                                message:Nil
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                             [alert show];
                         }
                         else if ([[bwebsitetext.text stringByTrimmingCharactersInSet:whitespace] length]> 0 && ![self validateUrl:bwebsitetext.text])
                         {
                             alert = [[UIAlertView alloc] initWithTitle:@"Please Enter a valid Website"
                                                                message:Nil
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                             [alert show];
                         }
                         
                         else
                         {
                             
                             prefs = [NSUserDefaults standardUserDefaults];
                             NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=editprofile&name=%@&surname=%@&email=%@&website=%@&mobile_pre=%@&mobile_num=%@&phone_pre=%@&phone_num=%@&street=%@&housenumber=%@&zipcode=%@&city=%@&country=%@&b_email=%@&b_website=%@&b_phone_pre=%@&b_phone_num=%@&b_company=%@&b_function=%@&b_street=%@&b_housenumber=%@&b_zipcode=%@&b_city=%@&b_country=%@&access_token=%@&device_id=%@",[firstname.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding], [lastname.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[email.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[websitetext.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],mobile_pre_en,[yourmobile.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],phone_pre_en,[yourphone.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[streettext.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[numbertext.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[ziptext.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[citytext.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[countrylabel.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[bemailtext.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[bwebsitetext.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],bphone_pre_en,[bphonetext.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[bcompanytext.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[bdesigtext.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[bstreettext.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[bnumbertext.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[bziptext.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[bcitytext.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[bcountrylabel.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
                             
                             DebugLog(@"deny url: %@",urlString1);
                             NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
                             
                             NSError *error= nil;
                             NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
                             
                             NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                                       options:kNilOptions
                                                                                         error:&error];
                             DebugLog(@"deny json returns: %@",json_deny);
                             
                             if ([[json_deny objectForKey:@"success"]intValue] == 1)
                             {
                                 //        [self loadrequestsdata];
                                 //        tab_request =Nil;
                                 //        [tab_request reloadData];
                                 
                                 [self loadscreen];
                                 
                                 
                                 
                                 alert = [[UIAlertView alloc] initWithTitle:@"Changes have been Successfully Saved!"
                                                                    message:nil
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                                 [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"profile_edited"];
                                 [alert show];
                                 [SVProgressHUD dismiss];
                                 //            ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
                                 //            con.other=@"no";
                                 //            con.uid=[prefs objectForKey:@"id"];
                                 //            [self.navigationController pushViewController:con animated:NO];
                                 
                                 [self editpic];
                                 
                                // AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                                 //[del showTabStatus:YES];
                                 
                                 ConAccountSettingsViewController *account_set = [[ConAccountSettingsViewController alloc] init];
                                 account_set.push_check = TRUE;
                                 
                                 //[del selectedIcon];
                                 
                                 CATransition *transition = [CATransition animation];
                                 
                                 transition.duration = 0.4f;
                                 
                                 transition.type = kCATransitionFade;
                                 transition.subtype = kCATransitionFromRight;
                                 [[self navigationController].view.layer addAnimation:transition forKey:nil];
                                 [self dismissViewControllerAnimated:NO completion:nil];
                                 [self presentViewController:account_set animated:NO completion:nil];
                                 
                                 
                             }
                             
                         }
                         
                         
                     }];

  
    
}

- (BOOL) validateUrl: (NSString *) candidate {
    NSString *urlRegEx =
    @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:candidate];
}


-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    /*BOOL stricterFilter = YES;
     NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
     NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
     NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
     NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
     return [emailTest evaluateWithObject:checkString];*/
    checkString = [checkString lowercaseString];
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:checkString];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    //    if (self.view.frame.size.height < 500)
    if (business == 0)
    {
        if (textField == email || textField == yourphone || textField==websitetext)
            [UIView animateWithDuration:0.5f animations:^{
                scviewbudget.contentOffset = CGPointMake(0, 150);
            }];
        
        else if(textField == streettext|| textField==numbertext || textField==citytext || textField==ziptext){
            
            if (self.view.frame.size.height < 500)
            {
                [UIView animateWithDuration:0.5f animations:^{
                    scviewbudget.contentOffset = CGPointMake(0, 350);
                }];
                
            }
            else
            {
                [UIView animateWithDuration:0.5f animations:^{
                    scviewbudget.contentOffset = CGPointMake(0, 290);
                }];
            }
        }
    }
    else
    {
        if (textField == bemailtext || textField == bphonetext || textField==bwebsitetext)
            [UIView animateWithDuration:0.5f animations:^{
                scviewbudget.contentOffset = CGPointMake(0, 150);
            }];
        
        else if(textField == bstreettext|| textField==bnumbertext || textField==bcitytext || textField==bziptext){
            if (self.view.frame.size.height < 500)
            {
                [UIView animateWithDuration:0.5f animations:^{
                    scviewbudget.contentOffset = CGPointMake(0, 350);
                }];
                
            }
            else
            {
                [UIView animateWithDuration:0.5f animations:^{
                    scviewbudget.contentOffset = CGPointMake(0, 300);
                }];
            }
        }
        
    }
}


-(void)camera_func
{
    DebugLog(@"camera func");
    if ([self isCameraAvailable] && [self doesCameraSupportTakingPhotos]) {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypeCamera;
        if ([self isFrontCameraAvailable]) {
            controller.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        }
        NSMutableArray *mediaTypes = [[NSMutableArray alloc] init];
        [mediaTypes addObject:(__bridge NSString *)kUTTypeImage];
        controller.mediaTypes = mediaTypes;
        controller.delegate = self;
        [self presentViewController:controller
                           animated:YES
                         completion:^(void){
                             DebugLog(@"Picker View Controller is presented");
                         }];
    }
}

-(void)gallery_func
{
    DebugLog(@"gallery func");
    if ([self isPhotoLibraryAvailable]) {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        NSMutableArray *mediaTypes = [[NSMutableArray alloc] init];
        [mediaTypes addObject:(__bridge NSString *)kUTTypeImage];
        controller.mediaTypes = mediaTypes;
        controller.delegate = self;
        [self presentViewController:controller
                           animated:YES
                         completion:^(void){
                             DebugLog(@"Picker View Controller is presented");
                         }];
    }
    
}

- (void)imageCropper:(VPImageCropperViewController *)cropperViewController didFinished:(UIImage *)editedImage {
    prof_img.image = editedImage;
    portraitImg1 =editedImage;
    prof_img.contentMode=UIViewContentModeScaleAspectFill;
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
        // TO DO
    }];
    
    //    NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(editedImage)];
    //    //    NSData *plainData = [plainString dataUsingEncoding:NSUTF8StringEncoding];
    //
    //    UIImage* image = [UIImage imageWithData:imageData];
    //
    //    base64String = [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:0];
    //
    ////    base64String = [imageData base64EncodedStringWithOptions:0];
    //    DebugLog(@"%@", base64String); // Zm9
    //
    //    NSString *encodedText = [self encodeToPercentEscapeString:base64String];
    //    DebugLog(@"Encoded text: %@", encodedText);
    //
    //    //    NSString *strImageData = [base64String stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    //
    //    [[NSUserDefaults standardUserDefaults]setObject:encodedText forKey:@"r_prof_image"];
    
    NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(editedImage)];
    //    NSData *plainData = [plainString dataUsingEncoding:NSUTF8StringEncoding];
    base64String = [imageData base64EncodedStringWithOptions:0];
    //    DebugLog(@"%@", base64String); // Zm9
    DebugLog(@"Encoded text: %@", base64String);
    
    //    NSString *encodedText = [self encodeToPercentEscapeString:base64String];
    
    //    NSString *strImageData = [base64String stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    
   // [[NSUserDefaults standardUserDefaults]setObject:base64String forKey:@"r_prof_image"];
    
    
    picuploaded=1;
}

- (void)imageCropperDidCancel:(VPImageCropperViewController *)cropperViewController {
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^() {
        
        UIImage *portraitImg = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        portraitImg = [self imageByScalingToMaxSize:portraitImg];
        
        pathss = [NSTemporaryDirectory() stringByAppendingPathComponent:@"image.png"];
        NSData *imageData = UIImagePNGRepresentation(portraitImg);
        //you can also use UIImageJPEGRepresentation(img,1); for jpegs
        [imageData writeToFile:pathss atomically:YES];
        
        // present the cropper view controller
        VPImageCropperViewController *imgCropperVC = [[VPImageCropperViewController alloc] initWithImage:portraitImg cropFrame:CGRectMake(35, (self.view.frame.size.height- (self.view.frame.size.width-70))/2, self.view.frame.size.width-70, self.view.frame.size.width-70) limitScaleRatio:3.0];
        imgCropperVC.delegate = self;
        [self presentViewController:imgCropperVC animated:YES completion:^{
            // TO DO
        }];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:^(){
    }];
}

#pragma mark image scale utility
//- (UIImage *)imageByScalingToMaxSize:(UIImage *)sourceImage {
//    if (sourceImage.size.width < ORIGINAL_MAX_WIDTH) return sourceImage;
//    CGFloat btWidth = 0.0f;
//    CGFloat btHeight = 0.0f;
//    if (sourceImage.size.width > sourceImage.size.height) {
//        btHeight = ORIGINAL_MAX_WIDTH;
//        btWidth = sourceImage.size.width * (ORIGINAL_MAX_WIDTH / sourceImage.size.height);
//    } else {
//        btWidth = ORIGINAL_MAX_WIDTH;
//        btHeight = sourceImage.size.height * (ORIGINAL_MAX_WIDTH / sourceImage.size.width);
//    }
//    CGSize targetSize = CGSizeMake(btWidth, btHeight);
//    return [self imageByScalingAndCroppingForSourceImage:sourceImage targetSize:targetSize];
//}

- (UIImage *)imageByScalingAndCroppingForSourceImage:(UIImage *)sourceImage targetSize:(CGSize)targetSize {
    DebugLog(@"called scaling&croppingforsize");
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    if (CGSizeEqualToSize(imageSize, targetSize) == NO)
    {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor > heightFactor)
            scaleFactor = widthFactor; // scale to fit height
        else
            scaleFactor = heightFactor; // scale to fit width
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else
            if (widthFactor < heightFactor)
            {
                thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
            }
    }
    UIGraphicsBeginImageContext(targetSize); // this will crop
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil) DebugLog(@"could not scale image");
    
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - UINavigationControllerDelegate
//- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
//}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
}

#pragma mark camera utility
- (BOOL) isCameraAvailable{
    return [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
}

- (BOOL) isRearCameraAvailable{
    return [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear];
}

- (BOOL) isFrontCameraAvailable {
    return [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront];
}

- (BOOL) doesCameraSupportTakingPhotos {
    return [self cameraSupportsMedia:(__bridge NSString *)kUTTypeImage sourceType:UIImagePickerControllerSourceTypeCamera];
}

- (BOOL) isPhotoLibraryAvailable{
    return [UIImagePickerController isSourceTypeAvailable:
            UIImagePickerControllerSourceTypePhotoLibrary];
}
- (BOOL) canUserPickVideosFromPhotoLibrary{
    return [self
            cameraSupportsMedia:(__bridge NSString *)kUTTypeMovie sourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}
- (BOOL) canUserPickPhotosFromPhotoLibrary{
    return [self
            cameraSupportsMedia:(__bridge NSString *)kUTTypeImage sourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}

- (BOOL) cameraSupportsMedia:(NSString *)paramMediaType sourceType:(UIImagePickerControllerSourceType)paramSourceType{
    __block BOOL result = NO;
    if ([paramMediaType length] == 0) {
        return NO;
    }
    NSArray *availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:paramSourceType];
    [availableMediaTypes enumerateObjectsUsingBlock: ^(id obj, NSUInteger idx, BOOL *stop) {
        NSString *mediaType = (NSString *)obj;
        if ([mediaType isEqualToString:paramMediaType]){
            result = YES;
            *stop= YES;
        }
    }];
    return result;
}

-(NSString*) encodeToPercentEscapeString:(NSString *)string {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                              (CFStringRef) string,
                                                              NULL,
                                                              (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
}

-(void)changeprofpic: (UIGestureRecognizer *)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose Your Action"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Open Camera", @"Choose From Gallery",nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [actionSheet showInView:mainview];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
            
        case 0:
        {
            DebugLog(@"here camera");
            [self camera_func];
        }
            break;
            
        case 1:
        {
            [self gallery_func];
        }
            break;
    }
}
-(void)viewDidDisappear:(BOOL)animated
{
    //    [[NSUserDefaults standardUserDefaults]setObject:@"Y" forKey:@"editpicdisappear"];
    [super viewDidDisappear:YES];
}
-(void)gopersonal:(id)sender
{
    downbutton.hidden=NO;
    downbtphone.hidden=NO;
    business=0;
    [businessview setHidden:YES];
    //    [self loadViews];
    [personalbt setBackgroundImage:[UIImage imageNamed:@"personal-ac.png"] forState:UIControlStateNormal];
    [businessbt setBackgroundImage:[UIImage imageNamed:@"businessdark.png"] forState:UIControlStateNormal];
    //    countrycodetable=[[UITableView alloc]initWithFrame:CGRectMake(5, 20, (self.view.bounds.size.width)-10,(self.view.bounds.size.height)-30)];
    countrycodetable.frame= CGRectMake(0, 64, [UIScreen mainScreen].bounds.size.width, self.view.bounds.size.height-64);
    countrycodetable.hidden=YES;
}

-(void)gobusiness:(id)sender
{
    business=1;
    [businessview setHidden:NO];
    [downbutton setHidden:YES];
    [downbtphone setHidden:YES];
    [personalbt setBackgroundImage:[UIImage imageNamed:@"personaldark.png"] forState:UIControlStateNormal];
    [businessbt setBackgroundImage:[UIImage imageNamed:@"business-acset.png"] forState:UIControlStateNormal];
    //    [self loadViews];
    //    countrycodetable=[[UITableView alloc]initWithFrame:CGRectMake(5, 20, (self.view.bounds.size.width)-10,(self.view.bounds.size.height)-30)];
    countrycodetable.frame= CGRectMake(0, 64, [UIScreen mainScreen].bounds.size.width, self.view.bounds.size.height-64);
    countrycodetable.hidden=YES;
    
}


-(void)editpic
{
    prefs = [NSUserDefaults standardUserDefaults];
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=editpicture&access_token=%@&device_id=%@",[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
    
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    
    [request setHTTPShouldHandleCookies:NO];
    
    [request setURL:[NSURL URLWithString:newString1]];
    
    [request setTimeoutInterval:10];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *imageData = UIImagePNGRepresentation(portraitImg1);
    NSString *base64Stringa = [imageData base64EncodedStringWithOptions:0];
    
    if ( [imageData length] > 0)
        //   if (pic_send ==1)
    {
        DebugLog(@"portrait img data is not nil");
        //if ([base64Stringa length] > 6)
        //        {
        //            NSData *decodedData1 = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
        //            //    NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
        //
        //            UIImage *profilepic1 = [UIImage imageWithData:decodedData1];
        //
        //            prof_img.image=profilepic1;
        //            prof_img.contentMode= UIViewContentModeScaleAspectFill;
        //            prof_img.clipsToBounds=YES;
        //        }
        
        //[profdict objectForKey:@"image"];
        
        //        NSString *boundary = [NSString stringWithFormat:@"%0.9u",arc4random()];
        //
        //        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        //
        //        [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
        //
        //        NSMutableData *body = [NSMutableData data];
        //
        //        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        //
        //        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image\"; filename=\".png\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        //
        //        [body appendData:[[NSString stringWithFormat:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        //
        //        [body appendData:[NSData dataWithData:UIImagePNGRepresentation(portraitImg1)]];
        //
        //        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        //
        //        [request setHTTPBody:body];
        
        NSString *params = [[NSString alloc] initWithFormat:@"image=%@",[base64Stringa stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"]];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        
        NSURLResponse *response = nil;
        NSError *error= nil;
        NSData *signeddataURL1 = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                  options:kNilOptions
                                                                    error:&error];
        DebugLog(@"deny json returns: %@",json_deny);
        
        if ([[json_deny objectForKey:@"success"]intValue] == 1)
        {
            
            prof_img.image = portraitImg1;
            
            [profdict setObject:[NSString stringWithFormat:@"%@",base64Stringa] forKey:@"image"];
            
            [[NSUserDefaults standardUserDefaults] setObject:profdict forKey:@"loadedProfDict"];
            //            if ([base64Stringa length] > 6)
            //            {
            //                NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
            //                //    NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
            //
            //                UIImage *profilepic = [UIImage imageWithData:decodedData];
            //
            //                prof_img.image=profilepic;
            //                prof_img.contentMode= UIViewContentModeScaleAspectFill;
            //                prof_img.clipsToBounds=YES;
            //            }
            //
            
            ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
            con.other=@"no";
            con.uid=[prefs objectForKey:@"id"];
            
//            CATransition* transition = [CATransition animation];
//            
//            transition.duration = 0.4;
//            transition.type = kCATransitionPush;
//            transition.subtype = kCATransitionFade;
//            
//            [[self navigationController].view.layer addAnimation:transition forKey:nil];
            
            [self.navigationController pushViewController:con animated:YES];
        }
    }
    else
    {
        ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
        con.other=@"no";
        con.uid=[prefs objectForKey:@"id"];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
}


#pragma mark image scale utility
- (UIImage *)imageByScalingToMaxSize:(UIImage *)sourceImage {
    if (sourceImage.size.height < ORIGINAL_MAX_HEIGHT) return sourceImage;
    CGFloat btWidth = 0.0f;
    CGFloat btHeight = 0.0f;
    //    if (sourceImage.size.height > sourceImage.size.width) {
    //        btHeight = ORIGINAL_MAX_HEIGHT;
    //        btWidth = sourceImage.size.height * (ORIGINAL_MAX_HEIGHT / sourceImage.size.width);
    //    } else {
    //        btHeight = ORIGINAL_MAX_HEIGHT;
    //        btWidth = sourceImage.size.width * (ORIGINAL_MAX_HEIGHT / sourceImage.size.height);
    //    }
    if (sourceImage.size.height > sourceImage.size.width) {
        btHeight = ORIGINAL_MAX_HEIGHT;
        btWidth = sourceImage.size.width * (ORIGINAL_MAX_HEIGHT / sourceImage.size.height);
    } else {
        btHeight = ORIGINAL_MAX_HEIGHT;
        btWidth = sourceImage.size.width * (ORIGINAL_MAX_HEIGHT / sourceImage.size.height);
    }
    
    CGSize targetSize = CGSizeMake(btWidth, btHeight);
    return [self imageByScalingAndCroppingForSourceImage:sourceImage targetSize:targetSize];
}

- (void)pageDesign
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
    }];
    prof_img = [[UIImageView alloc]initWithFrame:CGRectMake(15, 10, [UIScreen mainScreen].bounds.size.width/3.55, 90)];
    [prof_img setImage:[UIImage imageNamed:@"profileImage"]];
    [scviewbudget addSubview:prof_img];
    
    //    base64String= [profdict objectForKey:@"image"];
    //    if ([base64String length] >6)
    //    {
    //        NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
    //        NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
    //        UIImage *profilepic = [UIImage imageWithData:decodedData];
    //prof_img.image=[UIImage imageNamed:@"profile-pic.png"];
    prof_img.contentMode= UIViewContentModeScaleAspectFill;
    prof_img.clipsToBounds=YES;
    prof_img.userInteractionEnabled=YES;
    UITapGestureRecognizer *tapprofpic = [[UITapGestureRecognizer alloc]
                                          initWithTarget:self
                                          action:@selector(changeprofpic:)];
    
    [prof_img addGestureRecognizer:tapprofpic];
    //    }
    
    UIImageView *viewimg=[[UIImageView alloc]initWithFrame:CGRectMake(prof_img.frame.origin.x+prof_img.frame.size.width+10, 9, ([UIScreen mainScreen].bounds.size.width-15)-(prof_img.frame.origin.x+prof_img.frame.size.width+10), 41)];
    
    viewimg.image=[UIImage imageNamed:@"input-filed1.png"];
    
    [scviewbudget addSubview:viewimg];
    viewimg.userInteractionEnabled=YES;
    
    firstname= [[UITextField alloc]initWithFrame:CGRectMake(12, 4, 167, 37)];
    [viewimg addSubview:firstname];
    firstname.delegate=self;
    firstname.backgroundColor=[UIColor clearColor];
    firstname.placeholder=@"First Name";
    firstname.text=@"";  //Bhaswar
    firstname.textColor=[UIColor grayColor];
    firstname.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
    firstname.tintColor=[UIColor grayColor];
    firstname.keyboardType=UIKeyboardTypeDefault;
    firstname.keyboardAppearance=UIKeyboardAppearanceDark;
    
    UIImageView *viewimg1=[[UIImageView alloc]initWithFrame:CGRectMake(prof_img.frame.origin.x+prof_img.frame.size.width+10, 64, ([UIScreen mainScreen].bounds.size.width-15)-(prof_img.frame.origin.x+prof_img.frame.size.width+10), 41)];
    viewimg1.image=[UIImage imageNamed:@"input-filed1.png"];
    [scviewbudget addSubview:viewimg1];
    viewimg1.userInteractionEnabled=YES;
    
    lastname= [[UITextField alloc]initWithFrame:CGRectMake(12, 4, 167, 37)];
    [viewimg1 addSubview:lastname];
    lastname.delegate=self;
    lastname.backgroundColor=[UIColor clearColor];
    lastname.placeholder=@"Last Name";
    lastname.text=@"";  //Mukherjee
    lastname.textColor=[UIColor grayColor];
    lastname.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
    lastname.tintColor=[UIColor grayColor];
    lastname.keyboardType=UIKeyboardTypeDefault;
    lastname.keyboardAppearance=UIKeyboardAppearanceDark;
    
    personalbt = [UIButton buttonWithType:UIButtonTypeCustom];
    personalbt.frame = CGRectMake(15, 117, [UIScreen mainScreen].bounds.size.width/2-19, 30);
    [personalbt setBackgroundImage:[UIImage imageNamed:@"personal-ac.png"] forState:UIControlStateNormal];
    [personalbt addTarget:self action:@selector(gopersonal:) forControlEvents:UIControlEventTouchUpInside];
    [scviewbudget addSubview:personalbt];
    
    businessbt = [UIButton buttonWithType:UIButtonTypeCustom];
    businessbt.frame = CGRectMake([UIScreen mainScreen].bounds.size.width/2+4, 117, [UIScreen mainScreen].bounds.size.width/2-19, 30);
    [businessbt setBackgroundImage:[UIImage imageNamed:@"businessdark.png"] forState:UIControlStateNormal];
    [businessbt addTarget:self action:@selector(gobusiness:) forControlEvents:UIControlEventTouchUpInside];
    [scviewbudget addSubview:businessbt];
    
    
    for (int i=0; i<4; i++)
    {
        UIImageView *longtexts=[[UIImageView alloc]initWithFrame:CGRectMake(15, 160+ (i*53), [UIScreen mainScreen].bounds.size.width-30, 42.5f)];
        
        longtexts.image=[UIImage imageNamed:@"input filed2.png"];
        
        [scviewbudget addSubview:longtexts];
    }
    
    NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"countries" ofType:@"json"]];
    NSError *localError = nil;
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
    
    if (localError != nil) {
    }
    countriesList = (NSArray *)parsedObject;
    
    NSSortDescriptor *sort1 = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    
    countriesList = [countriesList sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort1]];
    
    for (check=0; check < countriesList.count; check++) {
        
        if ([[[countriesList objectAtIndex:check]objectForKey:@"code"] isEqualToString: [[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkISOCode"]]) {
            dialCode = [NSString stringWithFormat:@"%@",[[countriesList objectAtIndex:check]objectForKey:@"dial_code"]];
        }
    }
    
    email= [[UITextField alloc]initWithFrame:CGRectMake(25, 271, [UIScreen mainScreen].bounds.size.width-60, 37)];
    [scviewbudget addSubview:email];
    email.delegate=self;
    email.backgroundColor=[UIColor clearColor];
    email.placeholder=@"Email";
    email.text=@"";  //bhaswar.mukherjee@esolzmail.com
    email.textColor=[UIColor grayColor];
    email.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
    email.tintColor=[UIColor grayColor];
    email.keyboardType=UIKeyboardTypeEmailAddress;
    email.keyboardAppearance=UIKeyboardAppearanceDark;
    
    websitetext= [[UITextField alloc]initWithFrame:CGRectMake(25, 324, [UIScreen mainScreen].bounds.size.width-60, 37)];
    [scviewbudget addSubview:websitetext];
    websitetext.delegate=self;
    websitetext.backgroundColor=[UIColor clearColor];
    websitetext.placeholder=@"Website";
    websitetext.text=@"";  //bhaswar.mukherjee@esolzmail.com
    websitetext.textColor=[UIColor grayColor];
    websitetext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
    websitetext.tintColor=[UIColor grayColor];
    websitetext.keyboardType=UIKeyboardTypeEmailAddress;
    websitetext.keyboardAppearance=UIKeyboardAppearanceDark;
    
    countrycode = [[UILabel alloc]initWithFrame:CGRectMake(21, 164, 30, 35)];
    [scviewbudget addSubview:countrycode];
    countrycode.backgroundColor=[UIColor clearColor];
    countrycode.text=@"";   //[[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkISOCode"];           DebugLog(@"ISO PERSONAL==========>%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkISOCode"]);//@"AF";
    countrycode.textColor=[UIColor blackColor];
    countrycode.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
    countrycode.textAlignment=NSTextAlignmentCenter;
    
    downbutton = [UIButton buttonWithType:UIButtonTypeCustom];
    downbutton.frame = CGRectMake(51, 171, 22, 20);
    [downbutton setBackgroundImage:[UIImage imageNamed:@"dnwarr.png"] forState:UIControlStateNormal];
    downbutton.backgroundColor=[UIColor clearColor];
    [downbutton addTarget:self action:@selector(showtable:) forControlEvents:UIControlEventTouchUpInside];
    [scviewbudget addSubview:downbutton];
    
    isdlabel = [[UILabel alloc]initWithFrame:CGRectMake(downbutton.frame.origin.x+downbutton.frame.size.width+2, 164, 50, 35)];
    [scviewbudget addSubview:isdlabel];
    isdlabel.backgroundColor=[UIColor clearColor];
    isdlabel.text=@"";  //[NSString stringWithFormat:@"%@",dialCode];           DebugLog(@"DIAL CODE PERSONAL==========>%@",[NSString stringWithFormat:@"%@",dialCode]);//@"+93";
    isdlabel.textColor=[UIColor blackColor];
    isdlabel.font=[UIFont fontWithName:@"ProximaNova-Regular" size:14];
    isdlabel.textAlignment=NSTextAlignmentCenter;
    
    yourmobile= [[UITextField alloc]initWithFrame:CGRectMake(120, 164, [UIScreen mainScreen].bounds.size.width-60, 37)];
    [scviewbudget addSubview:yourmobile];
    yourmobile.delegate=self;
    yourmobile.backgroundColor=[UIColor clearColor];
    yourmobile.placeholder=@"Your Mobile Phone";
    yourmobile.text=@"";  //9999999999
    yourmobile.textColor=[UIColor grayColor];
    yourmobile.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
    yourmobile.tintColor=[UIColor grayColor];
    yourmobile.keyboardType=UIKeyboardTypeNumbersAndPunctuation;
    yourmobile.keyboardAppearance=UIKeyboardAppearanceDark;
    
    countryphone = [[UILabel alloc]initWithFrame:CGRectMake(21, 217, 30, 35)];
    [scviewbudget addSubview:countryphone];
    countryphone.backgroundColor=[UIColor clearColor];
    countryphone.text=@"";   //[[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkISOCode"];//@"AF";
    countryphone.textColor=[UIColor blackColor];
    countryphone.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
    countryphone.textAlignment=NSTextAlignmentCenter;
    
    downbtphone = [UIButton buttonWithType:UIButtonTypeCustom];
    downbtphone.frame = CGRectMake(51, 224, 22, 20);
    [downbtphone setBackgroundImage:[UIImage imageNamed:@"dnwarr.png"] forState:UIControlStateNormal];
    downbtphone.backgroundColor=[UIColor clearColor];
    [downbtphone addTarget:self action:@selector(showtablephone:) forControlEvents:UIControlEventTouchUpInside];
    [scviewbudget addSubview:downbtphone];
    
    //    DebugLog(@"DIAL CODE PERSONAL 111111==========>%@",[NSString stringWithFormat:@"%@",dialCode]);
    
    isdphone = [[UILabel alloc]initWithFrame:CGRectMake(70, 217, 50, 35)];
    [scviewbudget addSubview:isdphone];
    isdphone.backgroundColor=[UIColor clearColor];
    isdphone.text=@"";     //[NSString stringWithFormat:@"%@",dialCode];//@"+93";
    isdphone.textColor=[UIColor blackColor];
    isdphone.font=[UIFont fontWithName:@"ProximaNova-Regular" size:14];
    isdphone.textAlignment=NSTextAlignmentCenter;
    
    yourphone= [[UITextField alloc]initWithFrame:CGRectMake(120, 217, [UIScreen mainScreen].bounds.size.width/1.97, 37)];
    [scviewbudget addSubview:yourphone];
    yourphone.delegate=self;
    yourphone.backgroundColor=[UIColor clearColor];
    yourphone.placeholder=@"Your Phone Number";
    yourphone.text=@"";  //9999999999
    yourphone.textColor=[UIColor grayColor];
    yourphone.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
    yourphone.tintColor=[UIColor grayColor];
    yourphone.keyboardType=UIKeyboardTypeNumbersAndPunctuation;
    yourphone.keyboardAppearance=UIKeyboardAppearanceDark;
    
    
    countryphonetable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - 64)];
    [self.view addSubview:countryphonetable];
    [self.view bringSubviewToFront:countryphonetable];
    countryphonetable.backgroundColor=[UIColor whiteColor];
    countryphonetable.dataSource=self;
    countryphonetable.delegate=self;
    countryphonetable.separatorStyle=UITableViewCellSeparatorStyleNone;
    countryphonetable.hidden=YES;
    countryphonetable.showsVerticalScrollIndicator=NO;
    countryphonetable.layer.borderColor=[[UIColor grayColor]CGColor];
    countryphonetable.layer.borderWidth=1;
    countryphonetable.layer.zPosition=1;
    
    countryflagimg=[[UIImageView alloc]initWithFrame:CGRectMake(15, 373, [UIScreen mainScreen].bounds.size.width-30, 42.5f)];
    
    countryflagimg.image=[UIImage imageNamed:@"input-filed.png"];
    
    [scviewbudget addSubview:countryflagimg];
    countryflagimg.userInteractionEnabled=YES;
    
    
    UIImageView *zipimage=[[UIImageView alloc]initWithFrame:CGRectMake(15, 427, [UIScreen mainScreen].bounds.size.width/3.1, 42.5f)];
    
    zipimage.image=[UIImage imageNamed:@"zip-input-field.png"];
    
    [scviewbudget addSubview:zipimage];
    zipimage.userInteractionEnabled=YES;
    
    ziptext= [[UITextField alloc]initWithFrame:CGRectMake(0, 4, [UIScreen mainScreen].bounds.size.width/3.1, 37)];
    [zipimage addSubview:ziptext];
    ziptext.delegate=self;
    ziptext.backgroundColor=[UIColor clearColor];
    ziptext.placeholder=@"Zip Code";
    ziptext.text=@"";  //121212
    ziptext.textColor=[UIColor grayColor];
    ziptext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    ziptext.tintColor=[UIColor grayColor];
    ziptext.keyboardType=UIKeyboardTypeNumbersAndPunctuation;
    ziptext.textAlignment=NSTextAlignmentCenter;
    ziptext.keyboardAppearance=UIKeyboardAppearanceDark;
    
    UIImageView *cityimage=[[UIImageView alloc]initWithFrame:CGRectMake(zipimage.frame.origin.x+zipimage.frame.size.width+10, 427, ([UIScreen mainScreen].bounds.size.width-15)-(zipimage.frame.origin.x+zipimage.frame.size.width+10), 42.5f)];
    
    cityimage.image=[UIImage imageNamed:@"country-input-field.png"];
    
    [scviewbudget addSubview:cityimage];
    cityimage.userInteractionEnabled=YES;
    
    citytext= [[UITextField alloc]initWithFrame:CGRectMake(10, 4, [UIScreen mainScreen].bounds.size.width/1.9, 37)];
    [cityimage addSubview:citytext];
    citytext.delegate=self;
    citytext.backgroundColor=[UIColor clearColor];
    citytext.placeholder=@"City";
    citytext.text=@"";  //Kolkata
    citytext.textColor=[UIColor grayColor];
    citytext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    citytext.tintColor=[UIColor grayColor];
    citytext.keyboardType=UIKeyboardTypeDefault;
    citytext.keyboardAppearance=UIKeyboardAppearanceDark;
    
    
    UIImageView *streetimage=[[UIImageView alloc]initWithFrame:CGRectMake(15, 480, [UIScreen mainScreen].bounds.size.width/1.76, 42.5f)];
    streetimage.image=[UIImage imageNamed:@"country-input-field.png"];
    [scviewbudget addSubview:streetimage];
    streetimage.userInteractionEnabled=YES;
    
    streettext= [[UITextField alloc]initWithFrame:CGRectMake(10, 4, [UIScreen mainScreen].bounds.size.width/1.93, 37)];
    [streetimage addSubview:streettext];
    streettext.delegate=self;
    streettext.backgroundColor=[UIColor clearColor];
    streettext.placeholder=@"Street";
    streettext.text=@"";                    //Demo Street
    streettext.textColor=[UIColor grayColor];
    streettext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    streettext.tintColor=[UIColor grayColor];
    streettext.keyboardType=UIKeyboardTypeDefault;
    streettext.keyboardAppearance=UIKeyboardAppearanceDark;
    
    
    UIImageView *numberimage=[[UIImageView alloc]initWithFrame:CGRectMake(streetimage.frame.origin.x+streetimage.frame.size.width+10, 480,  (cityimage.frame.origin.x+cityimage.frame.size.width)-(streetimage.frame.origin.x+streetimage.frame.size.width+10), 42.5f)];
    
    numberimage.image=[UIImage imageNamed:@"zip-input-field.png"];
    
    [scviewbudget addSubview:numberimage];
    numberimage.userInteractionEnabled=YES;
    
    
    numbertext= [[UITextField alloc]initWithFrame:CGRectMake(7, 4, [UIScreen mainScreen].bounds.size.width/3.55, 37)];
    [numberimage addSubview:numbertext];
    numbertext.delegate=self;
    numbertext.backgroundColor=[UIColor clearColor];
    numbertext.placeholder=@"Number";
    numbertext.text=@"";                            //9999999998
    numbertext.textColor=[UIColor grayColor];
    numbertext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:15.5f];
    numbertext.tintColor=[UIColor grayColor];
    numbertext.keyboardType=UIKeyboardTypeNumbersAndPunctuation;
    numbertext.keyboardAppearance=UIKeyboardAppearanceDark;
    
    UIButton *nextbt = [UIButton buttonWithType:UIButtonTypeCustom];
    nextbt.frame = CGRectMake(15, 533, [UIScreen mainScreen].bounds.size.width-30, 45);
    nextbt.layer.cornerRadius=5;
    [nextbt setBackgroundImage:[UIImage imageNamed:@"save-button-ac.png"] forState:UIControlStateNormal];
    nextbt.backgroundColor=[UIColor clearColor];
    [nextbt addTarget:self action:@selector(gotonext:) forControlEvents:UIControlEventTouchUpInside];
    [scviewbudget addSubview:nextbt];
    
    countryimg=[[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 35, 22)];
    
    countryimg.image=[UIImage imageNamed:@"afghanistan_128.png"];
    
    [countryflagimg addSubview:countryimg];
    countryimg.userInteractionEnabled=YES;
    
    countrylabel = [[UILabel alloc]initWithFrame:CGRectMake(60, 12, 225, 22)];
    [countryflagimg addSubview:countrylabel];
    countrylabel.backgroundColor=[UIColor clearColor];
    countrylabel.text=@"Afghanistan";
    countrylabel.textColor=[UIColor grayColor];
    countrylabel.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
    
    NSData *dataflags = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"countriesflags" ofType:@"json"]];
    NSError *localErrorflags = nil;
    NSDictionary *parsedObjectflags = [NSJSONSerialization JSONObjectWithData:dataflags options:0 error:&localErrorflags];
    
    if (localError != nil) {
        DebugLog(@"%@", [localError userInfo]);
    }
    countriesFlags = (NSArray *)parsedObjectflags;
    
    NSSortDescriptor *sort11 = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    
    countriesFlags = [countriesFlags sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort11]];
    DebugLog(@"FLAGS======================>%@",countriesFlags);
    
    for (check=0; check < countriesFlags.count; check++){
        
        if ([[[countriesFlags objectAtIndex:check]objectForKey:@"code2l"] isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"placemarkISOCode"]]) {
            
            flagCode = [[countriesFlags objectAtIndex:check]objectForKey:@"flag_128"];
        }
        //        DebugLog(@"FLAG_CODE===============>%@",flagCode);
    }
    
    /////////////////////////////// Business View ////////////////////////////////////
    
    businessview = [[UIView alloc]initWithFrame:CGRectMake(0, 158, [UIScreen mainScreen].bounds.size.width, 370)];
    businessview.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
    [scviewbudget addSubview:businessview];
    businessview.layer.zPosition=3;
    if (business==0)
        businessview.hidden=YES;
    
    UIImageView *bcompany=[[UIImageView alloc]initWithFrame:CGRectMake(15, 0, personalbt.frame.size.width, 42)];
    
    bcompany.image=[UIImage imageNamed:@"input filed2.png"];
    bcompany.layer.zPosition=2;
    bcompany.userInteractionEnabled=YES;
    [businessview addSubview:bcompany];
    
    bcompanytext= [[UITextField alloc]initWithFrame:CGRectMake(8, 4, 130, 37)];
    [bcompany addSubview:bcompanytext];
    bcompanytext.delegate=self;
    bcompanytext.backgroundColor=[UIColor clearColor];
    bcompanytext.placeholder=@"Company";
    bcompanytext.text=@"";  //bhaswar.mukherjee@esolzmail.com
    bcompanytext.textColor=[UIColor grayColor];
    bcompanytext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
    bcompanytext.tintColor=[UIColor grayColor];
    bcompanytext.keyboardAppearance=UIKeyboardAppearanceDark;
    
    UIImageView *bdesig=[[UIImageView alloc]initWithFrame:CGRectMake(businessbt.frame.origin.x, 0, businessbt.frame.size.width, 42)];
    
    bdesig.image=[UIImage imageNamed:@"input filed2.png"];
    bdesig.userInteractionEnabled=YES;
    bdesig.layer.zPosition=2;
    [businessview addSubview:bdesig];
    
    bdesigtext= [[UITextField alloc]initWithFrame:CGRectMake(8, 4, 130, 37)];
    [bdesig addSubview:bdesigtext];
    bdesigtext.delegate=self;
    bdesigtext.backgroundColor=[UIColor clearColor];
    bdesigtext.placeholder=@"Designation";
    bdesigtext.text=@"";  //bhaswar.mukherjee@esolzmail.com
    bdesigtext.textColor=[UIColor grayColor];
    bdesigtext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
    bdesigtext.tintColor=[UIColor grayColor];
    bdesigtext.keyboardAppearance=UIKeyboardAppearanceDark;
    
    for (int i=0; i<3; i++)
    {
        UIImageView *longtexts=[[UIImageView alloc]initWithFrame:CGRectMake(15, 50+ (i*53), [UIScreen mainScreen].bounds.size.width-30, 42.5f)];
        
        longtexts.image=[UIImage imageNamed:@"input filed2.png"];
        
        [businessview addSubview:longtexts];
    }
    
    bemailtext= [[UITextField alloc]initWithFrame:CGRectMake(25, 55, [UIScreen mainScreen].bounds.size.width-50, 37)];
    [businessview addSubview:bemailtext];
    bemailtext.delegate=self;
    bemailtext.backgroundColor=[UIColor clearColor];
    bemailtext.placeholder=@"Email";
    bemailtext.text=@"";  //bhaswar.mukherjee@esolzmail.com
    bemailtext.textColor=[UIColor grayColor];
    bemailtext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
    bemailtext.tintColor=[UIColor grayColor];
    bemailtext.keyboardType=UIKeyboardTypeEmailAddress;
    bemailtext.keyboardAppearance=UIKeyboardAppearanceDark;
    
    bwebsitetext= [[UITextField alloc]initWithFrame:CGRectMake(25, 110, [UIScreen mainScreen].bounds.size.width-60, 37)];
    [businessview addSubview:bwebsitetext];
    bwebsitetext.delegate=self;
    bwebsitetext.backgroundColor=[UIColor clearColor];
    bwebsitetext.placeholder=@"Website";
    bwebsitetext.text=@"";  //bhaswar.mukherjee@esolzmail.com
    bwebsitetext.textColor=[UIColor grayColor];
    bwebsitetext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
    bwebsitetext.tintColor=[UIColor grayColor];
    bwebsitetext.keyboardType=UIKeyboardTypeEmailAddress;
    bwebsitetext.keyboardAppearance=UIKeyboardAppearanceDark;
    
    bcountrycode = [[UILabel alloc]initWithFrame:CGRectMake(21, 162, 30, 35)];
    [businessview addSubview:bcountrycode];
    bcountrycode.backgroundColor=[UIColor clearColor];
    bcountrycode.text=@"";  //[[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkISOCode"];//@"AF";
    bcountrycode.textColor=[UIColor blackColor];
    bcountrycode.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
    bcountrycode.textAlignment=NSTextAlignmentCenter;
    
    downbtbusiness = [UIButton buttonWithType:UIButtonTypeCustom];
    downbtbusiness.frame = CGRectMake(51, 168, 22, 20);
    [downbtbusiness setBackgroundImage:[UIImage imageNamed:@"dnwarr.png"] forState:UIControlStateNormal];
    downbtbusiness.backgroundColor=[UIColor clearColor];
    [downbtbusiness addTarget:self action:@selector(showtable:) forControlEvents:UIControlEventTouchUpInside];
    [businessview addSubview:downbtbusiness];
    
    bisdlabel = [[UILabel alloc]initWithFrame:CGRectMake(downbutton.frame.origin.x+downbutton.frame.size.width+2, 162, 50, 35)];
    bisdlabel.backgroundColor=[UIColor clearColor];
    bisdlabel.text=@""; //[NSString stringWithFormat:@"%@",dialCode];    DebugLog(@"DIAL CODE BUSINESS==========>%@",[NSString stringWithFormat:@"%@",dialCode]);//@"+93";
    bisdlabel.textColor=[UIColor blackColor];
    bisdlabel.font=[UIFont fontWithName:@"ProximaNova-Regular" size:14];
    bisdlabel.textAlignment=NSTextAlignmentLeft;
    [businessview addSubview:bisdlabel];
    
    
    bphonetext= [[UITextField alloc]initWithFrame:CGRectMake(120, 162, [UIScreen mainScreen].bounds.size.width/1.97, 37)];
    [businessview addSubview:bphonetext];
    bphonetext.delegate=self;
    bphonetext.backgroundColor=[UIColor clearColor];
    bphonetext.placeholder=@"Your Business Phone";
    bphonetext.text=@"";  //9999999999
    bphonetext.textColor=[UIColor grayColor];
    bphonetext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
    bphonetext.tintColor=[UIColor grayColor];
    bphonetext.keyboardType=UIKeyboardTypeNumbersAndPunctuation;
    bphonetext.keyboardAppearance=UIKeyboardAppearanceDark;
    
    
    
    bcountryflagimg=[[UIImageView alloc]initWithFrame:CGRectMake(15, 210, [UIScreen mainScreen].bounds.size.width-30, 42.5f)];
    bcountryflagimg.image=[UIImage imageNamed:@"input-filed.png"];
    
    [businessview addSubview:bcountryflagimg];
    bcountryflagimg.userInteractionEnabled=YES;
    
    
    UIImageView *bzipimage=[[UIImageView alloc]initWithFrame:CGRectMake(15, 264, [UIScreen mainScreen].bounds.size.width/3.1, 42.5f)];
    
    bzipimage.image=[UIImage imageNamed:@"zip-input-field.png"];
    
    [businessview addSubview:bzipimage];
    bzipimage.userInteractionEnabled=YES;
    
    bziptext= [[UITextField alloc]initWithFrame:CGRectMake(0, 4, [UIScreen mainScreen].bounds.size.width/3.1, 37)];
    [bzipimage addSubview:bziptext];
    bziptext.delegate=self;
    bziptext.backgroundColor=[UIColor clearColor];
    bziptext.placeholder=@"Zip Code";
    bziptext.text=@"";  //121212
    bziptext.textColor=[UIColor grayColor];
    bziptext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    bziptext.tintColor=[UIColor grayColor];
    bziptext.keyboardType=UIKeyboardTypeNumbersAndPunctuation;
    bziptext.textAlignment=NSTextAlignmentCenter;
    bziptext.keyboardAppearance=UIKeyboardAppearanceDark;
    
    UIImageView *bcityimage=[[UIImageView alloc]initWithFrame:CGRectMake(bzipimage.frame.origin.x+bzipimage.frame.size.width+10, 264, ([UIScreen mainScreen].bounds.size.width-15)-(bzipimage.frame.origin.x+bzipimage.frame.size.width+10), 42.5f)];
    
    bcityimage.image=[UIImage imageNamed:@"country-input-field.png"];
    
    [businessview addSubview:bcityimage];
    bcityimage.userInteractionEnabled=YES;
    
    bcitytext= [[UITextField alloc]initWithFrame:CGRectMake(10, 4, 165, 37)];
    [bcityimage addSubview:bcitytext];
    bcitytext.delegate=self;
    bcitytext.backgroundColor=[UIColor clearColor];
    bcitytext.placeholder=@"City";
    bcitytext.text=@"";  //Kolkata
    bcitytext.textColor=[UIColor grayColor];
    bcitytext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    bcitytext.tintColor=[UIColor grayColor];
    bcitytext.keyboardType=UIKeyboardTypeDefault;
    bcitytext.keyboardAppearance=UIKeyboardAppearanceDark;
    
    
    UIImageView *bstreetimage=[[UIImageView alloc]initWithFrame:CGRectMake(15, 318, [UIScreen mainScreen].bounds.size.width/1.76, 42.5f)];
    bstreetimage.image=[UIImage imageNamed:@"country-input-field.png"];
    [businessview addSubview:bstreetimage];
    bstreetimage.userInteractionEnabled=YES;
    
    bstreettext= [[UITextField alloc]initWithFrame:CGRectMake(10, 4, [UIScreen mainScreen].bounds.size.width/1.93, 37)];
    [bstreetimage addSubview:bstreettext];
    bstreettext.delegate=self;
    bstreettext.backgroundColor=[UIColor clearColor];
    bstreettext.placeholder=@"Street";
    bstreettext.text=@"";                    //Demo Street
    bstreettext.textColor=[UIColor grayColor];
    bstreettext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    bstreettext.tintColor=[UIColor grayColor];
    bstreettext.keyboardType=UIKeyboardTypeDefault;
    bstreettext.keyboardAppearance=UIKeyboardAppearanceDark;
    
    
    UIImageView *bnumberimage=[[UIImageView alloc]initWithFrame:CGRectMake(streetimage.frame.origin.x+streetimage.frame.size.width+10, 318,  (bcityimage.frame.origin.x+bcityimage.frame.size.width)-(bstreetimage.frame.origin.x+bstreetimage.frame.size.width+10), 42.5f)];
    
    bnumberimage.image=[UIImage imageNamed:@"zip-input-field.png"];
    [businessview addSubview:bnumberimage];
    bnumberimage.userInteractionEnabled=YES;
    
    
    bnumbertext= [[UITextField alloc]initWithFrame:CGRectMake(7, 4, [UIScreen mainScreen].bounds.size.width/3.55, 37)];
    [bnumberimage addSubview:bnumbertext];
    bnumbertext.delegate=self;
    bnumbertext.backgroundColor=[UIColor clearColor];
    bnumbertext.placeholder=@"Number";
    bnumbertext.text=@"";                            //9999999998
    bnumbertext.textColor=[UIColor grayColor];
    bnumbertext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:15.5f];
    bnumbertext.tintColor=[UIColor grayColor];
    bnumbertext.keyboardType=UIKeyboardTypeNumbersAndPunctuation;
    bnumbertext.keyboardAppearance=UIKeyboardAppearanceDark;
    
    
    bcountryimg=[[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 35, 22)];
    
    bcountryimg.image=[UIImage imageNamed:@"afghanistan_128.png"];
    
    [bcountryflagimg addSubview:bcountryimg];
    bcountryimg.userInteractionEnabled=YES;
    
    bcountrylabel = [[UILabel alloc]initWithFrame:CGRectMake(60, 12, 225, 22)];
    [bcountryflagimg addSubview:bcountrylabel];
    bcountrylabel.backgroundColor=[UIColor clearColor];
    bcountrylabel.text=@"Afghanistan";
    
    bcountrylabel.textColor=[UIColor grayColor];
    bcountrylabel.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
    
    
    UITapGestureRecognizer *tapbs = [[UITapGestureRecognizer alloc]
                                     initWithTarget:self
                                     action:@selector(toggletable)];
    
    [bcountryflagimg addGestureRecognizer:tapbs];
    
    
    if (business == 0)
    {
        //        countrycodetable=[[UITableView alloc]initWithFrame:CGRectMake(15, 205, 50, 150)];
        //        countrycodetable=[[UITableView alloc]initWithFrame:CGRectMake(5, 20, (self.view.bounds.size.width)-10,(self.view.bounds.size.height)-30)];
        countrycodetable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, [UIScreen mainScreen].bounds.size.width, self.view.frame.size.height-64)];
        [self.view addSubview:countrycodetable];
        [self.view bringSubviewToFront:countrycodetable];

    }
    else
    {
        //        countrycodetable=[[UITableView alloc]initWithFrame:CGRectMake(15, 275, 50, 100)];
        //        countrycodetable=[[UITableView alloc]initWithFrame:CGRectMake(5, 20, (self.view.bounds.size.width)-10,(self.view.bounds.size.height)-30)];
        countrycodetable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-64)];
        [self.view addSubview:countrycodetable];
        [self.view bringSubviewToFront:countrycodetable];
    }
    countrycodetable.backgroundColor=[UIColor whiteColor];
    countrycodetable.dataSource=self;
    countrycodetable.delegate=self;
    countrycodetable.separatorStyle=UITableViewCellSeparatorStyleNone;
    countrycodetable.hidden=YES;
    countrycodetable.showsVerticalScrollIndicator=NO;
    countrycodetable.layer.borderColor=[[UIColor grayColor]CGColor];
    countrycodetable.layer.borderWidth=1;
    countrycodetable.layer.zPosition=3;
    
    
    countryflagtable=[[UITableView alloc]initWithFrame:CGRectMake(15, 63, [UIScreen mainScreen].bounds.size.width-30, self.view.bounds.size.height-63)];
    [self.view addSubview:countryflagtable];
    [self.view bringSubviewToFront:countryflagtable];

    countryflagtable.backgroundColor=[UIColor whiteColor];
    countryflagtable.dataSource=self;
    countryflagtable.delegate=self;
    countryflagtable.separatorStyle=UITableViewCellSeparatorStyleNone;
    countryflagtable.hidden=YES;
    countryflagtable.showsVerticalScrollIndicator=NO;
    countryflagtable.layer.borderColor=[[UIColor grayColor]CGColor];
    countryflagtable.layer.borderWidth=1;
    countryflagtable.layer.zPosition=2;
    
    bcountryflagtable=[[UITableView alloc]initWithFrame:CGRectMake(15,59,[UIScreen mainScreen].bounds.size.width-30,self.view.bounds.size.height-59)];
    [self.view addSubview:bcountryflagtable];
    [self.view bringSubviewToFront:bcountryflagtable];

    bcountryflagtable.backgroundColor=[UIColor whiteColor];
    bcountryflagtable.dataSource=self;
    bcountryflagtable.delegate=self;
    bcountryflagtable.separatorStyle=UITableViewCellSeparatorStyleNone;
    bcountryflagtable.hidden=YES;
    bcountryflagtable.showsVerticalScrollIndicator=NO;
    bcountryflagtable.layer.borderColor=[[UIColor grayColor]CGColor];
    bcountryflagtable.layer.borderWidth=1;
    bcountryflagtable.layer.zPosition=2;
    
    //    [self loadViews];
    //    [act removeFromSuperview];
    firstTime = true;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(toggletable)];
    [countryflagimg addGestureRecognizer:tap];
}

-(void)startDBInserta:(NSNotification *)notification
{
    if (profile_insertion == 0)
    {
        profile_insertion =1;
        DebugLog(@"startdbinserta called");
        [[DBManager getSharedInstance]insertProfile:profdict :[[prefs objectForKey:@"userid"] intValue]];
    }
}

-(void)servererror
{
    alert = [[UIAlertView alloc] initWithTitle:@"Error in Profile Server Connection!"
                                       message:nil delegate:self
                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
    [alert show];
}

-(void)loaderShowing
{
    act1 = [[UIActivityIndicatorView alloc] init];
    act1.center = mainview.center;
    act1.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [act1 setColor:[UIColor blackColor]];
    [mainview addSubview:act1];
    [act startAnimating];
}



@end