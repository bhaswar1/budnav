//
//  ConAddBackUpViewController.h
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 10/06/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConParentTopBarViewController.h"
#import <MessageUI/MessageUI.h>
#import "GAITrackedViewController.h"

@interface ConAddBackUpViewController : GAITrackedViewController<NSURLConnectionDataDelegate,NSURLConnectionDelegate>
{
    UIAlertView *alert;
    sqlite3 *contactDB;
    
    __block int dbstatusq;
    
    NSDictionary *json1, *indexedSections;
    NSMutableArray *app_contacts, *new_con_array, *testarr, *final_con_array, *check_app_cont,*appdelwebarray;
    NSMutableDictionary *ns, *add_contacts_dict;
    int loadcheck, number_of_iterations, connectiondoublefiler;
    NSString *con_count;
    NSMutableData *responseData;
}
@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) UINavigationController *navigationController;
@property (strong, nonatomic) UITabBarController *tabBarController;
@property (nonatomic, assign) int previousTabIndex, dbstatus;
@property(nonatomic,retain)NSMutableArray *arr_contacts_ad, *recom_search_array, *con_array, *uniquearray, *part_array;
@property(nonatomic,retain)NSString *recom_search_string, *pushtoprofileid;
@property(nonatomic,readwrite)long writetodbcount;
//@property (nonatomic, retain) ConLoginViewController *viewcontroller;

-(void) reloadNewContactsFromWeb;

@end
