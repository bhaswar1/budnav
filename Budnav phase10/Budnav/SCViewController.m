/*
 Copyright 2013 Scott Logic Ltd
 http://www.apache.org/licenses/LICENSE-2.0 */
#import "SCViewController.h"
#import "AppDelegate.h"
@import AVFoundation;
#import "SCShapeView.h"
#import "ConAccountSettingsViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import "ConNewRequestsViewController.h"
#import "ConProfileOwnViewController.h"
#import "iToast.h"
#import "ConScanRequestViewController.h"
#import "ConInviteViewController.h"
#import "ConSyncLoaderViewController.h"
#import "ConRequestViewController.h"
#import "SVProgressHUD.h"
#import "ConQROptionsViewController.h"
#import "ConNewProfileViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"
#import "ConLocateGroupViewController.h"
#import "ConFullSettingsViewController.h"
#import "ConPersonalProfileViewController.h"


@interface SCViewController () <AVCaptureMetadataOutputObjectsDelegate,UIAlertViewDelegate> {
    AVCaptureVideoPreviewLayer *_previewLayer;
    SCShapeView *_boundingBox;
    NSTimer *_boxHideTimer, *redtimer;
    UILabel *_decodedMessage;
    UIView *redview, *mainview;
    UIAlertView *alert;
    NSString *resultscan, *useridstr;
    AVAudioPlayer *player;
    AVCaptureSession *session;
}
@end
@implementation SCViewController
@synthesize request_type;

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"go"];
    mainview = [[UIView alloc]initWithFrame:CGRectMake(0, 65, [UIScreen mainScreen].bounds.size.width, self.view.frame.size.height-65)];
    [self.view addSubview:mainview];
    mainview.backgroundColor=[UIColor clearColor];
    
    //Play sound clip on QR scan sucessful
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"beep-07" ofType:@"mp3"]; //sounds-751-youve-been-informed
    NSError *error = nil;
    NSURL *url = [NSURL fileURLWithPath:path];
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    
    
    // Create a new AVCaptureSession
    session = [[AVCaptureSession alloc] init];
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    // Want the normal device
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
    if(input) {
        // Add the input to the session
        [session addInput:input];
    } else {
        DebugLog(@"error: %@", error);
        return;
    }
    
    AVCaptureMetadataOutput *output = [[AVCaptureMetadataOutput alloc] init];
    // Have to add the output before setting metadata types
    [session addOutput:output];
    // What different things can we register to recognise?
    //    DebugLog(@"%@", [output availableMetadataObjectTypes]);
    // We're only interested in QR Codes
    [output setMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]];
    // This VC is the delegate. Please call us on the main queue
    [output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    
    // Display on screen
    _previewLayer = [AVCaptureVideoPreviewLayer layerWithSession:session];
    _previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    //    _previewLayer.bounds = self.view.bounds;
    DebugLog(@"bhbbjbj : %f",self.view.bounds.size.height);
    _previewLayer.frame= CGRectMake(0, 65, [UIScreen mainScreen].bounds.size.width, self.view.bounds.size.height-115);
    //    _previewLayer.position = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds));
    [self.view.layer addSublayer:_previewLayer];
    
    // Add the view to draw the bounding box for the UIView
    _boundingBox = [[SCShapeView alloc] initWithFrame: CGRectMake(45, _previewLayer.bounds.origin.y+100, 231, 280) ];
    _boundingBox.backgroundColor = [UIColor clearColor];
    _boundingBox.center=self.view.center;
    _boundingBox.hidden = NO;
    _boundingBox.layer.borderColor= [[UIColor colorWithPatternImage:[UIImage imageNamed:@"focus.png"]]CGColor];
    _boundingBox.layer.borderWidth=1;
    [self.view addSubview:_boundingBox];
    
    // Add a label to display the resultant message
    _decodedMessage = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.view.bounds) - 150, CGRectGetWidth(self.view.bounds),75)];
    _decodedMessage.numberOfLines = 0;
    _decodedMessage.backgroundColor = [UIColor clearColor];
    _decodedMessage.textColor = [UIColor whiteColor];
    _decodedMessage.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:_decodedMessage];
    
    // Start the AVSession running
    //    [session startRunning];
    
    redview = [[UIView alloc]initWithFrame:CGRectMake(0, 150, [UIScreen mainScreen].bounds.size.width, 4)];
    redview.center=self.view.center;
    //    redview.backgroundColor=[UIColor redColor];
    [self.view addSubview:redview];
    redview.layer.zPosition=3;
    redview.hidden=NO;
    redview.layer.shadowOffset = CGSizeMake(0, 5);
    redview.layer.shadowRadius = 5;
    redview.layer.shadowOpacity = 0.7;
    //    redview.layer.shadowColor=[[UIColor redColor]CGColor];
    
    redtimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                target:self
                                              selector:@selector(targetMethod:)
                                              userInfo:nil
                                               repeats:YES];
}

-(void)targetMethod:(NSTimer *)timer
{
    if (redview.hidden==YES)
        redview.hidden=NO;
    else
        redview.hidden=YES;
}

#pragma mark - AVCaptureMetadataOutputObjectsDelegate
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    
    //  Vibration on playing alert sound clip============//
    
    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
    //  AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    [player play];
    
    
    for (AVMetadataObject *metadata in metadataObjects) {
        if ([metadata.type isEqualToString:AVMetadataObjectTypeQRCode]) {
            // Transform the meta-data coordinates to screen coords
            AVMetadataMachineReadableCodeObject *transformed = (AVMetadataMachineReadableCodeObject *)[_previewLayer transformedMetadataObjectForMetadataObject:metadata];
            // Update the frame on the _boundingBox view, and show it
            //            _boundingBox.frame = transformed.bounds;
            _boundingBox.hidden = NO;
            // Now convert the corners array into CGPoints in the coordinate system of the bounding box itself
            //    NSArray *translatedCorners = [self translatePoints:transformed.corners fromView:self.view toView:_boundingBox];
            // Set the corners array
            //            _boundingBox.corners = translatedCorners;
            
            // Update the view with the decoded text
            _decodedMessage.text = [transformed stringValue];
            
            resultscan = [transformed stringValue];
            DebugLog(@"QR=====>%@",resultscan);
            
            if (![[resultscan substringToIndex:23] isEqualToString:@"http://dev.mikroware.nl"]){
                
                UIAlertView *alertMe = [[UIAlertView alloc] initWithTitle:@"Not a Budnav QR code, please go back"
                                                                  message:[NSString stringWithFormat:@"%@",resultscan]
                                                                 delegate:self
                                                        cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                [alertMe show];
                
            }else{
                
                NSArray *testarr= [resultscan componentsSeparatedByString:@"id="];
                NSString *newstr = [testarr objectAtIndex:1];
                NSArray *arrtest = [newstr componentsSeparatedByString:@"&&"];
                useridstr = [arrtest objectAtIndex:0];
                
                if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"go"] isEqualToString:@"yes"])
                    [self add_friend];
                
            }
            
            [player stop];
            // Start the timer which will hide the overlay
            [session stopRunning];
            [self startOverlayHideTimer];
            break;
        }
        break;
    }
}

#pragma mark - Utility Methods
- (void)startOverlayHideTimer
{
    // Cancel it if we're already running
    if(_boxHideTimer) {
        [_boxHideTimer invalidate];
    }
    // Restart it to hide the overlay when it fires
    _boxHideTimer = [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(removeBoundingBox:) userInfo:nil repeats:NO];
    
}

- (void)removeBoundingBox:(NSTimer *)sender
{
    // Hide the box and remove the decoded text
    //    _boundingBox.hidden = YES;
    _decodedMessage.text = @"";
}

- (NSArray *)translatePoints:(NSArray *)points fromView:(UIView *)fromView toView:(UIView *)toView
{
    NSMutableArray *translatedPoints = [NSMutableArray new];
    
    // The points are provided in a dictionary with keys X and Y
    for (NSDictionary *point in points) {
        // Let's turn them into CGPoints
        CGPoint pointValue = CGPointMake([point[@"X"] floatValue], [point[@"Y"] floatValue]);
        // Now translate from one view to the other
        CGPoint translatedPoint = [fromView convertPoint:pointValue toView:toView];
        // Box them up and add to the array
        [translatedPoints addObject:[NSValue valueWithCGPoint:translatedPoint]];
    }
    return [translatedPoints copy];
}

+(void)chngpostion
{
    DebugLog(@"Change pos requests page");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Data" object:Nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getData:) name:@"Data" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(navtopage:) name:@"pagenav" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestreceivedaction) name:@"Requestreceived_push" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(acceptedrequestaction) name:@"Accepted_push" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AcceptedRequest) name:@"Accepted_request" object:nil];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AcceptedRequest) name:@"TypeChange" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shareLocation) name:@"ShareLocation" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdateInformation) name:@"UpdateInfo" object:nil];
    
}

-(void)UpdateInformation
{
    ConPersonalProfileViewController *PVC = [[ConPersonalProfileViewController alloc]init];
    PVC.toUpdateInfo = @"YES";
    [self.navigationController pushViewController:PVC animated:YES];
  //  [[NSNotificationCenter defaultCenter] postNotificationName:@"Update Info" object:Nil];
    
}


-(void)shareLocation
{
    ConLocateGroupViewController *conLocate = [[ConLocateGroupViewController alloc]init];
    conLocate.group_id=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"sharedLocGrpId"]];
    [self.navigationController pushViewController:conLocate animated:YES];
}

-(void)AcceptedRequest
{
    ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
    // con.other=@"yes";
    con.request = YES;
    [self.tabBarController.tabBar setHidden:YES];
    // con.uid=[NSString stringWithFormat:@"%d",[userid intValue]];
    //                [self.navigationController presentViewController:con animated:NO completion:nil];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
    
}

-(void)requestreceivedaction
{
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
    con.other=@"yes";
    con.uid=[NSString stringWithFormat:@"%@",del.pushtoprofileid];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}

-(void)acceptedrequestaction
{
    ConRequestViewController *con = [[ConRequestViewController alloc]init];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}


-(void)navtopage: (NSNotification *)notification
{
    DebugLog(@"navtopage");
    NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
    if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"4"])
    {
        ConAccountSettingsViewController *con = [[ConAccountSettingsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"6"])
    {
        ConInviteViewController *con = [[ConInviteViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"5"])
    {
        ConSyncLoaderViewController *con = [[ConSyncLoaderViewController alloc]init];
        //        [self.navigationController pushViewController:con animated:NO];
        
        [self presentViewController:con animated:NO completion:nil];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"2"])
    {
        ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"3"])
    {
        ConQROptionsViewController *con = [[ConQROptionsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    
    
    [mainview setFrame:CGRectMake(0, mainview.frame.origin.y, mainview.frame.size.width, mainview.frame.size.height)];
    //    [prefs setObject:@"111" forKey:@"whichpage"];
}


-(void)getData:(NSNotification *)notification {
    
    if(mainview.frame.origin.x == 0) {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake(280, mainview.frame.origin.y, mainview.frame.size.width, mainview.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                         }];
    } else {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake(0, (mainview.frame.origin.y),mainview.frame.size.width, mainview.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                         }];
    }
}


-(void)viewDidAppear:(BOOL)animated
{
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    self.navigationController.navigationBarHidden=YES;
    [del showTabValues:YES];
    
    [session startRunning];
    
    UIView *coverView = [[UIView alloc]init];
    
    coverView.frame = CGRectMake(10, 23, 30, 27);
    
    coverView.backgroundColor = [UIColor blackColor];
    
    [self.view addSubview:coverView];
    
    
    UIImageView *logo_img = [[UIImageView alloc]initWithFrame:CGRectMake(114, 31, 92.5f, 16)];
    logo_img.image=[UIImage imageNamed:@"logomod.png"];
    [self.view addSubview:logo_img];
    
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    backBtn.frame = CGRectMake(12, 32, 12, 20);
    
    backBtn.backgroundColor=[UIColor clearColor];
    
    [backBtn setBackgroundImage:[UIImage imageNamed:@"back3"] forState:UIControlStateNormal];
    
    [backBtn addTarget:self action:@selector(gomenu) forControlEvents:UIControlEventTouchUpInside];
    
    [coverView addSubview:backBtn];
    
    UIView *backbuttonextended = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 85, 60)];
    backbuttonextended.backgroundColor=[UIColor clearColor];
    [self.view addSubview:backbuttonextended];
    backbuttonextended.userInteractionEnabled=YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gomenu)];
    [backbuttonextended addGestureRecognizer:tap];
    
}

-(void)add_friend
{
    DebugLog(@"calledddddd");
    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"go"];
    
    //    ConScanRequestViewController *con = [[ConScanRequestViewController alloc]init];
    //    con.userid= useridstr;
    //         con.request_type=request_type;
    //    [self.navigationController pushViewController:con animated:NO];
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *urlString1;
    
    if ([request_type isEqualToString:@"business"])
        
        urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=addrequest&id=%d&access_token=%@&device_id=%@&business=true",[useridstr intValue],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
    
    else
        
        urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=addrequest&id=%d&access_token=%@&device_id=%@",[useridstr intValue],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
    
    DebugLog(@"deny url: %@",urlString1);
    
    
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    
    if (signeddataURL1 == nil)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        //            [alert show];
        [SVProgressHUD dismiss];
        
//        CATransition *transition = [CATransition animation];
//        
//        transition.duration = 0.4f;
//        
//        transition.type = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        NSError *error=nil;
        NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                  options:kNilOptions
                                                                    error:&error];
        DebugLog(@"scan request json returns: %@",json_deny);
        
        //            [[[[iToast makeText:[NSString stringWithFormat:@" %@ ",[json_deny objectForKey:@"success"]]]
        //               setGravity:iToastGravityTop] setDuration:iToastDurationShort] show];
        
        if ([[json_deny objectForKey:@"success"]intValue] == 1)
        {
            
            
            NSDictionary *details_dict = [json_deny objectForKey:@"details"];
            
            if ([[details_dict objectForKey:@"switch"] intValue] ==0) {
                
                //                    [[[[iToast makeText:[NSString stringWithFormat:@"switch %d ",[[json_deny objectForKey:@"switch"] intValue]]]
                //                       setGravity:iToastGravityTop] setDuration:iToastDurationShort] show];
                
                [SVProgressHUD dismiss];
                
                ConNewRequestsViewController *con = [[ConNewRequestsViewController alloc]init];
                
                con.userid= [useridstr intValue];
                
                
//                CATransition* transition = [CATransition animation];
//                
//                transition.duration = 0.4;
//                transition.type = kCATransitionPush;
//                transition.subtype = kCATransitionFade;
//                
//                [[self navigationController].view.layer addAnimation:transition forKey:nil];
                
                
                [self.navigationController pushViewController:con animated:YES];
                
                alert = [[UIAlertView alloc] initWithTitle:@"Success!"
                                                   message:@"Request Successfully Sent"
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                [alert show];
                
            }
            else{
                
                //                    [[[[iToast makeText:[NSString stringWithFormat:@"switch %d ",[[json_deny objectForKey:@"switch"] intValue]]]
                //                       setGravity:iToastGravityTop] setDuration:iToastDurationShort] show];
                
                [SVProgressHUD dismiss];
                
                alert = [[UIAlertView alloc] initWithTitle:@"Success!"
                                                   message:@"Request Successfully Sent"
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                [alert show];
                
                [SVProgressHUD dismiss];
                
                
                ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
                
                con.other=@"yes";
                con.uid=useridstr;
                
//                CATransition* transition = [CATransition animation];
//                
//                transition.duration = 0.4;
//                transition.type = kCATransitionPush;
//                transition.subtype = kCATransitionFade;
//                
//                [[self navigationController].view.layer addAnimation:transition forKey:nil];
                
                [self.navigationController pushViewController:con animated:YES];
            }
        }
        
        else
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                               message:[json_deny objectForKey:@"error"]
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
            
            [SVProgressHUD dismiss];
            
            ConQROptionsViewController *con = [[ConQROptionsViewController alloc]init];
            
            
//            CATransition* transition = [CATransition animation];
//            
//            transition.duration = 0.4;
//            transition.type = kCATransitionPush;
//            transition.subtype = kCATransitionFade;
//            
//            [[self navigationController].view.layer addAnimation:transition forKey:nil];
            
            [self.navigationController pushViewController:con animated:YES];
        }
    }
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [session stopRunning];
    [_previewLayer removeFromSuperlayer];
    [_boundingBox removeFromSuperview];
    [_decodedMessage removeFromSuperview];
    [super viewDidDisappear:YES];
}

-(void)gomenu//: (id)sender
{
    int index;
    NSArray* navarr = [[NSArray alloc] initWithArray:self.navigationController.viewControllers];
    for(int i=0 ; i<[navarr count] ; i++)
    {
        if(![[navarr objectAtIndex:i] isKindOfClass:NSClassFromString(@"SCViewController")])
        {
            index = i;
        }
    }
    [self.navigationController popToViewController:[navarr objectAtIndex:index] animated:NO];
}
@end