//  ConSocialEditViewController.m
//  Created by Bhaswar's MacBook Air on 30/05/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
#import "ConSocialEditViewController.h"
#import "AppDelegate.h"
#import "ConAccountSettingsViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

@interface ConSocialEditViewController ()
{
    UIButton *save_btn;
    UILabel *fblabel, *skype_label, *gplus_label;
    UIScrollView *scrollView;
}
@end
@implementation ConSocialEditViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor colorWithRed:(238.0f/255.0f) green:(238.0f/255.0f) blue:(238.0f/255.0f) alpha:1.0];

    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, [[UIScreen mainScreen]bounds].size.height-44, [UIScreen mainScreen].bounds.size.width, 44)];
    del.tabBarController.tabBar.hidden = YES;
    self.navigationController.navigationBarHidden=YES;
    self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
    
    topbar = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64)];
    [self.view addSubview:topbar];
    //topbar.backgroundColor=[UIColor blackColor];
    topbar.layer.zPosition=2;
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
    
    // Add the gradient to the view
    [topbar.layer insertSublayer:gradient atIndex:0];
    
    
    UIImageView *logo_img = [[UIImageView alloc]init];
    logo_img.frame = CGRectMake(27+([UIScreen mainScreen].bounds.size.width-160)/2, 32, 101, 20);
    logo_img.backgroundColor = [UIColor clearColor];
    
    logo_img.image = [UIImage imageNamed:@"new_logo"];
    [topbar addSubview:logo_img];
    
    
    UILabel *topbartitle = [[UILabel alloc]initWithFrame:CGRectMake(60, 32, [UIScreen mainScreen].bounds.size.width-120, 20)];
    topbartitle.backgroundColor=[UIColor clearColor];
    //[topbar addSubview:topbartitle];
    topbartitle.text=@"SOCIAL MEDIA";
    topbartitle.textAlignment=NSTextAlignmentCenter;
    topbartitle.font=[UIFont fontWithName:@"ProximaNova-Regular" size:15.5f];
    topbartitle.textColor=[UIColor whiteColor];
    
    //    UIButton *menubt = [UIButton buttonWithType:UIButtonTypeCustom];
    //    menubt.frame = CGRectMake(15, 22, 26, 25);
    //    [menubt setBackgroundImage:[UIImage imageNamed:@"back-button-ac.png"] forState:UIControlStateNormal];
    //    [menubt addTarget:self action:@selector(goback:) forControlEvents:UIControlEventTouchUpInside];
    //    [topbar addSubview:menubt];
    
    UIImageView *bck_img=[[UIImageView alloc]initWithFrame:CGRectMake(15,32,12,20)];//(13, 39, 10, 22)];
    bck_img.image=[UIImage imageNamed:@"back3"];
    [topbar addSubview:bck_img];
    
    UIButton *helpbt = [UIButton buttonWithType:UIButtonTypeCustom];
    helpbt.frame = CGRectMake(0, 27, 60, 37);
    helpbt.backgroundColor=[UIColor clearColor];
    [helpbt setTitle:nil forState:UIControlStateNormal];
    [helpbt addTarget:self action:@selector(goback) forControlEvents:UIControlEventTouchUpInside];
    [topbar addSubview:helpbt];
    
    UIView *backbuttonextended = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 85, 64)];
    backbuttonextended.backgroundColor=[UIColor clearColor];
    [topbar addSubview:backbuttonextended];
    backbuttonextended.userInteractionEnabled=YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goback)];
    [backbuttonextended addGestureRecognizer:tap];
    
    
    save_btn = [[UIButton alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-90, 25, 90, 39)];
    [save_btn setTitle:@"Save" forState:UIControlStateNormal];
    save_btn.backgroundColor = [UIColor clearColor];
    [save_btn.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Bold" size:20.0]];
    // [save_btn.titleLabel.text] = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    [save_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [topbar addSubview:save_btn];
    [save_btn addTarget:self action:@selector(changeColor) forControlEvents:UIControlEventTouchDown];
    [save_btn addTarget:self action:@selector(changeColor) forControlEvents:UIControlEventTouchDragEnter];
    [save_btn addTarget:self action:@selector(changeColor) forControlEvents:UIControlEventTouchDragExit];
    [save_btn addTarget:self action:@selector(savechanges:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    //    CGRect frame = CGRectMake(110,165,100,100);
    act = [[UIActivityIndicatorView alloc] init];
    act.center=self.view.center;//WithFrame:frame];
    [act startAnimating];
    act.layer.zPosition=1;
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [act setColor:[UIColor blackColor]];
    [self.view addSubview:act];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    //    [del showTabValues:YES];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, [[UIScreen mainScreen]bounds].size.height-44, [UIScreen mainScreen].bounds.size.width, 44)];
    self.navigationController.navigationBarHidden=YES;
    del.tabBarController.tabBar.hidden = YES;
    //imagesiconarr = [NSMutableArray arrayWithObjects:[UIImage imageNamed:@"twitter.png"],[UIImage imageNamed:@"pinterest.png"],[UIImage imageNamed:@"facebook.png"],[UIImage imageNamed:@"google-plus.png"],[UIImage imageNamed:@"you-tube.png"],[UIImage imageNamed:@"skype.png"],[UIImage imageNamed:@"linkedin1.png"],[UIImage imageNamed:@"soundcloud.png"],[UIImage imageNamed:@"instagram.png"],[UIImage imageNamed:@"vimeo.png"], nil];
    //imagesiconarr = [NSMutableArray arrayWithObjects:@"twitter.png",@"pinterest@2x1.png",@"facebook.png",@"google-plus.png",@"you-tube@2x1.png",@"skype@2x1.png",@"linkedin1@2x1.png",@"soundcloud@2x1.png",@"instagram@2x1.png",@"vimeo@2x1.png", nil];
    
//    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 65, [UIScreen mainScreen].bounds.size.width,self.view.frame.size.height) ];
//    scrollView.backgroundColor =[UIColor clearColor];
//    scrollView.scrollEnabled = YES;
//    scrollView.userInteractionEnabled = YES;
//    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width,scrollView.frame.size.height+255)];
//    scrollView.contentSize = CGSizeMake(1, scrollView.frame.size.height+255);
//    scrollView.showsVerticalScrollIndicator=NO;
//    [self.view addSubview:scrollView];
//    scrollView.contentOffset=CGPointMake(0, 0);
    
    
    imagesiconarr = [NSMutableArray arrayWithObjects:@"Facebook",@"Google+",@"Instagram",@"Linkedin",@"Pinterest",@"Skype",@"Soundcloud",@"Twitter",@"Vimeo",@"YouTube", nil];
    DebugLog(@"imagesicon : %@",imagesiconarr);
    
    toptextarr = [NSMutableArray arrayWithObjects:@"www.twitter.com/",@"www.pinterest.com/",@"www.facebook.com/",@"Google+",@"Youtube",@"Skype",@"LinkedIN",@"www.soundcloud.com/",@"www.instagram.com/",@"www.vimeo.com/",nil];
    
    
    scviewbudget = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 65, [UIScreen mainScreen].bounds.size.width,self.view.frame.size.height-65) ];
    scviewbudget.backgroundColor =[UIColor clearColor];
    scviewbudget.scrollEnabled = YES;
    scviewbudget.userInteractionEnabled = YES;
    [scviewbudget setContentSize:CGSizeMake(scviewbudget.frame.size.width,1080)];
    scviewbudget.showsVerticalScrollIndicator=NO;
    [self.view addSubview:scviewbudget];
    scviewbudget.contentOffset=CGPointMake(0, 0);
    
    
    fblabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
    [fblabel setBackgroundColor:[UIColor whiteColor]];
    [scviewbudget addSubview:fblabel];
    
    UILabel *fb_div = [[UILabel alloc] initWithFrame:CGRectMake(0,fblabel.frame.origin.y+fblabel.frame.size.height,[UIScreen mainScreen].bounds.size.width,1.0f)];
    fb_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f];
    [scviewbudget addSubview:fb_div];
    
    
    skype_label = [[UILabel alloc] initWithFrame:CGRectMake(0,fb_div.frame.origin.y+fb_div.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50)];
    [skype_label setBackgroundColor:[UIColor whiteColor]];
    [scviewbudget addSubview:skype_label];
    
    UILabel *skype_div = [[UILabel alloc] initWithFrame:CGRectMake(0,skype_label.frame.origin.y+skype_label.frame.size.height,[UIScreen mainScreen].bounds.size.width,1.0f)];
    skype_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f];
    [scviewbudget addSubview:skype_div];
    
    
//    twittertext= [[UITextField alloc]initWithFrame:CGRectMake(60, 34+1, [UIScreen mainScreen].bounds.size.width-75, 31)];
//    twittertext.backgroundColor=[UIColor clearColor];
//    twittertext.placeholder=@"Enter your Twitter Link";
//    twittertext.keyboardAppearance=UIKeyboardAppearanceDark;
//    twittertext.delegate=self;
//    
//    
//    pinteresttext= [[UITextField alloc]initWithFrame:CGRectMake(60, 34*2+45*1+1, [UIScreen mainScreen].bounds.size.width-75, 31)];
//    pinteresttext.backgroundColor=[UIColor clearColor];
//    pinteresttext.placeholder=@"Enter your Pinterest Link";
//    pinteresttext.keyboardAppearance=UIKeyboardAppearanceDark;
//    pinteresttext.delegate=self;
    
    
//    facebooktext= [[UITextField alloc]initWithFrame:CGRectMake(60, 34*3+45*2+1, [UIScreen mainScreen].bounds.size.width-75, 31)];
//    facebooktext.backgroundColor=[UIColor clearColor];
//    facebooktext.placeholder=@"Enter your Facebook Link";
//    facebooktext.keyboardAppearance=UIKeyboardAppearanceDark;
//    facebooktext.delegate=self;
//    
//    
//    googleplustext= [[UITextField alloc]initWithFrame:CGRectMake(60, 34*4+45*3+1, [UIScreen mainScreen].bounds.size.width-75, 31)];
//    googleplustext.backgroundColor=[UIColor clearColor];
//    googleplustext.placeholder=@"Enter your Google Plus Link";
//    googleplustext.keyboardAppearance=UIKeyboardAppearanceDark;
//    googleplustext.delegate=self;
    
    
//    youtubetext= [[UITextField alloc]initWithFrame:CGRectMake(60, 34*5+45*4+1, [UIScreen mainScreen].bounds.size.width-75, 31)];
//    youtubetext.backgroundColor=[UIColor clearColor];
//    youtubetext.placeholder=@"Enter your Youtube Link";
//    youtubetext.keyboardAppearance=UIKeyboardAppearanceDark;
//    youtubetext.delegate=self;
//    
//    
//    
//    skypetext= [[UITextField alloc]initWithFrame:CGRectMake(60, 34*6+45*5+1, [UIScreen mainScreen].bounds.size.width-75, 31)];
//    skypetext.backgroundColor=[UIColor clearColor];
//    skypetext.placeholder=@"Enter your Skype Link";
//    skypetext.keyboardAppearance=UIKeyboardAppearanceDark;
//    skypetext.delegate=self;
//    
//    
//    
//    linkedintext= [[UITextField alloc]initWithFrame:CGRectMake(60, 34*7+45*6+1, [UIScreen mainScreen].bounds.size.width-75, 31)];
//    linkedintext.backgroundColor=[UIColor clearColor];
//    linkedintext.placeholder=@"Enter your LinkedIN Link";
//    linkedintext.keyboardAppearance=UIKeyboardAppearanceDark;
//    linkedintext.delegate=self;
//    
//    
//    
//    soundcloudtext= [[UITextField alloc]initWithFrame:CGRectMake(60, 34*8+45*7+1, [UIScreen mainScreen].bounds.size.width-75, 31)];
//    soundcloudtext.backgroundColor=[UIColor clearColor];
//    soundcloudtext.placeholder=@"Enter your username";
//    soundcloudtext.keyboardAppearance=UIKeyboardAppearanceDark;
//    soundcloudtext.delegate=self;
//    
//    
//    
//    instagramtext= [[UITextField alloc]initWithFrame:CGRectMake(60, 34*9+45*8+1, [UIScreen mainScreen].bounds.size.width-75, 31)];
//    instagramtext.backgroundColor=[UIColor clearColor];
//    instagramtext.placeholder=@"Enter your username";
//    instagramtext.keyboardAppearance=UIKeyboardAppearanceDark;
//    instagramtext.delegate=self;
//    
//    
//    
//    vimeotext= [[UITextField alloc]initWithFrame:CGRectMake(60, 34*10+45*9+1, [UIScreen mainScreen].bounds.size.width-75, 31)];
//    vimeotext.backgroundColor=[UIColor clearColor];
//    vimeotext.placeholder=@"Enter your username";
//    vimeotext.keyboardAppearance=UIKeyboardAppearanceDark;
//    vimeotext.delegate=self;
    
    
    
    
    NSOperationQueue *queue = [NSOperationQueue new];
    NSInvocationOperation *op = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(loaddata) object:nil];
    [queue addOperation:op];
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}


-(void)loaddata
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    NSError *error = nil;
    
    NSString *user_id= [prefs objectForKey:@"userid"];
    
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=profile&id=%@&access_token=%@&device_id=%@&image=true",user_id,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
    
    DebugLog(@"profile url: %@",urlString1);
    
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    
    if (signeddataURL1 != nil)
        json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                 
                                                options:kNilOptions
                 
                                                  error:&error];
    //    DebugLog(@"json returns: %@",json1);
    else
    {
        [act stopAnimating];
        DebugLog(@"no connnn profile");
        alert = [[UIAlertView alloc] initWithTitle:@"Error in Profile Server Connection!"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        
    }
    
    NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
    DebugLog(@"err  %@",errornumber);
    
    if (![errornumber isEqualToString:@"0"])
    {
        [act stopAnimating];
        DebugLog(@"if if");
        NSString *err_str = [json1 objectForKey:@"error"];
        alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                           message:err_str
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
    }
    else
    {
        personal_dict = [[NSMutableDictionary alloc]init];
        business_dict = [[NSMutableDictionary alloc]init];
        social_dict = [[NSMutableDictionary alloc]init];
        
        profdict= [json1 objectForKey:@"details"];
        NSDictionary *dict_profile = [profdict objectForKey:@"profile"];
        
        for( NSString *aKey in [dict_profile allKeys])
        {
            NSString *newString = [aKey substringToIndex:2];
            if ([newString isEqualToString:@"s_"])
                [social_dict setValue:[dict_profile valueForKey:[NSString stringWithFormat:@"%@",aKey]] forKey:[NSString stringWithFormat:@"%@",aKey]];
        }
        DebugLog(@"social dict; %@",social_dict);
    }
    
    
    [self performSelectorOnMainThread:@selector(displaydata) withObject:nil waitUntilDone:NO];
    
}


-(void)displaydata
{
    [act stopAnimating];
    for (int i=0; i<10; i++)
    {
        UIImageView *iconimg = [[UIImageView alloc]initWithFrame:CGRectMake(15, 34*(i+1)+45*i, 35, 34)];
        [scviewbudget addSubview:iconimg];
        iconimg.backgroundColor=[UIColor clearColor];
        UIImage *imgs = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[imagesiconarr objectAtIndex:i]]];
        iconimg.image= imgs;
        UIImageView *viewimg1=[[UIImageView alloc]initWithFrame:CGRectMake(55, 34*(i+1)+45*i, [UIScreen mainScreen].bounds.size.width-70, 32)];
        viewimg1.image=[UIImage imageNamed:@"input filed2.png"];
        [scviewbudget addSubview:viewimg1];
        viewimg1.userInteractionEnabled=YES;
        
//        UILabel *sociallb = [[UILabel alloc]initWithFrame:CGRectMake(55, iconimg.frame.origin.y-19, [UIScreen mainScreen].bounds.size.width-70, 20)];
//        [scviewbudget addSubview:sociallb];
//        sociallb.backgroundColor=[UIColor clearColor];
//        sociallb.text=[NSString stringWithFormat:@"%@",[toptextarr objectAtIndex:i]];
//        sociallb.textColor=[UIColor darkGrayColor];
        
        //        UILabel *socialblanks = [[UILabel alloc]initWithFrame:CGRectMake(55, 34*(i+1)+45*i, 250, 32)];
        //        [scviewbudget addSubview:socialblanks];
        //        socialblanks.backgroundColor=[UIColor clearColor];
        //        socialblanks.layer.borderWidth=1;
        //        socialblanks.layer.borderColor=[[UIColor grayColor]CGColor];
    }
    
    if ([social_dict objectForKey:@"s_twitter"] != false)
    {
        twittertext.text= [NSString stringWithFormat:@"%@",[social_dict objectForKey:@"s_twitter"]];
    }
    else
        twittertext.text=@"";
    
    if ([social_dict objectForKey:@"s_facebook"] != false)
    {
        facebooktext.text= [NSString stringWithFormat:@"%@",[social_dict objectForKey:@"s_facebook"]];
    }
    else
        facebooktext.text=@"";
    if ([social_dict objectForKey:@"s_pinterest"] != false)
    {
        pinteresttext.text= [NSString stringWithFormat:@"%@",[social_dict objectForKey:@"s_pinterest"]];
    }
    else
        pinteresttext.text=@"";
    if ([social_dict objectForKey:@"s_google"] != false)
    {
        googleplustext.text= [NSString stringWithFormat:@"%@",[social_dict objectForKey:@"s_google"]];
    }
    else
        googleplustext.text=@"";
    if ([social_dict objectForKey:@"s_youtube"] != false)
    {
        youtubetext.text= [NSString stringWithFormat:@"%@",[social_dict objectForKey:@"s_youtube"]];
    }
    else
        youtubetext.text=@"";
    if ([social_dict objectForKey:@"s_skype"] != false)
    {
        skypetext.text= [NSString stringWithFormat:@"%@",[social_dict objectForKey:@"s_skype"]];
    }
    else
        skypetext.text=@"";
    if ([social_dict objectForKey:@"s_linkedin"] != false)
    {
        linkedintext.text= [NSString stringWithFormat:@"%@",[social_dict objectForKey:@"s_linkedin"]];
    }
    else
        linkedintext.text=@"";
    if ([social_dict objectForKey:@"s_instagram"] != false)
    {
        instagramtext.text= [NSString stringWithFormat:@"%@",[social_dict objectForKey:@"s_instagram"]];
    }
    else
        instagramtext.text=@"";
    if ([social_dict objectForKey:@"s_vimeo"] != false)
    {
        vimeotext.text= [NSString stringWithFormat:@"%@",[social_dict objectForKey:@"s_vimeo"]];
    }
    else
        vimeotext.text=@"";
    if ([social_dict objectForKey:@"s_soundcloud"] != false)
    {
        soundcloudtext.text= [NSString stringWithFormat:@"%@",[social_dict objectForKey:@"s_soundcloud"]];
    }
    else
        soundcloudtext.text=@"";
    
    [scviewbudget addSubview:twittertext];
    [scviewbudget addSubview:pinteresttext];
    [scviewbudget addSubview:facebooktext];
    [scviewbudget addSubview:googleplustext];
    [scviewbudget addSubview:youtubetext];
    [scviewbudget addSubview:skypetext];
    [scviewbudget addSubview:linkedintext];
    [scviewbudget addSubview:soundcloudtext];
    [scviewbudget addSubview:instagramtext];
    [scviewbudget addSubview:vimeotext];
    
//    UIButton *nextbt = [UIButton buttonWithType:UIButtonTypeCustom];
//    nextbt.frame = CGRectMake(15, vimeotext.frame.origin.y + vimeotext.frame.size.height+30, [UIScreen mainScreen].bounds.size.width-30, 45);
//    nextbt.layer.cornerRadius=5;
//    [nextbt setBackgroundImage:[UIImage imageNamed:@"save-button-ac.png"] forState:UIControlStateNormal];
//    nextbt.backgroundColor=[UIColor clearColor];
//    [nextbt addTarget:self action:@selector(savechanges:) forControlEvents:UIControlEventTouchUpInside];
//    [scviewbudget addSubview:nextbt];
    
    //    [del showTabValues:YES];
    
}


-(void) goback//: (id)sender
{
    //    ConAccountSettingsViewController *con = [[ConAccountSettingsViewController alloc]init];
    //    [self.navigationController pushViewController:con animated:NO];
    
    
//    CATransition *transition = [CATransition animation];
//    
//    transition.duration = 0.4f;
//    
//    transition.type = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    [self.navigationController popViewControllerAnimated:YES];
    //[self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)savechanges: (id)sender
{
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=editsocial&s_twitter=%@&s_pinterest=%@&s_facebook=%@&s_google=%@&s_youtube=%@&s_skype=%@&s_linkedin=%@&s_instagram=%@&s_soundcloud=%@&s_vimeo=%@&access_token=%@&device_id=%@",[twittertext.text stringByTrimmingCharactersInSet:whitespace],[pinteresttext.text stringByTrimmingCharactersInSet:whitespace],[facebooktext.text stringByTrimmingCharactersInSet:whitespace],[googleplustext.text stringByTrimmingCharactersInSet:whitespace],[youtubetext.text stringByTrimmingCharactersInSet:whitespace],[skypetext.text stringByTrimmingCharactersInSet:whitespace],[linkedintext.text stringByTrimmingCharactersInSet:whitespace],[instagramtext.text stringByTrimmingCharactersInSet:whitespace],[soundcloudtext.text stringByTrimmingCharactersInSet:whitespace],[vimeotext.text stringByTrimmingCharactersInSet:whitespace],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
    
    DebugLog(@"deny url: %@",urlString1);
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    
    NSError *error=nil;
    NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                              options:kNilOptions
                                                                error:&error];
    DebugLog(@"deny json returns: %@",json_deny);
    DebugLog(@"errorno : %d",[[json_deny objectForKey:@"errorno"]intValue]);
    
    NSString *errornumber_reqcount= [NSString stringWithFormat:@"%@",[json_deny objectForKey:@"errorno"]];
    if (![errornumber_reqcount isEqualToString:@"0"])
    {
        DebugLog(@"if if");
        NSString *err_str_reqcount = [json_deny objectForKey:@"error"];
        alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                           message:err_str_reqcount
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        
        save_btn.alpha = 1.0f;
    }
    
    if ([[json_deny objectForKey:@"success"]intValue] == 1)
    {
        
        alert = [[UIAlertView alloc] initWithTitle:@"Changes have been Successfully Saved!"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        save_btn.alpha = 1.0f;
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if (textField == facebooktext)
    {
        if (scviewbudget.contentOffset.y < 60)
            [scviewbudget setContentOffset:CGPointMake(0, 60)];
    }
    if (textField == googleplustext)
    {
        if (scviewbudget.contentOffset.y < 120)
            [scviewbudget setContentOffset:CGPointMake(0, 120)];
    }
    if (textField == youtubetext)
    {
        if (scviewbudget.contentOffset.y < 200)
            [scviewbudget setContentOffset:CGPointMake(0, 200)];
    }
    if (textField == skypetext)
    {
        if (scviewbudget.contentOffset.y < 280)
            [scviewbudget setContentOffset:CGPointMake(0, 280)];
    }
    if (textField == linkedintext)
    {
        if (scviewbudget.contentOffset.y < 360)
            [scviewbudget setContentOffset:CGPointMake(0, 360)];
    }
    if (textField == soundcloudtext)
    {
        if (scviewbudget.contentOffset.y < 440)
            [scviewbudget setContentOffset:CGPointMake(0, 440)];
    }
    if (textField == instagramtext)
    {
        if (scviewbudget.contentOffset.y < 470)
            [scviewbudget setContentOffset:CGPointMake(0,520)];
    }
    if (textField == vimeotext)
    {
        if (scviewbudget.contentOffset.y < 500)
            [scviewbudget setContentOffset:CGPointMake(0, 600)];
    }
}

-(void)changeColor
{
    save_btn.alpha = 0.5f;
}




@end