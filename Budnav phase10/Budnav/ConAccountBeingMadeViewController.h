//
//  ConAccountBeingMadeViewController.h
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 07/05/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface ConAccountBeingMadeViewController : GAITrackedViewController<NSURLConnectionDelegate>

@property (nonatomic,retain) UIImage *portraitImg;
@end
