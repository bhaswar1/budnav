//
//  ConGroupChangeAdminViewController.h
//  Contacter
//
//  Created by ios on 09/01/15.
//  Copyright (c) 2015 Esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GroupDetailsViewController.h"
#import "ConGroupSettingsViewController.h"
#import "GAITrackedViewController.h"

@interface ConGroupChangeAdminViewController : GAITrackedViewController<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
@property(nonatomic)NSString *groupid,*adminname,*userid,*groupname,*membercount;
@end
