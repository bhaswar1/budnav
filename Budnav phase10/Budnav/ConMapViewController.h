//
//  ConMapViewController.h
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 19/05/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConParentTopBarViewController.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <Superpin/Superpin.h>
#import "REVClusterMapView.h"
#import "SMCalloutView.h"
#import "GAITrackedViewController.h"

@interface ConMapViewController : GAITrackedViewController<MKMapViewDelegate,UIScrollViewDelegate,UIWebViewDelegate,UIGestureRecognizerDelegate,SMCalloutViewDelegate>
{
    NSDictionary *json1;
    UIAlertView *alert;
    NSMutableArray *requestslist;
    UITableView *tab_request;
//    UIView *mainview;
    
    NSMutableArray *newarr;
    NSMutableArray *newarr3;
    NSMutableArray *newarr1;
    NSMutableArray *newarr2;
    NSMutableArray *newarr4;
    int taxic,eventc,tradec,listc,typec;
    
    REVClusterMapView *_mapView;
}
+(void)chngpostion;

@property(nonatomic,retain) NSString *getmapvalue;
@property (nonatomic, strong) UIView *mainview;

@end
