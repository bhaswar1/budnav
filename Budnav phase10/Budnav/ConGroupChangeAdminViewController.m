//
//  ConGroupChangeAdminViewController.m
//  Contacter
//
//  Created by ios on 09/01/15.
//  Copyright (c) 2015 Esolz. All rights reserved.
//

#import "ConGroupChangeAdminViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

@interface ConGroupChangeAdminViewController ()
{
    UIActivityIndicatorView *act;
    UIView *mainview,*blackBack,*grayView,*additionalview;
    UIImageView *bck_img,*checkimg;
    UILabel *backlbl,*nouserlb;
    UIButton *back,*change;
    UITextField *searchfield;
    UITableView *groupstable;
    NSDictionary *groupdict,*groupnames;
    NSMutableArray *group_array,*filtered_arr,*searchresults;
    BOOL filter,isAtLeast7,isAtLeast8;
    NSCharacterSet *whitespace;
    UITableViewCell *cell;
    NSInteger k,i;
    NSString *groups_url,*st,*check,*version;
    
}

@end

@implementation ConGroupChangeAdminViewController
@synthesize groupid,adminname,userid,groupname,membercount;


- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"From ConGroupChangeAdminViewController");
    // Do any additional setup after loading the view.
    
    
    
    k=0;
    filter=NO;
}

-(void)viewDidAppear:(BOOL)animated
{
    version = [[UIDevice currentDevice] systemVersion];
    DebugLog(@"version: %@",version);
    isAtLeast7 = [version floatValue] < 8.0;
    isAtLeast8 = [version floatValue] >= 8.0;
    
    blackBack = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    [self.view addSubview:blackBack];
    
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
    
    // Add the gradient to the view
    [blackBack.layer insertSublayer:gradient atIndex:0];
    
    
    bck_img=[[UIImageView alloc]initWithFrame:CGRectMake(15,32,12,20)];//(13, 33, 14, 22)];
    bck_img.image=[UIImage imageNamed:@"back3"];
    [blackBack addSubview:bck_img];
    
    backlbl=[[UILabel alloc]initWithFrame:CGRectMake(29, 28, 50, 30)];
    backlbl.text=@"Back";
    backlbl.textColor=[UIColor whiteColor];
    backlbl.font=[UIFont fontWithName:@"ProximaNova-Regular" size:20];
    backlbl.backgroundColor=[UIColor clearColor];
    [blackBack addSubview:backlbl];
    
    back=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 150, 64)];
    [back setTitle:@"" forState:UIControlStateNormal];
    back.backgroundColor=[UIColor clearColor];
    [back addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDown];
    [back addTarget:self action:@selector(changecoloragain) forControlEvents:UIControlEventTouchDragExit];
    [back addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDragEnter];
    [back addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [blackBack addSubview:back];
    
    
    change=[[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width-85, 29, 77, 30)];
    [change setTitle:@"Change" forState:UIControlStateNormal];
    change.titleLabel.font=[UIFont fontWithName:@"ProximaNova-Regular" size:20];
    [change addTarget:self action:@selector(change) forControlEvents:UIControlEventTouchUpInside];
    change.backgroundColor=[UIColor clearColor];
    change.enabled = YES;
    [blackBack addSubview:change];
    
    grayView=[[UIView alloc]initWithFrame:CGRectMake(0, 65, self.view.frame.size.width, 50.5)];
    //grayView.backgroundColor=[UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1.0];
    grayView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:grayView];
    
    additionalview=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 19, 30.5)];
    
    searchfield = [[UITextField alloc] initWithFrame:CGRectMake(15, 10, [UIScreen mainScreen].bounds.size.width-30, 31)];
    searchfield.delegate=self;
    
    searchfield.backgroundColor=[UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f];

    searchfield.layer.cornerRadius=5;
    searchfield.tintColor = [UIColor whiteColor];
    searchfield.autocorrectionType=UITextAutocorrectionTypeNo;
    searchfield.clearButtonMode=UITextFieldViewModeWhileEditing;
    searchfield.leftView=additionalview;
    searchfield.leftViewMode=UITextFieldViewModeAlways;
    searchfield.keyboardAppearance=UIKeyboardAppearanceDark;
    searchfield.font=[UIFont fontWithName:@"ProximaNova-Regular" size:14];
    searchfield.textColor = [UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:1.0f];
    [grayView addSubview:searchfield];
    searchfield.tag=2;
    
    
     NSAttributedString *str = [[NSAttributedString alloc] initWithString:@"Search members" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:(0.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:0.8f] }];
    searchfield.attributedPlaceholder = str;
    
    
    mainview = [[UIView alloc]initWithFrame:CGRectMake(0,grayView.frame.origin.y+grayView.frame.size.height, self.view.frame.size.width, [UIScreen mainScreen].bounds.size.height)];
    mainview.backgroundColor=[UIColor whiteColor];
    //    mainview.layer.zPosition=3;
    [self.view addSubview:mainview];
    
    act = [[UIActivityIndicatorView alloc] init];
    act.center = self.view.center;
    [act startAnimating];
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [act setColor:[UIColor blackColor]];
    [self.view addSubview:act];
    act.layer.zPosition=2;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    groups_url =[NSString stringWithFormat:@"https://budnav.com/ext/?action=group-getmembers&group_id=%@&access_token=%@&device_id=%@",groupid,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
    DebugLog(@"groups url string ------ %@",groups_url);
    
    NSError *error1=nil;
    @try {
        NSData *data1=[NSData dataWithContentsOfURL:[NSURL URLWithString:groups_url]options:NSDataReadingUncached error:&error1];
        if (data1 != nil)
            groupdict=[NSJSONSerialization JSONObjectWithData:data1 //1
                                                      options:kNilOptions
                                                        error:&error1];
        DebugLog(@"-----Groups info ----- %@",groupdict);
    }
    @catch (NSException *exception) {
        DebugLog(@"exception %@",exception);
    }
    groupnames = [groupdict objectForKey:@"details"];
    DebugLog(@"groups array ---- %@",groupnames);
    
    group_array =[[NSMutableArray alloc]init];
    group_array =[[groupnames valueForKey:@"members"] mutableCopy];
    DebugLog(@"bb %@",group_array);
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)];
    group_array=[[group_array sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]] mutableCopy];
    
    
    groupstable =[[UITableView alloc]initWithFrame:CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width,480)];
    groupstable.delegate=self;
    groupstable.tag=1;
    groupstable.dataSource=self;
    [groupstable setBackgroundColor:[UIColor clearColor]];
    groupstable.separatorStyle=UITableViewCellSeparatorStyleNone;
    groupstable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [mainview addSubview:groupstable];
    
    nouserlb= [[UILabel alloc]initWithFrame:CGRectMake(0, 70, [UIScreen mainScreen].bounds.size.width, 30)];
    [mainview addSubview:nouserlb];
    nouserlb.text=@"No results";
    nouserlb.backgroundColor=[UIColor clearColor];
    nouserlb.textAlignment=NSTextAlignmentCenter;
    nouserlb.textColor=[UIColor grayColor];//colorWithRed:238.0f/255.0f green:238.0f/255.0f blue:238.0f/255.0f alpha:1];
    nouserlb.hidden=YES;
    
    
    
    if (!filter) {
        for (i=0; i<[group_array count]; i++)
        {
            NSString *checkad=[NSString stringWithFormat:@"%@",[[group_array objectAtIndex:i] objectForKey:@"admin"]];
            if ([checkad isEqualToString:@"1"])
            {
                k=i;
            }
        }
    }
    else if (filter) {
        for (i=0; i<[filtered_arr count]; i++)
        {
            NSString *checkad=[NSString stringWithFormat:@"%@",[[filtered_arr objectAtIndex:i] objectForKey:@"admin"]];
            if ([checkad isEqualToString:@"1"])
            {
                k=i;
            }
        }
    }
    
}

-(void)back
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.4f;
    //transition.type = kCATransitionPush;
    transition.type = kCATransitionFade;
    //transition.subtype = kCATransitionFromLeft;
    
    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)change
{
    DebugLog(@"CHNAGE ADMIN!!");
    
    if (isAtLeast8)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil message:@"Are you sure you want to replace yourself as group admin?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                                     [[NSNotificationCenter defaultCenter] removeObserver:self
                                                                                     name:UITextFieldTextDidChangeNotification
                                                                                   object:nil];
                                     
                                 }];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                             {
                                 [[NSNotificationCenter defaultCenter] removeObserver:self
                                                                                 name:UITextFieldTextDidChangeNotification
                                                                               object:nil];
                                 [self changeadmin];
                                 
                             }];
        
        [alert addAction:cancel];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    if (isAtLeast7)
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:nil message:@"Are you sure you want to replace yourself as group admin?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
        [alert show];
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        [self changeadmin];
    }
}


-(void)changeadmin
{
    groupdict=[[NSDictionary alloc]init];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    groups_url =[NSString stringWithFormat:@"https://budnav.com/ext/?action=group-changeadmin&group_id=%@&user_id=%@&access_token=%@&device_id=%@",groupid,userid,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
    DebugLog(@"groups url string ------ %@",groups_url);
    
    NSError *error1=nil;
    @try
    {
        NSData *data1=[NSData dataWithContentsOfURL:[NSURL URLWithString:groups_url]options:NSDataReadingUncached error:&error1];
        if (data1 != nil)
            groupdict=[NSJSONSerialization JSONObjectWithData:data1 //1
                                                      options:kNilOptions
                                                        error:&error1];
        DebugLog(@"-----Groups info ----- %@",groupdict);
        check=[NSString stringWithFormat:@"%@",[groupdict valueForKey:@"success"]];
        if ([check isEqualToString:@"1"])
        {
            if (isAtLeast8)
            {
                UIAlertController * alert=   [UIAlertController
                                              alertControllerWithTitle:nil
                                              message:@"You are no longer admin. Admin change sucessful"
                                              preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:nil];
                [self performSelector:@selector(test:) withObject:alert afterDelay:2];
            }
            if (isAtLeast7) {
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:nil message:@"You are no longer admin. Admin change sucessful" delegate:self cancelButtonTitle:nil otherButtonTitles:nil, nil];
                [alert show];
                
                [self performSelector:@selector(test:) withObject:alert afterDelay:2];
            }
            
            
        }
        else if ([check isEqualToString:@"0"])
        {
            if (isAtLeast8)
            {
                UIAlertController * alert=   [UIAlertController
                                              alertControllerWithTitle:@"Warning"
                                              message:[NSString stringWithFormat:@"%@",[groupdict valueForKey:@"error"]]
                                              preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* cancel = [UIAlertAction
                                         actionWithTitle:@"Ok"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                             
                                         }];
                
                [alert addAction:cancel];
                
                [self presentViewController:alert animated:YES completion:nil];
            }
            if (isAtLeast7)
            {
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Warning" message:[NSString stringWithFormat:@"%@",[groupdict valueForKey:@"error"]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                
                [alert show];
            }
        }
        
    }
    @catch (NSException *exception)
    {
        DebugLog(@"exception %@",exception);
    }
    
    
    
}

-(void)test:(UIAlertController*)x
{
    [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
    GroupDetailsViewController *grpdtls=[[GroupDetailsViewController alloc]init];
    grpdtls.groupid=groupid;
    grpdtls.groupname=groupname;
    grpdtls.member_count=membercount;
    grpdtls.isadmin=@"0";
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    transition.subtype = kCATransitionFromRight;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:grpdtls animated:NO];
}


#pragma mark-dismiss alertcontroller
- (void)didEnterBackground:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UITextFieldTextDidChangeNotification
                                                  object:nil];
    
    [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
}


#pragma mark-tableview methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!filter)
    {
        DebugLog(@"count of groups ---- %lu",(unsigned long)group_array.count);
        return [group_array count];
    }
    if (filter) {
        return [filtered_arr count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DebugLog(@"grop: %@",group_array);
    static NSString *MyIdentifier = @"MyReuseIdentifier";
    
    cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    cell=nil;
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:MyIdentifier];
    }
    if (!filter)
    {
        cell.backgroundColor = [UIColor clearColor];
        [act stopAnimating];
        UILabel *namelbl = [[UILabel alloc]initWithFrame:CGRectMake(15.0f,5.0f, 240.0f, 30.0f)];
        if ([[NSString stringWithFormat:@"%@",[[group_array objectAtIndex:indexPath.row]objectForKey:@"surname"]] isEqualToString:@""] || [[NSString stringWithFormat:@"%@",[[group_array objectAtIndex:indexPath.row]objectForKey:@"surname"]] length]==0 || [[NSString stringWithFormat:@"%@",[[group_array objectAtIndex:indexPath.row]objectForKey:@"surname"]] isKindOfClass:[NSNull class]]) {
            [namelbl setText:[NSString stringWithFormat:@"%@",[[group_array objectAtIndex:indexPath.row]objectForKey:@"name"]]];
        }
        else
            [namelbl setText:[NSString stringWithFormat:@"%@ %@",[[group_array objectAtIndex:indexPath.row]objectForKey:@"name"],[[group_array objectAtIndex:indexPath.row]objectForKey:@"surname"]]];
        //            [namelbl sizeToFit];
        [namelbl setTextAlignment:NSTextAlignmentLeft];
        [namelbl setTextColor:[UIColor blackColor]];
        [namelbl setFont:[UIFont fontWithName:@"ProximaNova-Bold" size:17]];
        [cell addSubview:namelbl];
        
        UIImageView *proficon = [[UIImageView alloc]initWithFrame:CGRectMake(15.0f,31.5f,[UIImage imageNamed:@"Map Marker Contacts Screen copy2x.png"].size.width/1.5, [UIImage imageNamed:@"Map Marker Contacts Screen copy2x.png"].size.height/1.4)];
        DebugLog(@"PROFICON FRAME W:%lf",proficon.frame.size.width);
        DebugLog(@"PROFICON FRAME H:%lf",proficon.frame.size.height);
        proficon.backgroundColor=[UIColor clearColor];
        
        [cell addSubview:proficon];
        
        UILabel *citylbl = [[UILabel alloc]initWithFrame:CGRectMake(35.0f,25.0f,150.0f, 30.0f)];
        if ([[NSString stringWithFormat:@"%@",[[group_array objectAtIndex:indexPath.row]objectForKey:@"city"]] isKindOfClass:[NSNull class]] || [[NSString stringWithFormat:@"%@",[[group_array objectAtIndex:indexPath.row]objectForKey:@"city"]] length]==0 || [[NSString stringWithFormat:@"%@",[[group_array objectAtIndex:indexPath.row]objectForKey:@"city"]] isEqualToString:@""])
        {
            if ([[NSString stringWithFormat:@"%@",[[group_array objectAtIndex:indexPath.row]objectForKey:@"country"]] isKindOfClass:[NSNull class]] || [[NSString stringWithFormat:@"%@",[[group_array objectAtIndex:indexPath.row]objectForKey:@"country"]] length]==0 || [[NSString stringWithFormat:@"%@",[[group_array objectAtIndex:indexPath.row]objectForKey:@"country"]] isEqualToString:@""])
            {
                citylbl.text=@"";
                namelbl.frame = CGRectMake(35, 0, 250, 63);
            }
            else
            {
                citylbl.text=[NSString stringWithFormat:@"%@",[[group_array objectAtIndex:indexPath.row]objectForKey:@"country"]];
                [proficon setImage:[UIImage imageNamed:@"Map Marker Contacts Screen copy2x.png"]];
            }
        }
        else
        {
            if ([[NSString stringWithFormat:@"%@",[[group_array objectAtIndex:indexPath.row]objectForKey:@"country"]] isKindOfClass:[NSNull class]] || [[NSString stringWithFormat:@"%@",[[group_array objectAtIndex:indexPath.row]objectForKey:@"country"]] length]==0 || [[NSString stringWithFormat:@"%@",[[group_array objectAtIndex:indexPath.row]objectForKey:@"country"]] isEqualToString:@""])
            {
                [citylbl setText:[NSString stringWithFormat:@"%@",[[group_array objectAtIndex:indexPath.row]objectForKey:@"city"]]];
                [proficon setImage:[UIImage imageNamed:@"Map Marker Contacts Screen copy2x.png"]];
            }
            else
            {
                [citylbl setText:[NSString stringWithFormat:@"%@, %@",[[group_array objectAtIndex:indexPath.row]objectForKey:@"city"],[[group_array objectAtIndex:indexPath.row]objectForKey:@"country"]]];
                [proficon setImage:[UIImage imageNamed:@"Map Marker Contacts Screen copy2x.png"]];
            }
            
        }
        //[[[group_array objectAtIndex:0] objectAtIndex:indexPath.row]objectForKey:@"city"]]];
        [citylbl setTextAlignment:NSTextAlignmentLeft];
        [citylbl setTextColor:[UIColor grayColor]];
        [citylbl setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:13]];
        [cell addSubview:citylbl];
        UILabel *separatorlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 62.5, [UIScreen mainScreen].bounds.size.width, 0.5)];
        separatorlabel.backgroundColor=[UIColor lightGrayColor];
        [cell addSubview:separatorlabel];
        
        if (indexPath.row==k)
        {
            checkimg=[[UIImageView alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-60, 20, 22, 19)];
            checkimg.image=[UIImage imageNamed:@"Check.png"];
            [cell addSubview:checkimg];
        }
        
        
    }
    else if (filter)
    {
        cell.backgroundColor = [UIColor clearColor];
        
        UILabel *namelbl = [[UILabel alloc]initWithFrame:CGRectMake(15.0f,5.0f, 240.0f, 30.0f)];
        if ([[NSString stringWithFormat:@"%@",[[filtered_arr valueForKey:@"surname"]objectAtIndex:indexPath.row]] isEqualToString:@""] || [[NSString stringWithFormat:@"%@",[[filtered_arr valueForKey:@"surname"]objectAtIndex:indexPath.row]] length]==0 || [[NSString stringWithFormat:@"%@",[[filtered_arr valueForKey:@"surname"]objectAtIndex:indexPath.row]] isKindOfClass:[NSNull class]]) {
            [namelbl setText:[NSString stringWithFormat:@"%@",[[filtered_arr valueForKey:@"name"]objectAtIndex:indexPath.row]]];
        }
        else
            
            [namelbl setText:[NSString stringWithFormat:@"%@ %@",[[filtered_arr valueForKey:@"name"]objectAtIndex:indexPath.row],[[filtered_arr valueForKey:@"surname"]objectAtIndex:indexPath.row]]];
        [namelbl setTextAlignment:NSTextAlignmentLeft];
        [namelbl setTextColor:[UIColor blackColor]];
        [namelbl setFont:[UIFont fontWithName:@"ProximaNova-Bold" size:17]];
        [cell addSubview:namelbl];
        
        UIImageView *proficon = [[UIImageView alloc]initWithFrame:CGRectMake(15.0f,31.5f,[UIImage imageNamed:@"Map Marker Contacts Screen copy2x.png"].size.width/1.5, [UIImage imageNamed:@"Map Marker Contacts Screen copy2x.png"].size.height/1.4)];
        
        DebugLog(@"PROFICON FRAME W:%lf",proficon.frame.size.width);
        DebugLog(@"PROFICON FRAME H:%lf",proficon.frame.size.height);
        
        proficon.backgroundColor=[UIColor clearColor];
        //            [proficon setImage:[UIImage imageNamed:@"Map Marker Contacts Screen copy2x.png"]];
        [cell addSubview:proficon];
        
        UILabel *citylbl = [[UILabel alloc]initWithFrame:CGRectMake(35.0f,25.0f,150.0f, 30.0f)];
        if ([[NSString stringWithFormat:@"%@",[[filtered_arr valueForKey:@"city"]objectAtIndex:indexPath.row]] isKindOfClass:[NSNull class]] || [[NSString stringWithFormat:@"%@",[[filtered_arr valueForKey:@"city"]objectAtIndex:indexPath.row]] length]==0 || [[NSString stringWithFormat:@"%@",[[filtered_arr valueForKey:@"city"]objectAtIndex:indexPath.row]] isEqualToString:@""])
        {
            if ([[NSString stringWithFormat:@"%@",[[filtered_arr valueForKey:@"country"]objectAtIndex:indexPath.row]] isKindOfClass:[NSNull class]] || [[NSString stringWithFormat:@"%@",[[filtered_arr valueForKey:@"country"]objectAtIndex:indexPath.row]] length]==0 || [[NSString stringWithFormat:@"%@",[[filtered_arr valueForKey:@"country"]objectAtIndex:indexPath.row]] isEqualToString:@""])
            {
                citylbl.text=@"";
                namelbl.frame = CGRectMake(35, 0, 250, 63);
            }
            else
            {
                citylbl.text=[NSString stringWithFormat:@"%@",[[filtered_arr valueForKey:@"country"]objectAtIndex:indexPath.row]];
                [proficon setImage:[UIImage imageNamed:@"Map Marker Contacts Screen copy2x.png"]];
            }
        }
        else
        {
            if ([[NSString stringWithFormat:@"%@",[[filtered_arr valueForKey:@"country"]objectAtIndex:indexPath.row]] isKindOfClass:[NSNull class]] || [[NSString stringWithFormat:@"%@",[[filtered_arr valueForKey:@"country"]objectAtIndex:indexPath.row]] length]==0 || [[NSString stringWithFormat:@"%@",[[filtered_arr valueForKey:@"country"]objectAtIndex:indexPath.row]] isEqualToString:@""])
            {
                [citylbl setText:[NSString stringWithFormat:@"%@",[[filtered_arr valueForKey:@"city"]objectAtIndex:indexPath.row]]];
                [proficon setImage:[UIImage imageNamed:@"Map Marker Contacts Screen copy2x.png"]];
            }
            else
            {
                [citylbl setText:[NSString stringWithFormat:@"%@, %@",[[filtered_arr valueForKey:@"city"]objectAtIndex:indexPath.row],[[filtered_arr valueForKey:@"country"]objectAtIndex:indexPath.row]]];
                [proficon setImage:[UIImage imageNamed:@"Map Marker Contacts Screen copy2x.png"]];
            }
            
        }
        
        [citylbl setTextAlignment:NSTextAlignmentLeft];
        [citylbl setTextColor:[UIColor grayColor]];
        [citylbl setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:13]];
        [cell addSubview:citylbl];
        
        if (indexPath.row==k) {
            checkimg=[[UIImageView alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-60, 20, 22, 19)];
            checkimg.image=[UIImage imageNamed:@"Check.png"];
            [cell addSubview:checkimg];
        }
        
        
        UILabel *separatorlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 62.5, [UIScreen mainScreen].bounds.size.width, 0.5)];
        separatorlabel.backgroundColor=[UIColor lightGrayColor];
        [cell addSubview:separatorlabel];
        
        
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (filter)
    {
        userid=[NSString stringWithFormat:@"%@",[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"id"]];
        check=[NSString stringWithFormat:@"%@",[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"admin"]];
        if ([check isEqualToString:@"1"])
        {
            change.enabled=NO;
        }
        else if ([check isEqualToString:@"0"])
        {
            change.enabled=YES;
        }
        
    }
    
    else if (!filter)
    {
        userid=[NSString stringWithFormat:@"%@",[[group_array objectAtIndex:indexPath.row] objectForKey:@"id"]];
        check=[NSString stringWithFormat:@"%@",[[group_array objectAtIndex:indexPath.row] objectForKey:@"admin"]];
        if ([check isEqualToString:@"1"])
        {
            change.enabled=NO;
        }
        else if ([check isEqualToString:@"0"])
        {
            change.enabled=YES;
        }
        
    }
    k=indexPath.row;
    [groupstable reloadData];
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 63.0f;
}


#pragma mark- textfield delegates

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    filter=NO;
    return YES;
}

- (BOOL) textField: (UITextField *)theTextField shouldChangeCharactersInRange: (NSRange)range replacementString: (NSString *)string
{
    NSString *str=  [NSString stringWithFormat:@"%@",[theTextField.text stringByReplacingCharactersInRange:range withString:string]];
    //filter=NO;
    if ([str length]>0)
    {
        whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        st = [str stringByTrimmingCharactersInSet:whitespace];
        if([st length]>0)
        {
            [filtered_arr removeAllObjects];
            filter = YES;
            
            NSPredicate *resultPredicate = [NSPredicate
                                            predicateWithFormat:@"name contains[c] %@",str];
            DebugLog(@"aa %@",group_array);
            
            searchresults =[NSMutableArray arrayWithArray:[group_array filteredArrayUsingPredicate:resultPredicate] ];
            
            filtered_arr=[[NSMutableArray alloc] initWithArray:searchresults];
            NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)];
            filtered_arr=[[filtered_arr sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]] mutableCopy];
            DebugLog(@"aaa %@",filtered_arr);
            [groupstable reloadData];
            
            if ([filtered_arr count] ==0)
            {
                [filtered_arr removeAllObjects];
                groupstable.hidden=YES;
                nouserlb.hidden=NO;
            }
            else
            {
                nouserlb.hidden=YES;
                groupstable.hidden=NO;
                [groupstable reloadData];
            }
            
        }
        else {
            nouserlb.hidden=YES;
            groupstable.hidden=NO;
            NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
            filtered_arr=[[group_array sortedArrayUsingDescriptors:[NSMutableArray arrayWithObject:sort]] mutableCopy];
            [groupstable reloadData];
        }
        
    }
    else if ([str length]==0)
    {
        //filter=NO;
        nouserlb.hidden=YES;
        groupstable.hidden=NO;
        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
        filtered_arr=[[group_array sortedArrayUsingDescriptors:[NSMutableArray arrayWithObject:sort]] mutableCopy];
        [groupstable reloadData];
    }
    return YES;
}


- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    nouserlb.hidden=YES;
    groupstable.hidden=NO;
    filter=NO;
    [searchfield resignFirstResponder];
    [groupstable reloadData];
    return YES;
    
    
}

#pragma mark- dismiss keyboard
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    DebugLog(@"Dismiss keyboard");
    
    [textField resignFirstResponder];
    return YES;
}

-(void)changecoloragain
{
    back.alpha = 1.0f;
    bck_img.alpha = 1.0f;
    backlbl.alpha = 1.0f;
}

-(void)changecolor
{
    back.alpha = 0.5f;
    bck_img.alpha = 0.5f;
    backlbl.alpha = 0.5f;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
