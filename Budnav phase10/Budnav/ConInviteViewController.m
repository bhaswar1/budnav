#import "ConInviteViewController.h"
#import <AddressBookUI/AddressBookUI.h>
#import "AppDelegate.h"
#import "ConProfileOwnViewController.h"
#import "ConManageRequestsViewController.h"
#import "ConNewRequestsViewController.h"
#import "iToast.h"
#import "ConAccountSettingsViewController.h"
#import "ConSyncLoaderViewController.h"
#import "ConRequestViewController.h"
#import "ConQROptionsViewController.h"
#import "ConNewProfileViewController.h"
#import "ConAddFriendViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"
#import "ConLocateGroupViewController.h"
#import "ConFullSettingsViewController.h"
#import "ConPersonalProfileViewController.h"


@interface ConInviteViewController () {
    NSDictionary *animals,*json1;
    NSArray *animalSectionTitles,*dictkeysarr;
    NSArray *animalIndexTitles, *anintit;
    UITableView *tab_contact;
    NSMutableArray *con_array,*final_con_array,*filtered_arr;
    NSMutableDictionary *add_contacts_dict;
    NSMutableArray *app_contacts, *check_app_cont, *phonesarr;
    UIAlertView *alert;
    int currentRow,currentSection,height,filter,move;
    NSArray *searchresults;
    NSCharacterSet *whitespace;
    NSString *st;
    UILabel *name, *nouserlb;
    NSMutableArray *uniquearray;
    NSString *smstr, *whatsapp_num, *whatsapp_name;
    NSArray *dictsms;
    NSMutableArray *sendarray, *transfer_array, *phonearr;
    UIButton *invite_send;
    CGSize myStringSize,maximumSize;
    UIFont *myFont;
    UIActivityIndicatorView *act;
    UIImageView *logo_img, *typeimg, *blue_typeimg, *bck_img;
    UIButton *inviteBtn;
    NSMutableArray *email_array;
    BOOL select_all;
    AppDelegate *del;
    UIButton *helpbt;
    
}
@end
@implementation ConInviteViewController
@synthesize mainview;
@synthesize searchfriends;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"From ConInviteViewController");
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
    
    // Add the gradient to the view
    [self.view.layer insertSublayer:gradient atIndex:0];
    
    
    logo_img = [[UIImageView alloc]init];
    logo_img.frame = CGRectMake(27+([UIScreen mainScreen].bounds.size.width-160)/2, 32, 101, 20);
    logo_img.backgroundColor = [UIColor clearColor];
    
    logo_img.image = [UIImage imageNamed:@"new_logo"];
    [self.view addSubview:logo_img];
    
    
    inviteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    inviteBtn.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-90, 25, 90, 39);
    inviteBtn.backgroundColor=[UIColor clearColor];
    [inviteBtn.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Bold" size:20.0]];
    [inviteBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [inviteBtn setTitle:[NSString stringWithFormat:@"Invite"] forState:UIControlStateNormal];
    //        [finishBtn addTarget:self action:@selector(finishBtn:) forControlEvents:UIControlEventTouchUpInside];
    [inviteBtn addTarget:self action:@selector(invite_all:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:inviteBtn];
    
    
    bck_img=[[UIImageView alloc]initWithFrame:CGRectMake(15,32,12,20)];//(13, 39, 10, 22)];
    bck_img.image=[UIImage imageNamed:@"back3"];
    [self.view addSubview:bck_img];
    
    helpbt = [UIButton buttonWithType:UIButtonTypeCustom];
    helpbt.frame = CGRectMake(0, 0, logo_img.frame.origin.x
                              -1, 64);
    helpbt.backgroundColor=[UIColor clearColor];
    [helpbt setTitle:nil forState:UIControlStateNormal];
    [helpbt addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDown];
    [helpbt addTarget:self action:@selector(changecoloragain) forControlEvents:UIControlEventTouchDragExit];
    [helpbt addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDragEnter];
    [helpbt addTarget:self action:@selector(goback) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:helpbt];
    
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    

    email_array = [[NSMutableArray alloc] init];
    phonearr = [[NSMutableArray alloc] init];
    
    self.view.frame=CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
    move=0;
    sendarray= [[NSMutableArray alloc]init];
    transfer_array= [[NSMutableArray alloc]init];
    
    DebugLog(@"SEARCHFRIENDZ=========>%@ &&  %@",searchfriends,[[NSUserDefaults standardUserDefaults]objectForKey:@"searchfriends"]);
    
        del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.tabBarController.tabBar.translucent = YES;
    self.navigationController.navigationBarHidden=YES;
    del.tabBarController.tabBar.hidden = YES;
    DebugLog(@"TABBAR FRAME:%f %f",del.tabBarController.tabBar.frame.origin.y,del.tabBarController.tabBar.frame.size.height);
    
    filter=0;
    currentRow=-1;
    height=0;
    currentSection=-1;
    
    mainview = [[UIView alloc]initWithFrame:CGRectMake(0, 64, [UIScreen mainScreen].bounds.size.width, self.view.frame.size.height-64)];
    [self.view addSubview:mainview];
    mainview.backgroundColor=[UIColor whiteColor];
    
    CGRect frame = CGRectMake(110,225,[UIScreen mainScreen].bounds.size.width-220,100);
    act = [[UIActivityIndicatorView alloc] initWithFrame:frame];
    [act startAnimating];
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [act setColor:[UIColor blackColor]];
    [self.view addSubview:act];
    
    NSOperationQueue *queue = [NSOperationQueue new];
    NSInvocationOperation *op = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(loaddata) object:nil];
    [queue addOperation:op];
    //--------------------------------------------------------------------------------------------------------------
    
    
}

-(void)loaddata
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    
    con_array = [[NSMutableArray alloc]init];
    
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=contacts&amount=-1&id=%@&access_token=%@&device_id=%@",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
    
    DebugLog(@"profile url: %@",urlString1);
    
    NSError *error = nil;
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    
    if (signeddataURL1 != nil)
    {
        json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                 
                                                options:kNilOptions
                 
                                                  error:&error];
        
        NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
        DebugLog(@"err  %@",errornumber);
        
        if (![errornumber isEqualToString:@"0"])
        {
            DebugLog(@"if if");
            NSString *err_str = [json1 objectForKey:@"error"];
            alert = [[UIAlertView alloc] initWithTitle:@"Error in Retrieving Data!"
                                               message:err_str
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
        }
        else
        {
            app_contacts = [[NSMutableArray alloc]init];
            //        DebugLog(@"details req= %@",[json1 objectForKey:@"details"]);
            if ([[json1 objectForKey:@"details"] isKindOfClass:[NSNull class]] || [json1 objectForKey:@"details"] == (id)[NSNull null] ||[[json1 objectForKey:@"details"] count] ==0)
            {
                DebugLog(@"App Contacts = 0");
            }
            else
            {
                NSDictionary *contacts_temp_dict = [json1 objectForKey:@"details"];
                for (NSDictionary *dict in [contacts_temp_dict objectForKey:@"contacts"])
                {
                    [app_contacts addObject:dict];
                }
            }
        }
    }
    [self performSelectorOnMainThread:@selector(displaydata) withObject:nil waitUntilDone:NO];
}

-(void)displaydata
{
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
    searchBar.delegate=self;
    searchBar.tintColor=[UIColor darkGrayColor];
    searchBar.backgroundColor=[UIColor clearColor];
    searchBar.barTintColor= [UIColor clearColor];
    searchBar.backgroundImage=[UIImage new];
    //searchBar.placeholder= @"Search Users";
    UITextField *searchField = [searchBar valueForKey:@"_searchField"];
    searchField.textColor = [UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:1.0f];
    searchField.backgroundColor = [UIColor colorWithRed:(20.0f/255.0f) green:(102.0f/255.0f) blue:(110.0f/255.0f) alpha:1.0f];
    
    [searchBar setImage:[UIImage imageNamed:@"searchIcon"]
       forSearchBarIcon:UISearchBarIconSearch
                  state:UIControlStateNormal];
    
    NSAttributedString *str = [[NSAttributedString alloc] initWithString:@"Search Users" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:(153.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f] }];
    searchField.attributedPlaceholder = str;
    searchBar.keyboardAppearance=UIKeyboardAppearanceDark;
    [mainview addSubview:searchBar];
    searchBar.hidden=YES;
    
    nouserlb= [[UILabel alloc]initWithFrame:CGRectMake(0, 50, [UIScreen mainScreen].bounds.size.width, 30)];
    [mainview addSubview:nouserlb];
    nouserlb.text=@"No results";
    nouserlb.backgroundColor=[UIColor clearColor];
    nouserlb.textAlignment=NSTextAlignmentCenter;
    nouserlb.textColor=[UIColor colorWithRed:238.0f/255.0f green:238.0f/255.0f blue:238.0f/255.0f alpha:1];
    nouserlb.hidden=YES;
    
    
    tab_contact=[[UITableView alloc]initWithFrame:CGRectMake(0, 50, [UIScreen mainScreen].bounds.size.width, mainview.frame.size.height-50)];
    [mainview addSubview:tab_contact];
    tab_contact.backgroundColor=[UIColor clearColor];
    tab_contact.dataSource=self;
    tab_contact.delegate=self;
    tab_contact.layer.zPosition = 5;
    [mainview bringSubviewToFront:tab_contact];
    tab_contact.separatorStyle=UITableViewCellSeparatorStyleNone;
    tab_contact.showsVerticalScrollIndicator=NO;
    
    [act removeFromSuperview];
    
    DebugLog(@"SUBVIEWS:%@",mainview.subviews);
    
    
    CFErrorRef *errorab = nil;
    
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, errorab);
    
    __block BOOL accessGranted = NO;
    if (ABAddressBookRequestAccessWithCompletion != NULL) { // we're on iOS 6
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(sema);
        });
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        //        dispatch_release(sema);
    }
    else { // we're on iOS 5 or older
        accessGranted = YES;
    }
    if (accessGranted) {
        
        NSMutableArray *contactpickarr = [[NSMutableArray alloc]init];
        
        ABAddressBookRef UsersAddressBook = ABAddressBookCreateWithOptions(NULL, NULL);
        
        //contains details for all the contacts
        CFArrayRef ContactInfoArray = ABAddressBookCopyArrayOfAllPeople(UsersAddressBook);
        
        //get the total number of count of the users contact
        CFIndex numberofPeople = CFArrayGetCount(ContactInfoArray);
        DebugLog(@"nop %ld",numberofPeople);
        
        //iterate through each record and add the value in the array
        for (int i =0; i<numberofPeople; i++) {
            ABRecordRef ref = CFArrayGetValueAtIndex(ContactInfoArray, i);
            if (ABRecordCopyValue(ref, kABPersonFirstNameProperty) == NULL && ABRecordCopyValue(ref, kABPersonLastNameProperty) == NULL)
            {
                
            }
            else
            {
                ABMultiValueRef names = (__bridge ABMultiValueRef)((__bridge NSString*)ABRecordCopyValue(ref, kABPersonFirstNameProperty));
                ABMultiValueRef lastnames = (__bridge ABMultiValueRef)((__bridge NSString*)ABRecordCopyValue(ref, kABPersonLastNameProperty));
                
                //        DebugLog(@"name from address book = %@ %@",names,lastnames);  // works fine.
                NSString *fullname;
                NSString *fnm = [NSString stringWithFormat:@"%@",names];
                NSString *lnm = [NSString stringWithFormat:@"%@",lastnames];
                //        DebugLog(@"fnm : %@",fnm);
                if ([fnm length] ==0 || [fnm isKindOfClass:[NSNull class]] || fnm == (id)[NSNull null] || [fnm isEqualToString:@"(null)"])
                    fullname = [NSString stringWithFormat:@"%@",lastnames];
                
                else if ([lnm length] ==0 || [lnm isKindOfClass:[NSNull class]] || lnm == (id)[NSNull null] || [lnm isEqualToString:@"(null)"])
                    fullname = [NSString stringWithFormat:@"%@",names];
                else
                    fullname = [NSString stringWithFormat:@"%@ %@",names, lastnames];
                
                //        NSString *contactName = (__bridge NSString *)(names);
                
                [contactpickarr addObject:fullname];
                
                NSData  *imgData = (__bridge NSData *)ABPersonCopyImageData(ref);
                UIImage  *img = [UIImage imageWithData:imgData];
                //        DebugLog(@"Image : %@",img);
                
                ABMultiValueRef emailProperty = ABRecordCopyValue(ref, kABPersonEmailProperty);
                
                NSArray *emailArray = (__bridge NSArray *)ABMultiValueCopyArrayOfAllValues(emailProperty);
                
                NSMutableDictionary *ns = [[NSMutableDictionary alloc]init];
                
                [ns setObject:fullname forKey:@"name"];
                [ns setObject:fullname forKey:@"fullname"];
                [ns setValue:img forKey:@"image"];
                [ns setValue:emailArray forKey:@"email"];
                
                ABMultiValueRef phoneNumberProperty = ABRecordCopyValue(ref, kABPersonPhoneProperty);
                NSArray *_phoneNumbers = (__bridge NSArray*)ABMultiValueCopyArrayOfAllValues(phoneNumberProperty);
                
                NSMutableArray *mutableph= [_phoneNumbers mutableCopy];
                int pp;
                //        DebugLog(@"mutable first: %@",mutableph);
                for (pp =0; pp< [mutableph count]; pp++)
                {
                    if ([[mutableph objectAtIndex:pp] isKindOfClass:[NSNull class]] || [mutableph objectAtIndex:pp] == (id)[NSNull null] || [[mutableph objectAtIndex:pp] length] < 3)
                    {
                        [mutableph removeObjectAtIndex:pp];
                        pp= pp-1;
                    }
                }
                
                [ns setValue:mutableph forKey:@"numbers"];
                
                ABMultiValueRef phones = ABRecordCopyValue(ref, kABPersonPhoneProperty);
                for(CFIndex j = 0; j < ABMultiValueGetCount(phones); j++)
                {
                    CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(phones, j);
                    CFStringRef locLabel = ABMultiValueCopyLabelAtIndex(phones, j);
                    NSString *phoneLabel =(__bridge NSString*) ABAddressBookCopyLocalizedLabel(locLabel);
                    ////CFRelease(phones);
                    NSString *phoneNumber = (__bridge NSString *)phoneNumberRef;
                    [ns setValue:phoneNumber forKey:phoneLabel];
                }
                if ([mutableph count] > 0)
                    [con_array addObject:ns];
            }
            
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    check_app_cont = [[NSMutableArray alloc]init];
    NSMutableArray *testarr = [con_array mutableCopy];
    NSMutableArray *invite_array = [[NSMutableArray alloc]init];
    int apps,dev;
    int test;
    test=0;
    NSString *conhome,*conmobile,*conwork; //*conmain,*coniphone,*conpager,*conother;
    NSDictionary *testdict;
    
    for (dev=0; dev< [testarr count]; dev++)
    {
        conhome = [[[[testarr objectAtIndex:dev]objectForKey:@"home"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
        conmobile = [[[[testarr objectAtIndex:dev]objectForKey:@"mobile"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
        conwork = [[[[testarr objectAtIndex:dev]objectForKey:@"work"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
              testdict = [testarr objectAtIndex:dev];
        
        for (apps=0; apps< [app_contacts count]; apps++)
        {
            NSString *homestr = [[[[app_contacts objectAtIndex:apps]objectForKey:@"mobile_num"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
            
            NSString *b_phn = [[[[app_contacts objectAtIndex:apps]objectForKey:@"b_phone_num"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
            
            NSString *landphnstr = [[[[app_contacts objectAtIndex:apps]objectForKey:@"phone_num"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
            
            if (([conhome longLongValue] != 0 && [homestr longLongValue] != 0 && [conhome rangeOfString:homestr].location != NSNotFound) || ([conhome longLongValue] != 0 && [b_phn longLongValue] != 0 && [conhome rangeOfString:b_phn].location != NSNotFound) || ([conmobile longLongValue] != 0 && [homestr longLongValue] != 0 && [conmobile rangeOfString:homestr].location != NSNotFound) || ([conmobile longLongValue] != 0 && [b_phn longLongValue] != 0 && [conmobile rangeOfString:b_phn].location != NSNotFound) || ([conwork longLongValue] != 0 && [homestr longLongValue] != 0 && [conwork rangeOfString:homestr].location != NSNotFound) || ([conwork longLongValue] != 0 && [b_phn longLongValue] != 0 && [conwork rangeOfString:b_phn].location != NSNotFound) || ([conhome longLongValue] != 0 && [landphnstr longLongValue] != 0 && [conhome rangeOfString:landphnstr].location != NSNotFound) || ([conmobile longLongValue] != 0 && [landphnstr longLongValue] != 0 && [conmobile rangeOfString:landphnstr].location != NSNotFound) || ([conwork longLongValue] != 0 && [landphnstr longLongValue] != 0 && [conwork rangeOfString:landphnstr].location != NSNotFound))
            {
                //                        DebugLog(@"home no. not found");
                test=0;
                break;
            }
            else
            {
                //                        DebugLog(@"home no. found");
                test=1;
                continue;
            }
        }
        if (test ==1)
            [invite_array addObject:testdict];
    }
    
    uniquearray = [[[NSSet setWithArray:invite_array] allObjects] mutableCopy];
    
    DebugLog(@"check contacts app has: %@",uniquearray);
    
    for (int ij =0; ij< [uniquearray count]; ij++)
    {
        
        NSString *newString = [[[[uniquearray objectAtIndex:ij] objectForKey:@"name"] substringToIndex:1] lowercaseString];
        
        NSCharacterSet *strCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"];//1234567890_"];
        
        strCharSet = [strCharSet invertedSet];
        //And you can then use a string method to find if your string contains anything in the inverted set:
        
        NSRange r = [newString rangeOfCharacterFromSet:strCharSet];
        if (r.location != NSNotFound) {
            DebugLog(@"the string contains illegal characters");
            
            [uniquearray removeObjectAtIndex:ij];
            ij= ij-1;
        }
    }
    
    
    if ([check_app_cont count] >0 && [con_array count] > 0)
        uniquearray = [NSMutableArray arrayWithArray:[app_contacts arrayByAddingObjectsFromArray:uniquearray]];
    
    else if ([check_app_cont count] > 0 && [con_array count] == 0)
        uniquearray =check_app_cont;
    else
        uniquearray =con_array;
    DebugLog(@"now con array is: %@",con_array);
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"fullname" ascending:YES selector:@selector(caseInsensitiveCompare:)];
    uniquearray=[[uniquearray sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]] mutableCopy];
    
    //-------------------------------------------------------------------------------------------------------------
    
    UIView *invitecountview =[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
//    invitecountview.backgroundColor=[UIColor colorWithRed:228.0f/255.0f green:229.0f/255.0f blue:232.0f/255.0f alpha:1];
    invitecountview.backgroundColor=[UIColor colorWithRed:(238.0f/255.0f) green:(238.0f/255.0f) blue:(238.0f/255.0f) alpha:1];
    [mainview addSubview:invitecountview];
    
    UIView *sep = [[UIView alloc] init];
    sep.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f];
    [invitecountview addSubview:sep];
    sep.frame = CGRectMake(0, 49.4f, self.view.frame.size.width, 0.6f);
    
    
    UILabel *invitecountlb = [[UILabel alloc]initWithFrame:CGRectMake(20, 18, 150, 14)];
    invitecountlb.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(153.0f/255.0f) blue:(153.0f/255.0f) alpha:1];
    [invitecountview addSubview:invitecountlb];
    invitecountlb.text=[NSString stringWithFormat:@"%lu contacts found",(unsigned long)[uniquearray count]];
    invitecountlb.font= [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    
    
    if ([_request_check isEqualToString:@"email"]) {
        
        invite_send = [UIButton buttonWithType:UIButtonTypeCustom];
        invite_send.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-120, 18, 120, 14);
        invite_send.layer.cornerRadius=5;
        [invite_send setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [invite_send setTitle:@"Select all" forState:UIControlStateNormal];
        [invite_send.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17.0]];
        [invite_send setTitleColor:[UIColor colorWithRed:(51.0f/255.0f) green:(153.0f/255.0f) blue:(153.0f/255.0f) alpha:1] forState:UIControlStateNormal];
        invite_send.backgroundColor=[UIColor clearColor];
        [invite_send addTarget:self action:@selector(select_all) forControlEvents:UIControlEventTouchUpInside];
        [invitecountview addSubview:invite_send];
        
    }
    
    
    
    if ([_request_check isEqualToString:@"whatsapp"]) {
        
        invite_send.userInteractionEnabled = NO;
    }
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (filter == 0)
    {
        return [uniquearray count];
    }
    else
        return [filtered_arr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tab_contact dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell.backgroundColor =[UIColor clearColor];
    
    if (filter==0)
    {
        
        //        dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        maximumSize = CGSizeMake(215, 30);
        NSString *firstname= [[[uniquearray objectAtIndex:indexPath.row] objectForKey:@"fullname"] capitalizedString];
        
        NSArray *myWords = [firstname componentsSeparatedByString:@" "];
        
        NSString *fname= [myWords objectAtIndex:0];
        NSString *lastname;
        myFont = [UIFont fontWithName:@"ProximaNova-Bold" size:17];
        myStringSize = [fname sizeWithFont:myFont constrainedToSize:maximumSize lineBreakMode:NSLineBreakByWordWrapping];
        name = [[UILabel alloc]init];
        name.frame = CGRectMake(20, 14, myStringSize.width, 22);
        
        //        #endif
        [cell addSubview:name];
        name.backgroundColor=[UIColor clearColor];
        name.text=fname;
        name.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
        name.textAlignment=NSTextAlignmentLeft;
        
        UILabel *surname = [[UILabel alloc]initWithFrame:CGRectMake(name.frame.origin.x+name.frame.size.width+5, 14, 270-name.frame.origin.x-name.frame.size.width, 22)];
        //        [cell addSubview:surname];
        surname.backgroundColor=[UIColor clearColor];
        //        surname.text=[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"surname"];
        for (int k=1;k< [myWords count];k++)
        {
            if (k==1)
            {
                if ([[myWords objectAtIndex:k] isKindOfClass:[NSNull class]] || [myWords objectAtIndex:k] == (id)[NSNull null] ||[[myWords objectAtIndex:k] length] ==0 || [[myWords objectAtIndex:k] isEqualToString:@"(null)"])
                {
                    lastname = @"";
                }
                else
                    lastname= [[NSString stringWithFormat:@"%@",[myWords objectAtIndex:k]] capitalizedString];
                
            }
            else
                lastname= [[NSString stringWithFormat:@"%@ %@",lastname,[myWords objectAtIndex:k]] capitalizedString];
        }
        surname.text=lastname;
        surname.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
        surname.textAlignment=NSTextAlignmentLeft;
        [cell addSubview:surname];
        
        
        blue_typeimg = [[UIImageView alloc]init];
        [cell addSubview:blue_typeimg];
        blue_typeimg.frame= CGRectMake([UIScreen mainScreen].bounds.size.width-42, 14, 0, 0);
        blue_typeimg.image=[UIImage imageNamed:@"blue_circle"];
        
        
        typeimg = [[UIImageView alloc]init];
        [cell addSubview:typeimg];
        typeimg.frame= CGRectMake([UIScreen mainScreen].bounds.size.width-42, 14, 22, 22);
        typeimg.image=[UIImage imageNamed:@"circle"];
        
        typeimg.userInteractionEnabled = YES;
        
        
        UIView *sep = [[UIView alloc] init];
        sep.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f];
        [cell addSubview:sep];
        sep.frame = CGRectMake(0, 49.4f, self.view.frame.size.width, 0.6f);
        
        cell.tag = indexPath.row;
        cell.selectionStyle = UITableViewCellEditingStyleNone;
        
        BOOL isSelected = [self.selectedIndexPaths containsObject:indexPath];
        if (isSelected)
        {
            
            cell.backgroundColor = [self colorWithHexString:@"DCDCDC"];
            blue_typeimg.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-42, 14, 22, 22);
            typeimg.frame= CGRectMake([UIScreen mainScreen].bounds.size.width-42, 14, 0, 0);
            inviteBtn.userInteractionEnabled = YES;
        }
        else
        {
            if (select_all) {
                
                blue_typeimg.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-42, 14, 22, 22);
                typeimg.frame= CGRectMake([UIScreen mainScreen].bounds.size.width-42, 14, 0, 0);
                inviteBtn.userInteractionEnabled = YES;
            }
            else
            {
            blue_typeimg.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-42, 14, 0, 0);
            typeimg.frame= CGRectMake([UIScreen mainScreen].bounds.size.width-42, 14, 22, 22);
            inviteBtn.userInteractionEnabled = NO;
            cell.backgroundColor = [UIColor clearColor];
            }
        }
        
    }
    
    //        ////////////////////////////////         Filter = 1          //////////////////////////////////
    else
    {
        maximumSize = CGSizeMake(160, 50);
        NSString *firstname= [[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"fullname"];
        myFont = [UIFont fontWithName:@"ProximaNova-Bold" size:17];
        
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
        myStringSize = CGSizeMake(300,NSUIntegerMax);
        NSDictionary *attributes = @{NSFontAttributeName: myFont};
        CGRect rect = [firstname boundingRectWithSize:myStringSize options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:attributes context:nil];
        name = [[UILabel alloc]init];
        name.frame = rect;
        
#else
        myStringSize = [firstname sizeWithFont:myFont constrainedToSize:maximumSize lineBreakMode:NSLineBreakByWordWrapping];
        name = [[UILabel alloc]init];
        name.frame = CGRectMake(20, 18, myStringSize.width, 14);
        
#endif
        
        [cell addSubview:name];
        name.backgroundColor=[UIColor clearColor];
        name.text=[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"fullname"];
        name.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
        name.textAlignment=NSTextAlignmentLeft;
        
        
        typeimg = [[UIImageView alloc]init];
        //[cell addSubview:typeimg];
        typeimg.frame= CGRectMake([UIScreen mainScreen].bounds.size.width-42, 14, 22, 22);
        typeimg.image=[UIImage imageNamed:@"circle"];
        
        typeimg.userInteractionEnabled = YES;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                       initWithTarget:self
                                       action:@selector(invite_fun:)];
        
        [typeimg addGestureRecognizer:tap];
        
        
        UIView *sep = [[UIView alloc] init];
        sep.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f];
        [cell addSubview:sep];
        sep.frame = CGRectMake(0, 49.4f, self.view.frame.size.width, 0.6f);
        
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DebugLog(@"DID SELECT");
    
        for (whatsapp_num in [[uniquearray objectAtIndex:indexPath.row] objectForKey:@"numbers"]) {
            
            [phonearr addObject:whatsapp_num];
        }
        whatsapp_name = [[uniquearray objectAtIndex:indexPath.row] objectForKey:@"name"];
    NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
    [self addOrRemoveSelectedIndexPath:indexPath1];
    
    if ([sendarray count] == 0)
    {
        invite_send.backgroundColor = [UIColor clearColor];
        
    }
    else
    {
    }
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 50;
}


- (void)addOrRemoveSelectedIndexPath:(NSIndexPath *)indexPath
{
    if (!self.selectedIndexPaths) {
        self.selectedIndexPaths = [NSMutableArray new];
    }
    BOOL containsIndexPath = [self.selectedIndexPaths containsObject:indexPath];
    
    if (containsIndexPath) {
        
        [self.selectedIndexPaths removeObject:indexPath];
        
        DebugLog(@"OBJECT TO REMOVE:%@",[[uniquearray objectAtIndex:indexPath.row] objectForKey:@"email"]);
        for ( NSString *str in [[uniquearray objectAtIndex:indexPath.row] objectForKey:@"email"]) {
            
            [email_array removeObject:str];
            DebugLog(@"ADDED MAIL:%@",email_array);
            
        }
        DebugLog(@"AFTER REMOVE:%@",email_array);
        if ([email_array count]>0) {
            inviteBtn.userInteractionEnabled = YES;
        }
        [tab_contact reloadRowsAtIndexPaths:@[indexPath]
                           withRowAnimation:UITableViewRowAnimationNone];
        
        [sendarray removeObject:[[uniquearray objectAtIndex:indexPath.row]objectForKey:@"numbers"]];
        
        if ([email_array count]>0 || [sendarray count]>0) {
            inviteBtn.userInteractionEnabled = YES;
        }
        
    }else{
        
        if (select_all) {
            
            
        }
        else
        {
            [self.selectedIndexPaths addObject:indexPath];
            
            for ( NSString *str in [[uniquearray objectAtIndex:indexPath.row] objectForKey:@"email"]) {
                
                [email_array addObject:str];
                DebugLog(@"ADDED MAIL:%@",email_array);
            }
            
            
            [tab_contact reloadRowsAtIndexPaths:@[indexPath]
                               withRowAnimation:UITableViewRowAnimationNone];
            
            
            [sendarray addObject:[[uniquearray objectAtIndex:indexPath.row]objectForKey:@"numbers"]];
            
        }
        
        
    }
    
    if ([sendarray count] == 0)
    {
    }
    else
    {
    }
}


+(void)chngpostion
{
    DebugLog(@"Change Position profile page");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Data" object:Nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getData:) name:@"Data" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(navtopage:) name:@"pagenav" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestreceivedaction) name:@"Requestreceived_push" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(acceptedrequestaction) name:@"Accepted_push" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AcceptedRequest) name:@"Accepted_request" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AddFriend) name:@"NewConnection" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ConnectionType) name:@"NewConnectionType" object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AcceptedRequest) name:@"TypeChange" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shareLocation) name:@"ShareLocation" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdateInformation) name:@"UpdateInfo" object:nil];
    
}

-(void)UpdateInformation
{
    ConPersonalProfileViewController *PVC = [[ConPersonalProfileViewController alloc]init];
    PVC.toUpdateInfo = @"YES";
    [self.navigationController pushViewController:PVC animated:YES];
  //  [[NSNotificationCenter defaultCenter] postNotificationName:@"Update Info" object:Nil];
    
}


-(void)shareLocation
{
    ConLocateGroupViewController *conLocate = [[ConLocateGroupViewController alloc]init];
    conLocate.group_id=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"sharedLocGrpId"]];
    [self.navigationController pushViewController:conLocate animated:YES];
}

-(void)ConnectionType
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDel.newrequestRedirect)
    {
  
    ConNewRequestsViewController *mng = [[ConNewRequestsViewController alloc]init];
       mng.userid = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userforrequest"] intValue];
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
        
    appDel.newrequestRedirect = NO;
    
 //   [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:mng animated:YES];
        
    }
}


-(void)AddFriend
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDel.newUser)
    {
    
    ConAddFriendViewController *con = [[ConAddFriendViewController alloc]init];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
        
        appDel.newUser = NO;
    
 //   [[self navigationController].view.layer addAnimation:transition forKey:nil];
    [[self navigationController] pushViewController:con animated:YES];
        
    }
}

-(void)AcceptedRequest
{
    
    if (del.profileRedirect)
    {
    
    ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
    // con.other=@"yes";
    con.request = YES;
    [self.tabBarController.tabBar setHidden:YES];
    // con.uid=[NSString stringWithFormat:@"%d",[userid intValue]];
    //                [self.navigationController presentViewController:con animated:NO completion:nil];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
        
        del.profileRedirect = NO;
    
  //  [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
        
    }
    
}

-(void)requestreceivedaction
{
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
    ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
    con.other=@"yes";
    con.uid=[NSString stringWithFormat:@"%@",del.pushtoprofileid];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}

-(void)acceptedrequestaction
{
    ConRequestViewController *con = [[ConRequestViewController alloc]init];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}


-(void)navtopage: (NSNotification *)notification
{
    DebugLog(@"navtopage");
    NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
    if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"4"])
    {
        ConAccountSettingsViewController *con = [[ConAccountSettingsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"6"])
    {
        ConInviteViewController *con = [[ConInviteViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"5"])
    {
        ConSyncLoaderViewController *con = [[ConSyncLoaderViewController alloc]init];
        //        [self.navigationController pushViewController:con animated:NO];
        
        [self presentViewController:con animated:NO completion:nil];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"2"])
    {
        ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"3"])
    {
        ConQROptionsViewController *con = [[ConQROptionsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    
    [mainview setFrame:CGRectMake(0, mainview.frame.origin.y, mainview.frame.size.width, mainview.frame.size.height)];
    //    [prefs setObject:@"111" forKey:@"whichpage"];
}


-(void)getData:(NSNotification *)notification {
    
    if(move == 0) {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-40, (mainview.frame.origin.y), mainview.frame.size.width, mainview.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             move=1;
                         }];
    } else {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake(0, (mainview.frame.origin.y), mainview.frame.size.width, mainview.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             move=0;
                         }];
    }
}


-(void)chatfun:(UIButton *)sender
{
    DebugLog(@"button tag: %ld",(long)sender.tag);
    phonesarr=[[NSMutableArray alloc]init];
    NSString *mobile_str,*business_str;
    NSInteger section = (sender.tag)/1000;
    NSInteger row = (sender.tag)%1000;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
    
    NSString *sectionTitle = [dictkeysarr objectAtIndex:indexPath.section];
    NSArray *sectionAnimals = [add_contacts_dict objectForKey:sectionTitle];
    
    if ([[[sectionAnimals objectAtIndex:indexPath.row] objectForKey:@"surname"] length]> 0)
    {
        mobile_str = [NSString stringWithFormat:@"%@%@",[[sectionAnimals objectAtIndex:indexPath.row]objectForKey:@"mobile_pre"],[[sectionAnimals objectAtIndex:indexPath.row]objectForKey:@"mobile_num"]];
        if ([mobile_str length] > 3)
            [phonesarr addObject:mobile_str];
        business_str = [NSString stringWithFormat:@"%@%@",[[sectionAnimals objectAtIndex:indexPath.row]objectForKey:@"b_phone_pre"],[[sectionAnimals objectAtIndex:indexPath.row]objectForKey:@"b_phone_num"]];
        if ([business_str length] > 3)
            [phonesarr addObject:business_str];
    }
    else
    {
        phonesarr = [[sectionAnimals objectAtIndex:indexPath.row]objectForKey:@"numbers"];
        if ([phonesarr count] == 0)
        {
            alert = [[UIAlertView alloc] initWithTitle:@"No Email found for this user!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
        }
    }
    DebugLog(@"phones arr: %@",phonesarr);
    anActionSheet = [[UIActionSheet alloc] initWithTitle:@"Options" delegate:self
                                       cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil, nil];
    
    for (int i = 0; i < [phonesarr count]; i++)
        [anActionSheet addButtonWithTitle:[phonesarr objectAtIndex:i]];
    
    anActionSheet.cancelButtonIndex = [phonesarr count];
    [anActionSheet addButtonWithTitle:@"Cancel"];
    anActionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [anActionSheet showInView:mainview];
    anActionSheet.tag=2;
}

-(void)messagefun: (UIButton *)sender
{
    DebugLog(@"button tag: %ld",(long)sender.tag);
    DebugLog(@"button tag: %ld",(long)sender.tag);
    phonesarr=[[NSMutableArray alloc]init];
    NSInteger section = (sender.tag)/1000;
    NSInteger row = (sender.tag)%1000;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
    
    NSString *sectionTitle = [dictkeysarr objectAtIndex:indexPath.section];
    NSArray *sectionAnimals = [add_contacts_dict objectForKey:sectionTitle];
    
    if ([[[sectionAnimals objectAtIndex:indexPath.row] objectForKey:@"surname"] length]> 0)
    {
        NSString *app_mail = [[sectionAnimals objectAtIndex:indexPath.row]objectForKey:@"email"];
        if ([app_mail isKindOfClass:[NSNull class]] || app_mail == (id)[NSNull null] || [app_mail length] == 0 || [app_mail isEqualToString:@"(null)"])
        {
        }
        else
            [phonesarr addObject:app_mail];
        if ([phonesarr count] == 0)
        {
            alert = [[UIAlertView alloc] initWithTitle:@"No Email found for this user!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
        }
        else
        {
            anActionSheet = [[UIActionSheet alloc] initWithTitle:@"Options" delegate:self
                                               cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil, nil];
            
            for (int i = 0; i < [phonesarr count]; i++)
                [anActionSheet addButtonWithTitle:[phonesarr objectAtIndex:i]];
            
            anActionSheet.cancelButtonIndex = [phonesarr count];
            [anActionSheet addButtonWithTitle:@"Cancel"];
            anActionSheet.actionSheetStyle = UIActionSheetStyleDefault;
            [anActionSheet showInView:mainview];
            anActionSheet.tag=3;
        }
    }
    else
    {
        phonesarr= [[sectionAnimals objectAtIndex:indexPath.row]objectForKey:@"email"];
        if ([phonesarr count] == 0)
        {
            alert = [[UIAlertView alloc] initWithTitle:@"No Email found for this user!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
        }
        else
        {
            anActionSheet = [[UIActionSheet alloc] initWithTitle:@"Options" delegate:self
                                               cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil, nil];
            
            for (int i = 0; i < [phonesarr count]; i++)
                [anActionSheet addButtonWithTitle:[phonesarr objectAtIndex:i]];
            
            anActionSheet.cancelButtonIndex = [phonesarr count];
            [anActionSheet addButtonWithTitle:@"Cancel"];
            anActionSheet.actionSheetStyle = UIActionSheetStyleDefault;
            [anActionSheet showInView:mainview];
            anActionSheet.tag=3;
        }
    }
    DebugLog(@"phones arr: %@",phonesarr);
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    DebugLog(@"button index: %ld",(long)buttonIndex);
    
    if (actionSheet.tag==1)
    {
        DebugLog(@"actionsheet call");
        if (buttonIndex != [phonesarr count])
        {
            UIDevice *device = [UIDevice currentDevice];
            if ([[device model] isEqualToString:@"iPhone"] ) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString: [NSString stringWithFormat:@"tel:%@",[phonesarr objectAtIndex:buttonIndex]]]];
            } else {
                UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [notPermitted show];
            }
        }
    }
    if (actionSheet.tag==2)
    {
        DebugLog(@"actionsheet sms");
        if (buttonIndex != [phonesarr count])
        {
            UIDevice *device = [UIDevice currentDevice];
            if ([[device model] isEqualToString:@"iPhone"] ) {
                
                MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
                
                if([MFMessageComposeViewController canSendText]) {
                    
                    controller.body = @"";
                    controller.recipients = [phonesarr objectAtIndex:buttonIndex];
                    controller.messageComposeDelegate = self;
                    [self.navigationController presentViewController:controller animated:YES completion:nil];
                }
            }
            else
            {
                UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [notPermitted show];
            }
            
        }
    }
    
    if (actionSheet.tag==3)
    {
        DebugLog(@"actionsheet mail");
        
        if (buttonIndex != [phonesarr count])
        {
            if ([MFMailComposeViewController canSendMail]==YES)
            {
                MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
                controller.mailComposeDelegate = self;
                if (self.title)
                    [controller setSubject:@""];
                
                NSMutableString *emailBody = [[NSMutableString alloc] initWithString:@"<html><body>"];
                
                //            [emailBody appendString:[NSString stringWithFormat:@"<p>via PXC </p>"]];
                //
                //            [emailBody appendString:@"</body></html>"];
                NSArray *toRecipients = [NSArray arrayWithObjects:[phonesarr objectAtIndex:buttonIndex],nil];
                [controller setToRecipients:toRecipients];
                
                [controller setMessageBody:emailBody isHTML:YES];
                
                [self.navigationController presentViewController:controller animated:YES completion:nil];
#if !__has_feature(objc_arc)
                [controller release];
                [emailBody release];
#endif
            }
            
            else
            {
                NSString *deviceType        = [UIDevice currentDevice].model;
                alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                   message:[NSString stringWithFormat:@"Your %@ must have an email account set up",deviceType ]
                                                  delegate:nil
                                         cancelButtonTitle:@"Ok"
                                         otherButtonTitles:nil];
                [alert show];
#if !__has_feature(objc_arc)
                [alert release];
#endif
            }
        }
    }
    
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    DebugLog (@"mail finished");
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    if (result == MessageComposeResultCancelled) {
        
        DebugLog(@"Message cancelled");
        
    } else if (result == MessageComposeResultSent) {
        
        DebugLog(@"Message sent");
    }
    
}


- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBarlocal
{
    searchBarlocal.showsCancelButton=YES;
    return YES;
}

- (BOOL)searchBarDidBeginEditing:(UISearchBar *)searchBarlocal
{
    searchBarlocal.showsCancelButton=YES;
    //    searchview.hidden=NO;
    [searchBarlocal becomeFirstResponder];
    
    return YES;
}

- (BOOL)searchBarDidEndEditing:(UISearchBar *)searchBarlocal
{
    //    searchview.hidden=YES;
    searchBarlocal.showsCancelButton=NO;
    filter=0;
    [searchBarlocal resignFirstResponder];
    searchBarlocal.text=@"";
    return YES;
}

- (BOOL)searchBar:(UISearchBar *)searchBarlocal shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return YES;
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBarlocal
{
    searchBarlocal.showsCancelButton = NO;
    [searchBarlocal resignFirstResponder];
    if([st length] >= 3)
        filter = 1;
    else
        filter=0;
    //    searchview.hidden=YES;
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBarlocal
{
    //    searchview.hidden=YES;
    [searchBarlocal resignFirstResponder];
    searchBarlocal.text=@"";
    searchBarlocal.showsCancelButton=NO;
    filter=NO;
    [tab_contact reloadData];
    tab_contact.hidden=NO;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    filter = YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    DebugLog(@"Text change - %@",searchText);
    //    searchview.hidden=NO;
    whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    st = [searchText stringByTrimmingCharactersInSet:whitespace];
    //Remove all objects first.
    [filtered_arr removeAllObjects];
    DebugLog(@"length of text: %lu",(unsigned long)st.length);
    if([st length] >= 3) {
        filter = 1;
        
        //        NSPredicate *resultPredicate = [NSPredicate
        //                                        predicateWithFormat:@"(name contains[c] %@) OR (username CONTAINS[c] %@)",
        //                                        searchText,searchText];
        //        //       [NSPredicate predicateWithFormat:@"(first CONTAINS[c] %@) OR (last CONTAINS[c] %@)", keyword, keyword]
        //        searchresults = [con_array filteredArrayUsingPredicate:resultPredicate];
        
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=search&q=%@&access_token=%@&device_id=%@&thumb=true",[st stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
        
        DebugLog(@"profile url: %@",urlString1);
        
        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
        
        NSError *error=nil;
        
        json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                 
                                                options:kNilOptions
                 
                                                  error:&error];
        //    DebugLog(@"json returns: %@",json1);
        
        NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
        DebugLog(@"err  %@",errornumber);
        
        if (![errornumber isEqualToString:@"0"])
        {
            DebugLog(@"if if");
            NSString *err_str = [json1 objectForKey:@"error"];
            alert = [[UIAlertView alloc] initWithTitle:@"Error in Retrieving Data!"
                                               message:err_str
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
        }
        else
        {
            filtered_arr = [[NSMutableArray alloc]init];
            if ([[json1 objectForKey:@"details"] isKindOfClass:[NSNull class]] || [json1 objectForKey:@"details"] == (id)[NSNull null] ||[[json1 objectForKey:@"details"] count] ==0)
            {
                DebugLog(@"Filtered Contacts = 0");
            }
            else
            {
                NSDictionary *contacts_temp_dict = [json1 objectForKey:@"details"];
                for (NSDictionary *dict in [contacts_temp_dict objectForKey:@"search"])
                {
                    [filtered_arr addObject:dict];
                }
                DebugLog(@"filtered array : %@",filtered_arr);
            }
        }
        //        filtered_arr=[[NSMutableArray alloc] initWithArray:searchresults];
        
        [tab_contact reloadData];
        if ([filtered_arr count] ==0)
        {
            [filtered_arr removeAllObjects];
            tab_contact.hidden=YES;
            nouserlb.hidden=NO;
        }
        else
        {
            nouserlb.hidden=YES;
            tab_contact.hidden=NO;
        }
    }
    else {
        filter = 0;
        [filtered_arr removeAllObjects];
        nouserlb.hidden=YES;
        tab_contact.hidden=NO;
        [tab_contact reloadData];
    }
}

-(void)invite_all: (id)sender
{
    if ([_request_check isEqualToString:@"whatsapp"]) {
        
        [self whatsapp];
    }
    else
    {
    if (select_all) {
        
        if ([_request_check isEqualToString:@"email"]) {
            
            [self email];
        }
        if ([_request_check isEqualToString:@"message"]) {
            
            [self message];
            
        }
        
        
        
        select_all = FALSE;
    }
    else
    {
    
    if ([sendarray count] == 0 && [email_array count]==0)
    {
        
        for (int p =0; p < [uniquearray count]; p++)
        {
            [sendarray addObject:[[uniquearray objectAtIndex:p]objectForKey:@"numbers"]];
            [self addOrRemoveSelectedIndexPath:nil];
        }
        for (int i=0; i< [sendarray count]; i++)
        {
            NSArray *xyz = [sendarray objectAtIndex:i];
            for (int q=0; q< [xyz count]; q++)
            {
                if ([[xyz objectAtIndex:q] length]>16) {
                    
                }
                else
                {
                [transfer_array addObject:[xyz objectAtIndex:q]];
                }
            }
        }
        
        DebugLog(@"sendarray all: %@",transfer_array);
        DebugLog(@"SELECT ALL:%@",sendarray);
        alert = [[UIAlertView alloc] initWithTitle:@"Do You Want to Invite All Your Contacts to Budnav?"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"Cancel"  otherButtonTitles:@"Ok", nil];
        alert.tag=1;
        [alert show];
    }
    else
    {
        DebugLog(@"sendarray some: %@",sendarray);
        for (int i=0; i< [sendarray count]; i++)
        {
            NSArray *xyz = [sendarray objectAtIndex:i];
            for (int q=0; q< [xyz count]; q++)
            {
                if ([[xyz objectAtIndex:q] length]>16) {
                    
                }
                else
                {
                    [transfer_array addObject:[xyz objectAtIndex:q]];
                }
            }
        }
        
        DebugLog(@"transfer array: %@",transfer_array);
        if ([_request_check isEqualToString:@"email"]) {
            [self email];
        }
        if ([_request_check isEqualToString:@"message"]) {
            [self message];
            //[self performSelector:@selector(message)
                       //withObject:nil
                      // afterDelay:2.0f];
        }
        
    }
    self.selectedIndexPaths = [NSMutableArray new];
    [transfer_array removeAllObjects];
    sendarray = [NSMutableArray new];
    [tab_contact reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
    }
    }
}

-(void)select_all
{
    if (select_all) {
        select_all = FALSE;
        
        self.selectedIndexPaths = [NSMutableArray new];
        [sendarray removeAllObjects];
        [transfer_array removeAllObjects];
        [email_array removeAllObjects];
        [tab_contact reloadData];
    }
    else
    {
        select_all = TRUE;
    
        [tab_contact reloadData];
        
        for (int p =0; p < [uniquearray count]; p++)
        {
            [sendarray addObject:[[uniquearray objectAtIndex:p]objectForKey:@"numbers"]];
            [self addOrRemoveSelectedIndexPath:nil];
            
            for ( NSString *str in [[uniquearray objectAtIndex:p] objectForKey:@"email"]) {
                
                [email_array addObject:str];
            }
        }
        for (int i=0; i< [sendarray count]; i++)
        {
            NSArray *xyz = [sendarray objectAtIndex:i];
            for (int q=0; q< [xyz count]; q++)
            {
                if ([[xyz objectAtIndex:q] length]>16) {
                    
                }
                else
                {
                    [transfer_array addObject:[xyz objectAtIndex:q]];
                }
            }
        }
    
    

    
        DebugLog(@"sendarray all: %@",transfer_array);
        DebugLog(@"SELECT ALL:%@",sendarray);
    
}

    
}

-(void) invite_fun: (UIGestureRecognizer *)sender
{

    DebugLog(@"row is: %ld",(long)sender.view.tag);
    alert = [[UIAlertView alloc] initWithTitle:@"Do You Want to Invite This Contact to Contacter?"
                                       message:nil
                                      delegate:self
                             cancelButtonTitle:@"Cancel"  otherButtonTitles:@"Ok", nil];
    alert.delegate=self;
    alert.tag=2;
    [alert show];
    
    dictsms = [[uniquearray objectAtIndex:sender.view.tag]objectForKey:@"numbers"];
    smstr = [dictsms objectAtIndex:0];
    DebugLog(@"%@",smstr);
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (alertView.tag) {
        case 1:
            switch (buttonIndex) {
                case 0:
                {
                    DebugLog(@"case11");
                    [transfer_array removeAllObjects];
                    [sendarray removeAllObjects];
                }
                    break;
                case 1:
                {
                    DebugLog(@"case12");
                    
                    DebugLog(@"sendarray some: %@",sendarray);
                    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
                    
                    if([MFMessageComposeViewController canSendText])
                    {
                        controller.body = @"This is an invitation to Contacter IOS App";
                        controller.recipients = [NSArray arrayWithArray:sendarray];
                        
                        controller.messageComposeDelegate = self;
                        [self presentViewController:controller animated:YES completion:Nil];
                        [transfer_array removeAllObjects];
                        [sendarray removeAllObjects];
                    }
                }
                    break;
            }
            break;
            
        case 2:
        {
            switch (buttonIndex) {
                case 0:
                {
                    DebugLog(@"case1");
                    [transfer_array removeAllObjects];
                    [sendarray removeAllObjects];
                }
                    break;
                case 1:
                {
                    DebugLog(@"case2");
                    
                    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
                    
                    if([MFMessageComposeViewController canSendText])
                    {
                        controller.body = @"This is an invitation to Contacter IOS App";
                        
                        NSString *smsstring= [NSString stringWithFormat:@"%@",[dictsms objectAtIndex:0]];
                        controller.recipients = [NSArray arrayWithObjects:smsstring, nil];
                        
                        controller.messageComposeDelegate = self;
                        
                        //[self presentModalViewController:controller animated:YES];
                        
                        [self presentViewController:controller animated:YES completion:Nil];
                    }
                }
                    break;
            }
            [sendarray removeAllObjects];
            [transfer_array removeAllObjects];
        }
            break;
        default:
            break;
    }
    
}

-(void)finishBtn:(UIButton *)sender{
    
    DebugLog(@"FINISH %@",sendarray);
    ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"searchfriends"];
    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"searchafterreg"];
}

-(void) goback//: (id)sender
{
    int index;
    NSArray* navarr = [[NSArray alloc] initWithArray:self.navigationController.viewControllers];
    for(int i=0 ; i<[navarr count] ; i++)
    {
        if(![[navarr objectAtIndex:i] isKindOfClass:NSClassFromString(@"ConInviteViewController")])
        {
            index = i;
        }
    }
    
    del.tabBarController.tabBar.hidden = NO;
    CATransition *transition = [CATransition animation];
    
    transition.duration = 0.4f;
    
    transition.type = kCATransitionFade;
    
    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

-(void)email
{
    int blank_mail = 0;
    DebugLog(@"EMAIL ARRAY:%@",email_array);
    
    for (NSString *str in email_array) {
        
        if (![str isEqualToString:@""]) {
            
            blank_mail++;
        }
    }
    
    if (blank_mail>0) {
        
        if ([MFMailComposeViewController canSendMail]==YES)
        {
            MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
            controller.mailComposeDelegate = self;
            if (self.title)
                [controller setSubject:@""];
            
            NSMutableString *emailBody = [[NSMutableString alloc] initWithString:@"Discover Budnav - The Smart Phone Book. Download now https://budnav.com/dl/"];
            
            NSArray *toRecipients = email_array;
            DebugLog(@"TO RECIPIENT:%@",toRecipients);
            [controller setToRecipients:toRecipients];
            [controller setSubject:@"Invite to Budnav"];
            [controller setMessageBody:emailBody isHTML:YES];
            
            [self.navigationController presentViewController:controller animated:YES completion:nil];
#if !__has_feature(objc_arc)
            [controller release];
            [emailBody release];
            
#endif
            
            [tab_contact removeFromSuperview];
        }
        
        else
        {
            NSString *deviceType = [UIDevice currentDevice].model;
            alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                               message:[NSString stringWithFormat:@"Your %@ must have an email account set up",deviceType ]
                                              delegate:nil
                                     cancelButtonTitle:@"Ok"
                                     otherButtonTitles:nil];
            [alert show];
#if !__has_feature(objc_arc)
            [alert release];
#endif
        }
}
else
{
    UIAlertView *emailAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"No Email found for this user!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [emailAlert show];
}

}

-(void)message
{
//    if (buttonIndex != [phonesarr count])
//    {
    
    
        UIDevice *device = [UIDevice currentDevice];
        if ([[device model] isEqualToString:@"iPhone"] ) {
            
            MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
            
            if([MFMessageComposeViewController canSendText]) {
                
                DebugLog(@"SELECT ALL ALL:%@",transfer_array);
                
                if (controller) {
                    controller = nil;
                    controller = [[MFMessageComposeViewController alloc]init];
                }
                controller.recipients = [NSArray arrayWithArray:(NSArray*)transfer_array];
                controller.messageComposeDelegate = self;
                controller.body = @"Discover Budnav - The Smart Phone Book. Download now https://budnav.com/dl/";
                //isMessageComposeAppear = 1;
                [self presentViewController:controller animated:YES completion:nil];
                 [tab_contact removeFromSuperview];
            }
            
        }
        else
        {
            UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [notPermitted show];
        }
        
   // }
    
}

-(void)whatsapp
{
    DebugLog(@"profile name field text: %@",whatsapp_name);
    // Fetch the address book
    CFErrorRef *errorab = nil;
    
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, errorab);
    
       
    CFArrayRef people = ABAddressBookCopyPeopleWithName(addressBook,
                                                        (__bridge CFStringRef)whatsapp_name);
    
    // Display "Appleseed" information if found in the address book
    if ((people != nil) && (CFArrayGetCount(people) > 0))
    {
        for (int m = 0; m < (CFArrayGetCount(people)); m++){
            DebugLog(@"count e dichhe: %ld == %@", (CFArrayGetCount(people)),people);
            ABRecordRef person = CFArrayGetValueAtIndex(people, 0);
            
            ABRecordID recordID = ABRecordGetRecordID(person);
            
            DebugLog(@"ab id: %d",recordID);
            DebugLog(@"phone array; %@", whatsapp_num);
            
            ABMultiValueRef phoneNumberProperty = ABRecordCopyValue(person, kABPersonPhoneProperty);
            NSArray *_phoneNumbers = (__bridge NSArray*)ABMultiValueCopyArrayOfAllValues(phoneNumberProperty);
            
            NSMutableArray *mutableph= [_phoneNumbers mutableCopy];
            int pp;
            for (pp =0; pp< [mutableph count]; pp++)
            {
                if ([[mutableph objectAtIndex:pp] isKindOfClass:[NSNull class]] || [mutableph objectAtIndex:pp] == (id)[NSNull null] || [[mutableph objectAtIndex:pp] length] < 3)
                {
                    [mutableph removeObjectAtIndex:pp];
                    pp= pp-1;
                }
            }
            NSMutableSet* set1 = [NSMutableSet setWithArray:mutableph];
            NSMutableSet* set2 = [NSMutableSet setWithArray:phonearr];
            [set1 intersectSet:set2]; //this will give you only the obejcts that are in both sets
            
            NSArray* result = [set1 allObjects];
            DebugLog(@"result pelo: %@",result);
            
            if ([result count] > 0)
            {
                NSString * urlWhats = [NSString stringWithFormat:@"whatsapp://send?abid=%d&text=Discover Budnav - The Smart Phone Book. Download now https://budnav.com/dl/",recordID];
                NSURL * whatsappURL = [NSURL URLWithString:[urlWhats stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
                    [[UIApplication sharedApplication] openURL: whatsappURL];
                } else {
                    UIAlertView * alert1 = [[UIAlertView alloc] initWithTitle:@"WhatsApp not installed." message:@"Your device has no WhatsApp installed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert1 show];
                }
                break;
            }
        }
    }
    else
    {
        // Show an alert if "Appleseed" is not in Contacts
    }
    //CFRelease(addressBook);
    //CFRelease(people);
}

-(void)changecoloragain
{
    helpbt.alpha = 1.5f;
    bck_img.alpha = 1.5f;
}
-(void)changecolor
{
    helpbt.alpha = 0.5f;
    bck_img.alpha = 0.5f;
}


@end