//
//  Mymodel.m
//  Randomtest
//
//  Created by Soumalya Banerjee on 10/8/13.
//  Copyright (c) 2013 Esolz. All rights reserved.
//

#import "Mymodel.h"

@implementation Mymodel

@synthesize str,db_busy;
static Mymodel *instance =nil;

+(Mymodel *)getInstance
{
    @synchronized(self)
    {
        if(instance==nil)
        {
            
            instance= [Mymodel new];
        }
    }
    return instance;
}

@end
