//
//  ConPictureProfViewController.h
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 09/07/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface ConPictureProfViewController : GAITrackedViewController
@property(nonatomic,retain)UIImage *profilepic;
@end
