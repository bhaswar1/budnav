//
//  ConDetailsViewController.h
//  Contacter
//
//  Created by ios on 10/01/15.
//  Copyright (c) 2015 Esolz. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "ConParentTopBarViewController.h"
#import "ConSettingsView.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "Mymodel.h"
#import "GroupDetailsViewController.h"
#import "GAITrackedViewController.h"

@interface ConDetailsViewController : GAITrackedViewController<UIGestureRecognizerDelegate,MKMapViewDelegate, NSURLConnectionDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,UIScrollViewDelegate,UIActionSheetDelegate,UIAlertViewDelegate,CLLocationManagerDelegate>
{
    MKMapView *map_View;
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    MFMailComposeViewController *mailComposer;
    UIScrollView *mainscroll,*mainscroll_busi;
    UIActionSheet *anActionSheet;
}
@property (nonatomic, strong) NSString *groupid,*groupname,*isadmin,*other,*uid, *mapReturn,*response,*userid,*membercount;
@property (nonatomic, strong) UIView *mainview;
@property(nonatomic,retain) NSString *getmapvalue;
@property(nonatomic,retain) NSMutableDictionary *personal_dict, *business_dict, *social_dict;
@property(nonatomic,retain) NSMutableArray *personal_array, *business_array, *social_array, *phonearr,*filter_arr,*search;
@property(nonatomic,retain) NSDictionary *dict_profile,*profdict;
@property(nonatomic,retain)UIButton *backBtn;
@property(nonatomic,assign) int p;
+(void)chngpostion;

@end
