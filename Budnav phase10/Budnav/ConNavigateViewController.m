//
//  ConNavigateViewController.m
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 09/06/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//
#import "AppDelegate.h"
#import "ConNavigateViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

@interface ConNavigateViewController ()<UIAlertViewDelegate,UIWebViewDelegate>
{
    UIAlertView *alert;
    UIView *topbar;
}
@end

@implementation ConNavigateViewController
@synthesize currlat,currlong,destlat,destlong,fireurl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIActivityIndicatorView *act = [[UIActivityIndicatorView alloc] init];
    act.center=self.view.center;
    [act startAnimating];
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [act setColor:[UIColor grayColor]];
    [self.view addSubview:act];
    
    //    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fb://"]]) {
    //        // Facebook app is installed
    //    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    //    [del showTabValues:YES];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    self.navigationController.navigationBarHidden=YES;
    
    topbar = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 65)];
    [self.view addSubview:topbar];
    topbar.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"topbar.png"]];
    
    UIImageView *logo_img = [[UIImageView alloc]initWithFrame:CGRectMake(99, 23, 122, 25)];
    logo_img.image=[UIImage imageNamed:@"logo.png"];
    [topbar addSubview:logo_img];
    
    UIButton *helpbt = [UIButton buttonWithType:UIButtonTypeCustom];
    helpbt.frame = CGRectMake(12, 24, 11, 21);
    helpbt.backgroundColor=[UIColor clearColor];
    [helpbt setBackgroundImage:[UIImage imageNamed:@"back_white1.png"] forState:UIControlStateNormal];
    [helpbt addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:helpbt];
    
    UIView *backbuttonextended = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 85, 60)];
    backbuttonextended.backgroundColor=[UIColor clearColor];
    [self.view addSubview:backbuttonextended];
    backbuttonextended.userInteractionEnabled=YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goBack)];
    [backbuttonextended addGestureRecognizer:tap];
    
    NSURL *theURL = [NSURL URLWithString: fireurl];
    NSURLRequest *request = [NSURLRequest requestWithURL: theURL];
    
    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0,65,[UIScreen mainScreen].bounds.size.width,self.view.frame.size.height-65)];
    webView.scalesPageToFit = YES;
    [webView setDelegate: self];
    [webView loadRequest: request];
    [self.view addSubview:webView];
    
    [del showTabValues:YES];
}

-(void)goBack//:(id)sender
{
    int index;
    NSArray* navarr = [[NSArray alloc] initWithArray:self.navigationController.viewControllers];
    for(int i=0 ; i<[navarr count] ; i++)
    {
        if(![[navarr objectAtIndex:i] isKindOfClass:NSClassFromString(@"ConNavigateViewController")])
        {
            index = i;
        }
    }
    
    
    CATransition *transition = [CATransition animation];
    
    transition.duration = 0.4f;
    
    transition.type = kCATransitionFade;
    
    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    [self.navigationController popToViewController:[navarr objectAtIndex:index] animated:YES];
}
@end