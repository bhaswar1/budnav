//
//  ConRequestViewController.m
//  Contacter
//  Created by Bhaswar's MacBook Air on 08/05/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
#import "ConInviteViewController.h"
#import "ConRequestViewController.h"
#import "AppDelegate.h"
#import "ConProfileOwnViewController.h"
#import "ConAccountSettingsViewController.h"
#import "SVProgressHUD.h"
#import "DBManager.h"
#import "ConQROptionsViewController.h"
#import "ConSyncLoaderViewController.h"
#import "ConGroupsViewController.h"
#import "GroupDetailsViewController.h"
#import "ConNewContactsViewController.h"
#import "ConNewProfileViewController.h"
#import "ConAddFriendViewController.h"
#import "ConNewRequestsViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "ConLocateGroupViewController.h"
#import "ConFullSettingsViewController.h"
#import "ConPersonalProfileViewController.h"


@interface ConRequestViewController ()<UIGestureRecognizerDelegate, UITextViewDelegate, CLLocationManagerDelegate, UIAlertViewDelegate>
{
    //   UIView *mainview;
    int move, hours, minutes, seconds, count;
    UILabel *nouserlb;
    UIActivityIndicatorView *act, *act1;
    NSMutableArray *groupid_array,*index_array,*indexgroup_array;
    UIImageView *profile_imgv, *logo_img;
    NSString *err_str1, *base64String, *userid;
    UIRefreshControl *refreshcontrolnew;
    NSMutableAttributedString *refreshString;
    NSData *decodedData;
    UIButton *accept, *deny_notnow;
    UITextView *notification_message;
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    NSString *latitude, *longitude;
    BOOL get_location;
    int shareTag;
    AppDelegate *del;
}
@end

@implementation ConRequestViewController
@synthesize mainview;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void)gettingPush
{
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:3.0f target:self
                                                    selector:@selector(reloadPage)
                                                    userInfo:nil
                                                     repeats:NO];
}

-(void)reloadPage
{
    ConRequestViewController *con = [[ConRequestViewController alloc]init];
    self.tabBarController.selectedIndex = 3;
    [self.navigationController pushViewController:con animated:NO];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    DebugLog(@"requests bm11");
    
    self.view.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
    
    del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    [del showTabValues:YES];
    
    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    
//    [[del.tabBarController.viewControllers objectAtIndex:3] tabBarItem].badgeValue = nil;
    
    self.navigationController.navigationBarHidden=YES;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    
    [[del.tabBarController.viewControllers objectAtIndex:3] tabBarItem].badgeValue = nil;
    self.screenName = @"Notifications";
    
    count = 0;
    
    
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createEventWithCategory:@"Notifications"
                                             action:@"Notifications"
                                              label:nil
                                              value:nil] build]];
    
    
    self.view.frame=CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height-44);
    
    [mainview removeFromSuperview];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    move=0;
    
    del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    [del showTabValues:YES];
    
    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    
    self.navigationController.navigationBarHidden=YES;
    
    groupid_array = [NSMutableArray new];
    index_array = [NSMutableArray new];
    
    UIView *topbar = [[UIView alloc] init];
    topbar.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64);
    //topbar.backgroundColor = [UIColor blackColor];
    [self.view addSubview:topbar];
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
    
    // Add the gradient to the view
    [topbar.layer insertSublayer:gradient atIndex:0];
    
    logo_img = [[UIImageView alloc]init];
    logo_img.frame = CGRectMake(27+([UIScreen mainScreen].bounds.size.width-160)/2, 32, 101, 20);
    logo_img.backgroundColor = [UIColor clearColor];
    
    logo_img.image = [UIImage imageNamed:@"new_logo"];
    [topbar addSubview:logo_img];
    
    
    mainview = [[UIView alloc]initWithFrame:CGRectMake(0, 65, [UIScreen mainScreen].bounds.size.width, self.view.frame.size.height-65)];
    [self.view addSubview:mainview];
    mainview.backgroundColor=[UIColor whiteColor];
    
    act = [[UIActivityIndicatorView alloc] init];
    act.center = self.view.center;
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [act setColor:[UIColor blackColor]];
    [mainview addSubview:act];
    [act startAnimating];
    
    //    [SVProgressHUD showWithStatus:@"Loading"];
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"firstRequest"] isEqualToString:@"yes"])
    {
        NSOperationQueue *queue1 = [NSOperationQueue new];
        NSInvocationOperation *op1 = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(loadrequestsdataurl) object:nil];
        [queue1 addOperation:op1];
        //        [self loadrequestsdata];
        [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"firstRequest"];
    }
    else
    {
        DebugLog(@"1st time er por theke");
        //      NSArray *retval = [[DBManager getSharedInstance]fetchRequests];
        [tab_request removeFromSuperview];
        NSOperationQueue *queue = [NSOperationQueue new];
        NSInvocationOperation *op = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(loadrequestsfromweb) object:nil];
        //            [self loadrequestsfromweb];
        [queue addOperation:op];
        
        
        //      [SVProgressHUD dismiss];
        //        [act removeFromSuperview];
        
        
    }
}

-(void)loadrequestsdataurl
{
    DebugLog(@"1st time er jonne");
    [tab_request removeFromSuperview];
    //    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=notification_get&id=%@&access_token=%@&device_id=%@&thumb=true",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
    
    DebugLog(@"requests url: %@",urlString1);
    
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    NSString *errornumber;
    if (signeddataURL1 != nil)
    {
        NSError *error=nil;
        
        json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                 
                                                options:kNilOptions
                 
                                                  error:&error];
        DebugLog(@"json returns: %@",json1);
        
        errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
        DebugLog(@"err  %@",errornumber);
        if (![errornumber isEqualToString:@"0"])
        {
            DebugLog(@"if if");
            err_str1 = [json1 objectForKey:@"error"];
            //            alert = [[UIAlertView alloc] initWithTitle:@"Error in Retrieving Data!"
            //                                               message:err_str
            //                                              delegate:self
            //                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            //            [alert show];
            //            //        [SVProgressHUD dismiss];
            //            [act removeFromSuperview];
            
            [self performSelectorOnMainThread:@selector(errorAlert) withObject:nil waitUntilDone:YES];
        }
        else
        {
            
            [self performSelectorOnMainThread:@selector(loadrequestsdata) withObject:nil waitUntilDone:YES];
        }
        
    }
    else
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        //            [alert show];
        //            [SVProgressHUD dismiss];
        [act removeFromSuperview];
    }
    
}

-(void)loadrequestsdata
{
    requestslist = [[NSMutableArray alloc]init];
    groupadminlist = [[NSMutableArray alloc]init];
    
    DebugLog(@"details req= %@",[json1 objectForKey:@"details"]);
    
    [nouserlb removeFromSuperview];
    if ([[json1 objectForKey:@"details"] isKindOfClass:[NSNull class]] || [json1 objectForKey:@"details"] == (id)[NSNull null] ||[[json1 objectForKey:@"details"] count] ==0)
    {
        DebugLog(@"if called req");
        nouserlb= [[UILabel alloc]initWithFrame:CGRectMake(10, 70, [UIScreen mainScreen].bounds.size.width-20, 60)];
        [mainview addSubview:nouserlb];
        nouserlb.numberOfLines=3;
        nouserlb.text=@"There are no new contact requests. Improve findability by completing your profile.";
        nouserlb.backgroundColor=[UIColor clearColor];
        nouserlb.font=[UIFont fontWithName:@"ProximaNova-Bold" size:16];
        nouserlb.textAlignment=NSTextAlignmentCenter;
        nouserlb.textColor=[UIColor blackColor];
        
        //    [SVProgressHUD dismiss];
        [act removeFromSuperview];
    }
    else
    {
//        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Getting Push" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
        
        DebugLog(@"else called req");
        NSDictionary *newdict= [json1 objectForKey:@"details"];
        requestslist = [[newdict objectForKey:@"notifications"] mutableCopy];
        
        //        for (NSDictionary *reqdict in [newdict objectForKey:@"requests"])
        //        {
        //            if ([[reqdict objectForKey:@"id"] intValue] != [[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]intValue])
        //                [requestslist addObject:reqdict];
        //        }
        //        for (NSDictionary *reqdict1 in [newdict objectForKey:@"requestsForGroups"])
        //        {
        //
        //            [requestslist addObject:reqdict1];
        //        }
        //        for (NSDictionary *reqdict2 in [newdict objectForKey:@"requestsToGroupAdmin"])
        //        {
        //
        //            [groupadminlist addObject:reqdict2];
        //        }
        //        DebugLog(@"group admin request list: %@",groupadminlist);
        //        DebugLog(@"requests list array has: %@",requestslist);
        //        DebugLog(@"request count of contact and group1 is %lu",(unsigned long)[requestslist count]);
        //        DebugLog(@"request count of group admin1 is %lu",(unsigned long)[groupadminlist count]);
        //
        //
        //        [[NSUserDefaults standardUserDefaults] setObject:requestslist forKey:@"yourRequests"];
        //        [[NSUserDefaults standardUserDefaults] setObject:groupadminlist forKey:@"yourGroupAdminRequests"];
        
        if (tab_request)
        {
            [tab_request removeFromSuperview];
        }
        
        tab_request=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, mainview.frame.size.height)];
        [mainview addSubview:tab_request];
        tab_request.backgroundColor=[UIColor clearColor];
        tab_request.dataSource=self;
        tab_request.delegate=self;
        tab_request.separatorStyle=UITableViewCellSeparatorStyleNone;
        tab_request.showsVerticalScrollIndicator=NO;
        
        
        
        UITableViewController *tableViewController = [[UITableViewController alloc] init];
        tableViewController.tableView = tab_request;
        
        refreshcontrolnew = [[UIRefreshControl alloc] init];
        [refreshString addAttributes:@{NSForegroundColorAttributeName : [UIColor grayColor]} range:NSMakeRange(0, refreshString.length)];
        refreshcontrolnew.attributedTitle = refreshString;
        [refreshcontrolnew addTarget:self action:@selector(reloadAgain) forControlEvents:UIControlEventValueChanged];
        tableViewController.refreshControl = refreshcontrolnew;
        refreshcontrolnew.tintColor=[UIColor grayColor];
        
        
        
        //            [SVProgressHUD dismiss];
        [act removeFromSuperview];
    }
    //            }
    del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabValues:YES];
    //        });
    //    });
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

+(void)chngpostion
{
    DebugLog(@"Change pos requests page");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Data" object:Nil];
}

-(void)viewWillAppear:(BOOL)animated{
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gettingPush) name:@"ActivePush" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getData:) name:@"Data" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(navtopage:) name:@"pagenav" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestreceivedaction) name:@"Requestreceived_push" object:nil];
    // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(acceptedrequestaction) name:@"Accepted_push" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AcceptedRequest) name:@"Accepted_request" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AddFriend) name:@"NewConnection" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ConnectionType) name:@"NewConnectionType" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AcceptedRequest) name:@"TypeChange" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shareLocation) name:@"ShareLocation" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdateInformation) name:@"UpdateInfo" object:nil];
    
}

-(void)UpdateInformation
{
    ConPersonalProfileViewController *PVC = [[ConPersonalProfileViewController alloc]init];
    PVC.toUpdateInfo = @"YES";
    [self.navigationController pushViewController:PVC animated:YES];
  //  [[NSNotificationCenter defaultCenter] postNotificationName:@"Update Info" object:Nil];
    
}


-(void)shareLocation
{
    ConLocateGroupViewController *conLocate = [[ConLocateGroupViewController alloc]init];
    conLocate.group_id=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"sharedLocGrpId"]];
    [self.navigationController pushViewController:conLocate animated:YES];
}

-(void)ConnectionType
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDel.newrequestRedirect)
    {
        
        ConNewRequestsViewController *mng = [[ConNewRequestsViewController alloc]init];
        mng.userid = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userforrequest"] intValue];
        //    CATransition* transition = [CATransition animation];
        //
        //    transition.duration = 0.4;
        //    transition.type = kCATransitionPush;
        //    transition.subtype = kCATransitionFade;
        
        appDel.newrequestRedirect = NO;
        
        //    [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:mng animated:YES];
        
    }
    
}


-(void)AddFriend
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDel.newUser)
    {
        
        ConAddFriendViewController *con = [[ConAddFriendViewController alloc]init];
        
        //    CATransition* transition = [CATransition animation];
        //
        //    transition.duration = 0.4;
        //    transition.type = kCATransitionPush;
        //    transition.subtype = kCATransitionFade;
        
        appDel.newUser = NO;
        
        //    [[self navigationController].view.layer addAnimation:transition forKey:nil];
        [[self navigationController] pushViewController:con animated:YES];
        
    }
    
}

-(void)AcceptedRequest
{
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (del.profileRedirect)
    {
        
        ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
        // con.other=@"yes";
        con.request = YES;
        [self.tabBarController.tabBar setHidden:YES];
        // con.uid=[NSString stringWithFormat:@"%d",[userid intValue]];
        //                [self.navigationController presentViewController:con animated:NO completion:nil];
        
        //    CATransition* transition = [CATransition animation];
        //
        //    transition.duration = 0.4;
        //    transition.type = kCATransitionPush;
        //    transition.subtype = kCATransitionFade;
        
        del.profileRedirect = NO;
        
        //    [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
        
    }
    
}

-(void)requestreceivedaction
{
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
    con.request = TRUE;
    con.other=@"yes";
    con.uid=[NSString stringWithFormat:@"%@",del.pushtoprofileid];
    
    //    CATransition* transition = [CATransition animation];
    //
    //    transition.duration = 0.4;
    //    transition.type = kCATransitionPush;
    //    transition.subtype = kCATransitionFade;
    //
    //    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}

-(void)acceptedrequestaction
{
    ConRequestViewController *con = [[ConRequestViewController alloc]init];
    
    //    CATransition* transition = [CATransition animation];
    //
    //    transition.duration = 0.4;
    //    transition.type = kCATransitionPush;
    //    transition.subtype = kCATransitionFade;
    //
    //    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}


-(void)navtopage: (NSNotification *)notification
{
    DebugLog(@"navtopage");
    NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
    if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"4"])
    {
        ConAccountSettingsViewController *con = [[ConAccountSettingsViewController alloc]init];
        
        //        CATransition* transition = [CATransition animation];
        //
        //        transition.duration = 0.4;
        //        transition.type = kCATransitionPush;
        //        transition.subtype = kCATransitionFade;
        //
        //        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"6"])
    {
        ConInviteViewController *con = [[ConInviteViewController alloc]init];
        
        //        CATransition* transition = [CATransition animation];
        //
        //        transition.duration = 0.4;
        //        transition.type = kCATransitionPush;
        //        transition.subtype = kCATransitionFade;
        //
        //        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"5"])
    {
        ConSyncLoaderViewController *con = [[ConSyncLoaderViewController alloc]init];
        //        [self.navigationController pushViewController:con animated:NO];
        
        
        [self presentViewController:con animated:NO completion:nil];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"2"])
    {
        ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
        con.request = TRUE;
        //        CATransition* transition = [CATransition animation];
        //
        //        transition.duration = 0.4;
        //        transition.type = kCATransitionPush;
        //        transition.subtype = kCATransitionFade;
        //
        //        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"3"])
    {
        ConQROptionsViewController *con = [[ConQROptionsViewController alloc]init];
        
        //        CATransition* transition = [CATransition animation];
        //
        //        transition.duration = 0.4;
        //        transition.type = kCATransitionPush;
        //        transition.subtype = kCATransitionFade;
        //
        //        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    
    [mainview setFrame:CGRectMake(0, mainview.frame.origin.y, mainview.frame.size.width, mainview.frame.size.height)];
    //    [prefs setObject:@"111" forKey:@"whichpage"];
}


-(void)getData:(NSNotification *)notification {
    
    if(move == 0) {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-40, mainview.frame.origin.y, mainview.frame.size.width, mainview.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             move=1;
                         }];
    } else {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake(0, mainview.frame.origin.y,mainview.frame.size.width, mainview.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             move=0;
                         }];
    }
}


#pragma  mark - TableView Delegates

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //    if (groupadminlist.count!=0)
    //    {
    //        if (requestslist.count!=0)
    //            return 2;
    //        else
    //            return 1;
    //    }
    //
    //    else
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    if (groupadminlist.count!=0)
    //    {
    //        if (requestslist.count!=0)
    //        {
    //            if (section==0)
    //                return [groupadminlist count];
    //            else
    //                return [requestslist count];
    //        }
    //        else
    //            return [groupadminlist count];
    //    }
    //    else
    return [requestslist count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (groupadminlist.count!=0)
    {
        if (requestslist.count!=0)
            return 0;
        else
        {
            if (section==0) {
                return 0;
            }
            else
                return 0;
        }
    }
    
    //    else if (requestslist.count!=0)
    //        return 20;
    else
        return 0;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *headerLabel;
    UIView* customView;
    
    if (groupadminlist.count!=0) {
        if (section==0)
        {
            // create the label
            headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
            headerLabel.backgroundColor = [UIColor clearColor];
            headerLabel.font = [UIFont fontWithName:@"ProximaNova-Bold" size:14.0];
            headerLabel.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 0);
            headerLabel.textAlignment = NSTextAlignmentCenter;
            headerLabel.text = @"Requests to groups where you are group admin.";
            headerLabel.textColor = [UIColor blackColor];
            
            // create the parent view that will hold header Label
            customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 0)];
            customView.backgroundColor = [UIColor lightGrayColor];
            [customView addSubview:headerLabel];
            
        }
        else if (section==1)
        {
            // create the label
            headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
            headerLabel.backgroundColor = [UIColor clearColor];
            headerLabel.font = [UIFont fontWithName:@"ProximaNova-Bold" size:14.0];
            headerLabel.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 0);
            headerLabel.textAlignment = NSTextAlignmentCenter;
            headerLabel.text = @"Requests for your profile.";
            headerLabel.textColor = [UIColor blackColor];
            
            // create the parent view that will hold header Label
            customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 0)];
            customView.backgroundColor = [UIColor lightGrayColor];
            [customView addSubview:headerLabel];
            
        }
        
    }
    else
    {
        headerLabel=nil;
        customView=nil;
    }
    
    return customView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tab_request dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    // cell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"profile-bg.png"]];
    
    cell.backgroundColor=[UIColor clearColor];
    
    profile_imgv = [[UIImageView alloc] init];
    profile_imgv.frame = CGRectMake(12, 12, 50, 50);
    profile_imgv.backgroundColor = [UIColor clearColor];
    profile_imgv.layer.cornerRadius = profile_imgv.frame.size.width/2;
    base64String = [[requestslist objectAtIndex:indexPath.row] objectForKey:@"thumb"];
    decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
    profile_imgv.image = [UIImage imageWithData:decodedData];
    // profile_imgv.image = [UIImage imageNamed:@"Notification"];
    [cell addSubview:profile_imgv];
    
    NSString *time;
    [self timeFormatted:[[[requestslist objectAtIndex:indexPath.row] objectForKey:@"timestamp_difference"] intValue]];
    
    if (hours>0)
    {
        if (hours>24)
        {
            int rem = hours%24;
            time = [NSString stringWithFormat:@"%dd",rem];
            
        }
        else
        {
            time = [NSString stringWithFormat:@"%dh",hours];
        }
    }
    else if (minutes>0)
    {
        time = [NSString stringWithFormat:@"%dm",minutes];
    }
    else
    {
        time = [NSString stringWithFormat:@"%ds",seconds];
    }
    
    DebugLog(@"Time:%@",time);
    //        NSString *base64String1= [[requestslist objectAtIndex:indexPath.row] objectForKey:@"thumb"];
    //        if ([base64String1 length] > 6)
    //        {
    //            NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String1 options:0];
    //            //    NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
    //            UIImage *profilepic = [UIImage imageWithData:decodedData];
    //            profile_imgv.image=profilepic;
    //            profile_imgv.contentMode= UIViewContentModeScaleAspectFill;
    //            profile_imgv.clipsToBounds=YES;
    //        }
    
    UILabel *name=[[UILabel alloc]initWithFrame:CGRectMake(profile_imgv.frame.origin.x+profile_imgv.frame.size.width+12, 9, [UIScreen mainScreen].bounds.size.width-(profile_imgv.frame.origin.x+profile_imgv.frame.size.width+10), 60)];
    name.lineBreakMode = YES;
    name.lineBreakMode = NSLineBreakByWordWrapping;
    name.numberOfLines = 2;
    name.backgroundColor=[UIColor redColor];
    name.font=[UIFont fontWithName:@"ProximaNova-Bold" size:15];
    // [cell addSubview:name];
    
    if ([[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue]==1 || [[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue]==4 || [[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue]==8)
    {
        if (![[[requestslist objectAtIndex:indexPath.row] objectForKey:@"status"] isKindOfClass:[NSNumber class]] || [[[requestslist objectAtIndex:indexPath.row] objectForKey:@"status"] isKindOfClass:[NSNull class]])
        {
            DebugLog(@"Cell Tag:%ld",(long)indexPath.row);
            accept = [UIButton buttonWithType:UIButtonTypeCustom];
            accept.frame = CGRectMake(12, 74, ([UIScreen mainScreen].bounds.size.width-36)/2, 34);
            
            accept.layer.cornerRadius = 5;
            accept.clipsToBounds = YES;
            [accept.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Bold" size:14]];
            [accept setBackgroundColor:[UIColor colorWithRed:(40.0f/255.0f) green:(170.0f/255.0f) blue:(185.0f/255.0f) alpha:1.0f]];
            [accept setTitle:@"Accept" forState:UIControlStateNormal];
            [accept setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            // [accept setBackgroundImage:[UIImage imageNamed:@"Accept"] forState:UIControlStateNormal];
            //[accept setBackgroundImage:[UIImage imageNamed:@"Accept (active)"] forState:UIControlStateHighlighted];
            //        [accept addTarget:self action:@selector(highlight:) forControlEvents:UIControlEventTouchDown];
            //        [accept addTarget:self action:@selector(highlight:) forControlEvents:UIControlEventTouchUpInside];
            [accept setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(3.0f/255.0f) green:(76.0f/255.0f) blue:(95.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
            
            
            [accept addTarget:self action:@selector(accept:) forControlEvents:UIControlEventTouchUpInside];
            accept.tag= indexPath.row;//[[[requestslist objectAtIndex:indexPath.row] objectForKey:@"id"]intValue];
            //        accept.showsTouchWhenHighlighted = YES;
            [cell addSubview:accept];
            
            deny_notnow = [UIButton buttonWithType:UIButtonTypeCustom];
            deny_notnow.frame = CGRectMake(accept.frame.origin.x+accept.frame.size.width+12, 74, ([UIScreen mainScreen].bounds.size.width-36)/2, 34);
            //[deny_notnow setBackgroundImage:[UIImage imageNamed:@"Not now"] forState:UIControlStateNormal];
            deny_notnow.layer.cornerRadius = 5;
            [deny_notnow.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Bold" size:14]];
            [deny_notnow setBackgroundColor:[UIColor colorWithRed:(234.0f/255.0f) green:(234.0f/255.0f) blue:(234.0f/255.0f) alpha:1.0f]];
            [deny_notnow setTitle:@"Not now" forState:UIControlStateNormal];
            [deny_notnow setTitleColor:[UIColor colorWithRed:(4.0f/255.0f) green:(4.0f/255.0f) blue:(4.0f/255.0f) alpha:1.0f] forState:UIControlStateNormal];
            
            // [deny_notnow setBackgroundImage:[UIImage imageNamed:@"Not now (active)"] forState:UIControlStateHighlighted];
            // [deny_notnow addTarget:self action:@selector(deny_highlight:) forControlEvents:UIControlEventTouchDown];
            [deny_notnow addTarget:self action:@selector(deny_highlight:) forControlEvents:UIControlEventTouchUpInside];
            
            [deny_notnow addTarget:self action:@selector(deny:) forControlEvents:UIControlEventTouchUpInside];
            deny_notnow.tag= indexPath.row;//[[[requestslist objectAtIndex:indexPath.row] objectForKey:@"id"]intValue];
            //        deny_notnow.showsTouchWhenHighlighted = YES;
            [cell addSubview:deny_notnow];
        }
        
        
    }
    
    //        UIImageView *typeimg = [[UIImageView alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-47, 24.5, 35, 35)];
    
    NSDictionary *font_regular = [[NSDictionary alloc]init];
    NSDictionary *time_font_regular = [[NSDictionary alloc]init];
    NSDictionary *font_medium = [[NSDictionary alloc]init];
    NSMutableAttributedString *attrString1, *attrString2;
    
    time_font_regular = @{NSFontAttributeName: [UIFont fontWithName:@"ProximaNova-Regular" size:15.0f],
                          NSForegroundColorAttributeName: [UIColor colorWithRed:(153.0f/255.0f) green:(153.0f/255.0f) blue:(153.0f/255.0f) alpha:1.0f]};
    
    font_regular = @{NSFontAttributeName: [UIFont fontWithName:@"ProximaNova-Regular" size:15.0f],
                     NSForegroundColorAttributeName: [UIColor blackColor]};
    
    font_medium = @{NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Bold" size:15.0f],
                    NSForegroundColorAttributeName:[UIColor colorWithRed:(47.0f/255.0f) green:(157.0f/255.0f) blue:(163.0f/255.0f) alpha:1.0f]};
    
    if ([[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue] == 1)
    {
        
        if ([[requestslist objectAtIndex:indexPath.row] objectForKey:@"params"]!=nil)
        {
            if ([[[[requestslist objectAtIndex:indexPath.row] objectForKey:@"params"] objectForKey:@"type"] isEqualToString:@"business"])
            {
                if ([[[requestslist objectAtIndex:indexPath.row] objectForKey:@"status"] isKindOfClass:[NSNumber class]] || ![[[requestslist objectAtIndex:indexPath.row] objectForKey:@"status"] isKindOfClass:[NSNull class]])
                {
                    attrString1 = [[NSMutableAttributedString alloc] initWithString:@"is now a connection." attributes: font_regular];
                }
                else
                {
                    attrString1 = [[NSMutableAttributedString alloc] initWithString:@"sends a business contact request." attributes: font_regular];
                }
            }
            else
            {
                if ([[[requestslist objectAtIndex:indexPath.row] objectForKey:@"status"] isKindOfClass:[NSNumber class]] || ![[[requestslist objectAtIndex:indexPath.row] objectForKey:@"status"] isKindOfClass:[NSNull class]])
                {
                    attrString1 = [[NSMutableAttributedString alloc] initWithString:@"is now a connection." attributes: font_regular];
                }
                else
                {
                    attrString1 = [[NSMutableAttributedString alloc] initWithString:@"sends a contact request." attributes: font_regular];
                }
            }
            
            // name.text=[NSString stringWithFormat:@"%@",[[requestslist objectAtIndex:indexPath.row] objectForKey:@"fullname"]];
            
            
            attrString2 = [[NSMutableAttributedString alloc] initWithString:[[NSString stringWithFormat:@"%@ ",[[[requestslist objectAtIndex:indexPath.row] objectForKey:@"params"] objectForKey:@"name"]] capitalizedString] attributes: font_medium];
            
            
        }
        else
        {
            //                attrString2= [[NSMutableAttributedString alloc] initWithString:[[NSString stringWithFormat:@"%@ ",[[requestslist objectAtIndex:indexPath.row] objectForKey:@"name"]] capitalizedString] attributes: font_medium];
        }
        
        [attrString2 appendAttributedString:attrString1];
        //            [name setAttributedText:attrString2];
        
        [attrString2 appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",time] attributes: time_font_regular]];
        
        notification_message = [[UITextView alloc] initWithFrame:CGRectMake(profile_imgv.frame.origin.x+profile_imgv.frame.size.width+12, 16, [UIScreen mainScreen].bounds.size.width-(profile_imgv.frame.origin.x+profile_imgv.frame.size.width+10), 50)];
        notification_message.backgroundColor = [UIColor clearColor];
        notification_message.textAlignment = NSTextAlignmentCenter;
        [cell addSubview:notification_message];
        [notification_message setAttributedText:attrString2];
        
        notification_message.delegate = (id)self;
        notification_message.editable = NO;
        notification_message.selectable = NO;
        notification_message.tag = indexPath.row;
        
        
        UITapGestureRecognizer  *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedTextView:)];
        [notification_message addGestureRecognizer:tap];
        
    }
    else if ([[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue] == 2)
    {
        attrString1 = [[NSMutableAttributedString alloc] initWithString:@"is now a connection." attributes: font_regular];
        
        if ([[requestslist objectAtIndex:indexPath.row] objectForKey:@"params"]!=nil)
        {
            attrString2 = [[NSMutableAttributedString alloc] initWithString:[[NSString stringWithFormat:@"%@ ",[[[requestslist objectAtIndex:indexPath.row] objectForKey:@"params"] objectForKey:@"name"]] capitalizedString] attributes: font_medium];
            
        }
        
        [attrString2 appendAttributedString:attrString1];
        // [name setAttributedText:attrString2];
        
        [attrString2 appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",time] attributes: time_font_regular]];
        notification_message = [[UITextView alloc] initWithFrame:CGRectMake(profile_imgv.frame.origin.x+profile_imgv.frame.size.width+12, 16, [UIScreen mainScreen].bounds.size.width-(profile_imgv.frame.origin.x+profile_imgv.frame.size.width+10), 50)];
        notification_message.backgroundColor = [UIColor clearColor];
        notification_message.textAlignment = NSTextAlignmentCenter;
        [cell addSubview:notification_message];
        [notification_message setAttributedText:attrString2];
        
        notification_message.delegate = (id)self;
        notification_message.editable = NO;
        notification_message.selectable = NO;
        notification_message.tag = indexPath.row;
        
        UITapGestureRecognizer  *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedTextView:)];
        [notification_message addGestureRecognizer:tap];
        
    }
    else if ([[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue] == 3)
    {
        
        if ([[requestslist objectAtIndex:indexPath.row] objectForKey:@"params"]!=nil)
        {
            
            NSAttributedString *attrString3 = [[NSMutableAttributedString alloc] initWithString:[[NSString stringWithFormat:@"%@ ",[[[requestslist objectAtIndex:indexPath.row] objectForKey:@"params"] objectForKey:@"type"]] capitalizedString] attributes: font_regular];
            
            
            
            attrString2 = [[NSMutableAttributedString alloc] initWithString:[[NSString stringWithFormat:@"%@ ",[[[requestslist objectAtIndex:indexPath.row] objectForKey:@"params"] objectForKey:@"name"]] capitalizedString] attributes: font_medium];
            
            
            
            NSAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:@"accepted the change to a " attributes: font_regular];
            
            [attrString2 appendAttributedString:attrString];
            
            [attrString2 appendAttributedString:attrString3];
            
            attrString = [[NSMutableAttributedString alloc] initWithString:@"connection." attributes: font_regular];
            
            [attrString2 appendAttributedString:attrString];
            
        }
        
        
        //[attrString2 appendAttributedString:attrString1];
        
        // [name setAttributedText:attrString2];
        
        [attrString2 appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",time] attributes: time_font_regular]];
        
        
        
        notification_message = [[UITextView alloc] initWithFrame:CGRectMake(profile_imgv.frame.origin.x+profile_imgv.frame.size.width+12, 16, [UIScreen mainScreen].bounds.size.width-(profile_imgv.frame.origin.x+profile_imgv.frame.size.width+10), 50)];
        
        notification_message.backgroundColor = [UIColor clearColor];
        
        notification_message.textAlignment = NSTextAlignmentCenter;
        
        [cell addSubview:notification_message];
        
        [notification_message setAttributedText:attrString2];
        
        
        
        notification_message.delegate = (id)self;
        
        notification_message.editable = NO;
        
        notification_message.selectable = NO;
        
        notification_message.tag = indexPath.row;
        
        
        
        UITapGestureRecognizer  *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedTextView:)];
        
        [notification_message addGestureRecognizer:tap];
        
    }
    else if ([[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue] == 4)
    {
        // attrString1 = [[NSMutableAttributedString alloc] initWithString:@"is now a connection." attributes: font_regular];
        if ([[[requestslist objectAtIndex:indexPath.row] objectForKey:@"status"] isKindOfClass:[NSNumber class]] || ![[[requestslist objectAtIndex:indexPath.row] objectForKey:@"status"] isKindOfClass:[NSNull class]])
        {
            attrString1 = [[NSMutableAttributedString alloc] initWithString:@"You joined " attributes: font_regular];
            
            if ([[requestslist objectAtIndex:indexPath.row] objectForKey:@"params"]!=nil)
            {
                attrString2 = [[NSMutableAttributedString alloc] initWithString:[[NSString stringWithFormat:@"%@.",[[[requestslist objectAtIndex:indexPath.row] objectForKey:@"params"] objectForKey:@"groupname"]] capitalizedString] attributes: font_medium];
                
            }
            
            [attrString1 appendAttributedString:attrString2];
            // [name setAttributedText:attrString1];
            [attrString1 appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",time] attributes: time_font_regular]];
            notification_message = [[UITextView alloc] initWithFrame:CGRectMake(profile_imgv.frame.origin.x+profile_imgv.frame.size.width+12, 21, [UIScreen mainScreen].bounds.size.width-(profile_imgv.frame.origin.x+profile_imgv.frame.size.width+10), 44)];
            notification_message.backgroundColor = [UIColor clearColor];
            notification_message.textAlignment = NSTextAlignmentCenter;
            [cell addSubview:notification_message];
            [notification_message setAttributedText:attrString1];
            
            notification_message.delegate = (id)self;
            notification_message.editable = NO;
            notification_message.selectable = NO;
            notification_message.tag = indexPath.row;
            
            UITapGestureRecognizer  *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedTextView:)];
            [notification_message addGestureRecognizer:tap];
            
            DebugLog(@"Single Line:%@",notification_message.text );
            
        }
        else
        {
            attrString1 = [[NSMutableAttributedString alloc] initWithString:@"sends a group request." attributes: font_regular];
            
            if ([[requestslist objectAtIndex:indexPath.row] objectForKey:@"params"]!=nil)
            {
                attrString2 = [[NSMutableAttributedString alloc] initWithString:[[NSString stringWithFormat:@"%@ ",[[[requestslist objectAtIndex:indexPath.row] objectForKey:@"params"] objectForKey:@"groupname"]] capitalizedString] attributes: font_medium];
                
            }
            
            [attrString2 appendAttributedString:attrString1];
            // [name setAttributedText:attrString2];
            
            [attrString2 appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",time] attributes: time_font_regular]];
            notification_message = [[UITextView alloc] initWithFrame:CGRectMake(profile_imgv.frame.origin.x+profile_imgv.frame.size.width+12, 21, [UIScreen mainScreen].bounds.size.width-(profile_imgv.frame.origin.x+profile_imgv.frame.size.width+10), 44)];
            notification_message.backgroundColor = [UIColor clearColor];
            notification_message.textAlignment = NSTextAlignmentCenter;
            [cell addSubview:notification_message];
            [notification_message setAttributedText:attrString2];
            
            notification_message.delegate = (id)self;
            notification_message.editable = NO;
            notification_message.selectable = NO;
            notification_message.tag = indexPath.row;
            
            UITapGestureRecognizer  *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedTextView:)];
            [notification_message addGestureRecognizer:tap];
            
        }
        
        
    }
    else if ([[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue] == 5)
    {
        // attrString1 = [[NSMutableAttributedString alloc] initWithString:@"is now a connection." attributes: font_regular];
        NSMutableAttributedString *attrString;
        
        if ([[requestslist objectAtIndex:indexPath.row] objectForKey:@"params"]!=nil)
        {
            attrString = [[NSMutableAttributedString alloc] initWithString:@"Your friend " attributes: font_regular];
            
            attrString2 = [[NSMutableAttributedString alloc] initWithString:[[NSString stringWithFormat:@"%@ ",[[[requestslist objectAtIndex:indexPath.row] objectForKey:@"params"] objectForKey:@"name"]] capitalizedString] attributes: font_medium];
            
            [attrString appendAttributedString:attrString2];
            
            attrString2 = [[NSMutableAttributedString alloc] initWithString:@"is now on Budnav. " attributes: font_regular];
            [attrString appendAttributedString:attrString2];
        }
        
        //[attrString2 appendAttributedString:attrString1];
        //  [name setAttributedText:attrString2];
        
        [attrString appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",time] attributes: time_font_regular]];
        notification_message = [[UITextView alloc] initWithFrame:CGRectMake(profile_imgv.frame.origin.x+profile_imgv.frame.size.width+12, 13, [UIScreen mainScreen].bounds.size.width-(profile_imgv.frame.origin.x+profile_imgv.frame.size.width+10), 50)];
        notification_message.backgroundColor = [UIColor clearColor];
        notification_message.textAlignment = NSTextAlignmentCenter;
        [cell addSubview:notification_message];
        [notification_message setAttributedText:attrString];
        
        notification_message.delegate = (id)self;
        notification_message.editable = NO;
        notification_message.selectable = NO;
        notification_message.tag = indexPath.row;
        
        UITapGestureRecognizer  *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedTextView:)];
        [notification_message addGestureRecognizer:tap];
        
    }
    else if ([[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue] == 6)
    {
        // attrString1 = [[NSMutableAttributedString alloc] initWithString:@"is now a connection." attributes: font_regular];
        
        //        if ([[requestslist objectAtIndex:indexPath.row] objectForKey:@"params"]!=nil)
        //        {
        //            NSAttributedString *attrString3 = [[NSMutableAttributedString alloc] initWithString:[[NSString stringWithFormat:@"%@ ",[[[requestslist objectAtIndex:indexPath.row] objectForKey:@"params"] objectForKey:@"groupname"]] capitalizedString] attributes: font_medium];
        //
        //            attrString2 = [[NSMutableAttributedString alloc] initWithString:[[NSString stringWithFormat:@"%@ ",[[[requestslist objectAtIndex:indexPath.row] objectForKey:@"params"] objectForKey:@"name"]] capitalizedString] attributes: font_medium];
        
        profile_imgv.image = [UIImage imageNamed:@"Notification"];
        
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:@"Multiple friends " attributes: font_medium];
        
        NSMutableAttributedString *attrString1 = [[NSMutableAttributedString alloc] initWithString:@"are now on Budnav." attributes: font_regular];
        
        [attrString appendAttributedString:attrString1];
        //            [attrString2 appendAttributedString:attrString];
        //            [attrString2 appendAttributedString:attrString3];
        //        }
        
        //[attrString2 appendAttributedString:attrString1];
        // [name setAttributedText:attrString2];
        
        [attrString appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",time] attributes: time_font_regular]];
        notification_message = [[UITextView alloc] initWithFrame:CGRectMake(profile_imgv.frame.origin.x+profile_imgv.frame.size.width+12, 13, [UIScreen mainScreen].bounds.size.width-(profile_imgv.frame.origin.x+profile_imgv.frame.size.width+10), 50)];
        notification_message.backgroundColor = [UIColor clearColor];
        notification_message.textAlignment = NSTextAlignmentCenter;
        [cell addSubview:notification_message];
        [notification_message setAttributedText:attrString];
        
        notification_message.delegate = (id)self;
        notification_message.editable = NO;
        notification_message.selectable = NO;
        notification_message.tag = indexPath.row;
        
        UITapGestureRecognizer  *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedTextView:)];
        [notification_message addGestureRecognizer:tap];
        //
        //        [accept setTitle:@"" forState:UIControlStateNormal];
        //        [accept setTitle:@"Share" forState:UIControlStateNormal];
        
    }
    else if ([[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue] == 7)
    {
        NSMutableAttributedString *attrString3 = [[NSMutableAttributedString alloc] initWithString:[[NSString stringWithFormat:@"%@ ",[[[requestslist objectAtIndex:indexPath.row] objectForKey:@"params"] objectForKey:@"name"]] capitalizedString] attributes: font_medium];
        
        attrString1 = [[NSMutableAttributedString alloc] initWithString:@"joined " attributes: font_regular];
        
        [attrString3 appendAttributedString:attrString1];
        
        if ([[requestslist objectAtIndex:indexPath.row] objectForKey:@"params"]!=nil)
        {
            attrString2 = [[NSMutableAttributedString alloc] initWithString:[[NSString stringWithFormat:@"%@.",[[[requestslist objectAtIndex:indexPath.row] objectForKey:@"params"] objectForKey:@"groupname"]] capitalizedString] attributes: font_medium];
            
        }
        
        [attrString3 appendAttributedString:attrString2];
        // [name setAttributedText:attrString1];
        
        [attrString3 appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",time] attributes: time_font_regular]];
        notification_message = [[UITextView alloc] initWithFrame:CGRectMake(profile_imgv.frame.origin.x+profile_imgv.frame.size.width+12, 13, [UIScreen mainScreen].bounds.size.width-(profile_imgv.frame.origin.x+profile_imgv.frame.size.width+10), 50)];
        notification_message.backgroundColor = [UIColor clearColor];
        notification_message.textAlignment = NSTextAlignmentCenter;
        [cell addSubview:notification_message];
        [notification_message setAttributedText:attrString3];
        
        notification_message.delegate = (id)self;
        notification_message.editable = NO;
        notification_message.selectable = NO;
        notification_message.tag = indexPath.row;
        
        UITapGestureRecognizer  *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedTextView:)];
        [notification_message addGestureRecognizer:tap];
        
    }
    else if ([[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue] == 8)
    {
        if ([[[requestslist objectAtIndex:indexPath.row] objectForKey:@"status"] isKindOfClass:[NSNumber class]] || ![[[requestslist objectAtIndex:indexPath.row] objectForKey:@"status"] isKindOfClass:[NSNull class]])
        {
            attrString1 = [[NSMutableAttributedString alloc] initWithString:@"You shared your location with " attributes: font_regular];
            
            if ([[requestslist objectAtIndex:indexPath.row] objectForKey:@"params"]!=nil)
            {
                attrString2 = [[NSMutableAttributedString alloc] initWithString:[[NSString stringWithFormat:@"%@.",[[[requestslist objectAtIndex:indexPath.row] objectForKey:@"params"] objectForKey:@"groupname"]] capitalizedString] attributes: font_medium];
                
            }
            
            [attrString1 appendAttributedString:attrString2];
            // [name setAttributedText:attrString1];
            
            [attrString1 appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",time] attributes: time_font_regular]];
            notification_message = [[UITextView alloc] initWithFrame:CGRectMake(profile_imgv.frame.origin.x+profile_imgv.frame.size.width+12, 13, [UIScreen mainScreen].bounds.size.width-(profile_imgv.frame.origin.x+profile_imgv.frame.size.width+10), 50)];
            notification_message.backgroundColor = [UIColor clearColor];
            notification_message.textAlignment = NSTextAlignmentCenter;
            [cell addSubview:notification_message];
            [notification_message setAttributedText:attrString1];
            
            notification_message.delegate = (id)self;
            notification_message.editable = NO;
            notification_message.selectable = NO;
            notification_message.tag = indexPath.row;
            
            UITapGestureRecognizer  *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedTextView:)];
            [notification_message addGestureRecognizer:tap];
            
        }
        else
        {
            NSMutableAttributedString *attrString3 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ",[[[requestslist objectAtIndex:indexPath.row] objectForKey:@"params"] objectForKey:@"name"]] attributes: font_medium];
            
            attrString1 = [[NSMutableAttributedString alloc] initWithString:@"from " attributes: font_regular];
            
            [attrString3 appendAttributedString:attrString1];
            
            if ([[requestslist objectAtIndex:indexPath.row] objectForKey:@"params"]!=nil)
            {
                attrString2 = [[NSMutableAttributedString alloc] initWithString:[[NSString stringWithFormat:@"%@ ",[[[requestslist objectAtIndex:indexPath.row] objectForKey:@"params"] objectForKey:@"groupname"]] capitalizedString] attributes: font_medium];
                
            }
            
            [attrString3 appendAttributedString:attrString2];
            
            NSMutableAttributedString *attrString4 = [[NSMutableAttributedString alloc] initWithString:@"requests your location. " attributes: font_regular];
            
            [attrString3 appendAttributedString:attrString4];
            
            // [name setAttributedText:attrString3];
            [attrString3 appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",time] attributes: time_font_regular]];
            
            notification_message = [[UITextView alloc] initWithFrame:CGRectMake(profile_imgv.frame.origin.x+profile_imgv.frame.size.width+12, 13, [UIScreen mainScreen].bounds.size.width-(profile_imgv.frame.origin.x+profile_imgv.frame.size.width+10), 50)];
            notification_message.backgroundColor = [UIColor clearColor];
            notification_message.textAlignment = NSTextAlignmentCenter;
            [cell addSubview:notification_message];
            [notification_message setAttributedText:attrString3];
            
            notification_message.delegate = (id)self;
            notification_message.editable = NO;
            notification_message.selectable = NO;
            notification_message.tag = indexPath.row;
            
            UITapGestureRecognizer  *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedTextView:)];
            [notification_message addGestureRecognizer:tap];
            
            [accept setTitle:@"" forState:UIControlStateNormal];
            [accept setTitle:@"Share" forState:UIControlStateNormal];
            
        }
        
    }
    else
    {
        NSMutableAttributedString *attrString3;
        
        if ([[requestslist objectAtIndex:indexPath.row] objectForKey:@"params"]!=nil)
        {
            attrString3 = [[NSMutableAttributedString alloc] initWithString:[[NSString stringWithFormat:@"%@ ",[[[requestslist objectAtIndex:indexPath.row] objectForKey:@"params"] objectForKey:@"name"]] capitalizedString] attributes: font_medium];
            
          //  attrString1 = [[NSMutableAttributedString alloc] initWithString:@"from " attributes: font_regular];
            
        //    [attrString3 appendAttributedString:attrString1];
            
//            attrString2 = [[NSMutableAttributedString alloc] initWithString:[[NSString stringWithFormat:@"%@ ",[[[requestslist objectAtIndex:indexPath.row] objectForKey:@"params"] objectForKey:@"groupname"]] capitalizedString] attributes: font_medium];
//            [attrString3 appendAttributedString:attrString2];
            
            attrString1 = [[NSMutableAttributedString alloc] initWithString:@"shared his/her " attributes: font_regular];
            [attrString3 appendAttributedString:attrString1];
            
            attrString2 = [[NSMutableAttributedString alloc] initWithString:@"location." attributes: font_medium];
            
            [attrString3 appendAttributedString:attrString2];
            
        }
        
        // [name setAttributedText:attrString1];
        
        [attrString3 appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",time] attributes: time_font_regular]];
        notification_message = [[UITextView alloc] initWithFrame:CGRectMake(profile_imgv.frame.origin.x+profile_imgv.frame.size.width+12, 7, [UIScreen mainScreen].bounds.size.width-(profile_imgv.frame.origin.x+profile_imgv.frame.size.width+10), 50)];
        notification_message.backgroundColor = [UIColor clearColor];
        notification_message.textAlignment = NSTextAlignmentCenter;
        [cell addSubview:notification_message];
        [notification_message setAttributedText:attrString3];
        
        notification_message.delegate = (id)self;
        notification_message.editable = NO;
        notification_message.selectable = NO;
        notification_message.tag = indexPath.row;
        
        UITapGestureRecognizer  *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedTextView:)];
        [notification_message addGestureRecognizer:tap];
        
    }
    // [cell addSubview:typeimg];
    //    if ([[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue] == 3)
    //    {
    //        if ([[[requestslist objectAtIndex:indexPath.row] objectForKey:@"status"] isKindOfClass:[NSNumber class]] || ![[[requestslist objectAtIndex:indexPath.row] objectForKey:@"status"] isKindOfClass:[NSNull class]])
    //        {
    //            DebugLog(@"TYPE TYPE:%d",[[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue]);
    //            UILabel *separatorlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 74, [UIScreen mainScreen].bounds.size.width, 0.6f)];
    //            separatorlabel.backgroundColor=[UIColor colorWithRed:(204.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f];
    //            [cell addSubview:separatorlabel];
    //        }
    //        else
    //        {
    //            DebugLog(@"TYPE TYPE:%d",[[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue]);
    //            UILabel *separatorlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 119, [UIScreen mainScreen].bounds.size.width, 0.6)];
    //            separatorlabel.backgroundColor=[UIColor colorWithRed:(204.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f];
    //            [cell addSubview:separatorlabel];;
    //        }
    //
    //    }
    if([[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue] == 2 || [[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue] == 9  || [[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue] == 6 || [[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue] == 5 || [[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue] == 3)
    {
        DebugLog(@"TYPE TYPE:%d",[[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue]);
        
        UILabel *separatorlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 74,[UIScreen mainScreen].bounds.size.width, 0.6f)];
        separatorlabel.backgroundColor=[UIColor colorWithRed:(204.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f];
        [cell addSubview:separatorlabel];
    }
    else
    {
        if ([[[requestslist objectAtIndex:indexPath.row] objectForKey:@"status"] isKindOfClass:[NSNumber class]] || ![[[requestslist objectAtIndex:indexPath.row] objectForKey:@"status"] isKindOfClass:[NSNull class]])
        {
            DebugLog(@"TYPE TYPE:%d",[[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue]);
            UILabel *separatorlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 74, [UIScreen mainScreen].bounds.size.width, 0.6f)];
            separatorlabel.backgroundColor=[UIColor colorWithRed:(204.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f];
            [cell addSubview:separatorlabel];
        }
        else
        {
            if ([[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue]==7)
            {
                DebugLog(@"TYPE TYPE:%d",[[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue]);
                UILabel *separatorlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 74, [UIScreen mainScreen].bounds.size.width, 0.6f)];
                separatorlabel.backgroundColor=[UIColor colorWithRed:(204.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f];
                [cell addSubview:separatorlabel];
            }
            else
            {
                DebugLog(@"TYPE TYPE:%d",[[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue]);
                UILabel *separatorlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 119, [UIScreen mainScreen].bounds.size.width, 0.6f)];
                separatorlabel.backgroundColor=[UIColor colorWithRed:(204.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f];
                [cell addSubview:separatorlabel];
            }
        }
    }
    
    
    notification_message.frame =  CGRectMake(profile_imgv.frame.origin.x+profile_imgv.frame.size.width+12, 12, [UIScreen mainScreen].bounds.size.width-(profile_imgv.frame.origin.x+profile_imgv.frame.size.width+10), 50);
    
    notification_message.scrollsToTop = false;
    //    notification_message.scrollEnabled = NO;
    notification_message.editable = NO;
    notification_message.scrollEnabled = NO;          ////////////////// Added on 11.05.16
    // }
    notification_message.backgroundColor = [UIColor clearColor];
    
    CGSize contentSize = [notification_message sizeThatFits:CGSizeMake(notification_message.bounds.size.width, CGFLOAT_MAX)];
    CGFloat topCorrection = (notification_message.bounds.size.height - contentSize.height * notification_message.zoomScale) / 2.0;
    notification_message.contentOffset = CGPointMake(0, -topCorrection);
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DebugLog(@"hiii");
    
    //    if ([[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue]==1 || [[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue]==2)
    //    {
    //        ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
    //        con.request = TRUE;
    //        con.other=@"yes";
    //        con.uid=[NSString stringWithFormat:@"%@",[[[requestslist objectAtIndex:indexPath.row] objectForKey:@"params"] objectForKey:@"user"]];//[NSString stringWithFormat:@"%ld",(long)[sender tag]];
    //
    //        DebugLog(@"profile page uid fetched: %@",con.uid);
    //
    //        [self.navigationController pushViewController:con animated:YES];
    //
    //    }
    //    else if ([[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue]==3 || [[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue]==4)
    //    {
    //        NSString *grp_id = [NSString stringWithFormat:@"%@",[[[requestslist objectAtIndex:indexPath.row] objectForKey:@"params"] objectForKey:@"group"]];
    //        NSString *group_name = [NSString stringWithFormat:@"%@",[[[requestslist objectAtIndex:indexPath.row] objectForKey:@"params"] objectForKey:@"groupname"]];
    //
    //        GroupDetailsViewController *group_page = [[GroupDetailsViewController alloc] init];
    //
    //
    ////        if ([base64String length] >6)
    ////        {
    ////            decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
    ////
    ////        }
    //
    //        group_page.groupname = group_name;
    //        group_page.isadmin = @"0";
    //        group_page.groupid=grp_id;
    //        DebugLog(@"group id: %@",group_page.groupid);
    //
    //        //group_page.grouppic=[UIImage imageWithData:decodedData];
    //        group_page.accept_ckeck = TRUE;
    //
    //        [self.navigationController pushViewController:group_page animated:NO];
    //    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //    if ([[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue] == 3)
    //    {
    //        if ([[[requestslist objectAtIndex:indexPath.row] objectForKey:@"status"] isKindOfClass:[NSNumber class]] || ![[[requestslist objectAtIndex:indexPath.row] objectForKey:@"status"] isKindOfClass:[NSNull class]])
    //        {
    //
    //            DebugLog(@"Tag:%ld",(long)indexPath.row);
    //            return 75.0f;
    //        }
    //        else
    //        {
    //            return 120;
    //        }
    //    }
    if ([[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue] == 2 || [[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue] == 9 || [[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue] == 7 || [[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue] == 6 || [[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue] == 5 || [[[requestslist objectAtIndex:indexPath.row] objectForKey:@"type"] intValue] == 3)
    {
        return 75.0f;
    }
    else
    {
        if ([[[requestslist objectAtIndex:indexPath.row] objectForKey:@"status"] isKindOfClass:[NSNumber class]] || ![[[requestslist objectAtIndex:indexPath.row] objectForKey:@"status"] isKindOfClass:[NSNull class]])
        {
            return 75.0f;
        }
        else
        {
            return 120;
        }
    }
}
-(void)acceptgroup: (id)sender
{
    
    [UIView animateWithDuration:0.0
                     animations:^{
                         
                         [SVProgressHUD showWithStatus:@"Accepting Request"];
                         
                         
                     }
                     completion:^(BOOL finished){
                         
                         
                         DebugLog(@"%ld",(long)[sender tag]);
                         DebugLog(@"accept sender: %@",[NSString stringWithFormat:@"%@",[[groupadminlist objectAtIndex:[sender tag]] objectForKey:@"id"]]);
                         DebugLog(@"group id: %@",[NSString stringWithFormat:@"%@",[[groupadminlist objectAtIndex:[sender tag]] objectForKey:@"group_id"]]);
                         NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                         NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=group-accept&group_id=%@&user_id=%@&access_token=%@&device_id=%@",[NSString stringWithFormat:@"%@",[[groupadminlist objectAtIndex:[sender tag]] objectForKey:@"group_id"]],[NSString stringWithFormat:@"%@",[[groupadminlist objectAtIndex:[sender tag]] objectForKey:@"id"]],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
                         
                         DebugLog(@"accept url: %@",urlString1);
                         NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
                         
                         NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
                         
                         NSError *error=nil;
                         NSDictionary *json_accept = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                                     options:kNilOptions
                                                                                       error:&error];
                         DebugLog(@"accept json returns: %@",json_accept);
                         NSString *errornumber= [NSString stringWithFormat:@"%@",[json_accept objectForKey:@"errorno"]];
                         DebugLog(@"err  %@",errornumber);
                         
                         if ([[json_accept objectForKey:@"success"]intValue] == 1)
                         {
                             [self loadrequestsdataurl];
                             [tab_request reloadData];
                             
                             //[act1 removeFromSuperview];
                             alert = [[UIAlertView alloc] initWithTitle:@"Request Successfully Accepted!"
                                                                message:nil
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                             [alert show];
                             //        ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
                             //        con.other=@"yes";
                             //        con.uid=[NSString stringWithFormat:@"%ld",(long)[sender tag]];
                             //        [self.navigationController pushViewController:con animated:NO];
                             [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"request_accept"];
                             [SVProgressHUD dismiss];
                         }
                         else if (![errornumber isEqualToString:@"0"])
                         {
                             NSString *err_str = [json_accept objectForKey:@"error"];
                             [SVProgressHUD showErrorWithStatus:err_str];
                         }
                         
                     }];
    
}

-(void)denygroup: (id)sender
{
    [self loaderShowing];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=group-deny&group_id=%@&user_id=%@&access_token=%@&device_id=%@",[NSString stringWithFormat:@"%@",[[groupadminlist objectAtIndex:[sender tag]] objectForKey:@"group_id"]],[NSString stringWithFormat:@"%@",[[groupadminlist objectAtIndex:[sender tag]] objectForKey:@"id"]],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
    
    DebugLog(@"deny url: %@",urlString1);
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    
    NSError *error=nil;
    NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                              options:kNilOptions
                                                                error:&error];
    DebugLog(@"deny json returns: %@",json_deny);
    
    NSString *errornumber= [NSString stringWithFormat:@"%@",[json_deny objectForKey:@"errorno"]];
    DebugLog(@"err  %@",errornumber);
    if ([[json_deny objectForKey:@"success"]intValue] == 1)
    {
        //[act1 removeFromSuperview];
        alert = [[UIAlertView alloc] initWithTitle:@"Request Successfully Denied!"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        
        [self loadrequestsdataurl];
        [tab_request reloadData];
        //        [self loadrequestsdata];
        //        //        tab_request =Nil;
        //        [tab_request reloadData];
        [SVProgressHUD dismiss];
        
    }
    else if (![errornumber isEqualToString:@"0"])
    {
        NSString *err_str = [json_deny objectForKey:@"error"];
        [SVProgressHUD showErrorWithStatus:err_str];
    }
    
    
}

-(void)accept: (id)sender
{
    //[self loaderShowing];
    
    DebugLog(@"accept sender: %ld",(long)[sender tag]);
    DebugLog(@"Request List:%@",requestslist);
    
    [UIView animateWithDuration:0.0
                     animations:^{
                         
                         if ([[[requestslist objectAtIndex:[sender tag]] objectForKey:@"type"] intValue]==8)
                         {
                              [SVProgressHUD showWithStatus:@"Sharing Location"];
                         }
                         else
                         {
                         
                             [SVProgressHUD showWithStatus:@"Accepting Request"];
                         }
//                         tab_request.userInteractionEnabled = NO;
                         
                     }
                     completion:^(BOOL finished){
                         
                         NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                         NSString *readUrl =[NSString stringWithFormat:@"https://budnav.com/ext/?action=notification_read&id=%@&access_token=%@&device_id=%@",[[requestslist objectAtIndex:[sender tag]] objectForKey:@"id"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
                         DebugLog(@"read url: %@",readUrl);
                         NSString *newString1 = [readUrl stringByReplacingOccurrencesOfString:@" " withString:@""];
                         
                         NSData *readdataUrl =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
                         
                         if (readdataUrl!=nil)
                         {
                             NSError *error=nil;
                             NSDictionary *json_accept = [NSJSONSerialization JSONObjectWithData:readdataUrl //1
                                                                                         options:kNilOptions
                                                                                           error:&error];
                             DebugLog(@"accept json returns: %@",json_accept);
                             NSString *errornumber= [NSString stringWithFormat:@"%@",[json_accept objectForKey:@"errorno"]];
                             DebugLog(@"err  %@",errornumber);
                             
                             if ([[json_accept objectForKey:@"success"]intValue] == 1)
                             {
                                 
                                 if ([[NSString stringWithFormat:@"%@",[[[requestslist objectAtIndex:[sender tag]] objectForKey:@"action"] objectForKey:@"p"]] isEqualToString:@"profile"])
                                 {
                                     if ([[[[requestslist objectAtIndex:[sender tag]] objectForKey:@"params"] objectForKey:@"type"] isEqualToString:@"business"]) {
                                         
                                         [[GAI sharedInstance].defaultTracker send:
                                          [[GAIDictionaryBuilder createEventWithCategory:@"Notification Accept"
                                                                                  action:@"Accept business contact request"
                                                                                   label:nil
                                                                                   value:nil] build]];
                                     }
                                     else
                                     {
                                         [[GAI sharedInstance].defaultTracker send:
                                          [[GAIDictionaryBuilder createEventWithCategory:@"Notification Accept"
                                                                                  action:@"Accept personal contact request"
                                                                                   label:nil
                                                                                   value:nil] build]];
                                     }
                                     
                                     NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                                     NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=acceptrequest&id=%@&access_token=%@&device_id=%@&thumb=true",[NSString stringWithFormat:@"%@",[[[requestslist objectAtIndex:[sender tag]] objectForKey:@"params"] objectForKey:@"user"]],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
                                     
                                     DebugLog(@"accept url: %@",urlString1);
                                     NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
                                     
                                     NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
                                     tab_request.userInteractionEnabled = YES;
                                     if (signeddataURL1!=nil)
                                     {
                                         
                                         
                                         NSError *error=nil;
                                         NSDictionary *json_accept = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                                                     options:kNilOptions
                                                                                                       error:&error];
                                         DebugLog(@"accept json returns: %@",json_accept);
                                         NSString *errornumber= [NSString stringWithFormat:@"%@",[json_accept objectForKey:@"errorno"]];
                                         DebugLog(@"err  %@",errornumber);
                                         
                                         if ([[json_accept objectForKey:@"success"]intValue] == 1)
                                         {
                                             //[act1 removeFromSuperview];
                                             alert = [[UIAlertView alloc] initWithTitle:@"Request Successfully Accepted!"
                                                                                message:nil
                                                                               delegate:self
                                                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                                             [alert show];
                                             
                                             ConNewContactsViewController *contact = [[ConNewContactsViewController alloc]init];
                                             
                                             NSOperationQueue *pull = [NSOperationQueue new];
                                             [pull addOperationWithBlock:^{
                                                 
                                                 [contact reloadAgain];
                                                 
                                             }];
                                             ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
                                             con.request = TRUE;
                                             con.other=@"yes";
                                             con.uid=[NSString stringWithFormat:@"%@",[[[requestslist objectAtIndex:[sender tag]] objectForKey:@"params"] objectForKey:@"user"]];//[NSString stringWithFormat:@"%ld",(long)[sender tag]];
                                             
                                             DebugLog(@"profile page uid fetched: %@",con.uid);
                                             
                                             //                                 [self loadrequestsdataurl];
                                             //                                 [tab_request reloadData];
                                             
                                             [requestslist removeObjectAtIndex:[sender tag]];
                                             [tab_request reloadData];
                                             
                                             //                                 CATransition* transition = [CATransition animation];
                                             //
                                             //                                 transition.duration = 0.4;
                                             //                                 transition.type = kCATransitionPush;
                                             //                                 transition.subtype = kCATransitionFade;
                                             //
                                             //                                 [[self navigationController].view.layer addAnimation:transition forKey:nil];
                                             
                                             [self.navigationController pushViewController:con animated:YES];
                                             [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"request_accept"];
                                             [SVProgressHUD dismiss];
                                         }
                                         else if (![errornumber isEqualToString:@"0"])
                                         {
                                             NSString *err_str = [json_accept objectForKey:@"error"];
                                             [SVProgressHUD showErrorWithStatus:err_str];
                                         }
                                     }
                                     else
                                     {
                                         alert = [[UIAlertView alloc] initWithTitle:@"Check Your Internet Connection!"
                                                                            message:nil
                                                                           delegate:self
                                                                  cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                                         [alert show];
                                         [SVProgressHUD dismiss];
                                     }
                                     
                                 }
                                 else if([[[requestslist objectAtIndex:[sender tag]] objectForKey:@"type"] intValue]==8)
                                 {
                                     
//                                     [SVProgressHUD dismiss];
                                     shareTag = (int)[sender tag];
                                     [self CurrentLocationIdentifier];
                                     
                                     
//                                     if (get_location)
//                                     {
//                                         
//                                         [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//                                             
//                                             [act startAnimating];
//                                             
//                                         }];
//                                         
//                                         [[NSOperationQueue new] addOperationWithBlock:^{
//                                             
//                                             [[GAI sharedInstance].defaultTracker send:
//                                              
//                                              [[GAIDictionaryBuilder createEventWithCategory:@"Notifications"
//                                                
//                                                                                      action:@"Accept location request"
//                                                
//                                                                                       label:nil
//                                                
//                                                                                       value:nil] build]];
//                                             
//                                             
//                                             
//                                             NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
//                                             
//                                             NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=group-location_update&group_id=%@&latitude=%@&longitude=%@&access_token=%@&device_id=%@",[NSString stringWithFormat:@"%@",[[[requestslist objectAtIndex:[sender tag]] objectForKey:@"params"] objectForKey:@"group"]],latitude,longitude,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
//                                             
//                                             
//                                             
//                                             DebugLog(@"accept url: %@",urlString1);
//                                             
//                                             NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
//                                             
//                                             NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
//                                             
//                                             [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//                                                 
//                                                 [act stopAnimating];
//                                                 
//                                                 NSError *error=nil;
//                                                 tab_request.userInteractionEnabled = YES;
//                                                 if (signeddataURL1!=nil)
//                                                 {
//                                                     
//                                                     NSDictionary *json_accept = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
//                                                                                  
//                                                                                                                 options:kNilOptions
//                                                                                  
//                                                                                                                   error:&error];
//                                                     
//                                                     DebugLog(@"Share json returns: %@",json_accept);
//                                                     
//                                                     NSString *errornumber= [NSString stringWithFormat:@"%@",[json_accept objectForKey:@"errorno"]];
//                                                     
//                                                     DebugLog(@"err  %@",errornumber);
//                                                     
//                                                     
//                                                     
//                                                     if ([[json_accept objectForKey:@"success"]intValue] == 1)
//                                                         
//                                                     {
//                                                         
//                                                         [self loadrequestsdataurl];
//                                                         
//                                                         
//                                                         
//                                                         alert = [[UIAlertView alloc] initWithTitle:@"Location Successfully Shared!"
//                                                                  
//                                                                                            message:nil
//                                                                  
//                                                                                           delegate:self
//                                                                  
//                                                                                  cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
//                                                         
//                                                         [alert show];
//                                                         
//                                                         
//                                                         
//                                                         [SVProgressHUD dismiss];
//                                                         
//                                                         
//                                                         
//                                                         ConLocateGroupViewController *con = [[ConLocateGroupViewController alloc]init];
//                                                         
//                                                         con.group_id = [NSString stringWithFormat:@"%@",[[[requestslist objectAtIndex:[sender tag]] objectForKey:@"params"] objectForKey:@"group"]];
//                                                         [tab_request removeFromSuperview];
//                                                         
//                                                         [self.navigationController pushViewController:con animated:YES];
//                                                         
//                                                         [self.tabBarController.tabBar setHidden:YES];
//                                                         
//                                                         
//                                                     }
//                                                     
//                                                     else
//                                                         
//                                                     {
//                                                         
//                                                         NSString *err_str = [json_accept objectForKey:@"error"];
//                                                         
//                                                         [SVProgressHUD showErrorWithStatus:err_str];
//                                                         
//                                                     }
//                                                     
//                                                     
//                                                     
//                                                 }
//                                                 
//                                                 else
//                                                     
//                                                 {
//                                                     
//                                                     alert = [[UIAlertView alloc] initWithTitle:@"Check Your Internet Connection!"
//                                                              
//                                                                                        message:nil
//                                                              
//                                                                                       delegate:self
//                                                              
//                                                                              cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
//                                                     
//                                                     [alert show];
//                                                     
//                                                     [SVProgressHUD dismiss];
//                                                     
//                                                 }
//                                                 
//                                                 
//                                             }];
//                                             
//                                         }];
//                                         
//                                     }
//                                     else
//                                     {
//                                         
//                                         [SVProgressHUD dismiss];
//                                         
//                                     }
                                     
                                 }
                                 else
                                 {
                                     NSString *grp_id = [NSString stringWithFormat:@"%@",[[[requestslist objectAtIndex:[sender tag]] objectForKey:@"params"] objectForKey:@"group"]];
                                     NSString *group_name = [NSString stringWithFormat:@"%@",[[[requestslist objectAtIndex:[sender tag]] objectForKey:@"params"] objectForKey:@"groupname"]];
                                     
                                     if ([[[[requestslist objectAtIndex:[sender tag]] objectForKey:@"params"] objectForKey:@"type"] isEqualToString:@"business"])
                                     {
                                         [[GAI sharedInstance].defaultTracker send:
                                          [[GAIDictionaryBuilder createEventWithCategory:@"Notification Accept"
                                                                                  action:@"Accept business group request"
                                                                                   label:nil
                                                                                   value:nil] build]];
                                     }
                                     else
                                     {
                                         [[GAI sharedInstance].defaultTracker send:
                                          [[GAIDictionaryBuilder createEventWithCategory:@"Notification Accept"
                                                                                  action:@"Accept personal group request"
                                                                                   label:nil
                                                                                   value:nil] build]];
                                     }
                                     
                                     
                                     
                                     DebugLog(@"Group Id: %@  %@",grp_id,group_name);
                                     
                                     NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                                     NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=group-accept&group_id=%@&access_token=%@&device_id=%@&thumb=true",[NSString stringWithFormat:@"%@",[[[requestslist objectAtIndex:[sender tag]] objectForKey:@"params"] objectForKey:@"group"]],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
                                     
                                     DebugLog(@"accept url: %@",urlString1);
                                     NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
                                     
                                     NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
                                     
                                     NSError *error=nil;
                                     tab_request.userInteractionEnabled = YES;
                                     if (signeddataURL1!=nil)
                                     {
                                         
                                         
                                         NSDictionary *json_accept = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                                                     options:kNilOptions
                                                                                                       error:&error];
                                         DebugLog(@"accept json returns: %@",json_accept);
                                         NSString *errornumber= [NSString stringWithFormat:@"%@",[json_accept objectForKey:@"errorno"]];
                                         DebugLog(@"err  %@",errornumber);
                                         
                                         if ([[json_accept objectForKey:@"success"]intValue] == 1)
                                         {
                                             [self loadrequestsdataurl];
                                             // [requestslist removeObjectAtIndex:[sender tag]];
                                             // [tab_request reloadData];
                                             
                                             //[act1 removeFromSuperview];
                                             alert = [[UIAlertView alloc] initWithTitle:@"Request Successfully Accepted!"
                                                                                message:nil
                                                                               delegate:self
                                                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                                             [alert show];
                                             //            ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
                                             //            con.other=@"yes";
                                             //            con.uid=[NSString stringWithFormat:@"%ld",(long)[sender tag]];
                                             //            [self.navigationController pushViewController:con animated:NO];
                                             [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"request_accept"];
                                             [SVProgressHUD dismiss];
                                             
                                             // ConGroupsViewController *group = [[ConGroupsViewController alloc] init];
                                             //                                 group.accept_check = true;
                                             //                                 [group loaddata];
                                             
                                             GroupDetailsViewController *group_page = [[GroupDetailsViewController alloc] init];
                                             
                                             
                                             if ([base64String length] >6)
                                             {
                                                 decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
                                                 
                                             }
                                             
                                             group_page.groupname = group_name;
                                             group_page.isadmin = @"0";
                                             group_page.groupid=grp_id;
                                             //DebugLog(@"group id: %@",group.groupid);
                                             
                                             // group_page.grouppic=[UIImage imageWithData:decodedData];
                                             
                                             
                                             
                                             //                                 group_page.groupid=[[requestslist objectAtIndex:[sender tag]] objectForKey:@"id"];
                                             DebugLog(@"group id: %@",group_page.groupid);
                                             
                                             //group_page.grouppic=[UIImage imageWithData:decodedData];
                                             group_page.accept_ckeck = TRUE;
                                             del.tabBarController.tabBar.frame = CGRectMake(0, 0, 0, 0);
                                             [tab_request removeFromSuperview];
                                             //[del selectedIcon];
                                             
                                             //                                 CATransition *transition = [CATransition animation];
                                             //
                                             //                                 transition.duration = 0.4f;
                                             //
                                             //                                 transition.type = kCATransitionFade;
                                             //                                 transition.subtype = kCATransitionFromRight;
                                             //                                 [[self navigationController].view.layer addAnimation:transition forKey:nil];
                                             //                                 [self dismissViewControllerAnimated:NO completion:nil];
                                             //                                 [self presentViewController:group_page animated:NO completion:nil];
                                             [self.navigationController pushViewController:group_page animated:YES];
                                             
                                         }
                                         
                                         else if (![errornumber isEqualToString:@"0"])
                                         {
                                             NSString *err_str = [json_accept objectForKey:@"error"];
                                             [SVProgressHUD showErrorWithStatus:err_str];
                                         }
                                         
                                     }
                                     else
                                     {
                                         alert = [[UIAlertView alloc] initWithTitle:@"Check Your Internet Connection!"
                                                                            message:nil
                                                                           delegate:self
                                                                  cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                                         [alert show];
                                         [SVProgressHUD dismiss];
                                     }
                                 }
                                 
                             }
                             else
                             {
                                 tab_request.userInteractionEnabled = YES;
                                 NSString *err_str = [json_accept objectForKey:@"error"];
                                 [SVProgressHUD showErrorWithStatus:err_str];
                             }
                         }
                         else
                         {
                             tab_request.userInteractionEnabled = YES;
                             alert = [[UIAlertView alloc] initWithTitle:@"Check Your Internet Connection!"
                                                                message:nil
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                             [alert show];
                             [SVProgressHUD dismiss];
                         }
                         
                         
                         
                     }];
    
}

-(void)deny: (id)sender
{
    
    //[self loaderShowing];
    
    
    
    
    
    [UIView animateWithDuration:0.0
     
                     animations:^{
                         
                         
                         
                         [SVProgressHUD showWithStatus:@"Denying Request"];
                         
//                         tab_request.userInteractionEnabled = NO;
                         
                     }
     
                     completion:^(BOOL finished){
                         
                         
                         
                         NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                         
                         NSString *readUrl =[NSString stringWithFormat:@"https://budnav.com/ext/?action=notification_read&id=%@&access_token=%@&device_id=%@",[[requestslist objectAtIndex:[sender tag]] objectForKey:@"id"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
                         
                         DebugLog(@"read url: %@",readUrl);
                         
                         NSString *newString1 = [readUrl stringByReplacingOccurrencesOfString:@" " withString:@""];
                         
                         
                         
                         NSData *readdataUrl =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
                         
                         
                         
                         if (readdataUrl!=nil)
                             
                         {
                             
                             NSError *error=nil;
                             
                             NSDictionary *json_accept = [NSJSONSerialization JSONObjectWithData:readdataUrl //1
                                                          
                                                                                         options:kNilOptions
                                                          
                                                                                           error:&error];
                             
                             DebugLog(@"accept json returns: %@",json_accept);
                             
                             NSString *errornumber= [NSString stringWithFormat:@"%@",[json_accept objectForKey:@"errorno"]];
                             
                             DebugLog(@"err  %@",errornumber);
                             
                             
                             
                             if ([[json_accept objectForKey:@"success"]intValue] == 1)
                                 
                             {
                                 
                                 
                                 
                                 DebugLog(@"deny sender: %ld",(long)[sender tag]);
                                 
                                 if ([[NSString stringWithFormat:@"%@",[[requestslist objectAtIndex:[sender tag]] objectForKey:@"request"]] isEqualToString:@"contact"])
                                     
                                 {
                                     
                                     if ([[[[requestslist objectAtIndex:[sender tag]] objectForKey:@"params"] objectForKey:@"type"] isEqualToString:@"business"])
                                     {
                                         [[GAI sharedInstance].defaultTracker send:
                                          [[GAIDictionaryBuilder createEventWithCategory:@"Notification Deny"
                                                                                  action:@"Deny business contact request"
                                                                                   label:nil
                                                                                   value:nil] build]];
                                     }
                                     else
                                     {
                                         [[GAI sharedInstance].defaultTracker send:
                                          [[GAIDictionaryBuilder createEventWithCategory:@"Notification Deny"
                                                                                  action:@"Deny personal contact request"
                                                                                   label:nil
                                                                                   value:nil] build]];
                                     }
                                     
                                     
                                     
                                     NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                                     
                                     NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=notification_remove&id=%@&access_token=%@&device_id=%@",[NSString stringWithFormat:@"%@",[[requestslist objectAtIndex:[sender tag]] objectForKey:@"id"]],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
                                     
                                     
                                     
                                     DebugLog(@"deny url: %@",urlString1);
                                     
                                     NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
                                     
                                     
                                     
                                     NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
                                     
                                     
                                     
                                     NSError *error=nil;
                                     
                                     NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                
                                                                                               options:kNilOptions
                                                                
                                                                                                 error:&error];
                                     
                                     DebugLog(@"deny json returns: %@",json_deny);
                                     
                                     
                                     
                                     NSString *errornumber= [NSString stringWithFormat:@"%@",[json_deny objectForKey:@"errorno"]];
                                     
                                     DebugLog(@"err  %@",errornumber);
                                     tab_request.userInteractionEnabled = YES;
                                     if ([[json_deny objectForKey:@"success"]intValue] == 1)
                                         
                                     {
                                         
                                         //[act1 removeFromSuperview];
                                         
                                         alert = [[UIAlertView alloc] initWithTitle:@"Request Successfully Denied!"
                                                  
                                                                            message:nil
                                                  
                                                                           delegate:self
                                                  
                                                                  cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                                         
                                         [alert show];
                                         
                                         
                                         
                                         [self loadrequestsdataurl];
                                         
                                         [tab_request reloadData];
                                         
                                         //            [self loadrequestsdata];
                                         
                                         //            //        tab_request =Nil;
                                         
                                         //            [tab_request reloadData];
                                         
                                         [SVProgressHUD dismiss];
                                         
                                         
                                         
                                     }
                                     
                                     else if (![errornumber isEqualToString:@"0"])
                                         
                                     {
                                         
                                         NSString *err_str = [json_deny objectForKey:@"error"];
                                         
                                         [SVProgressHUD showErrorWithStatus:err_str];
                                         
                                     }
                                     
                                 }
                                 
                                 else
                                     
                                 {
                                     
                                     if ([[[requestslist objectAtIndex:[sender tag]] objectForKey:@"type"] intValue]==8)
                                         
                                     {
                                         
                                         [[GAI sharedInstance].defaultTracker send:
                                          [[GAIDictionaryBuilder createEventWithCategory:@"Notification Deny"
                                                                                  action:@"Deny location request"
                                                                                   label:nil
                                                                                   value:nil] build]];
                                         
                                         
                                         
                                         NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                                         
                                         NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=notification_remove&id=%@&access_token=%@&device_id=%@&thumb=true",[NSString stringWithFormat:@"%@",[[requestslist objectAtIndex:[sender tag]] objectForKey:@"id"]],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
                                         
                                         
                                         
                                         DebugLog(@"deny url: %@",urlString1);
                                         
                                         NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
                                         
                                         
                                         
                                         NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
                                         
                                         
                                         
                                         NSError *error=nil;
                                         
                                         NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                    
                                                                                                   options:kNilOptions
                                                                    
                                                                                                     error:&error];
                                         
                                         DebugLog(@"deny json returns: %@",json_deny);
                                         
                                         
                                         
                                         NSString *errornumber= [NSString stringWithFormat:@"%@",[json_deny objectForKey:@"errorno"]];
                                         
                                         DebugLog(@"err  %@",errornumber);
                                         
                                         tab_request.userInteractionEnabled = YES;
                                         if ([[json_deny objectForKey:@"success"]intValue] == 1)
                                             
                                         {
                                             
                                             //[act1 removeFromSuperview];
                                             
                                             alert = [[UIAlertView alloc] initWithTitle:@"Request Successfully Denied!"
                                                      
                                                                                message:nil
                                                      
                                                                               delegate:self
                                                      
                                                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                                             
                                             [alert show];
                                             
                                             
                                             
                                             [self loadrequestsdataurl];
                                             
                                             [tab_request reloadData];
                                             
                                             //            [self loadrequestsdata];
                                             
                                             //            //        tab_request =Nil;
                                             
                                             //            [tab_request reloadData];
                                             
                                             [SVProgressHUD dismiss];
                                             
                                         }
                                         
                                         else if (![errornumber isEqualToString:@"0"])
                                             
                                         {
                                             
                                             NSString *err_str = [json_deny objectForKey:@"error"];
                                             
                                             [SVProgressHUD showErrorWithStatus:err_str];
                                             
                                         }
                                         
                                         
                                     }
                                     
                                     else
                                         
                                     {
                                         if ([[[[requestslist objectAtIndex:[sender tag]] objectForKey:@"params"] objectForKey:@"type"] isEqualToString:@"business"])
                                         {
                                             [[GAI sharedInstance].defaultTracker send:
                                              [[GAIDictionaryBuilder createEventWithCategory:@"Notification Deny"
                                                                                      action:@"Deny business group request"
                                                                                       label:nil
                                                                                       value:nil] build]];
                                         }
                                         else
                                         {
                                             [[GAI sharedInstance].defaultTracker send:
                                              [[GAIDictionaryBuilder createEventWithCategory:@"Notification Deny"
                                                                                      action:@"Deny personal group request"
                                                                                       label:nil
                                                                                       value:nil] build]];
                                         }
                                         
                                         
                                         NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                                         
                                         NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=notification_remove&id=%@&access_token=%@&device_id=%@&thumb=true",[NSString stringWithFormat:@"%@",[[requestslist objectAtIndex:[sender tag]] objectForKey:@"id"]],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
                                         
                                         
                                         
                                         DebugLog(@"deny url: %@",urlString1);
                                         
                                         NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
                                         
                                         
                                         
                                         NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
                                         
                                         
                                         
                                         NSError *error=nil;
                                         
                                         NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                    
                                                                                                   options:kNilOptions
                                                                    
                                                                                                     error:&error];
                                         
                                         DebugLog(@"deny json returns: %@",json_deny);
                                         
                                         
                                         
                                         NSString *errornumber= [NSString stringWithFormat:@"%@",[json_deny objectForKey:@"errorno"]];
                                         
                                         DebugLog(@"err  %@",errornumber);
                                         tab_request.userInteractionEnabled = YES;
                                         
                                         if ([[json_deny objectForKey:@"success"]intValue] == 1)
                                             
                                         {
                                             
                                             //[act1 removeFromSuperview];
                                             
                                             alert = [[UIAlertView alloc] initWithTitle:@"Request Successfully Denied!"
                                                      
                                                                                message:nil
                                                      
                                                                               delegate:self
                                                      
                                                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                                             
                                             [alert show];
                                             
                                             
                                             
                                             [self loadrequestsdataurl];
                                             
                                             [tab_request reloadData];
                                             
                                             //            [self loadrequestsdata];
                                             
                                             //            //        tab_request =Nil;
                                             
                                             //            [tab_request reloadData];
                                             
                                             [SVProgressHUD dismiss];
                                             
                                             
                                             
                                         }
                                         
                                         else if (![errornumber isEqualToString:@"0"])
                                             
                                         {
                                             
                                             NSString *err_str = [json_deny objectForKey:@"error"];
                                             
                                             [SVProgressHUD showErrorWithStatus:err_str];
                                             
                                         }
                                         
                                     }
                                     
                                 }
                                 
                                 
                                 
                             }
                             
                             else
                                 
                             {
                                 tab_request.userInteractionEnabled = YES;
                                 NSString *err_str = [json_accept objectForKey:@"error"];
                                 
                                 [SVProgressHUD showErrorWithStatus:err_str];
                                 
                             }
                             
                         }
                         
                         else
                             
                         {
                             tab_request.userInteractionEnabled = YES;
                             alert = [[UIAlertView alloc] initWithTitle:@"Check Your Internet Connection!"
                                      
                                                                message:nil
                                      
                                                               delegate:self
                                      
                                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                             
                             [alert show];
                             
                             [SVProgressHUD dismiss];
                             
                         }
                         
                     }];
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    move=0;
    [tab_request removeFromSuperview];
    tab_request = Nil;
    [mainview removeFromSuperview];
    //  [requestslist removeAllObjects];
    [super viewDidDisappear:YES];
}



/*    Fire the Requests Url in Background when data is shown from local database    */
-(void) loadrequestsfromweb
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=notification_get&id=%@&access_token=%@&device_id=%@&thumb=true",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
    DebugLog(@"requests url web: %@",urlString1);
    
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    NSString *errornumber;
    if (signeddataURL1 != nil)
    {
        NSError *error=nil;
        
        json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                 
                                                options:kNilOptions
                 
                                                  error:&error];
        
        DebugLog(@"All requests: %@",json1);
        errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
        DebugLog(@"err  %@",errornumber);
        NSString *err_str= @"Log in again";
        if (![errornumber isEqualToString:@"0"])
        {
            DebugLog(@"error in retrieving data");
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Error!" message:err_str delegate:self cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                
                [alert1 show];
                
                [act removeFromSuperview];
                
            }];
        }
        else
        {
            requestslist = [[NSMutableArray alloc]init];
            groupadminlist = [[NSMutableArray alloc]init];
            
            //                DebugLog(@"details req= %@",[json1 objectForKey:@"details"]);
            if ([[json1 objectForKey:@"details"] isKindOfClass:[NSNull class]] || [json1 objectForKey:@"details"] == (id)[NSNull null] ||[[json1 objectForKey:@"details"] count] ==0)
            {
                DebugLog(@"no requests found");
                
                [self performSelectorOnMainThread:@selector(loaddata) withObject:nil waitUntilDone:YES];
            }
            else
            {
                DebugLog(@"else called req");
                NSDictionary *newdict= [json1 objectForKey:@"details"];
                requestslist = [[newdict objectForKey:@"notifications"] mutableCopy];
                [self performSelectorOnMainThread:@selector(loaddata) withObject:nil waitUntilDone:YES];
                //  [[NSUserDefaults standardUserDefaults] setObject:requestslist forKey:@"yourRequests"];
                
                //                for (NSDictionary *reqdict in [newdict objectForKey:@"requests"])
                //                {
                //                    if ([[reqdict objectForKey:@"id"] intValue] != [[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]intValue])
                //                        [requestslist addObject:reqdict];
                //                }
                //                for (NSDictionary *reqdict1 in [newdict objectForKey:@"requestsForGroups"])
                //                {
                //
                //                    [requestslist addObject:reqdict1];
                //                }
                //                for (NSDictionary *reqdict2 in [newdict objectForKey:@"requestsToGroupAdmin"])
                //                {
                //
                //                    [groupadminlist addObject:reqdict2];
                //                }
                //
                //                [self performSelectorOnMainThread:@selector(loaddata) withObject:nil waitUntilDone:YES];
                //                [[NSUserDefaults standardUserDefaults] setObject:requestslist forKey:@"yourRequests"];
                //                [[NSUserDefaults standardUserDefaults] setObject:groupadminlist forKey:@"yourGroupAdminRequests"];
                //                DebugLog(@"group admin request list: %@",groupadminlist);
                //                DebugLog(@"requests list array has: %@",requestslist);
                //                DebugLog(@"request count of contact and group is %lu",(unsigned long)[requestslist count]);
                //                DebugLog(@"request count of group admin is %lu",(unsigned long)[groupadminlist count]);
                
                
            }
            
            
        }
        
    }
    else
    {
        DebugLog(@"error in server connection");
        //        [self loaddatabase_data];
        UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@"Error!" message:@"Server Error" delegate:self cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert2 show];
        [act removeFromSuperview];
    }
    
    
}
-(void)loaddata
{
    DebugLog(@"GROUP ADMIN LIST:%@",groupadminlist);
    [nouserlb removeFromSuperview];
    if ([requestslist count] ==0 && [groupadminlist count]==0)
    {
        nouserlb= [[UILabel alloc]initWithFrame:CGRectMake(10, 70, [UIScreen mainScreen].bounds.size.width-20, 60)];
        [mainview addSubview:nouserlb];
        nouserlb.numberOfLines=3;
        nouserlb.text=@"There are no new contact requests. Improve findability by completing your profile.";
        nouserlb.backgroundColor=[UIColor clearColor];
        nouserlb.font=[UIFont fontWithName:@"ProximaNova-Bold" size:16];
        nouserlb.textAlignment=NSTextAlignmentCenter;
        nouserlb.textColor=[UIColor blackColor];
        [act removeFromSuperview];
    }
    else
    {
        [tab_request removeFromSuperview];
        tab_request=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, mainview.frame.size.height)];
        [mainview addSubview:tab_request];
        tab_request.backgroundColor=[UIColor clearColor];
        tab_request.dataSource=self;
        tab_request.delegate=self;
        tab_request.separatorStyle=UITableViewCellSeparatorStyleNone;
        tab_request.showsVerticalScrollIndicator=NO;
        [act removeFromSuperview];
        
        UITableViewController *tableViewController = [[UITableViewController alloc] init];
        tableViewController.tableView = tab_request;
        
        refreshcontrolnew = [[UIRefreshControl alloc] init];
        [refreshString addAttributes:@{NSForegroundColorAttributeName : [UIColor grayColor]} range:NSMakeRange(0, refreshString.length)];
        refreshcontrolnew.attributedTitle = refreshString;
        [refreshcontrolnew addTarget:self action:@selector(reloadAgain) forControlEvents:UIControlEventValueChanged];
        tableViewController.refreshControl = refreshcontrolnew;
        refreshcontrolnew.tintColor=[UIColor grayColor];
        
        
    }
    
    
    
}


-(void) loaddatabase_data
{
    NSOperationQueue *queue = [NSOperationQueue new];
    [queue addOperationWithBlock:^{
        NSMutableArray *retval = [[DBManager getSharedInstance] fetchRequests];//[[NSUserDefaults standardUserDefaults]objectForKey:@"yourRequests"];
        requestslist = [retval mutableCopy];
        NSMutableArray *grpretval = [[NSUserDefaults standardUserDefaults]objectForKey:@"yourGroupAdminRequests"];
        groupadminlist = [grpretval mutableCopy];
        [act removeFromSuperview];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            [del showTabValues:YES];
        });
        
    }];
    
}

-(void)startDBInsert
{
    //    DebugLog(@"start db insert : %@",requestslist);
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del loadRequestsIntoDB:requestslist];
}

-(void)loaderShowing
{
    act1 = [[UIActivityIndicatorView alloc] init];
    act1.center = self.view.center;
    act1.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [act1 setColor:[UIColor blackColor]];
    [mainview addSubview:act1];
    [act startAnimating];
    
    
}

-(void)highlight:(id)sender
{
    [sender setBackgroundColor:[UIColor colorWithRed:(40.0f/255.0f) green:(85.0f/255.0f) blue:(38.0f/255.0f) alpha:1.0f]];
}

-(void)deny_highlight:(id)sender
{
    [sender setBackgroundColor:[UIColor colorWithRed:(135.0f/255.0f) green:(135.0f/255.0f) blue:(135.0f/255.0f) alpha:1.0f]];
    [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
}

-(void)errorAlert
{
    alert = [[UIAlertView alloc] initWithTitle:@"Error in Retrieving Data!"
             
                                       message:err_str1
             
                                      delegate:self
             
                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
    
    [alert show];
    
    //            //        [SVProgressHUD dismiss];
    
    [act removeFromSuperview];
    
}


-(void)reloadAgain
{
    [tab_request removeFromSuperview];
    [self loadrequestsdataurl];
    [refreshcontrolnew endRefreshing];
    //[tab_request  reloadData];
}

-(UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}


- (void)tappedTextView:(UITapGestureRecognizer *)tapGesture {
    if (tapGesture.state != UIGestureRecognizerStateEnded) {
        return;
    }
    
    NSString *pressedWord = [self getPressedWordWithRecognizer:tapGesture];
    DebugLog(@"pressedWord: %@",pressedWord);
    
    DebugLog(@"Request Dictionary:%@",[requestslist objectAtIndex:tapGesture.view.tag]);
    
    // [string rangeOfString:@"bla"].location == NSNotFound
    
    //  if ([pressedWord isEqualToString:[[[requestslist objectAtIndex:tapGesture.view.tag] objectForKey:@"params"] objectForKey:@"name"]])
    
    if (![[[requestslist objectAtIndex:tapGesture.view.tag] objectForKey:@"params"] isKindOfClass:[NSNull class]] && [[[requestslist objectAtIndex:tapGesture.view.tag] objectForKey:@"params"] count]>0 && [[[[requestslist objectAtIndex:tapGesture.view.tag] objectForKey:@"params"] objectForKey:@"name"] rangeOfString:pressedWord].location != NSNotFound)
    {
        if ([[[requestslist objectAtIndex:tapGesture.view.tag] objectForKey:@"type"] intValue]==1)
        {
            if ([[[requestslist objectAtIndex:tapGesture.view.tag] objectForKey:@"status"] intValue]==1)
            {
                ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
                con.request = TRUE;
                con.other=@"yes";
                con.uid=[NSString stringWithFormat:@"%@",[[[requestslist objectAtIndex:tapGesture.view.tag] objectForKey:@"params"] objectForKey:@"user"]];//[NSString stringWithFormat:@"%ld",(long)[sender tag]];
                
                DebugLog(@"profile page uid fetched: %@",con.uid);
                
                [self.navigationController pushViewController:con animated:YES];
            }
            else
            {
                AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                ConNewRequestsViewController *mng = [[ConNewRequestsViewController alloc]init];
                mng.userid = [[[[requestslist objectAtIndex:tapGesture.view.tag] objectForKey:@"params"] objectForKey:@"user"] intValue];
                appDel.newrequestRedirect = NO;
                
                [self.navigationController pushViewController:mng animated:YES];
            }
        }
        else if ([[[requestslist objectAtIndex:tapGesture.view.tag] objectForKey:@"type"] intValue]==5)
        {
            AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            ConNewRequestsViewController *mng = [[ConNewRequestsViewController alloc]init];
            mng.userid = [[[[requestslist objectAtIndex:tapGesture.view.tag] objectForKey:@"params"] objectForKey:@"user"] intValue];
            appDel.newrequestRedirect = NO;
            
            [self.navigationController pushViewController:mng animated:YES];
        }
        else
        {
            DebugLog(@"Word Pressed:%@",[[[requestslist objectAtIndex:tapGesture.view.tag] objectForKey:@"params"] objectForKey:@"name"]);
            ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
            con.request = TRUE;
            con.other=@"yes";
            con.uid=[NSString stringWithFormat:@"%@",[[[requestslist objectAtIndex:tapGesture.view.tag] objectForKey:@"params"] objectForKey:@"user"]];//[NSString stringWithFormat:@"%ld",(long)[sender tag]];
            
            DebugLog(@"profile page uid fetched: %@",con.uid);
            
            [self.navigationController pushViewController:con animated:YES];
        }
        //        YDSignUpViewController *signupvc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SignUp"];
        //        [self.navigationController pushViewController:signupvc animated:YES];
    }
    else if ([[[requestslist objectAtIndex:tapGesture.view.tag] objectForKey:@"type"] intValue]==3 || [[[requestslist objectAtIndex:tapGesture.view.tag] objectForKey:@"type"] intValue]==4 || [[[requestslist objectAtIndex:tapGesture.view.tag] objectForKey:@"type"] intValue]==7 || [[[requestslist objectAtIndex:tapGesture.view.tag] objectForKey:@"type"] intValue]==9 || [[[requestslist objectAtIndex:tapGesture.view.tag] objectForKey:@"type"] intValue]==8)
    {
        if ([[[requestslist objectAtIndex:tapGesture.view.tag] objectForKey:@"type"] intValue]==9 && [pressedWord isEqualToString:@"location"])
        {
            
            [tab_request removeFromSuperview];
            ConLocateGroupViewController *con = [[ConLocateGroupViewController alloc]init];
            
            con.group_id = [NSString stringWithFormat:@"%@",[[[requestslist objectAtIndex:tapGesture.view.tag] objectForKey:@"params"] objectForKey:@"group"]];
            
            
            [self.navigationController pushViewController:con animated:YES];
            
            [self.tabBarController.tabBar setHidden:YES];
        }
        else
        {
        
        if ([[[[requestslist objectAtIndex:tapGesture.view.tag] objectForKey:@"params"] objectForKey:@"groupname"] rangeOfString:pressedWord].location != NSNotFound)
        {
            
            NSString *grp_id = [NSString stringWithFormat:@"%@",[[[requestslist objectAtIndex:tapGesture.view.tag] objectForKey:@"params"] objectForKey:@"group"]];
            NSString *group_name = [NSString stringWithFormat:@"%@",[[[requestslist objectAtIndex:tapGesture.view.tag] objectForKey:@"params"] objectForKey:@"groupname"]];
            
            GroupDetailsViewController *group_page = [[GroupDetailsViewController alloc] init];
            
            group_page.groupname = group_name;
            group_page.isadmin = @"0";
            group_page.groupid=grp_id;
            DebugLog(@"group id: %@",group_page.groupid);
            
            //group_page.grouppic=[UIImage imageWithData:decodedData];
            group_page.accept_ckeck = TRUE;
            
            del.tabBarController.tabBar.frame = CGRectMake(0, 0, 0, 0);
            [tab_request removeFromSuperview];
            [self.navigationController pushViewController:group_page animated:YES];
        }
    }
    
    }
    
    if ([[[requestslist objectAtIndex:tapGesture.view.tag] objectForKey:@"type"] intValue]==6)
    {
        if ([@"Multiple friends" rangeOfString:pressedWord].location != NSNotFound)
        {
            DebugLog(@"Multiple Friends");
            
            ConAddFriendViewController *con = [[ConAddFriendViewController alloc]init];
            
            [[self navigationController] pushViewController:con animated:YES];
            
        }
        
    }
    
}

-(NSString*)getPressedWordWithRecognizer:(UIGestureRecognizer*)recognizer
{
    UITextView *textView = (UITextView *)recognizer.view;
    //get location
    CGPoint location = [recognizer locationInView:textView];
    UITextPosition *tapPosition = [textView closestPositionToPoint:location];
    UITextRange *textRange = [textView.tokenizer rangeEnclosingPosition:tapPosition withGranularity:UITextGranularityWord inDirection:UITextLayoutDirectionRight];
    
    return [textView textInRange:textRange];
}

//------------ Current Location Address-----


-(void)CurrentLocationIdentifier
{
    
    if (![CLLocationManager locationServicesEnabled]) {
        
        if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            
            get_location = false;
            
            alert = [[UIAlertView alloc] initWithTitle:@"App Permission Denied"
                     
                                               message:@"To re-enable, please go to Settings and turn on Location Service for this app."
                     
                                              delegate:nil
                     
                                     cancelButtonTitle:@"OK"
                     
                                     otherButtonTitles:nil];
            
            alert.delegate = self;
            
            alert.tag = 121;
            
            [alert show];
            [SVProgressHUD dismiss];
        
        }
        else
        {
            //---- For getting current gps location
            
            locationManager = [CLLocationManager new];
            
            locationManager.delegate = self;
            
            locationManager.distanceFilter = kCLDistanceFilterNone;
            
            locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            
            [locationManager startUpdatingLocation];
            
            //------
            
//            if (![latitude isKindOfClass:[NSNull class]] && ![latitude isEqualToString:@"(null)"] && latitude!=nil && ![longitude isKindOfClass:[NSNull class]] && ![longitude isEqualToString:@"(null)"] && longitude!=nil)
//            {
//                count = 0;
//                get_location = true;
//            }
//            else
//            {
//                if (count==3)
//                {
//                    DebugLog(@"Count:%d",count);
//                    count = 0;
//                    get_location = false;
//                    
//                    alert = [[UIAlertView alloc] initWithTitle:@"Unable to Get Your Location. Please Try Again Later!"
//                             
//                                                       message:nil
//                             
//                                                      delegate:self
//                             
//                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
//                    
//                    [alert show];
//                    [SVProgressHUD dismiss];
//                }
//                else
//                {
//                    count++;
//                    [self CurrentLocationIdentifier];
//                }
//                
//                
//                
//            }
        }
        
    }
    else
    {
        DebugLog(@"Location Services Enabled");
        
        if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            
            get_location = false;
            
            alert = [[UIAlertView alloc] initWithTitle:@"App Permission Denied"
                     
                                               message:@"To re-enable, please go to Settings and turn on Location Service for this app."
                     
                                              delegate:nil
                     
                                     cancelButtonTitle:@"OK"
                     
                                     otherButtonTitles:nil];
            
            alert.delegate = self;
            
            alert.tag = 121;
            
            [alert show];
            [SVProgressHUD dismiss];
        }
        else
        {
            //---- For getting current gps location
            
            locationManager = [CLLocationManager new];
            
            locationManager.delegate = self;
            
            locationManager.distanceFilter = kCLDistanceFilterNone;
            
            locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            
            [locationManager startUpdatingLocation];
            
            
            //------
            
//            if (![latitude isKindOfClass:[NSNull class]] && ![latitude isEqualToString:@"(null)"] && latitude!=nil && ![longitude isKindOfClass:[NSNull class]] && ![longitude isEqualToString:@"(null)"] && longitude!=nil)
//            {
//                count = 0;
//                get_location = true;
//            }
//            else
//            {
//                
//                if (count==3)
//                {
//                    DebugLog(@"Count:%d",count);
//                    count = 0;
//                    get_location = false;
//                    
//                    alert = [[UIAlertView alloc] initWithTitle:@"Unable to Get Your Location. Please Try Again Later!"
//                             
//                                                       message:nil
//                             
//                                                      delegate:self
//                             
//                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
//                    
//                    [alert show];
//                    [SVProgressHUD dismiss];
//                }
//                else
//                {
//                    count++;
//                    [self CurrentLocationIdentifier];
//                }
//                
//                
//            }
        }
        
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    DebugLog(@"buttonIndex:%ld",(long)buttonIndex);
    
    if (alertView.tag == 121 && buttonIndex == 0)
    {
        //code for opening settings app in iOS 8
        
        [[UIApplication sharedApplication] openURL:[NSURL  URLWithString:UIApplicationOpenSettingsURLString]];
        
    }
    
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    DebugLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    // [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    //    DebugLog(@"didUpdateToLocation: %@", newLocation);
    currentLocation = newLocation;
    
    if (currentLocation != nil) {
        
        DebugLog(@"Lat:%ld Lon:%ld",(long)currentLocation.coordinate.latitude,(long)currentLocation.coordinate.longitude);
        longitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        latitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        
///////////////////////////////////////// ////////////////// Added on 11.05.16  /////////////////////////////////////////
        if (![latitude isKindOfClass:[NSNull class]] && ![latitude isEqualToString:@"(null)"] && latitude!=nil && ![longitude isKindOfClass:[NSNull class]] && ![longitude isEqualToString:@"(null)"] && longitude!=nil)
            {
                count = 0;
                get_location = true;
                [locationManager stopUpdatingLocation];
                locationManager = nil;
                [self acceptShareLocation];
            }
        
    }
}


- (void)timeFormatted:(int)totalSeconds
{
    // totalSeconds = totalSeconds/1000;
    seconds = totalSeconds % 60;
    minutes = (totalSeconds / 60) % 60;
    hours = totalSeconds / 3600;
    
    //return [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
}

-(void)acceptShareLocation                  ////////////////// Added on 11.05.16
{
    
    //                                     [SVProgressHUD dismiss];
    
    //  [self CurrentLocationIdentifier];
    
    if (get_location)
    {
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            [act startAnimating];
            
        }];
        
        [[NSOperationQueue new] addOperationWithBlock:^{
            
            [[GAI sharedInstance].defaultTracker send:
             
             [[GAIDictionaryBuilder createEventWithCategory:@"Notifications"
               
                                                     action:@"Accept location request"
               
                                                      label:nil
               
                                                      value:nil] build]];
            
            
            
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            
            NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=group-location_update&group_id=%@&latitude=%@&longitude=%@&access_token=%@&device_id=%@",[NSString stringWithFormat:@"%@",[[[requestslist objectAtIndex:shareTag] objectForKey:@"params"] objectForKey:@"group"]],latitude,longitude,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
            
            
            
            DebugLog(@"accept url: %@",urlString1);
            
            NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                [act stopAnimating];
                
                NSError *error=nil;
                tab_request.userInteractionEnabled = YES;
                if (signeddataURL1!=nil)
                {
                    
                    NSDictionary *json_accept = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                 
                                                                                options:kNilOptions
                                                 
                                                                                  error:&error];
                    
                    DebugLog(@"Share json returns: %@",json_accept);
                    
                    NSString *errornumber= [NSString stringWithFormat:@"%@",[json_accept objectForKey:@"errorno"]];
                    
                    DebugLog(@"err  %@",errornumber);
                    
                    
                    
                    if ([[json_accept objectForKey:@"success"]intValue] == 1)
                        
                    {
                        
                        [self loadrequestsdataurl];
                        
                        
                        
                        alert = [[UIAlertView alloc] initWithTitle:@"Location Successfully Shared!"
                                 
                                                           message:nil
                                 
                                                          delegate:self
                                 
                                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                        
                        [alert show];
                        
                        
                        
                        [SVProgressHUD dismiss];
                        
                        
                        
                        ConLocateGroupViewController *con = [[ConLocateGroupViewController alloc]init];
                        
                        con.group_id = [NSString stringWithFormat:@"%@",[[[requestslist objectAtIndex:shareTag] objectForKey:@"params"] objectForKey:@"group"]];
                        [tab_request removeFromSuperview];
                        
                        [self.navigationController pushViewController:con animated:YES];
                        
                        [self.tabBarController.tabBar setHidden:YES];
                        
                        
                    }
                    
                    else
                        
                    {
                        
                        NSString *err_str = [json_accept objectForKey:@"error"];
                        
                        [SVProgressHUD showErrorWithStatus:err_str];
                        
                    }
                    
                    
                    
                }
                
                else
                    
                {
                    
                    alert = [[UIAlertView alloc] initWithTitle:@"Check Your Internet Connection!"
                             
                                                       message:nil
                             
                                                      delegate:self
                             
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                    
                    [alert show];
                    
                    [SVProgressHUD dismiss];
                    
                }
                
                
            }];
            
        }];
        
    }
    else
    {
        [SVProgressHUD dismiss];
    }
    
}



//- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
//{
//    currentLocation = [locations objectAtIndex:0];
//    [locationManager stopUpdatingLocation];
//    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
//    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
//     {
//         if (!(error))
//         {
//             CLPlacemark *placemark = [placemarks objectAtIndex:0];
//             NSLog(@"\nCurrent Location Detected\n");
//             NSLog(@"placemark %@",placemark);
//             NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
//             NSString *Address = [[NSString alloc]initWithString:locatedAt];
//             NSString *Area = [[NSString alloc]initWithString:placemark.locality];
//             NSString *Country = [[NSString alloc]initWithString:placemark.country];
//             NSString *CountryArea = [NSString stringWithFormat:@"%@, %@", Area,Country];
//             NSLog(@"Area%@",CountryArea);
//         }
//         else
//         {
//             NSLog(@"Geocode failed with error %@", error);
//             NSLog(@"\nCurrent Location Not Detected\n");
//             //return;
//            // CountryArea = NULL;
//         }
//         /*---- For more results
//          placemark.region);
//          placemark.country);
//          placemark.locality);
//          placemark.name);
//          placemark.ocean);
//          placemark.postalCode);
//          placemark.subLocality);
//          placemark.location);
//          ------*/
//     }];
//}

@end