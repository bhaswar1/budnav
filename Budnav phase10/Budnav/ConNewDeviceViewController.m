//
//  ConNewDeviceViewController.m
//  Budnav
//
//  Created by intel on 03/06/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "ConNewDeviceViewController.h"
#import "ConNewContactsViewController.h"
#import "ConRequestViewController.h"
#import "ConNewProfileViewController.h"
#import "ConInviteViewController.h"
#import "ConAccountSettingsViewController.h"
#import "ConSyncLoaderViewController.h"
#import "ConQROptionsViewController.h"
#import "ConPictureProfViewController.h"
#import "SocialWebViewController.h"
#import "DBManager.h"
#import "AppDelegate.h"
#import "ConAddFriendViewController.h"
#import "ConNewRequestsViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"
#import "ConLocateGroupViewController.h"
#import "ConFullSettingsViewController.h"
#import "ConPersonalProfileViewController.h"

@interface ConNewDeviceViewController ()<UIActionSheetDelegate>
{
    int switchprof, allowpersonal, nobusiness, profile_insertion, profile_fetch, move;
    double latitude,longitude;
    AppDelegate *del;
    NSUserDefaults *prefs;
    NSString *user_id, *base64String, *fullname, *idnew, *emaildata, *streetdata, *citydata, *zipdata, *countrydata, *housenumberdata, *mail, *googleMapUrlString, *appleMapUrlString, *websitedata;
    UIView *coverView, *mainview;
    CAGradientLayer *gradient;
    UIImageView *logo_img, *bck_img, *moreImage, *profile_image, *call_img, *sms_img;
    UIView *change, *grayback, *mobileView, *homeView, *emailView, *addressView, *websiteView, *workView;
    UILabel *backlbl, *labelHead, *prof_name, *location, *mobile_num, *home_num, *work_num, *mobileView_div, *workView_div;
    UIButton *backBtn, *rightMenu, *call_btn, *sms_btn;
    UIActivityIndicatorView *act;
    NSMutableDictionary *dict_profile, *deviceContactsPhoneDetails;
    NSDictionary *json1;
    UIAlertView *alert;
    UIScrollView *scrollView;
    NSMutableArray *mobileArray;
    NSOperationQueue *loadqueue;
    UIImage *image_prof, *image_prof_original;
    UITextView *address;
    BOOL invite_Me;
    
    NSString *home, *mobile, *work;
}

@end

@implementation ConNewDeviceViewController

- (void)viewDidLoad
{
    switchprof=0;
    [super viewDidLoad];
    NSLog(@"From ConNewDeviceViewController");
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"error"];
    self.tabBarController.tabBar.hidden = YES;
    // Do any additional setup after loading the view.
    
    prefs = [NSUserDefaults standardUserDefaults];
    DebugLog(@"user id %@",[prefs objectForKey:@"userid"]);
    
    self.view.backgroundColor=[UIColor whiteColor];
    self.tabBarController.tabBar.translucent = YES;
    
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.navigationController.navigationBarHidden=YES;
    
    
    coverView = [[UIView alloc]init];
    coverView.frame = CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width,64);
    coverView.userInteractionEnabled=YES;
    [self.view addSubview:coverView];
    
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, coverView.frame.size.height);
    
    // Add the gradient to the view
    [coverView.layer insertSublayer:gradient atIndex:0];
    
    
    logo_img = [[UIImageView alloc]init];
    logo_img.frame = CGRectMake(27+([UIScreen mainScreen].bounds.size.width-160)/2, 32, 101, 20);
    logo_img.backgroundColor = [UIColor clearColor];
    
    logo_img.image = [UIImage imageNamed:@"new_logo"];
    [coverView addSubview:logo_img];
    
    
    //    if ([_other isEqualToString:@"yes"])
    //    {
    //        user_id =_uid;
    //
    //    }
    //    else
    //    {
    //        user_id= [prefs objectForKey:@"userid"];
    //
    //    }
    
    if ([user_id intValue] == [[prefs objectForKey:@"userid"] intValue])
    {
        DebugLog(@"IFFFFFF=============>");
        self.tabBarController.tabBar.frame = CGRectMake(0, [[UIScreen mainScreen]bounds].size.height-44, [UIScreen mainScreen].bounds.size.width, 44);
        self.tabBarController.tabBar.hidden = NO;
        
        change = [[UIView alloc]initWithFrame:CGRectMake(8,32,115,30)];
        [change setBackgroundColor:[UIColor clearColor]];
        [coverView addSubview:change];
        
        
        bck_img=[[UIImageView alloc]initWithFrame:CGRectMake(7, 0, 12, 20)];//(9,33,10,20)];//(13, 39, 10, 22)];
        bck_img.image=[UIImage imageNamed:@"back3"];
        [change addSubview:bck_img];
        
        backlbl=[[UILabel alloc]initWithFrame:CGRectMake(bck_img.frame.origin.x+bck_img.frame.size.width+5, 3, 50, 20)];//(bck_img.frame.origin.x+bck_img.frame.size.width+5, 28, 54, 30)];
        backlbl.text=@"Back";
        backlbl.textColor=[UIColor whiteColor];
        backlbl.backgroundColor=[UIColor clearColor];
        backlbl.userInteractionEnabled=YES;
        
        
        backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        backBtn.frame = change.frame;//CGRectMake(bck_img.frame.origin.x, bck_img.frame.origin.y, backlbl.frame.origin.x+backlbl.frame.size.width-bck_img.frame.origin.x, backlbl.frame.size.height);
        backBtn.backgroundColor=[UIColor clearColor];
        [backBtn setTitle:@"" forState:UIControlStateNormal];
        [backBtn addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDown];
        [backBtn addTarget:self action:@selector(changecoloragain) forControlEvents:UIControlEventTouchDragExit];
        [backBtn addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDragEnter];
        
        
        [backBtn addTarget:self action:@selector(gobackoption) forControlEvents:UIControlEventTouchUpInside];
        [coverView addSubview:backBtn];
        
    }else{
        
        DebugLog(@"ELSEEEE=============>");
        
        [self.tabBarController.tabBar setHidden:YES]; //===========OTHER PROFILE REMOVE FOOTER BUTTON BAR==========//
        change = [[UIView alloc]initWithFrame:CGRectMake(8,32,115,30)];
        [change setBackgroundColor:[UIColor clearColor]];
        [coverView addSubview:change];
        
        bck_img=[[UIImageView alloc]initWithFrame:CGRectMake(7, 0, 12, 20)];//(9,33,10,20)];//(13, 39, 10, 22)];
        bck_img.image=[UIImage imageNamed:@"back3"];
        [change addSubview:bck_img];
        
        backlbl=[[UILabel alloc]initWithFrame:CGRectMake(bck_img.frame.origin.x+bck_img.frame.size.width+5, 3, 50, 20)];
        backlbl.text=@"Back";
        backlbl.textColor=[UIColor whiteColor];
        backlbl.contentMode=UIViewContentModeScaleAspectFit;
        backlbl.backgroundColor=[UIColor clearColor];
        
        backBtn=[UIButton buttonWithType:UIButtonTypeCustom];//WithFrame:CGRectMake(bck_img.frame.origin.x, backlbl.frame.origin.y, backlbl.frame.origin.x+backlbl.frame.size.width-bck_img.frame.origin.x, backlbl.frame.size.height)];
        backBtn.frame=CGRectMake(0, 0, logo_img.frame.origin.x
                                 -1, 64);
        [backBtn setTitle:@"" forState:UIControlStateNormal];
        backBtn.backgroundColor=[UIColor clearColor];
        [backBtn setTitle:@"" forState:UIControlStateNormal];
        [backBtn addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDown];
        [backBtn addTarget:self action:@selector(changecoloragain) forControlEvents:UIControlEventTouchDragExit];
        [backBtn addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDragEnter];
        [backBtn addTarget:self action:@selector(gobackoption) forControlEvents:UIControlEventTouchUpInside];
        [coverView addSubview:backBtn];
        
    }
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    if (_ContactDetails_Dict !=nil)
    {
        self.screenName = @"Backup contact";
        [[GAI sharedInstance].defaultTracker send:
         
         [[GAIDictionaryBuilder createEventWithCategory:@"Contacts"
                                                 action:@"Backup contact"
                                                  label:nil
                                                  value:nil] build]];
    }
    
    nobusiness=0;
    move=0;
    profile_insertion=0;
    profile_fetch=0;
    allowpersonal=0;
    
    UIView *headingView = [[UIView alloc] initWithFrame:CGRectMake(0, 65, [UIScreen mainScreen].bounds.size.width, 101)];
    [headingView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:headingView];
    
    profile_image = [[UIImageView alloc] initWithFrame:CGRectMake(12, 12, 76, 76)];
    profile_image.backgroundColor = [UIColor clearColor];
    profile_image.layer.cornerRadius = profile_image.frame.size.width/2;
    profile_image.clipsToBounds = YES;
    profile_image.image = [UIImage imageNamed:@"Backup profile pic"];
    [headingView addSubview:profile_image];
    
    ////////////////////////////////// Contact Name /////////////////////////////////////////////
    NSString *whole_name= [[NSString stringWithFormat:@"%@ ",[_ContactDetails_Dict objectForKey:@"fullname"]] capitalizedString];
    NSArray *myWords = [whole_name componentsSeparatedByString:@" "];
    
    NSString *fnm= [[myWords objectAtIndex:0] capitalizedString];
    NSString *lnm;
    
    for (int k=1;k< [myWords count];k++)
    {
        if (k==1)
        {
            if ([[myWords objectAtIndex:k] isKindOfClass:[NSNull class]] || [myWords objectAtIndex:k] == (id)[NSNull null] ||[[myWords objectAtIndex:k] length] ==0 || [[myWords objectAtIndex:k] isEqualToString:@"(null)"])
            {
                lnm = @"";
            }
            else
                lnm= [[NSString stringWithFormat:@"%@",[myWords objectAtIndex:k]] capitalizedString];
        }
        else
            lnm= [[NSString stringWithFormat:@"%@ %@",lnm,[myWords objectAtIndex:k]] capitalizedString];
    }
    if ([fnm length] ==0 || [fnm isKindOfClass:[NSNull class]] || fnm == (id)[NSNull null] || [fnm isEqualToString:@"(null)"])
        whole_name = [[NSString stringWithFormat:@"%@",lnm] capitalizedString];
    
    else if ([lnm length] ==0 || [lnm isKindOfClass:[NSNull class]] || lnm == (id)[NSNull null] || [lnm isEqualToString:@"(null)"])
        whole_name = [[NSString stringWithFormat:@"%@",fnm] capitalizedString];
    else
        whole_name = [NSString stringWithFormat:@"%@ %@",[fnm capitalizedString],[lnm capitalizedString]];
    
    
    
    fullname= whole_name;
    
    
    
    prof_name = [[UILabel alloc] initWithFrame:CGRectMake(profile_image.frame.origin.x+profile_image.frame.size.width+24, 33, [UIScreen mainScreen].bounds.size.width-120, 35)];
    [prof_name setBackgroundColor:[UIColor clearColor]];
    prof_name.font = [UIFont fontWithName:@"ProximaNova-Bold" size:17];
    prof_name.numberOfLines = 2;
    prof_name.text = fullname;
    prof_name.lineBreakMode = NSLineBreakByWordWrapping;
    prof_name.textColor = [UIColor blackColor];
    prof_name.textAlignment = NSTextAlignmentLeft;
    [headingView addSubview:prof_name];
    [prof_name adjustsFontSizeToFitWidth];
    
    
    
    UILabel *heading_div = [[UILabel alloc] initWithFrame:CGRectMake(0, 100.5f, [UIScreen mainScreen].bounds.size.width, 0.5f)];
    [heading_div setBackgroundColor:[UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f]];
    [headingView addSubview:heading_div];
    
    UIView *actionView = [[UIView alloc] init];
    actionView.frame = CGRectMake(0, 166, [UIScreen mainScreen].bounds.size.width, 56);
    [self.view addSubview:actionView];
    
    UILabel *actionView_div = [[UILabel alloc] init];
    actionView_div.frame = CGRectMake(0, 55.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
    actionView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
    [actionView addSubview:actionView_div];
    
    call_img = [[UIImageView alloc] init];
    call_img.frame = CGRectMake(([UIScreen mainScreen].bounds.size.width/4-23)/2, 16.5, 23, 23);
    call_img.image = [UIImage imageNamed:@"Call"];
    [actionView addSubview:call_img];
    
    UILabel *call_div = [[UILabel alloc] init];
    call_div.frame = CGRectMake(call_img.frame.origin.x+call_img.frame.size.width+([UIScreen mainScreen].bounds.size.width/4-23)/2-0.5f, 0, 0.5f, 56);
    call_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
    [actionView addSubview:call_div];
    
    call_btn = [[UIButton alloc] init];
    call_btn.tag = 1;
    call_btn.frame = CGRectMake(0, 0, call_img.frame.origin.x+call_img.frame.size.width+35-0, 56);
    [call_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [call_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
    [call_btn addTarget:self action:@selector(phonefunc:) forControlEvents:UIControlEventTouchUpInside];
    [call_btn setBackgroundColor:[UIColor clearColor]];
    [actionView addSubview:call_btn];
    
    sms_img = [[UIImageView alloc] init];
    sms_img.frame = CGRectMake(call_div.frame.origin.x+call_div.frame.size.width+([UIScreen mainScreen].bounds.size.width/4-23)/2, 16.5, 23, 23);
    sms_img.image = [UIImage imageNamed:@"SMS"];
    [actionView addSubview:sms_img];
    
    UILabel *sms_div = [[UILabel alloc] init];
    sms_div.frame = CGRectMake(sms_img.frame.origin.x+sms_img.frame.size.width+([UIScreen mainScreen].bounds.size.width/4-23)/2-0.5f, 0, 0.5f, 56);
    sms_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
    [actionView addSubview:sms_div];
    
    sms_btn = [[UIButton alloc] init];
    sms_btn.frame = CGRectMake(call_div.frame.origin.x+call_div.frame.size.width, 0, sms_img.frame.origin.x+sms_img.frame.size.width+40-(call_div.frame.origin.x+call_div.frame.size.width+5), 56);
    [sms_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
    [sms_btn addTarget:self action:@selector(smsfunc) forControlEvents:UIControlEventTouchUpInside];
    [sms_btn setBackgroundColor:[UIColor clearColor]];
    [actionView addSubview:sms_btn];
    
    UIImageView *email_img = [[UIImageView alloc] init];
    email_img.frame = CGRectMake(sms_div.frame.origin.x+sms_div.frame.size.width+([UIScreen mainScreen].bounds.size.width/4-23)/2, 16.5, 23, 23);
    email_img.image = [UIImage imageNamed:@"Email(grey)"];
    [actionView addSubview:email_img];
    
    UILabel *email_div = [[UILabel alloc] init];
    email_div.frame = CGRectMake(email_img.frame.origin.x+email_img.frame.size.width+([UIScreen mainScreen].bounds.size.width/4-23)/2-0.5f, 0, 0.5f, 56);
    email_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
    [actionView addSubview:email_div];
    
    UIImageView *location_img = [[UIImageView alloc] init];
    location_img.frame = CGRectMake(email_div.frame.origin.x+email_div.frame.size.width+([UIScreen mainScreen].bounds.size.width/4-23)/2, 16.5, 23, 23);
    location_img.image = [UIImage imageNamed:@"Navigate(grey)"];
    [actionView addSubview:location_img];
    
    
    mainview = [[UIView alloc]initWithFrame:CGRectMake(0, 222, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    [self.view addSubview:mainview];
    mainview.backgroundColor=[UIColor clearColor];
    
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width,self.view.frame.size.height-62) ];
    scrollView.backgroundColor =[UIColor clearColor];
    scrollView.scrollEnabled = YES;
    scrollView.userInteractionEnabled = YES;
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width,scrollView.frame.size.height+255)];
    scrollView.contentSize = CGSizeMake(1, scrollView.frame.size.height+255);
    scrollView.showsVerticalScrollIndicator=NO;
    [mainview addSubview:scrollView];
    scrollView.contentOffset=CGPointMake(0, 0);
    
    
    act = [[UIActivityIndicatorView alloc] init];//WithFrame:CGRectMake(135, 190, 40, 40)];
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    act.center=self.view.center;
    [act setColor:[UIColor blackColor]];
    [self.view addSubview:act];
    [act startAnimating];
    
    UIView *footerView = [[UIView alloc] init];
    footerView.frame = CGRectMake(0, self.view.frame.size.height-62, [UIScreen mainScreen].bounds.size.width, 62);
    footerView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:footerView];
    
    UILabel *footer_div = [[UILabel alloc] init];
    footer_div.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0.5f);
    footer_div.backgroundColor = [UIColor colorWithRed:(29.0f/255.f) green:(157.0f/255.f) blue:(181.0f/255.f) alpha:0.8f];
    [footerView addSubview:footer_div];
    
    UILabel *footerLabel = [[UILabel alloc] init];
    footerLabel.frame = CGRectMake(0, 18, [UIScreen mainScreen].bounds.size.width, 26);
    footerLabel.text = @"Invite this contact to Budnav!";
    footerLabel.textColor = [UIColor colorWithRed:(29.0f/255.f) green:(157.0f/255.f) blue:(181.0f/255.f) alpha:0.8f];
    footerLabel.font = [UIFont fontWithName:@"ProximaNova-Bold" size:17];
    footerLabel.textAlignment = NSTextAlignmentCenter;
    [footerView addSubview:footerLabel];
    
    UIButton *footer_btn = [[UIButton alloc] init];
    footer_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 62);
    footer_btn.enabled = YES;
    footer_btn.userInteractionEnabled = YES;
    [footer_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [footer_btn setBackgroundColor:[UIColor clearColor]];
    [footer_btn addTarget:self action:@selector(inviteMe) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:footer_btn];
    
    loadqueue = [NSOperationQueue new];
    NSInvocationOperation *op = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(loaddata) object:nil];
    [loadqueue addOperation:op];
}

-(void)viewDidDisappear:(BOOL)animated
{
    mobileView.backgroundColor = [UIColor clearColor];
    homeView.backgroundColor = [UIColor clearColor];
}

-(void)loaddata
{
    DebugLog(@"id details in array contains: %@",_ContactDetails_Dict);
    NSString *userid = [_ContactDetails_Dict objectForKey:@"id"];
    idnew= [userid substringFromIndex:1];
    
    
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
    ABRecordID recordId = (ABRecordID)[idnew intValue];
    
    ABRecordRef person = ABAddressBookGetPersonWithRecordID(addressBook, recordId);
    
    ABMultiValueRef emailProperty = ABRecordCopyValue(person, kABPersonEmailProperty);
    // DebugLog(@"jjjjjnnm %@ %d %@, %@",idnew,recordId,person, emailProperty);
    
    NSArray *emailArray = (__bridge NSArray *)ABMultiValueCopyArrayOfAllValues(emailProperty);
    
    NSMutableArray *mutableemail= [emailArray mutableCopy];
    
    int qq;
    for (qq =0; qq< [mutableemail count]; qq++)
    {
        if ([[mutableemail objectAtIndex:qq] isKindOfClass:[NSNull class]] || [mutableemail objectAtIndex:qq] == (id)[NSNull null] || [[mutableemail objectAtIndex:qq] length] < 3)
        {
            [mutableemail removeObjectAtIndex:qq];
            qq= qq-1;
        }
    }
    
    emaildata = [emailArray objectAtIndex:0];
    
    deviceContactsPhoneDetails = [[NSMutableDictionary alloc]init];
    
    ABMultiValueRef phones = ABRecordCopyValue(person, kABPersonPhoneProperty);
    for(CFIndex j = 0; j < ABMultiValueGetCount(phones); j++)
    {
        CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(phones, j);
        CFStringRef locLabel = ABMultiValueCopyLabelAtIndex(phones, j);
        NSString *phoneLabel =(__bridge NSString*) ABAddressBookCopyLocalizedLabel(locLabel);
        ////CFRelease(phones);
        NSString *phoneNumber = (__bridge NSString *)phoneNumberRef;
        //CFRelease(phoneNumberRef);
        //CFRelease(locLabel);
        DebugLog(@"phno:  - %@ (%@)", phoneNumber, phoneLabel);
        [deviceContactsPhoneDetails setValue:phoneNumber forKey:phoneLabel];
    }
    
    
    DebugLog(@"details from phonebook: %@",deviceContactsPhoneDetails);
    
    ///////////////////////////////////////////////////////// Website //////////////////////////////////////////////////////////
    ABMultiValueRef websiteProperty = ABRecordCopyValue(person, kABPersonURLProperty);
    // DebugLog(@"jjjjjnnm %@ %d %@, %@",idnew,recordId,person, emailProperty);
    
    NSArray *websiteArray = (__bridge NSArray *)ABMultiValueCopyArrayOfAllValues(websiteProperty);
    
    NSMutableArray *mutablewebsite= [websiteArray mutableCopy];
    
    int qq1;
    for (qq1 =0; qq1< [mutablewebsite count]; qq1++)
    {
        if ([[mutablewebsite objectAtIndex:qq1] isKindOfClass:[NSNull class]] || [mutablewebsite objectAtIndex:qq1] == (id)[NSNull null] || [[mutablewebsite objectAtIndex:qq1] length] < 3)
        {
            [mutablewebsite removeObjectAtIndex:qq1];
            qq1= qq1-1;
        }
    }
    
    websitedata = [websiteArray objectAtIndex:0];
    
    
    
    //////////////////////////////////////////////////////  Address  ///////////////////////////////////////////////////
    
    
    ABMultiValueRef addrss = ABRecordCopyValue(person, kABPersonAddressProperty);
    //DebugLog(@"addrss: %@",addrss);
    for(CFIndex k = 0; k < ABMultiValueGetCount(addrss); k++)
    {
        ABMultiValueRef stt = ABRecordCopyValue(person, kABPersonAddressProperty);
        if (ABMultiValueGetCount(stt) > 0) {
            CFDictionaryRef dict = ABMultiValueCopyValueAtIndex(stt, 0);
            //                    DebugLog(@"dict address contacts: %@",dict);
            NSString *streetp = CFDictionaryGetValue(dict, kABPersonAddressStreetKey);
            NSString *cityp = CFDictionaryGetValue(dict, kABPersonAddressCityKey);
            NSString *zipcdep = CFDictionaryGetValue(dict, kABPersonAddressZIPKey);
            //                    NSString *statep = CFDictionaryGetValue(dict, kABPersonAddressStateKey);
            NSString *countryp = CFDictionaryGetValue(dict, kABPersonAddressCountryKey);
            
            if ([streetp isKindOfClass:[NSNull class]] || streetp == (id)[NSNull null])
                streetdata = @"";
            else
                streetdata = streetp;
            
            if ([cityp isKindOfClass:[NSNull class]] || cityp == (id)[NSNull null])
                citydata =@"";
            else
                citydata = cityp;
            
            if ([zipcdep isKindOfClass:[NSNull class]] || zipcdep == (id)[NSNull null])
                zipdata =@"";
            else
                zipdata =zipcdep;
            
            if ([countryp isKindOfClass:[NSNull class]] || countryp == (id)[NSNull null])
                countrydata =@"";
            else
                countrydata =countryp;
            
            housenumberdata = @"";
            
        }
        else
        {
            streetdata = @"";
            citydata =@"";
            zipdata =@"";
            countrydata =@"";
            housenumberdata = @"";
        }
        
    }
    
    if ([countrydata isKindOfClass:[NSNull class]] || countrydata == (id)[NSNull null] || [countrydata length] == 0 || [countrydata isEqualToString:@"(null)"]) {
        if (![[_ContactDetails_Dict objectForKey:@"country"] isKindOfClass:[NSNull class]] && [_ContactDetails_Dict objectForKey:@"country"] != (id)[NSNull null] && [[_ContactDetails_Dict objectForKey:@"country"] length] != 0 && ![[_ContactDetails_Dict objectForKey:@"country"] isEqualToString:@"(null)"])
        {
            countrydata = [_ContactDetails_Dict objectForKey:@"country"];
        }
    }
    
    if ([citydata isKindOfClass:[NSNull class]] || citydata == (id)[NSNull null] || [citydata length] == 0 || [citydata isEqualToString:@"(null)"]) {
        if (![[_ContactDetails_Dict objectForKey:@"city"] isKindOfClass:[NSNull class]] && [_ContactDetails_Dict objectForKey:@"city"] != (id)[NSNull null] && [[_ContactDetails_Dict objectForKey:@"city"] length] != 0 && ![[_ContactDetails_Dict objectForKey:@"city"] isEqualToString:@"(null)"])
        {
            citydata = [_ContactDetails_Dict objectForKey:@"city"];
        }
    }
    
    if ([zipdata isKindOfClass:[NSNull class]] || zipdata == (id)[NSNull null] || [zipdata length] == 0 || [zipdata isEqualToString:@"(null)"]) {
        if (![[_ContactDetails_Dict objectForKey:@"zipcode"] isKindOfClass:[NSNull class]] && [_ContactDetails_Dict objectForKey:@"zipcode"] != (id)[NSNull null] && [[_ContactDetails_Dict objectForKey:@"zipcode"] length] != 0 && ![[_ContactDetails_Dict objectForKey:@"zipcode"] isEqualToString:@"(null)"])
        {
            zipdata = [_ContactDetails_Dict objectForKey:@"zipcode"];
        }
    }
    
    if ([streetdata isKindOfClass:[NSNull class]] || streetdata == (id)[NSNull null] || [streetdata length] == 0 || [streetdata isEqualToString:@"(null)"]) {
        if (![[_ContactDetails_Dict objectForKey:@"street"] isKindOfClass:[NSNull class]] && [_ContactDetails_Dict objectForKey:@"street"] != (id)[NSNull null] && [[_ContactDetails_Dict objectForKey:@"street"] length] != 0 && ![[_ContactDetails_Dict objectForKey:@"street"] isEqualToString:@"(null)"])
        {
            streetdata = [_ContactDetails_Dict objectForKey:@"street"];
        }
    }
    
    if ([housenumberdata isKindOfClass:[NSNull class]] || housenumberdata == (id)[NSNull null] || [housenumberdata length] == 0 || [housenumberdata isEqualToString:@"(null)"]) {
        if (![[_ContactDetails_Dict objectForKey:@"housenumber"] isKindOfClass:[NSNull class]] && [_ContactDetails_Dict objectForKey:@"housenumber"] != (id)[NSNull null] && [[_ContactDetails_Dict objectForKey:@"housenumber"] length] != 0 && ![[_ContactDetails_Dict objectForKey:@"housenumber"] isEqualToString:@"(null)"])
        {
            housenumberdata = [_ContactDetails_Dict objectForKey:@"housenumber"];
        }
    }
    
    if ([emaildata isKindOfClass:[NSNull class]] || emaildata == (id)[NSNull null] || [emaildata length] == 0 || [emaildata isEqualToString:@"(null)"]) {
        if (![[_ContactDetails_Dict objectForKey:@"email"] isKindOfClass:[NSNull class]] && [_ContactDetails_Dict objectForKey:@"email"] != (id)[NSNull null] && [[_ContactDetails_Dict objectForKey:@"email"] length] != 0 && ![[_ContactDetails_Dict objectForKey:@"email"] isEqualToString:@"(null)"])
        {
            emaildata = [_ContactDetails_Dict objectForKey:@"email"];
        }
    }
    
    //if ([deviceContactsPhoneDetails count]==0) {
    
    
    //NSArray *strings = [[_ContactDetails_Dict objectForKey:@"phone"] componentsSeparatedByString:@","];
    // DebugLog(@"PHONE NUMBERS:%@",strings);
    
    // int count = (int)strings.count;
    //            if ([[finalwritedict objectForKey:@"phone"] length] > 2)
    //                homephone = [NSString stringWithFormat:@"%@",[finalwritedict objectForKey:@"phone"]];
    //        for (int i =0; i<count; i++) {
    //            if (i==0) {
    if ([[_ContactDetails_Dict objectForKey:@"home"] length] > 2)
    {
        home = [NSString stringWithFormat:@"%@",[[[_ContactDetails_Dict objectForKey:@"home"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"] invertedSet]] componentsJoinedByString:@""]];
        DebugLog(@"HOME:%@",home);
    }
    
    //            if (i==1) {
    if ([[_ContactDetails_Dict objectForKey:@"mobile"] length] > 2)
    {
        mobile = [NSString stringWithFormat:@"%@",[[[_ContactDetails_Dict objectForKey:@"mobile"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"] invertedSet]] componentsJoinedByString:@""]];
        DebugLog(@"Mobile%@",mobile);
    }
    
    if ([[_ContactDetails_Dict objectForKey:@"work"] length] > 2)
    {
        work = [NSString stringWithFormat:@"%@",[[[_ContactDetails_Dict objectForKey:@"work"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"] invertedSet]] componentsJoinedByString:@""]];
        DebugLog(@"Work:%@",work);
    }
    
    
    //  }
    
    
    //[deviceContactsPhoneDetails setValue:[_ContactDetails_Dict objectForKey:@"phone"] forKey:@"mobile"];
    //[deviceContactsPhoneDetails setValue:[_ContactDetails_Dict objectForKey:@"phone"] forKey:@"mobile"];
    //deviceContactsPhoneDetails = [_ContactDetails_Dict mutableCopy];
    //    }
    //
    //    else
    //    {
    
    //        NSArray *strings = [[deviceContactsPhoneDetails objectForKey:@"phoneLabel"] componentsSeparatedByString:@","];
    //        DebugLog(@"PHONE NUMBERS:%@",strings);
    //
    //        int count = (int)strings.count;
    //        //            if ([[finalwritedict objectForKey:@"phone"] length] > 2)
    //        //                homephone = [NSString stringWithFormat:@"%@",[finalwritedict objectForKey:@"phone"]];
    //        for (int i =0; i<count; i++) {
    //            if (i==0) {
    //                if ([[strings objectAtIndex:0] length] > 2)
    //                    home = [NSString stringWithFormat:@"%@",[strings objectAtIndex:0]];
    //            }
    //
    //            if (i==1) {
    //                if ([[strings objectAtIndex:1] length] > 2)
    //                    mobile = [NSString stringWithFormat:@"%@",[strings objectAtIndex:1]];
    //            }
    //
    //        }
    //
    //    }
    
    DebugLog(@"country: %@ \n  city: %@ \nData:%@",countrydata,citydata,_ContactDetails_Dict);
    //DebugLog(@"email,street, city, zipcode, housenumber, country  %@, %@, %@, %@, %@, %@",emaildata,streetdata,citydata,zipdata,housenumberdata,countrydata);
    
    
    [self performSelectorOnMainThread:@selector(displaydata) withObject:nil waitUntilDone:NO];
}

-(void)displaydata
{
    
    //=================================Address===============================//
    
    if ([countrydata isKindOfClass:[NSNull class]] || countrydata == (id)[NSNull null] || [countrydata length] == 0 || [countrydata isEqualToString:@"(null)"]) {
        
        address_str = @"";
    }
    else{
        if ([citydata isKindOfClass:[NSNull class]] || citydata == (id)[NSNull null] || [citydata length]==0 || [citydata isEqualToString:@"(null)"]) {
            address_str= [NSString stringWithFormat:@"%@",countrydata];
        }
        else{
            
            if ([zipdata isKindOfClass:[NSNull class]] || zipdata == (id)[NSNull null] || [zipdata length]==0 || [zipdata isEqualToString:@"(null)"]) {
                
                address_str= [NSString stringWithFormat:@"%@, \n%@",citydata,countrydata];
            }
            else
            {
                if ([streetdata isKindOfClass:[NSNull class]] || streetdata == (id)[NSNull null] || [streetdata length]==0 || [streetdata isEqualToString:@"(null)"]) {
                    
                    address_str= [NSString stringWithFormat:@"%@, %@ \n%@",zipdata,citydata,countrydata];
                    
                }
                else
                {
                    if ([housenumberdata isKindOfClass:[NSNull class]] || housenumberdata == (id)[NSNull null] || [housenumberdata length]==0 || [housenumberdata isEqualToString:@"(null)"]) {
                        
                        address_str= [NSString stringWithFormat:@"%@,\n%@, %@\n%@",streetdata,zipdata,citydata,countrydata];
                        
                    }
                    else{
                        address_str= [NSString stringWithFormat:@"%@, %@, \n%@, %@ \n%@",streetdata,housenumberdata,zipdata,citydata,countrydata];
                        
                    }
                    
                }
                
                
            }
            
            
        }
    }
    
    //===========================================MOBILE=======================//
    
    if (mobile != false && ![mobile isEqualToString:@"0"] && [mobile length] > 1 && ![mobile isEqualToString:@"(null)"] && ![mobile isEqualToString:@""])
    {
        
        mobileView = [[UIView alloc] init];
        mobileView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 62);
        mobileView.backgroundColor = [UIColor clearColor];
        [scrollView addSubview:mobileView];
        
        mobileView_div = [[UILabel alloc] init];
        mobileView_div.frame = CGRectMake(0, 61.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
        mobileView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        [mobileView addSubview:mobileView_div];
        
        UILabel *mobileLabel = [[UILabel alloc] init];
        mobileLabel.frame = CGRectMake(20, 10, 100, 22);
        mobileLabel.text = @"MOBILE";
        mobileLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        mobileLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
        [mobileView addSubview:mobileLabel];
        
        mobile_num = [[UILabel alloc] init];
        mobile_num.frame = CGRectMake(20, 32, 150, 22);
        //mobile_num.text = @"";
        mobile_num.textColor = [UIColor blackColor];
        mobile_num.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
        [mobileView addSubview:mobile_num];
        
        UIImageView *whatsapp_img = [[UIImageView alloc] init];
        whatsapp_img.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-50, 20, 22, 22);
        whatsapp_img.image = [UIImage imageNamed:@"Whatsapp"];
       // [mobileView addSubview:whatsapp_img];
        
        UIButton *whatsapp_btn = [[UIButton alloc] init];
        whatsapp_btn.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-65, 10, 52, 42);
        [whatsapp_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [whatsapp_btn setBackgroundColor:[UIColor clearColor]];
        [whatsapp_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
        [whatsapp_btn addTarget:self action:@selector(whatsapp_func) forControlEvents:UIControlEventTouchUpInside];
      //  [mobileView addSubview:whatsapp_btn];
        
        //        NSArray *mobString = [[NSArray alloc] initWithObjects:[[deviceContactsPhoneDetails objectForKey:@"mobile"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"] invertedSet]], nil];
        //        DebugLog(@"Mobile Number:%@",[mobString objectAtIndex:0]);
        //
        //        mobile_num.text = [[[deviceContactsPhoneDetails objectForKey:@"mobile"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"] invertedSet]] ];
        //mobile_num.text = [NSString stringWithFormat:@"%@",[deviceContactsPhoneDetails objectForKey:@"mobile"]];
        
        mobile_num.text = mobile ; ////change
        
        UIButton *mobileView_btn = [[UIButton alloc] init];
        mobileView_btn.tag = 1;
        mobileView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-65, 62);
        [mobileView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [mobileView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
        [mobileView_btn addTarget:self action:@selector(phonefunc:) forControlEvents:UIControlEventTouchUpInside];
        [mobileView addSubview:mobileView_btn];
        
        
    }
    
    
    //=========================================HOME===============================//
    homeView = [[UIView alloc] init];
    
    if (![home isKindOfClass:[NSNull class]] && ![home isEqualToString:@"(null)"] && ![home isEqualToString:@""] && [home length]>0)
    {
        
        homeView.frame = CGRectMake(0, mobileView.frame.origin.y+mobileView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 62);
        homeView.backgroundColor = [UIColor clearColor];
        [scrollView addSubview:homeView];
        
        UILabel *homeView_div = [[UILabel alloc] init];
        homeView_div.frame = CGRectMake(0, 61.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
        homeView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        [homeView addSubview:homeView_div];
        
        UILabel *homeLabel = [[UILabel alloc] init];
        homeLabel.frame = CGRectMake(20, 10, 100, 22);
        homeLabel.text = @"HOME";
        homeLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        homeLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
        [homeView addSubview:homeLabel];
        
        home_num = [[UILabel alloc] init];
        home_num.frame = CGRectMake(20, 32, 200, 22);
        //home_num.text = @"+31 70 51";
        home_num.textColor = [UIColor blackColor];
        home_num.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
        [homeView addSubview:home_num];
        
        home_num.text = home ; //change
        
        
        // home_num.text = [[[deviceContactsPhoneDetails objectForKey:@"home"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"] invertedSet]] componentsJoinedByString:@""];
        //home_num.text = [NSString stringWithFormat:@"%@",[deviceContactsPhoneDetails objectForKey:@"home"]];
        UIButton *homeView_btn = [[UIButton alloc] init];
        if (mobileView) {
            homeView_btn.tag = 2;
        }
        else
        {
            homeView_btn.tag = 1;
        }
        
        homeView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 62);
        [homeView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [homeView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
        [homeView_btn addTarget:self action:@selector(phonefunc:) forControlEvents:UIControlEventTouchUpInside];
        [homeView addSubview:homeView_btn];
    }
    else
    {
        homeView.frame = CGRectMake(0, mobileView.frame.origin.y+mobileView.frame.size.height, 0, 0);
    }
    
    
    //======================================================WORK=======================================================//
    workView = [[UIView alloc] init];
    
    if (![work isKindOfClass:[NSNull class]] && ![work isEqualToString:@"(null)"] && ![work isEqualToString:@""] && [work length]>0)
    {
        
        workView.frame = CGRectMake(0, homeView.frame.origin.y+homeView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 62);
        workView.backgroundColor = [UIColor clearColor];
        [scrollView addSubview:workView];
        
        workView_div = [[UILabel alloc] init];
        workView_div.frame = CGRectMake(0, 61.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
        workView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        [workView addSubview:workView_div];
        
        UILabel *workLabel = [[UILabel alloc] init];
        workLabel.frame = CGRectMake(20, 10, 100, 22);
        workLabel.text = @"WORK";
        workLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        workLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
        [workView addSubview:workLabel];
        
        work_num = [[UILabel alloc] init];
        work_num.frame = CGRectMake(20, 32, 200, 22);
        //home_num.text = @"+31 70 51";
        work_num.textColor = [UIColor blackColor];
        work_num.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
        [workView addSubview:work_num];
        
        work_num.text = work ; //change
        
        
        // home_num.text = [[[deviceContactsPhoneDetails objectForKey:@"home"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"] invertedSet]] componentsJoinedByString:@""];
        //home_num.text = [NSString stringWithFormat:@"%@",[deviceContactsPhoneDetails objectForKey:@"home"]];
        UIButton *workView_btn = [[UIButton alloc] init];
        if (mobileView) {
            workView_btn.tag = 3;
        }
        else
        {
            workView_btn.tag = 1;
        }
        
        workView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 62);
        [workView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [workView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
        [workView_btn addTarget:self action:@selector(phonefunc:) forControlEvents:UIControlEventTouchUpInside];
        [workView addSubview:workView_btn];
    }
    else
    {
        workView.frame = CGRectMake(0, homeView.frame.origin.y+homeView.frame.size.height, 0, 0);
    }
    
    
    
    //======================================================EMAIL=======================================================//
    emailView = [[UIView alloc] init];
    if (emaildata != false && ![emaildata isEqualToString:@"0"] && [emaildata length] > 1 && ![emaildata isEqualToString:@"(null)"] && ![emaildata isEqualToString:@""]) {
        
        
        emailView.frame = CGRectMake(0, workView.frame.origin.y+workView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 62);
        emailView.backgroundColor = [UIColor clearColor];
        [scrollView addSubview:emailView];
        
        UILabel *emailView_div = [[UILabel alloc] init];
        emailView_div.frame = CGRectMake(0, 61.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
        emailView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        [emailView addSubview:emailView_div];
        
        UILabel *emailLabel = [[UILabel alloc] init];
        emailLabel.frame = CGRectMake(20, 10, 100, 22);
        emailLabel.text = @"EMAIL";
        emailLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        emailLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
        [emailView addSubview:emailLabel];
        
        UILabel *emailId = [[UILabel alloc] init];
        emailId.frame = CGRectMake(20, 32, [UIScreen mainScreen].bounds.size.width, 22);
        //emailId.text = [NSString stringWithFormat:@"%@",[_Condetails_dict objectForKey:@"email"]];
        emailId.textColor = [UIColor blackColor];
        emailId.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
        [emailView addSubview:emailId];
        mail = emailId.text;
        
        UIButton *emailView_btn = [[UIButton alloc] init];
        emailView_btn.tag = 2;
        emailView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 62);
        [emailView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [emailView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
        [emailView_btn addTarget:self action:@selector(mailFun:) forControlEvents:UIControlEventTouchUpInside];
        [emailView addSubview:emailView_btn];
        
        emailId.text = emaildata;
        mail = emailId.text;
    }
    else
    {
        emailView.frame = CGRectMake(0, workView.frame.origin.y+workView.frame.size.height, 0, 0);
    }
    
    
    /////////////////////////////////////////////// WEBSITE /////////////////////////////////////////
    
    websiteView = [[UIView alloc] init];
    
    if (websitedata != false && ![websitedata isEqualToString:@"0"] && [websitedata length] > 1 && ![websitedata isEqualToString:@"(null)"] && ![websitedata isEqualToString:@""]) {
        
        websiteView.frame = CGRectMake(0, emailView.frame.origin.y+emailView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 62);
        websiteView.backgroundColor = [UIColor clearColor];
        [scrollView addSubview:websiteView];
        
        UILabel *websiteView_div = [[UILabel alloc] init];
        websiteView_div.frame = CGRectMake(0, 61.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
        websiteView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        [websiteView addSubview:websiteView_div];
        
        UILabel *websiteLabel = [[UILabel alloc] init];
        websiteLabel.frame = CGRectMake(20, 10, 100, 22);
        websiteLabel.text = @"WEBSITE";
        websiteLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        websiteLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
        [websiteView addSubview:websiteLabel];
        
        
        //[landphonelb addTarget:self action:@selector(landphoneFun:) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *website = [[UILabel alloc] init];
        website.frame = CGRectMake(20, 32, 180, 22);
        website.text = [NSString stringWithFormat:@"%@",websitedata];
        website.textColor = [UIColor blackColor];
        website.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
        [websiteView addSubview:website];
        
        UIButton *websiteView_btn = [[UIButton alloc] init];
        websiteView_btn.tag = 1;
        websiteView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 62);
        [websiteView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [websiteView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
        [websiteView_btn addTarget:self action:@selector(websiteFun:) forControlEvents:UIControlEventTouchUpInside];
        [websiteView addSubview:websiteView_btn];
        
    }
    else
    {
        websiteView.frame = CGRectMake(0, emailView.frame.origin.y+emailView.frame.size.height, 0,0);
    }
    
    
    //    [address sizeToFit];
    
    NSString *addressValue;
    addressView = [[UIView alloc] init];
    
    
    if (streetdata != false && ![streetdata isKindOfClass:[NSNull class]] && ![streetdata isEqualToString:@"(null)"] && [streetdata length]!=0 && ![streetdata isEqualToString:@""] && ![streetdata isEqualToString:@"0"])
        
    {
        addressValue = streetdata;
        
    }
    else
    {
        addressValue = @"";
        
    }
    if (housenumberdata != false && ![housenumberdata isEqualToString:@"(null)"] && [housenumberdata length]!=0 && ![housenumberdata isEqualToString:@""] && ![housenumberdata isEqualToString:@"0"])
    {
        addressValue = [addressValue stringByAppendingString:[NSString stringWithFormat:@"%@<br>",housenumberdata]];
    }
    else
    {
        addressValue = [addressValue stringByAppendingString:[NSString stringWithFormat:@"<br>"]];
    }
    
    if (zipdata != false && ![zipdata isEqualToString:@"(null)"] && [zipdata length]!=0 && ![zipdata isEqualToString:@""] && ![zipdata isEqualToString:@"0"])
    {
        
        nobusiness=1;
        
        addressValue = [addressValue stringByAppendingString:[NSString stringWithFormat:@"%@",zipdata]];
        
    }
    if (citydata != false && ![citydata isKindOfClass:[NSNull class]] && ![citydata isEqualToString:@"(null)"] && [citydata length]!=0 && ![citydata isEqualToString:@""] && ![citydata isEqualToString:@"0"])
    {
        addressValue = [addressValue stringByAppendingString:[NSString stringWithFormat:@" %@<br>",citydata]];
        
    }
    
    if (countrydata != false && ![countrydata isEqualToString:@"(null)"] && [countrydata length]!=0 && ![countrydata isEqualToString:@""] && ![countrydata isEqualToString:@"0"])
    {
        addressValue = [addressValue stringByAppendingString:[NSString stringWithFormat:@"%@",countrydata]];
        
    }
    
    if ([addressValue isEqualToString:@""] || [addressValue isEqualToString:@"<br>"]) {
        addressView.frame = CGRectMake(0, websiteView.frame.origin.y+websiteView.frame.size.height, 0, 0);
    }
    else
    {
        addressView.frame = CGRectMake(0, websiteView.frame.origin.y+websiteView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 106);
        addressView.backgroundColor = [UIColor clearColor];
        [scrollView addSubview:addressView];
        
        UILabel *addressView_div = [[UILabel alloc] init];
        addressView_div.frame = CGRectMake(0, 105.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
        addressView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        [addressView addSubview:addressView_div];
        
        UILabel *addressLabel = [[UILabel alloc] init];
        addressLabel.frame = CGRectMake(20, 10, 100, 22);
        addressLabel.text = @"ADDRESS";
        addressLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        addressLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
        [addressView addSubview:addressLabel];
        
        address = [[UITextView alloc] init];
        address.frame = CGRectMake(15, 32, [UIScreen mainScreen].bounds.size.width-15, 70);
        address.backgroundColor = [UIColor clearColor];
        [address setValue:addressValue forKey:@"contentToHTMLString"];
        address.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
        
        address.editable = NO;
        [addressView addSubview:address];
        
        UIButton *addressView_btn = [[UIButton alloc] init];
        addressView_btn.tag = 3;
        addressView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 106);
        [addressView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [addressView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
        [addressView_btn addTarget:self action:@selector(openmap:) forControlEvents:UIControlEventTouchUpInside];
        [addressView addSubview:addressView_btn];
        
    }
    
    
    //==============================================WORK=================================//
    
    
    if (![[deviceContactsPhoneDetails objectForKey:@"work"] isKindOfClass:[NSNull class]] && ![[deviceContactsPhoneDetails objectForKey:@"work"] isEqualToString:@"(null)"] && ![[deviceContactsPhoneDetails objectForKey:@"work"] isEqualToString:@""] && [[deviceContactsPhoneDetails objectForKey:@"work"] length]>0)
    {
    }
    else
    {
        
    }
    
    //===================================Mail===============================//
    
    if(emaildata != nil && ![emaildata isKindOfClass:[NSNull class]] && ![emaildata isEqualToString:@"(null)"] && ![emaildata isEqualToString:@""] && [emaildata length]>0)
    {
    }
    else
    {
    }
    
    //======================================Address==================================//
    DebugLog(@"address: %@",address_str);
    if (![address_str isKindOfClass:[NSNull class]]&& address_str != (id)[NSNull null]&& ![address_str isEqualToString:@"(null)"]&&![address_str isEqualToString:@""]&&[address_str length]!=0)
    {
        
        if ([streetdata length]>0 || [housenumberdata length]>0) {
            
            if ([zipdata length]>0 || [citydata length]>0) {
                
                if ([countrydata length]>0) {
                    
                    
                }
                else
                {
                    
                }
            }
            else
            {
                if ([countrydata length]>0) {
                    
                }
                else
                {
                }
                
            }
        }
        
        else
        {
            if ([zipdata length]>0 || [citydata length]>0) {
                
                if ([countrydata length]>0) {
                    
                }
                else
                {
                    
                }
            }
            else
            {
                if ([countrydata length]>0)
                {
                    
                }
                else
                {
                    
                }
                
            }
            
        }
        
    }
    else
    {
    }
    
    int id_contact = [idnew intValue];
    
    DebugLog(@"ID:%d",id_contact);
    image_prof = [self contactPicture:id_contact];
    
    //    UIImage *image = [UIImage imageWithData:imageData];
    
    CGImageRef cgref = [image_prof CGImage];
    CIImage *cim = [image_prof CIImage];
    
    if (cim == nil && cgref == NULL)
    {
        DebugLog(@"no underlying data %@",image_prof);
    }
    else
    {
        DebugLog(@"image has; %@",image_prof);
        
        @try {
            
            profile_image.image = image_prof;
            profile_image.layer.cornerRadius = profile_image.frame.size.width/2;
            profile_image.clipsToBounds = YES;
            image_prof_original = [self contactPictureOriginal:id_contact];
            
            profile_image.userInteractionEnabled=YES;
            
            UITapGestureRecognizer *propictap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(detailpic:)];
            
            [profile_image addGestureRecognizer:propictap];
        }
        @catch (NSException *exception) {
            // DebugLog(@"exception is: %@",exception.description);
        }
    }
    //[mainview addSubview:profile_image];
    
    phonesarr1 =[[NSMutableArray alloc]init];
    NSString *mobile_strd, *business_strd, *landphnstrd;
    // mobile_strd = [NSString stringWithFormat:@"%@",[deviceContactsPhoneDetails objectForKey:@"mobile"]];
    mobile_strd = mobile;
    DebugLog(@"deviceContactsPhoneDetails mobile %@",mobile_strd);
    business_strd = [NSString stringWithFormat:@"%@",[_ContactDetails_Dict objectForKey:@"work"]];
    //landphnstrd = [NSString stringWithFormat:@"%@",[deviceContactsPhoneDetails objectForKey:@"home"]];
    landphnstrd = home;
    DebugLog(@"deviceContactsPhoneDetails mobile %@",landphnstrd);
    
    if ([mobile_strd length] > 3 && ![mobile_strd isKindOfClass:[NSNull class]] && mobile_strd != (id)[NSNull null] && ![mobile_strd isEqualToString:@"(null)"])
    {
        [phonesarr1 addObject:mobile_strd];
    }
    if ([landphnstrd length] > 3 && ![landphnstrd isKindOfClass:[NSNull class]] && landphnstrd != (id)[NSNull null] && ![landphnstrd isEqualToString:@"(null)"])
    {
        [phonesarr1 addObject:landphnstrd];
    }
    else if ([business_strd length] > 3 && ![business_strd isKindOfClass:[NSNull class]] && business_strd != (id)[NSNull null] && ![business_strd isEqualToString:@"(null)"])
    {
        [phonesarr1 addObject:business_strd];
    }
    DebugLog(@"phonesarr1: %@",phonesarr1);
    //[activity stopAnimating];
    [act removeFromSuperview];
    
}

- (UIImage *) contactPicture:(int)contactId {
    // Get contact from Address Book
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
    ABRecordID recordId = (ABRecordID)contactId;
    ABRecordRef person = ABAddressBookGetPersonWithRecordID(addressBook, recordId);
    
    // Check for contact picture
    if (person != nil && ABPersonHasImageData(person)) {
        if ( &ABPersonCopyImageDataWithFormat != nil ) {
            return [UIImage imageWithData:(__bridge NSData *)ABPersonCopyImageDataWithFormat(person, kABPersonImageFormatThumbnail)];
        } else {
            return [UIImage imageWithData:(__bridge NSData *)ABPersonCopyImageData(person)];
        }
    } else {
        return nil;
    }
}

- (UIImage *) contactPictureOriginal:(int)contactId {
    // Get contact from Address Book
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
    ABRecordID recordId = (ABRecordID)contactId;
    ABRecordRef person = ABAddressBookGetPersonWithRecordID(addressBook, recordId);
    
    // Check for contact picture
    if (person != nil && ABPersonHasImageData(person)) {
        if ( &ABPersonCopyImageDataWithFormat != nil ) {
            // iOS >= 4.1
            return [UIImage imageWithData:(__bridge NSData *)ABPersonCopyImageDataWithFormat(person, kABPersonImageFormatOriginalSize)];
        } else {
            // iOS < 4.1
            return [UIImage imageWithData:(__bridge NSData *)ABPersonCopyImageData(person)];
        }
    } else {
        return nil;
    }
}

-(void)detailpic: (UIGestureRecognizer *)sender
{
    ConPictureProfViewController *con = [[ConPictureProfViewController alloc]init];
    con.profilepic = image_prof_original;
    //   con.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:con animated:NO completion:nil];
}


-(void)closedetailpic: (UIGestureRecognizer *)sender
{
    CATransition *animation = [CATransition animation];
    [animation setType:kCATransitionFade];
    [animation setSubtype:kCATransitionFade];
    animation.duration = 0.7f;
    // [detailimg.layer addAnimation:animation forKey:nil];
    //    if(detailimg.hidden==NO)
    //    {
    //        detailimg.hidden=YES;
    //    }
    //    [tapbgview removeFromSuperview];
}

-(void)viewWillAppear:(BOOL)animated{
    self.tabBarController.tabBar.frame = CGRectMake(0, [[UIScreen mainScreen]bounds].size.height, [UIScreen mainScreen].bounds.size.width,44);
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getData:) name:@"Data" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(navtopage:) name:@"pagenav" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestreceivedaction) name:@"Requestreceived_push" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(acceptedrequestaction) name:@"Accepted_push" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AcceptedRequest) name:@"Accepted_request" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AddFriend) name:@"NewConnection" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ConnectionType) name:@"NewConnectionType" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AcceptedRequest) name:@"TypeChange" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shareLocation) name:@"ShareLocation" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdateInformation) name:@"UpdateInfo" object:nil];
    
}

-(void)UpdateInformation
{
    ConPersonalProfileViewController *PVC = [[ConPersonalProfileViewController alloc]init];
    PVC.toUpdateInfo = @"YES";
    [self.navigationController pushViewController:PVC animated:YES];
   // [[NSNotificationCenter defaultCenter] postNotificationName:@"Update Info" object:Nil];
    
}


-(void)shareLocation
{
    ConLocateGroupViewController *conLocate = [[ConLocateGroupViewController alloc]init];
    conLocate.group_id=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"sharedLocGrpId"]];
    [self.navigationController pushViewController:conLocate animated:YES];
}

-(void)ConnectionType
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDel.newrequestRedirect)
    {
        
        ConNewRequestsViewController *mng = [[ConNewRequestsViewController alloc]init];
        mng.userid = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userforrequest"] intValue];
        //    CATransition* transition = [CATransition animation];
        //
        //    transition.duration = 0.4;
        //    transition.type = kCATransitionPush;
        //    transition.subtype = kCATransitionFade;
        
        appDel.newrequestRedirect = NO;
        
        //   [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:mng animated:YES];
        
    }
    
}

-(void)AddFriend
{
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDel.newUser)
    {
        
        ConAddFriendViewController *con = [[ConAddFriendViewController alloc]init];
        
        //    CATransition* transition = [CATransition animation];
        //
        //    transition.duration = 0.4;
        //    transition.type = kCATransitionPush;
        //    transition.subtype = kCATransitionFade;
        
        appDel.newUser = NO;
        
        //   [[self navigationController].view.layer addAnimation:transition forKey:nil];
        [[self navigationController] pushViewController:con animated:YES];
        
    }
    
}

-(void)AcceptedRequest
{
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (del.profileRedirect)
    {
        ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
        // con.other=@"yes";
        con.request = YES;
        [self.tabBarController.tabBar setHidden:YES];
        // con.uid=[NSString stringWithFormat:@"%d",[userid intValue]];
        //                [self.navigationController presentViewController:con animated:NO completion:nil];
        
        //    CATransition* transition = [CATransition animation];
        //
        //    transition.duration = 0.4;
        //    transition.type = kCATransitionPush;
        //    transition.subtype = kCATransitionFade;
        
        del.profileRedirect = NO;
        
        // [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
        
    }
    
}

-(void)requestreceivedaction
{
    //    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
    con.other=@"yes";
    con.uid=[NSString stringWithFormat:@"%@",del.pushtoprofileid];
    
    //    CATransition* transition = [CATransition animation];
    //
    //    transition.duration = 0.4;
    //    transition.type = kCATransitionPush;
    //    transition.subtype = kCATransitionFade;
    //
    //    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}

-(void)acceptedrequestaction
{
    ConRequestViewController *con = [[ConRequestViewController alloc]init];
    //    CATransition* transition = [CATransition animation];
    //
    //    transition.duration = 0.4;
    //    transition.type = kCATransitionPush;
    //    transition.subtype = kCATransitionFade;
    //
    //    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}

-(void)navtopage: (NSNotification *)notification
{
    //DebugLog(@"navtopage");
    prefs =[NSUserDefaults standardUserDefaults];
    if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"4"])
    {
        ConAccountSettingsViewController *con = [[ConAccountSettingsViewController alloc]init];
        //        CATransition* transition = [CATransition animation];
        //
        //        transition.duration = 0.4;
        //        transition.type = kCATransitionPush;
        //        transition.subtype = kCATransitionFade;
        //
        //        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"6"])
    {
        ConInviteViewController *con = [[ConInviteViewController alloc]init];
        
        //        CATransition* transition = [CATransition animation];
        //
        //        transition.duration = 0.4;
        //        transition.type = kCATransitionPush;
        //        transition.subtype = kCATransitionFade;
        //
        //        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"5"])
    {
        ConSyncLoaderViewController *con = [[ConSyncLoaderViewController alloc]init];
        //        [self.navigationController pushViewController:con animated:NO];
        
        
        [self presentViewController:con animated:NO completion:nil];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"2"])
    {
        ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
        
        //        CATransition* transition = [CATransition animation];
        //
        //        transition.duration = 0.4;
        //        transition.type = kCATransitionPush;
        //        transition.subtype = kCATransitionFade;
        //
        //        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"3"])
    {
        ConQROptionsViewController *con = [[ConQROptionsViewController alloc]init];
        
        //        CATransition* transition = [CATransition animation];
        //
        //        transition.duration = 0.4;
        //        transition.type = kCATransitionPush;
        //        transition.subtype = kCATransitionFade;
        //
        //        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    
    [mainview setFrame:CGRectMake(0, mainview.frame.origin.y, mainview.frame.size.width, mainview.frame.size.height)];
    //    [prefs setObject:@"111" forKey:@"whichpage"];
}


-(void)getData:(NSNotification *)notification {
    
    if(move == 0) {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake(280, (mainview.frame.origin.y), mainview.frame.size.width, mainview.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             move=1;
                         }];
    } else {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake(0, (mainview.frame.origin.y), mainview.frame.size.width, mainview.frame.size.height)];
                             
                         }
                         completion:^(BOOL finished){
                             move=0;
                         }];
    }
}

-(void)phonefunc:(UIButton *)sender
{
    if (sender.tag == 1) {
        mobileView.backgroundColor = [UIColor lightGrayColor];
    }
    if (sender.tag == 2) {
        homeView.backgroundColor = [UIColor lightGrayColor];
    }
    //call_btn.alpha=0.0f;
    call_img.alpha=1.0f;
    call_btn.userInteractionEnabled = YES;
    DebugLog(@"I Want to Make a Call");
    if ([phonesarr1 count] == 0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"No Number found for this user!"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        mobileView.backgroundColor = [UIColor clearColor];
        homeView.backgroundColor = [UIColor clearColor];
    }
    else
    {
        UIDevice *device = [UIDevice currentDevice];
        if ([[device model] isEqualToString:@"iPhone"] ) {
            NSString *phoneNumber = [[[phonesarr1 objectAtIndex:sender.tag-1] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"] invertedSet]] componentsJoinedByString:@""];
            phoneNumber = [@"tel://" stringByAppendingString:phoneNumber];
            DebugLog(@"PHONE NUMBER: %@",phoneNumber);
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
            mobileView.backgroundColor = [UIColor clearColor];
            homeView.backgroundColor = [UIColor clearColor];
        } else {
            UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [notPermitted show];
            mobileView.backgroundColor = [UIColor clearColor];
            homeView.backgroundColor = [UIColor clearColor];
        }
    }
}

-(void)smsfunc
{
    //sms_btn.alpha=0.0f;
    sms_img.alpha=1.0f;
    sms_btn.userInteractionEnabled = YES;
    DebugLog(@"I Want to Send a SMS");
    UIAlertView *smsAlert1;
    if ([phonesarr1 count] == 0)
    {
        smsAlert1 = [[UIAlertView alloc] initWithTitle:@"No Number found for this user!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        smsAlert1.tag = 6;
        [smsAlert1 show];
    }
    else
    {
        UIDevice *device = [UIDevice currentDevice];
        if ([[device model] isEqualToString:@"iPhone"]) {
            
            MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
            if([MFMessageComposeViewController canSendText])
            {
                [self presentViewController:controller animated:YES completion:Nil];
                if (invite_Me) {
                    controller.body = @"Discover Budnav - The Smart Phone Book. Download now https://budnav.com/dl/";
                }
                else
                {
                    controller.body = @"";
                }
                NSString *smsstring= [NSString stringWithFormat:@"%@",[phonesarr1 objectAtIndex:0]];
                controller.recipients = [NSArray arrayWithObjects:smsstring, nil];
                controller.messageComposeDelegate = self;
                //[self presentModalViewController:controller animated:YES];
            }
        }
        else
        {
            smsAlert1=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            smsAlert1.tag = 66;
            [smsAlert1 show];
        }
    }
    
}

-(void)whatsapp_func
{
    NSString * urlWhats;
    DebugLog(@"Whatsapp ID: %@",idnew);
    if (invite_Me) {
        urlWhats = [NSString stringWithFormat:@"whatsapp://send?abid=%@&text=Discover Budnav - The Smart Phone Book. Download now https://budnav.com/dl/",idnew];
    }
    else
    {
        urlWhats = [NSString stringWithFormat:@"whatsapp://send?abid=%@&text=",idnew];
    }
    NSURL * whatsappURL = [NSURL URLWithString:[urlWhats stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
        [[UIApplication sharedApplication] openURL: whatsappURL];
    } else {
        alert = [[UIAlertView alloc] initWithTitle:@"WhatsApp not installed." message:@"Your device has no WhatsApp installed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

-(void)phone_color
{
    call_img.alpha = 0.5f;
    // call_btn.alpha = 0.5f;
    //[call_btn setBackgroundColor:[UIColor grayColor]];
}

-(void)phone_colorAgain
{
    call_img.alpha = 1.0f;
    //call_btn.alpha = 1.0f;
    //[call_btn setBackgroundColor:[UIColor clearColor]];
}
-(void)sms_color
{
    sms_img.alpha = 0.5f;
    // sms_btn.alpha = 0.5f;
    // [sms_btn setBackgroundColor:[UIColor grayColor]];
}
-(void)sms_colorAgain
{
    sms_img.alpha = 1.0f;
    //sms_btn.alpha = 1.0f;
    //[sms_btn setBackgroundColor:[UIColor clearColor]];
}

-(void)changecoloragain
{
    backlbl.alpha = 1.0f;
    labelHead.alpha = 1.0f;
    bck_img.alpha = 1.0f;
}
-(void)changecolor
{
    backlbl.alpha = 0.5f;
    labelHead.alpha = 1.0f;
    bck_img.alpha = 0.5f;
    
}

-(void)changecoloragain1
{
    DebugLog(@"CHANGE COLOR");
    labelHead.alpha = 1.0f;
    bck_img.alpha = 1.0f;
}

-(void)changecolor1
{
    DebugLog(@"CHANGE COLOR1");
    labelHead.alpha = 0.5f;
    bck_img.alpha = 0.5f;
    
}

-(void)gobackoption
{
    labelHead.alpha = 0.5f;
    bck_img.alpha = 0.5f;
    
    
    //    CATransition *transition = [CATransition animation];
    //
    //    transition.duration = 0.4f;
    //
    //    transition.type = kCATransitionFade;
    //
    //    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    [self.navigationController popViewControllerAnimated:YES];
    //[self.navigationController popToViewController:[navarr objectAtIndex:index] animated:YES];
    
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    if (result == MessageComposeResultCancelled) {
        // DebugLog(@"Message cancelled");
        
    } else if (result == MessageComposeResultSent) {
        // DebugLog(@"Message sent");
    }
}

-(void)inviteMe
{
    DebugLog(@"INVITE ME");
//    UIActionSheet *mobileAction = [[UIActionSheet alloc]initWithTitle:Nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles:@"Whatsapp",@"Message", nil];
    UIActionSheet *mobileAction = [[UIActionSheet alloc]initWithTitle:Nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles:@"Message", nil];

    mobileAction.tag = 101;
    [mobileAction showInView:self.view];
    
    invite_Me = TRUE;
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //    [actionSheet removeFromSuperview];
    if (actionSheet.tag == 101){
        
//        if (buttonIndex == 0) {
//            [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
//            [self whatsapp_func];
//        }
        if (buttonIndex == 0)
        {
            [actionSheet dismissWithClickedButtonIndex:1 animated:YES];
            DebugLog(@"MESSAGE");
            [self smsfunc];
        }
        else{
            
            DebugLog(@"dismissWithClickedButtonIndex");
            [actionSheet dismissWithClickedButtonIndex:5 animated:NO];
        }
        
    }
    
}

-(void)openmap: (UIButton *)sender
{
    
    if (switchprof == 0)
    {
        latitude=[[dict_profile objectForKey:@"lat"] doubleValue];
        longitude=[[dict_profile objectForKey:@"lng"] doubleValue];
    }
    else
    {
        latitude=[[dict_profile objectForKey:@"b_lat"] doubleValue];
        longitude=[[dict_profile objectForKey:@"b_lng"] doubleValue];
    }
    DebugLog(@"lati=== %f",latitude);
    DebugLog(@"long=== %f",longitude);
    [self generateRoute];
}

- (void)generateRoute {
    CLLocationCoordinate2D end = {latitude, longitude};
    
    MKMapItem *destination_mapitem = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:end addressDictionary:nil]];
    
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
    
    request.source = [MKMapItem mapItemForCurrentLocation];
    
    request.destination = destination_mapitem;
    
    [request setTransportType:MKDirectionsTransportTypeAny]; // This can be limited to automobile and walking directions.
    
    [request setRequestsAlternateRoutes:YES];
    
    MKDirections *directions = [[MKDirections alloc] initWithRequest:request];
    
    [directions calculateDirectionsWithCompletionHandler:
     
     ^(MKDirectionsResponse *response, NSError *error) {
         
         if (error) {
             DebugLog(@"error generate route----  %@",error);
             
         } else {
             [self showRoute:response];
         }
     }];
    [self navigateinapple];
}

-(void)navigateinapple
{
    CLLocationCoordinate2D endingCoord = CLLocationCoordinate2DMake(latitude, longitude);
    MKPlacemark *endLocation = [[MKPlacemark alloc] initWithCoordinate:endingCoord addressDictionary:nil];
    MKMapItem *endingItem = [[MKMapItem alloc] initWithPlacemark:endLocation];
    NSMutableDictionary *launchOptions = [[NSMutableDictionary alloc] init];
    [launchOptions setObject:MKLaunchOptionsDirectionsModeDriving forKey:MKLaunchOptionsDirectionsModeKey];
    [endingItem openInMapsWithLaunchOptions:launchOptions];
}

-(void)showRoute:(MKDirectionsResponse *)response
{
    for (MKRoute *route in response.routes)
    {
        [map_View addOverlay:route.polyline level:MKOverlayLevelAboveLabels];
        
        for (MKRouteStep *step in route.steps)
        {
            DebugLog(@" here %@", step.instructions);
        }
    }
}


- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id < MKOverlay >)overlay
{
    MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
    renderer.strokeColor = [UIColor blueColor];
    renderer.lineWidth = 4.0;
    return renderer;
}


- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    CLLocation *location = map_View.userLocation.location;
    DebugLog(@"lat current: %f - long current: %f", location.coordinate.latitude, location.coordinate.longitude);
}


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    // Handle any custom annotations.
    
    if ([annotation isKindOfClass:[MKPointAnnotation class]])
    {
        // Try to dequeue an existing pin view first.
        
        MKPinAnnotationView *pinView = (MKPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomPinAnnotationView"];
        //        if (!pinView)
        //        {
        pinView = [[MKPinAnnotationView alloc] init];
        //            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomPinAnnotationView"];
        
        pinView.animatesDrop = YES;
        pinView.canShowCallout = YES;
        pinView.image = [UIImage imageNamed:@"locator1.png"];
        pinView.calloutOffset = CGPointMake(0, 0);
        
        
        UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        
        pinView.rightCalloutAccessoryView = rightButton;
        
        
        UIImageView *iconView = [[UIImageView alloc] init];
        
        iconView.frame = CGRectMake(0.0f, 0.0f,20, 20);
        
        pinView.leftCalloutAccessoryView = iconView;
        
        iconView.backgroundColor=[UIColor whiteColor];
        
        
        if (annotation == mapView.userLocation)
            
        {
            pinView.image= [UIImage imageNamed:@"locatorown.png"];
            
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomPinAnnotationView"];
        }
        
        DebugLog(@"(int)annotation.subtitle===%@",annotation.subtitle);
        
        iconView.image= profilepic;
        
        if (switchprof == 0)
            pinView.image = [UIImage imageNamed:@"locator1.png"];
        else
            pinView.image = [UIImage imageNamed:@"locator2.png"];
        
        //        }
        return pinView;
    }
    return nil;
}





-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    DebugLog(@"annotation selected");
}

- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)aUserLocation {
    
    MKCoordinateRegion region;
    
    MKCoordinateSpan span;
    
    span.latitudeDelta = 0.005;
    
    span.longitudeDelta = 0.005;
    
    CLLocationCoordinate2D map_location;
    
    map_location.latitude =  latitude;       //aUserLocation.coordinate.latitude;
    
    map_location.longitude = longitude;      //aUserLocation.coordinate.longitude;
    
    region.span = span;
    
    region.center = map_location;
    
    googleMapUrlString = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%f,%f&daddr=%f,%f", aUserLocation.location.coordinate.latitude,aUserLocation.location.coordinate.longitude, map_location.latitude, map_location.longitude];
    
    DebugLog(@"url fired map: %@",googleMapUrlString);
    
    //    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapUrlString]];
    appleMapUrlString = [NSString stringWithFormat:@"http://maps.apple.com/maps?saddr=%f,%f&daddr=%f,%f", aUserLocation.location.coordinate.latitude,aUserLocation.location.coordinate.longitude, map_location.latitude, map_location.longitude];
    [self generateRoute];
}

-(void)mailFun:(UIButton *)sender {
    
    if ([mail length]>0) {
        
        //mailback.backgroundColor = [UIColor colorWithRed:(238.0f/255.0f) green:(238.0f/255.0f) blue:(238.0f/255.0f) alpha:1.0f];
        
        mailComposer = [[MFMailComposeViewController alloc]init];
        mailComposer.mailComposeDelegate = self;
        NSArray *recipentsArray = [NSArray arrayWithObject:mail];
        [mailComposer setToRecipients:recipentsArray];
        [mailComposer setMessageBody:@"" isHTML:NO];
        [self.navigationController presentViewController:mailComposer animated:YES completion:nil];
    }
    else{
        alert = [[UIAlertView alloc] initWithTitle:@"No Email Id found for this user!"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        emailView.backgroundColor = [UIColor clearColor];
        emailView.backgroundColor = [UIColor clearColor];
        
    }
}

#pragma mark - mail compose delegate

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (result) {
        DebugLog(@"Result : %d",result);
    }
    if (error) {
        DebugLog(@"Error : %@",error);
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
    //mailback.backgroundColor = [UIColor clearColor];
}


-(UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

-(void)websiteFun: (id)sender
{
    DebugLog(@"dcdc");
    SocialWebViewController *social= [[SocialWebViewController alloc]init];
    if (switchprof == 0)
        //        social.type_social=[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"website"]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",websitedata]]];
    else
        //        social.type_social=[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_website"]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",websitedata]]];
    
    //    CATransition* transition = [CATransition animation];
    //
    //    transition.duration = 0.4;
    //    transition.type = kCATransitionPush;
    //    transition.subtype = kCATransitionFade;
    //
    //    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:social animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

