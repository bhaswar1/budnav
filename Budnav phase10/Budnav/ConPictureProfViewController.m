//
//  ConPictureProfViewController.m
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 09/07/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//
#import "ConPictureProfViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

@interface ConPictureProfViewController ()
{
    UIView *tapbgview;
    UIImageView *detailimg;
    NSTimer *timerpic;
}
@end

@implementation ConPictureProfViewController
@synthesize profilepic;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    tapbgview = [[UIView alloc]initWithFrame:self.view.frame];
    
    [self.view addSubview:tapbgview];
    
    tapbgview.backgroundColor=[UIColor blackColor];
    
    tapbgview.alpha=0.95f;
    
    UITapGestureRecognizer *tapback = [[UITapGestureRecognizer alloc]
                                       
                                       initWithTarget:self
                                       
                                       action:@selector(closedetailpic:)];
    
    tapbgview.userInteractionEnabled=YES;
    
    [tapbgview addGestureRecognizer:tapback];
    
    
    
    detailimg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, self.view.bounds.size.height)];
    
    
    
    [self.view addSubview:detailimg];
    
    detailimg.hidden=YES;
    
    detailimg.image = profilepic;
    
    detailimg.contentMode=UIViewContentModeScaleAspectFit;
    
    detailimg.userInteractionEnabled=YES;
    
    
    
    UITapGestureRecognizer *tapimg1 = [[UITapGestureRecognizer alloc]
                                       
                                       initWithTarget:self
                                       
                                       action:@selector(closedetailpic:)];
    
    detailimg.userInteractionEnabled=YES;
    
    [detailimg addGestureRecognizer:tapimg1];
    
    
    CATransition *animation = [CATransition animation];
    
    [animation setType:kCATransitionReveal];
    
    [animation setSubtype:kCATransitionReveal];
    
    animation.duration = 1.5f;
    
    [detailimg.layer addAnimation:animation forKey:nil];
    
    if(detailimg.hidden==YES)
    {
        detailimg.hidden=NO;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)closedetailpic: (UIGestureRecognizer *)sender
{
    
    CATransition *animation = [CATransition animation];
    
    [animation setType:kCATransitionFade];
    
    [animation setSubtype:kCATransitionFade];
    
    animation.duration = 0.25f;
    
    [detailimg.layer addAnimation:animation forKey:nil];
    
    if(detailimg.hidden==NO)
    {
        detailimg.hidden=YES;
        //        [self dismissViewControllerAnimated:NO completion:nil];
        timerpic = [NSTimer scheduledTimerWithTimeInterval:0.3f
                                                    target:self
                                                  selector:@selector(targetMethoda)
                                                  userInfo:nil
                                                   repeats:NO];
    }
}

-(void)targetMethoda
{
    [self dismissViewControllerAnimated:NO completion:nil];
}
@end