//
//  GroupDetailsViewController.h
//  Contacter
//
//  Created by intel on 31/12/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConGroupsViewController.h"
#import "GAITrackedViewController.h"

@interface GroupDetailsViewController : GAITrackedViewController<UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIAlertViewDelegate,UIActionSheetDelegate,UIGestureRecognizerDelegate>
{
    ConGroupsViewController *groupview;
}

@property(nonatomic)NSString *groupid,*groupname,*st,*search_name,*isadmin,*finalsearch_result,*check_search,*check_member,*member_count;
@property(nonatomic,strong)UIButton *menu,*invite;
@property(nonatomic,assign)BOOL filter;
@property(nonatomic,retain) NSMutableArray *filtered_arr,*search_result,*group_array,*searchresults;
@property(strong,nonatomic)NSMutableDictionary *copydic;
@property(nonatomic,strong)UITableView *groupstable,*menutable,*searchtablefront,*searchtableback;
@property(nonatomic,strong)UILabel *group_namelbl;
@property(nonatomic,strong)UIButton *group_namebtn;
@property(nonatomic,strong)UIImage *grouppic;
@property(nonatomic,strong)UIImageView *groupimage;
@property(nonatomic,strong)UISearchBar *searchBar;
@property(nonatomic,assign) int p;
@property(nonatomic,readwrite) UISearchBar *searchBar1;
@property(nonatomic,assign) BOOL accept_ckeck;
//@property(nonatomic,strong) UIView *popupview;
@property(strong,nonatomic) NSData *mydata;


@end
