//
//  ConAccountSettingsViewController.h
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 28/05/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConParentTopBarViewController.h"
#import "GAITrackedViewController.h"

@interface ConAccountSettingsViewController : GAITrackedViewController<UIAlertViewDelegate, UITableViewDelegate,UITableViewDataSource,NSURLConnectionDelegate>
{
    UIAlertView *alert;
    NSArray *cellobjects;
    UITableView *itemsTable;
}
@property(nonatomic, assign) float length;
@property(nonatomic, assign) BOOL push_check;
@end

//                CFErrorRef error = NULL;
//                ABAddressBookRef UsersAddressBook = ABAddressBookCreateWithOptions(NULL,NULL);
//
//                ABRecordRef newPerson = ABPersonCreate();
//
//                ABRecordSetValue(newPerson, kABPersonFirstNameProperty, (__bridge CFTypeRef)([finalwritedict objectForKey:@"name"]), &error);
//                ABRecordSetValue(newPerson, kABPersonLastNameProperty, @"Bell", &error);
//
//                ABMutableMultiValueRef multiPhone =     ABMultiValueCreateMutable(kABMultiStringPropertyType);
////                ABMultiValueAddValueAndLabel(multiPhone,(__bridge CFTypeRef)([phonesfinal objectAtIndex:0]), kABHomeLabel, NULL);
////                ABMultiValueAddValueAndLabel(multiPhone, @"7777466666", kABOtherLabel, NULL);
//
//        ABMultiValueAddValueAndLabel(multiPhone, (__bridge CFTypeRef)([finalwritedict objectForKey:@"city"]), kABPersonAddressCityKey, NULL);
//        ABMultiValueAddValueAndLabel(multiPhone, (__bridge CFTypeRef)([finalwritedict objectForKey:@"street"]), kABPersonAddressStreetKey, NULL);
//        ABMultiValueAddValueAndLabel(multiPhone, (__bridge CFTypeRef)([finalwritedict objectForKey:@"zipcode"]), kABPersonAddressZIPKey, NULL);
//        ABMultiValueAddValueAndLabel(multiPhone, (__bridge CFTypeRef)([finalwritedict objectForKey:@"country"]), kABPersonAddressCountryKey, NULL);
//        ABMultiValueAddValueAndLabel(multiPhone, (__bridge CFTypeRef)([finalwritedict objectForKey:@"mobile_num"]), kABPersonPhoneMobileLabel, NULL);
//
//
//                ABRecordSetValue(newPerson, kABPersonPhoneProperty, multiPhone,nil);
//                //CFRelease(multiPhone);
//                // ...
//                // Set other properties
//                // ...
//                ABAddressBookAddRecord(UsersAddressBook, newPerson, &error);
//
//                ABAddressBookSave(UsersAddressBook, &error);
//                //CFRelease(newPerson);
//                //CFRelease(UsersAddressBook);
//                if (error != NULL)
//                {
//                    CFStringRef errorDesc = CFErrorCopyDescription(error);
//                    //CFRelease(errorDesc);
//                }

//                ABMutableMultiValueRef phoneNumberMultiValue =  ABMultiValueCreateMutable(kABPersonPhoneProperty);
//                //Here is the problem this release dictionaryDetail after come back another time
//                NSString *phoneNumber = [NSString stringWithFormat:@"%@",[phonesfinal objectAtIndex:0]];
//                ABMultiValueAddValueAndLabel(phoneNumberMultiValue,(__bridge CFTypeRef)(phoneNumber) ,  kABHomeLabel, NULL);
//                ABRecordCopyValue(newPerson, kABPersonPhoneProperty);
//                ABRecordSetValue(newPerson, kABPersonPhoneProperty, phoneNumberMultiValue, nil);
////CFRelease(phoneNumberMultiValue);

// ABRecordSetValue(person, kABPersonPhoneProperty, @"2444", nil);



// Start of Address
//                ABMutableMultiValueRef address1 = ABMultiValueCreateMutable(kABMultiDictionaryPropertyType);
//                NSMutableDictionary *addressDict = [[NSMutableDictionary alloc] init];
//                [addressDict setObject:[finalwritedict objectForKey:@"street"] forKey:(NSString *)kABPersonAddressStreetKey];
//                [addressDict setObject:[finalwritedict objectForKey:@"zipcode"] forKey:(NSString *)kABPersonAddressZIPKey];
//                [addressDict setObject:[finalwritedict objectForKey:@"city"] forKey:(NSString *)kABPersonAddressCityKey];
//                [addressDict setObject:[finalwritedict objectForKey:@"country"] forKey:(NSString *)kABPersonAddressCountryKey];
//
//                ABMultiValueAddValueAndLabel(address1, (__bridge_retained CFTypeRef)(addressDict), nil, NULL);
//                ABRecordSetValue(newPerson, kABPersonAddressProperty, address1, nil);
////CFRelease(address);

//start of email
//                ABMutableMultiValueRef emailMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
//                ABMultiValueAddValueAndLabel(emailMultiValue, (__bridge CFTypeRef)[finalwritedict objectForKey:@"email"], (CFStringRef)@"Global", NULL);
//                ABMultiValueAddValueAndLabel(emailMultiValue, nil, (CFStringRef)@"Work", NULL);
//                ABRecordSetValue(newPerson, kABPersonURLProperty, emailMultiValue, nil);

//                ABAddressBookAddRecord(UsersAddressBook, newPerson, &error);
//                // save/commit entry
//                ABAddressBookSave(UsersAddressBook, &error);
//
//                if (error != NULL) {
//                }
