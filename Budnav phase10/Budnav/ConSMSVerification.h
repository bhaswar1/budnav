//
//  ConSMSVerification.h
//  Budnav
//
//  Created by Sohini's on 23/03/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"
@interface ConSMSVerification : GAITrackedViewController<UITextFieldDelegate>
{
    UIView *coverview,*mainview,*divider1,*divider2;
    UIImageView *back,*next;
    UILabel *bck,*nxt,*number,*label1;
    UIButton *backbutton,*nextbutton;
    UITextField *code;
}

@property (nonatomic, strong) NSString *name, *surname, *mobile_num, *mobile_pre, *email, *password, *defaultCode;

@end
