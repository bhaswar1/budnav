//  ConMapViewController.m
//  Created by Bhaswar's MacBook Air on 19/05/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
#import "ConInviteViewController.h"
#import "ConAddFriendViewController.h"
#import "REVClusterAnnotationView.h"
#import "ConMapViewController.h"
#import "AppDelegate.h"
#import "ConProfileOwnViewController.h"
#import "ConAccountSettingsViewController.h"
#import "REVClusterMap.h"
#import "iToast.h"
#import "SVProgressHUD.h"
#import "DBManager.h"
#import "ConSyncLoaderViewController.h"
#import "CalloutView.h"
#import "myAnnotation.h"
#import "WildcardGestureRecognizer.h"
#import "ConRequestViewController.h"
#import "ConQROptionsViewController.h"
#import "ConNewProfileViewController.h"
#import "ConNewRequestsViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"
#import "ConLocateGroupViewController.h"
#import "ConFullSettingsViewController.h"
#import "ConPersonalProfileViewController.h"

//#import <MapKit/MKAnnotationContainerView.h>
//#import "TouchView.h"
@interface ConMapViewController ()<MKMapViewDelegate,CLLocationManagerDelegate>
{
    MKMapView *map_View;
    int userid,move,map_insertion, map_fetch;
    NSMutableArray *loadagainarr;
    UIActivityIndicatorView *act;
    CalloutView *calloutView;
    UIView *calloutView1;
    UIImageView *logo_img;
    CLLocationCoordinate2D coord;
    NSString *mapname;
    //    TouchView* touchView;
    NSArray *retval;
}
@end

@implementation ConMapViewController
@synthesize getmapvalue,mainview;
//static int count=0;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"From ConMapViewController");
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    self.navigationController.navigationBarHidden=YES;
    
    retval = [[NSArray alloc]init];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    move=0;
    map_insertion=0;
    map_fetch=0;
    calloutView = (CalloutView *)[[[NSBundle mainBundle] loadNibNamed:@"calloutView" owner:self options:nil] objectAtIndex:0];
    calloutView.userInteractionEnabled=YES;
    for (UIView *subview in calloutView.subviews ){
        subview.userInteractionEnabled=YES;
    }
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(maptap1:)];
    tap.delegate=self;
    [calloutView addGestureRecognizer:tap];
    
    
    
    loadagainarr = [[NSMutableArray alloc]init];
    
    //    newarr1 = [[NSMutableArray alloc]init];
    
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    [del showTabValues:YES];

    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    self.navigationController.navigationBarHidden=YES;
    
    UIView *topview=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64)];
    [self.view addSubview:topview];
    //topview.backgroundColor=[UIColor blackColor];
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
    
    // Add the gradient to the view
    [topview.layer insertSublayer:gradient atIndex:0];
    
    
    logo_img = [[UIImageView alloc]init];
    logo_img.frame = CGRectMake(27+([UIScreen mainScreen].bounds.size.width-160)/2, 32, 101, 20);
    logo_img.backgroundColor = [UIColor clearColor];
    
    logo_img.image = [UIImage imageNamed:@"new_logo"];
    [topview addSubview:logo_img];
    
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
       
    //    [SVProgressHUD showWithStatus:@"Loading"];
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"profiletaverse"])
    {
        mainview = [[UIView alloc]initWithFrame:CGRectMake(0, 64, [UIScreen mainScreen].bounds.size.width, self.view.frame.size.height)];
        [self.view addSubview:mainview];
        mainview.backgroundColor=[UIColor whiteColor];
        
        act = [[UIActivityIndicatorView alloc] init];
        act.center = self.view.center;
        [act startAnimating];
        act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        [act setColor:[UIColor blackColor]];
        [mainview addSubview:act];
        
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        
        //    NSError *error =nil;
        CLLocation *location = map_View.userLocation.location;
        DebugLog(@"get dir lat current: %f - long current: %f", location.coordinate.latitude, location.coordinate.longitude);
        _mapView =[[REVClusterMapView alloc] initWithFrame:CGRectMake(0, 0,[[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height)];
        //    [map_View setDelegate:self];
        //    map_View.showsUserLocation=YES;
        //    [mainview addSubview:map_View];
        
        _mapView.delegate = self;
        
        [mainview addSubview:_mapView];
        
        CLLocationCoordinate2D coordinate;
        coordinate.latitude = 51.22;
        coordinate.longitude = 4.39625;
        //    _mapView.region = MKCoordinateRegionMakeWithDistance(coordinate, 5000, 5000);
        
        _mapView.showsUserLocation=YES;
        
        CLLocationManager *locManager = [[CLLocationManager alloc] init];
        [locManager setDelegate:self];
        [locManager setDesiredAccuracy:kCLLocationAccuracyBest];
        //    [locManager startUpdatingLocation];
        
        if ([[prefs objectForKey:@"firstMap"] isEqualToString:@"yes"])
        {
            DebugLog(@"hmmmmmm 1st map");
            newarr1 = [[NSMutableArray alloc]init];
           // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=map&id=%@&access_token=%@&device_id=%@",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
                
                DebugLog(@"profile url: %@",urlString1);
                
                NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
                
                NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
                
                if (signeddataURL1 == nil)
                {
                    alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                                       message:nil
                                                      delegate:self
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                    //        [alert show];
                    //        [SVProgressHUD dismiss];
                    [act removeFromSuperview];
                }
                else
                {
                    NSError *error =nil;
                    json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                             
                                                            options:kNilOptions
                             
                                                              error:&error];
                    DebugLog(@"json returns: %@",json1);
                    
                    NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
                    DebugLog(@"err  %@",errornumber);
                    if (![errornumber isEqualToString:@"0"])
                    {
                        DebugLog(@"if if");
                        NSString *err_str = [json1 objectForKey:@"error"];
                        alert = [[UIAlertView alloc] initWithTitle:@"Error in Retrieving Data!"
                                                           message:err_str
                                                          delegate:self
                                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                        //        [alert show];
                        //        [SVProgressHUD dismiss];
                        [act removeFromSuperview];
                    }
                    
                    if ([[json1 objectForKey:@"details"] isKindOfClass:[NSNull class]] || [json1 objectForKey:@"details"] == (id)[NSNull null] ||[[json1 objectForKey:@"details"] count] ==0)
                    {
                        //DebugLog(@"if called req");
                        alert = [[UIAlertView alloc] initWithTitle:@"The map will show addresses once you connect with people!"
                                                           message:nil
                                                          delegate:self
                                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                        [alert show];
                        //        [SVProgressHUD dismiss];
                        [act removeFromSuperview];
                        [prefs setObject:@"yes" forKey:@"firstMap"];
                        
                    }
                    else
                    {
                        NSDictionary *newdict= [json1 objectForKey:@"details"];
                        for (NSDictionary *reqdict in [newdict objectForKey:@"markers"])
                        {
                            [newarr1 addObject:reqdict];
                        }
                        DebugLog(@"map list array has: %@",newarr1);
                        
                        
                        int i=0;
                        NSMutableArray *pins = [NSMutableArray array];
                        while (i<[newarr1 count])
                        {
                            double latitude=[[[newarr1 objectAtIndex:i]objectForKey:@"lat"] doubleValue];
                            double longitude=[[[newarr1 objectAtIndex:i]objectForKey:@"lng"] doubleValue];
                            
                            DebugLog(@"lati=== %f",latitude);
                            DebugLog(@"long=== %f",longitude);
                            
                            CLLocationCoordinate2D newCoord = {latitude, longitude};
                            REVClusterPin *pin = [[REVClusterPin alloc] init];
                            pin.title = [NSString stringWithFormat:@"%@ %@",[[newarr1 objectAtIndex:i]objectForKey:@"name"],[[newarr1 objectAtIndex:i]objectForKey:@"surname"]];
                            pin.subtitle = nil;
                            mapname=pin.title;
                            pin.coordinate = newCoord;
                            [pins addObject:pin];
                            
                            i++;
                        }
                        [_mapView addAnnotations:pins];
                        //            [SVProgressHUD dismiss];
                        [act removeFromSuperview];
                        
                        [self performSelectorOnMainThread:@selector(startDBInsert)
                                               withObject:nil
                                            waitUntilDone:YES];
                        
                        [prefs setObject:@"no" forKey:@"firstMap"];
                        
                    }
                    
                   // DebugLog(@"newarr ache: %@",newarr1);
                    
                    
                }
            
        }
        
        else
        {
            DebugLog(@"1st time er por theke");
            retval= [[NSArray alloc]init];
            
            retval = [[NSUserDefaults standardUserDefaults]objectForKey:@"mapdata"];
            DebugLog(@"ciiijj : %@", retval);
            
            
            newarr1 = [[NSMutableArray alloc]init];
            newarr1 = [retval mutableCopy];
            CLLocationCoordinate2D coordinate;
            coordinate.latitude = 51.22;
            coordinate.longitude = 4.39625;
            _mapView.showsUserLocation=YES;
            
            int i=0;
            
            NSMutableArray *pins = [NSMutableArray array];
            while (i<[newarr1 count])
            {
                double latitude=[[[newarr1 objectAtIndex:i]objectForKey:@"lat"] doubleValue];
                double longitude=[[[newarr1 objectAtIndex:i]objectForKey:@"lng"] doubleValue];
                
                CLLocationCoordinate2D newCoord = {latitude, longitude};
                REVClusterPin *pin = [[REVClusterPin alloc] init];
                pin.title = [NSString stringWithFormat:@"%@ %@",[[newarr1 objectAtIndex:i]objectForKey:@"name"],[[newarr1 objectAtIndex:i]objectForKey:@"surname"]];
                pin.subtitle = nil;
                pin.coordinate = newCoord;
                
                [pins addObject:pin];
                
                    i++;
            }
            
            [_mapView addAnnotations:pins];
            //        [SVProgressHUD dismiss];
            [act removeFromSuperview];
            
            NSTimer *timerr;
            timerr = [NSTimer scheduledTimerWithTimeInterval:3.0
                                                      target:self
                                                    selector:@selector(targetMethod:)
                                                    userInfo:nil
                                                     repeats:NO];
       // }
        
        }
        
        
    }
    
    else
    {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"profiletaverse"];
        _mapView.delegate = self;
    }
        
    }];

}

-(void) targetMethod:(NSTimer *)timer
{
    [self loadmapfromweb];
}


- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)aUserLocation {
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 8;
    span.longitudeDelta = 8;
    CLLocationCoordinate2D location;
    location.latitude = aUserLocation.coordinate.latitude;
    location.longitude = aUserLocation.coordinate.longitude;
    region.span = span;
    region.center = location;
    //    [aMapView setRegion:region animated:YES];
    
    MKAnnotationView* annotationView = [aMapView viewForAnnotation:aUserLocation];
    annotationView.canShowCallout = NO;
}

+(void)chngpostion
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Data" object:Nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getData:) name:@"Data" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(navtopage:) name:@"pagenav" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startDBInsertAgain) name:@"db_busy" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestreceivedaction) name:@"Requestreceived_push" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(acceptedrequestaction) name:@"Accepted_push" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AcceptedRequest) name:@"Accepted_request" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AddFriend) name:@"NewConnection" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ConnectionType) name:@"NewConnectionType" object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AcceptedRequest) name:@"TypeChange" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shareLocation) name:@"ShareLocation" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdateInformation) name:@"UpdateInfo" object:nil];
    
}

-(void)UpdateInformation
{
    ConPersonalProfileViewController *PVC = [[ConPersonalProfileViewController alloc]init];
    PVC.toUpdateInfo = @"YES";
    [self.navigationController pushViewController:PVC animated:YES];
   // [[NSNotificationCenter defaultCenter] postNotificationName:@"Update Info" object:Nil];
    
}


-(void)shareLocation
{
    ConLocateGroupViewController *conLocate = [[ConLocateGroupViewController alloc]init];
    conLocate.group_id=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"sharedLocGrpId"]];
    [self.navigationController pushViewController:conLocate animated:YES];
}

-(void)ConnectionType
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDel.newrequestRedirect)
    {
    ConNewRequestsViewController *mng = [[ConNewRequestsViewController alloc]init];
       mng.userid = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userforrequest"] intValue];
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
        
    appDel.newrequestRedirect = NO;
    
 //   [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:mng animated:YES];
    }
    
}


-(void)AddFriend
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDel.newUser)
    {
    
    ConAddFriendViewController *con = [[ConAddFriendViewController alloc]init];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
        
        appDel.newUser = NO;
    
 //   [[self navigationController].view.layer addAnimation:transition forKey:nil];
    [[self navigationController] pushViewController:con animated:YES];
        
    }
    
}

-(void)AcceptedRequest
{
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (del.profileRedirect)
    {
    
    ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
    // con.other=@"yes";
    con.request = YES;
    [self.tabBarController.tabBar setHidden:YES];
    // con.uid=[NSString stringWithFormat:@"%d",[userid intValue]];
    //                [self.navigationController presentViewController:con animated:NO completion:nil];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
        
        del.profileRedirect = NO;
    
 //   [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
        
    }
    
}

-(void)requestreceivedaction
{
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
    ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
    con.other=@"yes";
    con.uid=[NSString stringWithFormat:@"%@",del.pushtoprofileid];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"profiletaverse"];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}

-(void)acceptedrequestaction
{
    ConRequestViewController *con = [[ConRequestViewController alloc]init];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}


-(void)navtopage: (NSNotification *)notification
{
    NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
    if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"4"])
    {
        ConAccountSettingsViewController *con = [[ConAccountSettingsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"6"])
    {
        ConInviteViewController *con = [[ConInviteViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"5"])
    {
        ConSyncLoaderViewController *con = [[ConSyncLoaderViewController alloc]init];
        //        [self.navigationController pushViewController:con animated:NO];
        
        [self presentViewController:con animated:NO completion:nil];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"2"])
    {
        //ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
         ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"profiletaverse"];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"3"])
    {
        ConQROptionsViewController *con = [[ConQROptionsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    [mainview setFrame:CGRectMake(0, mainview.frame.origin.y, mainview.frame.size.width, mainview.frame.size.height)];
    //    [prefs setObject:@"111" forKey:@"whichpage"];
}

-(void)getData:(NSNotification *)notification {
    
    if(move == 0) {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake(280, mainview.frame.origin.y, mainview.frame.size.width, mainview.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             move = 1;
                         }];
    } else {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake(0, (mainview.frame.origin.y),mainview.frame.size.width, mainview.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             move = 0;
                         }];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getDirections
{
    CLLocationCoordinate2D start = {37.774929, -122.419416 };
    CLLocationCoordinate2D end = {40.714353, -74.005973};
    
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
    
    MKMapItem *source_mapitem =[[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:start addressDictionary:nil]];
    [source_mapitem setName:@"Source"];
    
    MKMapItem *destination_mapitem = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:end addressDictionary:nil]];
    
    [destination_mapitem setName:@"Destination"];
    request.source = source_mapitem;
    request.destination = destination_mapitem;
    
    request.requestsAlternateRoutes = YES;
    MKDirections *directions =
    [[MKDirections alloc] initWithRequest:request];
    
    [directions calculateDirectionsWithCompletionHandler:
     ^(MKDirectionsResponse *response, NSError *error) {
         if (error) {
             DebugLog(@" error is %@",error.localizedDescription);
         } else {
             DebugLog(@" here we came");
             [self showRoute:response];
         }
     }];
}

-(void)showRoute:(MKDirectionsResponse *)response
{
    for (MKRoute *route in response.routes)
    {
        [map_View addOverlay:route.polyline level:MKOverlayLevelAboveLabels];
        for (MKRouteStep *step in route.steps)
        {
            DebugLog(@" here %@", step.instructions);
        }
    }
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id < MKOverlay >)overlay
{
    MKPolylineRenderer *renderer =
    [[MKPolylineRenderer alloc] initWithOverlay:overlay];
    renderer.strokeColor = [UIColor blueColor];
    renderer.lineWidth = 2.0;
    return renderer;
}


- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views {
    MKAnnotationView *aV;
    for (aV in views) {
        if ([aV.annotation isKindOfClass:[MKUserLocation class]]) {
            MKAnnotationView* annotationView = aV;
            annotationView.canShowCallout = NO;
        }
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    MKAnnotationView *annView;
    
    if([annotation class] == MKUserLocation.class) {
        //userLocation = annotation;
        annView.canShowCallout = NO;
        return nil;
    }
    
    REVClusterPin *pin = (REVClusterPin *)annotation;
    
    if( [pin nodeCount] > 0 ){
        pin.title = @"___";
        

        
        annView = (REVClusterAnnotationView*)
        [mapView dequeueReusableAnnotationViewWithIdentifier:@"cluster"];
        
        if( !annView )
            annView = (REVClusterAnnotationView*)[[REVClusterAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"cluster"];
        
        annView.image = [UIImage imageNamed:@"locator.png"];
        
        [(REVClusterAnnotationView*)annView setClusterText:
         [NSString stringWithFormat:@"%lu",(unsigned long)[pin nodeCount]]];
        
        annView.canShowCallout = NO;
    } else {
        annView = [mapView dequeueReusableAnnotationViewWithIdentifier:@"pin"];
        
        //        if( !annView )
        //            annView = [[MKAnnotationView alloc] initWithAnnotation:annotation
        //                                                    reuseIdentifier:@"pin"];
        
        //        annView.image = [UIImage imageNamed:@"locator2.png"];
        
        
        annView = [[MKAnnotationView alloc] init]; //MKAnnotationView 18.11
        //            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomPinAnnotationView"];
        //        annView.animatesDrop = YES;
        annView.canShowCallout = NO;
        annView.image = [UIImage imageNamed:@"locator1.png"];
        annView.calloutOffset = CGPointMake(0, 0);
        
        
        UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        annView.rightCalloutAccessoryView = rightButton;
        annView.rightCalloutAccessoryView.frame=CGRectMake(0, 0, 0, 0);
        
        UIImageView *iconView = [[UIImageView alloc] init];
        iconView.frame = CGRectMake(0.0f, 0.0f,0, 0);
        annView.leftCalloutAccessoryView = iconView;
        iconView.backgroundColor=[UIColor whiteColor];
        annView.userInteractionEnabled=YES;
        if (annotation == mapView.userLocation)
        {
            annView.image= [UIImage imageNamed:@"locatorown.png"];
            annView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomPinAnnotationView"]; ////MKAnnotationView 18.11
        }
        //            if(typec==1)
        //            {
        DebugLog(@"(int)annotation.subtitle===%@",annotation.subtitle);
        for(int newi=0;newi<[newarr1 count];newi++)
        {
            DebugLog(@"pin.title = %@", pin.title);
            //            NSString *pintitle = pin.title;
            //            NSArray *namearr= [pintitle componentsSeparatedByString:@" "];
            //            NSString *namefirst = [namearr objectAtIndex:0];
            //            DebugLog(@"namefirst: %@",namefirst);
            
            NSString *webstr = [NSString stringWithFormat:@"%@ %@",[[newarr1 objectAtIndex:newi]objectForKey:@"name"],[[newarr1 objectAtIndex:newi]objectForKey:@"surname"]];
            DebugLog(@"webstr = %@",webstr);
            //            if([[[newarr1 objectAtIndex:newi]objectForKey:@"name"]isEqualToString:namefirst])
            if ([webstr isEqualToString:pin.title])
            {
                if([[[newarr1 objectAtIndex:newi]objectForKey:@"business"]intValue] ==1)
                {
                    //                    [self loadImage:[NSString stringWithFormat:@"%@",[[newarr1 objectAtIndex:newi] objectForKey:@"image"]] andImageView:iconView];
                    iconView.image=[UIImage imageNamed:@"icon_busi.png"];
                    annView.image = [UIImage imageNamed:@"locator2.png"];
                }
                else
                {
                    iconView.image=[UIImage imageNamed:@"icon_friend.png"];
                    annView.image = [UIImage imageNamed:@"locator1.png"];
                    DebugLog(@"friend");
                }
                userid =[[[newarr1 objectAtIndex:newi] objectForKey:@"id"]intValue];
                annView.tag = [[[newarr1 objectAtIndex:newi] objectForKey:@"id"]intValue];
                break;
            }
        }
        annView.canShowCallout = YES;
        annView.calloutOffset = CGPointMake(-6.0, 0.0);
    }
    if (annView != nil)
    {
        
        CALayer* layer = annView.layer;
        [layer removeAllAnimations];
    }
    return annView;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    DebugLog(@"REVMapViewController mapView didSelectAnnotationView: %d",userid);
    
   // NSLog(@"pin.title------------- %@",pin.title);
//    CLLocationCoordinate2D centerCoordinate = [(REVClusterPin *)view.annotation coordinate];
//   MKCoordinateSpan span = {.latitudeDelta =  1, .longitudeDelta =  1};
//   //  MKCoordinateSpan span = {0.097, 0.097};
//    MKCoordinateRegion region = {centerCoordinate, span};
//    
//    [mapView setRegion:region animated:YES];
//
//    REVClusterPin *pin = [[REVClusterPin alloc] init];
//    pin.title = [NSString stringWithFormat:@"%@",[newarr1 valueForKey:@"name"]];
    
    
    if ([view isKindOfClass:[REVClusterAnnotationView class]])
    {
        CLLocationCoordinate2D centerCoordinate = [(REVClusterPin *)view.annotation coordinate];
        MKCoordinateSpan newSpan =
        MKCoordinateSpanMake(mapView.region.span.latitudeDelta/4.0,
                             mapView.region.span.longitudeDelta/4.0);
        
        //mapView.region = MKCoordinateRegionMake(centerCoordinate, newSpan);
        [mapView setRegion:MKCoordinateRegionMake(centerCoordinate, newSpan)
                  animated:YES];
        
        
    }
}


//-(void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
//    
//    for (UIView *subview in view.subviews ){
//        DebugLog(@"sub: %@",subview);
//        if ([view isKindOfClass:[REVClusterAnnotationView class]])
//        {
//        }
//    }
//}



-(void)maptap1: (UIGestureRecognizer *)sender
{
    DebugLog(@"bhaswar yes: %ld",(long)sender.view.tag);
//    ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
    ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
    con.other=@"yes";
    con.uid=[NSString stringWithFormat:@"%ld",(long)sender.view.tag];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"profiletaverse"];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}


- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    DebugLog(@"Tapped on %@ & Tapped user id: %ld", view.annotation.title, (long)view.tag);
    DebugLog(@"Tapped on %@ & Tapped user id: %ld", view.annotation.title, (long)userid);
    //        NSString *IdTag =view.annotation.subtitle;
//    ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
    ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
    con.other=@"yes";
    con.mapReturn = @"mapBack";
    con.uid=[NSString stringWithFormat:@"%ld",(long)view.tag];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"profiletaverse"];
    
    if ([retval count]>0)
    {
        for (NSDictionary *tempDict in retval)
            
        {
            
            if ([[tempDict objectForKey:@"id"] intValue]==userid)
                
            {
                
                if ([[tempDict objectForKey:@"business"] intValue]==1)
                    
                {
                    
                    con.business_friend = @"yes";
                    
                }
                
                
                
            }
            
        }
        
    }
    if (json1!=nil)
        
    {
        
        NSDictionary *newdict= [json1 objectForKey:@"details"];
        
        for (NSDictionary *reqdict in [newdict objectForKey:@"markers"])
            
        {
            
            if ([[reqdict objectForKey:@"id"] intValue]==userid)
                
            {
                
                if ([[reqdict objectForKey:@"business"] intValue]==1)
                    
                {
                    
                    con.business_friend = @"yes";
                    
                }
                
            }
            
        }
        
    }
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}

-(void)viewDidDisappear:(BOOL)animated
{
    move=0;
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"profiletaverse"])
        [mainview removeFromSuperview];
    [super viewDidDisappear:YES];
}

-(void)loadmapfromweb
{
    loadagainarr = [[NSMutableArray alloc]init];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        DebugLog(@"hmmmmmm again load map");
        NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=map&id=%@&access_token=%@&device_id=%@",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
        
        DebugLog(@"profile url: %@",urlString1);
        
        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
        
        if (signeddataURL1 == nil)
        {
            DebugLog(@"ekhanei to elona,mane data asheni");
            alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            //            [alert show];
            //            [SVProgressHUD dismiss];
            [act removeFromSuperview];
        }
        else
        {
            NSError *error =nil;
            json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                     
                                                    options:kNilOptions
                     
                                                      error:&error];
            DebugLog(@"json returns: %@",json1);
            
            NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
            DebugLog(@"err  %@",errornumber);
            if (![errornumber isEqualToString:@"0"])
            {
                DebugLog(@"if if");
                
                [act removeFromSuperview];
            }
            
            if ([[json1 objectForKey:@"details"] isKindOfClass:[NSNull class]] || [json1 objectForKey:@"details"] == (id)[NSNull null] ||[[json1 objectForKey:@"details"] count] ==0)
            {
                DebugLog(@"if called req");
                alert = [[UIAlertView alloc] initWithTitle:@"The map will show addresses once you connect with people!"
                                                   message:nil
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                //                [alert show];
                //                [SVProgressHUD dismiss];
                [act removeFromSuperview];
            }
            else
            {
                NSDictionary *newdict= [json1 objectForKey:@"details"];
                for (NSDictionary *reqdict in [newdict objectForKey:@"markers"])
                {
                    [loadagainarr addObject:reqdict];
                }
                //                DebugLog(@"map list array has: %@",loadagainarr);
                
            }
            
            //            [[DBManager getSharedInstance]insertMap:loadagainarr];
            
            DebugLog(@"loadagainarr ache: %lu",(unsigned long)[loadagainarr count]);
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            DebugLog(@"loadagainarr ekhon.. %lu",(unsigned long)[loadagainarr count]);
            if([loadagainarr count]>0)
            {
                [self performSelectorOnMainThread:@selector(startDBInsertAgain)
                                       withObject:nil
                                    waitUntilDone:YES];
                [self loaddatabase_data];
                
            }
            
        });
    });
}


-(void) loaddatabase_data
{
    DebugLog(@"1st time er por theke");
    //    NSArray *retval = [[DBManager getSharedInstance]fetchMap];
    retval = [[NSUserDefaults standardUserDefaults]objectForKey:@"mapdata"];
    newarr1= [NSMutableArray new];
    newarr1 = [retval mutableCopy];
    
   
}


-(void)startDBInsert
{
       [[NSUserDefaults standardUserDefaults]setObject:newarr1 forKey:@"mapdata"];
}

-(void)startDBInsertAgain
{
        DebugLog(@"start db insert");
      
    [[NSUserDefaults standardUserDefaults]setObject:loadagainarr forKey:@"mapdata"];
}

@end