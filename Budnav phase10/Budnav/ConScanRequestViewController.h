//
//  ConScanRequestViewController.h
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 26/06/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConParentTopBarViewController.h"
#import "GAITrackedViewController.h"

@interface ConScanRequestViewController : GAITrackedViewController

@property(nonatomic,retain)NSString *userid,*request_type;

@end
