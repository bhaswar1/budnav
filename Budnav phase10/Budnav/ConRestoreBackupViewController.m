//
//  ConRestoreBackupViewController.m
//  Budnav
//
//  Created by intel on 06/07/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "ConRestoreBackupViewController.h"
#import "MDRadialProgressView.h"
#import "MDRadialProgressTheme.h"
#import "MDRadialProgressLabel.h"
#import "SVProgressHUD.h"
#import <AddressBook/AddressBook.h>
#import "AppDelegate.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

@interface ConRestoreBackupViewController ()<UITableViewDataSource,UITableViewDelegate,NSURLConnectionDelegate,NSURLConnectionDataDelegate,UIAlertViewDelegate>
{
    UIView *topbar, *blackView;
    AppDelegate *del;
    UIImageView *bck_img;
    UILabel *heading_div;
    UITableView *restore_table;
    NSMutableArray *dateArray, *contactsArray, *backups, *backup_array, *app_contacts, *newcontactsapp, *con_array;
    NSDictionary *json, *indexedSections;
    UIAlertView *alert;
    UIActivityIndicatorView *act;
    NSUserDefaults *prefs;
    int index;
}

@end

@implementation ConRestoreBackupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor colorWithRed:(238.0f/255.0f) green:(238.0f/255.0f) blue:(238.0f/255.0f) alpha:1.0f];
    del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, [[UIScreen mainScreen]bounds].size.height-44, [UIScreen mainScreen].bounds.size.width, 44)];
    
    del.tabBarController.tabBar.hidden = YES;
    self.navigationController.navigationBarHidden=YES;
    
    
    topbar = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64)];
    [self.view addSubview:topbar];
    //topbar.backgroundColor=[UIColor blackColor];
    topbar.layer.zPosition=2;
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
    
    // Add the gradient to the view
    [topbar.layer insertSublayer:gradient atIndex:0];
    
    
    UIImageView *logo_img = [[UIImageView alloc]init];
    logo_img.frame = CGRectMake(27+([UIScreen mainScreen].bounds.size.width-160)/2, 32, 101, 20);
    logo_img.backgroundColor = [UIColor clearColor];
    
    logo_img.image = [UIImage imageNamed:@"new_logo"];
    [topbar addSubview:logo_img];
    
    
    bck_img = [[UIImageView alloc]initWithFrame:CGRectMake(15,32,12,20)];//(13, 39, 10, 22)];
    bck_img.image=[UIImage imageNamed:@"back3"];
    [topbar addSubview:bck_img];
    
    
    UIButton *back_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    back_btn.frame = CGRectMake(0, 0, logo_img.frame.origin.x
                                -1, 64);
    back_btn.backgroundColor=[UIColor clearColor];
    [back_btn setTitle:nil forState:UIControlStateNormal];
    
    [back_btn addTarget:self action:@selector(backColor) forControlEvents:UIControlEventTouchDown];
    [back_btn addTarget:self action:@selector(backColorAgain) forControlEvents:UIControlEventTouchDragExit];
    [back_btn addTarget:self action:@selector(backColor) forControlEvents:UIControlEventTouchDragEnter];
    [back_btn addTarget:self action:@selector(goback) forControlEvents:UIControlEventTouchUpInside];
    [topbar addSubview:back_btn];
    
    UIView *headingView = [[UIView alloc] initWithFrame:CGRectMake(0, 64, [UIScreen mainScreen].bounds.size.width, 101)];
    [headingView setBackgroundColor:[UIColor colorWithRed:(193.0f/255.0f) green:(39.0f/255.0f) blue:(45.0f/255.0f) alpha:1.0f]];
    [self.view addSubview:headingView];
    
    
    UILabel *headingLabel = [[UILabel alloc] initWithFrame:CGRectMake(33, 20, [UIScreen mainScreen].bounds.size.width-66, 60)];
    [headingLabel setBackgroundColor:[UIColor clearColor]];
    headingLabel.text = @"Attention! Restoring a backup will\nreplace your phone contacts with\nthe selected backup!";
    if ([UIScreen mainScreen].bounds.size.width>320) {
        headingLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:20];
    }
    else
    {
    headingLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    }
    //    headingLabel.lineBreakMode = YES;
    headingLabel.numberOfLines = 3;
    headingLabel.textColor = [UIColor colorWithRed:(255.0f/255.f) green:(255.0f/255.f) blue:(255.0f/255.f) alpha:1.0f];
    headingLabel.textAlignment = NSTextAlignmentCenter;
    [headingView addSubview:headingLabel];
    
    
    
    heading_div = [[UILabel alloc] initWithFrame:CGRectMake(0, 100.4, [UIScreen mainScreen].bounds.size.width, 0.6f)];
    [heading_div setBackgroundColor:[UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f]];
    [headingView addSubview:heading_div];
    
    dateArray = [[NSMutableArray alloc] initWithObjects:@"20-05-2015",@"05-05-2015",@"19-03-2015",@"16-10-2014",@"10-08-2014", nil];
    contactsArray = [[NSMutableArray alloc] initWithObjects:@"233",@"200",@"225",@"200",@"12", nil];
    
    [self getBackupList];
}

-(void)viewDidAppear:(BOOL)animated
{
    restore_table = [[UITableView alloc] initWithFrame:CGRectMake(0.0f, 165.0f, self.view.frame.size.width,[backups count]*50)];
    [restore_table setBackgroundColor:[UIColor whiteColor]];
    [restore_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    restore_table.delegate= self;
    [restore_table setDataSource:self];
    restore_table.userInteractionEnabled=YES;
    restore_table.scrollEnabled=YES;
    [self.view addSubview:restore_table];
    
    indexedSections = [[NSDictionary alloc] init];
}

-(void)getBackupList
{
    prefs = [NSUserDefaults standardUserDefaults];
    
        NSString *restoreUrl = [NSString stringWithFormat:@"https://budnav.com/ext/?action=getbackuppoints&access_token=%@&device_id=%@",[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
        
        NSError *errorjs= nil;
        
        NSData *restore =[NSData dataWithContentsOfURL:[NSURL URLWithString:restoreUrl]];
        
        if (restore != nil) {
            
            DebugLog(@"GetBackup Point URL:%@",restoreUrl);
            
            NSDictionary *backup_json = [NSJSONSerialization JSONObjectWithData:restore //1
                                                                        options:kNilOptions
                                         
                                                                          error:&errorjs];
            DebugLog(@"GetBackup Point Return: %@",backup_json);
            
            if ([[backup_json objectForKey:@"success"]intValue] == 1)
            {
                DebugLog(@"GetBackup Point:%@",backup_json);
                
                NSMutableDictionary *details = [[NSMutableDictionary alloc] init];
                details = [backup_json objectForKey:@"details"];
                
                backups = [[NSMutableArray alloc] init];
                backups = [details objectForKey:@"backups"];
                
            }
            else
            {
                UIAlertView *alrt= [[UIAlertView alloc] initWithTitle:nil message:@"No Backup Found!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alrt show];
            }
        }
        else
        {
            UIAlertView *alrt= [[UIAlertView alloc] initWithTitle:nil message:@"Error in server connection!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alrt show];
        }
        
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 50;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [backups count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell==nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    DebugLog(@"Timestamp:%d",[[[backups valueForKey:@"time"] objectAtIndex:indexPath.row] intValue]);
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[[[backups valueForKey:@"time"] objectAtIndex:indexPath.row] intValue]];
    
    NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
    [_formatter setLocale:[NSLocale currentLocale]];
    [_formatter setDateFormat:@"dd-MM-yyyy"];
    NSString *exactDate=[_formatter stringFromDate:date];
    DebugLog(@"Date:%@",exactDate);
    
    UILabel *dateLabel = [[UILabel alloc] init];
    dateLabel.frame = CGRectMake(20, 15, 100, 20);
    dateLabel.backgroundColor = [UIColor clearColor];
    dateLabel.textColor = [UIColor blackColor];
    dateLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    //dateLabel.text = [dateArray objectAtIndex:indexPath.row];
    [cell addSubview:dateLabel];
    
    
    dateLabel.text = exactDate;
    
    UILabel *contacts = [[UILabel alloc] init];
    contacts.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-120, 15, 100, 20);
    contacts.backgroundColor = [UIColor clearColor];
    contacts.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f];
    contacts.textAlignment = NSTextAlignmentRight;
    contacts.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    //contacts.text = [NSString stringWithFormat:@"%@ contacts",[contactsArray objectAtIndex:indexPath.row]];
    [cell addSubview:contacts];
    
    contacts.text = [NSString stringWithFormat:@"%@ contacts",[[backups valueForKey:@"contacts"] objectAtIndex:indexPath.row]];
    
    UILabel *separator = [[UILabel alloc] init];
    separator.frame = CGRectMake(0, 49.4f, [UIScreen mainScreen].bounds.size.width, 0.6f);
    separator.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f];
    [cell addSubview:separator];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    index = (int)indexPath.row;
   alert = [[UIAlertView alloc] initWithTitle:@"Are you sure you want to restore this backup?"
                                       message:nil
                                      delegate:self
                             cancelButtonTitle:@"No"  otherButtonTitles:@"Yes", nil];
    alert.tag=99;
    [alert show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    DebugLog(@"ALERT . TAG====%ld",(long)alertView.tag);
    if (alertView.tag == 99) {
        
        if(buttonIndex == 0)
        {
            // Do something
        }
        else
        {
            [UIView animateWithDuration:0.0f animations:^{
                
                blackView = [[UIView alloc] init];
                blackView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
                blackView.backgroundColor = [UIColor blackColor];
                [self.view addSubview:blackView];
                
                act = [[UIActivityIndicatorView alloc] init];
                act.center=blackView.center;
                [act startAnimating];
                act.hidden=NO;
                act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
                [act setColor:[UIColor whiteColor]];
                [blackView addSubview:act];
                
                UILabel *restoring = [[UILabel alloc] init];
                restoring.frame = CGRectMake(0, act.frame.origin.y+act.frame.size.height+10, [UIScreen mainScreen].bounds.size.width, 50);
                restoring.text = @"Restoring Contacts...";
                restoring.textColor = [UIColor whiteColor];
                restoring.textAlignment = NSTextAlignmentCenter;
                [blackView addSubview:restoring];
                
            }completion:^(BOOL finished){
                
                [self delAllContacts];
                
                prefs = [NSUserDefaults standardUserDefaults];
                
                NSString *restoreUrl = [NSString stringWithFormat:@"https://budnav.com/ext/?action=restorebackup&backupid=%d&access_token=%@&device_id=%@",[[[backups valueForKey:@"id"] objectAtIndex:index] intValue],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
                
                NSError *errorjs= nil;
                
                NSData *restore =[NSData dataWithContentsOfURL:[NSURL URLWithString:restoreUrl]];
                
                if (restore != nil) {
                    
                    DebugLog(@"Restore URL:%@",restoreUrl);
                    
                    NSDictionary *backup_json = [NSJSONSerialization JSONObjectWithData:restore //1
                                                                                options:kNilOptions
                                                 
                                                                                  error:&errorjs];
                    DebugLog(@"Restore Return: %@",backup_json);
                    
                    if ([[backup_json objectForKey:@"success"]intValue] == 1)
                    {
                        DebugLog(@"Restore Backup:%@",backup_json);
                        
                        //////////////////////////////////// Again Call Getbackup For Restoring /////////////////////////////////////////////
                        
                        NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=getbackup&id=%@&access_token=%@&device_id=%@&thumb=true",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
                        
                        DebugLog(@"contacts -- now the url is : %@",urlString1);
                        
                        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
                        
                        NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
                        
                        if (signeddataURL1 == nil)
                        {
                            alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                                               message:nil
                                                              delegate:self
                                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                            [alert show];
                            
                        }
                        else
                        {
                            json = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                   options:kNilOptions
                                                                     error:&errorjs];
                            DebugLog(@"JSON FIRST HIT:%@",json);
                            
                            NSString *errornumber= [NSString stringWithFormat:@"%@",[json objectForKey:@"errorno"]];
                            
                            if (![errornumber isEqualToString:@"0"])
                            {
                                NSString *err_str = [json objectForKey:@"error"];
                                alert = [[UIAlertView alloc] initWithTitle:@"Error in Retrieving Data!"
                                                                   message:err_str
                                                                  delegate:self
                                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                                [alert show];
                            }
                            else
                            {
                                backup_array = [[NSMutableArray alloc]init];
                                if ([[json objectForKey:@"details"] isKindOfClass:[NSNull class]] || [json objectForKey:@"details"] == (id)[NSNull null] ||[[json objectForKey:@"details"] count] ==0)
                                {
                                    UIAlertView *alrt= [[UIAlertView alloc] initWithTitle:nil message:@"No Data Found!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                    [alrt show];
                                }
                                else
                                {
                                    
                                    NSDictionary *contacts_temp_dict = [json objectForKey:@"details"];
                                    
                                    DebugLog(@"CONTACTS TEMP DICT:%@",contacts_temp_dict);
                                    // newcontactsapp = [[NSMutableArray alloc]init];
                                    int con_count1;
                                    con_count1 = 0;
                                    for (NSDictionary *dict in [contacts_temp_dict objectForKey:@"contacts"])
                                    {
                                        [backup_array addObject:dict]; //[dict objectForKey:@"fullname"]
                                    }
                                    //result = [newcontactsapp componentsJoinedByString:@","];
                                    
                                    if (con_count1 == 1)
                                    {
                                        //                                [[[[iToast makeText:[NSString stringWithFormat:@" %@ is a new contact",result]]
                                        //                                   setGravity:iToastGravityTop] setDuration:iToastDurationLong] show];
                                    }
                                    else
                                    {
                                        //                                [[[[iToast makeText:[NSString stringWithFormat:@" %@ are new contacts",result]]
                                        //                                   setGravity:iToastGravityTop] setDuration:iToastDurationLong] show];
                                    }
                                    
                                    DebugLog(@"Back Up Contacts: %@",backup_array);
                                    //////////////////////////////////////    add to phonebook    ////////////////////////////////////
                                    //                    if ([[prefs objectForKey:@"copycontacts"] isEqualToString:@"no"])
                                    
                                    //                        }   //con_count closing brace
                                    //                    for (NSDictionary *dict in [contacts_temp_dict objectForKey:@"contacts"])
                                    //                    {
                                    //                        [app_contacts addObject:dict];
                                    //                    }
                                    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%ld",(unsigned long)[backup_array count]] forKey:@"backup_count"];
                                    [self getBudnavContact];
                                    
                                    con_array = [[NSMutableArray alloc]init];
                                    
                                    for (NSDictionary *appDict in newcontactsapp) {
                                        [con_array addObject:appDict];
                                    }
                                    
                                    for (NSDictionary *backupDict in backup_array) {
                                        [con_array addObject:backupDict];
                                    }
                                    
                                    [self Indexing];
                                    
                                    [self RestoreInDevice];
                                }
                            }
                        }
                        
                        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        
                        
                        
                    }
                    else
                    {
                        [blackView removeFromSuperview];
                        UIAlertView *alrt= [[UIAlertView alloc] initWithTitle:nil message:@"No Backup Found!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                        [alrt show];
                    }
                }
                else
                {
                    [blackView removeFromSuperview];
                    UIAlertView *alrt= [[UIAlertView alloc] initWithTitle:nil message:@"Error in server connection!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alrt show];
                }

                
            }];
            
        }
    }
}

-(void)backColorAgain
{
    bck_img.alpha = 1.0f;
}
-(void)backColor
{
    bck_img.alpha = 0.5f;
}

-(void)goback
{
    del.tabBarController.tabBar.hidden = NO;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)getBudnavContact
{
    prefs = [[NSUserDefaults standardUserDefaults] init];
    NSError *error = nil;
    NSString *urlString =[NSString stringWithFormat:@"https://budnav.com/ext/?action=contacts&amount=-1&id=%@&access_token=%@&device_id=%@&thumb=true",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
    
    DebugLog(@"contacts -- now the url is : %@",urlString);
    
    NSString *newString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString]];
    
    if (signeddataURL == nil)
    {
    
        alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        //[act removeFromSuperview];
    }
    else
    {
        json = [NSJSONSerialization JSONObjectWithData:signeddataURL //1
                                                options:kNilOptions
                                                  error:&error];
        DebugLog(@"JSON FIRST HIT:%@",json);
        
        NSString *errornumber= [NSString stringWithFormat:@"%@",[json objectForKey:@"errorno"]];
        
        if (![errornumber isEqualToString:@"0"])
        {
            [blackView removeFromSuperview];
            NSString *err_str = [json objectForKey:@"error"];
            alert = [[UIAlertView alloc] initWithTitle:@"Error in Retrieving Data!"
                                               message:err_str
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
            //[act removeFromSuperview];
        }
        else
        {
            app_contacts = [[NSMutableArray alloc]init];
            //        //DebugLog(@"details req= %@",[json1 objectForKey:@"details"]);
            if ([[json objectForKey:@"details"] isKindOfClass:[NSNull class]] || [json objectForKey:@"details"] == (id)[NSNull null] ||[[json objectForKey:@"details"] count] ==0)
            {
                //[act removeFromSuperview];
                //            [SVProgressHUD dismiss];
            }
            else
            {
                ////////////////////////////   App Contacts Found   /////////////////////////////////
                
                //            if ([string rangeOfString:@"123"].location == NSNotFound) {
                //            } else {}
                
                NSDictionary *contacts_temp_dict = [json objectForKey:@"details"];
                
                DebugLog(@"CONTACTS TEMP DICT:%@",contacts_temp_dict);
                newcontactsapp = [[NSMutableArray alloc]init];
                NSString *result;
                int con_count1;
                con_count1 = 0;
                //                        if (con_count1 > 0)
                //                        {
                for (NSDictionary *dict in [contacts_temp_dict objectForKey:@"contacts"])
                {
                    [newcontactsapp addObject:dict]; //[dict objectForKey:@"fullname"]
                }
                result = [newcontactsapp componentsJoinedByString:@","];
                
                if (con_count1 == 1)
                {
                    //                                [[[[iToast makeText:[NSString stringWithFormat:@" %@ is a new contact",result]]
                    //                                   setGravity:iToastGravityTop] setDuration:iToastDurationLong] show];
                }
                else
                {
                    //                                [[[[iToast makeText:[NSString stringWithFormat:@" %@ are new contacts",result]]
                    //                                   setGravity:iToastGravityTop] setDuration:iToastDurationLong] show];
                }
                
                DebugLog(@"newcontactsapp: %@",newcontactsapp);
                //////////////////////////////////////    add to phonebook    ////////////////////////////////////
                //                    if ([[prefs objectForKey:@"copycontacts"] isEqualToString:@"no"])
                
                //                        }   //con_count closing brace
                for (NSDictionary *dict in [contacts_temp_dict objectForKey:@"contacts"])
                {
                    [app_contacts addObject:dict];
                }
                [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%ld",(unsigned long)[app_contacts count]] forKey:@"app_contacts_count"];
            }
        }
    }
}

-(void)RestoreInDevice
{
    DebugLog(@"Budnav Contacts=%ld\nBackup Contacts=%ld\nCombined Array:%ld %@",[newcontactsapp count],[backup_array count],[con_array count],con_array);
    
     ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, nil);
    
    CFErrorRef *errorab = nil;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, errorab);
    
    __block BOOL accessGranted = NO;
    
    if (&ABAddressBookRequestAccessWithCompletion != NULL) { // we're on iOS 6
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(sema);
        });
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        //        dispatch_release(sema);
    }
    else { // we're on iOS 5 or older
        accessGranted = YES;
    }
    if (accessGranted) {
        
        
        int finalwrite;
        for (finalwrite =0; finalwrite < [con_array count]; finalwrite++)
        {
            int traverselooptocheck;
            traverselooptocheck=0;
            
            ///////////////////////////////// Newly Added //////////////////////////////////////
            NSDictionary *finalwritedict = [con_array objectAtIndex:finalwrite];
            NSData *decodedData;
            UIImage *profilepic;
            
           // NSString *petFirstName = [finalwritedict objectForKey:@"name"];
           // NSString *petLastName = [finalwritedict objectForKey:@"surname"];
            NSString *petfullname= [finalwritedict objectForKey:@"fullname"];
            NSString *workphone = @"";
            NSString *mobilephone = @"";
            NSString *homephone = @"";
            NSString *strretp = @"";
            NSString *cityp= @"";
            NSString *housenumberp = @"";
            NSString *countryp = @"";
            NSString *zipCode = @"";
            NSString *petEmail = @"";
            
            ////////////////// Base64 Conversion //////////////////////
            
            NSString *base64String1= [finalwritedict objectForKey:@"thumb"];
            if ([base64String1 length] > 6)
            {
               decodedData = [[NSData alloc] initWithBase64EncodedString:base64String1 options:0];
               profilepic = [UIImage imageWithData:decodedData];
                
            }
            
            NSData *petImageData = UIImageJPEGRepresentation(profilepic, 0.7f);
            
            
            DebugLog(@"ALL CONTACTS:%@",finalwritedict);
            NSArray *strings = [[finalwritedict objectForKey:@"phone"] componentsSeparatedByString:@","];
            DebugLog(@"PHONE NUMBERS:%@",strings);
            
            //int count = (int)strings.count;
//            if ([[finalwritedict objectForKey:@"phone"] length] > 2)
//                homephone = [NSString stringWithFormat:@"%@",[finalwritedict objectForKey:@"phone"]];
//            for (int i =0; i<count; i++) {
//                if (i==0) {
                    if ([[finalwritedict objectForKey:@"home"] length] > 2)
                    {
                        homephone = [NSString stringWithFormat:@"%@",[finalwritedict objectForKey:@"home"]];
                    }
//                }
//                
//                if (i==1) {
                if ([[finalwritedict objectForKey:@"mobile"] length] > 2)
                {
                    mobilephone = [NSString stringWithFormat:@"%@",[finalwritedict objectForKey:@"mobile"]];
                }
            
            if ([[finalwritedict objectForKey:@"work"] length] > 2)
            {
                workphone = [NSString stringWithFormat:@"%@",[finalwritedict objectForKey:@"work"]];
            }

            
           // }
            
//            if ([[strings objectAtIndex:0] length] > 2)
//                homephone = [NSString stringWithFormat:@"%@",[strings objectAtIndex:0]];
//            if ([[strings objectAtIndex:1] length] > 2)
//                mobilephone = [NSString stringWithFormat:@"%@",[strings objectAtIndex:1]];
            if ([[finalwritedict objectForKey:@"city"] length] > 2)
                cityp = [NSString stringWithFormat:@"%@",[finalwritedict objectForKey:@"city"]];
            if ([[finalwritedict objectForKey:@"country"] length] > 2)
                countryp = [NSString stringWithFormat:@"%@",[finalwritedict objectForKey:@"country"]];
            
            
            if ([[finalwritedict objectForKey:@"street"]length]>2) {
                strretp = [finalwritedict objectForKey:@"street"];
                
            }
          
            
            if (![[finalwritedict objectForKey:@"housenumber"] isKindOfClass:[NSNull class]] &&  ![[finalwritedict objectForKey:@"housenumber"] isEqualToString:@"(null)"] && ![[finalwritedict objectForKey:@"housenumber"] isEqualToString:@""] && [[finalwritedict objectForKey:@"housenumber"] length] > 0) {
                housenumberp = [finalwritedict objectForKey:@"housenumber"];
                
                strretp = [NSString stringWithFormat:@"%@,%@",strretp,housenumberp];
                
            }
            
            
            if ([[finalwritedict objectForKey:@"zipcode"] length]>2) {
                zipCode = [NSString stringWithFormat:@"%@", [finalwritedict objectForKey:@"zipcode"] ];
                
            }
            
            if ([[finalwritedict objectForKey:@"email"] length]>2) {
                petEmail = [finalwritedict objectForKey:@"email"];
                
            }
            
            
            if ([[finalwritedict objectForKey:@"id"] intValue]) {
                
                if ([[finalwritedict objectForKey:@"mobile_num"] length] > 2)
                    mobilephone = [NSString stringWithFormat:@"%@ %@",[finalwritedict objectForKey:@"mobile_pre"],[[[finalwritedict objectForKey:@"mobile_num"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""]];
                
                if ([[finalwritedict objectForKey:@"phone_num"] length] > 2)
                    homephone = [NSString stringWithFormat:@"%@ %@",[finalwritedict objectForKey:@"phone_pre"],[[[finalwritedict objectForKey:@"phone_num"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""]];
                if ([[finalwritedict objectForKey:@"city"] length] > 2)
                    cityp = [NSString stringWithFormat:@"%@",[finalwritedict objectForKey:@"city"]];
                if ([[finalwritedict objectForKey:@"country"] length] > 2)
                    countryp = [NSString stringWithFormat:@"%@",[finalwritedict objectForKey:@"country"]];
                
                
                if ([[finalwritedict objectForKey:@"street"]length]>2) {
                    strretp = [finalwritedict objectForKey:@"street"];
                    
                }
                
                
                if (![[finalwritedict objectForKey:@"housenumber"] isKindOfClass:[NSNull class]] &&  ![[finalwritedict objectForKey:@"housenumber"] isEqualToString:@"(null)"] && ![[finalwritedict objectForKey:@"housenumber"] isEqualToString:@""] && [[finalwritedict objectForKey:@"housenumber"] length] > 0) {
                    housenumberp = [finalwritedict objectForKey:@"housenumber"];
                    
                    strretp = [NSString stringWithFormat:@"%@,%@",strretp,housenumberp];
                    
                }
                
                
                if ([[finalwritedict objectForKey:@"zipcode"] length]>2) {
                    zipCode = [NSString stringWithFormat:@"%@", [finalwritedict objectForKey:@"zipcode"] ];
                    
                }
                
                if ([[finalwritedict objectForKey:@"email"] length]>2) {
                    petEmail = [finalwritedict objectForKey:@"email"];
                    
                }
                
                
            }
            
            ABRecordRef pet = ABPersonCreate();
            //ABRecordSetValue(pet, kABPersonFirstNameProperty, (__bridge CFStringRef)petFirstName, nil);
           // ABRecordSetValue(pet, kABPersonLastNameProperty, (__bridge CFStringRef)petLastName, nil);
            ABRecordSetValue(pet, kABPersonCompositeNameFormatFirstNameFirst, (__bridge CFStringRef)petfullname, nil);
            
            ABMutableMultiValueRef phoneNumbers = ABMultiValueCreateMutable(kABMultiStringPropertyType);
            ABMultiValueAddValueAndLabel(phoneNumbers, (__bridge CFStringRef)workphone, kABWorkLabel, NULL);
            ABMultiValueAddValueAndLabel(phoneNumbers, (__bridge CFStringRef)homephone, kABHomeLabel, NULL);
            ABMultiValueAddValueAndLabel(phoneNumbers, (__bridge CFStringRef)mobilephone, kABPersonPhoneMobileLabel, NULL);
            ABRecordSetValue(pet, kABPersonPhoneProperty, phoneNumbers, nil);
            ABPersonSetImageData(pet, (__bridge CFDataRef)petImageData, nil);
            
            
            /////////////////////// Email & Address Adding /////////////////
            
            
            ABMutableMultiValueRef emails = ABMultiValueCreateMutable(kABMultiStringPropertyType);
            ABMultiValueAddValueAndLabel(emails,  (__bridge CFStringRef)petEmail, kABWorkLabel, NULL);
            
            ABRecordSetValue(pet, kABPersonEmailProperty, emails, nil);
            
            //CFRelease(emails);
            
            
            //////////////////////// Adding Address //////////////////////////////
            
            ABMutableMultiValueRef multiAddress1 = ABMultiValueCreateMutable(kABMultiDictionaryPropertyType);
            
            NSMutableDictionary *addressDictionary2 = [[NSMutableDictionary alloc] init];
            [addressDictionary2 setObject:strretp forKey:(NSString *) kABPersonAddressStreetKey];
            [addressDictionary2 setObject:zipCode forKey:(NSString *)kABPersonAddressZIPKey];
            
            [addressDictionary2 setObject:cityp forKey:(NSString *)kABPersonAddressCityKey];
            
            [addressDictionary2 setObject:countryp forKey:(NSString *)kABPersonAddressCountryKey];
            //[addressDictionary setObject:@"us" forKey:(NSString *)kABPersonAddressCountryCodeKey];
            
            
            ABMultiValueAddValueAndLabel(multiAddress1, (__bridge CFTypeRef)(addressDictionary2), kABHomeLabel, NULL);
            
            
            //(@"home address added");
            
            ABRecordSetValue(pet, kABPersonAddressProperty, multiAddress1, nil);
            
            
            NSArray *allADContacts = (__bridge NSArray *)ABAddressBookCopyArrayOfAllPeople(addressBookRef);
            for (id record in allADContacts){
                ABRecordRef thisContact = (__bridge ABRecordRef)record;
                
                if (ABRecordCopyCompositeName(thisContact) != NULL)
                {
                    if (CFStringCompare(ABRecordCopyCompositeName(thisContact),
                                        (__bridge CFStringRef)(petfullname), 0) == kCFCompareEqualTo){
                        DebugLog(@"The contact already exists!  %@", (__bridge CFStringRef)(petfullname));
                        
                        
                        NSString *searchName = (__bridge NSString *)((__bridge CFStringRef)(petfullname));
                        ABAddressBookRef addressbook = ABAddressBookCreate();
                        CFStringRef nameRef = (__bridge CFStringRef) searchName;
                        CFArrayRef  allSearchRecords = ABAddressBookCopyPeopleWithName(addressbook, nameRef);
                        
                        
                        traverselooptocheck =0;
                    }
                    else
                    {
                    }
                }
            }
            
            if (traverselooptocheck == 0)
            {
                ABAddressBookAddRecord(addressBookRef, pet, nil);
                ABAddressBookSave(addressBookRef, nil);
            }
            
        }
    }
    
    [blackView removeFromSuperview];
    [restore_table reloadData];

    DebugLog(@"RESTORED");
    
}

- (void)delAllContacts
{
    
//    ABAddressBookRef addressBook = CFBridgingRetain((__bridge id)(ABAddressBookCreateWithOptions(NULL, NULL)));
//    long count = ABAddressBookGetPersonCount(addressBook);
//    if(count==0 && addressBook!=NULL) { //If there are no contacts, don't delete
//        //CFRelease(addressBook);
//        return;
//    }
//    //Get all contacts and store it in a CFArrayRef
//    CFArrayRef theArray = ABAddressBookCopyArrayOfAllPeople(addressBook);
//    for(CFIndex i=0;i<count;i++) {
//        ABRecordRef person = CFArrayGetValueAtIndex(theArray, i); //Get the ABRecord
//        BOOL result = ABAddressBookRemoveRecord (addressBook,person,NULL); //remove it
//        if(result==YES) { //if successful removal
//            BOOL save = ABAddressBookSave(addressBook, NULL); //save address book state
//            if(save==YES && person!=NULL) {
//                //CFRelease(person);
//            } else {
//                NSLog(@"Couldn't save, breaking out");
//                break;
//            }
//        } else {
//            NSLog(@"Couldn't delete, breaking out");
//            break;
//        }
//    }
//    if(addressBook!=NULL) {
//        //CFRelease(addressBook);
//    }
    
    ABAddressBookRef addressBook = ABAddressBookCreate();
    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
    
    int peopleCount = CFArrayGetCount(allPeople);
    CFErrorRef *error = nil;
    for (int i = 0; i < peopleCount; i++){
        ABRecordRef person = CFArrayGetValueAtIndex(allPeople, i);
        error = nil;
        ABAddressBookRemoveRecord(addressBook, person, error);
    }
    error = nil;
    ABAddressBookSave(addressBook, error);
    
    CFRelease(allPeople);
    CFRelease(addressBook);
    
    DebugLog(@"DELETION DONE");
    
}

-(void)Indexing
{
    
#pragma mark -- this is the Beginning of NSLOCALIZEDINDEXEDCOLLATION SECTION - Bhaswar 16/7/2014
    
    // create a dictionary to store an array of objects for each section
    NSMutableDictionary *tempSections = [NSMutableDictionary dictionary];
    
    // iterate through each dictionaey in the list, and put them into the correct section
    for (NSDictionary *item in con_array)
    {
        // get the index of the section (Assuming the table index is showing A-#)
        NSInteger indexName = [[UILocalizedIndexedCollation currentCollation] sectionForObject:[item valueForKey:@"fullname"] collationStringSelector:@selector(description)];
        
        NSNumber *keyName = [NSNumber numberWithInteger:indexName];
        
        // if an array doesnt exist for the key, create one
        NSMutableArray *arrayName = [tempSections objectForKey:keyName];
        if (arrayName == nil)
        {
            arrayName = [NSMutableArray array];
        }
        
        // add the dictionary to the array (add the actual value as we need this object to sort the array later)
        [arrayName addObject: item];   //[item valueForKey:@"fullname"]];
        
        // put the array with new object in, back into the dictionary for the correct key
        [tempSections setObject:arrayName forKey:keyName];
    }
    
    /* now to do the sorting of each index */
    
    NSMutableDictionary *sortedSections = [NSMutableDictionary dictionary];
    
    // sort each index array (A..Z)
    [tempSections enumerateKeysAndObjectsUsingBlock:^(id key, id array, BOOL *stop)
     {
         // sort the array - again, we need to tell it which selctor to sort each object by
         NSArray *sortedArray = [[UILocalizedIndexedCollation currentCollation] sortedArrayFromArray:array collationStringSelector:@selector(description)];
         NSSortDescriptor *sort =[NSSortDescriptor sortDescriptorWithKey:@"fullname" ascending:YES selector:@selector(caseInsensitiveCompare:)];
         sortedArray=[sortedArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];
         [sortedSections setObject:[NSMutableArray arrayWithArray:sortedArray] forKey:key];
     }];
    
    // set the global sectioned dictionary
    indexedSections = sortedSections;
    //            DebugLog(@"indexedsections: %@",indexedSections);
    
    //                                                            AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //                                del.arr_contacts_ad= [app_contacts mutableCopy];
    //                                del.newarr_contacts_ad = [app_contacts mutableCopy];
    
    NSMutableArray *fresh_ban = [[NSMutableArray alloc]init];
    [fresh_ban addObject:indexedSections];
    
    [[NSUserDefaults standardUserDefaults]setObject:[NSKeyedArchiver archivedDataWithRootObject:fresh_ban] forKey:@"IndexedContactslocaldb"];
    NSData *data_SavedArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"IndexedContactslocaldb"];
    if (data_SavedArray != nil)
    {
        NSArray *oldSavedArray = [NSKeyedUnarchiver unarchiveObjectWithData:data_SavedArray];
        DebugLog(@"CONTACT LOCAL DB DB:%@",oldSavedArray);
    }
    
    DebugLog(@"CONTACT LOCAL indexedsections:%@",indexedSections);
    DebugLog(@"CONTACT LOCAL fresh_ban:%@",fresh_ban);
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:con_array ] forKey:@"Contactslocaldb"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    // DebugLog(@"conarray mutablecopy count: %ld", (unsigned long)[del.arr_contacts_ad count]);
    //                        [del loadContactsIntoDB:con_array];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //1
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"data.plist"];
    
    NSMutableArray *data = [[NSMutableArray alloc]init] ;
    data= [con_array mutableCopy];
    DebugLog(@"bbbbb data: %lu",(unsigned long)[data count]);
    [data writeToFile: path atomically:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
