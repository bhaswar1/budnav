//
//  AddMemberViewController.m
//  Budnav
//
//  Created by ios on 21/09/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "AddMemberViewController.h"
#import "GroupInfoAdminViewController.h"
#import "GroupDetailsViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

@interface AddMemberViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIAlertViewDelegate>
{
    UIView *backview,*mainview,*grayView,*searchview;
    UITableView *searchtablefront,*searchtableback;
    UIImageView *logo_img,*bck_img,*crossimage;
    UIButton *backbtn,*invite,*crossbutton;
    UILabel *back_label,*lbdivider,*nouserlabel;
    UITextField *searchfield;
    NSData *data1;
    NSMutableDictionary *temp;
    NSString *search_name;
    
    NSDictionary *groupnames,*searchnames,*array;
    NSMutableArray *groupmember_array,*array_copy,*arraymenu_admin,*arrayadmin_imagename,*array_imagename,*array_menu,*array_id,*hld,*search_result;
    NSString *finalsearch_result;
    UIActivityIndicatorView *activity;
    
}
@end

@implementation AddMemberViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    backview=[[UIView alloc]initWithFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    [backview setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:backview];
    
    UIView *coverview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64)];
    [backview addSubview:coverview];
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, coverview.frame.size.height);
    
    // Add the gradient to the view
    [coverview.layer insertSublayer:gradient atIndex:0];
    
    bck_img = [[UIImageView alloc]initWithFrame:CGRectMake(15,32,12,20)];//(13, 39, 10, 22)];
    bck_img.image=[UIImage imageNamed:@"back3"];
    [self.view addSubview:bck_img];
    
    logo_img = [[UIImageView alloc]init];
    logo_img.frame = CGRectMake(27+([UIScreen mainScreen].bounds.size.width-160)/2, 32, 101, 20);
    logo_img.backgroundColor = [UIColor clearColor];
    
    logo_img.image = [UIImage imageNamed:@"new_logo"];
    [coverview addSubview:logo_img];
    
    back_label=[[UILabel alloc]initWithFrame:CGRectMake(31, 28, 58, 30)];
    back_label.text=@"Back";
    back_label.font=[UIFont fontWithName:@"ProximaNova-Regular" size:20.0];
    back_label.textColor=[UIColor whiteColor];
    [coverview addSubview:back_label];
    
    backbtn=[[UIButton alloc]initWithFrame:CGRectMake(0.0f,0.0f,74,64)];
    [backbtn setBackgroundColor:[UIColor clearColor]];
    [coverview addSubview:backbtn];
    [backbtn addTarget:self action:@selector(backtoprev) forControlEvents:UIControlEventTouchUpInside];
    
    if (search_result==nil) {
        search_result =[[NSMutableArray alloc]init];
    }
    
    invite=[[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width-70, 29, 65, 30)];
    [invite setTitle:@"Invite" forState:UIControlStateNormal];
    invite.backgroundColor=[UIColor clearColor];
    invite.titleLabel.font=[UIFont fontWithName:@"ProximaNova-Regular" size:20.0];
    if (search_result.count==0) {
        [invite setEnabled:NO];
    }
    
    
    [invite addTarget:self action:@selector(invite:) forControlEvents:UIControlEventTouchUpInside];
    [coverview addSubview:invite];
    
    mainview = [[UIView alloc]initWithFrame:CGRectMake(0,123, self.view.frame.size.width, self.view.frame.size.height-123)];
    mainview.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:mainview];
    
    grayView=[[UIView alloc]initWithFrame:CGRectMake(0, 65, self.view.frame.size.width, 51)];
    //    grayView.backgroundColor=[UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1.0];
    grayView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:grayView];
    
    UIView *addView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 11, 31)];;
    
    //    searchBar1 = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
    //    searchBar1.delegate=self;
    //
    //    searchBar1.tintColor=[UIColor darkGrayColor];
    //    searchBar1.backgroundColor=[UIColor whiteColor];
    //    [searchBar1 setSearchBarStyle:UISearchBarStyleMinimal];
    //    //    searchBar.barTintColor= [UIColor colorWithRed:238.0f/255.0f green:238.0f/255.0f blue:238.0f/255.0f alpha:1];
    //    searchBar.barTintColor = [UIColor whiteColor];
    //
    //    UILabel *lbdivider = [[UILabel alloc]initWithFrame:CGRectMake(0, searchBar1.frame.origin.y + searchBar1.frame.size.height, self.view.frame.size.width, 0.6f)];
    //    [grayView addSubview:lbdivider];
    //    lbdivider.backgroundColor = [UIColor colorWithRed:238.0f/255.0f green:238.0f/255.0f blue:238.0f/255.0f alpha:1];
    //
    //
    //    //searchBar.placeholder= @"Search new contacts";
    //    UITextField *searchField = [searchBar valueForKey:@"_searchField"];
    //    //    searchField.frame = CGRectMake(10, 10, [UIScreen mainScreen].bounds.size.width, 50);
    //    searchField.textColor = [UIColor colorWithRed:(0.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0f];
    //    searchField.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f];
    //
    //    NSAttributedString *str = [[NSAttributedString alloc] initWithString:@"Search contacts" attributes:@{ NSForegroundColorAttributeName : [UIColor blueColor]}];//colorWithRed:(0.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:0.7f] }];
    //    searchField.attributedPlaceholder = str;
    //    searchBar1.keyboardAppearance=UIKeyboardAppearanceDark;
    //    [grayView addSubview:searchBar1];
    //    searchBar1.userInteractionEnabled = YES;
    //    searchBar1.searchTextPositionAdjustment = UIOffsetMake(10, 0);
    
    searchfield = [[UITextField alloc] initWithFrame:CGRectMake(10, 10, [UIScreen mainScreen].bounds.size.width-20, 31)];
    searchfield.delegate=self;
    searchfield.autocorrectionType=UITextAutocorrectionTypeNo;
    searchfield.tintColor=[UIColor darkGrayColor];
    //    searchfield.backgroundColor=[UIColor colorWithRed:41.0f/255.0f green:42.0f/255.0f blue:46.0f/255.0f alpha:1.0f];
    //    searchfield.placeholder= @"Search Contacts";
    //searchField.textColor = [UIColor colorWithRed:(0.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0f];
    searchfield.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f];
    // searchfield.backgroundColor = [UIColor whiteColor];
    searchfield.keyboardAppearance=UIKeyboardAppearanceDark;
    searchfield.layer.cornerRadius=5;
    searchfield.clearButtonMode=UITextFieldViewModeWhileEditing;
    searchfield.leftView=addView;
    searchfield.leftViewMode=UITextFieldViewModeAlways;
    searchfield.font=[UIFont fontWithName:@"ProximaNova-Regular" size:14];
    searchfield.textColor = [UIColor colorWithRed:(0.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0f];
    //searchfield.backgroundColor = [UIColor colorWithRed:(43.0f/255.0f) green:(44.0f/255.0f) blue:(48.0f/255.0f) alpha:1.0f];
    
    lbdivider = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 0.6f)];
    [mainview addSubview:lbdivider];
    lbdivider.backgroundColor = [UIColor colorWithRed:238.0f/255.0f green:238.0f/255.0f blue:238.0f/255.0f alpha:1];
    
    NSAttributedString *str = [[NSAttributedString alloc] initWithString:@"Search Contacts" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:(0.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:0.8f] }];
    searchfield.attributedPlaceholder = str;
    //    searchfield.keyboardAppearance=UIKeyboardAppearanceDark;
    
    [grayView addSubview:searchfield];
    searchfield.tag=2;
    
    searchtableback=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, mainview.frame.size.height)];
    searchtableback.delegate=self;
    searchtableback.dataSource=self;
    searchtableback.tag=4;
    searchtableback.separatorStyle=UITableViewCellSeparatorStyleNone;
    searchtableback.backgroundColor=[UIColor clearColor];
    searchtableback.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    //searchtableback.hidden=YES;
    [mainview addSubview:searchtableback];
    
    
    searchview=[[UIView alloc]initWithFrame:CGRectMake(10, 0, mainview.frame.size.width-20, 162)];
    searchview.backgroundColor=[[UIColor whiteColor]colorWithAlphaComponent:1.0];
    //searchview.backgroundColor=[UIColor clearColor];
    searchview.opaque=NO;
    searchview.layer.cornerRadius=0;
//    searchview.layer.shadowOpacity=0.8;
//    searchview.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    searchview.hidden=YES;
    
    
    searchtablefront=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, searchview.frame.size.width, searchview.frame.size.height)];
    searchtablefront.delegate=self;
    searchtablefront.dataSource=self;
    searchtablefront.tag=3;
    searchtablefront.backgroundColor=[UIColor whiteColor];
    searchtablefront.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    searchtablefront.separatorStyle = UITableViewCellSeparatorStyleNone;
    searchtablefront.hidden=YES;
    
    nouserlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 30, [UIScreen mainScreen].bounds.size.width, 30)];
    nouserlabel.text=@"No results";
    nouserlabel.backgroundColor=[UIColor clearColor];
    nouserlabel.textAlignment=NSTextAlignmentCenter;
    nouserlabel.textColor=[UIColor blackColor];//colorWithRed:238.0f/255.0f green:238.0f/255.0f blue:238.0f/255.0f alpha:1];
    nouserlabel.hidden=YES;
    
    [mainview addSubview:searchview];
    // Do any additional setup after loading the view.
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField==searchfield)
    {
        [textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
}

-(void)textFieldDidChange :(UITextField *)theTextField
{
    if([theTextField.text isEqualToString:@""])
        
    {
        
        searchtablefront.hidden=YES;
        
        searchview.hidden=YES;
        
        nouserlabel.hidden=YES;
        
        searchtableback.hidden = NO;
    }
    
    
    NSLog( @"text changed: %@", theTextField.text);
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *groups_url =[NSString stringWithFormat:@"https://budnav.com/ext/?action=search&q=%@&access_token=%@&device_id=%@",[theTextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
    DebugLog(@"groups url string ------ %@",groups_url);
    
    NSError *error1=nil;
    data1=[NSData dataWithContentsOfURL:[NSURL URLWithString:groups_url]options:NSDataReadingUncached error:&error1];
    if (data1==nil)
    {
        DebugLog(@"no connnnn");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                                        message:nil
                                                       delegate:self
                                              cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        alert.tag=5;
        [alert show];
    }
    if (data1 != nil)
    {
        temp=[[NSMutableDictionary alloc]init];
        temp=[NSJSONSerialization JSONObjectWithData:data1 //1
                                             options:kNilOptions
                                               error:&error1];
        DebugLog(@"-----Groups info ----- %@",temp);
    }
    
    
    searchnames = [temp objectForKey:@"details"];
    NSLog(@"searchnames.count------  %lu",(unsigned long)searchnames.count);
    if (searchnames.count!=0)
    {
        searchtableback.hidden = YES;
        
        array=[searchnames objectForKey:@"search"];
        hld=[[NSMutableArray alloc] init];
        array_copy=[array mutableCopy];
        DebugLog(@"aaaaa %lu",(unsigned long)array_copy.count);
        
        for (int i=0; i<array_copy.count; i++) {
            if ([[[array_copy objectAtIndex:i] objectForKey:@"isgroup"] intValue]!=1)
            {
                //hld=[[NSMutableArray alloc] init];
                [hld addObject:[array_copy objectAtIndex:i]];
                
            }
            
        }
        [array_copy removeAllObjects];
        array_copy=[hld mutableCopy];
        DebugLog(@"aaaaa %@",array_copy);
        [searchview addSubview:searchtablefront];
        [searchtablefront reloadData];
        searchview.hidden=NO;
        
        if ([array_copy count] ==0)
        {
            [array_copy removeAllObjects];
            [searchview addSubview:nouserlabel];
            searchtablefront.hidden=YES;
            nouserlabel.hidden=NO;
            // backbutton.tag=5;
        }
        else
        {
            nouserlabel.hidden=YES;
            searchtablefront.hidden=NO;
            // backbutton.tag=5;
        }
        
    }
    else if (searchnames.count==0)
    {
        [searchview addSubview:nouserlabel];
        searchtablefront.hidden=YES;
        nouserlabel.hidden=NO;
        
        searchtableback.hidden = NO;
        // backbutton.tag=5;
    }
    
    //    if(array_copy.count>0)
    //    {
    //        searchtableback.hidden=YES;
    //    }
    
    // backbutton.tag=5;
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section1
{
    if (tableView.tag==3)
    {
        DebugLog(@"%lu",(unsigned long)[[array_copy valueForKey:@"name"] count]);
        return [[array_copy valueForKey:@"name"] count];
    }
    else if (tableView.tag==4)
    {
        return [search_result count];
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // NSString *login_id = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"userid"]];
    
    //[group_array removeAllObjects];
    
    static NSString *MyIdentifier = @"MyReuseIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    cell=nil;
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:MyIdentifier];
    }
    
    if(tableView.tag==3)
    {
        cell.backgroundColor=[UIColor clearColor];
        if (array_copy.count<=3) {
            searchview.frame=CGRectMake(10, 0, mainview.frame.size.width-20, 55*array_copy.count);
        }
        
        
        if (![[[array_copy valueForKey:@"surname"]objectAtIndex:indexPath.row] isKindOfClass:[NSNull class]] && [[array_copy valueForKey:@"surname"]objectAtIndex:indexPath.row] != (id)[NSNull null])
        {
            
            search_name=[NSString stringWithFormat:@"%@ %@",[[array_copy valueForKey:@"name"]objectAtIndex:indexPath.row],[[array_copy valueForKey:@"surname"]objectAtIndex:indexPath.row]];
        }
        if ([[[array_copy valueForKey:@"surname"]objectAtIndex:indexPath.row] isKindOfClass:[NSNull class]] || [[array_copy valueForKey:@"surname"]objectAtIndex:indexPath.row] == (id)[NSNull null])
        {
            search_name=[NSString stringWithFormat:@"%@ ",[[array_copy valueForKey:@"name"]objectAtIndex:indexPath.row]];
        }
        
        UILabel *namelbl = [[UILabel alloc]initWithFrame:CGRectMake(15.0f,5.0f, 250.0f, 30.0f)];
        [namelbl setText:search_name];
        [namelbl setTextAlignment:NSTextAlignmentLeft];
        [namelbl setTextColor:[UIColor blackColor]];
        [namelbl setFont:[UIFont fontWithName:@"ProximaNova-Bold" size:17]];
        [cell addSubview:namelbl];
        
        UILabel *separatorlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 54, [UIScreen mainScreen].bounds.size.width, 1)];
        separatorlabel.backgroundColor=[UIColor lightGrayColor];
        [cell addSubview:separatorlabel];
        
        UIImageView *mapPin = [[UIImageView alloc]init];
        mapPin.frame = CGRectMake(15, 34, (18.0f/2.0f),11);// (25.0f/2.0f));
        
        mapPin.contentMode=UIViewContentModeScaleAspectFit;
        mapPin.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:mapPin];
        
        
        NSString *location;
        if ([[[array_copy objectAtIndex:indexPath.row] objectForKey:@"city"] isKindOfClass:[NSNull class]] || [[array_copy objectAtIndex:indexPath.row] objectForKey:@"city"] == (id)[NSNull null] || [[[array_copy objectAtIndex:indexPath.row] objectForKey:@"city"] length] ==0)
        {
            DebugLog(@"no city");
            if ([[[array_copy objectAtIndex:indexPath.row] objectForKey:@"country"] isKindOfClass:[NSNull class]] || [[array_copy objectAtIndex:indexPath.row] objectForKey:@"country"] == (id)[NSNull null] || [[[array_copy objectAtIndex:indexPath.row] objectForKey:@"country"] length] ==0 || [[NSString stringWithFormat:@"%@",[[array_copy objectAtIndex:indexPath.row] objectForKey:@"city"]] isEqualToString:@""])
            {
                DebugLog(@"no country");
            }
            else
                location= [NSString stringWithFormat:@"%@",[[array_copy objectAtIndex:indexPath.row] objectForKey:@"country"]];
        }
        else if (![[[array_copy objectAtIndex:indexPath.row] objectForKey:@"city"] isKindOfClass:[NSNull class]] || [[array_copy objectAtIndex:indexPath.row] objectForKey:@"city"] != (id)[NSNull null] || [[[array_copy objectAtIndex:indexPath.row] objectForKey:@"city"] length] !=0 ||![[NSString stringWithFormat:@"%@",[[array_copy objectAtIndex:indexPath.row] objectForKey:@"city"]] isEqualToString:@""])
        {
            DebugLog(@"city present");
            if ([[[array_copy objectAtIndex:indexPath.row] objectForKey:@"country"] isKindOfClass:[NSNull class]] || [[array_copy objectAtIndex:indexPath.row] objectForKey:@"country"] == (id)[NSNull null] || [[[array_copy objectAtIndex:indexPath.row] objectForKey:@"country"] length] ==0)
            {
                DebugLog(@"no country");
                location= [NSString stringWithFormat:@"%@",[[array_copy objectAtIndex:indexPath.row] objectForKey:@"city"]];
            }
            else
                location= [NSString stringWithFormat:@"%@,%@",[[array_copy objectAtIndex:indexPath.row] objectForKey:@"city"],[[array_copy objectAtIndex:indexPath.row] objectForKey:@"country"]];
        }
        
        else
            location= [NSString stringWithFormat:@"%@,%@",[[array_copy objectAtIndex:indexPath.row] objectForKey:@"city"],[[array_copy objectAtIndex:indexPath.row] objectForKey:@"country"]];
        
        
        UILabel *locationlb = [[UILabel alloc]initWithFrame:CGRectMake(26, 30, 210, 20)];
        [cell.contentView addSubview:locationlb];
        locationlb.backgroundColor=[UIColor clearColor];
        locationlb.text=location;
        locationlb.textColor=[UIColor colorWithRed:153.0f/255.0f green:153.0f/255.0f blue:153.0f/255.0f alpha:1];
        locationlb.font=[UIFont fontWithName:@"ProximaNova-Regular" size:13];
        locationlb.textAlignment=NSTextAlignmentLeft;
        
        if (location) {
            mapPin.image = [UIImage imageNamed:@"Map Marker Contacts Screen copy2x.png"];
        }
    }
    
    else if (tableView.tag==4)
    {
        cell.backgroundColor = [UIColor clearColor];
        finalsearch_result=[NSString stringWithFormat:@"%@ %@",[[search_result valueForKey:@"name"]objectAtIndex:indexPath.row],[[search_result valueForKey:@"surname"]objectAtIndex:indexPath.row]];
        UILabel *namelbl = [[UILabel alloc]initWithFrame:CGRectMake(15.0f,5.0f, 280.0f, 30.0f)];
        [namelbl setText:finalsearch_result];
        [namelbl setTextAlignment:NSTextAlignmentLeft];
        [namelbl setTextColor:[UIColor blackColor]];
        [namelbl setFont:[UIFont fontWithName:@"ProximaNova-Bold" size:17]];
        [cell addSubview:namelbl];
        
        crossimage=[[UIImageView alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-35, 19, 15, 15)];
        crossimage.image=[UIImage imageNamed:@"cross.png"];
        crossimage.userInteractionEnabled=YES;
        [cell addSubview:crossimage];
        
        crossbutton=[[UIButton alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-45, 10, 40, 35)];
        [crossbutton setTitle:nil forState:UIControlStateNormal];
        //        crossbutton.backgroundColor=[UIColor redColor];
        [crossbutton addTarget:self action:@selector(cross:) forControlEvents:UIControlEventTouchUpInside];
        crossbutton.userInteractionEnabled=YES;
        crossbutton.tag=indexPath.row;
        [cell addSubview:crossbutton];
        
        UILabel *separatorlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 54, [UIScreen mainScreen].bounds.size.width, 1)];
        separatorlabel.backgroundColor=[UIColor lightGrayColor];
        [cell addSubview:separatorlabel];
    }
    return  cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
        return 55.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
if (tableView.tag==3)
{
    
    [invite setEnabled:YES];
    invite.tag=indexPath.row;
    searchview.hidden=YES;
    searchview.userInteractionEnabled=YES;
    [searchtablefront scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    searchfield.text=@"";
    // searchfield.s
    [searchfield resignFirstResponder];
    

    int count = 0;
    
    for (NSDictionary *tempDict in search_result)
        
    {
        
        if ([[tempDict objectForKey:@"id"] intValue] == [[[array_copy objectAtIndex:invite.tag] objectForKey:@"id"]intValue])
            
        {
            
            count++;
            
            break;
            
        }
        
    }
    
    
    
    if (count==0)
        
    {
        
        [search_result addObject:[array_copy objectAtIndex:invite.tag]];
        
    }
    
    
    
  
//    else
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Name Already Exist!"
//                                                        message:nil
//                                                       delegate:self
//                                              cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
//        [alert show];
//    }
    DebugLog(@"copied array: %@",search_result);
    NSLog(@"New array...%@",array_copy);
    searchtableback.hidden=NO;
    [searchtableback reloadData];
    
    
}
    
    if (tableView.tag==4)
    {
        
        [invite setEnabled:YES];
        invite.tag=indexPath.row;
        searchview.hidden=YES;
        searchview.userInteractionEnabled=YES;
        [searchtablefront scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
        searchfield.text=@"";
        // searchfield.s
        [searchfield resignFirstResponder];
     //   [search_result addObject:[search_result objectAtIndex:invite.tag]];
      //  DebugLog(@"copied array: %@",search_result);
        searchtableback.hidden=NO;
        [searchtableback reloadData];
        
    }
}

-(void)cross:(UIButton *)sender
{
    //DebugLog(@"index path %ld %@",sender.tag,search_result);
    [search_result removeObjectAtIndex:sender.tag];
    //[[groupnames valueForKey:@"surname"] removeObjectAtIndex:sender.tag];
    //[self.numbers removeObjectAtIndex:indexPath.row];
    // Delete the row from the data source
    NSIndexPath *indexpath=[NSIndexPath indexPathForRow:sender.tag inSection:0];
    [searchtableback beginUpdates];
    [searchtableback deleteRowsAtIndexPaths:[[NSArray alloc] initWithObjects:indexpath, nil] withRowAnimation:
     UITableViewRowAnimationFade];
    // DebugLog(@"index path %ld %@",sender.tag,search_result);
    [searchtableback endUpdates];
    [searchtableback reloadData];
    if (search_result.count==0) {
        [invite setEnabled:NO];
    }    //    [groupstable endUpdates];
    //[groupstable deleteRowsAtIndexPaths:@[indexpath] withRowAnimation:UITableViewRowAnimationAutomatic];
    //[self performSelector:@selector(tableView:commitEditingStyle:forRowAtIndexPath:) withObject:indexpath];
    //[groupstable reloadData];
}


-(void)invite:(id)sender
{
    [lbdivider removeFromSuperview];
    [UIView animateWithDuration:0.0 animations:^{
        
        activity=[[UIActivityIndicatorView alloc] init];
        activity.center = self.view.center;
        [activity startAnimating];
        activity.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        [activity setColor:[UIColor blackColor]];
        [mainview addSubview:activity];
        
        
    }
                     completion:^(BOOL finished)
     {
    
         NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
         array_id =[[search_result valueForKey:@"id"] mutableCopy];
         DebugLog(@"123654789");
        // DebugLog(@"member count1%@",member_count);
         
         NSString *str=[array_id componentsJoinedByString:@","];
         DebugLog(@"string append %@",str);
         NSString *groups_url =[NSString stringWithFormat:@"https://budnav.com/ext/?action=group-request&group_id=%@&user_id=%@&access_token=%@&device_id=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"group_id"],str,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
         DebugLog(@"groups url string ------ %@",groups_url);
         
         NSError *error1=nil;
             
             NSData *data2=[NSData dataWithContentsOfURL:[NSURL URLWithString:groups_url]options:NSDataReadingUncached error:&error1];
             if (data2==nil)
             {
                 DebugLog(@"no connnnn");
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                                                 message:nil
                                                                delegate:self
                                                       cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                 alert.tag=5;
                 [alert show];
                 
                 
                 
             }
             
             if (data2 != nil)
             {
                 _grpdict=[[NSDictionary alloc] init];
                 _grpdict=[NSJSONSerialization JSONObjectWithData:data2 //1
                                                         options:kNilOptions
                                                           error:&error1];
                 DebugLog(@"-----Groups info123456  ----- %@",_grpdict);
                 
                 NSString *check=[NSString stringWithFormat:@"%@",[_grpdict valueForKey:@"success"]];
                 if ([check isEqualToString:@"1"])
                 {
                     [activity removeFromSuperview];
                     
//                      NSString *type=[NSString stringWithFormat:@"%@",[[_grpdict valueForKey:@"details"] valueForKey:@"type"]];
//                        
//                         if ([type isEqualToString:@"add"]||[type isEqualToString:@"request"])
//                         {
                            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Action" message:@"Request sent" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                             
                             [alert show];
                             
                            
                      //  }
                }
                 
                 else
                 {
                     [activity removeFromSuperview];
                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Action" message:[_grpdict objectForKey:@"error"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     
                     [alert show];
                 }
                
               
             }
         
     }];
    
}

- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == [alertView cancelButtonIndex])
    {
        [self backtoprev];
//        GroupInfoAdminViewController *info = [[GroupInfoAdminViewController alloc]init];
//            [self.navigationController pushViewController:info animated:YES];
        
     //   GroupDetailsViewController *info1 = [[GroupDetailsViewController alloc]init];
     //   [self.navigationController pushViewController:info1 animated:YES];

    }
    else
    {
        //reset clicked
    }
}

-(void)backtoprev
{
    NSLog(@"okkk");
    [self.navigationController popViewControllerAnimated:YES];
    //[self.navigationController dismissViewControllerAnimated:YES completion:Nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
