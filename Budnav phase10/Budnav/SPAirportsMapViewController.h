//  Superpin Airports Example
//  http://getsuperpin.com/
//
//  Copyright (c) 2011-2014 appscape. All rights reserved.

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <Superpin/Superpin.h>

@interface SPAirportsMapViewController : UIViewController
@property (nonatomic,weak) IBOutlet SPMapView *mapView;
@end
