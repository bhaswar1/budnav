//  ConFullSettingsView.m
//  Contacter
//  Created by Bhaswar's MacBook Air on 20/03/15.
//  Copyright (c) 2014 Esolz. All rights reserved.
#import "ConFullSettingsViewController.h"
#import "AppDelegate.h"
#import "ConLoginViewController.h"
#import "ConStartViewController.h"
#import "ConAccountSettingsViewController.h"
#import "ConAddBackUpViewController.h"
#import <AddressBookUI/AddressBookUI.h>
#import <AddressBook/AddressBook.h>
#import "SVProgressHUD.h"
#import "MDRadialProgressView.h"
#import "MDRadialProgressTheme.h"
#import "MDRadialProgressLabel.h"
#import "ConNewContactsViewController.h"
#import "ConHomeViewController.h"
#import "ConProfileOwnViewController.h"
#import "ConQROptionsViewController.h"
#import "ConInviteViewController.h"
#import "ConSyncLoaderViewController.h"
#import "ConBusinessViewController.h"
#import "ConPersonalProfileViewController.h"
#import "ConSocialMediaViewController.h"
#import "SocialWebViewController.h"
#import "ConRestoreBackupViewController.h"
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Twitter/Twitter.h>
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"


@interface ConFullSettingsViewController () <FBSDKSharingDelegate,FBSDKSharingDialog>
{
    UIImageView *logo_img;
    int b_count, s_count;
    AppDelegate *del;
    UIView *background;
    ABAddressBookRef addressBookRef;
}
@end

@implementation ConFullSettingsViewController
@synthesize length,cell_objects,about_cell_objects,itemsTable,about_Table,mainScroll,i,table_view,about_sep,aboutview,move, moveparent,mainview,topbar,myTextField,pass,con_array,final_con_array,filtered_arr, con_array1,add_contacts_dict,app_contacts, check_app_cont, phonesarr, check_app_cont1, backuparr, testarr,addtophonebook, existingphn,uniquearray, part_array,responseData,logoimg;

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"From ConFullSettingsViewController");
    i=0;
   
    del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    
    cell_objects = [[NSMutableArray alloc] initWithObjects:@"Account settings",@"Personal profile",@"Business profile",@"Social media",@"",@"Invite contacts to Budnav",@"Synchronize contacts",@"Restore backup",nil];
    //[cell_objects addObject:@"Restore backup"];
    about_cell_objects = [[NSMutableArray alloc] init];
    
    self.view.backgroundColor = [UIColor clearColor];
    
    UIView *top_bar=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    // background.backgroundColor=[UIColor blackColor];
    [self.view addSubview:top_bar];
    
    
    background = [[UIView alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height)];
    background.backgroundColor=[UIColor colorWithRed:(238.0f/255.0f) green:(238.0f/255.0f) blue:(238.0f/255.0f) alpha:1.0f];
    [self.view addSubview:background];
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, top_bar.frame.size.width, top_bar.frame.size.height);
    
    // Add the gradient to the view
    [top_bar.layer insertSublayer:gradient atIndex:0];
    
    
    logo_img = [[UIImageView alloc]init];
    logo_img.frame = CGRectMake(27+([UIScreen mainScreen].bounds.size.width-160)/2, 32, 101, 20);
    logo_img.backgroundColor = [UIColor clearColor];
    
    logo_img.image = [UIImage imageNamed:@"new_logo"];
    [top_bar addSubview:logo_img];
    
    UIActivityIndicatorView *act = [[UIActivityIndicatorView alloc] init];
    act.center=self.view.center;//WithFrame:frame];
    //act.layer.zPosition=1;
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [act setColor:[UIColor blackColor]];
    //[background addSubview:act];
    [act startAnimating];
    
    
    
    UILabel *heading = [[UILabel alloc]initWithFrame:CGRectMake(0, 23, top_bar.frame.size.width, 37)];
   // [top_bar addSubview:heading];
    heading.backgroundColor=[UIColor clearColor];
    heading.text=@"SETTINGS";
    heading.font=[UIFont fontWithName:@"ProximaNova-Regular" size:15.5f];
    heading.textColor=[UIColor whiteColor];
    heading.textAlignment=NSTextAlignmentCenter;
    
   
}

-(void)viewDidAppear:(BOOL)animated
{
    [del showTabStatus:YES];
    [del showTabValues:YES];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    self.navigationController.navigationBarHidden=YES;
    
    UIActivityIndicatorView *act = [[UIActivityIndicatorView alloc] init];
    act.center=background.center;//WithFrame:frame];
    //act.layer.zPosition=1;
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [act setColor:[UIColor blackColor]];
    //[background addSubview:act];
    [act startAnimating];

    self.screenName = @"Settings";
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"loadedProfDict"] isKindOfClass:[NSNull class]] || [[NSUserDefaults standardUserDefaults] objectForKey:@"loadedProfDict"]==(id)[NSNull null] || !([[NSUserDefaults standardUserDefaults] objectForKey:@"loadedProfDict"])) {
        
        
        [del ownDetails];
        [act removeFromSuperview];
    }
    else
    {
        [act removeFromSuperview];
        DebugLog(@"LOADED DATA:%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"loadedProfDict"]);
        del.personal_dict = [[NSMutableDictionary alloc]init];
        del.business_dict = [[NSMutableDictionary alloc]init];
        del.social_dict = [[NSMutableDictionary alloc]init];\
        
        del.profdict = [[NSMutableDictionary alloc]init];
        
        del.profdict = [[[NSUserDefaults standardUserDefaults] objectForKey:@"loadedProfDict"] mutableCopy];
        
        NSDictionary *dict_profile = [del.profdict objectForKey:@"profile"];
        DebugLog(@"array gives: %@",dict_profile);
        for( NSString *aKey in [dict_profile allKeys])
        {
            // DebugLog(@"AKEY:%@",aKey);
            NSString *newString = [aKey substringToIndex:2];
            //DebugLog(@"%@", newString);
            if ([newString isEqualToString:@"b_"])
            {
                [del.business_dict setValue:[dict_profile valueForKey:[NSString stringWithFormat:@"%@",aKey]] forKey:[NSString stringWithFormat:@"%@",aKey]];
                //[[NSUserDefaults standardUserDefaults] setObject:business_dict forKey:@"loadedBusiness"];
                
            }
            else if ([newString isEqualToString:@"s_"])
            {
                [del.social_dict setValue:[dict_profile valueForKey:[NSString stringWithFormat:@"%@",aKey]] forKey:[NSString stringWithFormat:@"%@",aKey]];
                
            }
            else
            {
                [del.personal_dict setValue:[dict_profile valueForKey:[NSString stringWithFormat:@"%@",aKey]] forKey:[NSString stringWithFormat:@"%@",aKey]];
                
            }
        }
        [act removeFromSuperview];
        
    }

    
     b_count = 0;
    s_count = 0;
    DebugLog(@"BUSINESS:%@",del.business_dict);
    DebugLog(@"SOCIAL:%@",del.social_dict);
    for (NSString *str in del.business_dict) {
        
        DebugLog(@"STRING OF DICT:%@",[del.business_dict valueForKey:str]);
        if (![[del.business_dict valueForKey:str] isKindOfClass:[NSNull class]] && ![[del.business_dict valueForKey:str] isEqual:@"(null)"] && ![[del.business_dict valueForKey:str] isEqual:@"0"]) {
            
            if (![[del.business_dict valueForKey:str] isKindOfClass:[NSNumber class]] && [[del.business_dict valueForKey:str] length] > 0)
            {
            b_count++;
                DebugLog(@"B COUNT:%@ %d",str, b_count);
            }
            else
            {
                DebugLog(@"string else if: %@ %@", str, [del.business_dict valueForKey:str]);
            }
        }
    }
    
    DebugLog(@"B COUNT:%d",b_count);
    for (NSString *str in del.social_dict) {
        
        //DebugLog(@"STRING OF DICT:%@",[del.social_dict objectForKey:str]);
        if (![[del.social_dict valueForKey:str] isKindOfClass:[NSNull class]] && ![[del.social_dict valueForKey:str] isEqual:@"(null)"] && ![[del.social_dict valueForKey:str] isEqual:@"0"] && [[del.social_dict valueForKey:str] length]>0) {
            s_count++;
        }
    }
    
    if (itemsTable) {
        [itemsTable removeFromSuperview];
    }
    
    itemsTable = [[UITableView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width,self.view.frame.size.height)];
    [itemsTable setBackgroundColor:[UIColor clearColor]];
    [itemsTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    itemsTable.delegate= self;
    [itemsTable setDataSource:self];
    itemsTable.userInteractionEnabled=YES;
    itemsTable.scrollEnabled=YES;
    [background addSubview:itemsTable];
    
    mainScroll.contentSize = CGSizeMake(0,table_view.frame.size.height+aboutview.frame.size.height+about_sep.frame.size.height);
    [act removeFromSuperview];
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [itemsTable reloadData];
    self.navigationController.delegate = nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat row_height;
    if (indexPath.row==4) {
        row_height = 35;
    }
    else
        row_height=50;
    
    return row_height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 8;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell==nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
//    UIImage *image;
//    switch (indexPath.row) {
//        case 0:
//            image=[UIImage imageNamed:@"profile_white.png"];
//            break;
//        case 1:
//            image=[UIImage imageNamed:@"scan_white.png"];
//            break;
//        case 2:
//            image = [UIImage imageNamed:@"profile_white.png"];
//            break;
//        case 3:
//            image = [UIImage imageNamed:@"scan_white.png"];
//            break;
//        case 4:
//            image = [UIImage imageNamed:@"account-settings.png"];
//            break;
//        case 5:
//            image = [UIImage imageNamed:@"sync.png"];
//            break;
//        case 6:
//            image = [UIImage imageNamed:@"inviting-friend.png"];
//            break;
//        case 7:
//            image = [UIImage imageNamed:@"rate-in-app-store.png"];
//            break;
//        case 8:
//            image = [UIImage imageNamed:@"sign-out.png"];
//            break;
//    }
    
    UILabel *title_label = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, cell.frame.size.width-50, 50)];
    title_label.backgroundColor = [UIColor clearColor];
    title_label.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17.0f];
    title_label.textColor = [UIColor blackColor];
    title_label.text = [cell_objects objectAtIndex:indexPath.row];
    title_label.textAlignment = NSTextAlignmentLeft;
    [cell addSubview:title_label];
    
    
    UIImageView *arrow = [[UIImageView alloc] init];
    arrow.backgroundColor = [UIColor clearColor];
    arrow.image = [UIImage imageNamed:@"grey_arrow"];
    [cell addSubview:arrow];
    
    UIImageView *excla = [[UIImageView alloc] init];
    excla.backgroundColor = [UIColor clearColor];
    excla.image = [UIImage imageNamed:@"blank_info"];
    [cell addSubview:excla];
    
    UIView *sep = [[UIView alloc] init];
    //sep.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"divider.png"]];
    sep.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f];
    [cell addSubview:sep];
    
     if ([[cell_objects objectAtIndex:indexPath.row] length]==0) {
        
        arrow.frame = CGRectMake(0, 0, 0, 0);
        
         [cell setBackgroundColor:[UIColor clearColor]];
        sep.frame = CGRectMake(0, 34, self.view.frame.size.width, 0.6f);
    }
    else
    {
        arrow.frame = CGRectMake(self.view.frame.size.width-28, 18, 8, 13);
        sep.frame = CGRectMake(0, 49, self.view.frame.size.width, 0.6f);
        
    }
    
    if (indexPath.row==2 && b_count==0) {
        
        DebugLog(@"B_COUNT:%d",b_count);
        excla.frame = CGRectMake(self.view.frame.size.width-62, 12.5, 24, 24);
        
    }
    if (indexPath.row==3 && s_count==0) {
        DebugLog(@"S_COUNT:%d",s_count);
        excla.frame = CGRectMake(self.view.frame.size.width-62, 12.5, 24, 24);
        
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    DebugLog(@"indexpath.row = %ld",(long)indexPath.row);
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row] forKey:@"whichpage"];
    
    if(indexPath.row==2)
    {
        ConBusinessViewController *con = [[ConBusinessViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    
    if(indexPath.row==1)
    {
        ConPersonalProfileViewController *con = [[ConPersonalProfileViewController alloc]init];
        con.toUpdateInfo = @"";
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    
    if(indexPath.row==0)
    {
        ConAccountSettingsViewController *con = [[ConAccountSettingsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    
    if(indexPath.row==6)
    {
        [[GAI sharedInstance].defaultTracker send:
         [[GAIDictionaryBuilder createEventWithCategory:@"Settings"
                                                 action:@"Synchronise contacts"
                                                  label:nil
                                                  value:nil] build]];
        
        
        ////////////////////////////////// Getting Contacts From Device Phonebook ////////////////////////////////////
        
        CFErrorRef *errorab = NULL;
//        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, errorab);
        addressBookRef = ABAddressBookCreateWithOptions(NULL, errorab);
        
        __block BOOL accessGranted = NO;
        if (&ABAddressBookRequestAccessWithCompletion != NULL) { // we're on iOS 6
            dispatch_semaphore_t sema = dispatch_semaphore_create(0);
            ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
                accessGranted = granted;
                dispatch_semaphore_signal(sema);
            });
            dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
            //        dispatch_release(sema);
        }
        else { // we're on iOS 5 or older
            accessGranted = YES;
        }
        if (accessGranted) {

            NSLog(@"hchecbejbcj");
        ConSyncLoaderViewController *con = [[ConSyncLoaderViewController alloc]init];
       [self presentViewController:con animated:YES completion:nil];
      }
    }
    if(indexPath.row==3)
    {
        
        ConSocialMediaViewController *con = [[ConSocialMediaViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    if(indexPath.row==5)
    {
        [[GAI sharedInstance].defaultTracker send:
         [[GAIDictionaryBuilder createEventWithCategory:@"Settings"
                                                 action:@"Invite contacts to Budnav"
                                                  label:nil
                                                  value:nil] build]];
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select an option" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Email",@"Message",@"Whatsapp",@"Twitter",@"Facebook", nil];
            
            actionSheet.tag = 101;
            [actionSheet showInView:mainview];
        }
    if(indexPath.row==7)
    {
        [[GAI sharedInstance].defaultTracker send:
         [[GAIDictionaryBuilder createEventWithCategory:@"Settings"
                                                 action:@"Restore backup"
                                                  label:nil
                                                  value:nil] build]];
        ConRestoreBackupViewController *con = [[ConRestoreBackupViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    
 
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 101){
        
        if (buttonIndex == 0) {
            [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
            DebugLog(@"EMAIL");
            ConInviteViewController *con = [[ConInviteViewController alloc] init];
            con.request_check = @"email";
//            CATransition* transition = [CATransition animation];
//            transition.duration = 0.4;
//            transition.type = kCATransitionPush;
//            transition.subtype = kCATransitionFade;
//            
//            [[self navigationController].view.layer addAnimation:transition forKey:nil];
            
            [self.navigationController pushViewController:con animated:YES];

        }
        else if (buttonIndex == 1)
        {
            [actionSheet dismissWithClickedButtonIndex:1 animated:YES];
            DebugLog(@"MESSAGE");
            ConInviteViewController *con = [[ConInviteViewController alloc] init];
            con.request_check = @"message";
//            CATransition* transition = [CATransition animation];
//            transition.duration = 0.4;
//            transition.type = kCATransitionPush;
//            transition.subtype = kCATransitionFade;
//            
//            [[self navigationController].view.layer addAnimation:transition forKey:nil];
            
            [self.navigationController pushViewController:con animated:YES];
        }
        else if (buttonIndex == 2)
        {
            [actionSheet dismissWithClickedButtonIndex:1 animated:YES];
            DebugLog(@"WHATSAPP");
            
            NSString * urlWhats = [NSString stringWithFormat:@"whatsapp://send?text=Discover Budnav - The Smart Phone Book. Download now https://budnav.com/dl/"];
            NSURL * whatsappURL = [NSURL URLWithString:[urlWhats stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            if ([[UIApplication sharedApplication] canOpenURL: whatsappURL])
            {
                [[UIApplication sharedApplication] openURL: whatsappURL];
            }
            else
            {
                UIAlertView * alert1 = [[UIAlertView alloc] initWithTitle:@"WhatsApp not installed." message:@"Your device has no WhatsApp installed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert1 show];
            }
            
            [itemsTable reloadData];
        }
        else if (buttonIndex == 3)
        {
            [actionSheet dismissWithClickedButtonIndex:1 animated:YES];
            DebugLog(@"TWITTER");
            [self sendTwitter];

            
            [itemsTable reloadData];
            
           
        }
        else if (buttonIndex == 4)
        {
            [actionSheet dismissWithClickedButtonIndex:1 animated:YES];
                        DebugLog(@"FACEBOOK");
            [self facebookPost];
            
            
            [itemsTable reloadData];
            
        }
        else{
            
            [itemsTable reloadData];
            NSLog(@"dismissWithClickedButtonIndex");
            [actionSheet dismissWithClickedButtonIndex:5 animated:NO];
        }
        
    }
    
}



-(void)yesSuccess{
    
    NSString *dict_details = [jsonparsed objectForKey:@"success"];
    
    DebugLog(@" dict detaisls %@",dict_details);
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    if([dict_details intValue]== 1)
    {
        NSDictionary * dict = [prefs dictionaryRepresentation];
        for (id key in dict) {
            
            [prefs removeObjectForKey:key];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"isloggedin"];
        [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"autologin"];
        [prefs synchronize];
        
    }
    
    [actincir stopAnimating];
    [actincir removeFromSuperview];
}


-(void)failure{
    
    
    [actincir stopAnimating];
    [actincir removeFromSuperview];
}

- (void) runSpinAnimationOnView:(UIImageView *)view duration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat;
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ * rotations * duration ];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = repeat;
    
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data
{
    [responseData appendData:data];
    float progress =  (float)[responseData length]/(float)self.length;
    DebugLog(@"progress: %ld",(unsigned long)[responseData length]);
    DebugLog(@" percentage: %f",progress);
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    DebugLog(@"connection failed");
    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"backupstatus"];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    DebugLog(@"connection finished loading");
    
    DebugLog(@"response data - %@", [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
    
    NSError *err;
    
    //    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString1]];
    
    if (responseData != nil)
    {
        NSDictionary  *json1aa=[NSJSONSerialization JSONObjectWithData:responseData options:0 error:&err];
        
        DebugLog(@"this now has json string url: %@",json1aa);
        
        if ([[json1aa objectForKey:@"success"] intValue] ==1)
        {
            [logoimg removeFromSuperview];
            //             [SVProgressHUD showSuccessWithStatus:@"Success!"];
            [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"backupstatus"];
        }
        else
        {
            [logoimg removeFromSuperview];
            NSString *err_str = [json1aa objectForKey:@"error"];
            DebugLog(@"error backing up: %@",err_str);
        }
        //            [SVProgressHUD showSuccessWithStatus:@"Success!"];
    }
    else
    {
        [logoimg removeFromSuperview];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                                        message:nil
                                                       delegate:self
                                              cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        alert.tag=7;
        //        [alert show];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    responseData = [NSMutableData data];
    self.length = [response expectedContentLength];
    DebugLog(@"expectd length= %f",self.length);
}

-(NSString*) encodeToPercentEscapeString:(NSString *)string {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                              (CFStringRef) string,
                                                              NULL,
                                                              (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
}

// Decode a percent escape encoded string.
-(NSString*) decodeFromPercentEscapeString:(NSString *)string {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL,
                                                                              (CFStringRef) string,
                                                                              CFSTR(""),
                                                                              kCFStringEncodingUTF8));
}


-(void) myMethod:(myCompletion) compblock{
    //do stuff
    DebugLog(@"do the stuff in this block");
    @autoreleasepool {
        
        //    UIBackgroundTaskIdentifier BGIdentifier = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{}];
        testarr = [existingphn mutableCopy];
        addtophonebook = [[NSMutableArray alloc]init];
        int dev, apps;
        NSString *conhome,*conwork, *conmobile;
        //            NSMutableDictionary *testdict;
        int testback;
        
        for (apps=0; apps< [backuparr count]; apps++)
        {
            testback = 0;
            NSDictionary *getbackupdict = [backuparr objectAtIndex:apps];
            NSString *phnstr = [NSString stringWithFormat:@"%@",[[backuparr objectAtIndex:apps]objectForKey:@"mobile_num"]];
            NSArray *backarrnumbers = [phnstr componentsSeparatedByString:@","];
            //                    DebugLog(@"backarrnumbers: %@",backarrnumbers);
            int ph_int;
            ph_int = 0;
            //                    testdict = [testarr objectAtIndex:dev];
            
            for (ph_int =0; ph_int< [backarrnumbers count]; ph_int++)
            {
                NSString *finalph = [backarrnumbers objectAtIndex:ph_int];
                finalph = [[finalph componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
                if ([finalph length] > 6)
                    finalph = [finalph substringFromIndex:[finalph length] - 6];
                
                //                        DebugLog(@"finalphone: %@ , == %ld",finalph,(long)[finalph intValue]);
                
                for (dev=0; dev< [testarr count]; dev++)
                {
                    conhome = [NSString stringWithFormat:@"%@",[[testarr objectAtIndex:dev]objectForKey:@"home"]];
                    conhome = [[conhome componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
                    if ([conhome length] > 6)
                        conhome = [conhome substringFromIndex:[conhome length] - 6];
                    conmobile = [NSString stringWithFormat:@"%@",[[testarr objectAtIndex:dev]objectForKey:@"mobile"]];
                    conmobile = [[conmobile componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
                    
                    if ([conmobile length] > 6)
                        conmobile = [conmobile substringFromIndex:[conmobile length] - 6];
                    
                    conwork = [NSString stringWithFormat:@"%@",[[testarr objectAtIndex:dev]objectForKey:@"work"]];
                    conwork = [[conwork componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
                    if ([conwork length] > 6)
                        conwork = [conwork substringFromIndex:[conwork length] - 6];
                    
                    if (([conhome intValue] != 0 && [conhome intValue] != 2147483647 && [finalph intValue] != 0 && [finalph intValue] != 2147483647 && [conhome rangeOfString:finalph].location != NSNotFound) || ([conmobile intValue] != 0 && [conmobile intValue] != 2147483647 && [finalph intValue] != 0 && [finalph intValue] != 2147483647 && [conmobile rangeOfString:finalph].location != NSNotFound) ||  ([conwork intValue] != 0 && [conwork intValue] != 2147483647 && [finalph intValue] != 0 && [finalph intValue] != 2147483647 && [conwork rangeOfString:finalph].location != NSNotFound))
                    {
                        testback=0;
                        break;
                    }
                    else
                    {
                        testback=1;
                        continue;
                    }
                    if (testback == 0)
                        break;
                }
                if (testback == 0)
                    break;
            }
            
            if (testback ==1)
            {
                [addtophonebook addObject:getbackupdict];
                //                 break;
            }
        }
        
        if ([testarr count] == 0)
            addtophonebook = [backuparr mutableCopy];
        
        //        [[UIApplication sharedApplication] endBackgroundTask:BGIdentifier];
    }
    compblock(YES);
}


- (void)facebookPost
{
    
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    
    //content.readPermissions = @[@"public_profile", @"email",@"user_photos",@"user_friends"];
    [content setContentTitle:@"Budnav"];
    
    [content setContentDescription:@"Budnav - The Smart Phone Book. Download now https://budnav.com/dl/"];
    
    [content setContentURL:[NSURL URLWithString:@"http://www.budnav.com"]];
   // [content setImageURL:[NSURL URLWithString:@""]];
    
    [FBSDKShareDialog showFromViewController:self
                                 withContent:content
                                    delegate:self];

}


- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results
{
    NSLog(@"%@",results);
    
    if([results count]==0)
    {
    }
    else{
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                  @"Share" message:@"Message shared!" delegate:self
                                                 cancelButtonTitle:@"ok" otherButtonTitles: nil];
        alertView.delegate=self;
        [alertView show];
    }
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer
{
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error
{
}

-(void)sendTwitter
{
    
    TWTweetComposeViewController *tweetComposeViewController =
    [[TWTweetComposeViewController alloc] init];
    [tweetComposeViewController setInitialText:@"Budnav - The Smart Phone Book. Download now https://budnav.com/dl/"];
    [tweetComposeViewController setCompletionHandler:^(TWTweetComposeViewControllerResult result) {
        [self dismissModalViewControllerAnimated:YES];
    }];
    [self presentModalViewController:tweetComposeViewController animated:YES];
    
    
}

@end