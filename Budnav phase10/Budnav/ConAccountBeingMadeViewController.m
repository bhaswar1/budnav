//
//  ConAccountBeingMadeViewController.m
//  Contacter
//  Created by Bhaswar's MacBook Air on 07/05/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
#import "ConAccountBeingMadeViewController.h"
#import "ConSuccessRegViewController.h"
#import "ConLoginViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

@interface ConAccountBeingMadeViewController ()
{
    NSTimer *timerr;
    UIAlertView *alert;
    NSMutableData *responseData;
    NSString *err_str;
}
@end

@implementation ConAccountBeingMadeViewController
@synthesize portraitImg;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
    
    UILabel *picoptionslb = [[UILabel alloc]initWithFrame:CGRectMake(0, 195, self.view.bounds.size.width, 55)];
    [self.view addSubview:picoptionslb];
    picoptionslb.backgroundColor=[UIColor clearColor];
    picoptionslb.text=@"Your account is being made...";
    picoptionslb.numberOfLines=2;
    picoptionslb.textColor=[UIColor colorWithRed:40.0f/255.0f green:82.0f/255.0f blue:135.0f/255.0f alpha:1];
    picoptionslb.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17.5f];
    picoptionslb.textAlignment=NSTextAlignmentCenter;
    
    CGRect frame = CGRectMake(110,225,[UIScreen mainScreen].bounds.size.width-220,100);
    UIActivityIndicatorView *act = [[UIActivityIndicatorView alloc] initWithFrame:frame];
    [act startAnimating];
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [act setColor:[UIColor grayColor]];
    [self.view addSubview:act];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    timerr = [NSTimer scheduledTimerWithTimeInterval:3.0
                                              target:self
                                            selector:@selector(targetMethod:)
                                            userInfo:nil
                                             repeats:YES];
    
    //    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    //    dispatch_async(q, ^{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=register&name=%@&surname=%@&email=%@&mobile_pre=%@&mobile_num=%@&password=%@&street=%@&housenumber=%@&zipcode=%@&city=%@&country=%@&device_id=%@&device_type=ios",[prefs objectForKey:@"r_fname"],[[prefs objectForKey:@"r_lname"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[prefs objectForKey:@"r_email"],[prefs objectForKey:@"r_mobile_pre"],[prefs objectForKey:@"r_mobile_num"],[prefs objectForKey:@"r_password"],[[prefs objectForKey:@"r_street"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[prefs objectForKey:@"r_housenumber"],[prefs objectForKey:@"r_zipcode"],[[prefs objectForKey:@"r_city"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[[prefs objectForKey:@"r_country"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
    
    DebugLog(@"deny url: %@",urlString1);
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    //    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    //
    //    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    //
    //    [request setHTTPShouldHandleCookies:NO];
    //
    //    [request setURL:[NSURL URLWithString:newString1]];
    //
    //    [request setTimeoutInterval:100];
    //
    //    [request setHTTPMethod:@"POST"];
    //
    //    NSData *imageData = UIImagePNGRepresentation(portraitImg);
    ////    if ( [imageData length] > 0)
    //        //   if (pic_send ==1)
    ////    {
    //        DebugLog(@"hmm portrait img achhe");
    //        NSString *boundary = [NSString stringWithFormat:@"%0.9u",arc4random()];
    //
    //        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    //
    //        [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    //
    //        NSMutableData *body = [NSMutableData data];
    //
    //        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    //
    //        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image\"; filename=\".jpg\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    //
    //        [body appendData:[[NSString stringWithFormat:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    //
    //        [body appendData:[NSData dataWithData:UIImagePNGRepresentation(portraitImg)]];
    //
    //        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    //
    //        [request setHTTPBody:body];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:newString1]];
    
    NSString *params = [[NSString alloc] initWithFormat:@"image=%@",[[[NSUserDefaults standardUserDefaults] objectForKey:@"r_prof_image"] stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"]];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if(theConnection)
    {
        responseData = [NSMutableData data];
    }
    else
    {
    }
    //    }
    
    //
    //    NSURLResponse *response = nil;
    //    NSError *error= nil;
    //    NSData *signeddataURL1 = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
    });
    //    });
    
    //       else
    //    {
    //        DebugLog(@"image upload failed");
    //    }
    
    //    [self editpic];
}

-(void)targetMethod: (NSTimer *)timer
{
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)yesSuccess
{
    ConSuccessRegViewController *con= [[ConSuccessRegViewController alloc]init];
    [self.navigationController pushViewController:con animated:NO];
    [timerr invalidate];
}

-(NSString*) encodeToPercentEscapeString:(NSString *)string {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                              (CFStringRef) string,
                                                              NULL,
                                                              (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
}



- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data
{
    [responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    DebugLog(@"connection failed");
    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"backupstatus"];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    DebugLog(@"connection finished loading");
    
    DebugLog(@"response data - %@", [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
    NSError *error;
    
    if (responseData != nil)
    {
        NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:responseData //1
                                                                  options:kNilOptions
                                                                    error:&error];
        DebugLog(@"deny json returns: %@",json_deny);
        
        NSString *errornumber= [NSString stringWithFormat:@"%@",[json_deny objectForKey:@"errorno"]];
        DebugLog(@"err  %@",errornumber);
        if (![errornumber isEqualToString:@"0"])
        {
            err_str = [json_deny objectForKey:@"error"];
            [self performSelectorOnMainThread:@selector(errorsignup) withObject:nil waitUntilDone:YES];
            ConLoginViewController *con = [[ConLoginViewController alloc]init];
            [self.navigationController pushViewController:con animated:NO];
        }
        else
        {
            if ([[json_deny objectForKey:@"success"]intValue] == 1)
            {
                //        [self loadrequestsdata];
                //        tab_request =Nil;
                //        [tab_request reloadData];
                
                [self performSelectorOnMainThread:@selector(yesSuccess)
                                       withObject:nil
                                    waitUntilDone:YES];
                
                NSDictionary *dict_details = [json_deny objectForKey:@"details"];
                
                NSString *access_token = [NSString stringWithFormat:@"%@",[dict_details objectForKey:@"access_token"]];
                NSString *userid = [NSString stringWithFormat:@"%@",[dict_details objectForKey:@"id"]];
                DebugLog(@"access token ret  %@  %@",access_token,userid);
                
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                [prefs setObject:userid forKey:@"userid"];
                [prefs setObject:access_token forKey:@"access_token"];
                [prefs setObject:@"yes" forKey:@"isloggedin"];
                [prefs setObject:@"yes" forKey:@"addbackup"];
                [prefs setObject:@"yes" forKey:@"copycontacts"];
                [prefs setObject:@"yes" forKey:@"firstRequest"];
                [prefs setObject:@"yes" forKey:@"firstContacts"];
                [prefs setObject:@"yes" forKey:@"firstMap"];
                [prefs setObject:@"yes" forKey:@"firstProfile"];
                
                [prefs synchronize];
            }
        }
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    responseData = [NSMutableData data];
}

-(void)errorsignup
{
    alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                       message:err_str
                                      delegate:self
                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
    [alert show];
    
}
@end