#import "ConSyncLoaderViewController.h"
#import "MDRadialProgressView.h"
#import "MDRadialProgressTheme.h"
#import "MDRadialProgressLabel.h"
#import "SVProgressHUD.h"
#import "ConSettingsView.h"
#import "AppDelegate.h"
#import "ConNewContactsViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

@interface ConSyncLoaderViewController ()<NSURLConnectionDelegate,NSURLConnectionDataDelegate>
{
    UILabel *syncstatus;
    int number_of_iterations,successTag,connectionsuccess,connectiondoublefilter, count_incre;
    NSData *decodedData;
    UIImage *profilepic;
    NSMutableArray *double_array, *newcontactsapp, *backup_array;
    NSDictionary *indexedSections, *json;
    NSUserDefaults *prefs;
    ABAddressBookRef addressBookRef;
    NSMutableData *responseData;
    float time_interval;
    BOOL checkIfFinished;
    UIAlertView *alert;
}
@end
@implementation ConSyncLoaderViewController
@synthesize length,cell_objects,about_cell_objects,itemsTable,about_Table,mainScroll,i,table_view,about_sep,aboutview,move,moveparent,mainview,topbar,myTextField,pass,con_array,final_con_array,filtered_arr,con_array1,add_contacts_dict,app_contacts,check_app_cont, phonesarr,check_app_cont1, backuparr,testarr,addtophonebook,existingphn,uniquearray,part_array,logoimg,phnstr,finalph,getbackupdict,backarrnumbers,conhome,conwork,conmobile,ns;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {}
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (objFirstViewController:) name:@"ContactsLoaded" object:nil];
    
    checkIfFinished = NO;
}


-(void)memoryfunction
{
    //DebugLog(@"Memory warning function called");
    addtophonebook = nil;
    backuparr = nil;
    phnstr = nil;
    finalph = nil;
    conhome = nil;
    conmobile = nil;
    conwork = nil;
    testarr = nil;
    getbackupdict = nil;
}
-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(memoryfunction) name:@"applicationDidReceiveMemoryWarning" object:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    
    prefs = [NSUserDefaults standardUserDefaults];
    
    DebugLog(@"ConSyncLoaderViewController");
    successTag = 0;
    number_of_iterations = 0;
    connectionsuccess = 0;
    connectiondoublefilter =0;
    // responseData = [NSMutableData new];
    uniquearray = [[NSMutableArray alloc]init];
    
    self.view.backgroundColor=[UIColor blackColor];
    
    UIView *whiteview = [[UIView alloc]initWithFrame:CGRectMake(55, 150, 210, 210)];
    [self.view addSubview:whiteview];
    whiteview.backgroundColor=[UIColor clearColor];
//    UIImageView *logo_img = [[UIImageView alloc]initWithFrame:CGRectMake((self.view.frame.size.width-92.5)/2, 31, 92.5f, 16)];
//    logo_img.image=[UIImage imageNamed:@"logomod.png"];
 //   UIImageView *logo_img = [[UIImageView alloc] initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-252)/2, [UIScreen mainScreen].bounds.size.height/9.2, 252, 87)];
    UIImageView *logo_img=[[UIImageView alloc]init];
    logo_img.frame = CGRectMake(27+([UIScreen mainScreen].bounds.size.width-160)/2, 32, 101, 20);  //16/09/15
    logo_img.image=[UIImage imageNamed:@"new_logo"];
    [self.view addSubview:logo_img];
    count_incre = 1;
    time_interval = 1.5f;
    radialView3 = [self progressViewWithFrame:CGRectMake(0, 140, 200, 200)];
    radialView3.progressTotal = 100;
    radialView3.progressCounter = 0;
    radialView3.theme.completedColor = [UIColor whiteColor];//colorWithRed:132/255.0 green:23/255.0 blue:23/255.0 alpha:1.0
    radialView3.theme.incompletedColor = [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]; //lightGrayColor
    radialView3.theme.thickness = 9.5f;
    radialView3.theme.sliceDividerHidden = YES;
    radialView3.theme.centerColor = [UIColor clearColor];
    radialView3.center= self.view.center;
    [self.view addSubview:radialView3];
    
    syncstatus = [[UILabel alloc]initWithFrame:CGRectMake(0, self.view.bounds.size.height-90, [UIScreen mainScreen].bounds.size.width, 40)];
    syncstatus.text=@"Backing up...";
    syncstatus.textColor=[UIColor whiteColor];
    syncstatus.textAlignment=NSTextAlignmentCenter;
    syncstatus.font= [UIFont fontWithName:@"ProximaNova-Regular" size:18];
    [self.view addSubview:syncstatus];
    
    timerr = [NSTimer scheduledTimerWithTimeInterval:time_interval
                                              target:self
                                            selector:@selector(targetMethod:)
                                            userInfo:nil
                                             repeats:YES];
    
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
    ///////////////////////////////////////////////     GET BACK UP      /////////////////////////////////////////
    
    @autoreleasepool {
        dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(q, ^{
            [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"FromRestoreToBackup"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSOperationQueue new] addOperationWithBlock:^{
                
                addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
                
                ////////////////////////////////// Getting Contacts From Device Phonebook ////////////////////////////////////
                
                CFErrorRef *errorab = nil;
                ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, errorab);
                
                __block BOOL accessGranted = NO;
                if (&ABAddressBookRequestAccessWithCompletion != NULL) { // we're on iOS 6
                    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
                    ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
                        accessGranted = granted;
                        dispatch_semaphore_signal(sema);
                    });
                    dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
                    //        dispatch_release(sema);
                }
                else { // we're on iOS 5 or older
                    accessGranted = YES;
                }
                if (accessGranted) {
                    
                    NSMutableArray *contactpickarr = [[NSMutableArray alloc]init];
                    ABAddressBookRef UsersAddressBook = ABAddressBookCreateWithOptions(NULL, NULL);
                    
                    //contains details for all the contacts
                    CFArrayRef ContactInfoArray = ABAddressBookCopyArrayOfAllPeople(UsersAddressBook);
                    
                    //get the total number of count of the users contact
                    CFIndex numberofPeople = CFArrayGetCount(ContactInfoArray);
                    // DebugLog(@"%ld",numberofPeople);
                    con_array = [[NSMutableArray alloc]init];
                    
                    //iterate through each record and add the value in the array
                    for (int ik =0; ik<numberofPeople; ik++) {
                        ABRecordRef ref = CFArrayGetValueAtIndex(ContactInfoArray, ik);
                        ABMultiValueRef names, lastnames;
                        if ((ABRecordCopyValue(ref, kABPersonFirstNameProperty) == NULL && ABRecordCopyValue(ref, kABPersonLastNameProperty) == NULL) || ABRecordCopyValue(ref, kABPersonPhoneProperty) == NULL)
                        {
                        }
                        else
                        {
                            names = (__bridge ABMultiValueRef)((__bridge NSString*)ABRecordCopyValue(ref, kABPersonFirstNameProperty));
                            lastnames = (__bridge ABMultiValueRef)((__bridge NSString*)ABRecordCopyValue(ref, kABPersonLastNameProperty));
                            
                            //        DebugLog(@"name from address book = %@ %@",names,lastnames);  // works fine.
                            NSString *fullname;
                            NSString *fnm = [NSString stringWithFormat:@"%@",names];
                            NSString *lnm = [NSString stringWithFormat:@"%@",lastnames];
                            //        //DebugLog(@"fnm : %@",fnm);
                            if ([fnm length] ==0 || [fnm isKindOfClass:[NSNull class]] || fnm == (id)[NSNull null] || [fnm isEqualToString:@"(null)"])
                                fullname = [NSString stringWithFormat:@"%@",lastnames];
                            
                            else if ([lnm length] ==0 || [lnm isKindOfClass:[NSNull class]] || lnm == (id)[NSNull null] || [lnm isEqualToString:@"(null)"])
                                fullname = [NSString stringWithFormat:@"%@",names];
                            else
                                fullname = [NSString stringWithFormat:@"%@ %@",names,lastnames];
                            
                            [contactpickarr addObject:fullname];
                            
                            ABMultiValueRef emailProperty = ABRecordCopyValue(ref, kABPersonEmailProperty);
                            
                            NSArray *emailArray = (__bridge NSArray *)ABMultiValueCopyArrayOfAllValues(emailProperty);
                            NSString *newstringemail= [emailArray componentsJoinedByString:@","];
                            
                            ns = [[NSMutableDictionary alloc]init];
                            
                            [ns setObject:fullname forKey:@"fullname"];
                            [ns setValue:newstringemail forKey:@"email"];
                            
                            if ([newstringemail isKindOfClass:[NSNull class]] || newstringemail == (id)[NSNull null] || [newstringemail length] < 4)
                                [ns setValue:@"" forKey:@"email"];
                            
                            ABRecordID recordID = ABRecordGetRecordID(ref);
                            
                            int useridphn = (int)recordID;
                            
                            //                    NSString *idphn = [NSString stringWithFormat:@"p%d",useridphn];
                            NSString *idphn = [NSString stringWithFormat:@"%d",useridphn];
                            
                            //                        DebugLog(@"recordID 2: %d .. %d %@ %@",recordID,useridphn, idphn,fullname);
                            
                            [ns setValue:idphn forKey:@"id"];
                            
                            ABMultiValueRef phoneNumberProperty = ABRecordCopyValue(ref, kABPersonPhoneProperty);
                            NSArray *_phoneNumbers = (__bridge NSArray*)ABMultiValueCopyArrayOfAllValues(phoneNumberProperty);
                            
                            NSMutableArray *mutableph= [_phoneNumbers mutableCopy];
                            int pp;
                            //                            DebugLog(@"mutable first: %@",mutableph);
                            for (pp =0; pp< [mutableph count]; pp++)
                            {
                                if ([[mutableph objectAtIndex:pp] isKindOfClass:[NSNull class]] || [mutableph objectAtIndex:pp] == (id)[NSNull null] || [[mutableph objectAtIndex:pp] length] < 3)
                                {
                                    [mutableph removeObjectAtIndex:pp];
                                    pp= pp-1;
                                }
                            }
                            //                            DebugLog(@"mutable second: %@",mutableph);
                            NSString *newstringph= [mutableph componentsJoinedByString:@","];
                            
                            // [ns setValue:newstringph forKey:@"phone"];
                            
                            
                            ABMultiValueRef phones = ABRecordCopyValue(ref, kABPersonPhoneProperty);
                            for(CFIndex j = 0; j < ABMultiValueGetCount(phones); j++)
                            {
                                CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(phones, j);
                                CFStringRef locLabel = ABMultiValueCopyLabelAtIndex(phones, j);
                                NSString *phoneLabel =(__bridge NSString*) ABAddressBookCopyLocalizedLabel(locLabel);
                                NSString *phoneNumber = (__bridge NSString *)phoneNumberRef;
                                DebugLog(@"phno:  - %@ (%@)", phoneNumber, phoneLabel);
                                [ns setValue:phoneNumber forKey:phoneLabel];
                            }
                            
                            DebugLog(@"NS NS:%@",ns);
                            
                            ABMultiValueRef stt = ABRecordCopyValue(ref, kABPersonAddressProperty);
                            if (ABMultiValueGetCount(stt) > 0) {
                                CFDictionaryRef dict = ABMultiValueCopyValueAtIndex(stt, 0);
                                //                    DebugLog(@"dict address contacts: %@",dict);
                                NSString *streetp = CFDictionaryGetValue(dict, kABPersonAddressStreetKey);
                                NSString *cityp = CFDictionaryGetValue(dict, kABPersonAddressCityKey);
                                NSString *zipcdep = CFDictionaryGetValue(dict, kABPersonAddressZIPKey);
                                NSString *countryp = CFDictionaryGetValue(dict, kABPersonAddressCountryKey);
                                
                                if ([streetp isKindOfClass:[NSNull class]] || streetp == (id)[NSNull null])
                                    [ns setValue:@"" forKey:@"street"];
                                else
                                    [ns setValue:streetp forKey:@"street"];
                                
                                if ([cityp isKindOfClass:[NSNull class]] || cityp == (id)[NSNull null])
                                    [ns setValue:@"" forKey:@"city"];
                                else
                                    [ns setValue:cityp forKey:@"city"];
                                
                                if ([zipcdep isKindOfClass:[NSNull class]] || zipcdep == (id)[NSNull null])
                                    [ns setValue:@"" forKey:@"zipcode"];
                                else
                                    [ns setValue:zipcdep forKey:@"street"];
                                
                                if ([countryp isKindOfClass:[NSNull class]] || countryp == (id)[NSNull null])
                                    [ns setValue:@"" forKey:@"country"];
                                else
                                    [ns setValue:countryp forKey:@"country"];
                                
                                [ns setValue:@"" forKey:@"housenumber"];
                                
                                if (![ns objectForKey:@"zipcode"]) {
                                    
                                    [ns setValue:@"" forKey:@"zipcode"];
                                }
                                
                                
                            }
                            else
                            {
                                [ns setValue:@"" forKey:@"housenumber"];
                                [ns setValue:@"" forKey:@"city"];
                                [ns setValue:@"" forKey:@"street"];
                                [ns setValue:@"" forKey:@"zipcode"];
                                [ns setValue:@"" forKey:@"country"];
                            }
                            //            } [ns setObject:addressarr forKey:@"address"];
                            
                            for( NSString *aKey in [ns allKeys])
                            {
                                
                                if ([aKey isEqualToString:@"name"]) {
                                    [ns setValue:[ns objectForKey:aKey] forKey:@"fullname"];
                                    [ns removeObjectForKey:aKey];
                                }
                                //                                if ([aKey isEqualToString:@"home"]) {
                                //                                    //                                                                        [changekeydict setValue:[changekeydict objectForKey:aKey] forKey:@"name"];
                                //                                    [ns removeObjectForKey:aKey];
                                //                                }
                                // if ([aKey isEqualToString:@"id"]) {
                                //                                                                        [changekeydict setValue:[changekeydict objectForKey:aKey] forKey:@"name"];
                                //     [ns removeObjectForKey:aKey];
                                // }
                                if ([aKey isEqualToString:@"userids"]) {
                                    //                                                                        [changekeydict setValue:[changekeydict objectForKey:aKey] forKey:@"name"];
                                    [ns removeObjectForKey:aKey];
                                }
                                
                                //                                if ([aKey isEqualToString:@"mobile"]) {
                                //                                    //                                                                        [changekeydict setValue:[changekeydict objectForKey:aKey] forKey:@"name"];
                                //                                    [ns removeObjectForKey:aKey];
                                //                                }
                                
                                //                                if ([aKey isEqualToString:@"work"]) {
                                //                                    //                                                                        [changekeydict setValue:[changekeydict objectForKey:aKey] forKey:@"name"];
                                //                                    [ns removeObjectForKey:aKey];
                                //                                }
                                
                                if ([aKey isEqualToString:@"iPhone"]) {
                                    //                                                                        [changekeydict setValue:[changekeydict objectForKey:aKey] forKey:@"name"];
                                    [ns removeObjectForKey:aKey];
                                }
                                
                            }
                            
                            [con_array addObject:ns];
                        }
                    }
                    
                    
                }
                
                [self NewBackupPoint];
                DebugLog(@"DEVICE CONTACTS ARRAYS:%ld %@",[con_array count],con_array);
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                
                
            }];
            
            
            
            dispatch_async(dispatch_get_main_queue(), ^{}); });
        
#pragma Mark End of Adding back Up to Web
    }
}

- (MDRadialProgressView *)progressViewWithFrame:(CGRect)frame
{
    MDRadialProgressView *view = [[MDRadialProgressView alloc] initWithFrame:frame];
    // Only required in this demo to align vertically the progress views.
    view.center = CGPointMake(self.view.center.x, view.center.y+40);
    return view;
}

-(void)targetMethod: (NSTimer *)timer
{
    NSLog(@"hit test");
    radialView3.progressCounter=radialView3.progressCounter+count_incre;
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"FromRestoreToBackup"] isEqualToString:@"yes"])
    {
        syncstatus.text = @"Backing up...";
    }
    
    if (radialView3.progressCounter == 96)
    {
        if (checkIfFinished == NO)
        {
            NSLog(@"drrrdddddd");
            [timer invalidate];
        }
    }
    
    if (radialView3.progressCounter == 100)
    {
        [timer invalidate];
        [timeragain invalidate];
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"backupstatus"] isEqualToString:@"yes"])
        {
            [SVProgressHUD showSuccessWithStatus:@"Success!"];
            [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"backupstatus"];
            addtophonebook = nil;
            backuparr = nil;
            phnstr = nil;
            finalph = nil;
            conhome = nil;
            conmobile = nil;
            conwork = nil;
            testarr = nil;
            getbackupdict = nil;
        }
        
        
        alert = [[UIAlertView alloc] initWithTitle:@"You have synchronized your contacts and created a new backup successfully!" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        [self dismissViewControllerAnimated:NO completion:nil];
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
        [UIApplication sharedApplication].idleTimerDisabled = NO;
    }
}


-(NSString*) encodeToPercentEscapeString:(NSString *)string {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                              (CFStringRef) string,
                                                              NULL,
                                                              (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    addtophonebook = nil;
    backuparr = nil;
    phnstr = nil;
    finalph = nil;
    conhome = nil;
    conmobile = nil;
    conwork = nil;
    testarr = nil;
    getbackupdict = nil;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    responseData = [[NSMutableData alloc] init];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data
{     [responseData appendData:data];
    responseData = [data mutableCopy];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    DebugLog(@"connection failed");
    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"backupstatus"];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // DebugLog(@"connection finished loading %d", successTag);
    NSString *datatostring = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *urgentdict = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&err];
    DebugLog(@"urgent dict gives: %@",urgentdict);
    
    //    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString1]];
    if (responseData != nil)
    {
        NSDictionary  *json1aa=[NSJSONSerialization JSONObjectWithData:responseData options:0 error:&err];
        DebugLog(@"this now has json string url: %@",json1aa);
        DebugLog(@"BACKUP CREATED");
        // [self getBudnavContact];
        
        //            if([datatostring rangeOfString:@"\"success\":true"].location != NSNotFound)
        //            {
        //                [logoimg removeFromSuperview];
        //                [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"backupstatus"];
        //            }
        //            else
        //            {
        //                [logoimg removeFromSuperview];
        //                NSString *err_str = [json1aa objectForKey:@"error"];
        //                //  DebugLog(@"error backing up: %@",err_str);
        //            }
    }
    else
    {
        [logoimg removeFromSuperview];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                                        message:nil
                                                       delegate:self
                                              cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        alert.tag=7;
    }
    [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"reloadinAppDelegate"];
    
}

//- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
//{
//    responseData = [NSMutableData new];
//}



-(void)compareandcontrast
{
    con_array = [double_array mutableCopy];
    NSArray *prefsarray = (NSArray *)con_array;
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    [def setObject:[NSKeyedArchiver archivedDataWithRootObject:prefsarray] forKey:@"devicefilter"];
    [def synchronize];
    
    check_app_cont = [[NSMutableArray alloc]init];
    testarr = [con_array mutableCopy];
    int apps,dev;
    int test;
    test=0; //*conmain,*coniphone,*conpager,*conother;
    NSDictionary *testdict;
    
    for (dev=0; dev< [testarr count]; dev++)
    {
        conmobile = [[testarr objectAtIndex:dev]objectForKey:@"mobile_num"];
        
        testdict = [testarr objectAtIndex:dev];
        
        for (apps=0; apps< [app_contacts count]; apps++)
        {
            //            if ([[con_array objectAtIndex:dev]objectForKey:@"home"])
            //                {
            NSString *homestr = [[[[app_contacts objectAtIndex:apps]objectForKey:@"mobile_num"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
            
            NSString *landphnstr = [[[[app_contacts objectAtIndex:apps]objectForKey:@"phone_num"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
            
            NSString *b_phn = [[[[app_contacts objectAtIndex:apps]objectForKey:@"b_phone_num"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
            
            if ( ([conmobile longLongValue] != 0 && [homestr longLongValue] != 0 && [conmobile rangeOfString:homestr].location != NSNotFound) || ([conmobile longLongValue] != 0 && [b_phn longLongValue] != 0 && [conmobile rangeOfString:b_phn].location != NSNotFound) || ([conmobile longLongValue] != 0 && [landphnstr longLongValue] != 0 && [conmobile rangeOfString:landphnstr].location != NSNotFound))
            {
                test=0;
                [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"entered"];
                break;
            }
            else
            {
                test=1;
                [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"entered"];
                continue;
            }
        }
        if (test == 1)
            [check_app_cont addObject:testdict];
    }
    
    DebugLog(@"CHECK APP:%@",check_app_cont);
    if ([check_app_cont count]>0) {
        uniquearray = check_app_cont;
    }
    
    //    uniquearray = check_app_cont;
    DebugLog(@"CHECK UNIQUEARRAY:%@",uniquearray);
    
    if ([check_app_cont count] >0 && [con_array count] > 0)
    {
        con_array = [NSMutableArray arrayWithArray:[app_contacts arrayByAddingObjectsFromArray:uniquearray]];
    }
    else if ([app_contacts count] > 0 && [con_array count] == 0)
    {
        con_array =[app_contacts mutableCopy];
    }
    else if ([app_contacts count] == 0 && [con_array count] > 0)
    {
    }
    //            DebugLog(@"now con array is: %@",con_array);
    //-------------------------------------------------------------------------------------------------------------
    
    AppDelegate *del =  (AppDelegate *)[[UIApplication sharedApplication] delegate];
    del.newarr_contacts_ad = [con_array mutableCopy];
    
    
    add_contacts_dict =[[NSMutableDictionary alloc]init];
    //    NSMutableArray *numericcontactsarray = [[NSMutableArray alloc]init];
    for (int ij =0; ij< [con_array count]; ij++)
    {
        NSString *newString = [[[[con_array objectAtIndex:ij] objectForKey:@"fullname"] substringToIndex:1] lowercaseString];
        
        NSCharacterSet *strCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"];//1234567890_"];
        
        strCharSet = [strCharSet invertedSet];
        //And you can then use a string method to find if your string contains anything in the inverted set:
        
        NSRange r = [newString rangeOfCharacterFromSet:strCharSet];
        if (r.location != NSNotFound) {
        }
        else
        {
            if (add_contacts_dict[newString])
            {
                NSMutableArray *checkarr =[[NSMutableArray alloc]init];
                for (NSDictionary *check in [add_contacts_dict objectForKey:newString] )
                {
                    [checkarr addObject:check];
                }
                [checkarr addObject:[con_array objectAtIndex:ij]];
                NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"fullname" ascending:YES selector:@selector(caseInsensitiveCompare:)];
                NSArray *sortedArray=[checkarr sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];
                
                [add_contacts_dict removeObjectForKey:newString];
                [add_contacts_dict setObject:sortedArray forKey:newString];
            }
            else
            {
                NSMutableArray *checkarr1 =[[NSMutableArray alloc]init];
                [checkarr1 addObject:[con_array objectAtIndex:ij]];
                //            [add_contacts_dict setObject:[con_array objectAtIndex:ij] forKey:newString];
                [add_contacts_dict setObject:checkarr1 forKey:newString];
            }
        }
    }
    final_con_array= [[NSMutableArray alloc]init];
    for (NSDictionary *dict in add_contacts_dict)
        [final_con_array addObject:dict];
    
    //////////////////////////////// Localized of Contacts //////////////////////////////////////////////////
    NSMutableDictionary *tempSections = [NSMutableDictionary dictionary];
    
    // iterate through each dictionaey in the list, and put them into the correct section
    for (NSDictionary *item in con_array)
    {
        // get the index of the section (Assuming the table index is showing A-#)
        NSInteger indexName = [[UILocalizedIndexedCollation currentCollation] sectionForObject:[item valueForKey:@"fullname"] collationStringSelector:@selector(description)];
        
        NSNumber *keyName = [NSNumber numberWithInteger:indexName];
        
        // if an array doesnt exist for the key, create one
        NSMutableArray *arrayName = [tempSections objectForKey:keyName];
        if (arrayName == nil)
        {
            arrayName = [NSMutableArray array];
        }
        
        // add the dictionary to the array (add the actual value as we need this object to sort the array later)
        [arrayName addObject: item];   //[item valueForKey:@"fullname"]];
        
        // put the array with new object in, back into the dictionary for the correct key
        [tempSections setObject:arrayName forKey:keyName];
    }
    
    /* now to do the sorting of each index */
    
    NSMutableDictionary *sortedSections = [NSMutableDictionary dictionary];
    
    // sort each index array (A..Z)
    [tempSections enumerateKeysAndObjectsUsingBlock:^(id key, id array, BOOL *stop)
     {
         // sort the array - again, we need to tell it which selctor to sort each object by
         NSArray *sortedArray = [[UILocalizedIndexedCollation currentCollation] sortedArrayFromArray:array collationStringSelector:@selector(description)];
         NSSortDescriptor *sort =[NSSortDescriptor sortDescriptorWithKey:@"fullname" ascending:YES selector:@selector(caseInsensitiveCompare:)];
         sortedArray=[sortedArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];
         [sortedSections setObject:[NSMutableArray arrayWithArray:sortedArray] forKey:key];
     }];
    
    // set the global sectioned dictionary
    indexedSections = sortedSections;
    //            DebugLog(@"indexedsections: %@",indexedSections);
    
    //                            AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    del.arr_contacts_ad= [con_array mutableCopy];
    del.newarr_contacts_ad = [con_array mutableCopy];
    
    [[NSUserDefaults standardUserDefaults]setObject:[NSKeyedArchiver archivedDataWithRootObject:con_array] forKey:@"Contactslocaldb"];
    
    NSMutableArray *fresh_ban = [[NSMutableArray alloc]init];
    [fresh_ban addObject:indexedSections];
    
    [[NSUserDefaults standardUserDefaults]setObject:[NSKeyedArchiver archivedDataWithRootObject:fresh_ban] forKey:@"IndexedContactslocaldb"];
    NSData *data_SavedArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"IndexedContactslocaldb"];
    if (data_SavedArray != nil)
    {
        NSArray *oldSavedArray = [NSKeyedUnarchiver unarchiveObjectWithData:data_SavedArray];
        DebugLog(@"CONTACT LOCAL DB DB:%@",oldSavedArray);
    }
    
    
    // [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:con_array] forKey:@"Contactslocaldb"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

-(void)removeContactWithRecordsList:(CFArrayRef) selectedRecords_ {
    ABAddressBookRef addressbook = ABAddressBookCreate();
    if (selectedRecords_ != NULL)
    {
        int count = CFArrayGetCount(selectedRecords_);
        for (int ijj = 0; ijj < count; ++ijj)
        {
            ABRecordRef contact = CFArrayGetValueAtIndex(selectedRecords_, ijj);
            ABAddressBookRemoveRecord(addressbook, contact, nil);
            DebugLog(@"REMOVED NAME:%@",selectedRecords_);
        }
    }
    ABAddressBookSave(addressbook, nil);
    //CFRelease(addressbook);
}

-(void)NewBackupPoint
{
    
    NSString *backupUrl = [NSString stringWithFormat:@"https://budnav.com/ext/?action=newbackup&backupid=%@&access_token=%@&device_id=%@",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
    
    NSError *errorjs= nil;
    
    NSData *newbakup =[NSData dataWithContentsOfURL:[NSURL URLWithString:backupUrl]];
    
    if (newbakup != nil) {
        
        DebugLog(@"NEW BACKUP URL:%@",backupUrl);
        
        NSDictionary *backup_json = [NSJSONSerialization JSONObjectWithData:newbakup //1
                                                                    options:kNilOptions
                                     
                                                                      error:&errorjs];
        DebugLog(@"this now has json string url: %@",backup_json);
        
        if ([[backup_json objectForKey:@"success"]intValue] == 1)
        {
            DebugLog(@"Backup:%@",backup_json);
            
            
            [[NSOperationQueue new] addOperationWithBlock:^{
                
                
                NSError *err1 = nil;
                
                //     DebugLog(@"unique array count: %lu",(unsigned long)[uniquearray count]);
                
                number_of_iterations = 0;
                part_array = [[NSMutableArray alloc]init];
                if ([con_array count] <= 300)
                {
                    part_array = [con_array mutableCopy];
                    number_of_iterations=1;
                }
                else
                {
                    if ([con_array count] % 300 == 0)
                    {
                        number_of_iterations = (int)[con_array count]/300;
                    }
                    else
                    {
                        number_of_iterations = (int)[con_array count]/300 +1 ;
                    }
                }
                //       DebugLog(@"number of iterations are: %d", number_of_iterations);
                
                for (int numb =0; numb < number_of_iterations; numb++)
                {
                    part_array = [[NSMutableArray alloc]init];
                    
                    if (numb == number_of_iterations -1)
                    {
                        NSArray *tempArray = [con_array subarrayWithRange:NSMakeRange(numb*300, [con_array count]-(300*numb))];
                        [part_array addObjectsFromArray:tempArray];
                    }
                    else
                    {
                        NSArray *tempArray = [con_array subarrayWithRange:NSMakeRange(numb*300, 299)];
                        [part_array addObjectsFromArray:tempArray];
                    }
                    
                    
                    DebugLog(@"number of elements in part array are: %lu %@",(unsigned long)[part_array count], part_array);//========================================================= ADD BACKUP=================================================//
                    
                    NSError *error = nil;
                    NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:(NSArray *)part_array options:NSJSONWritingPrettyPrinted error:&err1];
                    NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
                    NSString *encodedText = [self encodeToPercentEscapeString:jsonString];
                    
                    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=addbackup&access_token=%@&device_id=%@",[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];   //&contacts=%@  ,[jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                    
                    DebugLog(@"addbackuppart url: %@",urlString1);
                    responseData = [NSMutableData data];
                    NSURLResponse *response;
                    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString1]];
                    
                    NSString *params = [[NSString alloc] initWithFormat:@"contacts=%@",encodedText]; //[jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                    DebugLog(@"ADD BACKUP PARAM:%@",params);
                    [request setHTTPMethod:@"POST"];
                    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
                    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                    //            [[NSURLConnection alloc] initWithRequest:request delegate:self];
                    NSData* result = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                    
                    // DebugLog(@"ADD BACKUP:%@",response);
                    
                    
                    //NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
                    
                    //                    if(theConnection)
                    //                    {
                    //                        responseData = [NSMutableData data];
                    //                    }
                    //                    else
                    //                    {
                    //                    }
                    if (result!=nil) {
                        
                        NSDictionary *json_backup = [NSJSONSerialization JSONObjectWithData:result options:kNilOptions error:nil];
                        DebugLog(@"JSON BACKUP:%@",json_backup);
                        
                        //                        NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
                        //                        tempDict = [json_backup objectForKey:@"details"];
                        //
                        //                        NSMutableArray *tempArray = [[NSMutableArray alloc] init];
                        //                       tempArray = [tempDict objectForKey:@"contacts"];
                        //                        DebugLog(@"TEMP ARRAY:%@",tempArray);
                        //                        con_array = [[NSMutableArray alloc] init];
                        //                            for (NSDictionary *dict in tempArray) {
                        //                                [con_array addObject:dict];
                        //                            }
                        //                        [self getBudnavContact];
                        
                    }
                    else
                    {
                        DebugLog(@"CONNECTION FAILED");
                    }
                    
                }
                
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    DebugLog(@"NEW BACKUP POINT CREATED");
                    
                    [self getBackupList];
                    
                }];
                
                
                
                //[self getBudnavContact];
                
            }];
            
            
            //DebugLog(@"NEW BACKUP POINT CREATED");
            //[self createBackup];
            
        }
        else
        {
            //[activ removeFromSuperview];
            UIAlertView *alrt= [[UIAlertView alloc] initWithTitle:nil message:@"Backup not created" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alrt show];
        }
    }
}

//-(void)createBackup
//{
//
//        [[NSOperationQueue new] addOperationWithBlock:^{
//
//
//            NSError *err1 = nil;
//
//            //     DebugLog(@"unique array count: %lu",(unsigned long)[uniquearray count]);
//
//            number_of_iterations = 0;
//            part_array = [[NSMutableArray alloc]init];
//            if ([con_array count] <= 300)
//            {
//                part_array = [con_array mutableCopy];
//                number_of_iterations=1;
//            }
//            else
//            {
//                if ([con_array count] % 300 == 0)
//                {
//                    number_of_iterations = (int)[con_array count]/300;
//                }
//                else
//                {
//                    number_of_iterations = (int)[con_array count]/300 +1 ;
//                }
//            }
//            //       DebugLog(@"number of iterations are: %d", number_of_iterations);
//
//            for (int numb =0; numb < number_of_iterations; numb++)
//            {
//                part_array = [[NSMutableArray alloc]init];
//
//                if (numb == number_of_iterations -1)
//                {
//                    NSArray *tempArray = [con_array subarrayWithRange:NSMakeRange(numb*300, [con_array count]-(300*numb))];
//                    [part_array addObjectsFromArray:tempArray];
//                }
//                else
//                {
//                    NSArray *tempArray = [con_array subarrayWithRange:NSMakeRange(numb*300, 299)];
//                    [part_array addObjectsFromArray:tempArray];
//                }
//
//
//                DebugLog(@"number of elements in part array are: %lu %@",(unsigned long)[part_array count], part_array);//========================================================= ADD BACKUP=================================================//
//
//                NSError *error = nil;
//                NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:(NSArray *)part_array options:NSJSONWritingPrettyPrinted error:&err1];
//                NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
//                NSString *encodedText = [self encodeToPercentEscapeString:jsonString];
//
//                NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=addbackup&access_token=%@&device_id=%@",[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];   //&contacts=%@  ,[jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
//
//                DebugLog(@"addbackuppart url: %@",urlString1);
//                responseData = [NSMutableData data];
//                NSURLResponse *response;
//                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString1]];
//
//                NSString *params = [[NSString alloc] initWithFormat:@"contacts=%@",encodedText]; //[jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
//                DebugLog(@"ADD BACKUP PARAM:%@",params);
//                [request setHTTPMethod:@"POST"];
//                [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
//                [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
//                //            [[NSURLConnection alloc] initWithRequest:request delegate:self];
//                NSData* result = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//                DebugLog(@"ADD BACKUP:%@",response);
//                // NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
//                if (result!=nil) {
//
//                    NSDictionary *json_backup = [NSJSONSerialization JSONObjectWithData:result options:kNilOptions error:nil];
//                    //DebugLog(@"JSON BACKUP:%@",json_backup);
//                }
//                else
//                {
//                    DebugLog(@"CONNECTION FAILED");
//                }
//
//            }
//
//        }];
//
//}

-(void)getBudnavContact
{
    [self performSelectorOnMainThread:@selector(AddBudnavContact) withObject:nil waitUntilDone:YES];
    
    prefs = [[NSUserDefaults standardUserDefaults] init];
    NSError *error = nil;
    NSString *urlString =[NSString stringWithFormat:@"https://budnav.com/ext/?action=contacts&amount=-1&id=%@&access_token=%@&device_id=%@&thumb=true",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
    
    DebugLog(@"contacts -- now the url is : %@",urlString);
    
    NSString *newString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString]];
    
    if (signeddataURL == nil)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        //[act removeFromSuperview];
    }
    else
    {
        json = [NSJSONSerialization JSONObjectWithData:signeddataURL //1
                                               options:kNilOptions
                                                 error:&error];
        DebugLog(@"JSON FIRST HIT:%@",json);
        
        NSString *errornumber= [NSString stringWithFormat:@"%@",[json objectForKey:@"errorno"]];
        
        if (![errornumber isEqualToString:@"0"])
        {
            NSString *err_str = [json objectForKey:@"error"];
            alert = [[UIAlertView alloc] initWithTitle:@"Error in Retrieving Data!"
                                               message:err_str
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
            //[act removeFromSuperview];
        }
        else
        {
            app_contacts = [[NSMutableArray alloc]init];
            //        //DebugLog(@"details req= %@",[json1 objectForKey:@"details"]);
            if ([[json objectForKey:@"details"] isKindOfClass:[NSNull class]] || [json objectForKey:@"details"] == (id)[NSNull null] ||[[json objectForKey:@"details"] count] ==0)
            {
                //[act removeFromSuperview];
                //            [SVProgressHUD dismiss];
            }
            else
            {
                ////////////////////////////   App Contacts Found   /////////////////////////////////
                
                //            if ([string rangeOfString:@"123"].location == NSNotFound) {
                //            } else {}
                
                NSDictionary *contacts_temp_dict = [json objectForKey:@"details"];
                
                DebugLog(@"CONTACTS TEMP DICT:%@",contacts_temp_dict);
                newcontactsapp = [[NSMutableArray alloc]init];
                NSString *result;
                int con_count1;
                con_count1 = 0;
                //                        if (con_count1 > 0)
                //                        {
                for (NSDictionary *dict in [contacts_temp_dict objectForKey:@"contacts"])
                {
                    [newcontactsapp addObject:dict]; //[dict objectForKey:@"fullname"]
                }
                result = [newcontactsapp componentsJoinedByString:@","];
                
                if (con_count1 == 1)
                {
                }
                else
                {
                }
                
                DebugLog(@"newcontactsapp: %@",newcontactsapp);
                
                for (NSDictionary *dict in [contacts_temp_dict objectForKey:@"contacts"])
                {
                    [app_contacts addObject:dict];
                }
                
                for (NSDictionary *Appdict in newcontactsapp)
                {
                    [con_array addObject:Appdict];
                }
                
                [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%ld",[app_contacts count]] forKey:@"app_contacts_count"];
                
            }
        }
    }
    DebugLog(@"GOT BUDNAV CONTACTS");
    
    [self Indexing];
    [self RestoreInDevice];
    
}

-(void)RestoreInDevice
{
    
    addressBookRef = ABAddressBookCreateWithOptions(NULL, nil);
    
    CFErrorRef *errorab = nil;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, errorab);
    
    __block BOOL accessGranted = NO;
    
    if (&ABAddressBookRequestAccessWithCompletion != NULL) { // we're on iOS 6
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(sema);
        });
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        //        dispatch_release(sema);
    }
    else { // we're on iOS 5 or older
        accessGranted = YES;
    }
    if (accessGranted) {
        
        
        int finalwrite;
        for (finalwrite =0; finalwrite < [newcontactsapp count]; finalwrite++)
        {
            
            int traverselooptocheck;
            traverselooptocheck=0;
            
            ///////////////////////////////// Newly Added //////////////////////////////////////
            NSDictionary *finalwritedict = [newcontactsapp objectAtIndex:finalwrite];
            NSData *decodedData1;
            UIImage *profilepic1;
            
            DebugLog(@"Budnav Contacts:%@",finalwritedict);
            
            
            // NSString *petFirstName = [finalwritedict objectForKey:@"name"];
            // NSString *petLastName = [finalwritedict objectForKey:@"surname"];
            NSString *petfullname= [finalwritedict objectForKey:@"fullname"];
            NSString *workphone = @"";
            NSString *mobilephone = @"";
            NSString *homephone = @"";
            NSString *strretp = @"";
            NSString *cityp= @"";
            NSString *housenumberp = @"";
            NSString *countryp = @"";
            NSString *zipCode = @"";
            NSString *petEmail = @"";
            
            ////////////////// Base64 Conversion //////////////////////
            
            NSString *base64String1= [finalwritedict objectForKey:@"thumb"];
            if ([base64String1 length] > 6)
            {
                decodedData1 = [[NSData alloc] initWithBase64EncodedString:base64String1 options:0];
                profilepic1 = [UIImage imageWithData:decodedData1];
                
            }
            
            NSData *petImageData = UIImageJPEGRepresentation(profilepic1, 0.7f);
            
            
            if ([[finalwritedict objectForKey:@"phone_num"] length] > 2)
                homephone = [NSString stringWithFormat:@"%@ %@",[finalwritedict objectForKey:@"phone_pre"],[finalwritedict objectForKey:@"phone_num"]];
            if ([[finalwritedict objectForKey:@"mobile_num"] length] > 2)
                mobilephone = [NSString stringWithFormat:@"%@ %@",[finalwritedict objectForKey:@"mobile_pre"],[finalwritedict objectForKey:@"mobile_num"]];
            if ([[finalwritedict objectForKey:@"city"] length] > 2)
                cityp = [NSString stringWithFormat:@"%@",[finalwritedict objectForKey:@"city"]];
            if ([[finalwritedict objectForKey:@"country"] length] > 2)
                countryp = [NSString stringWithFormat:@"%@",[finalwritedict objectForKey:@"country"]];
            
            
            if ([[finalwritedict objectForKey:@"street"]length]>2) {
                strretp = [finalwritedict objectForKey:@"street"];
                
            }
            
            
            if (![[finalwritedict objectForKey:@"housenumber"] isKindOfClass:[NSNull class]] &&  ![[finalwritedict objectForKey:@"housenumber"] isEqualToString:@"(null)"] && ![[finalwritedict objectForKey:@"housenumber"] isEqualToString:@""] && [[finalwritedict objectForKey:@"housenumber"] length] > 0) {
                housenumberp = [finalwritedict objectForKey:@"housenumber"];
                
                strretp = [NSString stringWithFormat:@"%@,%@",strretp,housenumberp];
                
            }
            
            
            if ([[finalwritedict objectForKey:@"zipcode"] length]>2) {
                zipCode = [NSString stringWithFormat:@"%@", [finalwritedict objectForKey:@"zipcode"] ];
                
            }
            
            if ([[finalwritedict objectForKey:@"email"] length]>2) {
                petEmail = [finalwritedict objectForKey:@"email"];
                
            }
            
            
            ABRecordRef pet = ABPersonCreate();
            //ABRecordSetValue(pet, kABPersonFirstNameProperty, (__bridge CFStringRef)petFirstName, nil);
            // ABRecordSetValue(pet, kABPersonLastNameProperty, (__bridge CFStringRef)petLastName, nil);
            ABRecordSetValue(pet, kABPersonCompositeNameFormatFirstNameFirst, (__bridge CFStringRef)petfullname, nil);
            
            ABMutableMultiValueRef phoneNumbers = ABMultiValueCreateMutable(kABMultiStringPropertyType);
            ABMultiValueAddValueAndLabel(phoneNumbers, (__bridge CFStringRef)workphone, kABWorkLabel, NULL);
            ABMultiValueAddValueAndLabel(phoneNumbers, (__bridge CFStringRef)homephone, kABHomeLabel, NULL);
            ABMultiValueAddValueAndLabel(phoneNumbers, (__bridge CFStringRef)mobilephone, kABPersonPhoneMobileLabel, NULL);
            ABRecordSetValue(pet, kABPersonPhoneProperty, phoneNumbers, nil);
            ABPersonSetImageData(pet, (__bridge CFDataRef)petImageData, nil);
            
            
            /////////////////////// Email & Address Adding /////////////////
            
            
            ABMutableMultiValueRef emails = ABMultiValueCreateMutable(kABMultiStringPropertyType);
            ABMultiValueAddValueAndLabel(emails,  (__bridge CFStringRef)petEmail, kABWorkLabel, NULL);
            
            ABRecordSetValue(pet, kABPersonEmailProperty, emails, nil);
            
            //CFRelease(emails);
            
            
            //////////////////////// Adding Address //////////////////////////////
            
            ABMutableMultiValueRef multiAddress1 = ABMultiValueCreateMutable(kABMultiDictionaryPropertyType);
            
            NSMutableDictionary *addressDictionary2 = [[NSMutableDictionary alloc] init];
            [addressDictionary2 setObject:strretp forKey:(NSString *) kABPersonAddressStreetKey];
            [addressDictionary2 setObject:zipCode forKey:(NSString *)kABPersonAddressZIPKey];
            
            [addressDictionary2 setObject:cityp forKey:(NSString *)kABPersonAddressCityKey];
            
            [addressDictionary2 setObject:countryp forKey:(NSString *)kABPersonAddressCountryKey];
            //[addressDictionary setObject:@"us" forKey:(NSString *)kABPersonAddressCountryCodeKey];
            
            
            ABMultiValueAddValueAndLabel(multiAddress1, (__bridge CFTypeRef)(addressDictionary2), kABHomeLabel, NULL);
            
            
            //(@"home address added");
            
            ABRecordSetValue(pet, kABPersonAddressProperty, multiAddress1, nil);
            
            
            
            NSArray *allADContacts = (__bridge NSArray *)ABAddressBookCopyArrayOfAllPeople(addressBookRef);
            for (id record in allADContacts){
                ABRecordRef thisContact = (__bridge ABRecordRef)record;
                
                if (ABRecordCopyCompositeName(thisContact) != NULL)
                {
                    if (CFStringCompare(ABRecordCopyCompositeName(thisContact),
                                        (__bridge CFStringRef)(petfullname), 0) == kCFCompareEqualTo){
                        DebugLog(@"The contact already exists!  %@", (__bridge CFStringRef)(petfullname));
                        
                        
                        NSString *searchName = (__bridge NSString *)((__bridge CFStringRef)(petfullname));
                        ABAddressBookRef addressbook = ABAddressBookCreate();
                        CFStringRef nameRef = (__bridge CFStringRef) searchName;
                        CFArrayRef  allSearchRecords = ABAddressBookCopyPeopleWithName(addressbook, nameRef);
                        [self removeContactWithRecordsList:allSearchRecords];
                        
                        traverselooptocheck =0;
                    }
                    else
                    {
                    }
                }
            }
            
            if (traverselooptocheck == 0)
            {
                ABAddressBookAddRecord(addressBookRef, pet, nil);
                ABAddressBookSave(addressBookRef, nil);
            }
            
        }
    }
    
    DebugLog(@"PHONEBOOK WRITES DONE");
    
    checkIfFinished = YES;
    
    time_interval = 0.1f;
    
    [timerr invalidate];
    timerr = nil;
    
    [self performSelectorOnMainThread:@selector(objFirstViewController) withObject:nil waitUntilDone:YES];
    
    //   [[NSNotificationCenter defaultCenter] postNotificationName:@"ContactsLoaded" object:Nil];
}

-(void)objFirstViewController//:(NSNotification *)notification {
{
    time_interval = 0.1f;
    timeragain = [NSTimer scheduledTimerWithTimeInterval:0.1f
                                                  target:self
                                                selector:@selector(targetMethod:)
                                                userInfo:nil
                                                 repeats:YES];
    
}

-(void)AddBudnavContact
{
    ConNewContactsViewController *con = [[ConNewContactsViewController alloc] init];
    [con reloadAgain];
    DebugLog(@"ADD BUDNAV");
}


-(void)Indexing
{
    
#pragma mark -- this is the Beginning of NSLOCALIZEDINDEXEDCOLLATION SECTION - Bhaswar 16/7/2014
    
    // create a dictionary to store an array of objects for each section
    NSMutableDictionary *tempSections = [NSMutableDictionary dictionary];
    
    // iterate through each dictionaey in the list, and put them into the correct section
    for (NSDictionary *item in con_array)
    {
        // get the index of the section (Assuming the table index is showing A-#)
        NSInteger indexName = [[UILocalizedIndexedCollation currentCollation] sectionForObject:[item valueForKey:@"fullname"] collationStringSelector:@selector(description)];
        
        NSNumber *keyName = [NSNumber numberWithInteger:indexName];
        
        // if an array doesnt exist for the key, create one
        NSMutableArray *arrayName = [tempSections objectForKey:keyName];
        if (arrayName == nil)
        {
            arrayName = [NSMutableArray array];
        }
        
        // add the dictionary to the array (add the actual value as we need this object to sort the array later)
        [arrayName addObject: item];   //[item valueForKey:@"fullname"]];
        
        // put the array with new object in, back into the dictionary for the correct key
        [tempSections setObject:arrayName forKey:keyName];
    }
    
    /* now to do the sorting of each index */
    
    NSMutableDictionary *sortedSections = [NSMutableDictionary dictionary];
    
    // sort each index array (A..Z)
    [tempSections enumerateKeysAndObjectsUsingBlock:^(id key, id array, BOOL *stop)
     {
         // sort the array - again, we need to tell it which selctor to sort each object by
         NSArray *sortedArray = [[UILocalizedIndexedCollation currentCollation] sortedArrayFromArray:array collationStringSelector:@selector(description)];
         NSSortDescriptor *sort =[NSSortDescriptor sortDescriptorWithKey:@"fullname" ascending:YES selector:@selector(caseInsensitiveCompare:)];
         sortedArray=[sortedArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];
         [sortedSections setObject:[NSMutableArray arrayWithArray:sortedArray] forKey:key];
     }];
    
    // set the global sectioned dictionary
    indexedSections = sortedSections;
    //            DebugLog(@"indexedsections: %@",indexedSections);
    
    //                                                            AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //                                del.arr_contacts_ad= [app_contacts mutableCopy];
    //                                del.newarr_contacts_ad = [app_contacts mutableCopy];
    
    NSMutableArray *fresh_ban = [[NSMutableArray alloc]init];
    [fresh_ban addObject:indexedSections];
    
    [[NSUserDefaults standardUserDefaults]setObject:[NSKeyedArchiver archivedDataWithRootObject:fresh_ban] forKey:@"IndexedContactslocaldb"];
    NSData *data_SavedArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"IndexedContactslocaldb"];
    if (data_SavedArray != nil)
    {
        NSArray *oldSavedArray = [NSKeyedUnarchiver unarchiveObjectWithData:data_SavedArray];
        DebugLog(@"CONTACT LOCAL DB DB:%@",oldSavedArray);
    }
    
    DebugLog(@"CONTACT LOCAL indexedsections:%@",indexedSections);
    DebugLog(@"CONTACT LOCAL fresh_ban:%@",fresh_ban);
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:con_array ] forKey:@"Contactslocaldb"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    // DebugLog(@"conarray mutablecopy count: %ld", (unsigned long)[del.arr_contacts_ad count]);
    //                        [del loadContactsIntoDB:con_array];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //1
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"data.plist"];
    
    NSMutableArray *data = [[NSMutableArray alloc]init] ;
    data= [con_array mutableCopy];
    DebugLog(@"bbbbb data: %lu",(unsigned long)[data count]);
    [data writeToFile: path atomically:YES];
    
    DebugLog(@"INDEXING DONE");
    
}

-(void)getBackupList
{
    NSError *errorjs= nil;
    //////////////////////////////////// Again Call Getbackup For Restoring /////////////////////////////////////////////
    
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=getbackup&id=%@&access_token=%@&device_id=%@&thumb=true",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
    
    DebugLog(@"contacts -- now the url is : %@",urlString1);
    
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    
    if (signeddataURL1 == nil)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        
    }
    else
    {
        json = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                               options:kNilOptions
                                                 error:&errorjs];
        DebugLog(@"JSON FIRST HIT:%@",json);
        
        NSString *errornumber= [NSString stringWithFormat:@"%@",[json objectForKey:@"errorno"]];
        
        if (![errornumber isEqualToString:@"0"])
        {
            NSString *err_str = [json objectForKey:@"error"];
            alert = [[UIAlertView alloc] initWithTitle:@"Error in Retrieving Data!"
                                               message:err_str
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
        }
        else
        {
            backup_array = [[NSMutableArray alloc]init];
            if ([[json objectForKey:@"details"] isKindOfClass:[NSNull class]] || [json objectForKey:@"details"] == (id)[NSNull null] ||[[json objectForKey:@"details"] count] ==0)
            {
                UIAlertView *alrt= [[UIAlertView alloc] initWithTitle:nil message:@"No Data Found!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alrt show];
            }
            else
            {
                
                NSDictionary *contacts_temp_dict = [json objectForKey:@"details"];
                
                DebugLog(@"CONTACTS TEMP DICT:%@",contacts_temp_dict);
                // newcontactsapp = [[NSMutableArray alloc]init];
                int con_count1;
                con_count1 = 0;
                for (NSDictionary *dict in [contacts_temp_dict objectForKey:@"contacts"])
                {
                    [backup_array addObject:dict]; //[dict objectForKey:@"fullname"]
                }
                //result = [newcontactsapp componentsJoinedByString:@","];
                
                if (con_count1 == 1)
                {
                    //                                [[[[iToast makeText:[NSString stringWithFormat:@" %@ is a new contact",result]]
                    //                                   setGravity:iToastGravityTop] setDuration:iToastDurationLong] show];
                }
                else
                {
                    //                                [[[[iToast makeText:[NSString stringWithFormat:@" %@ are new contacts",result]]
                    //                                   setGravity:iToastGravityTop] setDuration:iToastDurationLong] show];
                }
                
                DebugLog(@"Back Up Contacts: %@",backup_array);
                //////////////////////////////////////    add to phonebook    ////////////////////////////////////
                //                    if ([[prefs objectForKey:@"copycontacts"] isEqualToString:@"no"])
                
                //                        }   //con_count closing brace
                //                    for (NSDictionary *dict in [contacts_temp_dict objectForKey:@"contacts"])
                //                    {
                //                        [app_contacts addObject:dict];
                //                    }
                [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%ld",[backup_array count]] forKey:@"backup_count"];
                
                
                con_array = [[NSMutableArray alloc]init];
                
                //                for (NSDictionary *appDict in newcontactsapp) {
                //                    [con_array addObject:appDict];
                //                }
                
                for (NSDictionary *backupDict in backup_array) {
                    [con_array addObject:backupDict];
                }
                
                [self getBudnavContact];
                //                [self Indexing];
                //                
                //                [self RestoreInDevice];
            }
        }
    }
    
    
    
}


@end