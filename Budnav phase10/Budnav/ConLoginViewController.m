//
//  ConLoginViewController.m
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 05/05/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//
#define mydomainurl @"https://budnav.com/ext"
#import "ConLoginViewController.h"
#import "ConPreSignUpViewController.h"
#import "AMScanViewController.h"
#import "SCViewController.h"
#import "ConProfileOwnViewController.h"
#import "ConNewContactsViewController.h"
#import "SVProgressHUD.h"
#import "AppDelegate.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"
#import <Contacts/Contacts.h>

@interface ConLoginViewController ()
{
    UITextField *emailtext,*passwordtext;
    UIAlertView *alert;
    UIScrollView *scviewbudget;
    NSString *access_token,*userid;
    UIActivityIndicatorView *act;
    UIScrollView *scroll_view;
    UIImageView *background_imgv;
    UIButton *login_btn, *forgot_btn;
    UILabel *forgot_label;
}
@end

@implementation ConLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
       NSLog(@"from ConLoginViewController");
    
    
    // Do any additional setup after loading the view.
    //self.view.backgroundColor=[UIColor blackColor];
    background_imgv = [[UIImageView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.origin.x, [UIScreen mainScreen].bounds.origin.y, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    [background_imgv setImage:[UIImage imageNamed:@"Background"]];
    [self.view addSubview:background_imgv];
    
    scroll_view = [[UIScrollView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.origin.x, [UIScreen mainScreen].bounds.origin.y, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    scroll_view.backgroundColor = [UIColor clearColor];
    scroll_view.scrollEnabled = YES;
    scroll_view.userInteractionEnabled = YES;
    [self.view addSubview:scroll_view];
    
    [scroll_view setContentOffset:CGPointMake(0, +[UIScreen mainScreen].bounds.size.height/4) animated:YES];
    jsonparsed=[[NSDictionary alloc]init];
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"isloggedin"] isEqualToString:@"yes"])
    {
        ConNewContactsViewController *con =[[ConNewContactsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:NO];
        AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        del.tabBarController.selectedIndex = 0;
    }
    
    
    if (self.view.bounds.size.height > 480) {
        
        //        UIImageView *viewimg=[[UIImageView alloc]initWithFrame:CGRectMake(67.5f, 100, 185, 60)];
        
        UIImageView *bck_img=[[UIImageView alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/35, [UIScreen mainScreen].bounds.size.height/30, [UIScreen mainScreen].bounds.size.width/32, [UIScreen mainScreen].bounds.size.width/18)];//(9,33,10,20)];//(13, 39, 10, 22)];
        bck_img.image=[UIImage imageNamed:@"back3"];
        [scroll_view addSubview:bck_img];
        
        UILabel *backlbl=[[UILabel alloc]initWithFrame:CGRectMake(bck_img.frame.origin.x+bck_img.frame.size.width+5, [UIScreen mainScreen].bounds.size.height/50,[UIScreen mainScreen].bounds.size.width/6, [UIScreen mainScreen].bounds.size.height/18)];
        backlbl.text=@"Back";
        backlbl.textColor=[UIColor whiteColor];
        backlbl.contentMode=UIViewContentModeScaleAspectFit;
        //    backlbl.font=[UIFont systemFontOfSize:17];//fontWithName:@"ProximaNova-Regular" size:17];
        backlbl.backgroundColor=[UIColor clearColor];
        [scroll_view addSubview:backlbl];
        
        UIButton *back_btn = [[UIButton alloc] init];
        back_btn.frame = CGRectMake(bck_img.frame.origin.x-5, bck_img.frame.origin.y-5, [UIScreen mainScreen].bounds.size.width/4, bck_img.frame.size.height+15);
        back_btn.backgroundColor = [UIColor clearColor];
        back_btn.userInteractionEnabled = YES;
        [back_btn addTarget:self action:@selector(goback:) forControlEvents:UIControlEventTouchUpInside];
        [scroll_view addSubview:back_btn];
        
        
        UIView *backvw= [[UIView alloc]initWithFrame:CGRectMake(15, 195, [UIScreen mainScreen].bounds.size.width-30, 95)];
        [scroll_view addSubview:backvw];
        backvw.backgroundColor=[UIColor whiteColor];
        backvw.layer.cornerRadius=5;
        
        UIView *separatorvw=[[UIView alloc]initWithFrame:CGRectMake(0, 49.5f, backvw.frame.size.width, 1)];
        [backvw addSubview:separatorvw];
        separatorvw.backgroundColor=[UIColor lightGrayColor];
        
        emailtext= [[UITextField alloc]initWithFrame:CGRectMake(12, 4, backvw.frame.size.width-24, 45)];
        [backvw addSubview:emailtext];
        emailtext.delegate=self;
        emailtext.autocapitalizationType= UITextAutocapitalizationTypeNone;
        emailtext.backgroundColor=[UIColor clearColor];
        emailtext.placeholder=@"Email";
        emailtext.text=@"";  //surajit.saha@esolzmail.com
        emailtext.textColor=[UIColor grayColor];
        emailtext.autocorrectionType=UITextAutocorrectionTypeNo;
        emailtext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
        emailtext.tintColor=[UIColor grayColor];
        emailtext.keyboardType=UIKeyboardTypeEmailAddress;
        emailtext.keyboardAppearance=UIKeyboardAppearanceDark;
        
        [emailtext becomeFirstResponder];
        passwordtext= [[UITextField alloc]initWithFrame:CGRectMake(12, 51, backvw.frame.size.width-24, 45)];
        [backvw addSubview:passwordtext];
        passwordtext.delegate=self;
        passwordtext.backgroundColor=[UIColor clearColor];
        passwordtext.placeholder=@"Password";
        passwordtext.text=@"";   //123456
        passwordtext.autocorrectionType=UITextAutocorrectionTypeNo;
        passwordtext.textColor=[UIColor grayColor];
        passwordtext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
        passwordtext.tintColor=[UIColor grayColor];
        passwordtext.keyboardAppearance=UIKeyboardAppearanceDark;
        passwordtext.secureTextEntry=YES;
        
        
        
        login_btn = [[UIButton alloc] initWithFrame:CGRectMake(15, 305, [UIScreen mainScreen].bounds.size.width-30, 50)];
        
        [login_btn setBackgroundColor:[UIColor clearColor]];
        
        [login_btn setBackgroundImage:[UIImage imageNamed:@"LOG_IN"] forState:UIControlStateNormal];
        [login_btn setBackgroundImage:[UIImage imageNamed:@"Log_in active"] forState:UIControlStateSelected];
        [login_btn setBackgroundImage:[UIImage imageNamed:@"Log_in active"] forState:UIControlStateHighlighted];
        login_btn.userInteractionEnabled = YES;
        [login_btn addTarget:self action:@selector(login_func:) forControlEvents:UIControlEventTouchUpInside];
        [scroll_view addSubview:login_btn];
        
        
        forgot_label = [[UILabel alloc] initWithFrame:CGRectMake(login_btn.frame.origin.x, login_btn.frame.origin.y+login_btn.frame.size.height+45, 253, 30)];
        forgot_label.backgroundColor = [UIColor clearColor];
        forgot_label.text = @"Forgot password?";
        forgot_label.textColor = [UIColor whiteColor];
        //forgotPass.titleLabel.textColor = [UIColor whiteColor];
        forgot_label.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
        //[forgot_label addTarget:self action:@selector(forgot:) forControlEvents:UIControlEventTouchUpInside];
        [scroll_view addSubview:forgot_label];
        
        forgot_btn = [UIButton buttonWithType:UIButtonTypeCustom];
        forgot_btn.frame = CGRectMake(forgot_label.frame.origin.x-5, forgot_label.frame.origin.y-5, forgot_label.frame.size.width+10, forgot_label.frame.size.height+10);
        forgot_btn.backgroundColor = [UIColor clearColor];
        [forgot_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
       
        [forgot_btn addTarget:self action:@selector(forgot:) forControlEvents:UIControlEventTouchUpInside];
        [scroll_view addSubview:forgot_btn];
        
          }
    
    else{
        
        UIImageView *bck_img=[[UIImageView alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/35, [UIScreen mainScreen].bounds.size.height/34, [UIScreen mainScreen].bounds.size.width/32, [UIScreen mainScreen].bounds.size.width/18)];//(9,33,10,20)];//(13, 39, 10, 22)];
        bck_img.image=[UIImage imageNamed:@"back3"];
        [scroll_view addSubview:bck_img];
        
        
        UILabel *backlbl=[[UILabel alloc]initWithFrame:CGRectMake(bck_img.frame.origin.x+bck_img.frame.size.width+5, [UIScreen mainScreen].bounds.size.height/50,[UIScreen mainScreen].bounds.size.width/6, [UIScreen mainScreen].bounds.size.height/18)];
        backlbl.text=@"Back";
        backlbl.textColor=[UIColor whiteColor];
        backlbl.contentMode=UIViewContentModeScaleAspectFit;
        //    backlbl.font=[UIFont systemFontOfSize:17];//fontWithName:@"ProximaNova-Regular" size:17];
        backlbl.backgroundColor=[UIColor clearColor];
        [scroll_view addSubview:backlbl];
        
        UIButton *back_btn = [[UIButton alloc] init];
        back_btn.frame = CGRectMake(bck_img.frame.origin.x, bck_img.frame.origin.y, [UIScreen mainScreen].bounds.size.width/6, [UIScreen mainScreen].bounds.size.width/18);
        back_btn.backgroundColor = [UIColor clearColor];
        back_btn.userInteractionEnabled = YES;
        [back_btn addTarget:self action:@selector(goback:) forControlEvents:UIControlEventTouchUpInside];
        [scroll_view addSubview:back_btn];
        
        
        
        
        
        UIView *backvw= [[UIView alloc]initWithFrame:CGRectMake(15, 165, [UIScreen mainScreen].bounds.size.width-30, 90)];
        [scroll_view addSubview:backvw];
        backvw.backgroundColor=[UIColor whiteColor];
        backvw.layer.cornerRadius=5;
        
        UIView *separatorvw=[[UIView alloc]initWithFrame:CGRectMake(0, 44.5f, backvw.frame.size.width, 1)];
        [backvw addSubview:separatorvw];
        separatorvw.backgroundColor=[UIColor lightGrayColor];
        
        emailtext= [[UITextField alloc]initWithFrame:CGRectMake(12, 4, backvw.frame.size.width-24, 40)];
        [backvw addSubview:emailtext];
        emailtext.delegate=self;
        emailtext.backgroundColor=[UIColor clearColor];
        emailtext.autocapitalizationType= UITextAutocapitalizationTypeNone;
        emailtext.placeholder=@"Email";
        emailtext.text=@"";  //surajit.saha@esolzmail.com
        emailtext.textColor=[UIColor grayColor];
        emailtext.autocorrectionType=UITextAutocorrectionTypeNo;
        emailtext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
        emailtext.tintColor=[UIColor grayColor];
        emailtext.keyboardType=UIKeyboardTypeEmailAddress;
        emailtext.keyboardAppearance=UIKeyboardAppearanceDark;
        [emailtext becomeFirstResponder];
        
        passwordtext= [[UITextField alloc]initWithFrame:CGRectMake(12, 46, backvw.frame.size.width-24, 40)];
        [backvw addSubview:passwordtext];
        passwordtext.delegate=self;
        passwordtext.backgroundColor=[UIColor clearColor];
        passwordtext.placeholder=@"Password";
        passwordtext.text=@"";   //123456
        passwordtext.autocorrectionType=UITextAutocorrectionTypeNo;
        passwordtext.textColor=[UIColor grayColor];
        passwordtext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
        passwordtext.tintColor=[UIColor grayColor];
        passwordtext.keyboardAppearance=UIKeyboardAppearanceDark;
        passwordtext.secureTextEntry=YES;
        
        
        UIButton *loginbt = [UIButton buttonWithType:UIButtonTypeCustom];
        loginbt.frame = CGRectMake(15, 270, [UIScreen mainScreen].bounds.size.width-30, 50);
        [loginbt setBackgroundColor:[UIColor clearColor]];
        
        [loginbt setBackgroundImage:[UIImage imageNamed:@"LOG_IN"] forState:UIControlStateNormal];
        loginbt.userInteractionEnabled = YES;
        [loginbt addTarget:self action:@selector(login_func:) forControlEvents:UIControlEventTouchUpInside];
        [scroll_view addSubview:loginbt];
        
        
        forgot_label = [[UILabel alloc] initWithFrame:CGRectMake(login_btn.frame.origin.x, login_btn.frame.origin.y+login_btn.frame.size.height+45, 253, 30)];
        forgot_label.backgroundColor = [UIColor clearColor];
        forgot_label.text = @"Forgot password?";
        forgot_label.textColor = [UIColor whiteColor];
        //forgotPass.titleLabel.textColor = [UIColor whiteColor];
        forgot_label.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
        //[forgot_label addTarget:self action:@selector(forgot:) forControlEvents:UIControlEventTouchUpInside];
        [scroll_view addSubview:forgot_label];
        
        forgot_btn = [UIButton buttonWithType:UIButtonTypeCustom];
        forgot_btn.frame = CGRectMake(forgot_label.frame.origin.x-5, forgot_label.frame.origin.y-5, forgot_label.frame.size.width+10, forgot_label.frame.size.height+10);
        forgot_btn.backgroundColor = [UIColor clearColor];
        [forgot_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        
        [forgot_btn addTarget:self action:@selector(forgot:) forControlEvents:UIControlEventTouchUpInside];
        [scroll_view addSubview:forgot_btn];
        
        
    }
}

-(void)viewDidDisappear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
}

-(void)viewDidAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
    [super viewDidAppear:YES];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    
    self.screenName = @"Login";
    
    
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createEventWithCategory:@"Home"
                                             action:@"Login"
                                              label:nil
                                              value:nil] build]];

    UIButton *helpbt = [UIButton buttonWithType:UIButtonTypeCustom];
    helpbt.frame = CGRectMake(15, self.view.bounds.size.height-55, [UIScreen mainScreen].bounds.size.width-30, 30);
    helpbt.layer.cornerRadius=5;
    helpbt.titleLabel.font=[UIFont fontWithName:@"ProximaNova-Bold" size:18];
    [helpbt setTitle:@"Help" forState:UIControlStateNormal];
    helpbt.titleLabel.textColor=[UIColor whiteColor];
    helpbt.backgroundColor=[UIColor clearColor];
    [helpbt addTarget:self action:@selector(gotoHelp:) forControlEvents:UIControlEventTouchUpInside];
    [helpbt setShowsTouchWhenHighlighted:NO];
    //    [scviewbudget addSubview:helpbt];
    
    CFErrorRef *errorab = nil;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, errorab);
    
    __block BOOL accessGranted = NO;
    if (&ABAddressBookRequestAccessWithCompletion != NULL) { // we're on iOS 6
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(sema);
        });
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        //        dispatch_release(sema);
    }
    else { // we're on iOS 5 or older
        accessGranted = YES;
    }
    if (accessGranted) {
        DebugLog(@"got it got it");
    }    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    [scroll_view setContentOffset:CGPointMake(0, 0) animated:YES];
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    //[scroll_view setContentSize:CGSizeMake(0,scroll_view.frame.size.height+50.0f)];
    [scroll_view setContentOffset:CGPointMake(0, +[UIScreen mainScreen].bounds.size.height/4) animated:YES];
    
    if (self.view.frame.size.height < 500)
        [UIView animateWithDuration:0.5f animations:^{
            scviewbudget.contentOffset = CGPointMake(0, 120);
        }];
}

-(void)gotosignup:(id)sender
{
    ConPreSignUpViewController *pre= [[ConPreSignUpViewController alloc]init];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:pre animated:YES];
}

-(void)gotoHelp:(id)sender
{
    
}

- (void)goback:(id)sender
{
    
    CATransition *transition = [CATransition animation];
    
    transition.duration = 0.4f;
    
    transition.type = kCATransitionFade;
    
    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)forgot:(id)sender
{
    DebugLog(@"I forgot my password");
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://budnav.com/?p=password"]];
    
    
}


-(void)login_func:(id)sender
{
    login_btn.userInteractionEnabled = NO;
    
    [self textFieldShouldReturn:passwordtext];
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    [emailtext becomeFirstResponder];
    //    [scroll_view setContentOffset:CGPointMake(0, +[UIScreen mainScreen].bounds.size.height/4) animated:YES];
    // [emailtext resignFirstResponder];
    
    if ([[emailtext.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your Email!"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        login_btn.userInteractionEnabled = YES;
        [self resignFirstResponder];
        [scroll_view setContentOffset:CGPointMake(0, +[UIScreen mainScreen].bounds.size.height/4) animated:YES];
        
        //        [act stopAnimating];
        //        act.hidden=YES;
    }
    else if(![self NSStringIsValidEmail:emailtext.text])
    {
        emailtext.text =nil;
        alert =[[UIAlertView alloc]initWithTitle:@"Email Address is Invalid" message:Nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        login_btn.userInteractionEnabled = YES;
        [scroll_view setContentOffset:CGPointMake(0, +[UIScreen mainScreen].bounds.size.height/4) animated:YES];
        //        [act stopAnimating];
        //        act.hidden=YES;
        return;
    }
    else if ([[passwordtext.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your Password!"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        login_btn.userInteractionEnabled = YES;
        [scroll_view setContentOffset:CGPointMake(0, +[UIScreen mainScreen].bounds.size.height/4) animated:YES];
    }
    else
    {
        act = [[UIActivityIndicatorView alloc] init];
        act.frame = CGRectMake(150,250, [UIScreen mainScreen].bounds.size.width-300, 20);
        // act.center = self.view.center;
        [act startAnimating];
        act.hidesWhenStopped=YES;
        act.hidden=NO;
        act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        [act setColor:[UIColor whiteColor]];
        [background_imgv addSubview:act];
        [act startAnimating];
        
        dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(q, ^{
            
            @try {
                
                #if TARGET_IPHONE_SIMULATOR
                
                [[NSUserDefaults standardUserDefaults] setObject:@"cb8143a8becd78c317b8e0c722c9177a4b9579ab25f5e2f5f4fe806dc2937a3e" forKey:@"deviceToken"];
#endif
                
                
                [[NSUserDefaults standardUserDefaults]setObject:emailtext.text forKey:@"EmailLogin"];
                
                ///////////////////////////// Password by POST METHOD /////////////////////////
                
                NSString *loginstring = [NSString stringWithFormat:@"%@/?action=login&device_id=%@&username=%@&device_type=ios",mydomainurl,[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"],[emailtext.text stringByTrimmingCharactersInSet:whitespace]];
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:loginstring]];
                [request setURL:[NSURL URLWithString:loginstring]];
                NSString *post = [NSString stringWithFormat:@"password=%@",[passwordtext.text stringByTrimmingCharactersInSet:whitespace]];
                [request setHTTPMethod:@"POST"];
                [request setHTTPBody:[post dataUsingEncoding:NSUTF8StringEncoding]];
              
                
                NSURLResponse *response = nil;
                NSError *error= nil;
                NSData *dataURL = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                
                
                DebugLog(@"Login URL = %@",request);
                
                NSError *localErr;
                
                
                DebugLog(@"DATA URL = %@",dataURL);
                if (dataURL != nil)
                {
                    if(localErr!=nil)
                    {
                        DebugLog(@" nil");
                    }
                    jsonparsed = [NSJSONSerialization JSONObjectWithData:dataURL //1
                                                                 options:kNilOptions
                                                                   error:&localErr];
                    
                    DebugLog(@"login data: %@",jsonparsed);
                    
                    NSString *errornumber= [NSString stringWithFormat:@"%@",[jsonparsed objectForKey:@"errorno"]];
                    DebugLog(@"err  %@",errornumber);
                    if (![errornumber isEqualToString:@"0"])
                    {
                        login_btn.userInteractionEnabled = YES;
                        //                        emailtext.text = @"";
                        //                        passwordtext.text = @"";
                        DebugLog(@"if if");
                        [self performSelectorOnMainThread:@selector(failure) withObject:nil waitUntilDone:YES]    ;
                    }
                    else
                    {
                        [self performSelectorOnMainThread:@selector(yesSuccess)
                                               withObject:nil
                                            waitUntilDone:YES];
                        
                    }
                }
                else
                {
                    DebugLog(@"server not responding");
                    
                    [self performSelectorOnMainThread:@selector(nointernetconnection) withObject:nil waitUntilDone:YES];
                    
                                      [act stopAnimating];
                }
            }
            @catch (NSException *exception) {
                DebugLog(@"exception occurred");
            }
            @finally {
            }
            dispatch_async(dispatch_get_main_queue(), ^{
            });
        });
    }
}


-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}


-(void)yesSuccess{
    
    DebugLog(@"else-e comin.....");
    NSDictionary *dict_details = [jsonparsed objectForKey:@"details"];
    NSDictionary *dict_profile = [dict_details objectForKey:@"profile"];
    
    access_token = [NSString stringWithFormat:@"%@",[dict_details objectForKey:@"access_token"]];
    userid = [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"id"]];
    DebugLog(@"access token ret  %@  %@",access_token,userid);
    
//    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//    
//    // You only need to set User ID on a tracker once. By setting it on the tracker, the ID will be
//    // sent with all subsequent hits.
//    [tracker set:kGAIUserId value:userid];
//    
//    // This hit will be sent with the User ID value and be visible in User-ID-enabled views (profiles).
//    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Home"            // Event category (required)
//                                                          action:@"User Sign In"  // Event action (required)
//                                                           label:nil              // Event label
//                                                           value:nil] build]];    // Event value

    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:userid forKey:@"userid"];
    [prefs setObject:access_token forKey:@"access_token"];
    [prefs setObject:@"yes" forKey:@"isloggedin"];
    [prefs setObject:@"yes" forKey:@"addbackup"];
    [prefs setObject:@"yes" forKey:@"copycontacts"];
    [prefs setObject:@"yes" forKey:@"firstRequest"];
    [prefs setObject:@"yes" forKey:@"firstContacts"];
    [prefs setObject:@"yes" forKey:@"firstMap"];
    [prefs setObject:@"yes" forKey:@"firstProfile"];
    [prefs setObject:@"yes" forKey:@"nowlogsin"];
    [prefs synchronize];
    DebugLog(@"id & access_token = %@ -- %@",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"]);
    ConNewContactsViewController *pre= [[ConNewContactsViewController alloc]init];
    
    [self.navigationController pushViewController:pre animated:NO];
}

-(void)failure{
    
    NSString *err_str = [jsonparsed objectForKey:@"error"];
    alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                       message:err_str
                                      delegate:self
                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
    [alert show];
    [act stopAnimating];
    [act removeFromSuperview];
    [scroll_view setContentOffset:CGPointMake(0, +[UIScreen mainScreen].bounds.size.height/4) animated:YES];
}

-(void)nointernetconnection{
    
    [SVProgressHUD showErrorWithStatus:@"Sorry!! Server Not Responding!"];
    [act stopAnimating];
    [act removeFromSuperview];
}
@end