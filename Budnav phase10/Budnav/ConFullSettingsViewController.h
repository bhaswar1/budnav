//
//  ConFullSettingsViewController.h
//  Budnav
//
//  Created by Bhaswar's MacBook Air on 20/03/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "MDRadialProgressView.h"
#import "GAITrackedViewController.h"

@interface ConFullSettingsViewController : GAITrackedViewController<UITableViewDelegate, UITableViewDataSource,UIWebViewDelegate,NSURLConnectionDelegate,UIActionSheetDelegate>{
    MDRadialProgressView *radialView3;
    NSTimer *timerr;
    NSDictionary *jsonparsed, *json1;
    UIActivityIndicatorView *actincir;
    NSMutableDictionary *business_dict, *personal_dict, *social_dict, *profdict;
}
@property(nonatomic, assign) float length;
@property(nonatomic, readwrite) NSMutableArray *cell_objects,*about_cell_objects;
@property(nonatomic, readwrite) UITableView *itemsTable,*about_Table;
@property(nonatomic, readwrite) UIScrollView *mainScroll;
@property(nonatomic, readwrite) int i;
@property(nonatomic, readwrite) UIView *table_view,*about_sep,*aboutview;

@property(nonatomic, readwrite) int move, moveparent;
@property(nonatomic, readwrite) UIView *mainview,*topbar;
@property(nonatomic, readwrite) UITextField *myTextField;
@property(nonatomic, readwrite) NSString *pass;
@property(nonatomic, readwrite) NSMutableArray *con_array,*final_con_array,*filtered_arr, *con_array1;
@property(nonatomic, readwrite) NSMutableDictionary *add_contacts_dict;
@property(nonatomic, readwrite) NSMutableArray *app_contacts, *check_app_cont, *phonesarr, *check_app_cont1, *backuparr, *testarr,*addtophonebook, *existingphn;
@property(nonatomic, readwrite) NSMutableArray *uniquearray, *part_array;
@property(nonatomic, readwrite) NSMutableData *responseData;
@property(nonatomic, readwrite) UIImageView *logoimg;
@property(nonatomic, assign) BOOL _control_check;

typedef void(^myCompletion)(BOOL);

@end
