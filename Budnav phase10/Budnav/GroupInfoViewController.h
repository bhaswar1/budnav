//
//  GroupInfoViewController.h
//  Budnav
//
//  Created by ios on 15/09/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface GroupInfoViewController : GAITrackedViewController

@property(strong,nonatomic) NSMutableDictionary *mydic;
@property(strong,nonatomic) NSData *mydata2;

@end
