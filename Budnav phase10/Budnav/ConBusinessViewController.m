
//  ConBusinessViewController.m
//  Budnav
//
//  Created by intel on 14/05/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "ConBusinessViewController.h"
#import "SVProgressHUD.h"
#import "ConFullSettingsViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

@interface ConBusinessViewController ()
{
    UIView *topbar, *flagView;
    UILabel *flag_name, *heading_div;
    NSArray *countriesList, *isdcode_list;
    UITableView *countrycodetable, *isdcode_table;
    UIImageView *flag_image, *bck_img;
    UIScrollView *scrollView;
    UIButton *save_btn, *pre_mob;
    UITextField *company, *jobTitle, *mobilePhone, *email, *website, *zipcode, *city, *street;
    UIAlertView *alert;
    NSUserDefaults *prefs;
    AppDelegate *del;
}

@end

@implementation ConBusinessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"countriesflags" ofType:@"json"]];
    NSError *localError = nil;
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
    
    if (localError != nil) {
        DebugLog(@"%@", [localError userInfo]);
    }
    countriesList = (NSArray *)parsedObject;
    
    NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
    countriesList = [countriesList sortedArrayUsingDescriptors:sortDescriptors];
    
    
    ///////////////////////////////////// ISD CODE ARRAY ///////////////////////////////////////////////
    NSData *data1 = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"countries" ofType:@"json"]];
    NSError *localError1 = nil;
    NSDictionary *parsedObject1 = [NSJSONSerialization JSONObjectWithData:data1 options:0 error:&localError1];
    
    if (localError1 != nil) {
    }
    isdcode_list = (NSArray *)parsedObject1;
    
    NSSortDescriptor *sort12 = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    
    isdcode_list = [isdcode_list sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort12]];
    
    for (check=0; check < isdcode_list.count; check++) {
        
        if ([[[isdcode_list objectAtIndex:check]objectForKey:@"code"] isEqualToString: [[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkISOCode"]]) {
            isdCode = [NSString stringWithFormat:@"%@",[[isdcode_list objectAtIndex:check]objectForKey:@"dial_code"]];
        }
    }
    
    DebugLog(@"ISDCODE ARRAY:%@",isdcode_list);
    
   ////////////////////////////////////////////////////////////////////////////////////////////
    
    
    self.view.backgroundColor = [UIColor colorWithRed:(238.0f/255.0f) green:(238.0f/255.0f) blue:(238.0f/255.0f) alpha:1.0f];
    del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, [[UIScreen mainScreen]bounds].size.height-44, [UIScreen mainScreen].bounds.size.width, 44)];
    
    del.tabBarController.tabBar.hidden = YES;
    self.navigationController.navigationBarHidden=YES;
    
    
    topbar = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64)];
    [self.view addSubview:topbar];
    //topbar.backgroundColor=[UIColor blackColor];
    topbar.layer.zPosition=2;
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
    
    // Add the gradient to the view
    [topbar.layer insertSublayer:gradient atIndex:0];
    
    
    UIImageView *logo_img = [[UIImageView alloc]init];
    logo_img.frame = CGRectMake(27+([UIScreen mainScreen].bounds.size.width-160)/2, 32, 101, 20);
    logo_img.backgroundColor = [UIColor clearColor];
    
    logo_img.image = [UIImage imageNamed:@"new_logo"];
    [topbar addSubview:logo_img];
    
    
    
    UILabel *topbartitle = [[UILabel alloc]initWithFrame:CGRectMake(60, 32, [UIScreen mainScreen].bounds.size.width-120, 20)];
    topbartitle.backgroundColor=[UIColor clearColor];
    //[topbar addSubview:topbartitle];
    topbartitle.text=@"CHANGE PASSWORD";
    topbartitle.textAlignment=NSTextAlignmentCenter;
    topbartitle.font=[UIFont fontWithName:@"ProximaNova-Regular" size:15.5f];
    topbartitle.textColor=[UIColor whiteColor];
    
    bck_img = [[UIImageView alloc]initWithFrame:CGRectMake(15,32,12,20)];//(13, 39, 10, 22)];
    bck_img.image=[UIImage imageNamed:@"back3"];
    [topbar addSubview:bck_img];
    
    
    UIButton *back_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    back_btn.frame = CGRectMake(0, 0, logo_img.frame.origin.x
                                -1, 64);
    back_btn.backgroundColor=[UIColor clearColor];
    [back_btn setTitle:nil forState:UIControlStateNormal];
    
    [back_btn addTarget:self action:@selector(backColor) forControlEvents:UIControlEventTouchDown];
    [back_btn addTarget:self action:@selector(backColorAgain) forControlEvents:UIControlEventTouchDragExit];
    [back_btn addTarget:self action:@selector(backColor) forControlEvents:UIControlEventTouchDragEnter];
    [back_btn addTarget:self action:@selector(goback) forControlEvents:UIControlEventTouchUpInside];
    [topbar addSubview:back_btn];
    
    
    save_btn = [[UIButton alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-90, 25, 90, 39)];
    [save_btn setTitle:@"Save" forState:UIControlStateNormal];
    save_btn.backgroundColor = [UIColor clearColor];
    [save_btn.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Bold" size:20.0]];
    [save_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [topbar addSubview:save_btn];
    [save_btn addTarget:self action:@selector(changeColor) forControlEvents:UIControlEventTouchDown];
    [save_btn addTarget:self action:@selector(changeColorAgain) forControlEvents:UIControlEventTouchDragExit];
    [save_btn addTarget:self action:@selector(changeColor) forControlEvents:UIControlEventTouchDragEnter];
    
    [save_btn addTarget:self action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *headingView = [[UIView alloc] initWithFrame:CGRectMake(0, 65, [UIScreen mainScreen].bounds.size.width, 101)];
    [headingView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:headingView];
    
    
    UILabel *headingLabel = [[UILabel alloc] initWithFrame:CGRectMake(65, 35, [UIScreen mainScreen].bounds.size.width-130, 35)];
    [headingLabel setBackgroundColor:[UIColor clearColor]];
    headingLabel.text = @"This profile can be seen \n by business connections";
    headingLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
//    headingLabel.lineBreakMode = YES;
    headingLabel.numberOfLines = 2;
    headingLabel.textColor = [UIColor blackColor];
    headingLabel.textAlignment = NSTextAlignmentCenter;
    [headingView addSubview:headingLabel];
    
    
    
    heading_div = [[UILabel alloc] initWithFrame:CGRectMake(0, 101, [UIScreen mainScreen].bounds.size.width, 0.6f)];
    [heading_div setBackgroundColor:[UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f]];
    [headingView addSubview:heading_div];
    
    for (check=0; check < countriesList.count; check++){
        
        if ([[[countriesList objectAtIndex:check]objectForKey:@"code2l"] isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"placemarkISOCode"]]) {
            
            flagCode = [[countriesList objectAtIndex:check]objectForKey:@"flag_128"];
            
        }
        //        DebugLog(@"FLAG_CODE===============>%@",flagCode);
    }
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
   self.screenName = @"Edit business profile";
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createEventWithCategory:@"Settings"
                                             action:@"Edit business profile"
                                              label:nil
                                              value:nil] build]];

    
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 166.7f, [UIScreen mainScreen].bounds.size.width,self.view.frame.size.height) ];
    scrollView.backgroundColor =[UIColor clearColor];
    scrollView.scrollEnabled = YES;
    scrollView.userInteractionEnabled = YES;
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width,scrollView.frame.size.height+255)];
    scrollView.contentSize = CGSizeMake(1, scrollView.frame.size.height+255);
    scrollView.showsVerticalScrollIndicator=NO;
    [self.view addSubview:scrollView];
    scrollView.contentOffset=CGPointMake(0, 0);
    
    
    company = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
        company.backgroundColor = [UIColor whiteColor];
        company.delegate = self;
        company.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
        company.textColor = [UIColor blackColor];
        company.keyboardAppearance = YES;
        company.keyboardAppearance = UIKeyboardAppearanceDark;
        company.placeholder = @"Company";
        [company setValue:[UIColor colorWithRed:(146.0f/255.0f) green:(146.0f/255.0f) blue:(146.0f/255.0f) alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
        company.layer.sublayerTransform = CATransform3DMakeTranslation(20.f, 0.0f, 0.0f);
        [scrollView addSubview:company];
    
        UILabel *company_div = [[UILabel alloc] initWithFrame:CGRectMake(0, 50, [UIScreen mainScreen].bounds.size.width, 0.6f)];
        [company_div setBackgroundColor:[UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f]];
        [scrollView addSubview:company_div];
    
    jobTitle = [[UITextField alloc] initWithFrame:CGRectMake(0, company_div.frame.origin.y+company_div.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50)];
    jobTitle.backgroundColor = [UIColor whiteColor];
    jobTitle.delegate = self;
    jobTitle.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    jobTitle.textColor = [UIColor blackColor];
    jobTitle.keyboardAppearance = UIKeyboardAppearanceDark;
    jobTitle.placeholder = @"Job title";
    [jobTitle setValue:[UIColor colorWithRed:(146.0f/255.0f) green:(146.0f/255.0f) blue:(146.0f/255.0f) alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    jobTitle.layer.sublayerTransform = CATransform3DMakeTranslation(20.f, 0.0f, 0.0f);
    [scrollView addSubview:jobTitle];
    
    UILabel *jobTitle_div = [[UILabel alloc] initWithFrame:CGRectMake(0, jobTitle.frame.origin.y+jobTitle.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.6f)];
    [jobTitle_div setBackgroundColor:[UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f]];
    [scrollView addSubview:jobTitle_div];
    
    
    /////////////////////// New Addition ISD Code ////////////////////////////////////////////
    pre_mob = [[UIButton alloc] initWithFrame:CGRectMake(0, jobTitle_div.frame.origin.y+jobTitle_div.frame.size.height, 70, 50)];
    [pre_mob setBackgroundColor:[UIColor whiteColor]];
    pre_mob.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    [pre_mob setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
       [scrollView addSubview:pre_mob];
    pre_mob.tag = 2;
    pre_mob.userInteractionEnabled = YES;
    [pre_mob addTarget:self action:@selector(select:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *premobile_div = [[UILabel alloc] initWithFrame:CGRectMake(70, pre_mob.frame.origin.y, 1.0f, 50)];
    [premobile_div setBackgroundColor:[UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f]];
    [scrollView addSubview:premobile_div];
    
    mobilePhone = [[UITextField alloc] initWithFrame:CGRectMake(72, jobTitle_div.frame.origin.y+jobTitle_div.frame.size.height, [UIScreen mainScreen].bounds.size.width-72, 50)];
    mobilePhone.backgroundColor = [UIColor whiteColor];
    mobilePhone.delegate = self;
    mobilePhone.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    mobilePhone.textColor = [UIColor blackColor];
    mobilePhone.keyboardAppearance = UIKeyboardAppearanceDark;
    mobilePhone.placeholder = @"Mobile phone";
    [mobilePhone setValue:[UIColor colorWithRed:(146.0f/255.0f) green:(146.0f/255.0f) blue:(146.0f/255.0f) alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    mobilePhone.layer.sublayerTransform = CATransform3DMakeTranslation(20.f, 0.0f, 0.0f);
    [scrollView addSubview:mobilePhone];
    
    UILabel *mobile_div = [[UILabel alloc] initWithFrame:CGRectMake(0, mobilePhone.frame.origin.y+mobilePhone.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.6f)];
    [mobile_div setBackgroundColor:[UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f]];
    [scrollView addSubview:mobile_div];
    
    
    email = [[UITextField alloc] initWithFrame:CGRectMake(0, mobile_div.frame.origin.y+mobile_div.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50)];
    email.backgroundColor = [UIColor whiteColor];
    email.delegate = self;
    email.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    email.textColor = [UIColor blackColor];
    email.placeholder = @"Email address";
    email.keyboardAppearance = UIKeyboardAppearanceDark;
    email.autocapitalizationType = UITextAutocapitalizationTypeNone;
    [email setValue:[UIColor colorWithRed:(146.0f/255.0f) green:(146.0f/255.0f) blue:(146.0f/255.0f) alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    email.layer.sublayerTransform = CATransform3DMakeTranslation(20.f, 0.0f, 0.0f);
    [scrollView addSubview:email];
    
    UILabel *email_div = [[UILabel alloc] initWithFrame:CGRectMake(0, email.frame.origin.y+email.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.6f)];
    [email_div setBackgroundColor:[UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f]];
    [scrollView addSubview:email_div];
    
    
    website = [[UITextField alloc] initWithFrame:CGRectMake(0, email_div.frame.origin.y+email_div.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50)];
    website.backgroundColor = [UIColor whiteColor];
    website.delegate = self;
    website.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    website.textColor = [UIColor blackColor];
    website.keyboardAppearance = UIKeyboardAppearanceDark;
    website.autocapitalizationType = UITextAutocapitalizationTypeNone;
    website.placeholder = @"Website";
    [website setValue:[UIColor colorWithRed:(146.0f/255.0f) green:(146.0f/255.0f) blue:(146.0f/255.0f) alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    website.layer.sublayerTransform = CATransform3DMakeTranslation(20.f, 0.0f, 0.0f);
    [scrollView addSubview:website];

    
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(website)];
//    [website addGestureRecognizer:tap];
    
    UILabel *website_div = [[UILabel alloc] initWithFrame:CGRectMake(0, website.frame.origin.y+website.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.6f)];
    [website_div setBackgroundColor:[UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f]];
    [scrollView addSubview:website_div];
    
    
    flagView = [[UIView alloc] initWithFrame:CGRectMake(0, website_div.frame.origin.y+website_div.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50)];
    flagView.backgroundColor=[UIColor whiteColor];
    [scrollView addSubview:flagView];
    
    flag_image = [[UIImageView alloc] initWithFrame:CGRectMake(20, 15, 25, 20)];
    flag_image.backgroundColor = [UIColor clearColor];
    flag_image.image = [UIImage imageNamed:flagCode];
    [flagView addSubview:flag_image];
    
    flag_name = [[UILabel alloc] init];
    flag_name.backgroundColor = [UIColor clearColor];
    flag_name.textColor = [UIColor colorWithRed:(0.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0];
    
    if ([del.business_dict objectForKey:@"b_country"] != false && ![[del.business_dict objectForKey:@"b_country"] isEqualToString:@"0"] && [[del.business_dict objectForKey:@"b_country"] length] > 1)
    {
        flag_name.text= [NSString stringWithFormat:@"%@",[del.business_dict objectForKey:@"b_country"]];
        
        NSString *countrystr = [[NSString stringWithFormat:@"%@",[del.business_dict objectForKey:@"b_country"]] lowercaseString];
        countrystr =[NSString stringWithFormat:@"%@_32.png",countrystr];
        flag_image.image=[UIImage imageNamed:countrystr];
        
        DebugLog(@"FLAG:%@ %@",countrystr, flag_name.text);
        
        if ([flag_name.text isEqualToString:@"Select country"]) {
            
            flag_name.frame = CGRectMake(20, 15, [[UIScreen mainScreen] bounds].size.width-120, 20);
        }
        else
        {
            flag_name.frame = CGRectMake(flag_image.frame.origin.x+flag_image.frame.size.width+15, 15, [[UIScreen mainScreen] bounds].size.width-120, 20);
        }
       
        //        NSString *flagCodeaa;
        //        for (check=0; check < countriesList.count; check++){
        //
        //            if ([[[countriesList objectAtIndex:check]objectForKey:@"name"] isEqualToString:[personal_dict objectForKey:@"country"]]) {
        //
        //                flagCodeaa = [[countriesFlags objectAtIndex:check]objectForKey:@"flag_128"];
        //            }
        //        }
        
    }
    else {
        
        flag_image.image=[UIImage imageNamed:flagCode];
        
        flag_name.text=[[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkCode"];
        DebugLog(@"FLAG:%@",flag_name.text);
        flag_name.frame = CGRectMake(flag_image.frame.origin.x+flag_image.frame.size.width+15, 15, [[UIScreen mainScreen] bounds].size.width-120, 20);
    }    //    country.text=@"hgsdjg";
    [flagView addSubview:flag_name];
    
    UIImageView *down = [[UIImageView alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-36, website_div.frame.origin.y+website_div.frame.size.height+17, 16, 16)];
    [down setBackgroundColor:[UIColor clearColor]];
    down.image=[UIImage imageNamed:@"dnwarr.png"];
    [scrollView addSubview:down];
    down.userInteractionEnabled=YES;
    
    UIButton *down_btn = [[UIButton alloc] initWithFrame:CGRectMake(0, website_div.frame.origin.y+website_div.frame.size.height+11, [UIScreen mainScreen].bounds.size.width, 26)];
    [down_btn addTarget:self action:@selector(select:) forControlEvents:UIControlEventTouchUpInside];
    [down_btn setBackgroundColor:[UIColor clearColor]];
    down_btn.tag = 1;
    [scrollView addSubview:down_btn];
    
    
    UILabel *flag_div = [[UILabel alloc] initWithFrame:CGRectMake(0, flagView.frame.origin.y+flagView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.6f)];
    [flag_div setBackgroundColor:[UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f]];
    [scrollView addSubview:flag_div];
    
    
    zipcode = [[UITextField alloc] initWithFrame:CGRectMake(0, flag_div.frame.origin.y+flag_div.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50)];
    zipcode.backgroundColor = [UIColor whiteColor];
    zipcode.delegate = self;
    zipcode.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    zipcode.textColor = [UIColor blackColor];
    zipcode.keyboardAppearance = UIKeyboardAppearanceDark;
    zipcode.placeholder = @"Zipcode";
    [zipcode setValue:[UIColor colorWithRed:(146.0f/255.0f) green:(146.0f/255.0f) blue:(146.0f/255.0f) alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    zipcode.layer.sublayerTransform = CATransform3DMakeTranslation(20.f, 0.0f, 0.0f);
    [scrollView addSubview:zipcode];
    
    UILabel *zipcode_div = [[UILabel alloc] initWithFrame:CGRectMake(0, zipcode.frame.origin.y+zipcode.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.6f)];
    [zipcode_div setBackgroundColor:[UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f]];
    [scrollView addSubview:zipcode_div];
    
    
    city = [[UITextField alloc] initWithFrame:CGRectMake(0, zipcode_div.frame.origin.y+zipcode_div.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50)];
    city.backgroundColor = [UIColor whiteColor];
    city.delegate = self;
    city.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    city.textColor = [UIColor blackColor];
    city.keyboardAppearance = UIKeyboardAppearanceDark;
    city.placeholder = @"City";
    [city setValue:[UIColor colorWithRed:(146.0f/255.0f) green:(146.0f/255.0f) blue:(146.0f/255.0f) alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    city.layer.sublayerTransform = CATransform3DMakeTranslation(20.f, 0.0f, 0.0f);
    [scrollView addSubview:city];
    
    UILabel *city_div = [[UILabel alloc] initWithFrame:CGRectMake(0, city.frame.origin.y+city.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.6f)];
    [city_div setBackgroundColor:[UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f]];
    [scrollView addSubview:city_div];
    
    street = [[UITextField alloc] initWithFrame:CGRectMake(0, city_div.frame.origin.y+city_div.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50)];
    street.backgroundColor = [UIColor whiteColor];
    street.delegate = self;
    street.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    street.textColor = [UIColor blackColor];
    street.keyboardAppearance = UIKeyboardAppearanceDark;
    street.placeholder = @"Street";
    [street setValue:[UIColor colorWithRed:(146.0f/255.0f) green:(146.0f/255.0f) blue:(146.0f/255.0f) alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    street.layer.sublayerTransform = CATransform3DMakeTranslation(20.f, 0.0f, 0.0f);
    [scrollView addSubview:street];
    
    UILabel *street_div = [[UILabel alloc] initWithFrame:CGRectMake(0, street.frame.origin.y+street.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.6f)];
    [street_div setBackgroundColor:[UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f]];
    [scrollView addSubview:street_div];
   
    
    [self displayData];
    
}

-(void)displayData
{
    if ([del.business_dict objectForKey:@"b_company"] != false)
    {
        company.text= [NSString stringWithFormat:@"%@",[del.business_dict objectForKey:@"b_company"]];
    }
    else
        company.text=@"";
    
    if ([del.business_dict objectForKey:@"b_function"] != false)
    {
        jobTitle.text= [NSString stringWithFormat:@"%@",[del.business_dict objectForKey:@"b_function"]];
    }
    else
        jobTitle.text=@"";
    
    if ([del.business_dict objectForKey:@"b_phone_pre"] != false)
    {
       // pre_mob.titleLabel.text= [NSString stringWithFormat:@"%@",[del.business_dict objectForKey:@"b_phone_pre"]];
        [pre_mob setTitle:[NSString stringWithFormat:@"%@",[del.business_dict objectForKey:@"b_phone_pre"]] forState:UIControlStateNormal];
    }
    else
    {
        pre_mob.titleLabel.text=@"";
    }
    
    if ([del.business_dict objectForKey:@"b_phone_num"] != false)
        mobilePhone.text= [NSString stringWithFormat:@"%@",[del.business_dict objectForKey:@"b_phone_num"]];
    else
        mobilePhone.text=@"";
    
    if ([del.business_dict objectForKey:@"b_email"] != false)
        email.text= [NSString stringWithFormat:@"%@",[del.business_dict objectForKey:@"b_email"]];
    else
        email.text=@"";
    
    if ([del.business_dict objectForKey:@"b_website"] != false)
        website.text= [NSString stringWithFormat:@"%@",[del.business_dict objectForKey:@"b_website"]];
    else
        website.text=@"";
    
    
    if ([del.business_dict objectForKey:@"b_city"] != false)
    {
        city.text= [NSString stringWithFormat:@"%@",[del.business_dict objectForKey:@"b_city"]];
    }
    else
        city.text=@"";
    
    if ([del.business_dict objectForKey:@"b_zipcode"] != false)
    {
        zipcode.text= [NSString stringWithFormat:@"%@",[del.business_dict objectForKey:@"b_zipcode"]];
    }
    else
        zipcode.text=@"";
    
    if ([del.business_dict objectForKey:@"b_street"] != false)
    {
        street.text= [NSString stringWithFormat:@"%@",[del.business_dict objectForKey:@"b_street"]];
    }
    else
        street.text=@"";

}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView == isdcode_table) {
        return [isdcode_list count];
    }
    else
    {
    return [countriesList count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    if (tableView == isdcode_table) {
        
        UITableViewCell *cell = [isdcode_table dequeueReusableCellWithIdentifier:CellIdentifier];
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor=[UIColor clearColor];
        
        UILabel *name=[[UILabel alloc]initWithFrame:CGRectMake(80, 14, 150, 22)];
        name.backgroundColor=[UIColor clearColor];
        name.text=[NSString stringWithFormat:@"%@",[[isdcode_list objectAtIndex:indexPath.row] objectForKey:@"name"]];
        name.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
        name.textColor=[UIColor blackColor];
        name.textAlignment=NSTextAlignmentLeft;
        [cell addSubview:name];
        
        
        UILabel *code=[[UILabel alloc]initWithFrame:CGRectMake(10, 14, 50, 22)];
        code.backgroundColor=[UIColor clearColor];
        code.text=[NSString stringWithFormat:@"%@",[[isdcode_list objectAtIndex:indexPath.row] objectForKey:@"dial_code"]];
        code.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
        code.textColor=[UIColor blackColor];
        code.textAlignment=NSTextAlignmentCenter;
        [cell addSubview:code];
        
        UILabel *separatorlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 49.5, [UIScreen mainScreen].bounds.size.width, 0.5)];
        separatorlabel.backgroundColor=[UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f];
        [cell addSubview:separatorlabel];
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return cell;
    }
    else
    {
        UITableViewCell *cell = [countrycodetable dequeueReusableCellWithIdentifier:CellIdentifier];
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor=[UIColor clearColor];
        
        UIImageView *cellimg=[[UIImageView alloc]initWithFrame:CGRectMake(10, 14, 35, 22)];
        cellimg.image=[UIImage imageNamed:[[countriesList objectAtIndex:indexPath.row] objectForKey:@"flag_32"]];
        cellimg.clipsToBounds=YES;
        cellimg.contentMode=UIViewContentModeScaleToFill;
        [cell addSubview:cellimg];
        
        UILabel *name=[[UILabel alloc]initWithFrame:CGRectMake(60, 0, 230, 50)];
        name.backgroundColor=[UIColor clearColor];
        name.text=[NSString stringWithFormat:@"%@",[[countriesList objectAtIndex:indexPath.row] objectForKey:@"name"]];
        name.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
        name.textColor=[UIColor blackColor];
        [cell addSubview:name];
        
        UILabel *separatorlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 49.5, [UIScreen mainScreen].bounds.size.width, 0.5)];
        separatorlabel.backgroundColor=[UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f];
        [cell addSubview:separatorlabel];
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return cell;
    }
    

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView == isdcode_table) {
    
        [pre_mob setTitle:[NSString stringWithFormat:@"%@",[[isdcode_list objectAtIndex:indexPath.row]objectForKey:@"dial_code"]] forState:UIControlStateNormal];
        DebugLog(@"ISD CODE:%@",pre_mob.titleLabel.text);
        isdcode_table.hidden=YES;
    }
    else
    {
    flag_name.frame = CGRectMake(flag_image.frame.origin.x+flag_image.frame.size.width+15, 15, [[UIScreen mainScreen] bounds].size.width-120, 20);
    flag_name.text = [NSString stringWithFormat:@"%@",[[countriesList objectAtIndex:indexPath.row]objectForKey:@"name"]];
    flag_name.textColor = [UIColor blackColor];
    flag_image.image = [UIImage imageNamed:[[countriesList objectAtIndex:indexPath.row] objectForKey:@"flag_128"]];
    countrycodetable.hidden=YES;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 50;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (textField == company)
    {
        if (scrollView.contentOffset.y < 0)
            [scrollView setContentOffset:CGPointMake(0, 0)];
    }
    if (textField == jobTitle)
    {
        if (scrollView.contentOffset.y < 50)
            [scrollView setContentOffset:CGPointMake(0, 0)];
    }
    if (textField == mobilePhone)
    {
        if (scrollView.contentOffset.y < 100)
            [scrollView setContentOffset:CGPointMake(0, 0)];
    }
    if (textField == email)
    {
        if (scrollView.contentOffset.y < 150)
            [scrollView setContentOffset:CGPointMake(0,51)];
    }
    if (textField == website)
    {
        if (scrollView.contentOffset.y < 200)
            [scrollView setContentOffset:CGPointMake(0, 102)];
    }
    if (textField == zipcode)
    {
        if (scrollView.contentOffset.y < 250)
            [scrollView setContentOffset:CGPointMake(0, 204)];
    }
    if (textField == city)
    {
        if (scrollView.contentOffset.y < 300)
            [scrollView setContentOffset:CGPointMake(0, 255)];
    }
    if (textField == street)
    {
        if (scrollView.contentOffset.y < 350)
            [scrollView setContentOffset:CGPointMake(0, 306)];
    }
    
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [scrollView setContentOffset:CGPointMake(0, 0)];
    [textField resignFirstResponder];
    return YES;
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField.tag==4)
    {
        if (self.view.bounds.size.height<=480) {
        }
    }
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (textField.tag==4)
    {
        if (self.view.bounds.size.height<=480) {
        }
    }
    
    return YES;
}


-(void)goback
{
    del.tabBarController.tabBar.hidden = NO;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)save
{
    DebugLog(@"gotonext");
    
    [company resignFirstResponder];
    [jobTitle resignFirstResponder];
    [mobilePhone resignFirstResponder];
    [email resignFirstResponder];
    [website resignFirstResponder];
    [zipcode resignFirstResponder];
    [city resignFirstResponder];
    [street resignFirstResponder];
    
    
    [UIView animateWithDuration:0.0
                     animations:^{
                         
                         [SVProgressHUD showWithStatus:@"Saving......"];
                         
                         
                     }
                     completion:^(BOOL finished){
                         
                         NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
                         NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
                         
                         
                         if ([[mobilePhone.text stringByTrimmingCharactersInSet:whitespace] length]>0)
                         {
                             if ([[mobilePhone.text stringByTrimmingCharactersInSet:whitespace] length] <8 || [[mobilePhone.text stringByTrimmingCharactersInSet:whitespace] length] > 10 || !([mobilePhone.text rangeOfCharacterFromSet:notDigits].location == NSNotFound))
                             {
                                 alert = [[UIAlertView alloc] initWithTitle:@"Please Enter a Valid Mobile Number"
                                                                    message:Nil
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                                 
                                 save_btn.alpha = 1.0f;
                                 [SVProgressHUD dismiss];
                                 [alert show];
                                 
                                 
                             }
                         }
                          if ([[email.text stringByTrimmingCharactersInSet:whitespace] length] > 0 && ![self NSStringIsValidEmail:email.text])
                         {
                             alert = [[UIAlertView alloc] initWithTitle:@"Please Enter a Valid email"
                                                                message:Nil
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                             save_btn.alpha = 1.0f;
                             [SVProgressHUD dismiss];
                             [alert show];
                             
                         }
                       else if([[website.text stringByTrimmingCharactersInSet:whitespace] length] > 0 && ![self validateUrl:website.text])
                         {
                             alert = [[UIAlertView alloc] initWithTitle:@"Please Enter a valid Website"
                                                                message:Nil
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                             save_btn.alpha = 1.0f;
                             [SVProgressHUD dismiss];
                             [alert show];
                             
                             
                         }
                         else
                         {
                             prefs = [NSUserDefaults standardUserDefaults];
                             NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=editprofile&b_email=%@&b_website=%@&b_phone_num=%@&b_company=%@&b_function=%@&b_zipcode=%@&b_city=%@&b_country=%@&b_street=%@&access_token=%@&device_id=%@",[email.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[website.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[mobilePhone.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[company.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[jobTitle.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[zipcode.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[city.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[flag_name.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[street.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
                             
                             DebugLog(@"deny url: %@",urlString1);
                             NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
                             
                             NSError *error= nil;
                             NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
                             
                             if (signeddataURL1!=nil) {
                                 
                                 NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                                           options:kNilOptions
                                                                                             error:&error];
                                 DebugLog(@"deny json returns: %@",json_deny);
                                 
                                 if ([[json_deny objectForKey:@"success"]intValue] == 1)
                                 {
                                     [del ownDetails];
                                     save_btn.alpha = 1.0f;
                                     [SVProgressHUD dismiss];
                                     
                                     alert = [[UIAlertView alloc] initWithTitle:@"Changes have been Successfully Saved!"
                                                                        message:nil
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                                     [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"profile_edited"];
                                     [alert show];
                                     
                                     
                                     CATransition *transition = [CATransition animation];
                                     
                                     transition.duration = 0.4f;
                                     
                                     transition.type = kCATransitionFade;
                                     
                                     del.tabBarController.tabBar.hidden = NO;
                                     [[self navigationController].view.layer addAnimation:transition forKey:nil];
                                     [self.navigationController popViewControllerAnimated:YES];
                                 }
                                 
                             }
                             else
                             {
                                 [SVProgressHUD dismiss];
                                 save_btn.alpha = 1.0f;
                                 alert = [[UIAlertView alloc] initWithTitle:@"Error in server connection!"
                                                                    message:nil
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                                 
                                 [alert show];
                             }
                             
                         }
                         
                     }];
    
}

-(void)select:(UIButton *)sender
{
    if ((int)sender.tag == 2) {
        
        isdcode_table=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-64)];
        [self.view addSubview:isdcode_table];
        [self.view bringSubviewToFront:isdcode_table];
        
        isdcode_table.backgroundColor=[UIColor whiteColor];
        isdcode_table.dataSource=self;
        isdcode_table.delegate=self;
        isdcode_table.separatorStyle=UITableViewCellSeparatorStyleNone;
        isdcode_table.hidden=NO;
        isdcode_table.showsVerticalScrollIndicator=NO;
        isdcode_table.layer.borderColor=[[UIColor grayColor]CGColor];
        isdcode_table.layer.borderWidth=1;
        isdcode_table.layer.zPosition=3;

        
    }
    else
    {
        
        countrycodetable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, self.view.bounds.size.width,self.view.bounds.size.height-64)];
        [self.view addSubview:countrycodetable];
        countrycodetable.backgroundColor=[UIColor whiteColor];
        countrycodetable.dataSource=self;
        countrycodetable.delegate=self;
        countrycodetable.separatorStyle=UITableViewCellSeparatorStyleNone;
        //    countrycodetable.hidden=YES;
        countrycodetable.showsVerticalScrollIndicator=NO;
        countrycodetable.layer.borderColor=[[UIColor grayColor]CGColor];
        countrycodetable.layer.borderWidth=1;
        
    }
    
}

- (BOOL) validateUrl: (NSString *) candidate {
    NSString *urlRegEx =
    @"(http|https)://((\\w\\w\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    
    NSString *urlRegEx1 =  @"((\\w\\w\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    
    if ([urlTest evaluateWithObject:candidate]) {
        
        return [urlTest evaluateWithObject:candidate];
    }
    else
    {
         NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx1];
        if ([urlTest evaluateWithObject:candidate]) {
            return [urlTest evaluateWithObject:candidate];
        }
        else
        {
            return false;
        }

    }
}


-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    checkString = [checkString lowercaseString];
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:checkString];
}


-(void)changeColor
{
    save_btn.alpha = 0.5f;
}

-(void)changeColorAgain
{
    save_btn.alpha = 1.0f;
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    DebugLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        DebugLog(@"currentLocation.coordinate.longitude=========>%f",currentLocation.coordinate.longitude);
        DebugLog(@"currentLocation.coordinate.longitude=========>%f",currentLocation.coordinate.latitude);
    }
    //    [locationManager stopUpdatingLocation];
    
    DebugLog(@"Resolving the Address");
    
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemrk, NSError *error) {
        DebugLog(@"Found placemarks: %@, error: %@", placemrk, error);
        if (error == nil && [placemrk count] > 0) {
            placemark = [placemrk lastObject];
            
            DebugLog(@"ADDRESS===================>%@ %@\n%@ %@\n%@\n%@\n%@",placemark.subThoroughfare, placemark.thoroughfare,
                     placemark.postalCode, placemark.locality,
                     placemark.administrativeArea,
                     placemark.country,placemark.ISOcountryCode);
            
            [[NSUserDefaults standardUserDefaults] setObject:placemark.ISOcountryCode forKey:@"placemarkISOCode"];
            [[NSUserDefaults standardUserDefaults] setObject:placemark.country forKey:@"placemarkCode"];
            
            [locationManager stopUpdatingLocation];
        } else {
            DebugLog(@"%@", error.debugDescription);
        }
    }];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    DebugLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

-(void)backColorAgain
{
    bck_img.alpha = 1.0f;
}
-(void)backColor
{
    bck_img.alpha = 0.5f;
}

-(void)website
{
    UIWebView *webView = [[UIWebView alloc]init];
    NSString *urlString = website.text;
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [webView loadRequest:urlRequest];
    [self.view addSubview:webView];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
