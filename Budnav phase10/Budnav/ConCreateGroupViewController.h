//
//  ConCreateGroupViewController.h
//  Contacter
//
//  Created by ios on 13/01/15.
//  Copyright (c) 2015 Esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VPImageCropperViewController.h"
#import "ImageCropperViewController.h"
#import "GAITrackedViewController.h"

@interface ConCreateGroupViewController : GAITrackedViewController<UITextFieldDelegate,UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,VPImageCropperDelegate,NSURLConnectionDelegate,ImageCropperDelegate>
@property(nonatomic,retain)NSString *groupname, *description;
@property(nonatomic,assign)int image_shown;
@end
