//
//  ConOptionsPicViewController.m
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 07/05/14.
//  Copyright (c) 2014 Esolz. All rights reserved.

#import "ConOptionsPicViewController.h"
#import "ConAccountBeingMadeViewController.h"
#import "VPImageCropperViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

#define ORIGINAL_MAX_WIDTH 640.0f
#define ORIGINAL_MAX_HEIGHT 550.0f

@interface ConOptionsPicViewController () <UINavigationControllerDelegate, UIImagePickerControllerDelegate,VPImageCropperDelegate>
{
    UIImageView *zipimage;
    NSTimer *timeraa;
    UIImage *portraitImg1;
    UIImagePickerController *controller;
}
@end

@implementation ConOptionsPicViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
    
    UILabel *startlb = [[UILabel alloc]initWithFrame:CGRectMake(0, 85, self.view.bounds.size.width, 35)];
    [self.view addSubview:startlb];
    startlb.backgroundColor=[UIColor clearColor];
    startlb.text=@"Picture";
    startlb.textColor=[UIColor colorWithRed:40.0f/255.0f green:82.0f/255.0f blue:135.0f/255.0f alpha:1];
    startlb.font=[UIFont fontWithName:@"ProximaNova-Regular" size:25.5f];
    startlb.textAlignment=NSTextAlignmentCenter;
    
    UILabel *picoptionslb = [[UILabel alloc]initWithFrame:CGRectMake(30, 135, self.view.bounds.size.width-60, 55)];
    [self.view addSubview:picoptionslb];
    picoptionslb.backgroundColor=[UIColor clearColor];
    picoptionslb.text=@"Add a picture so that your friends can find you";
    picoptionslb.numberOfLines=2;
    picoptionslb.textColor=[UIColor grayColor];
    picoptionslb.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17.5f];
    picoptionslb.textAlignment=NSTextAlignmentCenter;
    
    zipimage=[[UIImageView alloc]initWithFrame:CGRectMake(110, 214, 100, 100)];
    
    zipimage.image=[UIImage imageNamed:@"icon-make-pic.png"];
    
    [self.view addSubview:zipimage];
    zipimage.userInteractionEnabled=YES;
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    UIButton *makepicbt = [UIButton buttonWithType:UIButtonTypeCustom];
    makepicbt.frame = CGRectMake(15, 338, 290, 45);
    makepicbt.layer.cornerRadius=5;
    [makepicbt setBackgroundImage:[UIImage imageNamed:@"make-a-picture.png"] forState:UIControlStateNormal];
    makepicbt.backgroundColor=[UIColor clearColor];
    [makepicbt addTarget:self action:@selector(camera_func:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:makepicbt];
    
    UIButton *gallerybt = [UIButton buttonWithType:UIButtonTypeCustom];
    gallerybt.frame = CGRectMake(15, 393, 290, 45);
    gallerybt.layer.cornerRadius=5;
    [gallerybt setBackgroundImage:[UIImage imageNamed:@"choose-from-libary.png"] forState:UIControlStateNormal];
    gallerybt.backgroundColor=[UIColor clearColor];
    [gallerybt addTarget:self action:@selector(gallery_func:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:gallerybt];
    
    UIButton *helpbt = [UIButton buttonWithType:UIButtonTypeCustom];
    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[ver objectAtIndex:0] intValue] >= 7)
        helpbt.frame = CGRectMake(15, self.view.bounds.size.height-40, 290, 30);
    else
        helpbt.frame = CGRectMake(15, self.view.bounds.size.height-35, 290, 30);
    helpbt.layer.cornerRadius=5;
    helpbt.titleLabel.font=[UIFont fontWithName:@"ProximaNova-Regular" size:18];
    [helpbt setTitle:@"Back" forState:UIControlStateNormal];
    [helpbt setTitleColor:[UIColor colorWithRed:17.0f/255.0f green:61.0f/255.0f blue:125.0f/255.0f alpha:1] forState:UIControlStateNormal];
    helpbt.backgroundColor=[UIColor clearColor];
    [helpbt addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:helpbt];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)goBack: (id)sender
{
    
    CATransition *transition = [CATransition animation];
    
    transition.duration = 0.4f;
    
    transition.type = kCATransitionFade;
    
    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)camera_func:(id)sender
{
    DebugLog(@"camera func");
    if ([self isCameraAvailable] && [self doesCameraSupportTakingPhotos]) {
        controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypeCamera;
        if ([self isFrontCameraAvailable]) {
            controller.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        }
        NSMutableArray *mediaTypes = [[NSMutableArray alloc] init];
        [mediaTypes addObject:(__bridge NSString *)kUTTypeImage];
        controller.mediaTypes = mediaTypes;
        controller.delegate = self;
        
        //        if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0f) {
        //            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        //                [self openPhotoPicker];
        //            }];
        //        }
        //        else
        {
            [self presentViewController:controller
                               animated:YES
                             completion:^(void){
                                 DebugLog(@"Picker View Controller is presented");
                             }];
        }
    }
}


-(void)openPhotoPicker
{
    [self presentViewController:controller
                       animated:YES
                     completion:^(void){
                         DebugLog(@"Picker View Controller is presented");
                     }];
}

-(void)gallery_func:(id)sender
{
    DebugLog(@"gallery func");
    if ([self isPhotoLibraryAvailable]) {
        controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        NSMutableArray *mediaTypes = [[NSMutableArray alloc] init];
        [mediaTypes addObject:(__bridge NSString *)kUTTypeImage];
        controller.mediaTypes = mediaTypes;
        controller.delegate = self;
        //        controller.allowsEditing=YES;
        [self presentViewController:controller
                           animated:YES
                         completion:^(void){
                             DebugLog(@"Picker View Controller is presented");
                         }];
    }
    
}

- (void)imageCropper:(VPImageCropperViewController *)cropperViewController didFinished:(UIImage *)editedImage {
    
    DebugLog(@"edited img: %f %f",editedImage.size.width, editedImage.size.height);
    
    portraitImg1 = editedImage;
    
    zipimage.image = editedImage;
    zipimage.contentMode=UIViewContentModeScaleAspectFill;
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
        // TO DO
    }];
    timeraa = [NSTimer scheduledTimerWithTimeInterval:1.0
                                               target:self
                                             selector:@selector(targetMethod:)
                                             userInfo:nil
                                              repeats:YES];
    
    
    NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(editedImage)];
    //    NSData *plainData = [plainString dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64String = [imageData base64EncodedStringWithOptions:0];
    //    DebugLog(@"%@", base64String); // Zm9
    DebugLog(@"Encoded text: %@", base64String);
    
    //    NSString *encodedText = [self encodeToPercentEscapeString:base64String];
    
    //    NSString *strImageData = [base64String stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    
    [[NSUserDefaults standardUserDefaults]setObject:base64String forKey:@"r_prof_image"];
    
}

- (void)imageCropperDidCancel:(VPImageCropperViewController *)cropperViewController {
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
    }];
}

-(void)targetMethod: (NSTimer *)timer
{
    ConAccountBeingMadeViewController *con= [[ConAccountBeingMadeViewController alloc]init];
    con.portraitImg= portraitImg1;
    [self.navigationController pushViewController:con animated:NO];
    [timeraa invalidate];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^() {
        UIImage *portraitImg = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        portraitImg = [self imageByScalingToMaxSize:portraitImg];
        // present the cropper view controller
        VPImageCropperViewController *imgCropperVC = [[VPImageCropperViewController alloc] initWithImage:portraitImg cropFrame:CGRectMake(35, (self.view.frame.size.height- (self.view.frame.size.width-70))/2, self.view.frame.size.width-70, self.view.frame.size.width-70) limitScaleRatio:3.0];
        imgCropperVC.delegate = self;
        [self presentViewController:imgCropperVC animated:YES completion:^{
            // TO DO
        }];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:^(){
    }];
}

#pragma mark image scale utility
//- (UIImage *)imageByScalingToMaxSize:(UIImage *)sourceImage {
//    if (sourceImage.size.width < ORIGINAL_MAX_WIDTH) return sourceImage;
//    CGFloat btWidth = 0.0f;
//    CGFloat btHeight = 0.0f;
//    if (sourceImage.size.width > sourceImage.size.height) {
//        btHeight = ORIGINAL_MAX_WIDTH;
//        btWidth = sourceImage.size.width * (ORIGINAL_MAX_WIDTH / sourceImage.size.height);
//    } else {
//        btWidth = ORIGINAL_MAX_WIDTH;
//        btHeight = sourceImage.size.height * (ORIGINAL_MAX_WIDTH / sourceImage.size.width);
//    }
//    CGSize targetSize = CGSizeMake(btWidth, btHeight);
//    return [self imageByScalingAndCroppingForSourceImage:sourceImage targetSize:targetSize];
//}


- (UIImage *)imageByScalingToMaxSize:(UIImage *)sourceImage {
    if (sourceImage.size.height < ORIGINAL_MAX_HEIGHT) return sourceImage;
    CGFloat btWidth = 0.0f;
    CGFloat btHeight = 0.0f;
    if (sourceImage.size.height > sourceImage.size.width) {
        btHeight = ORIGINAL_MAX_HEIGHT;
        btWidth = sourceImage.size.width * (ORIGINAL_MAX_HEIGHT / sourceImage.size.height);
    } else {
        btHeight = ORIGINAL_MAX_HEIGHT;
        btWidth = sourceImage.size.width * (ORIGINAL_MAX_HEIGHT / sourceImage.size.height);
    }
    CGSize targetSize = CGSizeMake(btWidth, btHeight);
    return [self imageByScalingAndCroppingForSourceImage:sourceImage targetSize:targetSize];
}


- (UIImage *)imageByScalingAndCroppingForSourceImage:(UIImage *)sourceImage targetSize:(CGSize)targetSize {
    DebugLog(@"called scaling&croppingforsize %f %f",targetSize.width,targetSize.height);
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    
    
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    if (CGSizeEqualToSize(imageSize, targetSize) == NO)
    {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor > heightFactor)
            scaleFactor = widthFactor; // scale to fit height
        else
            scaleFactor = heightFactor; // scale to fit width
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else
            if (widthFactor < heightFactor)
            {
                thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
            }
    }
    UIGraphicsBeginImageContext(targetSize); // this will crop
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil) DebugLog(@"could not scale image");
    
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - UINavigationControllerDelegate
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}


- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
}

#pragma mark camera utility
- (BOOL) isCameraAvailable{
    return [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
}

- (BOOL) isRearCameraAvailable{
    return [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear];
}

- (BOOL) isFrontCameraAvailable {
    return [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront];
}

- (BOOL) doesCameraSupportTakingPhotos {
    return [self cameraSupportsMedia:(__bridge NSString *)kUTTypeImage sourceType:UIImagePickerControllerSourceTypeCamera];
}

- (BOOL) isPhotoLibraryAvailable{
    return [UIImagePickerController isSourceTypeAvailable:
            UIImagePickerControllerSourceTypePhotoLibrary];
}
- (BOOL) canUserPickVideosFromPhotoLibrary{
    return [self
            cameraSupportsMedia:(__bridge NSString *)kUTTypeMovie sourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}
- (BOOL) canUserPickPhotosFromPhotoLibrary{
    return [self
            cameraSupportsMedia:(__bridge NSString *)kUTTypeImage sourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}

- (BOOL) cameraSupportsMedia:(NSString *)paramMediaType sourceType:(UIImagePickerControllerSourceType)paramSourceType{
    __block BOOL result = NO;
    if ([paramMediaType length] == 0) {
        return NO;
    }
    NSArray *availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:paramSourceType];
    [availableMediaTypes enumerateObjectsUsingBlock: ^(id obj, NSUInteger idx, BOOL *stop) {
        NSString *mediaType = (NSString *)obj;
        if ([mediaType isEqualToString:paramMediaType]){
            result = YES;
            *stop= YES;
        }
    }];
    return result;
}

-(NSString*) encodeToPercentEscapeString:(NSString *)string {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                              (CFStringRef) string,
                                                              NULL,
                                                              (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
}
@end