//
//  ConGroupSettingsViewController.m
//  Contacter
//
//  Created by ios on 10/01/15.
//  Copyright (c) 2015 Esolz. All rights reserved.
//

#import "ConGroupSettingsViewController.h"
#import "ConGroupsViewController.h"
#import "GroupInfoAdminViewController.h"
#import "AppDelegate.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

#define ORIGINAL_MAX_HEIGHT 450.0f

@interface ConGroupSettingsViewController ()
{
    UIActivityIndicatorView *act;
    UIView *mainview,*blackBack,*grayView,*additionalview,*tapbgview,*change;
    UIButton *back,*save,*remove,*edit;
    UIImageView *prof,*bck_img,*detailimg;
    UIImage *portraitImg1;
    UITextField *group_name;
    UITextView *descp;
    UILabel *backlbl,*admin_lbl,*adminlbl;
    NSMutableDictionary *groupdict,*groupdetail,*hold,*groupdict1;
    NSMutableArray *detail;
    NSMutableData *responseData;
    UITableView *admin;
    BOOL isAtLeast7,isAtLeast8;
    UIActionSheet *actionsheet;
    int picuploaded;
    CGSize targetSize;
    NSString *groups_url,*base64String,*version,*err_str,*pathss;
    AppDelegate *appDel;
    
}
@end

@implementation ConGroupSettingsViewController
@synthesize group_id,adminname,groupname,membercount;


- (BOOL)hidesBottomBarWhenPushed {
    
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
     NSLog(@"From ConGroupSettingsViewController");
    self.view.backgroundColor = [UIColor whiteColor];
    //    self.tabBarController.tabBar.frame = CGRectMake(0, [[UIScreen mainScreen]bounds].size.height-54, 0, 0);
    self.tabBarController.tabBar.hidden = YES;
    
    blackBack = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];//79.6)];
    //[blackBack setBackgroundColor:[UIColor blackColor]];
    [self.view addSubview:blackBack];
    
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
    
    // Add the gradient to the view
    [blackBack.layer insertSublayer:gradient atIndex:0];
    
    
    bck_img=[[UIImageView alloc]initWithFrame:CGRectMake(15,32,12,20)];//(13, 39, 10, 22)];
    bck_img.image=[UIImage imageNamed:@"back3"];
    [blackBack addSubview:bck_img];
    
    backlbl=[[UILabel alloc]initWithFrame:CGRectMake(bck_img.frame.origin.x+bck_img.frame.size.width+5, 28, 54, 30)];
    backlbl.text=@"Back";
    backlbl.textColor=[UIColor whiteColor];
    backlbl.font=[UIFont fontWithName:@"ProximaNova-Regular" size:20];
    backlbl.backgroundColor=[UIColor clearColor];
    [blackBack addSubview:backlbl];
    
    back=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 150, 64)];
    [back setTitle:@"" forState:UIControlStateNormal];
    back.backgroundColor=[UIColor clearColor];
        [back addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDown];
        [back addTarget:self action:@selector(changecoloragain) forControlEvents:UIControlEventTouchDragExit];
        [back addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDragEnter];
    [back addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [blackBack addSubview:back];
    
    save=[[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width-70, 29, 60, 30)];
    [save setTitle:@"Save" forState:UIControlStateNormal];
    save.titleLabel.font=[UIFont fontWithName:@"ProximaNova-Regular" size:20];
    [save addTarget:self action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
    save.backgroundColor=[UIColor clearColor];
    [blackBack addSubview:save];
    
    
        
}

- (void) Start
{
    act.hidden = NO;
    [act startAnimating];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    appDel = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    picuploaded=0;
    
    version = [[UIDevice currentDevice] systemVersion];
    DebugLog(@"version: %@",version);
    isAtLeast7 = [version floatValue] < 8.0;
    isAtLeast8 = [version floatValue] >= 8.0;
    
    
    if (hold.count==0)
    {
        hold=[[NSMutableDictionary alloc] init];
        //hold=[groupdetail mutableCopy];
        DebugLog(@"hold: %@",hold);
        
        act = [[UIActivityIndicatorView alloc] init];
        act.center = self.view.center;
        act.hidesWhenStopped=YES;
        act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        [act setColor:[UIColor blackColor]];
        act.backgroundColor=[UIColor blueColor];
        [self.view addSubview:act];
        act.layer.zPosition=2;
        [NSThread detachNewThreadSelector: @selector(Start) toTarget:self withObject:nil];
        
        //[act startAnimating];
        
        NSOperationQueue *queue = [NSOperationQueue new];
        NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                            initWithTarget:self
                                            selector:@selector(loaddata)
                                            object:nil];
        [queue addOperation:operation];
        
    }
    
    else
       [self datadisplay];
    
    
    
}

-(void)changecoloragain
{
    change.backgroundColor=[UIColor clearColor];
    backlbl.alpha = 0.5f;
    bck_img.alpha = 0.5f;
}
-(void)changecolor
{
    change.backgroundColor=[UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0];
    backlbl.alpha = 0.5f;
    bck_img.alpha = 0.5f;
}


- (void)loaddata
{
    groupdict=[[NSMutableDictionary alloc]init];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    groups_url =[NSString stringWithFormat:@"https://budnav.com/ext/?action=group-get&group_id=%@&image=%d&access_token=%@&device_id=%@",group_id,true,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
    DebugLog(@"groups url string ------ %@",groups_url);
    
    NSError *error1=nil;
    @try {
        NSData *data1=[NSData dataWithContentsOfURL:[NSURL URLWithString:groups_url]options:NSDataReadingUncached error:&error1];
        if (data1==nil)
        {
            DebugLog(@"no connnnn");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                                            message:nil
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            alert.tag=5;
            [alert show];
            
            
            
        }
        
        if (data1 != nil)
            groupdict=[NSJSONSerialization JSONObjectWithData:data1 //1
                                                      options:kNilOptions
                                                        error:&error1];
        DebugLog(@"-----Groups info ----- %@",groupdict);
        
    }
    @catch (NSException *exception) {
        DebugLog(@"exception %@",exception);
    }
    [self performSelectorOnMainThread:@selector(datadisplay) withObject:nil waitUntilDone:NO];
}


- (void)datadisplay
{
    
    
    
    [act removeFromSuperview];
    groupdetail=[[NSMutableDictionary alloc]init];
    groupdetail=[groupdict valueForKey:@"details"];
    DebugLog(@"group details: %@",groupdetail);
    if (hold.count==0)
    {
        //hold=[[NSDictionary alloc] init];
        hold=[groupdetail mutableCopy];
        DebugLog(@"hold1: %@",hold);
    }
    
    detail=[[NSMutableArray alloc]init];
    if (groupdetail.count==0)
    {
        groupdetail=[hold mutableCopy];
        DebugLog(@"group details1: %@",groupdetail);
        
    }
    detail=[groupdetail mutableCopy];
    
    grayView=[[UIView alloc]initWithFrame:CGRectMake(0, blackBack.frame.origin.y+blackBack.frame.size.height, self.view.frame.size.width, 107)];
    grayView.backgroundColor=[UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0];
    grayView.hidden=NO;
    [self.view addSubview:grayView];
    
    
    prof=[[UIImageView alloc]initWithFrame:CGRectMake(15, 11, 86, grayView.frame.size.height-22.0)];//84)];
    prof.layer.cornerRadius=2;
    prof.layer.masksToBounds=YES;
    //prof.backgroundColor=[UIColor redColor];
    //prof.image=[UIImage imageNamed:@"Change Group Picture.png"];
    [grayView addSubview:prof];
    prof.userInteractionEnabled=YES;
    
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(grouppic:)];
    [prof addGestureRecognizer:tap];
    
    
    UILabel *label1=[[UILabel alloc]initWithFrame:CGRectMake(115, 19, 230, 20)];
    label1.text=@"Once you choose another";
    label1.textColor=[UIColor colorWithRed:101.0/255.0 green:101.0/255.0 blue:101.0/255.0 alpha:1.0];
    label1.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    [grayView addSubview:label1];
    
    UILabel *label2=[[UILabel alloc]initWithFrame:CGRectMake(115, 42, 230, 20)];
    label2.text=@"admin, you will lose";
    label2.textColor=[UIColor colorWithRed:101.0/255.0 green:101.0/255.0 blue:101.0/255.0 alpha:1.0];//colorWithRed:114.0/255.0 green:114.0/255.0 blue:114.0/255.0 alpha:1.0];
    label2.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    [grayView addSubview:label2];
    
    UILabel *label3=[[UILabel alloc]initWithFrame:CGRectMake(115, 65, 230, 20)];
    label3.text=@"control over the group!";
    label3.textColor=[UIColor colorWithRed:101.0/255.0 green:101.0/255.0 blue:101.0/255.0 alpha:1.0];
    label3.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    [grayView addSubview:label3];
    
    
    mainview = [[UIView alloc]initWithFrame:CGRectMake(0,grayView.frame.origin.y+grayView.frame.size.height, self.view.frame.size.width, [UIScreen mainScreen].bounds.size.height)];
    mainview.backgroundColor=[UIColor whiteColor];
    mainview.hidden=NO;
    [self.view addSubview:mainview];
    
    additionalview=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 19, 50)];
    
    group_name=[[UITextField alloc]initWithFrame:CGRectMake(0, 0, mainview.frame.size.width, 57.5)];
    UIColor *color = [UIColor blackColor];
    group_name.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Group Name" attributes:@{NSForegroundColorAttributeName: color}];
    group_name.delegate=self;
    
    if (![appDel.groupnameUpdate isKindOfClass:[NSNull class]] && ![appDel.groupnameUpdate isEqualToString:@""] && ![appDel.groupnameUpdate isEqualToString:@"(null)"] && appDel.groupnameUpdate!=nil)
    {
        if (![appDel.groupnameUpdate isEqualToString:[groupdetail valueForKey:@"name"]])
        {
            group_name.text = [NSString stringWithFormat:@"%@",appDel.groupnameUpdate];
        }
        else
        {
            group_name.text=[NSString stringWithFormat:@"%@",[groupdetail valueForKey:@"name"]];
        }
    }
    else
    {
    group_name.text=[NSString stringWithFormat:@"%@",[groupdetail valueForKey:@"name"]];
    }
    group_name.userInteractionEnabled=YES;
    group_name.autocorrectionType=UITextAutocorrectionTypeNo;
    group_name.keyboardAppearance=UIKeyboardAppearanceDark;
    group_name.leftView=additionalview;
    //group_name.backgroundColor=[UIColor redColor];
    //group_name.leftView.userInteractionEnabled=YES;
    group_name.leftViewMode=UITextFieldViewModeAlways;
    //group_name.leftViewMode=UITextFieldViewModeNever;
    
    group_name.font=[UIFont fontWithName:@"ProximaNova-Regular" size:18];
    [mainview addSubview:group_name];
    
    UILabel *separatorlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 57.5, [UIScreen mainScreen].bounds.size.width, 0.3)];
    separatorlabel.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"divider-acset@2x.png"]];
    //separatorlabel.backgroundColor=[UIColor lightGrayColor];
    [mainview addSubview:separatorlabel];
    
    
    
    descp=[[UITextView alloc]initWithFrame:CGRectMake(0, 57.9, mainview.frame.size.width, 154.4)];
    descp.delegate=self;
    descp.keyboardAppearance = UIKeyboardAppearanceDark;
    descp.autocorrectionType=UITextAutocorrectionTypeNo;
    descp.font = [UIFont fontWithName:@"ProximaNova-Regular" size:18];
    descp.textColor=[UIColor blackColor];
    if ([[NSString stringWithFormat:@"%@",[groupdetail valueForKey:@"description"]] isEqualToString:@""]) {
        descp.text = @"Description";
    }
    else
        descp.text = [NSString stringWithFormat:@"%@",[groupdetail valueForKey:@"description"]];
    descp.backgroundColor=[UIColor clearColor];
    descp.textContainerInset = UIEdgeInsetsMake(19, 15, 0, 0);//UIEdgeInsetsMake(0, 9, 0, 0);
    descp.scrollEnabled=YES;
    descp.pagingEnabled = YES;
    descp.editable = YES;
    [mainview addSubview:descp];
    
    UILabel *separatorlabel1=[[UILabel alloc]initWithFrame:CGRectMake(0, 212.3, [UIScreen mainScreen].bounds.size.width, 0.4)];
    separatorlabel1.backgroundColor=[UIColor lightGrayColor];
    [mainview addSubview:separatorlabel1];
    
    
    admin=[[UITableView alloc]initWithFrame:CGRectMake(0, 212.7, [UIScreen mainScreen].bounds.size.width, 59.3)];
    admin.backgroundColor=[UIColor clearColor];
    admin.delegate=self;
    admin.dataSource=self;
    admin.bounces=NO;
    //admin.backgroundColor=[UIColor redColor];
    admin.separatorStyle=UITableViewCellSeparatorStyleNone;
    admin.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    [mainview addSubview:admin];
    
    UILabel *separatorlabel2=[[UILabel alloc]initWithFrame:CGRectMake(0, 272, [UIScreen mainScreen].bounds.size.width, 0.4)];
    separatorlabel2.backgroundColor=[UIColor lightGrayColor];
    [mainview addSubview:separatorlabel2];
    
    
    remove=[[UIButton alloc]initWithFrame:CGRectMake(15, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.width-30, 46)];
    [remove setImage:[UIImage imageNamed:@"Remove Group.png"] forState:UIControlStateNormal];
    remove.tag=6;
    remove.contentMode=UIViewContentModeScaleAspectFit;
    [remove addTarget:self action:@selector(rem) forControlEvents:UIControlEventTouchUpInside];
    remove.layer.zPosition=5;
    [mainview addSubview:remove];
    
    if ([groupdetail valueForKey:@"image" ]!=nil)
    {
        base64String= [NSString stringWithFormat:@"%@",[groupdetail valueForKey:@"image"]];
        if ([base64String length] >6)
        {
            NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
            //    NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
            prof.image = [UIImage imageWithData:decodedData];
                   }
        
    }
    prof.userInteractionEnabled=YES;
    
}


#pragma mark- show detail image
-(void)grouppic: (UIGestureRecognizer *)sender
{
    [act startAnimating];
    [self selectphoto];
    
}


#pragma mark-actionsheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    
    if  ([buttonTitle isEqualToString:@"Open Camera"])
    {
        [self takePhoto];
    }
    if ([buttonTitle isEqualToString:@"Select from Gallery"])
    {
        [self selectphoto];
    }
}

#pragma mark- method to take photo
-(void)takePhoto
{
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        if (isAtLeast7)
        {
            UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                  message:@"Device has no camera"
                                                                 delegate:nil
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles: nil];
            
            [myAlertView show];
        }
        
        if (isAtLeast8)
        {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@"Error"
                                                  message:@"Device has no camera"
                                                  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* cancel = [UIAlertAction
                                     actionWithTitle:@"Ok"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [alertController dismissViewControllerAnimated:YES completion:nil];
                                         
                                     }];
            
            
            [alertController addAction:cancel];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        
    }
    else
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate=self;
        picker.allowsEditing =YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
}


#pragma mark - UINavigationControllerDelegate
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}


- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
}


#pragma mark-method to select photo from gallery
-(void)selectphoto
{
    [act startAnimating];
    [self performSelector:@selector(Start) withObject:nil afterDelay:0];
    //[self dismissViewControllerAnimated:YES completion:nil];
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate=self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [act stopAnimating];
    
    [self presentViewController:picker animated:NO completion:nil];
}



#pragma mark - Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^{
        UIImage *portraitImg = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        portraitImg = [self imageByScalingToMaxSize:portraitImg];
        
        DebugLog(@"%@",NSStringFromCGSize(portraitImg.size));
        
        pathss = [NSTemporaryDirectory() stringByAppendingPathComponent:@"image.png"];
        NSData *imageData = UIImagePNGRepresentation(portraitImg);
        //you can also use UIImageJPEGRepresentation(img,1); for jpegs
        [imageData writeToFile:pathss atomically:YES];
        
        // present the cropper view controller
        ImageCropperViewController *imgCropperVC = [[ImageCropperViewController alloc] initWithImage:portraitImg cropFrame:CGRectMake(35, (self.view.frame.size.height- (self.view.frame.size.width-70))/2, self.view.frame.size.width-70, self.view.frame.size.width-70)];
        imgCropperVC.delegate = self;
        [self presentViewController:imgCropperVC animated:YES completion:nil];
        
            }];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

#pragma mark- method to resize photo
- (UIImage *)imageByScalingToMaxSize:(UIImage *)sourceImage {
    CGFloat btWidth = 0.0f;
    CGFloat btHeight = 0.0f;
    DebugLog(@"%f",sourceImage.size.height);
    if (sourceImage.size.height < ORIGINAL_MAX_HEIGHT)
               return sourceImage;
    
    
    
    if (sourceImage.size.height > sourceImage.size.width) {
        //btHeight = sourceImage.size.width;
        btHeight = ORIGINAL_MAX_HEIGHT;
        btWidth = sourceImage.size.width* (ORIGINAL_MAX_HEIGHT / sourceImage.size.height);
    }
    else
    {
        btHeight = ORIGINAL_MAX_HEIGHT;
        //btWidth=sourceImage.size.height;
        btWidth = sourceImage.size.width * (ORIGINAL_MAX_HEIGHT / sourceImage.size.height);
    }
    //    if (btWidth>self.view.frame.size.width) {
    //        btWidth=self.view.frame.size.width;
    //    }
    targetSize = CGSizeMake(btWidth, btHeight);
    //return sourceImage;
    return [self imageByScalingAndCroppingForSourceImage:sourceImage targetSize:targetSize];
    
    
}


- (UIImage *)imageByScalingAndCroppingForSourceImage:(UIImage *)sourceImage targetSize:(CGSize)targetSize1 {
    DebugLog(@"called scaling&croppingforsize");
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize1.width;
    CGFloat targetHeight = targetSize1.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    if (CGSizeEqualToSize(imageSize, targetSize1) == NO)
    {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor > heightFactor)
            scaleFactor = widthFactor; // scale to fit height
        else
            scaleFactor = heightFactor; // scale to fit width
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else
            if (widthFactor < heightFactor)
            {
                thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
            }
    }
    UIGraphicsBeginImageContext(targetSize1); // this will crop
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil) DebugLog(@"could not scale image");
    
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    return newImage;
}



#pragma mark- method to change to base64 string
- (NSString*)base64forData:(NSData*)theData {
    
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}


#pragma mark-
- (void)imageCropper:(VPImageCropperViewController *)cropperViewController didFinished:(UIImage *)editedImage
{
    act = [[UIActivityIndicatorView alloc] init];
    act.center = cropperViewController.view.center;
    act.hidesWhenStopped=YES;
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [act setColor:[UIColor blackColor]];
    act.backgroundColor=[UIColor blueColor];
    [cropperViewController.view addSubview:act];
    act.layer.zPosition=2;
    [NSThread detachNewThreadSelector: @selector(Start) toTarget:self withObject:nil];
    
    portraitImg1 =editedImage;
    //prof.contentMode=UIViewContentModeScaleAspectFill;
    NSData *imageData = UIImageJPEGRepresentation(portraitImg1,1.0);
    NSString *encodedString = [self base64forData:imageData];
    groupdict1=[[NSMutableDictionary alloc]init];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    groups_url =[NSString stringWithFormat:@"https://budnav.com/ext/?action=group-editpicture&group_id=%@&access_token=%@&device_id=%@",group_id,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
    DebugLog(@"groups url string ------ %@",groups_url);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:groups_url]];
    
    NSString *params = [[NSString alloc] initWithFormat:@"image=%@",[encodedString stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"]];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    
    
    
    
    if(theConnection)
    {
        //[act removeFromSuperview];
        responseData = [NSMutableData data];
    }
    else
    {
        
    }
    
    
}

- (void)imageCropperDidCancel:(VPImageCropperViewController *)cropperViewController {
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
    }];
}





#pragma mark-back button
-(void)back
{
    //DebugLog(@"123456789101112");
    
    CATransition *transition = [CATransition animation];
    
    transition.duration = 0.4f;
   // transition.type = kCATransitionPush;
    transition.type = kCATransitionFade;
   // transition.type = kCATransitionFromLeft;
    
    
    
    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark-work of save button
-(void)save
{
  
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if ([descp.text isEqualToString:@"Description"]||[descp.text isEqualToString:@""])
    {
        groups_url =[NSString stringWithFormat:@"https://budnav.com/ext/?action=group-edit&group_id=%@&group_name=%@&access_token=%@&device_id=%@",group_id,[group_name.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
        
    }
    else
    {
        groups_url =[NSString stringWithFormat:@"https://budnav.com/ext/?action=group-edit&group_id=%@&group_name=%@&group_description=%@&access_token=%@&device_id=%@",group_id,[group_name.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[descp.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
}
    DebugLog(@"groups url string ------ %@",groups_url);
    
    NSError *error1=nil;
    @try {
        NSData *data1=[NSData dataWithContentsOfURL:[NSURL URLWithString:groups_url]options:NSDataReadingUncached error:&error1];
        if (data1==nil)
        {
            DebugLog(@"no connnnn");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                                            message:nil
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            alert.tag=5;
            [alert show];
            
            
            
        }
        
        if (data1 != nil)
            groupdict=[NSJSONSerialization JSONObjectWithData:data1 //1
                                                      options:kNilOptions
                                                        error:&error1];
        DebugLog(@"-----Groups info ----- %@",groupdict);
        NSString *check=[NSString stringWithFormat:@"%@",[groupdict valueForKey:@"success"]];
        
        
        
        
        if ([check isEqualToString:@"1"])
        {
            //[loader removeFromSuperview];
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Successfully Saved!" message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            
            [alert show];
            
            GroupInfoAdminViewController *grpAdmin = [[GroupInfoAdminViewController alloc] init];
            grpAdmin.group_update = YES;
            
        }
        else if ([check isEqualToString:@"0"])
        {
           // [loader removeFromSuperview];
            if (isAtLeast8)
            {
                UIAlertController * alert=   [UIAlertController
                                              alertControllerWithTitle:@"Warning"
                                              message:[NSString stringWithFormat:@"%@",[groupdict valueForKey:@"error"]]
                                              preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* cancel = [UIAlertAction
                                         actionWithTitle:@"Ok"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                             
                                         }];
                
                [alert addAction:cancel];
                
                [self presentViewController:alert animated:YES completion:nil];
                
            }
            
            if (isAtLeast7)
            {
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Warning" message:[NSString stringWithFormat:@"%@",[groupdict valueForKey:@"error"]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                
                [alert show];
            }
        }
        
    }
    @catch (NSException *exception) {
        DebugLog(@"exception %@",exception);
    }
    
}


#pragma mark-work of remove button
-(void)rem
{
    if (isAtLeast8)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil message:@"Are you sure you want to remove the group?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                                     [[NSNotificationCenter defaultCenter] removeObserver:self
                                                                                     name:UITextFieldTextDidChangeNotification
                                                                                   object:nil];
                                     
                                 }];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                             {
                                 [[NSNotificationCenter defaultCenter] removeObserver:self
                                                                                 name:UITextFieldTextDidChangeNotification
                                                                               object:nil];
                                 groupdict=[[NSMutableDictionary alloc]init];
                                 NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                                 groups_url =[NSString stringWithFormat:@"https://budnav.com/ext/?action=group-remove&confirm=true&group_id=%@&access_token=%@&device_id=%@",group_id,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
                                 DebugLog(@"groups url string ------ %@",groups_url);
                                 
                                 
                                 @try {
                                     NSError *error1=nil;
                                     NSData *data1=[NSData dataWithContentsOfURL:[NSURL URLWithString:groups_url]options:NSDataReadingUncached error:&error1];
                                     if (data1==nil)
                                     {
                                         DebugLog(@"no connnnn");
                                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                                                                         message:nil
                                                                                        delegate:self
                                                                               cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                                         alert.tag=5;
                                         [alert show];
                                         
                                         
                                         
                                     }
                                     
                                     if (data1 != nil)
                                         groupdict=[NSJSONSerialization JSONObjectWithData:data1 //1
                                                                                   options:kNilOptions
                                                                                     error:&error1];
                                     DebugLog(@"-----Groups info ----- %@",groupdict);
                                 }
                                 @catch (NSException *exception) {
                                     DebugLog(@"exception %@",exception);
                                 }
                                 if ([[NSString stringWithFormat:@"%@",[groupdict valueForKey:@"success"]] isEqualToString:@"1"])
                                 {
                                     ConGroupsViewController *congrp=[[ConGroupsViewController alloc] init];
                                     
//                                     CATransition* transition = [CATransition animation];
//                                     
//                                     transition.duration = 0.4;
//                                     transition.type = kCATransitionPush;
//                                     transition.subtype = kCATransitionFade;
//                                     transition.subtype = kCATransitionFromRight;
//                                     
//                                     [[self navigationController].view.layer addAnimation:transition forKey:nil];
                                     
                                     [self.navigationController pushViewController:congrp animated:NO];
                                 }
                                 
                             }];
        
        [alert addAction:cancel];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    if (isAtLeast7)
    {
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:nil
                                                              message:@"Are you sure you want to remove the group?"
                                                             delegate:self
                                                    cancelButtonTitle:@"CANCEL"
                                                    otherButtonTitles:@"OK" ,nil];
        
        [myAlertView show];
    }
    
}

#pragma mark-uialertview delegate method
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1)
    {
        
        groupdict=[[NSMutableDictionary alloc]init];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        groups_url =[NSString stringWithFormat:@"https://budnav.com/ext/?action=group-remove&confirm=true&group_id=%@&access_token=%@&device_id=%@",group_id,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
        DebugLog(@"groups url string ------ %@",groups_url);
        
        NSError *error1=nil;
        @try {
            NSData *data1=[NSData dataWithContentsOfURL:[NSURL URLWithString:groups_url]options:NSDataReadingUncached error:&error1];
            if (data1==nil)
            {
                DebugLog(@"no connnnn");
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                                                message:nil
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                alert.tag=5;
                [alert show];
                
                
                
            }
            
            if (data1 != nil)
                groupdict=[NSJSONSerialization JSONObjectWithData:data1 //1
                                                          options:kNilOptions
                                                            error:&error1];
            DebugLog(@"-----Groups info ----- %@",groupdict);
        }
        @catch (NSException *exception) {
            DebugLog(@"exception %@",exception);
        }
        
        if ([[NSString stringWithFormat:@"%@",[groupdict valueForKey:@"success"]] isEqualToString:@"1"])
        {
            ConGroupsViewController *congrp=[[ConGroupsViewController alloc] init];
            
//            CATransition* transition = [CATransition animation];
//            
//            transition.duration = 0.4;
//            transition.type = kCATransitionPush;
//            transition.subtype = kCATransitionFade;
//            
//            [[self navigationController].view.layer addAnimation:transition forKey:nil];
            
            [self.navigationController pushViewController:congrp animated:YES];
        }
        
        
    }
    
}


#pragma mark-nsurlconnection delegates
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data
{
    [responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    DebugLog(@"connection failed");
    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"backupstatus"];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    DebugLog(@"connection finished loading");
    
    DebugLog(@"response data - %@", [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
    NSError *error;
    
    if (responseData != nil)
    {
        groupdict = [NSJSONSerialization JSONObjectWithData:responseData //1
                                                    options:kNilOptions
                                                      error:&error];
        DebugLog(@"deny json returns: %@",groupdict);
        
        NSString *errornumber= [NSString stringWithFormat:@"%@",[groupdict objectForKey:@"errorno"]];
        DebugLog(@"err  %@",errornumber);
        if (![errornumber isEqualToString:@"0"])
        {
            err_str = [NSString stringWithFormat:@"%@",[groupdict objectForKey:@"error"]];
            DebugLog(@"error %@",err_str);
            
            if (isAtLeast7)
            {
                UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                      message:err_str
                                                                     delegate:nil
                                                            cancelButtonTitle:@"OK"
                                                            otherButtonTitles: nil];
                
                [myAlertView show];
            }
            
            
            if (isAtLeast8)
            {
                UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"Error" message:err_str preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* cancel = [UIAlertAction
                                         actionWithTitle:@"Ok"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                             
                                         }];
                
                
                [alert addAction:cancel];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
        else
        {
            //            [self dismissViewControllerAnimated:YES completion:nil];
            if ([[groupdict objectForKey:@"success"]intValue] == 1)
            {
                [hold removeAllObjects];
                [act stopAnimating];
                [self dismissViewControllerAnimated:YES completion:nil];
                grayView.hidden=YES;
                mainview.hidden=YES;
                
            }
        }
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    responseData = [NSMutableData data];
}





#pragma mark-dismiss alertcontroller
- (void)didEnterBackground:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UITextFieldTextDidChangeNotification
                                                  object:nil];
    
    [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
}
#pragma mark-dismiss textview keyboard
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}


#pragma mark- dismiss keyboard
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    DebugLog(@"Dismiss keyboard");
    [textField resignFirstResponder];
    return YES;
}

#pragma mark-tableview methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [act stopAnimating];
    static NSString *MyIdentifier = @"MyReuseIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    cell=nil;
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:MyIdentifier];
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle=UITableViewCellSelectionStyleGray;
    cell.textLabel.textAlignment=NSTextAlignmentCenter;
    admin_lbl=[[UILabel alloc]initWithFrame:CGRectMake(15, 0, 60, admin.frame.size.height)];
    admin_lbl.text=@"Admin:";
    admin_lbl.font=[UIFont fontWithName:@"ProximaNova-Regular" size:18];
    admin_lbl.textColor=[UIColor grayColor];
    admin_lbl.backgroundColor=[UIColor clearColor];
    [cell addSubview:admin_lbl];
    
    adminlbl=[[UILabel alloc]initWithFrame:CGRectMake(80, 0, 254, admin.frame.size.height)];
    adminlbl.text=adminname;
    adminlbl.font=[UIFont fontWithName:@"ProximaNova-Regular" size:20];
    adminlbl.textColor=[UIColor blackColor];
    adminlbl.backgroundColor=[UIColor clearColor];
    [cell addSubview:adminlbl];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return admin.frame.size.height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ConGroupChangeAdminViewController *conadm=[[ConGroupChangeAdminViewController alloc]init];
    conadm.groupid=group_id;
    conadm.groupname=groupname;
    conadm.membercount=membercount;
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    transition.subtype = kCATransitionFromRight;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:conadm animated:NO];
}


#pragma mark- textview delegate
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
    //[self animateTextView:textView up: YES];
    if ([textView.text isEqualToString:@"Description"])
    {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
        [textView becomeFirstResponder];
    }
}


- (void)textViewDidEndEditing:(UITextView *)textView
{
    
    if ([textView.text isEqualToString:@""])
    {
        textView.text = @"Description";
        textView.textColor = [UIColor blackColor];
    }
    
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    appDel.groupnameUpdate = textField.text;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
