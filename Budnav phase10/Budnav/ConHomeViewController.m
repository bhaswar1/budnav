//
//  ConHomeViewController.m
//  Budnav
//
//  Created by intel on 17/03/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "ConHomeViewController.h"
#import "ConLoginViewController.h"
#import "ConPreSignUpViewController.h"
#import "ConNewContactsViewController.h"
#import "AppDelegate.h"
#import "ConSignupViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

@interface ConHomeViewController ()
{
    NSTimer *timerr;
}
@end

@implementation ConHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"isloggedin"] isEqualToString:@"yes"])
    {
        ConNewContactsViewController *con =[[ConNewContactsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
        AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        del.tabBarController.selectedIndex = 0;
    }
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate=self;
    locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    locationManager.distanceFilter=kCLDistanceFilterNone;
    
    [locationManager startUpdatingLocation];
    geocoder = [[CLGeocoder alloc] init];
    
    NSUInteger code = [CLLocationManager authorizationStatus];
    if (code == kCLAuthorizationStatusNotDetermined && ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)] || [locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]))
    {
        // choose one request according to your business.
        if([[NSBundle mainBundle]
            objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"]){
            DebugLog(@"NSLocationAlwaysUsageDescription");
            [locationManager requestAlwaysAuthorization];
            
        } else if([[NSBundle mainBundle]
                   objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
            DebugLog(@"NSLocationWhenInUseUsageDescription");
            [locationManager requestWhenInUseAuthorization];
            
        } else {
            DebugLog(@"Info.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription");
        }
    }
    timerr = [NSTimer scheduledTimerWithTimeInterval:10.0
                                              target:self
                                            selector:@selector(targetMethod:)
                                            userInfo:nil
                                             repeats:NO];
    
}


-(void)targetMethod:(NSTimer *)timer{
    
    [locationManager stopUpdatingLocation];
}


- (void)viewDidAppear:(BOOL)animated
{
    UIImageView *background_imgv = [[UIImageView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.origin.x, [UIScreen mainScreen].bounds.origin.y, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    [background_imgv setBackgroundColor:[UIColor clearColor]];
    background_imgv.image = [UIImage imageNamed:@"Background"];
    [self.view addSubview:background_imgv];
    
    
    UIImageView *logo_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-252)/2, [UIScreen mainScreen].bounds.size.height/6.7, 252, 87)];
    [logo_imgv setBackgroundColor:[UIColor clearColor]];
    logo_imgv.image = [UIImage imageNamed:@"Logo"];
    [background_imgv addSubview:logo_imgv];
    
    UIButton *signup_btn = [[UIButton alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/9.6,logo_imgv.frame.origin.y+logo_imgv.frame.size.height+[UIScreen mainScreen].bounds.size.height/7.5,[UIScreen mainScreen].bounds.size.width/1.26, [UIScreen mainScreen].bounds.size.height/11)];
    [signup_btn setBackgroundColor:[UIColor clearColor]];
    [signup_btn setBackgroundImage:[UIImage imageNamed:@"Sign up"] forState:UIControlStateNormal];
    [signup_btn setBackgroundImage:[UIImage imageNamed:@"Sign up active"] forState:UIControlStateSelected];
    [signup_btn setBackgroundImage:[UIImage imageNamed:@"Sign up active"] forState:UIControlStateHighlighted];
    signup_btn.userInteractionEnabled = YES;
    [signup_btn addTarget:self action:@selector(signup:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:signup_btn];
    
    
    UIButton *login_btn = [[UIButton alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/9.6,signup_btn.frame.origin.y+signup_btn.frame.size.height+[UIScreen mainScreen].bounds.size.height/15,[UIScreen mainScreen].bounds.size.width/1.26, [UIScreen mainScreen].bounds.size.height/11)];
    
    [login_btn setBackgroundColor:[UIColor clearColor]];
    
    [login_btn setBackgroundImage:[UIImage imageNamed:@"Login"] forState:UIControlStateNormal];
    [login_btn setBackgroundImage:[UIImage imageNamed:@"Login active"] forState:UIControlStateHighlighted];
    [login_btn setBackgroundImage:[UIImage imageNamed:@"Login active"] forState:UIControlStateSelected];
    login_btn.userInteractionEnabled = YES;
    [login_btn addTarget:self action:@selector(login:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:login_btn];
    
    
    UIButton *forgotPass = [[UIButton alloc] initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-253)/2, login_btn.frame.origin.y+login_btn.frame.size.height+100, 253, 30)];
    forgotPass.backgroundColor = [UIColor clearColor];
    [forgotPass setTitle:@"Forgot password?" forState:UIControlStateNormal];
    [forgotPass setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    forgotPass.titleLabel.textAlignment = NSTextAlignmentCenter;
    //forgotPass.titleLabel.textColor = [UIColor whiteColor];
    forgotPass.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    [forgotPass addTarget:self action:@selector(forgot:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:forgotPass];
    
    DebugLog(@"Forgot AXIS:%lf",forgotPass.frame.origin.x);
    
    
    DebugLog(@"X_AXIS:%f",logo_imgv.frame.origin.x);
    DebugLog(@"RIGHT GAP:%f",[UIScreen mainScreen].bounds.size.width-(logo_imgv.frame.size.width+logo_imgv.frame.origin.x));
    
    
}

- (void)signup:(id)sender
{
    
    ConSignupViewController *sign_up = [[ConSignupViewController alloc] init];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:sign_up animated:YES];
}

- (void)login:(id)sender
{
    ConLoginViewController *log_in = [[ConLoginViewController alloc] init];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:log_in animated:YES];
    
}

- (void)forgot:(id)sender
{
    DebugLog(@"I forgot my password");

    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://budnav.com/?p=password"]];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    DebugLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        DebugLog(@"currentLocation.coordinate.longitude=========>%f",currentLocation.coordinate.longitude);
        DebugLog(@"currentLocation.coordinate.longitude=========>%f",currentLocation.coordinate.latitude);
    }
    //    [locationManager stopUpdatingLocation];
    
    DebugLog(@"Resolving the Address");
    
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemrk, NSError *error) {
        DebugLog(@"Found placemarks: %@, error: %@", placemrk, error);
        if (error == nil && [placemrk count] > 0) {
            placemark = [placemrk lastObject];
            
            DebugLog(@"ADDRESS===================>%@ %@\n%@ %@\n%@\n%@\n%@",placemark.subThoroughfare, placemark.thoroughfare,
                  placemark.postalCode, placemark.locality,
                  placemark.administrativeArea,
                  placemark.country,placemark.ISOcountryCode);
            
            [[NSUserDefaults standardUserDefaults] setObject:placemark.ISOcountryCode forKey:@"placemarkISOCode"];
            [[NSUserDefaults standardUserDefaults] setObject:placemark.country forKey:@"placemarkCode"];
            
            [locationManager stopUpdatingLocation];
        } else {
            DebugLog(@"%@", error.debugDescription);
        }
    }];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    DebugLog(@"didFailWithError: %@", error);
}

@end
