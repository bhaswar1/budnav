//
//  ImageCropperViewController.h
//  Budnav
//
//  Created by Sohini's on 06/02/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ImageCropperViewController;

@protocol ImageCropperDelegate <NSObject>

- (void)imageCropper:(ImageCropperViewController *)cropperViewController didFinished:(UIImage *)editedImage;
- (void)imageCropperDidCancel:(ImageCropperViewController *)cropperViewController;

@end

@interface ImageCropperViewController : UIViewController

@property (nonatomic, assign) NSInteger tag;
@property (nonatomic, assign) CGRect cropFrame;
@property (nonatomic, assign) id<ImageCropperDelegate> delegate;

- (id)initWithImage:(UIImage *)originalImage cropFrame:(CGRect)cropFrame; //limitScaleRatio:(NSInteger)limitRatio;

@end
