//
//  ConAddressSignupViewController.m
//  Budnav
//  Created by Sohini's on 24/03/15.
//  Copyright (c) 2015 esolz. All rights reserved.

#import "ConAddressSignupViewController.h"
#import "ConLoginViewController.h"
#import <AddressBookUI/AddressBookUI.h>
#import <AddressBook/AddressBook.h>
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"
#import "iToast.h"
#import "UIView+Toast.h"
#import "AppDelegate.h"

@interface ConAddressSignupViewController ()
{
    UIActivityIndicatorView *activ;
    ABAddressBookRef addressBookRef;
    NSMutableArray *con_array, *part_array;
    int number_of_iterations;
    
}
@end

@implementation ConAddressSignupViewController
@synthesize ns,add_contacts_dict;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    
    
    coverview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 64)];
    //coverview.backgroundColor = [UIColor blackColor];
    [self.view addSubview:coverview];
    
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
    
    // Add the gradient to the view
    [coverview.layer insertSublayer:gradient atIndex:0];
    
    
    
    
    
    back = [[UIImageView alloc] initWithFrame:CGRectMake(15, 32, 12, 20)];
    back.image = [UIImage imageNamed:@"back3"];
    [coverview addSubview:back];
    
    bck = [[UILabel alloc] initWithFrame:CGRectMake(back.frame.size.width+back.frame.origin.x+5, 32, 40, 20)];
    bck.text=@"Back";
    bck.textColor = [UIColor whiteColor];
    bck.backgroundColor=[UIColor clearColor];
    [coverview addSubview:bck];
    bck.userInteractionEnabled = YES;
    
    backbutton = [[UIButton alloc] initWithFrame:CGRectMake(0, bck.frame.origin.y-5, bck.frame.origin.x+bck.frame.size.width, bck.frame.size.height+10)];
    [backbutton setTitle:nil forState:UIControlStateNormal];
    backbutton.tag=1;
    backbutton.backgroundColor = [UIColor clearColor];
    
    [backbutton addTarget:self action:@selector(changecolor1) forControlEvents:UIControlEventTouchDown];
    [backbutton addTarget:self action:@selector(changecoloragain1) forControlEvents:UIControlEventTouchDragExit];
    [backbutton addTarget:self action:@selector(changecolor1) forControlEvents:UIControlEventTouchDragEnter];
    
    
    
    [backbutton addTarget:self action:@selector(backmethod:) forControlEvents:UIControlEventTouchUpInside];
    [coverview addSubview:backbutton];
    
    
    
    next = [[UIImageView alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-27, 32, 12, 20)];
    next.image= [UIImage imageWithCGImage:back.image.CGImage
                                    scale:back.image.scale
                              orientation:UIImageOrientationUpMirrored];
    [coverview addSubview:next];
    
    nxt = [[UILabel alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-77, 32, 46, 20)];
    nxt.text = @"Finish";
    nxt.textColor = [UIColor whiteColor];
    nxt.backgroundColor = [UIColor clearColor];
    [coverview addSubview:nxt];
    nxt.userInteractionEnabled = YES;
    
    nextbutton = [[UIButton alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-73, 27, 65, 30)];
    [nextbutton setTitle:nil forState:UIControlStateNormal];
    nextbutton.backgroundColor = [UIColor clearColor];
    
    [nextbutton addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDown];
    [nextbutton addTarget:self action:@selector(changecoloragain) forControlEvents:UIControlEventTouchDragExit];
    [nextbutton addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDragEnter];
    
    [nextbutton addTarget:self action:@selector(nextmethod) forControlEvents:UIControlEventTouchUpInside];
    [coverview addSubview:nextbutton];
    
    mainview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, coverview.frame.origin.y+coverview.frame.size.height, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height-coverview.frame.size.height)];
    mainview.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:mainview];
    
    
    DebugLog(@"ASCHE==============>ISO = %@\nCountry = %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"placemarkISOCode"],[[NSUserDefaults standardUserDefaults] objectForKey:@"placemarkCode"]);
    
    NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"countriesflags" ofType:@"json"]];
    NSError *localError = nil;
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
    
    if (localError != nil) {
        DebugLog(@"%@", [localError userInfo]);
    }
    countriesList = (NSArray *)parsedObject;
    
    NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
    countriesList = [countriesList sortedArrayUsingDescriptors:sortDescriptors];
    
    DebugLog(@"FLAGS======================>%@",countriesList);
    
    for (check=0; check < countriesList.count; check++){
        
        if ([[[countriesList objectAtIndex:check]objectForKey:@"code2l"] isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"placemarkISOCode"]]) {
            
            flagCode = [[countriesList objectAtIndex:check]objectForKey:@"flag_128"];
            
        }
    }
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    NSLog(@"hmmmmmmm hmmmmmm");
    self.screenName = @"Sign up address";
    
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createEventWithCategory:@"Home"
                                             action:@"Sign up address"
                                              label:nil
                                              value:nil] build]];
    
    
    
    [[NSOperationQueue new] addOperationWithBlock:^{
        
        addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
        
        ////////////////////////////////// Getting Contacts From Device Phonebook ////////////////////////////////////
        
        CFErrorRef *errorab = nil;
        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, errorab);
        
        __block BOOL accessGranted = NO;
        if (&ABAddressBookRequestAccessWithCompletion != NULL) { // we're on iOS 6
            dispatch_semaphore_t sema = dispatch_semaphore_create(0);
            ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
                accessGranted = granted;
                dispatch_semaphore_signal(sema);
            });
            dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
            //        dispatch_release(sema);
        }
        else { // we're on iOS 5 or older
            accessGranted = YES;
        }
        if (accessGranted) {
            
            NSMutableArray *contactpickarr = [[NSMutableArray alloc]init];
            ABAddressBookRef UsersAddressBook = ABAddressBookCreateWithOptions(NULL, NULL);
            
            //contains details for all the contacts
            CFArrayRef ContactInfoArray = ABAddressBookCopyArrayOfAllPeople(UsersAddressBook);
            
            //get the total number of count of the users contact
            CFIndex numberofPeople = CFArrayGetCount(ContactInfoArray);
            // DebugLog(@"%ld",numberofPeople);
            con_array = [[NSMutableArray alloc]init];
            
            //iterate through each record and add the value in the array
            for (int ik =0; ik<numberofPeople; ik++) {
                ABRecordRef ref = CFArrayGetValueAtIndex(ContactInfoArray, ik);
                ABMultiValueRef names, lastnames;
                if ((ABRecordCopyValue(ref, kABPersonFirstNameProperty) == NULL && ABRecordCopyValue(ref, kABPersonLastNameProperty) == NULL) || ABRecordCopyValue(ref, kABPersonPhoneProperty) == NULL)
                {
                }
                else
                {
                    names = (__bridge ABMultiValueRef)((__bridge NSString*)ABRecordCopyValue(ref, kABPersonFirstNameProperty));
                    lastnames = (__bridge ABMultiValueRef)((__bridge NSString*)ABRecordCopyValue(ref, kABPersonLastNameProperty));
                    
                    //        DebugLog(@"name from address book = %@ %@",names,lastnames);  // works fine.
                    NSString *fullname;
                    NSString *fnm = [NSString stringWithFormat:@"%@",names];
                    NSString *lnm = [NSString stringWithFormat:@"%@",lastnames];
                    //        //DebugLog(@"fnm : %@",fnm);
                    if ([fnm length] ==0 || [fnm isKindOfClass:[NSNull class]] || fnm == (id)[NSNull null] || [fnm isEqualToString:@"(null)"])
                        fullname = [NSString stringWithFormat:@"%@",lastnames];
                    
                    else if ([lnm length] ==0 || [lnm isKindOfClass:[NSNull class]] || lnm == (id)[NSNull null] || [lnm isEqualToString:@"(null)"])
                        fullname = [NSString stringWithFormat:@"%@",names];
                    else
                        fullname = [NSString stringWithFormat:@"%@ %@",names,lastnames];
                    
                    [contactpickarr addObject:fullname];
                    
                    ABMultiValueRef emailProperty = ABRecordCopyValue(ref, kABPersonEmailProperty);
                    
                    NSArray *emailArray = (__bridge NSArray *)ABMultiValueCopyArrayOfAllValues(emailProperty);
                    NSString *newstringemail= [emailArray componentsJoinedByString:@","];
                    
                    ns = [[NSMutableDictionary alloc]init];
                    
                    [ns setObject:fullname forKey:@"fullname"];
                    [ns setValue:newstringemail forKey:@"email"];
                    
                    if ([newstringemail isKindOfClass:[NSNull class]] || newstringemail == (id)[NSNull null] || [newstringemail length] < 4)
                        [ns setValue:@"" forKey:@"email"];
                    
                    ABRecordID recordID = ABRecordGetRecordID(ref);
                    
                    int useridphn = (int)recordID;
                    
                    //                    NSString *idphn = [NSString stringWithFormat:@"p%d",useridphn];
                    NSString *idphn = [NSString stringWithFormat:@"%d",useridphn];
                    
                    //                        DebugLog(@"recordID 2: %d .. %d %@ %@",recordID,useridphn, idphn,fullname);
                    
                    [ns setValue:idphn forKey:@"id"];
                    
                    
                    ABMultiValueRef phoneNumberProperty = ABRecordCopyValue(ref, kABPersonPhoneProperty);
                    NSArray *_phoneNumbers = (__bridge NSArray*)ABMultiValueCopyArrayOfAllValues(phoneNumberProperty);
                    
                    NSMutableArray *mutableph= [_phoneNumbers mutableCopy];
                    int pp;
                    //                            DebugLog(@"mutable first: %@",mutableph);
                    for (pp =0; pp< [mutableph count]; pp++)
                    {
                        if ([[mutableph objectAtIndex:pp] isKindOfClass:[NSNull class]] || [mutableph objectAtIndex:pp] == (id)[NSNull null] || [[mutableph objectAtIndex:pp] length] < 3)
                        {
                            [mutableph removeObjectAtIndex:pp];
                            pp= pp-1;
                        }
                    }
                    //                            DebugLog(@"mutable second: %@",mutableph);
                    NSString *newstringph= [mutableph componentsJoinedByString:@","];
                    
                    //[ns setValue:newstringph forKey:@"phone"];
                    
                    NSMutableDictionary *phone = [[NSMutableDictionary alloc] init];
                    
                    ABMultiValueRef phones = ABRecordCopyValue(ref, kABPersonPhoneProperty);
                    for(CFIndex j = 0; j < ABMultiValueGetCount(phones); j++)
                    {
                        CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(phones, j);
                        CFStringRef locLabel = ABMultiValueCopyLabelAtIndex(phones, j);
                        NSString *phoneLabel =(__bridge NSString*) ABAddressBookCopyLocalizedLabel(locLabel);
                        //CFRelease(phones);
                        NSString *phoneNumber = (__bridge NSString *)phoneNumberRef;
                        //                        //CFRelease(phoneNumberRef);
                        //                        //CFRelease(locLabel);
                        DebugLog(@"phno:  - %@ (%@)", phoneNumber, phoneLabel);
                        [ns setValue:phoneNumber forKey:phoneLabel];
                    }
                    
                    DebugLog(@"NS:%@",ns);
                    
                    ABMultiValueRef stt = ABRecordCopyValue(ref, kABPersonAddressProperty);
                    if (ABMultiValueGetCount(stt) > 0) {
                        CFDictionaryRef dict = ABMultiValueCopyValueAtIndex(stt, 0);
                        //                    DebugLog(@"dict address contacts: %@",dict);
                        NSString *streetp = CFDictionaryGetValue(dict, kABPersonAddressStreetKey);
                        DebugLog(@"STREET:%@",streetp);
                        NSString *cityp = CFDictionaryGetValue(dict, kABPersonAddressCityKey);
                        NSString *zipcdep = CFDictionaryGetValue(dict, kABPersonAddressZIPKey);
                        DebugLog(@"Zipcode:%@",zipcdep);
                        //                    NSString *statep = CFDictionaryGetValue(dict, kABPersonAddressStateKey);
                        NSString *countryp = CFDictionaryGetValue(dict, kABPersonAddressCountryKey);
                        
                        if ([streetp isKindOfClass:[NSNull class]] || streetp == (id)[NSNull null])
                            [ns setValue:@"" forKey:@"street"];
                        else
                            [ns setValue:streetp forKey:@"street"];
                        
                        if ([cityp isKindOfClass:[NSNull class]] || cityp == (id)[NSNull null])
                            [ns setValue:@"" forKey:@"city"];
                        else
                            [ns setValue:cityp forKey:@"city"];
                        
                        if ([zipcdep isKindOfClass:[NSNull class]] || zipcdep == (id)[NSNull null])
                            [ns setValue:@"" forKey:@"zipcode"];
                        else
                            [ns setValue:zipcdep forKey:@"street"];
                        
                        if ([countryp isKindOfClass:[NSNull class]] || countryp == (id)[NSNull null])
                            [ns setValue:@"" forKey:@"country"];
                        else
                            [ns setValue:countryp forKey:@"country"];
                        
                        [ns setValue:@"" forKey:@"housenumber"];
                        
                        if (![ns objectForKey:@"zipcode"]) {
                            
                            [ns setValue:@"" forKey:@"zipcode"];
                        }
                        
                        
                    }
                    else
                    {
                        [ns setValue:@"" forKey:@"housenumber"];
                        [ns setValue:@"" forKey:@"city"];
                        [ns setValue:@"" forKey:@"street"];
                        [ns setValue:@"" forKey:@"zipcode"];
                        [ns setValue:@"" forKey:@"country"];
                    }
                    //            } [ns setObject:addressarr forKey:@"address"];
                    
                    for( NSString *aKey in [ns allKeys])
                    {
                        
                        if ([aKey isEqualToString:@"name"]) {
                            [ns setValue:[ns objectForKey:aKey] forKey:@"fullname"];
                            [ns removeObjectForKey:aKey];
                        }
                        //                        if ([aKey isEqualToString:@"home"]) {
                        //                            //                                                                        [changekeydict setValue:[changekeydict objectForKey:aKey] forKey:@"name"];
                        //                            [ns removeObjectForKey:aKey];
                        //                        }
                        // if ([aKey isEqualToString:@"id"]) {
                        //                                                                        [changekeydict setValue:[changekeydict objectForKey:aKey] forKey:@"name"];
                        //     [ns removeObjectForKey:aKey];
                        // }
                        if ([aKey isEqualToString:@"userids"]) {
                            //                                                                        [changekeydict setValue:[changekeydict objectForKey:aKey] forKey:@"name"];
                            [ns removeObjectForKey:aKey];
                        }
                        
                        //                        if ([aKey isEqualToString:@"mobile"]) {
                        //                            //                                                                        [changekeydict setValue:[changekeydict objectForKey:aKey] forKey:@"name"];
                        //                            [ns removeObjectForKey:aKey];
                        //                        }
                        //
                        //                        if ([aKey isEqualToString:@"work"]) {
                        //                            //                                                                        [changekeydict setValue:[changekeydict objectForKey:aKey] forKey:@"name"];
                        //                            [ns removeObjectForKey:aKey];
                        //                        }
                        
                        if ([aKey isEqualToString:@"iPhone"]) {
                            //                                                                        [changekeydict setValue:[changekeydict objectForKey:aKey] forKey:@"name"];
                            [ns removeObjectForKey:aKey];
                        }
                        
                    }
                    
                    
                    [con_array addObject:ns];
                }
            }
            
            
        }
        
        
        DebugLog(@"DEVICE CONTACTS ARRAYS:%ld %@",(unsigned long)[con_array count],con_array);
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        
    }];
    
    
    coverview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 64)];
    [self.view addSubview:coverview];
    
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
    
    // Add the gradient to the view
    [coverview.layer insertSublayer:gradient atIndex:0];
    
    
    
    back = [[UIImageView alloc] initWithFrame:CGRectMake(15, 32, 12, 20)];
    back.image = [UIImage imageNamed:@"back3"];
    [coverview addSubview:back];
    
    bck = [[UILabel alloc] initWithFrame:CGRectMake(back.frame.size.width+back.frame.origin.x+5, 32, 40, 20)];
    bck.text=@"Back";
    bck.textColor = [UIColor whiteColor];
    bck.backgroundColor=[UIColor clearColor];
    [coverview addSubview:bck];
    bck.userInteractionEnabled = YES;
    
    backbutton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 150, 64)];
    [backbutton setTitle:nil forState:UIControlStateNormal];
    backbutton.tag=1;
    backbutton.backgroundColor = [UIColor clearColor];
    
    [backbutton addTarget:self action:@selector(changecolor1) forControlEvents:UIControlEventTouchDown];
    [backbutton addTarget:self action:@selector(changecoloragain1) forControlEvents:UIControlEventTouchDragExit];
    [backbutton addTarget:self action:@selector(changecolor1) forControlEvents:UIControlEventTouchDragEnter];
    
    
    
    [backbutton addTarget:self action:@selector(backmethod:) forControlEvents:UIControlEventTouchUpInside];
    [coverview addSubview:backbutton];
    
    
    
    next = [[UIImageView alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-27, 32, 12, 20)];
    next.image= [UIImage imageWithCGImage:back.image.CGImage
                                    scale:back.image.scale
                              orientation:UIImageOrientationUpMirrored];
    [coverview addSubview:next];
    
    nxt = [[UILabel alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-77, 32, 46, 20)];
    nxt.text = @"Finish";
    nxt.textColor = [UIColor whiteColor];
    nxt.backgroundColor = [UIColor clearColor];
    [coverview addSubview:nxt];
    nxt.userInteractionEnabled = YES;
    
    nextbutton = [[UIButton alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-73, 27, 65, 30)];
    [nextbutton setTitle:nil forState:UIControlStateNormal];
    nextbutton.backgroundColor = [UIColor clearColor];
    
    [nextbutton addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDown];
    [nextbutton addTarget:self action:@selector(changecoloragain) forControlEvents:UIControlEventTouchDragExit];
    [nextbutton addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDragEnter];
    
    [nextbutton addTarget:self action:@selector(nextmethod) forControlEvents:UIControlEventTouchUpInside];
    [coverview addSubview:nextbutton];
    
    mainview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, coverview.frame.origin.y+coverview.frame.size.height, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height-coverview.frame.size.height)];
    mainview.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:mainview];
    
    
    
    flag = [[UIImageView alloc] initWithFrame:CGRectMake(20, 17.5, 30, 20)];
    //    flag.backgroundColor=[UIColor redColor];
    flag.image=[UIImage imageNamed:flagCode];
    [mainview addSubview:flag];
    
    country = [[UILabel alloc] initWithFrame:CGRectMake(flag.frame.origin.x+flag.frame.size.width+8, 0, [[UIScreen mainScreen] bounds].size.width-20, 55)];
    country.backgroundColor = [UIColor clearColor];
    if([[[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkISOCode"] isKindOfClass:[NSNull class]] || [[[NSUserDefaults standardUserDefaults]objectForKey:@"placemarkISOCode"] length] == 0){
        
        country.text = @"Select country";
        
    }else{
        
        country.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"placemarkCode"];
    }
    
    //    country.text=@"hgsdjg";
    [mainview addSubview:country];
    
    down = [[UIImageView alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-40, 17.5, 25, 20)];
    down.image=[UIImage imageNamed:@"dnwarr.png"];
    [mainview addSubview:down];
    down.userInteractionEnabled=YES;
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 55)];
    [btn addTarget:self action:@selector(select) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [mainview addSubview:btn];
    
    
    divider1 = [[UIView alloc] initWithFrame:CGRectMake(0, country.frame.origin.y+country.frame.size.height, [[UIScreen mainScreen] bounds].size.width, 0.5)];
    divider1.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
    [mainview addSubview:divider1];
    
    //==============Keyboard type numpad Done button============//
    
    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneButton)],
                           nil];
    [numberToolbar sizeToFit];
    
    
    zipcode = [[UITextField alloc] initWithFrame:CGRectMake(20, divider1.frame.origin.y+divider1.frame.size.height, [[UIScreen mainScreen] bounds].size.width-40, 55)];
    zipcode.backgroundColor = [UIColor clearColor];
    UIColor *color = [UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0];
    zipcode.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Zip code (Optional)" attributes:@{NSForegroundColorAttributeName: color}];
    zipcode.delegate=self;
    zipcode.textColor = [UIColor blackColor];
    zipcode.userInteractionEnabled=YES;
    zipcode.keyboardType=UIKeyboardTypeNumbersAndPunctuation;
    zipcode.autocorrectionType=UITextAutocorrectionTypeNo;
    zipcode.keyboardAppearance=UIKeyboardAppearanceDark;
    zipcode.font=[UIFont fontWithName:@"ProximaNova-Regular" size:18];
    zipcode.tintColor=[UIColor grayColor];
    [mainview addSubview:zipcode];
    
    divider2= [[UIView alloc] initWithFrame:CGRectMake(0, zipcode.frame.origin.y+zipcode.frame.size.height-0.5, [[UIScreen mainScreen] bounds].size.width, 0.5)];
    divider2.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
    [mainview addSubview:divider2];
    
    city = [[UITextField alloc] initWithFrame:CGRectMake(20, divider2.frame.origin.y+divider2.frame.size.height, [[UIScreen mainScreen] bounds].size.width-40, 55)];
    city.backgroundColor = [UIColor clearColor];
    city.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"City" attributes:@{NSForegroundColorAttributeName: color}];
    city.delegate=self;
    city.textColor = [UIColor blackColor];
    city.userInteractionEnabled=YES;
    city.autocorrectionType=UITextAutocorrectionTypeNo;
    city.keyboardAppearance=UIKeyboardAppearanceDark;
    city.font=[UIFont fontWithName:@"ProximaNova-Regular" size:18];
    city.tintColor=[UIColor grayColor];
    [mainview addSubview:city];
    
    divider3= [[UIView alloc] initWithFrame:CGRectMake(0, city.frame.origin.y+city.frame.size.height-0.5, [[UIScreen mainScreen] bounds].size.width, 0.5)];
    divider3.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
    [mainview addSubview:divider3];
    
    street = [[UITextField alloc] initWithFrame:CGRectMake(20, divider3.frame.origin.y+divider3.frame.size.height, [[UIScreen mainScreen] bounds].size.width-20, 55)];
    street.backgroundColor = [UIColor clearColor];
    street.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Address (Optional)" attributes:@{NSForegroundColorAttributeName: color}];
    street.delegate=self;
    street.tag=4;
    street.textColor = [UIColor blackColor];
    street.userInteractionEnabled=YES;
    street.autocorrectionType=UITextAutocorrectionTypeNo;
    street.keyboardAppearance=UIKeyboardAppearanceDark;
    street.font=[UIFont fontWithName:@"ProximaNova-Regular" size:18];
    street.tintColor=[UIColor grayColor];
    [mainview addSubview:street];
    
    divider4= [[UIView alloc] initWithFrame:CGRectMake(0, street.frame.origin.y+street.frame.size.height-0.5, [[UIScreen mainScreen] bounds].size.width, 0.5)];
    divider4.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
    [mainview addSubview:divider4];
    
    
    details = [[UILabel alloc] initWithFrame:CGRectMake(0, divider4.frame.origin.y+divider4.frame.size.height+40, [[UIScreen mainScreen] bounds].size.width, 30)];
    details.textColor = [UIColor colorWithRed:135.0/255.0 green:135.0/255.0 blue:135.0/255.0 alpha:1.0];
    details.text=@"Please fill out your home address";
    details.textAlignment = NSTextAlignmentCenter;
    details.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
    [mainview addSubview:details];
    
}

-(void)select
{
    countrycodetable=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width,self.view.bounds.size.height-65)];
    [mainview addSubview:countrycodetable];
    countrycodetable.backgroundColor=[UIColor whiteColor];
    countrycodetable.dataSource=self;
    countrycodetable.delegate=self;
    countrycodetable.separatorStyle=UITableViewCellSeparatorStyleNone;
    //    countrycodetable.hidden=YES;
    countrycodetable.showsVerticalScrollIndicator=NO;
    countrycodetable.layer.borderColor=[[UIColor grayColor]CGColor];
    countrycodetable.layer.borderWidth=1;
    backbutton.tag=2;
    //    nxt.hidden=YES;
    //    next.hidden=YES;
}

-(void)backmethod:(UIButton *)button
{
    if (button.tag==1)
    {
        
        CATransition *transition = [CATransition animation];
        
        transition.duration = 0.4f;
        
        transition.type = kCATransitionFade;
        
        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    
    if (button.tag==2)
    {
        back.alpha = 1.0f;
        
        bck.alpha = 1.0f;
        countrycodetable.hidden=YES;
        backbutton.tag=1;
    }
}


-(void)nextmethod
{
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    //    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    
    if ([[country.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your country"
                                                        message:Nil
                                                       delegate:self
                                              cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        next.alpha = 1.0f;
        
        nxt.alpha = 1.0f;
        
        
    }
    else if ([country.text isEqualToString:@"Select country"])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your country"
                                                        message:Nil
                                                       delegate:self
                                              cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        next.alpha = 1.0f;
        
        nxt.alpha = 1.0f;
        
        
    }
//    else if ([[zipcode.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
//    {
//        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your Zip Code"
//                                                        message:Nil
//                                                       delegate:self
//                                              cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
//        [alert show];
//        next.alpha = 1.0f;
//        
//        nxt.alpha = 1.0f;
//        
//    }
//    else if ([zipcode.text isEqualToString:@"Zip Code"])
//    {
//        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your Zip Code"
//                                                        message:Nil
//                                                       delegate:self
//                                              cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
//        [alert show];
//        next.alpha = 1.0f;
//        
//        nxt.alpha = 1.0f;
//        
//        
//    }
    else if ([[city.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your City"
                                                        message:Nil
                                                       delegate:self
                                              cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        next.alpha = 1.0f;
        
        nxt.alpha = 1.0f;
        
    }
    else if ([city.text isEqualToString:@"City"])
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your City"
                                                        message:Nil
                                                       delegate:self
                                              cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        next.alpha = 1.0f;
        
        nxt.alpha = 1.0f;
        
    }
//    else if ([[street.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
//    {
//        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your Address"
//                                                        message:Nil
//                                                       delegate:self
//                                              cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
//        [alert show];
//        next.alpha = 1.0f;
//        
//        nxt.alpha = 1.0f;
//        
//    }
//    else if ([street.text isEqualToString:@"Street"])
//    {
//        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your Address"
//                                                        message:Nil
//                                                       delegate:self
//                                              cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
//        [alert show];
//        next.alpha = 1.0f;
//        
//        nxt.alpha = 1.0f;
//        
//        
//    }
//    
    
    else
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        
        activ=[[UIActivityIndicatorView alloc]init];
        
        activ.center=self.view.center;
        
        [activ startAnimating];
        
        activ.hidesWhenStopped=YES;
        
        activ.hidden=NO;
        
        activ.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        
        [activ setColor:[UIColor blackColor]];
        
        [self.view addSubview:activ];
        
        NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=register&name=%@&surname=%@&email=%@&mobile_pre=%@&mobile_num=%@&password=%@&street=%@&zipcode=%@&city=%@&country=%@&code=%@&device_id=%@&device_type=ios",[prefs objectForKey:@"firstname"],[[prefs objectForKey:@"lastname"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[prefs objectForKey:@"email"],[prefs objectForKey:@"countrycode"],[prefs objectForKey:@"phone"],[prefs objectForKey:@"password"],[street.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],zipcode.text,[city.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[country.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],_Code,[prefs objectForKey:@"deviceToken"]]; //&thumb=true
        
        DebugLog(@"deny url: %@",urlString1);
        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:newString1]];
        
        NSString *params = [[NSString alloc] initWithFormat:@"image=%@",[[[NSUserDefaults standardUserDefaults] objectForKey:@"r_prof_image"] stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"]];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        
        NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        
        if(theConnection)
        {
            responseData = [NSMutableData data];
        }
        else
        {
        }
        
    }
}



-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [countriesList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [countrycodetable dequeueReusableCellWithIdentifier:CellIdentifier];
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell.backgroundColor=[UIColor clearColor];
    
    UIImageView *cellimg=[[UIImageView alloc]initWithFrame:CGRectMake(10, 14, 35, 22)];
    cellimg.image=[UIImage imageNamed:[[countriesList objectAtIndex:indexPath.row] objectForKey:@"flag_32"]];
    cellimg.clipsToBounds=YES;
    cellimg.contentMode=UIViewContentModeScaleToFill;
    [cell addSubview:cellimg];
    
    UILabel *name=[[UILabel alloc]initWithFrame:CGRectMake(60, 0, 230, 50)];
    name.backgroundColor=[UIColor clearColor];
    name.text=[NSString stringWithFormat:@"%@",[[countriesList objectAtIndex:indexPath.row] objectForKey:@"name"]];
    name.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
    name.textColor=[UIColor blackColor];
    [cell addSubview:name];
    
    UILabel *separatorlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 49.5, [UIScreen mainScreen].bounds.size.width, 0.5)];
    separatorlabel.backgroundColor=[UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f];
    [cell addSubview:separatorlabel];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    countrylabel.text = @"";
    //    countryimg.image = nil;
    country.text = [NSString stringWithFormat:@"%@",[[countriesList objectAtIndex:indexPath.row]objectForKey:@"name"]];
    flag.image = [UIImage imageNamed:[[countriesList objectAtIndex:indexPath.row] objectForKey:@"flag_128"]];
    countrycodetable.hidden=YES;
    nxt.hidden=NO;
    next.hidden=NO;
    
    backbutton.tag = 1;
    
    //    CATransition *animation = [CATransition animation];
    //
    //    [animation setType:kCATransitionFade];
    //
    //    animation.duration = 0.2f;
    //
    //    [countrycodetable.layer addAnimation:animation forKey:nil];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 50;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    DebugLog(@"Dismiss keyboard");
    [textField resignFirstResponder];
    return YES;
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField.tag==4)
    {
        if (self.view.bounds.size.height<=480) {
            mainview.contentOffset=CGPointMake(0, 100);
        }
    }
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (textField.tag==4)
    {
        if (self.view.bounds.size.height<=480) {
            mainview.contentOffset=CGPointMake(0, 0);
        }
    }
    
    return YES;
}



-(void)doneButton{
    
    // NSString *numberFromTheKeyboard = ziptext.text;
    [zipcode resignFirstResponder];
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data
{
    [responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    DebugLog(@"connection failed");
    UIAlertView *alrt= [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please check your internet connection" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alrt show];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"backupstatus"];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    DebugLog(@"connection finished loading");
    
    DebugLog(@"response data - %@", [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
    NSError *error;
    
    if (responseData != nil)
    {
        NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:responseData //1
                                                                  options:kNilOptions
                                                                    error:&error];
        DebugLog(@"deny json returns: %@",json_deny);
        
        NSString *errornumber= [NSString stringWithFormat:@"%@",[json_deny objectForKey:@"errorno"]];
        DebugLog(@"err  %@",errornumber);
        if (![errornumber isEqualToString:@"0"])
        {
            
            if ([[json_deny objectForKey:@"success"] intValue]!=0)
                
            {
                
                
                
                [activ removeFromSuperview];
                
                ConLoginViewController *con = [[ConLoginViewController alloc]init];
                
                
                
                //                CATransition* transition = [CATransition animation];
                //
                //                transition.duration = 0.4;
                //
                //                transition.type = kCATransitionPush;
                //
                //                transition.subtype = kCATransitionFade;
                //
                //                [[self navigationController].view.layer addAnimation:transition forKey:nil];
                
                
                
                //  [self.navigationController pushViewController:con animated:YES];
                
            }
            
            else
                
            {
                
                err_str = [json_deny objectForKey:@"error"];
                
                [self performSelectorOnMainThread:@selector(errorsignup) withObject:nil waitUntilDone:YES];
                
            }
        }
        else
        {
            
            
            if ([[json_deny objectForKey:@"success"]intValue] == 1)
            {
                
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    
                    [activ startAnimating];
                }];
                
                [[NSOperationQueue new] addOperationWithBlock:^{
                    
                    NSDictionary *dict_details = [json_deny objectForKey:@"details"];
                    
                    NSString *access_token = [NSString stringWithFormat:@"%@",[dict_details objectForKey:@"access_token"]];
                    NSString *userid = [NSString stringWithFormat:@"%@",[dict_details objectForKey:@"id"]];
                    DebugLog(@"access token ret  %@  %@",access_token,userid);
                    
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                    [prefs setObject:userid forKey:@"userid"];
                    [prefs setObject:access_token forKey:@"access_token"];
                    [prefs setObject:@"yes" forKey:@"isloggedin"];
                    [prefs setObject:@"yes" forKey:@"addbackup"];
                    [prefs setObject:@"yes" forKey:@"copycontacts"];
                    [prefs setObject:@"yes" forKey:@"firstRequest"];
                    [prefs setObject:@"yes" forKey:@"firstContacts"];
                    [prefs setObject:@"yes" forKey:@"firstMap"];
                    [prefs setObject:@"yes" forKey:@"firstProfile"];
                    
                    [prefs synchronize];
                    
                    //                UIAlertView *alrt=[[UIAlertView alloc] initWithTitle:nil message:@"Account successfully created" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    //                [alrt show];
                    
                    
                    //                NSOperationQueue *newBackup_operation = [NSOperationQueue new];
                    //
                    //                [newBackup_operation addOperationWithBlock:^{
                    
                    //  }];
                    
                    
                    
                    
                    
                    NSString *backupUrl = [NSString stringWithFormat:@"https://budnav.com/ext/?action=newbackup&backupid=%@&access_token=%@&device_id=%@",userid,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
                    
                    NSError *errorjs= nil;
                    
                    NSData *newbakup =[NSData dataWithContentsOfURL:[NSURL URLWithString:backupUrl]];
                    
                    if (newbakup != nil) {
                        
                        DebugLog(@"NEW BACKUP URL:%@",backupUrl);
                        
                        NSDictionary *backup_json = [NSJSONSerialization JSONObjectWithData:newbakup //1
                                                                                    options:kNilOptions
                                                     
                                                                                      error:&errorjs];
                        DebugLog(@"this now has json string url: %@",backup_json);
                        
                        if ([[backup_json objectForKey:@"success"]intValue] == 1)
                        {
                            DebugLog(@"Backup:%@",backup_json);
                            
                            
                            NSError *err1 = nil;
                            
                            //     DebugLog(@"unique array count: %lu",(unsigned long)[uniquearray count]);
                            
                            number_of_iterations = 0;
                            part_array = [[NSMutableArray alloc]init];
                            if ([con_array count] <= 300)
                            {
                                part_array = [con_array mutableCopy];
                                number_of_iterations=1;
                            }
                            else
                            {
                                if ([con_array count] % 300 == 0)
                                {
                                    number_of_iterations = (int)[con_array count]/300;
                                }
                                else
                                {
                                    number_of_iterations = (int)[con_array count]/300 +1 ;
                                }
                            }
                            //       DebugLog(@"number of iterations are: %d", number_of_iterations);
                            
                            for (int numb =0; numb < number_of_iterations; numb++)
                            {
                                part_array = [[NSMutableArray alloc]init];
                                
                                if (numb == number_of_iterations -1)
                                {
                                    NSArray *tempArray = [con_array subarrayWithRange:NSMakeRange(numb*300, [con_array count]-(300*numb))];
                                    [part_array addObjectsFromArray:tempArray];
                                }
                                else
                                {
                                    NSArray *tempArray = [con_array subarrayWithRange:NSMakeRange(numb*300, 299)];
                                    [part_array addObjectsFromArray:tempArray];
                                }
                                
                                
                                DebugLog(@"number of elements in part array are: %lu %@",(unsigned long)[part_array count], part_array);//========================================================= ADD BACKUP=================================================//
                                
                                NSError *error = nil;
                                NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:(NSArray *)part_array options:NSJSONWritingPrettyPrinted error:&err1];
                                NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
                                NSString *encodedText = [self encodeToPercentEscapeString:jsonString];
                                
                                NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=addbackup&access_token=%@&device_id=%@",[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];   //&contacts=%@  ,[jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                                
                                DebugLog(@"addbackuppart url: %@",urlString1);
                                responseData = [NSMutableData data];
                                NSURLResponse *response;
                                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString1]];
                                
                                NSString *params = [[NSString alloc] initWithFormat:@"contacts=%@",encodedText]; //[jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                                DebugLog(@"ADD BACKUP PARAM:%@",params);
                                [request setHTTPMethod:@"POST"];
                                [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
                                [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                                //            [[NSURLConnection alloc] initWithRequest:request delegate:self];
                                NSData* result = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                                // DebugLog(@"ADD BACKUP:%@",response);
                                // NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
                                if (result!=nil) {
                                    
                                    NSDictionary *json_backup = [NSJSONSerialization JSONObjectWithData:result options:kNilOptions error:nil];
                                    DebugLog(@"JSON BACKUP:%@",json_backup);
                                }
                                else
                                {
                                    DebugLog(@"CONNECTION FAILED");
                                }
                                
                            }
                            
                            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                
                                
                                
//                                UIAlertView *alrt=[[UIAlertView alloc] initWithTitle:nil message:@"Account successfully created" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//                                [alrt show];
                                
                                AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                                del.signup = @"Signup";
                                
                                ConLoginViewController *con = [[ConLoginViewController alloc]init];
                                
                                [self.navigationController pushViewController:con animated:NO];
                                
                            }];
                            
                            
                            
                            //                CATransition* transition = [CATransition animation];
                            //
                            //                transition.duration = 0.4;
                            //                transition.type = kCATransitionPush;
                            //                transition.subtype = kCATransitionFade;
                            //
                            //                [[self navigationController].view.layer addAnimation:transition forKey:nil];
                            
                            
                        }
                        else
                        {
                            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                
                                [activ removeFromSuperview];
                                UIAlertView *alrt= [[UIAlertView alloc] initWithTitle:nil message:@"Backup not created" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                [alrt show];
                            }];
                        }
                    }
                    else
                    {
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            UIAlertView *alrt= [[UIAlertView alloc] initWithTitle:nil message:@"Error in server connection!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                            [alrt show];
                            
                        }];
                    }
                    
                }];
                
            }
            
            
        }
    }
    else
    {
        UIAlertView *alrt= [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please check your internet connection" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alrt show];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    responseData = [NSMutableData data];
}

-(void)errorsignup
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                                    message:err_str
                                                   delegate:self
                                          cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
    [alert show];
    [activ stopAnimating];
    [activ removeFromSuperview];
}

-(NSString*) encodeToPercentEscapeString:(NSString *)string {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                              (CFStringRef) string,
                                                              NULL,
                                                              (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)changecoloragain1
{
    DebugLog(@"CHANGE COLOR");
    bck.alpha = 1.0f;
    back.alpha = 1.0f;
}

-(void)changecolor1
{
    DebugLog(@"CHANGE COLOR1");
    bck.alpha = 0.5f;
    back.alpha = 0.5f;
}

-(void)changecoloragain
{
    DebugLog(@"CHANGE COLOR");
    nxt.alpha = 1.0f;
    next.alpha = 1.0f;
}

-(void)changecolor
{
    DebugLog(@"CHANGE COLOR1");
    nxt.alpha = 0.5f;
    next.alpha = 0.5f;
    
}



@end