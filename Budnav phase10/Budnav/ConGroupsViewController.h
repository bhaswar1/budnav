//
//  ConGroupsViewController.h
//  Contacter
//
//  Created by Prosenjit_K on 28/10/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConParentTopBarViewController.h"
#import "ConCreateGroupViewController.h"
#import "GAITrackedViewController.h"

@interface ConGroupsViewController : GAITrackedViewController<UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource>
{
    UILabel *namelbl;
}
@end
