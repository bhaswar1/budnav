//
//  ConGroupPicViewController.h
//  Contacter
//
//  Created by ios on 19/01/15.
//  Copyright (c) 2015 Esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface ConGroupPicViewController : GAITrackedViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIActionSheetDelegate,NSURLConnectionDelegate>
@property(nonatomic,strong) UIImage *grouppic;
@property(nonatomic,strong) NSString *groupid,*adminname;

@end
