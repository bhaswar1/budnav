//
//  ConNewProfileViewController.h
//  Budnav
//
//  Created by intel on 03/06/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConSettingsView.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "Mymodel.h"
#import "GAITrackedViewController.h"

@interface ConNewProfileViewController : GAITrackedViewController<UIGestureRecognizerDelegate,MKMapViewDelegate, NSURLConnectionDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,UIScrollViewDelegate,UIActionSheetDelegate,UIAlertViewDelegate,MKOverlay,MKAnnotation>
{
    //    UIView *topbar;
    MKMapView *map_View;
    MFMailComposeViewController *mailComposer;
    UIScrollView *mainscroll;
    UIActionSheet *anActionSheet;
}
@property (nonatomic, strong) NSString *other,*uid, *mapReturn, *business_friend;
//@property (nonatomic, strong) UIView *mainview;
@property(nonatomic,retain) NSString *getmapvalue;
@property(nonatomic,retain) NSMutableDictionary *personal_dict, *business_dict, *social_dict;
@property(nonatomic,retain) NSMutableArray *personal_array, *business_array, *social_array, *phonearr;
@property(nonatomic,retain) NSDictionary *dict_profile,*profdict;
@property(nonatomic, retain) NSDictionary *Condetails_dict;
@property(nonatomic, assign) BOOL request;
+(void)chngpostion;
@end
