#import "REVMapViewController.h"
#import "REVClusterMap.h"
#import "REVClusterAnnotationView.h"
#import "AppDelegate.h"
#import "ConAccountSettingsViewController.h"
#import "ConProfileOwnViewController.h"
#import "ConNewProfileViewController.h"

#define BASE_RADIUS .5 // = 1 mile
#define MINIMUM_LATITUDE_DELTA 0.20
#define BLOCKS 4

#define MINIMUM_ZOOM_LEVEL 100000
@interface REVMapViewController ()<MKMapViewDelegate>
{
    int move;
    UIView *mainview;
    UIAlertView *alert;
}

@end
@implementation REVMapViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    self.navigationController.navigationBarHidden=YES;
}

//- (void)dealloc
//{
//    [_mapView release], _mapView = nil;
//    [super dealloc];
//}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    move=0;
    
    newarr1 = [[NSMutableArray alloc]init];
    
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    self.navigationController.navigationBarHidden=YES;
    
    mainview = [[UIView alloc]initWithFrame:CGRectMake(0, 65, [UIScreen mainScreen].bounds.size.width, self.view.frame.size.height-60)];
    [self.view addSubview:mainview];
    mainview.backgroundColor=[UIColor whiteColor];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    //    NSString *urlString_concount =[NSString stringWithFormat:@"https://budnav.com/ext/?action=newcontactcount&id=%@&access_token=%@&device_id=%@",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
    //
    //
    //    NSString *newString_concount = [urlString_concount stringByReplacingOccurrencesOfString:@" " withString:@""];
    //
    //    NSData *signeddataURL_concount =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString_concount]];
    //
    //    NSError *errorcon=nil;
    //
    //    NSDictionary *jsona_concount = [NSJSONSerialization JSONObjectWithData:signeddataURL_concount //1
    //
    //                                                                   options:kNilOptions
    //
    //                                                                     error:&errorcon];
    //    //    DebugLog(@"json returns: %@",json1);
    //
    //    NSString *errornumber_concount= [NSString stringWithFormat:@"%@",[jsona_concount objectForKey:@"errorno"]];
    //    DebugLog(@"err  %@",errornumber_concount);
    //    if (![errornumber_concount isEqualToString:@"0"])
    //    {
    //        NSString *err_str_concount = [jsona_concount objectForKey:@"error"];
    //        alert = [[UIAlertView alloc] initWithTitle:@"Error in Retrieving Data!"
    //                                           message:err_str_concount
    //                                          delegate:self
    //                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
    //        [alert show];
    //    }
    //    else
    //    {
    //        NSDictionary *countcon= [jsona_concount objectForKey:@"details"];
    //        NSString *con_count = [NSString stringWithFormat:@"%d",[[countcon objectForKey:@"newcontactcount"] intValue]];
    //        if ([con_count intValue] == 0)
    //            [[super.tabBarController.viewControllers objectAtIndex:1] tabBarItem].badgeValue = Nil;
    //        else
    //            [[super.tabBarController.viewControllers objectAtIndex:1] tabBarItem].badgeValue =con_count;
    //    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    NSString *urlString_reqcount =[NSString stringWithFormat:@"https://budnav.com/ext/?action=requestcount&id=%@&access_token=%@&device_id=%@&business=true",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
    
    
    NSString *newString_reqcount = [urlString_reqcount stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL_reqcount =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString_reqcount]];
    
    NSError *error=nil;
    
    NSDictionary *jsona_reqcount = [NSJSONSerialization JSONObjectWithData:signeddataURL_reqcount //1
                                    
                                                                   options:kNilOptions
                                    
                                                                     error:&error];
    //    DebugLog(@"json returns: %@",json1);
    
    NSString *errornumber_reqcount= [NSString stringWithFormat:@"%@",[jsona_reqcount objectForKey:@"errorno"]];
    
    if (![errornumber_reqcount isEqualToString:@"0"])
    {
        NSString *err_str_reqcount = [jsona_reqcount objectForKey:@"error"];
        alert = [[UIAlertView alloc] initWithTitle:@"Error in Retrieving Data!"
                                           message:err_str_reqcount
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
    }
    else
    {
        NSDictionary *countreq= [jsona_reqcount objectForKey:@"details"];
        NSString *request_count = [NSString stringWithFormat:@"%d",[[countreq objectForKey:@"requestcount"] intValue]];
        if ([request_count intValue] == 0)
            [[super.tabBarController.viewControllers objectAtIndex:2] tabBarItem].badgeValue = Nil;
        else
            [[super.tabBarController.viewControllers objectAtIndex:2] tabBarItem].badgeValue =request_count;
    }
    
    
    //////////////////////////    Map Part Begins Here Bhaswar
    
    //    [super loadView];
    
    //    CGRect viewBounds = [[UIScreen mainScreen] applicationFrame];
    
    _mapView = [[REVClusterMapView alloc] initWithFrame:mainview.frame];
    _mapView.delegate = self;
    
    [mainview addSubview:_mapView];
    
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = 51.22;
    coordinate.longitude = 4.39625;
    //    _mapView.region = MKCoordinateRegionMakeWithDistance(coordinate, 5000, 5000);
    
    _mapView.showsUserLocation=YES;
    
    
    //    NSMutableArray *pins = [NSMutableArray array];
    //
    //    for(int i=0;i<50;i++) {
    //        CGFloat latDelta = rand()*0.125/RAND_MAX - 0.02;
    //        CGFloat lonDelta = rand()*0.130/RAND_MAX - 0.08;
    //
    //        CGFloat lat = 51.21992;
    //        CGFloat lng = 4.39625;
    //
    //
    //        CLLocationCoordinate2D newCoord = {lat+latDelta, lng+lonDelta};
    //        REVClusterPin *pin = [[REVClusterPin alloc] init];
    //        pin.title = [NSString stringWithFormat:@"Pin %i",i+1];;
    //        pin.subtitle = [NSString stringWithFormat:@"Pin %i subtitle",i+1];
    //        pin.coordinate = newCoord;
    //        [pins addObject:pin];
    //        [pin release];
    //    }
    //
    //    [_mapView addAnnotations:pins];
    
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=map&id=%@&access_token=%@&device_id=%@",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
    
    DebugLog(@"profile url: %@",urlString1);
    
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    
    
    NSDictionary *json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                           
                                                          options:kNilOptions
                           
                                                            error:&error];
    DebugLog(@"json returns: %@",json1);
    
    NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
    DebugLog(@"err  %@",errornumber);
    if (![errornumber isEqualToString:@"0"])
    {
        DebugLog(@"if if");
    }
    
    if ([[json1 objectForKey:@"details"] isKindOfClass:[NSNull class]] || [json1 objectForKey:@"details"] == (id)[NSNull null] ||[[json1 objectForKey:@"details"] count] ==0)
    {
        DebugLog(@"if called req");
        
    }
    else
    {
        newarr1= [[NSMutableArray alloc]init];
        NSDictionary *newdict= [json1 objectForKey:@"details"];
        for (NSDictionary *reqdict in [newdict objectForKey:@"markers"])
        {
            [newarr1 addObject:reqdict];
        }
        DebugLog(@"map list array has: %@",newarr1);
        
    }
    
    DebugLog(@"newarr ache: %@",newarr1);
    
    NSMutableArray *pins = [NSMutableArray array];
    
    int i=0;
    while (i<[newarr1 count])
    {
        double latitude=[[[newarr1 objectAtIndex:i]objectForKey:@"lat"] doubleValue];
        double longitude=[[[newarr1 objectAtIndex:i]objectForKey:@"lng"] doubleValue];
        
        DebugLog(@"lati=== %f",latitude);
        DebugLog(@"long=== %f",longitude);
        
        //        MKPointAnnotation *myAnnotation = [[MKPointAnnotation alloc] init];
        //        myAnnotation.coordinate = CLLocationCoordinate2DMake(latitude, longitude);
        //        myAnnotation.title=[[newarr1 objectAtIndex:i]objectForKey:@"name"];
        //        myAnnotation.subtitle= [[newarr1 objectAtIndex:i]objectForKey:@"surname"];
        
        
        CLLocationCoordinate2D newCoord = {latitude, longitude};
        REVClusterPin *pin = [[REVClusterPin alloc] init];
        pin.title = [[newarr1 objectAtIndex:i]objectForKey:@"name"];
        pin.subtitle = [[newarr1 objectAtIndex:i]objectForKey:@"surname"];
        pin.coordinate = newCoord;
        [pins addObject:pin];
        //        [pin release];
        
        i++;
        
    }
    [_mapView addAnnotations:pins];
    
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [_mapView removeAnnotations:_mapView.annotations];
    _mapView.frame = self.view.bounds;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

#pragma mark -
#pragma mark Map view delegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if([annotation class] == MKUserLocation.class) {
        //userLocation = annotation;
        return nil;
    }
    
    REVClusterPin *pin = (REVClusterPin *)annotation;
    
    MKAnnotationView *annView;
    
    if( [pin nodeCount] > 0 ){
        pin.title = @"___";
        
        annView = (REVClusterAnnotationView*)
        [mapView dequeueReusableAnnotationViewWithIdentifier:@"cluster"];
        
        if( !annView )
            annView = (REVClusterAnnotationView*)[[REVClusterAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"cluster"];
        
        annView.image = [UIImage imageNamed:@"locator.png"];
        
        [(REVClusterAnnotationView*)annView setClusterText:
         [NSString stringWithFormat:@"%lu",(unsigned long)[pin nodeCount]]];
        
        annView.canShowCallout = NO;
    } else {
        annView = [mapView dequeueReusableAnnotationViewWithIdentifier:@"pin"];
        
        //        if( !annView )
        //            annView = [[MKAnnotationView alloc] initWithAnnotation:annotation
        //                                                    reuseIdentifier:@"pin"];
        
        //        annView.image = [UIImage imageNamed:@"locator2.png"];
        
        
        annView = [[MKPinAnnotationView alloc] init];
        //            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomPinAnnotationView"];
        //        annView.animatesDrop = YES;
        annView.canShowCallout = YES;
        annView.image = [UIImage imageNamed:@"locator1.png"];
        annView.calloutOffset = CGPointMake(0, 0);
        
        
        UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        annView.rightCalloutAccessoryView = rightButton;
        
        UIImageView *iconView = [[UIImageView alloc] init];
        iconView.frame = CGRectMake(0.0f, 0.0f,20, 20);
        annView.leftCalloutAccessoryView = iconView;
        iconView.backgroundColor=[UIColor whiteColor];
        
        if (annotation == mapView.userLocation)
        {
            annView.image= [UIImage imageNamed:@"locatorown.png"];
            annView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomPinAnnotationView"];
        }
        //            if(typec==1)
        //            {
        DebugLog(@"(int)annotation.subtitle===%@",annotation.subtitle);
        for(int newi=0;newi<[newarr1 count];newi++)
        {
            if([[[newarr1 objectAtIndex:newi]objectForKey:@"surname"]isEqualToString:pin.subtitle])
            {
                if([[[newarr1 objectAtIndex:newi]objectForKey:@"business"]intValue] ==1)
                {
                    //                    [self loadImage:[NSString stringWithFormat:@"%@",[[newarr1 objectAtIndex:newi] objectForKey:@"image"]] andImageView:iconView];
                    iconView.image=[UIImage imageNamed:@"icon_busi.png"];
                    annView.image = [UIImage imageNamed:@"locator2.png"];
                    
                }
                else
                {
                    iconView.image=[UIImage imageNamed:@"icon_friend.png"];
                    annView.image = [UIImage imageNamed:@"locator1.png"];
                    DebugLog(@"friend");
                }
                userid =[[[newarr1 objectAtIndex:newi] objectForKey:@"id"]intValue];
                annView.tag = [[[newarr1 objectAtIndex:newi] objectForKey:@"id"]intValue];
                
                break;
            }
            
        }
        
        
        
        annView.canShowCallout = YES;
        
        annView.calloutOffset = CGPointMake(-6.0, 0.0);
    }
    return annView;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    DebugLog(@"REVMapViewController mapView didSelectAnnotationView:");
    
    if (![view isKindOfClass:[REVClusterAnnotationView class]])
        return;
    
    CLLocationCoordinate2D centerCoordinate = [(REVClusterPin *)view.annotation coordinate];
    
    MKCoordinateSpan newSpan =
    MKCoordinateSpanMake(mapView.region.span.latitudeDelta/2.0,
                         mapView.region.span.longitudeDelta/2.0);
    
    //mapView.region = MKCoordinateRegionMake(centerCoordinate, newSpan);
    
    [mapView setRegion:MKCoordinateRegionMake(centerCoordinate, newSpan)
              animated:YES];
}


+(void)chngpostion
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Data" object:Nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getData:) name:@"Data" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(navtopage:) name:@"pagenav" object:nil];
}

-(void)navtopage: (NSNotification *)notification
{
    DebugLog(@"navtopage");
    NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
    if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"2"])
    {
        ConAccountSettingsViewController *con = [[ConAccountSettingsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
}

-(void)getData:(NSNotification *)notification {
    
    if(move == 0) {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake(280, mainview.frame.origin.y, mainview.frame.size.width, mainview.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             move = 1;
                         }];
    } else {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake(0, (mainview.frame.origin.y),mainview.frame.size.width, mainview.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             move = 0;
                         }];
    }
}


- (void)getDirections
{
    CLLocationCoordinate2D start = {37.774929, -122.419416 };
    CLLocationCoordinate2D end = {40.714353, -74.005973};
    
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
    
    MKMapItem *source_mapitem =[[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:start addressDictionary:nil]];
    [source_mapitem setName:@"Source"];
    
    MKMapItem *destination_mapitem = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:end addressDictionary:nil]];
    
    [destination_mapitem setName:@"Destination"];
    request.source = source_mapitem;
    request.destination = destination_mapitem;
    
    request.requestsAlternateRoutes = YES;
    MKDirections *directions =
    [[MKDirections alloc] initWithRequest:request];
    
    [directions calculateDirectionsWithCompletionHandler:
     ^(MKDirectionsResponse *response, NSError *error) {
         if (error) {
             DebugLog(@" error is %@",error.localizedDescription);
         } else {
             DebugLog(@" here we came");
             [self showRoute:response];
         }
     }];
}

-(void)showRoute:(MKDirectionsResponse *)response
{
    for (MKRoute *route in response.routes)
    {
        [_mapView addOverlay:route.polyline level:MKOverlayLevelAboveLabels];
        for (MKRouteStep *step in route.steps)
        {
            DebugLog(@" here %@", step.instructions);
        }
    }
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id < MKOverlay >)overlay
{
    MKPolylineRenderer *renderer =
    [[MKPolylineRenderer alloc] initWithOverlay:overlay];
    renderer.strokeColor = [UIColor blueColor];
    renderer.lineWidth = 2.0;
    return renderer;
}

//-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
//{
//    DebugLog(@"annotation selected");
//    //    if ([view.annotation isKindOfClass:[MKPointAnnotation class]]) {
//    //        MKAnnotationView *annot = view;
//    //        NSInteger index = [newarr1 indexOfObject:annot];
//    //        DebugLog(@"%d",index);
//    //    }
//}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    DebugLog(@"Tapped on: %@", view.annotation.subtitle);
    DebugLog(@"Tapped user id: %ld", (long)view.tag);
    //        NSString *IdTag =view.annotation.subtitle;
    ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
    con.other=@"yes";
    con.uid=[NSString stringWithFormat:@"%ld",(long)view.tag];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}

-(void)viewDidDisappear:(BOOL)animated
{
    move=0;
    [mainview removeFromSuperview];
    [super viewDidDisappear:YES];
}
@end