//
//  ConStartViewController.h
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 05/05/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "GAITrackedViewController.h"

@interface ConStartViewController : GAITrackedViewController<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate, CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    NSString *dialCode;
    
    int check;
    NSString * const YOUR_STRING;
}
@end

