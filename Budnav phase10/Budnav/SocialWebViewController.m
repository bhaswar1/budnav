//
//  SocialWebViewController.m
//  Contacter
//
//  Created by Anirban Tah on 14/05/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

#import "SocialWebViewController.h"
#import "AppDelegate.h"
#import "ConAccountSettingsViewController.h"
#import "ConNewDetailsViewController.h"
@interface SocialWebViewController ()
{
    UIView *topbar;
    UIAlertView *alert;
    UIImageView *back;
    UIView *coverview;
    UILabel *bck;
    UIButton *backbutton;
}
@end

@implementation SocialWebViewController
@synthesize type_social;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIActivityIndicatorView *act = [[UIActivityIndicatorView alloc] init];
    act.center=self.view.center;
    [act startAnimating];
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [act setColor:[UIColor grayColor]];
    [self.view addSubview:act];
    
    //    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fb://"]]) {
    //        // Facebook app is installed
    //    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    self.navigationController.navigationBarHidden=YES;
    
//    topbar = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 60)];
//    [self.view addSubview:topbar];
//    topbar.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"topbar.png"]];
//    
//    UIImageView *logo_img = [[UIImageView alloc]initWithFrame:CGRectMake(99, 23, 122, 25)];
//    logo_img.image=[UIImage imageNamed:@"logo.png"];
//    [topbar addSubview:logo_img];
//    
//    UIButton *helpbt = [UIButton buttonWithType:UIButtonTypeCustom];
//    helpbt.frame = CGRectMake(12, 26, 11, 21);  //12,22,11,19
//    helpbt.backgroundColor=[UIColor clearColor];
//    [helpbt setBackgroundImage:[UIImage imageNamed:@"back_white1.png"] forState:UIControlStateNormal];
//    [helpbt addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:helpbt];
//    
//    UIView *backbuttonextended = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 85, 60)];
//    backbuttonextended.backgroundColor=[UIColor clearColor];
//    [self.view addSubview:backbuttonextended];
//    backbuttonextended.userInteractionEnabled=YES;
//    
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goBack)];
//    [backbuttonextended addGestureRecognizer:tap];
//    
    
   
    coverview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 64)];
    //coverview.backgroundColor = [UIColor blackColor];
    [self.view addSubview:coverview];
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
    
    // Add the gradient to the view
    [coverview.layer insertSublayer:gradient atIndex:0];
    
    UIImageView *logo_img = [[UIImageView alloc]init];
    logo_img.frame = CGRectMake(27+([UIScreen mainScreen].bounds.size.width-160)/2, 32, 101, 20);
    logo_img.backgroundColor = [UIColor clearColor];
    
    logo_img.image = [UIImage imageNamed:@"new_logo"];
    [topbar addSubview:logo_img];
    
    back = [[UIImageView alloc] initWithFrame:CGRectMake(15, 32, 12, 20)];
    back.image = [UIImage imageNamed:@"back3"];
    [coverview addSubview:back];
    
    bck = [[UILabel alloc] initWithFrame:CGRectMake(back.frame.size.width+back.frame.origin.x+5, 31, 40, 20)];
    bck.text=@"Back";
    bck.textColor = [UIColor whiteColor];
    //        backlbl.font=[UIFont fontWithName:@"ProximaNova-Regular" size:20];
    bck.backgroundColor=[UIColor clearColor];
    [coverview addSubview:bck];
    bck.userInteractionEnabled = YES;
    
    backbutton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, logo_img.frame.origin.x
                                                            -1, 64)];
    [backbutton setTitle:nil forState:UIControlStateNormal];
    backbutton.backgroundColor = [UIColor clearColor];
    [coverview addSubview:backbutton];
    
    [backbutton addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDown];
    [backbutton addTarget:self action:@selector(changecoloragain) forControlEvents:UIControlEventTouchDragExit];
    [backbutton addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDragEnter];
    [backbutton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    

    
    
    DebugLog(@"url: %@",type_social);
    if ([type_social length] == 0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"No Url Found"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
    }
    else
    {
    NSURL *theURL = [NSURL URLWithString: type_social];
    NSURLRequest *request = [NSURLRequest requestWithURL: theURL];
    
    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0,60,[UIScreen mainScreen].bounds.size.width,self.view.frame.size.height-60)];
    webView.scalesPageToFit = YES;
    [webView setDelegate: self];
    [webView loadRequest: request];
    [self.view addSubview:webView];
    }
    
    //    [del showTabValues:YES];
    
    
//        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
//        NSString *myString = [defaults objectForKey:@"webview"];
//        NSLog(@"default....%@",myString);
//        if([myString isEqualToString:@"WebViewOpen"])
//        {
//            NSLog(@"**********************");
//              ConNewDetailsViewController *nav=[[ConNewDetailsViewController alloc]init];
//            [self.navigationController pushViewController:nav animated:YES];
//           
//        }
}
-(void)changecoloragain
{
    DebugLog(@"CHANGE COLOR");
    bck.alpha = 1.0f;
    back.alpha = 1.0f;
}

-(void)changecolor
{
    DebugLog(@"CHANGE COLOR1");
    bck.alpha = 0.5f;
    back.alpha = 0.5f;
    
}


-(void)goBack//:(id)sender
{
    
//    CATransition *transition = [CATransition animation];
//    
//    transition.duration = 0.4f;
//    
//    transition.type = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    [self.navigationController popViewControllerAnimated:YES];
}
@end
