//
//  ConNameViewController.h
//  Budnav
//
//  Created by intel on 14/05/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "GAITrackedViewController.h"

@interface ConNameViewController : GAITrackedViewController<UITextFieldDelegate>
{
    UIView *topbar;
    UITextField *nametext, *surnametext;
    UIAlertView *alert;
    UIActivityIndicatorView *act;
}
@end
