//
//  ConParentTopBarViewController.m
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 08/05/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//
#import "ConAddFriendViewController.h"
#import "ConParentTopBarViewController.h"
#import "ConStartViewController.h"
#import "AppDelegate.h"
#import "ConProfileOwnViewController.h"
#import "ConNewContactsViewController.h"
#import "ConRequestViewController.h"
#import "ConMapViewController.h"
#import "ConQROptionsViewController.h"
#import "ConSuccessRegViewController.h"
@interface ConParentTopBarViewController ()
{
    UIView *topbar;
    int moveparent;
}
@end

@implementation ConParentTopBarViewController
@synthesize topbarparent;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //    UIImage *logoImage = [UIImage imageNamed:@"topbar.png"];
    //    UIImageView *titleLogo = [[UIImageView alloc] initWithImage:logoImage];
    //    titleLogo.frame = CGRectMake(0, 0, 320, 60);
    //    self.navigationItem.titleView = titleLogo;
    
    topbarparent = [[UIView alloc]init];
    topbarparent.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"topbar.png"]];
    topbarparent.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 65);
    [self.view addSubview:topbarparent];
    
    UIImageView *logo_img = [[UIImageView alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2.5, 31, 92.5f, 16)];
    logo_img.image=[UIImage imageNamed:@"logomod.png"];
    [topbarparent addSubview:logo_img];
    
    UIButton *menubt = [UIButton buttonWithType:UIButtonTypeCustom];
    menubt.frame = CGRectMake(10, 23, 35, 35);
    [menubt setBackgroundImage:[UIImage imageNamed:@"menu-icon.png"] forState:UIControlStateNormal];
    [menubt addTarget:self action:@selector(gomenu:) forControlEvents:UIControlEventTouchUpInside];
    [topbarparent addSubview:menubt];
    
    UIButton *plusbt = [UIButton buttonWithType:UIButtonTypeCustom];
    plusbt.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-45, 23, 35, 35);
    [plusbt setBackgroundImage:[UIImage imageNamed:@"plus-icon.png"] forState:UIControlStateNormal];
    [plusbt addTarget:self action:@selector(goPlus:) forControlEvents:UIControlEventTouchUpInside];
    [topbarparent addSubview:plusbt];
    
    leftmenu = [[ConSettingsView alloc] initWithFrame:CGRectMake(-([UIScreen mainScreen].bounds.size.width-40), 0, [UIScreen mainScreen].bounds.size.width-40, [UIScreen mainScreen].bounds.size.height)];
    [self.view addSubview:leftmenu];
    leftmenu.layer.zPosition=5;
    [leftmenu setHidden:YES];
    
}


-(void)viewDidAppear:(BOOL)animated
{
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    moveparent=0;
    
    topbarparent.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 65);
    [self.view addSubview:topbarparent];
    topbarparent.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"topbar.png"]];
    
    UIImageView *logo_img = [[UIImageView alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2.5, 31, 92.5f, 16)];
    logo_img.image=[UIImage imageNamed:@"logomod.png"];
    [topbarparent addSubview:logo_img];
    
    UIButton *menubt = [UIButton buttonWithType:UIButtonTypeCustom];
    menubt.frame = CGRectMake(10, 23, 35, 35);
    [menubt setBackgroundImage:[UIImage imageNamed:@"menu-icon.png"] forState:UIControlStateNormal];
    [menubt addTarget:self action:@selector(gomenu:) forControlEvents:UIControlEventTouchUpInside];
    [topbarparent addSubview:menubt];
    
    UIButton *plusbt = [UIButton buttonWithType:UIButtonTypeCustom];
    plusbt.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-45, 23, 35, 35);
    [plusbt setBackgroundImage:[UIImage imageNamed:@"plus-icon.png"] forState:UIControlStateNormal];
    [plusbt addTarget:self action:@selector(goPlus:) forControlEvents:UIControlEventTouchUpInside];
    [topbarparent addSubview:plusbt];
    
    UIButton *menubt1 = [UIButton buttonWithType:UIButtonTypeCustom];
    menubt1.frame = CGRectMake(0, 10, 85, 50);
    [menubt1 setBackgroundColor:[UIColor clearColor]];
    [menubt1 addTarget:self action:@selector(gomenu:) forControlEvents:UIControlEventTouchUpInside];
    [topbarparent addSubview:menubt1];
    
    UIButton *plusbt1 = [UIButton buttonWithType:UIButtonTypeCustom];
    plusbt1.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-70, 10, 70, 50);
    [plusbt1 setBackgroundColor:[UIColor clearColor]];
    [plusbt1 addTarget:self action:@selector(goPlus:) forControlEvents:UIControlEventTouchUpInside];
    [topbarparent addSubview:plusbt1];
    
    leftmenu = [[ConSettingsView alloc] initWithFrame:CGRectMake(-([UIScreen mainScreen].bounds.size.width-40), 0, [UIScreen mainScreen].bounds.size.width-40, [UIScreen mainScreen].bounds.size.height)];
    [self.view addSubview:leftmenu];
    leftmenu.layer.zPosition=5;
    [leftmenu setHidden:YES];
}

-(void)gomenu: (id)sender
{
    UIViewController *currentVC = self.navigationController.visibleViewController;
    
    DebugLog(@"menu clicked %@",currentVC);
    [currentVC.class chngpostion];
    //   currentVC.class *objctt = [[currentVC.class alloc]init];
    [self.view endEditing:YES];
    
    if (moveparent==0)
    {
        [leftmenu setHidden:NO];
        [UIView animateWithDuration:0.25
                         animations:^{
                             [leftmenu setFrame: CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-40, [UIScreen mainScreen].bounds.size.height)];
                             [topbarparent setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-40, 0, [UIScreen mainScreen].bounds.size.width, topbarparent.bounds.size.height)];
                             AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                             [del.tabBarController.tabBar setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-40, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
                             //                        [objctt.mainview setFrame:CGRectMake(280, (mainview.frame.origin.y), mainview.frame.size.width, mainview.frame.size.height)];
                             
                         }
                         completion:^(BOOL finished){
                             //                         [self.view bringSubviewToFront:leftmenu];
                             moveparent=1;
                         }];
    }
    else
    {
        [leftmenu setHidden:NO];
        [UIView animateWithDuration:0.25
                         animations:^{
                             [leftmenu setFrame: CGRectMake(-([UIScreen mainScreen].bounds.size.width-40), 0, [UIScreen mainScreen].bounds.size.width-40, [UIScreen mainScreen].bounds.size.height)];
                             [topbarparent setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, topbarparent.bounds.size.height)];
                             AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                             [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             [leftmenu setHidden:YES];
                             moveparent=0;
                         }];
    }
}

-(void)goPlus:(id)sender
{
    DebugLog(@"Plus clicked");
    ConAddFriendViewController *con = [[ConAddFriendViewController alloc]init];
    
    //    [self presentViewController:con animated:NO completion:nil];
    [self.navigationController pushViewController:con animated:NO];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [leftmenu setFrame: CGRectMake(-([UIScreen mainScreen].bounds.size.width-40), 0, [UIScreen mainScreen].bounds.size.width-40, [UIScreen mainScreen].bounds.size.height)];
    moveparent=0;
    //    [super viewDidDisappear:YES];
    [topbarparent setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 65)];
    //    [topbar removeFromSuperview];
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(navtopage:) name:@"pagenav" object:nil];
}

-(void)navtopage: (NSNotification *)notification
{
    DebugLog(@"aaaaaaa");
    [topbarparent setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 65)];
    
}
@end