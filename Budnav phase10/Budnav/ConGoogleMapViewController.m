//
//  ConGoogleMapViewController.m
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 06/06/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//
#import "ConAccountSettingsViewController.h"
#import "ConInviteViewController.h"
#import "ConGoogleMapViewController.h"
#import "AppDelegate.h"
#import <GoogleMaps/GoogleMaps.h>
#import "REVClusterAnnotationView.h"
#import "REVClusterMap.h"
#import "ConSyncLoaderViewController.h"
#import "ConProfileOwnViewController.h"
#import "ConRequestViewController.h"
#import "ConQROptionsViewController.h"
#import "ConNewProfileViewController.h"
#import "ConAddFriendViewController.h"
#import "ConNewRequestsViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"
#import "ConLocateGroupViewController.h"
#import "ConFullSettingsViewController.h"
#import "ConPersonalProfileViewController.h"

@interface ConGoogleMapViewController () <GMSMapViewDelegate,UIGestureRecognizerDelegate,CLLocationManagerDelegate>
{
    int move;
}
@end
@implementation ConGoogleMapViewController
{
    GMSMapView *mapView_;
}
@synthesize mainview;
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    self.navigationController.navigationBarHidden=YES;
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    move=0;
    
    newarr1 = [[NSMutableArray alloc]init];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    self.navigationController.navigationBarHidden=YES;
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:51.22
                                                            longitude:4
                                                                 zoom:4];   // ind 21,78
    
    mainview = [[UIView alloc]initWithFrame:CGRectMake(0, 65, [UIScreen mainScreen].bounds.size.width, self.view.frame.size.height-60)];
    [self.view addSubview:mainview];
    mainview.backgroundColor=[UIColor clearColor];
    
    mapView_ = [GMSMapView mapWithFrame:CGRectMake(0, 0, mainview.frame.size.width, mainview.frame.size.height) camera:camera];
    mapView_.myLocationEnabled = YES;
    [mainview addSubview:mapView_];
    [mapView_ animateToLocation:mapView_.myLocation.coordinate];
    // Creates a marker in the center of the map.
    
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=map&id=%@&access_token=%@&device_id=%@",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
    
    DebugLog(@"profile url: %@",urlString1);
    
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    
    NSError *error = nil;
    json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
             
                                            options:kNilOptions
             
                                              error:&error];
    DebugLog(@"json returns: %@",json1);
    
    NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
    DebugLog(@"err  %@",errornumber);
    if (![errornumber isEqualToString:@"0"])
    {
        DebugLog(@"if if");
        NSString *err_str = [json1 objectForKey:@"error"];
        alert = [[UIAlertView alloc] initWithTitle:@"Error in Retrieving Data!"
                                           message:err_str
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
    }
    
    if ([[json1 objectForKey:@"details"] isKindOfClass:[NSNull class]] || [json1 objectForKey:@"details"] == (id)[NSNull null] ||[[json1 objectForKey:@"details"] count] ==0)
    {
        DebugLog(@"if called req");
        alert = [[UIAlertView alloc] initWithTitle:@"No Locations Found!"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        
    }
    else
    {
        NSDictionary *newdict= [json1 objectForKey:@"details"];
        for (NSDictionary *reqdict in [newdict objectForKey:@"markers"])
        {
            [newarr1 addObject:reqdict];
        }
        DebugLog(@"map list array has: %@",newarr1);
        
    }
    
    DebugLog(@"newarr ache: %@",newarr1);
    int i=0;
    while (i<[newarr1 count])
    {
        double latitude=[[[newarr1 objectAtIndex:i]objectForKey:@"lat"] doubleValue];
        double longitude=[[[newarr1 objectAtIndex:i]objectForKey:@"lng"] doubleValue];
        
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake(latitude, longitude);
        marker.title = [[newarr1 objectAtIndex:i]objectForKey:@"name"];;
        marker.snippet = [[newarr1 objectAtIndex:i]objectForKey:@"surname"];;
        marker.map = mapView_;
        if([[[newarr1 objectAtIndex:i]objectForKey:@"business"]intValue] ==1)
            marker.icon = [UIImage imageNamed:@"locator2.png"];
        else
            marker.icon = [UIImage imageNamed:@"locator1.png"];
        i++;
    }
    
    UIPinchGestureRecognizer* pinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(didZoom:)];
    [pinchRecognizer setDelegate:self];
    [mapView_ addGestureRecognizer:pinchRecognizer];
    
    CLLocationManager *manager_;
    if (![CLLocationManager locationServicesEnabled]) {
        DebugLog(@"Please enable location services");
        return;
    }
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
        DebugLog(@"Please authorize location services");
        return;
    }
    
    manager_ = [[CLLocationManager alloc] init];
    manager_.delegate = self;
    manager_.desiredAccuracy = kCLLocationAccuracyBest;
    manager_.distanceFilter = 5.0f;
    [manager_ startUpdatingLocation];
    
}


- (void)didZoom:(UIGestureRecognizer*)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded){
        //        [self updateVisibleAnnotations];
    }
}

- (float)distanceFrom:(CGPoint)point1 to:(CGPoint)point2 {
    CGFloat xDist = (point2.x - point1.x);
    CGFloat yDist = (point2.y - point1.y);
    return sqrt((xDist * xDist) + (yDist * yDist));
}

- (NSSet *)annotationsInRect:(CGRect)rect forMapView:(GMSMapView *)mapView {
    GMSProjection *projection = mapView_.projection; //always take self.mapView because it is the only one zoomed on screen
    CLLocationCoordinate2D southWestCoordinates = [projection coordinateForPoint:CGPointMake(rect.origin.x, rect.origin.y + rect.size.height)];
    CLLocationCoordinate2D northEastCoordinates = [projection coordinateForPoint:CGPointMake(rect.origin.x + rect.size.width, rect.origin.y)];
    NSMutableSet *annotations = [NSMutableSet set];
    for (GMSMarker *marker in newarr) {
        if (marker.position.latitude < southWestCoordinates.latitude || marker.position.latitude >= northEastCoordinates.latitude) {
            continue;
        }
        if (marker.position.longitude < southWestCoordinates.longitude || marker.position.longitude >= northEastCoordinates.longitude) {
            continue;
        }
        [annotations addObject:marker.userData];
    }
    return annotations;
}

- (GMSMarker *)viewForAnnotation:(MKAnnotationView *)item forMapView:(GMSMapView *)mapView{
    for (GMSMarker *marker in newarr) {
        if (marker.userData == item) {
            return marker;
        }
    }
    return nil;
}



+(void)chngpostion
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Data" object:Nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getData:) name:@"Data" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(navtopage:) name:@"pagenav" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestreceivedaction) name:@"Requestreceived_push" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(acceptedrequestaction) name:@"Accepted_push" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AcceptedRequest) name:@"Accepted_request" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AddFriend) name:@"NewConnection" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ConnectionType) name:@"NewConnectionType" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AcceptedRequest) name:@"TypeChange" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shareLocation) name:@"ShareLocation" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdateInformation) name:@"UpdateInfo" object:nil];
    
}

-(void)UpdateInformation
{
    ConPersonalProfileViewController *PVC = [[ConPersonalProfileViewController alloc]init];
    PVC.toUpdateInfo = @"YES";
    [self.navigationController pushViewController:PVC animated:YES];
   // [[NSNotificationCenter defaultCenter] postNotificationName:@"Update Info" object:Nil];
    
}


-(void)shareLocation
{
    ConLocateGroupViewController *conLocate = [[ConLocateGroupViewController alloc]init];
    conLocate.group_id=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"sharedLocGrpId"]];
    [self.navigationController pushViewController:conLocate animated:YES];
}

-(void)ConnectionType
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDel.newrequestRedirect)
    {
      
    ConNewRequestsViewController *mng = [[ConNewRequestsViewController alloc]init];
       mng.userid = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userforrequest"] intValue];
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//        
       appDel.newrequestRedirect = NO;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:mng animated:YES];
        
    }
}

-(void)AddFriend
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDel.newUser)
    {

    
    ConAddFriendViewController *con = [[ConAddFriendViewController alloc]init];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
        
        appDel.newUser = NO;
    
  //  [[self navigationController].view.layer addAnimation:transition forKey:nil];
    [[self navigationController] pushViewController:con animated:YES];
        
    }
    
}

-(void)AcceptedRequest
{
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (del.profileRedirect)
    {
    
    ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
    // con.other=@"yes";
    con.request = YES;
    [self.tabBarController.tabBar setHidden:YES];
    // con.uid=[NSString stringWithFormat:@"%d",[userid intValue]];
    //                [self.navigationController presentViewController:con animated:NO completion:nil];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
        
    del.profileRedirect = NO;
    
   // [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
        
    }
    
}

-(void)requestreceivedaction
{
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
    ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
    con.other=@"yes";
    con.uid=[NSString stringWithFormat:@"%@",del.pushtoprofileid];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}

-(void)acceptedrequestaction
{
    ConRequestViewController *con = [[ConRequestViewController alloc]init];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}

-(void)navtopage: (NSNotification *)notification
{
    NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
    if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"4"])
    {
        ConAccountSettingsViewController *con = [[ConAccountSettingsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"6"])
    {
        ConInviteViewController *con = [[ConInviteViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"5"])
    {
        ConSyncLoaderViewController *con = [[ConSyncLoaderViewController alloc]init];
        //        [self.navigationController pushViewController:con animated:NO];
        
        [self presentViewController:con animated:NO completion:nil];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"2"])
    {
//        ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
        ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"3"])
    {
        ConQROptionsViewController *con = [[ConQROptionsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    [mainview setFrame:CGRectMake(0, mainview.frame.origin.y, mainview.frame.size.width, mainview.frame.size.height)];
    //    [prefs setObject:@"111" forKey:@"whichpage"];
}

-(void)getData:(NSNotification *)notification {
    
    if(move == 0) {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-40, mainview.frame.origin.y, mainview.frame.size.width, mainview.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             move = 1;
                         }];
    } else {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake(0, (mainview.frame.origin.y),mainview.frame.size.width, mainview.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             move = 0;
                         }];
    }
}

@end