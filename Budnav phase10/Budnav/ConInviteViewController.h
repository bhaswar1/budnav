//
//  ConInviteViewController.h
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 04/06/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConParentTopBarViewController.h"
#import <MessageUI/MessageUI.h>
#import "GAITrackedViewController.h"

@interface ConInviteViewController : GAITrackedViewController<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate,UIActionSheetDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,UISearchBarDelegate,UIAlertViewDelegate>
{
    //   UIView *mainview;
    UIActionSheet *anActionSheet;
    UISearchBar *searchBar;
}
@property (nonatomic, strong) UIView *mainview;
@property (nonatomic, strong) NSMutableArray *selectedIndexPaths;
@property (nonatomic, strong) NSString *searchfriends, *request_check;
+(void)chngpostion;
@end