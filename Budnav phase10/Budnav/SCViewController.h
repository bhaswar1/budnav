/*
 Copyright 2013 Scott Logic Ltd
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */


#import <UIKit/UIKit.h>
#import "ConParentTopBarViewController.h"
@interface SCViewController : ConParentTopBarViewController <UINavigationControllerDelegate>
@property (nonatomic, retain) NSString *request_type;
@end

/*
 Copyright 2013 Scott Logic Ltd
// http://www.apache.org/licenses/LICENSE-2.0 
#import "SCViewController.h"
#import "AppDelegate.h"
@import AVFoundation;
#import "SCShapeView.h"
#import "ConAccountSettingsViewController.h"
#import "ConNewRequestsViewController.h"
@interface SCViewController () <AVCaptureMetadataOutputObjectsDelegate,UIAlertViewDelegate> {
    AVCaptureVideoPreviewLayer *_previewLayer;
    SCShapeView *_boundingBox;
    NSTimer *_boxHideTimer, *redtimer;
    UILabel *_decodedMessage;
    UIView *redview, *mainview;
    UIAlertView *alert;
    NSString *resultscan, *useridstr;
}
@end
@implementation SCViewController
@synthesize request_type;

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    mainview = [[UIView alloc]initWithFrame:CGRectMake(0, 60, 320, self.view.frame.size.height-60)];
    [self.view addSubview:mainview];
    mainview.backgroundColor=[UIColor clearColor];
    
    // Create a new AVCaptureSession
    AVCaptureSession *session = [[AVCaptureSession alloc] init];
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError *error = nil;
    // Want the normal device
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
    if(input) {
        // Add the input to the session
        [session addInput:input];
    } else {
        return;
    }
    
    AVCaptureMetadataOutput *output = [[AVCaptureMetadataOutput alloc] init];
    // Have to add the output before setting metadata types
    [session addOutput:output];
    // What different things can we register to recognise?
    // We're only interested in QR Codes
    [output setMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]];
    // This VC is the delegate. Please call us on the main queue
    [output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    
    // Display on screen
    _previewLayer = [AVCaptureVideoPreviewLayer layerWithSession:session];
    _previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    //    _previewLayer.bounds = self.view.bounds;
    _previewLayer.frame= CGRectMake(0, 110, 320, self.view.bounds.size.height-100);
    _previewLayer.position = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds));
    [self.view.layer addSublayer:_previewLayer];
    
    // Add the view to draw the bounding box for the UIView
    _boundingBox = [[SCShapeView alloc] initWithFrame: CGRectMake(45, _previewLayer.bounds.origin.y+100, 231, 280) ];
    _boundingBox.backgroundColor = [UIColor clearColor];
    _boundingBox.center=self.view.center;
    _boundingBox.hidden = NO;
    _boundingBox.layer.borderColor= [[UIColor colorWithPatternImage:[UIImage imageNamed:@"focus.png"]]CGColor];
    _boundingBox.layer.borderWidth=1;
    [self.view addSubview:_boundingBox];
    
    // Add a label to display the resultant message
    _decodedMessage = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.view.bounds) - 150, CGRectGetWidth(self.view.bounds),75)];
    _decodedMessage.numberOfLines = 0;
    _decodedMessage.backgroundColor = [UIColor clearColor];
    _decodedMessage.textColor = [UIColor whiteColor];
    _decodedMessage.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:_decodedMessage];
    
    // Start the AVSession running
    [session startRunning];
    
    redview = [[UIView alloc]initWithFrame:CGRectMake(0, 150, 320, 4)];
    redview.center=self.view.center;
    redview.backgroundColor=[UIColor redColor];
    [self.view addSubview:redview];
    redview.layer.zPosition=3;
    redview.hidden=NO;
    redview.layer.shadowOffset = CGSizeMake(0, 5);
    redview.layer.shadowRadius = 5;
    redview.layer.shadowOpacity = 0.7;
    //    redview.layer.shadowColor=[[UIColor redColor]CGColor];
    
    redtimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                target:self
                                              selector:@selector(targetMethod:)
                                              userInfo:nil
                                               repeats:YES];
}

-(void)targetMethod:(NSTimer *)timer
{
    if (redview.hidden==YES)
        redview.hidden=NO;
    else
        redview.hidden=YES;
}

#pragma mark - AVCaptureMetadataOutputObjectsDelegate
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    for (AVMetadataObject *metadata in metadataObjects) {
        if ([metadata.type isEqualToString:AVMetadataObjectTypeQRCode]) {
            // Transform the meta-data coordinates to screen coords
            AVMetadataMachineReadableCodeObject *transformed = (AVMetadataMachineReadableCodeObject *)[_previewLayer transformedMetadataObjectForMetadataObject:metadata];
            // Update the frame on the _boundingBox view, and show it
            //            _boundingBox.frame = transformed.bounds;
            _boundingBox.hidden = NO;
            // Now convert the corners array into CGPoints in the coordinate system of the bounding box itself
            //    NSArray *translatedCorners = [self translatePoints:transformed.corners fromView:self.view toView:_boundingBox];
            // Set the corners array
            //            _boundingBox.corners = translatedCorners;
            
            // Update the view with the decoded text
            _decodedMessage.text = [transformed stringValue];
            
            resultscan = [transformed stringValue];
            NSArray *testarr= [resultscan componentsSeparatedByString:@"id="];
            NSString *newstr = [testarr objectAtIndex:1];
            NSArray *arrtest = [newstr componentsSeparatedByString:@"&&"];
            useridstr = [arrtest objectAtIndex:0];
            
            [self add_friend];
            // Start the timer which will hide the overlay
            [self startOverlayHideTimer];
        }
    }
}

#pragma mark - Utility Methods
- (void)startOverlayHideTimer
{
    // Cancel it if we're already running
    if(_boxHideTimer) {
        [_boxHideTimer invalidate];
    }
    // Restart it to hide the overlay when it fires
    _boxHideTimer = [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(removeBoundingBox:) userInfo:nil repeats:NO];
}

- (void)removeBoundingBox:(NSTimer *)sender
{
    // Hide the box and remove the decoded text
    //    _boundingBox.hidden = YES;
    _decodedMessage.text = @"";
}

- (NSArray *)translatePoints:(NSArray *)points fromView:(UIView *)fromView toView:(UIView *)toView
{
    NSMutableArray *translatedPoints = [NSMutableArray new];
    
    // The points are provided in a dictionary with keys X and Y
    for (NSDictionary *point in points) {
        // Let's turn them into CGPoints
        CGPoint pointValue = CGPointMake([point[@"X"] floatValue], [point[@"Y"] floatValue]);
        // Now translate from one view to the other
        CGPoint translatedPoint = [fromView convertPoint:pointValue toView:toView];
        // Box them up and add to the array
        [translatedPoints addObject:[NSValue valueWithCGPoint:translatedPoint]];
    }
    return [translatedPoints copy];
}

+(void)chngpostion
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Data" object:Nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getData:) name:@"Data" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(navtopage:) name:@"pagenav" object:nil];
}

-(void)navtopage: (NSNotification *)notification
{
    NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
    if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"2"])
    {
        ConAccountSettingsViewController *con = [[ConAccountSettingsViewController alloc]init];
        [self.navigationController pushViewController:con animated:NO];
    }
}


-(void)getData:(NSNotification *)notification {
    
    if(mainview.frame.origin.x == 0) {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake(280, mainview.frame.origin.y, mainview.frame.size.width, mainview.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                         }];
    } else {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake(0, (mainview.frame.origin.y),mainview.frame.size.width, mainview.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                         }];
    }
}


-(void)viewDidAppear:(BOOL)animated
{
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //    [del showTabStatus:YES];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    self.navigationController.navigationBarHidden=YES;
}

-(void)add_friend
{
    UIActivityIndicatorView *act = [[UIActivityIndicatorView alloc] init];
    act.center=self.view.center;
    [act startAnimating];
    act.hidden=NO;
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [act setColor:[UIColor blackColor]];
    [mainview addSubview:act];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *urlString1;
    
    if ([request_type isEqualToString:@"business"])
        
        urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=addrequest&id=%d&access_token=%@&device_id=%@&business=true",[useridstr intValue],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
    
    else
        
        urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=addrequest&id=%d&access_token=%@&device_id=%@",[useridstr intValue],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
    
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    
    NSError *error=nil;
    NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                              options:kNilOptions
                                                                error:&error];
 
    if ([[json_deny objectForKey:@"success"]intValue] == 1)
    {
        //    alert = [[UIAlertView alloc] initWithTitle:@"Request Successfully Sent!"
        //                                       message:nil
        //                                      delegate:self
        //                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        //    [alert show];
        
        ConNewRequestsViewController *con = [[ConNewRequestsViewController alloc]init];
        con.userid= [useridstr intValue];
        [self.navigationController pushViewController:con animated:NO];
    }
}
@end
*/

