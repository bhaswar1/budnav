//
//  ConSocialMediaViewController.m
//  Budnav
//
//  Created by intel on 15/05/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "ConSocialMediaViewController.h"
#import "SVProgressHUD.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

@interface ConSocialMediaViewController ()
{
    UIButton *save_btn;
    UILabel *fblabel, *skype_label, *twitter_label, *instagram_label, *scloud_label, *pint_label, *vimeo_label, *gplus_label, *link_label, *youtube_label;
    AppDelegate *del;
    UIImageView *bck_img;
}
@end

@implementation ConSocialMediaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor colorWithRed:(238.0f/255.0f) green:(238.0f/255.0f) blue:(238.0f/255.0f) alpha:1.0];
    
    del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, [[UIScreen mainScreen]bounds].size.height-44, [UIScreen mainScreen].bounds.size.width, 44)];
    del.tabBarController.tabBar.hidden = YES;
    self.navigationController.navigationBarHidden=YES;
    self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
    
    topbar = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64)];
    [self.view addSubview:topbar];
    //topbar.backgroundColor=[UIColor blackColor];
    topbar.layer.zPosition=2;
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
    
    // Add the gradient to the view
    [topbar.layer insertSublayer:gradient atIndex:0];
    
    
    UIImageView *logo_img = [[UIImageView alloc]init];
    logo_img.frame = CGRectMake(27+([UIScreen mainScreen].bounds.size.width-160)/2, 32, 101, 20);
    logo_img.backgroundColor = [UIColor clearColor];
    
    logo_img.image = [UIImage imageNamed:@"new_logo"];
    [topbar addSubview:logo_img];
    
    
    bck_img = [[UIImageView alloc]initWithFrame:CGRectMake(15,32,12,20)];//(13, 39, 10, 22)];
    bck_img.image=[UIImage imageNamed:@"back3"];
    [topbar addSubview:bck_img];
    
    UIButton *helpbt = [UIButton buttonWithType:UIButtonTypeCustom];
    helpbt.frame = CGRectMake(0, 0, logo_img.frame.origin.x
                              -1, 64);
    helpbt.backgroundColor=[UIColor clearColor];
    [helpbt setTitle:nil forState:UIControlStateNormal];
    
    [helpbt addTarget:self action:@selector(backColor) forControlEvents:UIControlEventTouchDown];
    [helpbt addTarget:self action:@selector(backColorAgain) forControlEvents:UIControlEventTouchDragExit];
    [helpbt addTarget:self action:@selector(backColor) forControlEvents:UIControlEventTouchDragEnter];
    [helpbt addTarget:self action:@selector(goback) forControlEvents:UIControlEventTouchUpInside];
    [topbar addSubview:helpbt];
    
    
    save_btn = [[UIButton alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-90, 25, 90, 39)];
    [save_btn setTitle:@"Save" forState:UIControlStateNormal];
    save_btn.backgroundColor = [UIColor clearColor];
    [save_btn.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Bold" size:20.0]];
    // [save_btn.titleLabel.text] = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    [save_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [topbar addSubview:save_btn];
    [save_btn addTarget:self action:@selector(changeColor) forControlEvents:UIControlEventTouchDown];
    [save_btn addTarget:self action:@selector(changeColor) forControlEvents:UIControlEventTouchDragEnter];
    [save_btn addTarget:self action:@selector(changeColorAgain) forControlEvents:UIControlEventTouchDragExit];
    
    [save_btn addTarget:self action:@selector(savechanges:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
//    act = [[UIActivityIndicatorView alloc] init];
//    act.center=self.view.center;//WithFrame:frame];
//    [act startAnimating];
//    act.layer.zPosition=1;
//    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
//    [act setColor:[UIColor blackColor]];
//    [self.view addSubview:act];
}

-(void)viewDidAppear:(BOOL)animated
{
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    //    [del showTabValues:YES];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, [[UIScreen mainScreen]bounds].size.height-44, [UIScreen mainScreen].bounds.size.width, 44)];
    self.navigationController.navigationBarHidden=YES;
    del.tabBarController.tabBar.hidden = YES;
    
    self.screenName = @"Edit social media";
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createEventWithCategory:@"Settings"
                                             action:@"Edit social media"
                                              label:nil
                                              value:nil] build]];

    
    imagesiconarr = [NSMutableArray arrayWithObjects:@"Facebook",@"Skype",@"Twitter",@"Instagram",@"Soundcloud",@"Pinterest",@"Vimeo",@"Google+",@"Linkedin",@"YouTube", nil];
    DebugLog(@"imagesicon : %@",imagesiconarr);
    
    toptextarr = [NSMutableArray arrayWithObjects:@"www.twitter.com/",@"www.pinterest.com/",@"www.facebook.com/",@"Google+",@"Youtube",@"Skype",@"LinkedIN",@"www.soundcloud.com/",@"www.instagram.com/",@"www.vimeo.com/",nil];
    
    
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 65, [UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height-65) ];
    scrollView.backgroundColor =[UIColor clearColor];
    scrollView.scrollEnabled = YES;
    scrollView.userInteractionEnabled = YES;
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width,[UIScreen mainScreen].bounds.size.height)];
    scrollView.showsVerticalScrollIndicator=NO;
    [self.view addSubview:scrollView];
    scrollView.contentOffset=CGPointMake(0, 0);
    
    
    fblabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
    [fblabel setBackgroundColor:[UIColor whiteColor]];
    [scrollView addSubview:fblabel];
    
    UILabel *fb_div = [[UILabel alloc] initWithFrame:CGRectMake(0,fblabel.frame.origin.y+fblabel.frame.size.height,[UIScreen mainScreen].bounds.size.width,0.6f)];
    fb_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f];
    [scrollView addSubview:fb_div];
    
    facebooktext = [[UITextField alloc]initWithFrame:CGRectMake(72, fblabel.frame.origin.y, [UIScreen mainScreen].bounds.size.width-72, 50)];
    facebooktext.backgroundColor=[UIColor clearColor];
    facebooktext.placeholder=@"Facebook numeric ID";
    [facebooktext setValue:[UIColor colorWithRed:(146.0f/255.0f) green:(146.0f/255.0f) blue:(146.0f/255.0f) alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    facebooktext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    facebooktext.textColor = [UIColor colorWithRed:(0.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0f];
    facebooktext.keyboardAppearance=UIKeyboardAppearanceDark;
    facebooktext.autocapitalizationType = UITextAutocapitalizationTypeNone;
    facebooktext.delegate=self;
    [scrollView addSubview:facebooktext];
    
    
    skype_label = [[UILabel alloc] initWithFrame:CGRectMake(0,fb_div.frame.origin.y+fb_div.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50)];
    [skype_label setBackgroundColor:[UIColor whiteColor]];
    [scrollView addSubview:skype_label];
    
    UILabel *skype_div = [[UILabel alloc] initWithFrame:CGRectMake(0,skype_label.frame.origin.y+skype_label.frame.size.height,[UIScreen mainScreen].bounds.size.width,0.6f)];
    skype_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f];
    [scrollView addSubview:skype_div];
    
    skypetext = [[UITextField alloc]initWithFrame:CGRectMake(72, skype_label.frame.origin.y, [UIScreen mainScreen].bounds.size.width-72, 50)];
    skypetext.backgroundColor=[UIColor clearColor];
    skypetext.placeholder=@"Skype username";
    [skypetext setValue:[UIColor colorWithRed:(146.0f/255.0f) green:(146.0f/255.0f) blue:(146.0f/255.0f) alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    skypetext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    skypetext.textColor = [UIColor colorWithRed:(0.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0f];
    skypetext.keyboardAppearance=UIKeyboardAppearanceDark;
    skypetext.autocapitalizationType = UITextAutocapitalizationTypeNone;
    skypetext.delegate=self;
    [scrollView addSubview:skypetext];
    
    
    twitter_label = [[UILabel alloc] initWithFrame:CGRectMake(0, skype_div.frame.origin.y+skype_div.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50)];
    [twitter_label setBackgroundColor:[UIColor whiteColor]];
    [scrollView addSubview:twitter_label];
    
    UILabel *twt_div = [[UILabel alloc] initWithFrame:CGRectMake(0,twitter_label.frame.origin.y+twitter_label.frame.size.height,[UIScreen mainScreen].bounds.size.width,0.6f)];
    twt_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f];
    [scrollView addSubview:twt_div];
    
    twittertext = [[UITextField alloc]initWithFrame:CGRectMake(72, twitter_label.frame.origin.y, [UIScreen mainScreen].bounds.size.width-72, 50)];
    twittertext.backgroundColor=[UIColor clearColor];
    twittertext.placeholder=@"Twitter username";
    [twittertext setValue:[UIColor colorWithRed:(146.0f/255.0f) green:(146.0f/255.0f) blue:(146.0f/255.0f) alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    twittertext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    twittertext.textColor = [UIColor colorWithRed:(0.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0f];
    twittertext.keyboardAppearance=UIKeyboardAppearanceDark;
    twittertext.autocapitalizationType = UITextAutocapitalizationTypeNone;
    twittertext.delegate=self;
    [scrollView addSubview:twittertext];
    
    
    instagram_label = [[UILabel alloc] initWithFrame:CGRectMake(0,twt_div.frame.origin.y+twt_div.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50)];
    [instagram_label setBackgroundColor:[UIColor whiteColor]];
    [scrollView addSubview:instagram_label];
    
    UILabel *inst_div = [[UILabel alloc] initWithFrame:CGRectMake(0,instagram_label.frame.origin.y+instagram_label.frame.size.height,[UIScreen mainScreen].bounds.size.width,0.6f)];
    inst_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f];
    [scrollView addSubview:inst_div];
    
    instagramtext = [[UITextField alloc]initWithFrame:CGRectMake(72, instagram_label.frame.origin.y, [UIScreen mainScreen].bounds.size.width-72, 50)];
    instagramtext.backgroundColor=[UIColor clearColor];
    instagramtext.placeholder=@"Instagram username";
    [instagramtext setValue:[UIColor colorWithRed:(146.0f/255.0f) green:(146.0f/255.0f) blue:(146.0f/255.0f) alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    instagramtext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    instagramtext.textColor = [UIColor colorWithRed:(0.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0f];
    instagramtext.keyboardAppearance=UIKeyboardAppearanceDark;
    instagramtext.autocapitalizationType = UITextAutocapitalizationTypeNone;
    instagramtext.delegate=self;
    [scrollView addSubview:instagramtext];
    
    
    
    scloud_label = [[UILabel alloc] initWithFrame:CGRectMake(0, inst_div.frame.origin.y+inst_div.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50)];
    [scloud_label setBackgroundColor:[UIColor whiteColor]];
    [scrollView addSubview:scloud_label];
    
    UILabel *sc_div = [[UILabel alloc] initWithFrame:CGRectMake(0,scloud_label.frame.origin.y+scloud_label.frame.size.height,[UIScreen mainScreen].bounds.size.width,0.6f)];
    sc_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f];
    [scrollView addSubview:sc_div];
    
    soundcloudtext = [[UITextField alloc]initWithFrame:CGRectMake(72, scloud_label.frame.origin.y, [UIScreen mainScreen].bounds.size.width-72, 50)];
    soundcloudtext.backgroundColor=[UIColor clearColor];
    soundcloudtext.placeholder=@"Soundcloud username";
    [soundcloudtext setValue:[UIColor colorWithRed:(146.0f/255.0f) green:(146.0f/255.0f) blue:(146.0f/255.0f) alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    soundcloudtext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    soundcloudtext.textColor = [UIColor colorWithRed:(0.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0f];
    soundcloudtext.keyboardAppearance=UIKeyboardAppearanceDark;
    soundcloudtext.autocapitalizationType = UITextAutocapitalizationTypeNone;
    soundcloudtext.delegate=self;
    [scrollView addSubview:soundcloudtext];
    
    
    pint_label = [[UILabel alloc] initWithFrame:CGRectMake(0,sc_div.frame.origin.y+sc_div.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50)];
    [pint_label setBackgroundColor:[UIColor whiteColor]];
    [scrollView addSubview:pint_label];
    
    UILabel *pint_div = [[UILabel alloc] initWithFrame:CGRectMake(0,pint_label.frame.origin.y+pint_label.frame.size.height,[UIScreen mainScreen].bounds.size.width,0.6f)];
    pint_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f];
    [scrollView addSubview:pint_div];
    
    pinteresttext = [[UITextField alloc]initWithFrame:CGRectMake(72, pint_label.frame.origin.y, [UIScreen mainScreen].bounds.size.width-72, 50)];
    pinteresttext.backgroundColor=[UIColor clearColor];
    pinteresttext.placeholder=@"Pinterest username";
    [pinteresttext setValue:[UIColor colorWithRed:(146.0f/255.0f) green:(146.0f/255.0f) blue:(146.0f/255.0f) alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    pinteresttext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    pinteresttext.textColor = [UIColor colorWithRed:(0.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0f];
    pinteresttext.keyboardAppearance=UIKeyboardAppearanceDark;
    pinteresttext.autocapitalizationType = UITextAutocapitalizationTypeNone;
    pinteresttext.delegate=self;
    [scrollView addSubview:pinteresttext];
    
    
    vimeo_label = [[UILabel alloc] initWithFrame:CGRectMake(0, pint_div.frame.origin.y+pint_div.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50)];
    [vimeo_label setBackgroundColor:[UIColor whiteColor]];
    [scrollView addSubview:vimeo_label];
    
    UILabel *vimeo_div = [[UILabel alloc] initWithFrame:CGRectMake(0,vimeo_label.frame.origin.y+vimeo_label.frame.size.height,[UIScreen mainScreen].bounds.size.width,0.6f)];
    vimeo_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f];
    [scrollView addSubview:vimeo_div];
    
    vimeotext = [[UITextField alloc]initWithFrame:CGRectMake(72, vimeo_label.frame.origin.y, [UIScreen mainScreen].bounds.size.width-72, 50)];
    vimeotext.backgroundColor=[UIColor clearColor];
    vimeotext.placeholder=@"Vimeo username";
    [vimeotext setValue:[UIColor colorWithRed:(146.0f/255.0f) green:(146.0f/255.0f) blue:(146.0f/255.0f) alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    vimeotext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    vimeotext.textColor = [UIColor colorWithRed:(0.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0f];
    vimeotext.keyboardAppearance=UIKeyboardAppearanceDark;
    vimeotext.autocapitalizationType = UITextAutocapitalizationTypeNone;
    vimeotext.delegate=self;
    [scrollView addSubview:vimeotext];
    
    
    gplus_label = [[UILabel alloc] initWithFrame:CGRectMake(0,vimeo_div.frame.origin.y+vimeo_div.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50)];
    [gplus_label setBackgroundColor:[UIColor whiteColor]];
    [scrollView addSubview:gplus_label];
    
    UILabel *gplus_div = [[UILabel alloc] initWithFrame:CGRectMake(0,gplus_label.frame.origin.y+gplus_label.frame.size.height,[UIScreen mainScreen].bounds.size.width,0.6f)];
    gplus_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f];
    [scrollView addSubview:gplus_div];
    
    googleplustext = [[UITextField alloc]initWithFrame:CGRectMake(72, gplus_label.frame.origin.y, [UIScreen mainScreen].bounds.size.width-72, 50)];
    googleplustext.backgroundColor=[UIColor clearColor];
    googleplustext.placeholder=@"Google + (link to profile)";
    [googleplustext setValue:[UIColor colorWithRed:(146.0f/255.0f) green:(146.0f/255.0f) blue:(146.0f/255.0f) alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    googleplustext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    googleplustext.textColor = [UIColor colorWithRed:(0.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0f];
    googleplustext.keyboardAppearance=UIKeyboardAppearanceDark;
    googleplustext.autocapitalizationType = UITextAutocapitalizationTypeNone;
    googleplustext.delegate=self;
    [scrollView addSubview:googleplustext];
    
    
    link_label = [[UILabel alloc] initWithFrame:CGRectMake(0,gplus_div.frame.origin.y+gplus_div.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50)];
    [link_label setBackgroundColor:[UIColor whiteColor]];
    [scrollView addSubview:link_label];
    
    UILabel *link_div = [[UILabel alloc] initWithFrame:CGRectMake(0,link_label.frame.origin.y+link_label.frame.size.height,[UIScreen mainScreen].bounds.size.width,0.6f)];
    link_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f];
    [scrollView addSubview:link_div];
    
    linkedintext = [[UITextField alloc]initWithFrame:CGRectMake(72, link_label.frame.origin.y, [UIScreen mainScreen].bounds.size.width-72, 50)];
    linkedintext.backgroundColor=[UIColor clearColor];
    linkedintext.placeholder=@"Linkedin (link to profile)";
    [linkedintext setValue:[UIColor colorWithRed:(146.0f/255.0f) green:(146.0f/255.0f) blue:(146.0f/255.0f) alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    linkedintext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    linkedintext.textColor = [UIColor colorWithRed:(0.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0f];
    linkedintext.keyboardAppearance=UIKeyboardAppearanceDark;
    linkedintext.autocapitalizationType = UITextAutocapitalizationTypeNone;
    linkedintext.delegate=self;
    [scrollView addSubview:linkedintext];
    
    
    youtube_label = [[UILabel alloc] initWithFrame:CGRectMake(0,link_div.frame.origin.y+link_div.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50)];
    [youtube_label setBackgroundColor:[UIColor whiteColor]];
    [scrollView addSubview:youtube_label];
    
    UILabel *youtube_div = [[UILabel alloc] initWithFrame:CGRectMake(0,youtube_label.frame.origin.y+youtube_label.frame.size.height,[UIScreen mainScreen].bounds.size.width,0.6f)];
    youtube_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f];
    [scrollView addSubview:youtube_div];
    
    youtubetext = [[UITextField alloc]initWithFrame:CGRectMake(72, youtube_label.frame.origin.y, [UIScreen mainScreen].bounds.size.width-72, 50)];
    youtubetext.backgroundColor=[UIColor clearColor];
    youtubetext.placeholder=@"Youtube (link to profile)";
    [youtubetext setValue:[UIColor colorWithRed:(146.0f/255.0f) green:(146.0f/255.0f) blue:(146.0f/255.0f) alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    youtubetext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    youtubetext.textColor = [UIColor colorWithRed:(0.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0f];
    youtubetext.keyboardAppearance=UIKeyboardAppearanceDark;
    youtubetext.autocapitalizationType = UITextAutocapitalizationTypeNone;
    youtubetext.delegate=self;
    [scrollView addSubview:youtubetext];
    
    
    
    for (int i = 0; i<10; i++) {
        
        UIImageView *social_img = [[UIImageView alloc]initWithFrame:CGRectMake(20, 9*(i+1)+41.5f*i, 32, 32)];
        [scrollView addSubview:social_img];
        social_img.backgroundColor=[UIColor clearColor];
        social_img.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[imagesiconarr objectAtIndex:i]]];
    }
    
    [self displayData];
    
}

-(void)goback
{
       
    del.tabBarController.tabBar.hidden = NO;
//    CATransition *transition = [CATransition animation];
//    
//    transition.duration = 0.4f;
//    
//    transition.type = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    [self.navigationController popViewControllerAnimated:YES];
    //[self dismissViewControllerAnimated:YES completion:nil];
}

-(void)savechanges:(id)sender
{
    [twittertext resignFirstResponder];
    [pinteresttext resignFirstResponder];
    [facebooktext resignFirstResponder];
    [googleplustext resignFirstResponder];
    [youtubetext resignFirstResponder];
    [skypetext resignFirstResponder];
    [instagramtext resignFirstResponder];
    [linkedintext resignFirstResponder];
    [vimeotext resignFirstResponder];
    [soundcloudtext resignFirstResponder];
    
    
    [UIView animateWithDuration:0.0 animations:^{
        
        [SVProgressHUD showWithStatus:@"Saving......"];
        
    }completion:^(BOOL finished){
        
        NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=editsocial&s_twitter=%@&s_pinterest=%@&s_facebook=%@&s_google=%@&s_youtube=%@&s_skype=%@&s_linkedin=%@&s_instagram=%@&s_soundcloud=%@&s_vimeo=%@&access_token=%@&device_id=%@",[twittertext.text stringByTrimmingCharactersInSet:whitespace],[pinteresttext.text stringByTrimmingCharactersInSet:whitespace],[facebooktext.text stringByTrimmingCharactersInSet:whitespace],[googleplustext.text stringByTrimmingCharactersInSet:whitespace],[youtubetext.text stringByTrimmingCharactersInSet:whitespace],[skypetext.text stringByTrimmingCharactersInSet:whitespace],[linkedintext.text stringByTrimmingCharactersInSet:whitespace],[instagramtext.text stringByTrimmingCharactersInSet:whitespace],[soundcloudtext.text stringByTrimmingCharactersInSet:whitespace],[vimeotext.text stringByTrimmingCharactersInSet:whitespace],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
        
        DebugLog(@"deny url: %@",urlString1);
        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
        
        NSError *error=nil;
        NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                  options:kNilOptions
                                                                    error:&error];
        DebugLog(@"deny json returns: %@",json_deny);
        DebugLog(@"errorno : %d",[[json_deny objectForKey:@"errorno"]intValue]);
        
        NSString *errornumber_reqcount= [NSString stringWithFormat:@"%@",[json_deny objectForKey:@"errorno"]];
        if (![errornumber_reqcount isEqualToString:@"0"])
        {
            DebugLog(@"if if");
            NSString *err_str_reqcount = [json_deny objectForKey:@"error"];
            alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                               message:err_str_reqcount
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            
            save_btn.alpha = 1.0f;
            [SVProgressHUD dismiss];
            [alert show];
            
        }
        
        if ([[json_deny objectForKey:@"success"]intValue] == 1)
        {
            save_btn.alpha = 1.0f;
            [SVProgressHUD dismiss];
            
            [del ownDetails];
            alert = [[UIAlertView alloc] initWithTitle:@"Changes have been Successfully Saved!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
            
//            CATransition *transition = [CATransition animation];
//            
//            transition.duration = 0.4f;
//            
//            transition.type = kCATransitionFade;
            
            del.tabBarController.tabBar.hidden = NO;
//            [[self navigationController].view.layer addAnimation:transition forKey:nil];
            [self.navigationController popViewControllerAnimated:YES];
            
        }

    }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if (textField == facebooktext)
    {
        if (scrollView.contentOffset.y < 0)
            [scrollView setContentOffset:CGPointMake(0, 0)];
    }
    if (textField == skypetext)
    {
        if (scrollView.contentOffset.y < 50)
            [scrollView setContentOffset:CGPointMake(0, 0)];
    }
    if (textField == twittertext)
    {
        if (scrollView.contentOffset.y < 100)
            [scrollView setContentOffset:CGPointMake(0, 0)];
    }
    if (textField == instagramtext)
    {
        if (scrollView.contentOffset.y < 150)
            [scrollView setContentOffset:CGPointMake(0,51)];
    }
    if (textField == soundcloudtext)
    {
        if (scrollView.contentOffset.y < 200)
            [scrollView setContentOffset:CGPointMake(0, 102)];
    }
    if (textField == pinteresttext)
    {
        if (scrollView.contentOffset.y < 250)
            [scrollView setContentOffset:CGPointMake(0, 153)];
    }
    if (textField == vimeotext)
    {
        if (scrollView.contentOffset.y < 300)
            [scrollView setContentOffset:CGPointMake(0, 204)];
    }
    if (textField == googleplustext)
    {
        if (scrollView.contentOffset.y < 350)
            [scrollView setContentOffset:CGPointMake(0, 255)];
    }
    if (textField == linkedintext)
    {
        if (scrollView.contentOffset.y < 400)
            [scrollView setContentOffset:CGPointMake(0, 306)];
    }
    if (textField == youtubetext)
    {
        if (scrollView.contentOffset.y < 450)
            [scrollView setContentOffset:CGPointMake(0, 357)];
    }
    
}

-(void)displayData
{
    if ([del.social_dict objectForKey:@"s_facebook"] != false && ![[del.social_dict objectForKey:@"s_facebook"] isKindOfClass:[NSNull class]] && ![[del.social_dict objectForKey:@"s_facebook"] isEqualToString:@"(null)"] && ![[del.social_dict objectForKey:@"s_facebook"] isEqualToString:@""]){
        
        facebooktext.text= [NSString stringWithFormat:@"%@",[del.social_dict objectForKey:@"s_facebook"]];
    }
    else{
        facebooktext.text=@"";
    }
    
    if ([del.social_dict objectForKey:@"s_skype"] != false && ![[del.social_dict objectForKey:@"s_skype"] isKindOfClass:[NSNull class]] && ![[del.social_dict objectForKey:@"s_skype"] isEqualToString:@"(null)"] && ![[del.social_dict objectForKey:@"s_skype"] isEqualToString:@""]){
        
        skypetext.text= [NSString stringWithFormat:@"%@",[del.social_dict objectForKey:@"s_skype"]];
    }
    else{
        skypetext.text=@"";
    }
    
    if ([del.social_dict objectForKey:@"s_twitter"] != false && ![[del.social_dict objectForKey:@"s_twitter"] isKindOfClass:[NSNull class]] && ![[del.social_dict objectForKey:@"s_twitter"] isEqualToString:@"(null)"] && ![[del.social_dict objectForKey:@"s_twitter"] isEqualToString:@""]){
        
        twittertext.text= [NSString stringWithFormat:@"%@",[del.social_dict objectForKey:@"s_twitter"]];
    }
    else{
        
        twittertext.text=@"";
    }
    
    if ([del.social_dict objectForKey:@"s_instagram"] != false && ![[del.social_dict objectForKey:@"s_instagram"] isKindOfClass:[NSNull class]] && ![[del.social_dict objectForKey:@"s_instagram"] isEqualToString:@"(null)"] && ![[del.social_dict objectForKey:@"s_instagram"] isEqualToString:@""]){
        
        instagramtext.text= [NSString stringWithFormat:@"%@",[del.social_dict objectForKey:@"s_instagram"]];
    }
    else{
        
        instagramtext.text=@"";
    }
    
    if ([del.social_dict objectForKey:@"s_soundcloud"] != false && ![[del.social_dict objectForKey:@"s_soundcloud"] isKindOfClass:[NSNull class]] && ![[del.social_dict objectForKey:@"s_soundcloud"] isEqualToString:@"(null)"] && ![[del.social_dict objectForKey:@"s_soundcloud"] isEqualToString:@""]){
        
        soundcloudtext.text= [NSString stringWithFormat:@"%@",[del.social_dict objectForKey:@"s_soundcloud"]];
    }
    else{
        
        soundcloudtext.text=@"";
    }
    
    if ([del.social_dict objectForKey:@"s_pinterest"] != false && ![[del.social_dict objectForKey:@"s_pinterest"] isKindOfClass:[NSNull class]] && ![[del.social_dict objectForKey:@"s_pinterest"] isEqualToString:@"(null)"] && ![[del.social_dict objectForKey:@"s_pinterest"] isEqualToString:@""]){
        
        pinteresttext.text= [NSString stringWithFormat:@"%@",[del.social_dict objectForKey:@"s_pinterest"]];
    }
    else{
        pinteresttext.text=@"";
    }
    
    if ([del.social_dict objectForKey:@"s_vimeo"] != false && ![[del.social_dict objectForKey:@"s_vimeo"] isKindOfClass:[NSNull class]] && ![[del.social_dict objectForKey:@"s_vimeo"] isEqualToString:@"(null)"] && ![[del.social_dict objectForKey:@"s_vimeo"] isEqualToString:@""]){
        
        vimeotext.text= [NSString stringWithFormat:@"%@",[del.social_dict objectForKey:@"s_vimeo"]];
    }
    else{
        
        vimeotext.text=@"";
    }
    
    if ([del.social_dict objectForKey:@"s_google"] != false && ![[del.social_dict objectForKey:@"s_google"] isKindOfClass:[NSNull class]] && ![[del.social_dict objectForKey:@"s_google"] isEqualToString:@"(null)"] && ![[del.social_dict objectForKey:@"s_google"] isEqualToString:@""]){
        
        googleplustext.text= [NSString stringWithFormat:@"%@",[del.social_dict objectForKey:@"s_google"]];
    }
    else{
        
        googleplustext.text=@"";
    }
    
    if ([del.social_dict objectForKey:@"s_linkedin"] != false && ![[del.social_dict objectForKey:@"s_linkedin"] isKindOfClass:[NSNull class]] && ![[del.social_dict objectForKey:@"s_linkedin"] isEqualToString:@"(null)"] && ![[del.social_dict objectForKey:@"s_linkedin"] isEqualToString:@""]){
        
        linkedintext.text= [NSString stringWithFormat:@"%@",[del.social_dict objectForKey:@"s_linkedin"]];
    }
    else{
        
        linkedintext.text=@"";
    }
    
    if ([del.social_dict objectForKey:@"s_youtube"] != false && ![[del.social_dict objectForKey:@"s_youtube"] isKindOfClass:[NSNull class]] && ![[del.social_dict objectForKey:@"s_youtube"] isEqualToString:@"(null)"] && ![[del.social_dict objectForKey:@"s_youtube"] isEqualToString:@""]){
        
        youtubetext.text= [NSString stringWithFormat:@"%@",[del.social_dict objectForKey:@"s_youtube"]];
    }
    else{
        
        youtubetext.text=@"";
    }
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [scrollView setContentOffset:CGPointMake(0, 0)];
    [textField resignFirstResponder];
    return YES;
}

-(void)changeColor
{
    save_btn.alpha = 0.5f;
}
-(void)changeColorAgain
{
    save_btn.titleLabel.alpha = 1.0f;
}

-(void)backColorAgain
{
    bck_img.alpha = 1.0f;
}
-(void)backColor
{
    bck_img.alpha = 0.5f;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
