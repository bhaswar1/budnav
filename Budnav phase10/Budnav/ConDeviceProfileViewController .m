//  ConDeviceProfileViewController.m
//  Contacter
//  Created by Soumarsi_kundu on 23/06/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
#import <AddressBookUI/AddressBookUI.h>
#import "ConDeviceProfileViewController.h"
#import "AppDelegate.h"
#import "ConAccountSettingsViewController.h"
#import "ConInviteViewController.h"
#import "UIImageView+WebCache.h"
#import "ConPictureProfViewController.h"
#import "ConSyncLoaderViewController.h"
#import "ConRequestViewController.h"
#import "ConProfileOwnViewController.h"
#import "ConAddFriendViewController.h"
#import "ConQROptionsViewController.h"
#import "ConNewProfileViewController.h"
#import "ConNewRequestsViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"
#import "ConLocateGroupViewController.h"
#import "ConFullSettingsViewController.h"
#import "ConPersonalProfileViewController.h"


@interface ConDeviceProfileViewController ()
{
    UIImage *image_prof_original,*image_prof;
    UIImageView *detailimg,*bck_img, *logo_img;
    UIView *tapbgview;
    NSMutableArray *phonesarr1;
    UIView *grayback;
    UIView *coverView;
    UIView *divider_10,*divider_11;
    UIAlertView *smsAlert1, *smsAlert2;
    UIButton *bgcallsms1, *bgcallsms;
    UIButton *mobileno, *homeno, *workno, *mail_lb, *address_lb;
    UIActivityIndicatorView *activity;
    UIView *change;
    UILabel *backlbl;
    UIButton *backBtn;
    AppDelegate *del;
    NSOperationQueue *loadqueue;
    NSMutableDictionary *deviceContactsPhoneDetails;
    NSString *streetdata, *citydata, *zipdata, *countrydata, *housenumberdata,*emaildata,*fullname,*idnew;
}
@end
@implementation ConDeviceProfileViewController
@synthesize ContactDetails_Dict;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tabBarController.tabBar.hidden = YES;
    self.view.backgroundColor = [UIColor whiteColor];
    y=110.0;
    
    del=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    del.tabBarController.tabBar.frame = CGRectMake(0, [[UIScreen mainScreen]bounds].size.height, [UIScreen mainScreen].bounds.size.width,44);
    self.tabBarController.tabBar.translucent = YES;
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    coverView = [[UIView alloc]init];
    coverView.frame = CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.origin.y+64);
   // coverView.backgroundColor = [UIColor blackColor];
    //    coverView.layer.zPosition=1;
    [self.view addSubview:coverView];
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, coverView.frame.size.height);
    
    // Add the gradient to the view
    [coverView.layer insertSublayer:gradient atIndex:0];
    
    
    
    
    change = [[UIView alloc]initWithFrame:CGRectMake(8,32,115,30)];
    [change setBackgroundColor:[UIColor clearColor]];
    [coverView addSubview:change];
    
    bck_img=[[UIImageView alloc]initWithFrame:CGRectMake(7, 0, 12, 20)];//(9,33,10,20)];//(13, 39, 10, 22)];
    bck_img.image=[UIImage imageNamed:@"back3"];
    [change addSubview:bck_img];
    
    backlbl=[[UILabel alloc]initWithFrame:CGRectMake(bck_img.frame.origin.x+bck_img.frame.size.width+5, 3, 95, 20)];
    backlbl.text=@"Back";
    backlbl.textColor=[UIColor whiteColor];
    backlbl.contentMode=UIViewContentModeScaleAspectFit;
    //    backlbl.font=[UIFont systemFontOfSize:17];//fontWithName:@"ProximaNova-Regular" size:17];
    backlbl.backgroundColor=[UIColor clearColor];
    //[change addSubview:backlbl];
    
    backBtn=[[UIButton alloc]init];//WithFrame:CGRectMake(bck_img.frame.origin.x, backlbl.frame.origin.y, backlbl.frame.origin.x+backlbl.frame.size.width-bck_img.frame.origin.x, backlbl.frame.size.height)];
    backBtn.frame=change.frame;
    backBtn.frame = CGRectMake(0, 27, 70, 37);
    [backBtn setTitle:@"" forState:UIControlStateNormal];
    backBtn.backgroundColor=[UIColor clearColor];
    [backBtn setTitle:@"" forState:UIControlStateNormal];
    //            [backBtn setBackgroundImage:[UIImage imageNamed:@"backnew_profileSelected"] forState:UIControlStateSelected];
    //            [backBtn setBackgroundImage:[UIImage imageNamed:@"backnew_profileSelected"] forState:UIControlStateHighlighted];
    [backBtn addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDown];
    [backBtn addTarget:self action:@selector(changecoloragain) forControlEvents:UIControlEventTouchDragExit];
    [backBtn addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDragEnter];
    [backBtn addTarget:self action:@selector(gobackoption) forControlEvents:UIControlEventTouchUpInside];
    [coverView addSubview:backBtn];
    
    
    logo_img = [[UIImageView alloc]init];
    logo_img.frame = CGRectMake(27+([UIScreen mainScreen].bounds.size.width-160)/2, 32, 101, 20);
    logo_img.backgroundColor = [UIColor clearColor];
    
    logo_img.image = [UIImage imageNamed:@"new_logo"];
    [coverView addSubview:logo_img];
    
    
    //    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    backBtn.frame = CGRectMake(9, 28, [UIImage imageNamed:@"backnew_profileUnselected"].size.width, [UIImage imageNamed:@"backnew_profileUnselected"].size.height);
    //    backBtn.backgroundColor=[UIColor clearColor];
    //    //    backBtn.layer.zPosition=2;
    //    [backBtn setBackgroundImage:[UIImage imageNamed:@"backnew_profileUnselected"] forState:UIControlStateNormal];
    //    [backBtn setBackgroundImage:[UIImage imageNamed:@"backnew_profileSelected"] forState:UIControlStateSelected];
    //    [backBtn setBackgroundImage:[UIImage imageNamed:@"backnew_profileSelected"] forState:UIControlStateHighlighted];
    //    [backBtn addTarget:self action:@selector(gobackoption) forControlEvents:UIControlEventTouchUpInside];
    //    [coverView addSubview:backBtn];
    
    
    
    //    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //    [del showTabStatus:YES];
    //    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    //    self.navigationController.navigationBarHidden=YES;
    //    [self.tabBarController.tabBar setHidden:YES];
}

-(void)changecoloragain
{
    //    change.backgroundColor=[UIColor clearColor];
    backlbl.alpha = 1.0f;
    bck_img.alpha = 1.0f;
}
-(void)changecolor
{
    //    change.backgroundColor=[UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0];
    backlbl.alpha = 0.5f;
    bck_img.alpha = 0.5f;
}


-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    move=0;
    //DebugLog(@"array ========= ====== %@",ContactDetails_Dict);
    
    //    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //    [del showTabStatus:YES];
    //    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    //    [self.tabBarController.tabBar setHidden:YES];
    
    
    
    
    
    //    UIView *backbuttonextended = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 85, 60)];
    //    backbuttonextended.backgroundColor=[UIColor clearColor];
    //    [self.view addSubview:backbuttonextended];
    //    backbuttonextended.userInteractionEnabled=YES;
    //
    //    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gobackoption)];
    //    [backbuttonextended addGestureRecognizer:tap];
    //
    //    UILabel *labelHead = [[UILabel alloc]initWithFrame:CGRectMake(30, 14, 100, 50)];
    //    labelHead.text = @"All contacts";
    //    labelHead.backgroundColor = [UIColor clearColor];
    //    labelHead.textColor = [UIColor whiteColor];
    //    [coverView addSubview:labelHead];
    
    //    UIButton *plusbt = [UIButton buttonWithType:UIButtonTypeCustom];
    //    plusbt.frame = CGRectMake(275, 23, 35, 35);
    //    [plusbt setBackgroundImage:[UIImage imageNamed:@"plus-icon.png"] forState:UIControlStateNormal];
    //    [plusbt addTarget:self action:@selector(goPlus:) forControlEvents:UIControlEventTouchUpInside];
    //    [coverView addSubview:plusbt];
    
    
    
    mainview = [[UIView alloc]initWithFrame:CGRectMake(0, 65, [UIScreen mainScreen].bounds.size.width, self.view.frame.size.height-65)];
    [self.view addSubview:mainview];
    //    mainview.layer.zPosition=4;
    mainview.backgroundColor=[UIColor whiteColor];
    
    
    activity = [[UIActivityIndicatorView alloc] init];//WithFrame:CGRectMake(135, 190, 90, 90)];
    activity.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [activity setColor:[UIColor blackColor]];
    activity.center = self.view.center;
    activity.hidesWhenStopped = YES;
    //    activity.layer.zPosition=6;
    //    activity.center = CGPointMake(150, 150);
    [mainview addSubview:activity];
    [activity startAnimating];
    
    loadqueue = [NSOperationQueue new];
    NSInvocationOperation *op = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(loaddata) object:nil];
    [loadqueue addOperation:op];
    
}


-(void)loaddata
{
    DebugLog(@"id details in array contains: %@",ContactDetails_Dict);
    NSString *userid = [ContactDetails_Dict objectForKey:@"id"];
    idnew= [userid substringFromIndex:1];
    
    
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
    ABRecordID recordId = (ABRecordID)[idnew intValue];
    
    ABRecordRef person = ABAddressBookGetPersonWithRecordID(addressBook, recordId);
    
    ABMultiValueRef emailProperty = ABRecordCopyValue(person, kABPersonEmailProperty);
    // DebugLog(@"jjjjjnnm %@ %d %@, %@",idnew,recordId,person, emailProperty);
    
    NSArray *emailArray = (__bridge NSArray *)ABMultiValueCopyArrayOfAllValues(emailProperty);
    
    NSMutableArray *mutableemail= [emailArray mutableCopy];
    
    int qq;
    for (qq =0; qq< [mutableemail count]; qq++)
    {
        if ([[mutableemail objectAtIndex:qq] isKindOfClass:[NSNull class]] || [mutableemail objectAtIndex:qq] == (id)[NSNull null] || [[mutableemail objectAtIndex:qq] length] < 3)
        {
            [mutableemail removeObjectAtIndex:qq];
            qq= qq-1;
        }
    }
    
    emaildata = [emailArray objectAtIndex:0];
    
    deviceContactsPhoneDetails = [[NSMutableDictionary alloc]init];
    
    ABMultiValueRef phones = ABRecordCopyValue(person, kABPersonPhoneProperty);
    for(CFIndex j = 0; j < ABMultiValueGetCount(phones); j++)
    {
        CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(phones, j);
        CFStringRef locLabel = ABMultiValueCopyLabelAtIndex(phones, j);
        NSString *phoneLabel =(__bridge NSString*) ABAddressBookCopyLocalizedLabel(locLabel);
        ////CFRelease(phones);
        NSString *phoneNumber = (__bridge NSString *)phoneNumberRef;
        //CFRelease(phoneNumberRef);
        //CFRelease(locLabel);
        //            DebugLog(@"phno:  - %@ (%@)", phoneNumber, phoneLabel);
        [deviceContactsPhoneDetails setValue:phoneNumber forKey:phoneLabel];
    }
    
    
    DebugLog(@"details from phonebook: %@",deviceContactsPhoneDetails);
    
    ////////////////////////////////////////////////////////  Address  ///////////////////////////////////////////////////////
    
    
    ABMultiValueRef addrss = ABRecordCopyValue(person, kABPersonAddressProperty);
    //DebugLog(@"addrss: %@",addrss);
    for(CFIndex k = 0; k < ABMultiValueGetCount(addrss); k++)
    {
        ABMultiValueRef stt = ABRecordCopyValue(person, kABPersonAddressProperty);
        if (ABMultiValueGetCount(stt) > 0) {
            CFDictionaryRef dict = ABMultiValueCopyValueAtIndex(stt, 0);
            //                    DebugLog(@"dict address contacts: %@",dict);
            NSString *streetp = CFDictionaryGetValue(dict, kABPersonAddressStreetKey);
            NSString *cityp = CFDictionaryGetValue(dict, kABPersonAddressCityKey);
            NSString *zipcdep = CFDictionaryGetValue(dict, kABPersonAddressZIPKey);
            //                    NSString *statep = CFDictionaryGetValue(dict, kABPersonAddressStateKey);
            NSString *countryp = CFDictionaryGetValue(dict, kABPersonAddressCountryKey);
            
            if ([streetp isKindOfClass:[NSNull class]] || streetp == (id)[NSNull null])
                streetdata = @"";
            else
                streetdata = streetp;
            
            if ([cityp isKindOfClass:[NSNull class]] || cityp == (id)[NSNull null])
                citydata =@"";
            else
                citydata = cityp;
            
            if ([zipcdep isKindOfClass:[NSNull class]] || zipcdep == (id)[NSNull null])
                zipdata =@"";
            else
                zipdata =zipcdep;
            
            if ([countryp isKindOfClass:[NSNull class]] || countryp == (id)[NSNull null])
                countrydata =@"";
            else
                countrydata =countryp;
            
            housenumberdata = @"";
            
        }
        else
        {
            streetdata = @"";
            citydata =@"";
            zipdata =@"";
            countrydata =@"";
            housenumberdata = @"";
        }
        
    }
    DebugLog(@"country: %@   city: %@",countrydata,citydata);
    //DebugLog(@"email,street, city, zipcode, housenumber, country  %@, %@, %@, %@, %@, %@",emaildata,streetdata,citydata,zipdata,housenumberdata,countrydata);
    NSString *whole_name= [[NSString stringWithFormat:@"%@ ",[ContactDetails_Dict objectForKey:@"fullname"]] capitalizedString];
    NSArray *myWords = [whole_name componentsSeparatedByString:@" "];
    
    NSString *fnm= [[myWords objectAtIndex:0] capitalizedString];
    NSString *lnm;
    
    for (int k=1;k< [myWords count];k++)
    {
        if (k==1)
        {
            if ([[myWords objectAtIndex:k] isKindOfClass:[NSNull class]] || [myWords objectAtIndex:k] == (id)[NSNull null] ||[[myWords objectAtIndex:k] length] ==0 || [[myWords objectAtIndex:k] isEqualToString:@"(null)"])
            {
                lnm = @"";
            }
            else
                lnm= [[NSString stringWithFormat:@"%@",[myWords objectAtIndex:k]] capitalizedString];
        }
        else
            lnm= [[NSString stringWithFormat:@"%@ %@",lnm,[myWords objectAtIndex:k]] capitalizedString];
    }
    if ([fnm length] ==0 || [fnm isKindOfClass:[NSNull class]] || fnm == (id)[NSNull null] || [fnm isEqualToString:@"(null)"])
        whole_name = [[NSString stringWithFormat:@"%@",lnm] capitalizedString];
    
    else if ([lnm length] ==0 || [lnm isKindOfClass:[NSNull class]] || lnm == (id)[NSNull null] || [lnm isEqualToString:@"(null)"])
        whole_name = [[NSString stringWithFormat:@"%@",fnm] capitalizedString];
    else
        whole_name = [NSString stringWithFormat:@"%@ %@",[fnm capitalizedString],[lnm capitalizedString]];
    
    
    
    fullname= whole_name;
    
    
    [self performSelectorOnMainThread:@selector(displaydata) withObject:nil waitUntilDone:NO];
}

-(void)displaydata
{
    divider_1 = [[UIView alloc]init];//WithFrame:CGRectMake(14.5f, 210.0f, 291, 1)];
    [divider_1 setBackgroundColor:[UIColor colorWithRed:(239.0f / 255.0f) green:(239.0f / 255.0f) blue:(239.0f / 255.0f) alpha:1.0f]];
    [mainview addSubview:divider_1];
    
    divider_2 = [[UIView alloc]init];//WithFrame:CGRectMake(14.5f, mail_lb.frame.origin.y+mail_lb.frame.size.height+15, 291, 1)];
    [divider_2 setBackgroundColor:[UIColor colorWithRed:(239.0f / 255.0f) green:(239.0f / 255.0f) blue:(239.0f / 255.0f) alpha:1.0f]];
    [mainview addSubview:divider_2];
    
    divider_9 = [[UIView alloc]init];//WithFrame:CGRectMake(14.5f, mail_lb.frame.origin.y+mail_lb.frame.size.height+15, 291, 1)];
    [divider_9 setBackgroundColor:[UIColor colorWithRed:(239.0f / 255.0f) green:(239.0f / 255.0f) blue:(239.0f / 255.0f) alpha:1.0f]];
    [mainview addSubview:divider_9];
    
    divider_10 = [[UIView alloc]init];//WithFrame:CGRectMake(14.5f, mail_lb.frame.origin.y+mail_lb.frame.size.height+15, 291, 1)];
    [divider_10 setBackgroundColor:[UIColor colorWithRed:(239.0f / 255.0f) green:(239.0f / 255.0f) blue:(239.0f / 255.0f) alpha:1.0f]];
    [mainview addSubview:divider_10];
    
    divider_11 = [[UIView alloc]init];//WithFrame:CGRectMake(14.5f, mail_lb.frame.origin.y+mail_lb.frame.size.height+15, 291, 1)];
    [divider_11 setBackgroundColor:[UIColor colorWithRed:(239.0f / 255.0f) green:(239.0f / 255.0f) blue:(239.0f / 255.0f) alpha:1.0f]];
    [mainview addSubview:divider_11];
    
    
    
    //===============Mobile no.=================//
    
    mobileno = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [mobileno setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 40.0f, 0, 0)];
    
    mobileno.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    [mobileno setBackgroundImage:[UIImage imageNamed:@"detbuttonNormal"] forState:UIControlStateNormal];
    
    [mobileno setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateHighlighted];
    
    [mobileno setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateSelected];
    
    [mobileno.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
    
    [mobileno setTitleColor:[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f]forState:UIControlStateNormal];
    
    //    [mobileno setTitle:[NSString stringWithFormat:@"%@",[deviceContactsPhoneDetails objectForKey:@"mobile"]] forState:UIControlStateNormal];
    
    [mobileno addTarget:self action:@selector(mobilenotap:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    mobileimg = [[UIImageView alloc]init];//WithFrame:CGRectMake(10.0f, 10.0f, [UIImage imageNamed:@"mobile_profile"].size.width, [UIImage imageNamed:@"mobile_profile"].size.height+12)];//WithFrame:CGRectMake(15, 195-60, 14, 13.5f)];
    mobileimg.image=[UIImage imageNamed:@"mobile_profile"];
    [mobileno addSubview:mobileimg];
    
    //    mobileno = [[UILabel alloc]init];//WithFrame:CGRectMake(38, 195-60, 275, 15)];
    //    [mainview addSubview:mobileno];
    //    mobileno.backgroundColor=[UIColor clearColor];
    //    mobileno.text = [NSString stringWithFormat:@"%@",[deviceContactsPhoneDetails objectForKey:@"mobile"]];
    //    mobileno.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    //    //    mobileno.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    //    mobileno.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
    DebugLog(@"MOBILE EXIST OR NOT:%@",[deviceContactsPhoneDetails objectForKey:@"mobile"]);
    //    if ([deviceContactsPhoneDetails objectForKey:@"mobile"] != false && ![[deviceContactsPhoneDetails objectForKey:@"mobile"] isEqualToString:@"0"] && [[deviceContactsPhoneDetails objectForKey:@"mobile"] length] > 1 && ![[deviceContactsPhoneDetails objectForKey:@"mobile"] isEqualToString:@"(null)"])
    //    {
    //        mobileimg.image=[UIImage imageNamed:@"mobile_profile"];
    //
    //        mobilearray = [[NSMutableArray alloc]init];
    //        //DebugLog(@"mobile array %@",deviceContactsPhoneDetails );
    //        [mobilearray addObject:[deviceContactsPhoneDetails objectForKey:@"mobile"]];
    //    }
    
    //    UITapGestureRecognizer *mobile_Tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(mobilenotap:)];
    //    [mobileno addGestureRecognizer:mobile_Tap];
    //    mobileno.userInteractionEnabled = YES;
    
    //==============Home no.===========//
    
    homeno = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [homeno setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 40.0f, 0, 0)];
    
    homeno.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    [homeno setBackgroundImage:[UIImage imageNamed:@"detbuttonNormal"] forState:UIControlStateNormal];
    
    [homeno setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateHighlighted];
    
    [homeno setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateSelected];
    
    [homeno.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
    
    [homeno setTitleColor:[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f]forState:UIControlStateNormal];
    
    //    if (![[deviceContactsPhoneDetails objectForKey:@"home"] isKindOfClass:[NSNull class]] && ![[deviceContactsPhoneDetails objectForKey:@"home"] isEqualToString:@"(null)"] && ![[deviceContactsPhoneDetails objectForKey:@"home"] isEqualToString:@""] && [[deviceContactsPhoneDetails objectForKey:@"home"] length]>0) {
    //
    //        homeimg = [[UIImageView alloc]init];//WithFrame:CGRectMake(15.0f, 160.0f, 14.0f, 13.5f)];
    //        homeimg.image = [UIImage imageNamed:@"home_profile"];
    //        [homeno addSubview:homeimg];
    //        [homeno setTitle:[NSString stringWithFormat:@"%@",[deviceContactsPhoneDetails objectForKey:@"home"]] forState:UIControlStateNormal];
    //
    //    }
    
    
    
    [homeno addTarget:self action:@selector(homenotap:) forControlEvents:UIControlEventTouchUpInside];
    
    homeimg = [[UIImageView alloc]init];//WithFrame:CGRectMake(15.0f, 160.0f, 14.0f, 13.5f)];
    homeimg.image = [UIImage imageNamed:@"home_profile"];
    [homeno addSubview:homeimg];
    
    
    
    
    
    //    homeno = [[UILabel alloc]init];//WithFrame:CGRectMake(38, 160.0f, 275, 15)];
    //    [mainview addSubview:homeno];
    //    homeno.backgroundColor=[UIColor clearColor];
    //    homeno.text = [NSString stringWithFormat:@"%@",[deviceContactsPhoneDetails objectForKey:@"home"]];
    //    //           mobileno.font=[UIFont fontWithName:@"ProximaNova-Bold.otf" size:19];
    //    homeno.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    //    homeno.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
    //
    //
    //    UITapGestureRecognizer *home_Tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(homenotap:)];
    //    [homeno addGestureRecognizer:home_Tap];
    //    homeno.userInteractionEnabled = YES;
    
    //=======================Work no.==================//
    
    
    
    
    workno = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [workno setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 40.0f, 0, 0)];
    
    workno.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    [workno setBackgroundImage:[UIImage imageNamed:@"detbuttonNormal"] forState:UIControlStateNormal];
    
    [workno setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateHighlighted];
    
    [workno setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateSelected];
    
    [workno.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
    
    [workno setTitleColor:[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f]forState:UIControlStateNormal];
    
    //    if (![[deviceContactsPhoneDetails objectForKey:@"work"] isKindOfClass:[NSNull class]] && ![[deviceContactsPhoneDetails objectForKey:@"work"] isEqualToString:@"(null)"] && ![[deviceContactsPhoneDetails objectForKey:@"work"] isEqualToString:@""] && [[deviceContactsPhoneDetails objectForKey:@"work"] length]>0) {
    //
    //
    //        [workno setTitle:[NSString stringWithFormat:@"%@",[deviceContactsPhoneDetails objectForKey:@"work"]] forState:UIControlStateNormal];
    //        [workno addSubview:workimg];
    //    }
    
    
    [workno addTarget:self action:@selector(worknotap:) forControlEvents:UIControlEventTouchUpInside];
    
    workimg = [[UIImageView alloc]init];//WithFrame:CGRectMake(15.0f, 185.0f, 14.0f, 13.5f)];
    //  workimg.image = [UIImage imageNamed:@"office_red.png"];
    workimg.image = [UIImage imageNamed:@"mobile_profile"];
    [workno addSubview:workimg];
    
    
    
    
    //    workno = [[UILabel alloc]initWithFrame:CGRectMake(38, 185.0f, 275, 15)];
    //    [mainview addSubview:workno];
    //    workno.backgroundColor=[UIColor clearColor];
    //    workno.text = [NSString stringWithFormat:@"%@",[deviceContactsPhoneDetails objectForKey:@"work"]];
    //    //           mobileno.font=[UIFont fontWithName:@"ProximaNova-Bold.otf" size:19];
    //    workno.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    //    workno.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
    
    workarray = [[NSMutableArray alloc]init];
    
    [workarray addObject:workno.titleLabel];
    
    //    UITapGestureRecognizer *work_Tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(worknotap:)];
    //    [workno addGestureRecognizer:work_Tap];
    //    workno.userInteractionEnabled = YES;
    
    
    
    //===============================Mail======================//
    
    
    
    
    
    mail_lb = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [mail_lb setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 40.0f, 0, 0)];
    
    mail_lb.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    [mail_lb setBackgroundImage:[UIImage imageNamed:@"detbuttonNormal"] forState:UIControlStateNormal];
    
    [mail_lb setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateHighlighted];
    
    [mail_lb setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateSelected];
    
    [mail_lb.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
    
    [mail_lb setTitleColor:[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f]forState:UIControlStateNormal];
    //    if (![emaildata isKindOfClass:[NSNull class]] && ![emaildata isEqualToString:@"(null)"] && ![emaildata isEqualToString:@""] && [emaildata length]>0) {
    //
    //        [mail_lb setTitle:[NSString stringWithFormat:@"%@",emaildata] forState:UIControlStateNormal];
    //        mailimg.image=[UIImage imageNamed:@"email_profile"];
    //        [mail_lb addSubview:mailimg];
    //    }
    
    
    [mail_lb addTarget:self action:@selector(emailtap:) forControlEvents:UIControlEventTouchUpInside];
    
    mailimg =[[UIImageView alloc]init];//WithFrame:CGRectMake(10, 10, [UIImage imageNamed:@"email_profile"].size.width, [UIImage imageNamed:@"email_profile"].size.height+12)];//WithFrame:CGRectMake(15, divider_1.frame.origin.y+divider_1.frame.size.height+15, 16, 11)];
    // mailimg.image=[UIImage imageNamed:@"email_profile"];
    mailimg.image=[UIImage imageNamed:@"email_profile"];
    [mail_lb addSubview:mailimg];
    
    
    
    //    mail_lb = [[UILabel alloc]init];//]WithFrame:CGRectMake(38, divider_1.frame.origin.y+divider_1.frame.size.height+10.5f, 275, 19)];
    //    [mainview addSubview:mail_lb];
    //    mail_lb.backgroundColor=[UIColor clearColor];
    //    mail_lb.text = [NSString stringWithFormat:@"%@",emaildata]; //[ContactDetails_Dict objectForKey:@"email"]
    //    //    personaltext.font=[UIFont fontWithName:@"Mark Simonson - Proxima Nova Alt Condensed Bold" size:19];
    //    mail_lb.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    //    mail_lb.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
    //
    //    UITapGestureRecognizer *mail_Tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(emailtap:)];
    //    [mail_lb addGestureRecognizer:mail_Tap];
    //    mail_lb.userInteractionEnabled = YES;
    
    
    
    
    //    address_str = [NSString stringWithFormat:@"%@%@",[ContactDetails_Dict objectForKey:@"city"],[ContactDetails_Dict objectForKey:@"country"]];
    //    [ContactDetails_Dict objectForKey:@"country"]
    
    //=================================Address===============================//
    
    
    
    
    if ([countrydata isKindOfClass:[NSNull class]] || countrydata == (id)[NSNull null] || [countrydata length] == 0 || [countrydata isEqualToString:@"(null)"]) {
        
        address_str = @"";
    }
    else{
        if ([citydata isKindOfClass:[NSNull class]] || citydata == (id)[NSNull null] || [citydata length]==0 || [citydata isEqualToString:@"(null)"]) {
            address_str= [NSString stringWithFormat:@"%@",countrydata];
        }
        else{
            
            if ([zipdata isKindOfClass:[NSNull class]] || zipdata == (id)[NSNull null] || [zipdata length]==0 || [zipdata isEqualToString:@"(null)"]) {
                
                address_str= [NSString stringWithFormat:@"%@, \n%@",citydata,countrydata];
            }
            else
            {
                if ([streetdata isKindOfClass:[NSNull class]] || streetdata == (id)[NSNull null] || [streetdata length]==0 || [streetdata isEqualToString:@"(null)"]) {
                    
                    address_str= [NSString stringWithFormat:@"%@, %@ \n%@",zipdata,citydata,countrydata];
                    
                }
                else
                {
                    if ([housenumberdata isKindOfClass:[NSNull class]] || housenumberdata == (id)[NSNull null] || [housenumberdata length]==0 || [housenumberdata isEqualToString:@"(null)"]) {
                        
                        address_str= [NSString stringWithFormat:@"%@,\n%@, %@\n%@",streetdata,zipdata,citydata,countrydata];
                        
                    }
                    else{
                        address_str= [NSString stringWithFormat:@"%@, %@, \n%@, %@ \n%@",streetdata,housenumberdata,zipdata,citydata,countrydata];
                        
                    }
                    
                }
                
                
            }
            
            
        }
    }
    
    
    
    
    
    
    address_lb = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [address_lb setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 40.0f, 0, 0)];
    
    address_lb.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    [address_lb setBackgroundImage:[UIImage imageNamed:@"detbuttonNormal"] forState:UIControlStateNormal];
    
    [address_lb setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateHighlighted];
    
    [address_lb setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateSelected];
    
    //    [address_lb.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
    
    [address_lb setTitleColor:[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f]forState:UIControlStateNormal];
    
    
    address_img=[[UIImageView alloc]init];//WithFrame:CGRectMake(15,divider_2.frame.origin.y+divider_2.frame.size.height+15,12,17)];
    address_img.image=[UIImage imageNamed:@"map_profile"];
    [address_lb addSubview:address_img];
    //    if (![address_str isKindOfClass:[NSNull class]] && ![address_str isEqualToString:@"(null)"] && ![address_str isEqualToString:@""] && [address_str length]>0) {
    //
    //        [address_lb setTitle:[NSString stringWithFormat:@"%@",address_str] forState:UIControlStateNormal];
    //        address_img.image=[UIImage imageNamed:@"map_profile"];
    //
    //    }
    
    
    
    //    [address_lb addTarget:self action:@selector(emailtap:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    
    //    address_lb = [[UILabel alloc]initWithFrame:CGRectMake(38,divider_2.frame.origin.y+divider_2.frame.size.height+15,12,17)];
    //    [mainview addSubview:address_lb];
    //    address_lb.backgroundColor=[UIColor clearColor];
    //    address_lb.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
    //    address_lb.text = address_str;
    //    DebugLog(@"addresss=====%@",address_str);
    //    //    prof_name.font=[UIFont fontWithName:@"Mark Simonson - Proxima Nova Alt Condensed Bold" size:19];
    //    address_lb.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    //    address_lb.numberOfLines=0;
    
    //===========================================MOBILE=======================//
    
    if ([deviceContactsPhoneDetails objectForKey:@"mobile"] != false && ![[deviceContactsPhoneDetails objectForKey:@"mobile"] isEqualToString:@"0"] && [[deviceContactsPhoneDetails objectForKey:@"mobile"] length] > 1 && ![[deviceContactsPhoneDetails objectForKey:@"mobile"] isEqualToString:@"(null)"])
    {
        mobileimg.frame = CGRectMake(13, 20, [UIImage imageNamed:@"mobile_profile"].size.width/1.5,[UIImage imageNamed:@"mobile_profile"].size.height/1.5);
        //        mobileimg.frame = CGRectMake(13, 18, 14, 18);
        mobileno.frame = CGRectMake(0,y, [UIScreen mainScreen].bounds.size.width, 50);
        divider_9.frame = CGRectMake(0.0f, mobileno.frame.origin.y+mobileno.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        [mobileno setTitle:[NSString stringWithFormat:@"%@",[deviceContactsPhoneDetails objectForKey:@"mobile"]] forState:UIControlStateNormal];
        //        divider_9.backgroundColor=[UIColor redColor];
        //                DebugLog(@"mobile frame %@",NSStringFromCGRect(mobileno.frame)) ;
        //                DebugLog(@"Divider frame %@",NSStringFromCGRect(divider_9.frame));
        y+=50;
        [mainview addSubview:mobileno];
        
    }
    else
    {
        mobileimg.frame = CGRectMake(13, 20, [UIImage imageNamed:@"mobile_profile"].size.width/1.5,0);
        //        mobileimg.frame = CGRectMake(13, 18, 14, 18);
        mobileno.frame = CGRectMake(0,y, [UIScreen mainScreen].bounds.size.width, 0);
        divider_9.frame = CGRectMake(0.0f, mobileno.frame.origin.y+mobileno.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        //        divider_9.backgroundColor=[UIColor redColor];
        //                DebugLog(@"mobile frame %@",NSStringFromCGRect(mobileno.frame)) ;
        //                DebugLog(@"Divider frame %@",NSStringFromCGRect(divider_9.frame));
        y+=50;
        [mainview addSubview:mobileno];
        
    }
    
    //    if ([deviceContactsPhoneDetails objectForKey:@"mobile"] != false && ![[deviceContactsPhoneDetails objectForKey:@"mobile"] isEqualToString:@"0"] && [[deviceContactsPhoneDetails objectForKey:@"mobile"] length] > 1 && ![[deviceContactsPhoneDetails objectForKey:@"mobile"] isEqualToString:@"(null)"])
    //    {
    //        DebugLog(@"asche...");
    //
    //        [mobileno setHidden:NO];
    //        [mobileimg setHidden:NO];
    //        mobileimg.frame = CGRectMake(13, 15, 14, 13.5f);
    //        mobileno.frame = CGRectMake(0,grayback.frame.origin.y+grayback.frame.size.height, [UIScreen mainScreen].bounds.size.width, [UIImage imageNamed:@"detbuttonNormal"].size.height);
    //        divider_9.frame = CGRectMake(0.0f, mobileno.frame.origin.y+mobileno.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
    //
    //    }
    //    else {
    //
    //        [mobileno setHidden:YES];
    //        [mobileimg setHidden:YES];
    //        mobileimg.frame = CGRectMake(13, 65, 14, 0);
    //        mobileno.frame = CGRectMake(0,grayback.frame.origin.y+grayback.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
    //        divider_9.frame = CGRectMake(0.0f, workno.frame.origin.y+workno.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
    //    }
    //    if (![[deviceContactsPhoneDetails objectForKey:@"mobile"] isEqualToString:@""] && ![[deviceContactsPhoneDetails objectForKey:@"home"] isEqualToString:@""] && ![[deviceContactsPhoneDetails objectForKey:@"work"] isEqualToString:@""]) {
    //
    //        divider_1.frame = CGRectMake(14.5f, workno.frame.origin.y+workno.frame.size.height+7, 291, 1);
    //    }
    
    //=========================================HOME===============================//
    
    if (![[deviceContactsPhoneDetails objectForKey:@"home"] isKindOfClass:[NSNull class]] && ![[deviceContactsPhoneDetails objectForKey:@"home"] isEqualToString:@"(null)"] && ![[deviceContactsPhoneDetails objectForKey:@"home"] isEqualToString:@""] && [[deviceContactsPhoneDetails objectForKey:@"home"] length]>0)
    {
        homeimg.frame = CGRectMake(13, 20, [UIImage imageNamed:@"home_profile"].size.width/1.5, [UIImage imageNamed:@"home_profile"].size.height/1.5);
        //        homeimg.frame = CGRectMake(13, 15, 14, 13.5f);
        homeno.frame = CGRectMake(0,divider_9.frame.origin.y+divider_9.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50);
        [homeno setTitle:[NSString stringWithFormat:@"%@",[deviceContactsPhoneDetails objectForKey:@"home"]] forState:UIControlStateNormal];
        divider_10.frame = CGRectMake(0.0f, homeno.frame.origin.y+homeno.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        //        divider_10.backgroundColor=[UIColor blueColor];
        //                DebugLog(@"home frame %@",NSStringFromCGRect(homeno.frame)) ;
        //                DebugLog(@"Divider frame %@",NSStringFromCGRect(divider_10.frame));
        y+=50;
        [mainview addSubview:homeno];
    }
    else
    {
        homeimg.frame = CGRectMake(13, 20, [UIImage imageNamed:@"home_profile"].size.width/1.5, 0);
        //        homeimg.frame = CGRectMake(13, 15, 14, 13.5f);
        homeno.frame = CGRectMake(0,divider_9.frame.origin.y+divider_9.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        divider_10.frame = CGRectMake(0.0f, homeno.frame.origin.y+homeno.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        //        divider_10.backgroundColor=[UIColor blueColor];
        //                DebugLog(@"home frame %@",NSStringFromCGRect(homeno.frame)) ;
        //                DebugLog(@"Divider frame %@",NSStringFromCGRect(divider_10.frame));
        //        y+=50;
        [mainview addSubview:homeno];
    }
    
    //////////////////////////////mobile & home is available
    
    
    //    if ([deviceContactsPhoneDetails objectForKey:@"mobile"] != false && ![[deviceContactsPhoneDetails objectForKey:@"mobile"] isEqualToString:@"0"] && [[deviceContactsPhoneDetails objectForKey:@"mobile"] length] > 1 && ![[deviceContactsPhoneDetails objectForKey:@"mobile"] isEqualToString:@"(null)"] && [deviceContactsPhoneDetails objectForKey:@"home"] != false && ![[deviceContactsPhoneDetails objectForKey:@"home"] isEqualToString:@"0"] && [[deviceContactsPhoneDetails objectForKey:@"home"] length] > 1 && ![[deviceContactsPhoneDetails objectForKey:@"home"] isEqualToString:@"(null)"])
    //    {
    //
    //        [homeno setHidden:NO];
    //        [homeimg setHidden:NO];
    //        homeimg.frame = CGRectMake(13, 15, 14, 13.5f);
    //        homeno.frame = CGRectMake(0,divider_9.frame.origin.y+divider_9.frame.size.height, [UIScreen mainScreen].bounds.size.width, [UIImage imageNamed:@"detbuttonNormal"].size.height);
    //        divider_10.frame = CGRectMake(0.0f, homeno.frame.origin.y+homeno.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
    //
    //////////////////////////No mobile only home
    
    //    else if ([deviceContactsPhoneDetails objectForKey:@"mobile"] == false && [[deviceContactsPhoneDetails objectForKey:@"mobile"] isEqualToString:@"0"] && [[deviceContactsPhoneDetails objectForKey:@"mobile"] isEqualToString:@"(null)"]    && [deviceContactsPhoneDetails objectForKey:@"home"] != false && ![[deviceContactsPhoneDetails objectForKey:@"home"] isEqualToString:@"0"] && [[deviceContactsPhoneDetails objectForKey:@"home"] length] > 1 && ![[deviceContactsPhoneDetails objectForKey:@"home"] isEqualToString:@"(null)"])
    //    {
    //        [homeno setHidden:NO];
    //        [homeimg setHidden:NO];
    //        homeimg.frame = CGRectMake(13, 15, 14, 13.5f);
    //        homeno.frame = CGRectMake(0,grayback.frame.origin.y+grayback.frame.size.height, [UIScreen mainScreen].bounds.size.width, [UIImage imageNamed:@"detbuttonNormal"].size.height);
    //        divider_10.frame = CGRectMake(0.0f, homeno.frame.origin.y+homeno.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
    //    }
    //    else{
    //
    //        [homeno setHidden:YES];
    //        [homeimg setHidden:YES];
    //        homeimg.frame = CGRectMake(13, 15, 14, 0);
    //        homeno.frame = CGRectMake(0,divider_9.frame.origin.y+divider_9.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
    //        divider_10.frame = CGRectMake(0.0f, homeno.frame.origin.y+homeno.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
    //    }
    
    //==============================================WORK=================================//
    
    
    if (![[deviceContactsPhoneDetails objectForKey:@"work"] isKindOfClass:[NSNull class]] && ![[deviceContactsPhoneDetails objectForKey:@"work"] isEqualToString:@"(null)"] && ![[deviceContactsPhoneDetails objectForKey:@"work"] isEqualToString:@""] && [[deviceContactsPhoneDetails objectForKey:@"work"] length]>0)
    {
        workimg.frame = CGRectMake(13, 22, [UIImage imageNamed:@"mobile_profile"].size.width/1.5,[UIImage imageNamed:@"mobile_profile"].size.height/1.5);//14, 13.5f);
        workno.frame = CGRectMake(0,divider_10.frame.origin.y+divider_10.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50);
        [workno setTitle:[NSString stringWithFormat:@"%@",[deviceContactsPhoneDetails objectForKey:@"work"]] forState:UIControlStateNormal];
        divider_11.frame = CGRectMake(0.0f, workno.frame.origin.y+workno.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        y+=50;
        //        divider_11.backgroundColor=[UIColor blackColor];
        //        DebugLog(@"work frame: %@",NSStringFromCGRect(workno.frame));
        //        DebugLog(@"divider3 frame: %@", NSStringFromCGRect(divider_11.frame));
        [mainview addSubview:workno];
        
    }
    else
    {
        workimg.frame = CGRectMake(13, 22, [UIImage imageNamed:@"mobile_profile"].size.width/1.5,0);//14, 13.5f);
        workno.frame = CGRectMake(0,divider_10.frame.origin.y+divider_10.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        divider_11.frame = CGRectMake(0.0f, workno.frame.origin.y+workno.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        //        divider_11.backgroundColor=[UIColor blackColor];
        //        DebugLog(@"work frame: %@",NSStringFromCGRect(workno.frame));
        //        DebugLog(@"divider3 frame: %@", NSStringFromCGRect(divider_11.frame));
        [mainview addSubview:workno];
    }
    
    ////////////////mobile home & work is available
    
    //    if ([deviceContactsPhoneDetails objectForKey:@"mobile"] != false && ![[deviceContactsPhoneDetails objectForKey:@"mobile"] isEqualToString:@"0"] && [[deviceContactsPhoneDetails objectForKey:@"mobile"] length] > 1 && ![[deviceContactsPhoneDetails objectForKey:@"mobile"] isEqualToString:@"(null)"] && [deviceContactsPhoneDetails objectForKey:@"home"] != false && ![[deviceContactsPhoneDetails objectForKey:@"home"] isEqualToString:@"0"] && [[deviceContactsPhoneDetails objectForKey:@"home"] length] > 1 && ![[deviceContactsPhoneDetails objectForKey:@"home"] isEqualToString:@"(null)"] && [deviceContactsPhoneDetails objectForKey:@"work"] != false && ![[deviceContactsPhoneDetails objectForKey:@"work"] isEqualToString:@"0"] && [[deviceContactsPhoneDetails objectForKey:@"work"] length] > 1 && ![[deviceContactsPhoneDetails objectForKey:@"work"] isEqualToString:@"(null)"])
    //    {
    //        [workno setHidden:NO];
    //        [workimg setHidden:NO];
    //        workimg.frame = CGRectMake(13, 15, 14, 13.5f);
    //        workno.frame = CGRectMake(0,divider_10.frame.origin.y+divider_10.frame.size.height, [UIScreen mainScreen].bounds.size.width, [UIImage imageNamed:@"detbuttonNormal"].size.height);
    //        divider_11.frame = CGRectMake(0.0f, workno.frame.origin.y+workno.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
    //
    //
    //
    //    }else
    //
    //        /////////////No mobile only home & work is available
    //
    //        if ([[deviceContactsPhoneDetails objectForKey:@"mobile"]isEqualToString:@""] && [deviceContactsPhoneDetails objectForKey:@"home"] != false && ![[deviceContactsPhoneDetails objectForKey:@"home"] isEqualToString:@"0"] && [[deviceContactsPhoneDetails objectForKey:@"home"] length] > 1 && ![[deviceContactsPhoneDetails objectForKey:@"home"] isEqualToString:@"(null)"] && [deviceContactsPhoneDetails objectForKey:@"work"] != false && ![[deviceContactsPhoneDetails objectForKey:@"work"] isEqualToString:@"0"] && [[deviceContactsPhoneDetails objectForKey:@"work"] length] > 1 && ![[deviceContactsPhoneDetails objectForKey:@"work"] isEqualToString:@"(null)"])
    //        {
    //            [workno setHidden:NO];
    //            [workimg setHidden:NO];
    //            workimg.frame = CGRectMake(13, 15, 14, 13.5f);
    //            workno.frame = CGRectMake(0,divider_10.frame.origin.y+divider_10.frame.size.height, [UIScreen mainScreen].bounds.size.width, [UIImage imageNamed:@"detbuttonNormal"].size.height);
    //            divider_11.frame = CGRectMake(0.0f, workno.frame.origin.y+workno.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
    //
    //            ///////////////No mobile no home only work
    //
    //        }else if ([[deviceContactsPhoneDetails objectForKey:@"mobile"]isEqualToString:@""] && [[deviceContactsPhoneDetails objectForKey:@"home"]isEqualToString:@""] && [deviceContactsPhoneDetails objectForKey:@"work"] != false && ![[deviceContactsPhoneDetails objectForKey:@"work"] isEqualToString:@"0"] && [[deviceContactsPhoneDetails objectForKey:@"work"] length] > 1 && ![[deviceContactsPhoneDetails objectForKey:@"work"] isEqualToString:@"(null)"])
    //        {
    //            [workno setHidden:NO];
    //            [workimg setHidden:NO];
    //            workimg.frame = CGRectMake(13, 15, 14, 13.5f);
    //            workno.frame = CGRectMake(0,divider_10.frame.origin.y+divider_10.frame.size.height, [UIScreen mainScreen].bounds.size.width, [UIImage imageNamed:@"detbuttonNormal"].size.height);
    //            divider_11.frame = CGRectMake(0.0f, workno.frame.origin.y+workno.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
    //        }
    //
    //    /////////////////No home only mobile & work is available
    //
    //        else if ([deviceContactsPhoneDetails objectForKey:@"mobile"] != false && ![[deviceContactsPhoneDetails objectForKey:@"mobile"] isEqualToString:@"0"] && [[deviceContactsPhoneDetails objectForKey:@"mobile"] length] > 1 && ![[deviceContactsPhoneDetails objectForKey:@"mobile"] isEqualToString:@"(null)"] && [[deviceContactsPhoneDetails objectForKey:@"home"]isEqualToString:@""] && [deviceContactsPhoneDetails objectForKey:@"work"] != false && ![[deviceContactsPhoneDetails objectForKey:@"work"] isEqualToString:@"0"] && [[deviceContactsPhoneDetails objectForKey:@"work"] length] > 1 && ![[deviceContactsPhoneDetails objectForKey:@"work"] isEqualToString:@"(null)"])
    //        {
    //            DebugLog(@"NO HOME ONLY MOBILE & WORK");
    //
    //            [workno setHidden:NO];
    //            [workimg setHidden:NO];
    //            workimg.frame = CGRectMake(13, 15, 14, 13.5f);
    //            workno.frame = CGRectMake(0,divider_9.frame.origin.y+divider_9.frame.size.height, [UIScreen mainScreen].bounds.size.width, [UIImage imageNamed:@"detbuttonNormal"].size.height);
    //            divider_11.frame = CGRectMake(0.0f, workno.frame.origin.y+workno.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
    //
    //        }
    //
    //        else{
    //
    //            DebugLog(@"MOBILE = %@  HOME = %@  WORK = %@",[deviceContactsPhoneDetails objectForKey:@"mobile"],[deviceContactsPhoneDetails objectForKey:@"home"],[deviceContactsPhoneDetails objectForKey:@"work"]);
    //
    //            DebugLog(@"NO WORK");
    //
    //            [workno setHidden:YES];
    //            [workimg setHidden:YES];
    //            workimg.frame = CGRectMake(13, 15, 14, 0);
    //            workno.frame = CGRectMake(0,divider_10.frame.origin.y+divider_10.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
    //            divider_11.frame = CGRectMake(0.0f, workno.frame.origin.y+workno.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
    //        }
    
    //    if (![[deviceContactsPhoneDetails objectForKey:@"mobile"] isEqualToString:@""] && ![[deviceContactsPhoneDetails objectForKey:@"home"] isEqualToString:@""] && ![[deviceContactsPhoneDetails objectForKey:@"work"] isEqualToString:@""]) {
    //
    //        divider_1.frame = CGRectMake(14.5f, workno.frame.origin.y+workno.frame.size.height+5, 291, 1);
    //    }
    //    else if (![[deviceContactsPhoneDetails objectForKey:@"mobile"] isEqualToString:@""] && [[deviceContactsPhoneDetails objectForKey:@"home"] isKindOfClass:[NSNull class]] &&[deviceContactsPhoneDetails objectForKey:@"home"] == (id)[NSNull null] && [[deviceContactsPhoneDetails objectForKey:@"work"] isKindOfClass:[NSNull class]] &&[deviceContactsPhoneDetails objectForKey:@"work"] == (id)[NSNull null]){
    //
    //
    //        divider_1.frame = CGRectMake(14.5f, mobileno.frame.origin.y+mobileno.frame.size.height+5, 291, 1);
    //    }
    //    else if (![[deviceContactsPhoneDetails objectForKey:@"mobile"] isEqualToString:@""] && ![[deviceContactsPhoneDetails objectForKey:@"home"] isEqualToString:@""] && [[deviceContactsPhoneDetails objectForKey:@"work"] isKindOfClass:[NSNull class]] &&[deviceContactsPhoneDetails objectForKey:@"work"] == (id)[NSNull null]){
    //
    //        divider_1.frame = CGRectMake(14.5f, homeno.frame.origin.y+homeno.frame.size.height+5, 291, 1);
    //    }
    //
    //    else if (![[deviceContactsPhoneDetails objectForKey:@"mobile"] isEqualToString:@""] && [[deviceContactsPhoneDetails objectForKey:@"home"] isKindOfClass:[NSNull class]] &&[deviceContactsPhoneDetails objectForKey:@"home"] == (id)[NSNull null] && ![[deviceContactsPhoneDetails objectForKey:@"work"] isEqualToString:@""]){
    //
    //
    //        divider_1.frame = CGRectMake(14.5f, workno.frame.origin.y+workno.frame.size.height+5, 291, 1);
    //    }
    //
    //    else if ([[deviceContactsPhoneDetails objectForKey:@"mobile"] isKindOfClass:[NSNull class]] &&[deviceContactsPhoneDetails objectForKey:@"mobile"] == (id)[NSNull null] && ![[deviceContactsPhoneDetails objectForKey:@"home"] isEqualToString:@""] && ![[deviceContactsPhoneDetails objectForKey:@"work"] isEqualToString:@""]){
    //
    //
    //        divider_1.frame = CGRectMake(14.5f, workno.frame.origin.y+workno.frame.size.height+5, 291, 1);
    //    }
    //    else if ([[deviceContactsPhoneDetails objectForKey:@"mobile"] isKindOfClass:[NSNull class]] &&[deviceContactsPhoneDetails objectForKey:@"mobile"] == (id)[NSNull null] && [[deviceContactsPhoneDetails objectForKey:@"home"] isKindOfClass:[NSNull class]] &&[deviceContactsPhoneDetails objectForKey:@"home"] == (id)[NSNull null] && [[deviceContactsPhoneDetails objectForKey:@"work"] isKindOfClass:[NSNull class]] &&[deviceContactsPhoneDetails objectForKey:@"work"] == (id)[NSNull null]){
    //
    //
    //        divider_1.frame = CGRectMake(14.5f, mobileno.frame.origin.y+mobileno.frame.size.height+5, 291, 0);
    //
    //
    //    }
    //    else if ([[deviceContactsPhoneDetails objectForKey:@"mobile"] isKindOfClass:[NSNull class]] &&[deviceContactsPhoneDetails objectForKey:@"mobile"] == (id)[NSNull null] && ![[deviceContactsPhoneDetails objectForKey:@"home"] isEqualToString:@""] && [[deviceContactsPhoneDetails objectForKey:@"work"] isKindOfClass:[NSNull class]] &&[deviceContactsPhoneDetails objectForKey:@"work"] == (id)[NSNull null]){
    //
    //        divider_1.frame = CGRectMake(14.5f, homeno.frame.origin.y+homeno.frame.size.height+5, 291, 0);
    //    }
    
    //===================================Mail===============================//
    
//    if(emaildata != nil)
     if(emaildata != nil && ![emaildata isKindOfClass:[NSNull class]] && ![emaildata isEqualToString:@"(null)"] && ![emaildata isEqualToString:@""] && [emaildata length]>0)
    {
        mailimg.frame = CGRectMake(13, 22, [UIImage imageNamed:@"email_profile"].size.width/1.5, [UIImage imageNamed:@"email_profile"].size.height/1.5);//14, 18);
        DebugLog(@"mail img : %@", NSStringFromCGRect(mailimg.frame));
        mail_lb.frame = CGRectMake(0,divider_11.frame.origin.y+divider_11.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50);
        [mail_lb setTitle:emaildata forState:UIControlStateNormal];
        divider_2.frame = CGRectMake(0.0f, mail_lb.frame.origin.y+mail_lb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        //        y+=50;
        //        divider_2.backgroundColor=[UIColor greenColor];
        DebugLog(@"mail  frame: %@",NSStringFromCGRect(mail_lb.frame));
        //        DebugLog(@"divider4 frame: %@",NSStringFromCGRect(divider_2.frame));
        [mainview addSubview:mail_lb];
    }
    else
    {
        mailimg.frame = CGRectMake(13, 22, [UIImage imageNamed:@"email_profile"].size.width/1.5, 0);
        DebugLog(@"mail img : %@", NSStringFromCGRect(mailimg.frame));
        mail_lb.frame = CGRectMake(0,divider_11.frame.origin.y+divider_11.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        divider_2.frame = CGRectMake(0.0f, mail_lb.frame.origin.y+mail_lb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        DebugLog(@"mail  frame: %@",NSStringFromCGRect(mail_lb.frame));
        [mainview addSubview:mail_lb];
        
    }
    
    //    else {
    //
    //        // No joy...
    //        if ([deviceContactsPhoneDetails objectForKey:@"work"] != false && ![[deviceContactsPhoneDetails objectForKey:@"work"] isEqualToString:@"0"] && [[deviceContactsPhoneDetails objectForKey:@"work"] length] > 1 && ![[deviceContactsPhoneDetails objectForKey:@"work"] isEqualToString:@"(null)"] && emaildata != false && ![emaildata isEqualToString:@"0"] && [emaildata length] > 1 && ![emaildata isEqualToString:@"(null)"]) {
    //
    //            DebugLog(@"ASDFGHJKL");
    //
    //            [mail_lb setHidden:NO];
    //            [mailimg setHidden:NO];
    //            mailimg.frame = CGRectMake(13, 15, 14, 13.5f);
    //            mail_lb.frame = CGRectMake(0,divider_11.frame.origin.y+divider_11.frame.size.height, [UIScreen mainScreen].bounds.size.width, [UIImage imageNamed:@"detbuttonNormal"].size.height);
    //
    //        }else if(emaildata != false && ![emaildata isEqualToString:@"0"] && [emaildata length] > 1 && ![emaildata isEqualToString:@"(null)"]){
    //
    //            DebugLog(@"DDDDDDDDDDDDDDDDDDD>>>>>>>>>>>>>>>>>");
    //
    //            [mail_lb setHidden:NO];
    //            [mailimg setHidden:NO];
    //            mailimg.frame = CGRectMake(13, 15, 14, 13.5f);
    //            mail_lb.frame = CGRectMake(0,divider_10.frame.origin.y+divider_10.frame.size.height, [UIScreen mainScreen].bounds.size.width, [UIImage imageNamed:@"detbuttonNormal"].size.height);
    //
    //        }
    //        else{
    //
    //            DebugLog(@"EEEEEEEEEEEE>>>>>>>>>>>>>>>>>%@",emaildata);
    //
    //            mailimg.frame = CGRectMake(13, 15, 14, 0);
    //            mail_lb.frame = CGRectMake(0,divider_10.frame.origin.y+divider_10.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
    //        }
    //
    //        if (![emaildata isEqualToString:@""]&& ![emaildata isEqualToString:@"(null)"]) {
    //            divider_2.frame = CGRectMake(0.0f, mail_lb.frame.origin.y+mail_lb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
    //        }
    //        else{
    //
    //            divider_2.frame = CGRectMake(0.0f, mail_lb.frame.origin.y+mail_lb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
    //        }
    //    }
    
    //======================================Address==================================//
    DebugLog(@"address: %@",address_str);
    if (![address_str isKindOfClass:[NSNull class]]&& address_str != (id)[NSNull null]&& ![address_str isEqualToString:@"(null)"]&&![address_str isEqualToString:@""]&&[address_str length]!=0)
    {
        address_img.frame = CGRectMake(13, 15,[UIImage imageNamed:@"map_profile"].size.width/1.5,[UIImage imageNamed:@"map_profile"].size.height/1.5);
        UILabel *address_label = [[UILabel alloc] init];
        
        address_label.backgroundColor = [UIColor clearColor];
        address_label.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
        
        address_label.text = address_str;
        address_label.numberOfLines=4;
        [address_label sizeToFit];
        [address_lb addSubview:address_label];
        
        if ([streetdata length]>0 || [housenumberdata length]>0) {
            
            if ([zipdata length]>0 || [citydata length]>0) {
                
                if ([countrydata length]>0) {
                    
                    
                    address_lb.frame = CGRectMake(0,divider_2.frame.origin.y+divider_2.frame.size.height, [UIScreen mainScreen].bounds.size.width, 70);
                    address_label.frame = CGRectMake(40, -15, 270, 100);
                    
                }
                else
                {
                    
                    
                    address_lb.frame = CGRectMake(0,divider_2.frame.origin.y+divider_2.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50);
                    address_label.frame = CGRectMake(40, -10, 270, 70);
                    
                }
            }
            else
            {
                if ([countrydata length]>0) {
                    
                    
                    address_lb.frame = CGRectMake(0,divider_2.frame.origin.y+divider_2.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50);
                    address_label.frame = CGRectMake(40, -10, 270, 70);
                    
                }
                else
                {
                    
                    
                    address_lb.frame = CGRectMake(0,divider_2.frame.origin.y+divider_2.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50);
                    address_label.frame = CGRectMake(40, -2, 270, 50);
                    
                }
                
            }
        }
        
        else
        {
            if ([zipdata length]>0 || [citydata length]>0) {
                
                if ([countrydata length]>0) {
                    
                    
                    address_lb.frame = CGRectMake(0,divider_2.frame.origin.y+divider_2.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50);
                    address_label.frame = CGRectMake(40, -10, 270, 70);
                    
                }
                else
                {
                    
                    
                    address_lb.frame = CGRectMake(0,divider_2.frame.origin.y+divider_2.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50);
                    address_label.frame = CGRectMake(40, -2, 270, 50);
                    
                }
            }
            else
            {
                if ([countrydata length]>0) {
                    
                    
                    address_lb.frame = CGRectMake(0,divider_2.frame.origin.y+divider_2.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50);
                    address_label.frame = CGRectMake(40, -2, 270, 50);
                    
                }
                else
                {
                    
                    
                    address_lb.frame = CGRectMake(0,divider_2.frame.origin.y+divider_2.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50);
                    address_label.frame = CGRectMake(40, 0, 270, 50);
                    
                }
                
            }
            
            
            
        }
        //        address_img.frame = CGRectMake(13, 15, 12, 15.0f);
        
        
        
        //[address_lb setTitle:address_str forState:UIControlStateNormal];
        divider_1.frame = CGRectMake(0, address_lb.frame.origin.y+address_lb.frame.size.height, [[UIScreen mainScreen] bounds].size.width, 0.5);
        
        [mainview addSubview:address_lb];
    }
    else
    {
        address_img.frame = CGRectMake(13, 15,[UIImage imageNamed:@"map_profile"].size.width/1.5,0);
        //        address_img.frame = CGRectMake(13, 15, 12, 15.0f);
        address_lb.frame = CGRectMake(0,divider_2.frame.origin.y+divider_2.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        divider_1.frame = CGRectMake(0, address_lb.frame.origin.y+address_lb.frame.size.height, [[UIScreen mainScreen] bounds].size.width, 0);
        
        [mainview addSubview:address_lb];
    }
    
    //    if (address_str != false && ![address_str isEqualToString:@"0"] && [address_str length] > 1 && ![address_str isEqualToString:@"(null)"]) {
    //
    //        [address_lb setHidden:NO];
    //        [address_img setHidden:NO];
    //        address_img.frame = CGRectMake(13, 15, 12, 15.0f);
    //        address_lb.frame = CGRectMake(0,divider_2.frame.origin.y+divider_2.frame.size.height, [UIScreen mainScreen].bounds.size.width, [UIImage imageNamed:@"detbuttonNormal"].size.height);
    //    }
    //    else{
    //
    //        address_img.frame = CGRectMake(13, 15, 12, 0);
    //        address_lb.frame = CGRectMake(0,divider_2.frame.origin.y+divider_2.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
    //    }
    
    //    UIImageView *bgcallsms = [[UIImageView alloc]initWithFrame:CGRectMake(120, 136.5f-61.5f, 91.3f, 30)];
    //    [mainview addSubview:bgcallsms];
    //    bgcallsms.userInteractionEnabled = YES;
    //    bgcallsms.image= [UIImage imageNamed:@"grn1.png"];
    //
    //    UIImageView *bgcallsms1 = [[UIImageView alloc]initWithFrame:CGRectMake(212.3f, 136.5f-61.5f, 91.3f, 30)];
    //    [mainview addSubview:bgcallsms1];
    //    bgcallsms1.userInteractionEnabled = YES;
    //    bgcallsms1.image= [UIImage imageNamed:@"grn2.png"];
    
    //    UIButton *phonebt = [UIButton buttonWithType:UIButtonTypeCustom];
    //    phonebt.frame= CGRectMake(154, 80, 20.5f, 20.5f);
    //    [phonebt setBackgroundImage:[UIImage imageNamed:@"Calln.png"] forState:UIControlStateNormal];
    //    phonebt.backgroundColor=[UIColor clearColor];
    //    [phonebt addTarget:self action:@selector(phonefunc) forControlEvents:UIControlEventTouchUpInside];
    //    [mainview addSubview:phonebt];
    //
    //    UIButton *chatbt = [UIButton buttonWithType:UIButtonTypeCustom];
    //    chatbt.frame= CGRectMake(247, 80.5f, 20.5f, 18);
    //    [chatbt setBackgroundImage:[UIImage imageNamed:@"Smsn.png"] forState:UIControlStateNormal];
    //    chatbt.backgroundColor=[UIColor clearColor];
    //    [chatbt addTarget:self action:@selector(chatfunc) forControlEvents:UIControlEventTouchUpInside];
    //    [mainview addSubview:chatbt];
    
    grayback = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 110.0f)];
    [grayback setBackgroundColor:[UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f]];
    [mainview addSubview:grayback];
    
    bgcallsms = [[UIButton alloc]initWithFrame:CGRectMake(120, 136.5f-75.5f, 91.3f, 35)];
    [grayback addSubview:bgcallsms];
    bgcallsms.userInteractionEnabled=YES;
    [bgcallsms setBackgroundImage:[UIImage imageNamed:@"call_profile"] forState:UIControlStateNormal];
    [bgcallsms setBackgroundImage:[UIImage imageNamed:@"callend_profile"] forState:UIControlStateSelected];
    [bgcallsms setBackgroundImage:[UIImage imageNamed:@"callend_profile"] forState:UIControlStateHighlighted];
    [bgcallsms addTarget:self action:@selector(phonefunc) forControlEvents:UIControlEventTouchUpInside];
    
    bgcallsms1 = [[UIButton alloc]initWithFrame:CGRectMake(212.3f, 136.5f-75.5f, 91.3f, 35)];
    [grayback addSubview:bgcallsms1];
    bgcallsms1.userInteractionEnabled=YES;
    [bgcallsms1 setBackgroundImage:[UIImage imageNamed:@"sms_profile"] forState:UIControlStateNormal];
    [bgcallsms1 setBackgroundImage:[UIImage imageNamed:@"smsend_profile"] forState:UIControlStateSelected];
    [bgcallsms1 setBackgroundImage:[UIImage imageNamed:@"smsend_profile"] forState:UIControlStateHighlighted];
    [bgcallsms1 addTarget:self action:@selector(chatfunc) forControlEvents:UIControlEventTouchUpInside];
    
    //    UITapGestureRecognizer *callTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(phonefunc)];
    //    [bgcallsms addGestureRecognizer:callTap];
    //    UITapGestureRecognizer *smsTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(chatfunc)];
    //    [bgcallsms1 addGestureRecognizer:smsTap];
    
    
    UILabel *prof_name = [[UILabel alloc]init]; //WithFrame:CGRectMake(112, 72, 196, 60)];
    [mainview addSubview:prof_name];
    prof_name.backgroundColor=[UIColor clearColor];
    prof_name.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
    prof_name.text=fullname;
    //    prof_name.font=[UIFont fontWithName:@"Mark Simonson - Proxima Nova Alt Condensed Bold" size:19];
    prof_name.font=[UIFont fontWithName:@"ProximaNova-Bold" size:20];
    prof_name.numberOfLines=0;
    prof_name.frame=CGRectMake(120, 75-70, 195, 60);
    
    int id_contact = [idnew intValue];
    
    image_prof = [self contactPicture:id_contact];
    
    UIImageView *prof_img = [[UIImageView alloc]initWithFrame:CGRectMake(15, 75-64, 90, 90)];
    //    prof_img.image = [NSString stringWithFormat:@"%@",[ContactDetails_Dict objectForKey:@"thumb"]];
    prof_img.image = [UIImage imageNamed:@"phonebokk1@2x.png"];//Nonfrienddp2.png
    prof_img.layer.borderWidth=0;
    prof_img.contentMode=UIViewContentModeScaleAspectFit;
    prof_img.clipsToBounds=YES;
    //    UIImage *image = [UIImage imageWithData:imageData];
    
    CGImageRef cgref = [image_prof CGImage];
    CIImage *cim = [image_prof CIImage];
    
    if (cim == nil && cgref == NULL)
    {
        // DebugLog(@"no underlying data %@",image_prof);
    }
    else
    {
        // DebugLog(@"image has; %@",image_prof);
        
        @try {
            prof_img.image = image_prof;
            image_prof_original = [self contactPictureOriginal:id_contact];
            
            prof_img.userInteractionEnabled=YES;
            
            UITapGestureRecognizer *propictap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(detailpic:)];
            
            [prof_img addGestureRecognizer:propictap];
        }
        @catch (NSException *exception) {
            // DebugLog(@"exception is: %@",exception.description);
        }
    }
    [mainview addSubview:prof_img];
    
    phonesarr1 =[[NSMutableArray alloc]init];
    NSString *mobile_strd, *business_strd, *landphnstrd;
    mobile_strd = [NSString stringWithFormat:@"%@",[deviceContactsPhoneDetails objectForKey:@"mobile"]];
    //DebugLog(@"deviceContactsPhoneDetails mobile %@",mobile_strd);
    business_strd = [NSString stringWithFormat:@"%@",[deviceContactsPhoneDetails objectForKey:@"work"]];
    landphnstrd = [NSString stringWithFormat:@"%@",[deviceContactsPhoneDetails objectForKey:@"home"]];
    
    if ([mobile_strd length] > 3 && ![mobile_strd isKindOfClass:[NSNull class]] && mobile_strd != (id)[NSNull null] && ![mobile_strd isEqualToString:@"(null)"])
    {
        [phonesarr1 addObject:mobile_strd];
    }
    else if ([landphnstrd length] > 3 && ![landphnstrd isKindOfClass:[NSNull class]] && landphnstrd != (id)[NSNull null] && ![landphnstrd isEqualToString:@"(null)"])
    {
        [phonesarr1 addObject:landphnstrd];
    }
    else if ([business_strd length] > 3 && ![business_strd isKindOfClass:[NSNull class]] && business_strd != (id)[NSNull null] && ![business_strd isEqualToString:@"(null)"])
    {
        [phonesarr1 addObject:business_strd];
    }
    //DebugLog(@"phonesarr1: %@",phonesarr1);
    [activity stopAnimating];
    
}

-(void)viewWillAppear:(BOOL)animated{
    //    activity = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(135, 190, 40, 40)];
    //    activity.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    //    [activity setColor:[UIColor blackColor]];
    //    activity.layer.zPosition=6;
    //    [self.view addSubview:activity];
    //    [activity startAnimating];
    self.tabBarController.tabBar.frame = CGRectMake(0, [[UIScreen mainScreen]bounds].size.height, [UIScreen mainScreen].bounds.size.width,44);
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getData:) name:@"Data" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(navtopage:) name:@"pagenav" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestreceivedaction) name:@"Requestreceived_push" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(acceptedrequestaction) name:@"Accepted_push" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AcceptedRequest) name:@"Accepted_request" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AddFriend) name:@"NewConnection" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ConnectionType) name:@"NewConnectionType" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AcceptedRequest) name:@"TypeChange" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shareLocation) name:@"ShareLocation" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdateInformation) name:@"UpdateInfo" object:nil];
    
}

-(void)UpdateInformation
{
    ConPersonalProfileViewController *PVC = [[ConPersonalProfileViewController alloc]init];
    PVC.toUpdateInfo = @"YES";
    [self.navigationController pushViewController:PVC animated:YES];
   // [[NSNotificationCenter defaultCenter] postNotificationName:@"Update Info" object:Nil];
    
}

-(void)shareLocation
{
    ConLocateGroupViewController *conLocate = [[ConLocateGroupViewController alloc]init];
    conLocate.group_id=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"sharedLocGrpId"]];
    [self.navigationController pushViewController:conLocate animated:YES];
}

-(void)ConnectionType
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDel.newrequestRedirect)
    {
  
    ConNewRequestsViewController *mng = [[ConNewRequestsViewController alloc]init];
       mng.userid = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userforrequest"] intValue];
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
    
        appDel.newrequestRedirect = NO;
   // [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:mng animated:YES];
        
        
    }
}


-(void)AddFriend
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDel.newUser)
    {
    
    ConAddFriendViewController *con = [[ConAddFriendViewController alloc]init];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
        
        appDel.newUser = NO;
    
   // [[self navigationController].view.layer addAnimation:transition forKey:nil];
    [[self navigationController] pushViewController:con animated:YES];
        
    }
    
}

-(void)AcceptedRequest
{
    
    
    if (del.profileRedirect)
    {
    
    ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
    // con.other=@"yes";
    con.request = YES;
    [self.tabBarController.tabBar setHidden:YES];
    // con.uid=[NSString stringWithFormat:@"%d",[userid intValue]];
    //                [self.navigationController presentViewController:con animated:NO completion:nil];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
        
    del.profileRedirect = NO;
    
   // [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
        
    }
    
}

-(void)requestreceivedaction
{
    //    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
    con.other=@"yes";
    con.uid=[NSString stringWithFormat:@"%@",del.pushtoprofileid];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}

-(void)acceptedrequestaction
{
    ConRequestViewController *con = [[ConRequestViewController alloc]init];
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}


-(void)phonefunc
{
    if ([phonesarr1 count] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Number found for this user!"
                                                        message:nil
                                                       delegate:self
                                              cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
    }
    else
    {
        UIDevice *device = [UIDevice currentDevice];
        if ([[device model] isEqualToString:@"iPhone"] ) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString: [NSString stringWithFormat:@"tel:%@",[phonesarr1 objectAtIndex:0]]]];
        } else {
            UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [notPermitted show];
        }
    }
}

-(void)chatfunc
{
    //===================Original code=================
    
    //    if ([phonesarr1 count] == 0)
    //    {
    //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Number found for this user!"
    //                                                        message:nil
    //                                                       delegate:self
    //                                              cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
    //        [alert show];
    //    }
    //    else
    //    {
    //        UIDevice *device = [UIDevice currentDevice];
    //        if ([[device model] isEqualToString:@"iPhone"]) {
    //
    //            MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
    //            if([MFMessageComposeViewController canSendText])
    //            {
    //                [self presentViewController:controller animated:YES completion:Nil];
    //                controller.body = @"";
    //                NSString *smsstring= [NSString stringWithFormat:@"%@",[phonesarr1 objectAtIndex:0]];
    //                controller.recipients = [NSArray arrayWithObjects:smsstring, nil];
    //                controller.messageComposeDelegate = self;
    //                //[self presentModalViewController:controller animated:YES];
    //            }
    //        }
    //        else
    //        {
    //            UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //            [notPermitted show];
    //        }
    //    }
    //    DebugLog(@"phonesarr1: %@",phonesarr1);
    
    //=======================================================
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select a option"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"WhatsApp", @"SMS", nil];
    
    //            [actionSheet showInView:self.view];
    actionSheet.tag = 900;
    [actionSheet showInView:mainview];
}

-(void)navtopage: (NSNotification *)notification
{
    //DebugLog(@"navtopage");
    NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
    if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"4"])
    {
        ConAccountSettingsViewController *con = [[ConAccountSettingsViewController alloc]init];
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"6"])
    {
        ConInviteViewController *con = [[ConInviteViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"5"])
    {
        ConSyncLoaderViewController *con = [[ConSyncLoaderViewController alloc]init];
        //        [self.navigationController pushViewController:con animated:NO];
        
        
        [self presentViewController:con animated:NO completion:nil];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"2"])
    {
        ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"3"])
    {
        ConQROptionsViewController *con = [[ConQROptionsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    
    [mainview setFrame:CGRectMake(0, mainview.frame.origin.y, mainview.frame.size.width, mainview.frame.size.height)];
    //    [prefs setObject:@"111" forKey:@"whichpage"];
}


-(void)getData:(NSNotification *)notification {
    
    if(move == 0) {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake(280, (mainview.frame.origin.y), mainview.frame.size.width, mainview.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             move=1;
                         }];
    } else {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake(0, (mainview.frame.origin.y), mainview.frame.size.width, mainview.frame.size.height)];
                             
                         }
                         completion:^(BOOL finished){
                             move=0;
                         }];
    }
}

-(void)viewDidDisappear:(BOOL)animated
{
    move=0;
    [mainview removeFromSuperview];
    del.tabBarController.tabBar.frame = CGRectMake(0, [[UIScreen mainScreen]bounds].size.height-44, [UIScreen mainScreen].bounds.size.width, 44);
    [[UITabBar appearance] setTintColor:[UIColor colorWithRed:192.0f/255.0f green:250.0f/255.0f blue:193.0f/255.0f alpha:1]];
    
    
    //=================FOR TAB BAR FONT HIGHLIGHTED=============//
    
    [[UITabBarItem appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor colorWithRed:192.0f/255.0f green:250.0f/255.0f blue:193.0f/255.0f alpha:1], NSForegroundColorAttributeName,
      [UIFont fontWithName:@"ProximaNova-Regular" size:13.0], NSFontAttributeName,
      nil] forState:UIControlStateHighlighted];
    
    //================FOR TAB BAR FONT SELECTED=================//
    
    [[UITabBarItem appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor colorWithRed:192.0f/255.0f green:250.0f/255.0f blue:193.0f/255.0f alpha:1], NSForegroundColorAttributeName,
      [UIFont fontWithName:@"ProximaNova-Regular" size:13.0], NSFontAttributeName,
      nil] forState:UIControlStateSelected];
    
    //===============FOR TAB BAR FONT NORMAL===================//
    //[[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    
    [[UITabBarItem appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor whiteColor], NSForegroundColorAttributeName ,
      [UIFont fontWithName:@"ProximaNova-Regular" size:13.0], NSFontAttributeName,
      nil] forState:UIControlStateNormal];  //colorWithRed:.5 green:.5 blue:.5 alpha:1
    
    
    [super viewDidDisappear:YES];
}

-(void)mobilenotap:(UITapGestureRecognizer *)sender{
    
    UIActionSheet *mobileAction = [[UIActionSheet alloc]initWithTitle:Nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles:@"Call",@"SMS", nil];
    mobileAction.tag = 100;
    [mobileAction showInView:mainview];
    
    
}
-(void)homenotap:(UITapGestureRecognizer *)sender{
    
    UIDevice *device = [UIDevice currentDevice];
    if ([[device model] isEqualToString:@"iPhone"] ) {
        NSString *phoneNumber = [@"telprompt://" stringByAppendingString:[NSString stringWithFormat:@"%@",homeno.titleLabel]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
        
        //                [[UIApplication sharedApplication] openURL:[NSURL URLWithString: [NSString stringWithFormat:@"tel:%@",[phonesarr objectAtIndex:buttonIndex]]]];
    } else {
        UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [notPermitted show];
    }
}

-(void)worknotap:(UITapGestureRecognizer *)sender{
    
    UIActionSheet *workAction = [[UIActionSheet alloc]initWithTitle:Nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles:@"Call",@"SMS", nil];
    workAction.tag = 102;
    [workAction showInView:mainview];
}

-(void)emailtap:(UITapGestureRecognizer *)sender{
    
    mailComposer = [[MFMailComposeViewController alloc]init];
    mailComposer.mailComposeDelegate = self;
    NSArray *recipentsArray = [NSArray arrayWithObject:mail_lb.titleLabel];
    [mailComposer setToRecipients:recipentsArray];
    [mailComposer setMessageBody:@"" isHTML:NO];
    //    [self presentModalViewController:mailComposer animated:YES];
    [self.navigationController presentViewController:mailComposer animated:YES completion:nil];
}

#pragma mark - mail compose delegate
-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (result) {
        // DebugLog(@"Result : %d",result);
    }
    if (error) {
        // DebugLog(@"Error : %@",error);
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 100) {
        
        if (buttonIndex == 0) {
            UIDevice *device = [UIDevice currentDevice];
            if ([[device model] isEqualToString:@"iPhone"] ) {
                NSString *phoneNumber = [@"telprompt://" stringByAppendingString:[NSString stringWithFormat:@"%@",mobileno.titleLabel]];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
                
                //                [[UIApplication sharedApplication] openURL:[NSURL URLWithString: [NSString stringWithFormat:@"tel:%@",[phonesarr objectAtIndex:buttonIndex]]]];
            } else {
                UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [notPermitted show];
            }
            
        }
        else if (buttonIndex == 1){
            
            UIDevice *device = [UIDevice currentDevice];
            if ([[device model] isEqualToString:@"iPhone"] ) {
                
                MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
                
                if([MFMessageComposeViewController canSendText]) {
                    [self.navigationController presentViewController:controller animated:YES completion:nil];
                    
                    controller.body = @"";
                    controller.recipients = [mobilearray objectAtIndex:0];
                    controller.messageComposeDelegate = self;
                }
            }
            else
            {
                UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [notPermitted show];
            }
        }}
    else if (actionSheet.tag == 102){
        
        if (buttonIndex == 0) {
            UIDevice *device = [UIDevice currentDevice];
            if ([[device model] isEqualToString:@"iPhone"] ) {
                NSString *phoneNumber = [@"telprompt://" stringByAppendingString:[NSString stringWithFormat:@"%@",workno.titleLabel]];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
                
                //                [[UIApplication sharedApplication] openURL:[NSURL URLWithString: [NSString stringWithFormat:@"tel:%@",[phonesarr objectAtIndex:buttonIndex]]]];
            } else {
                UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [notPermitted show];
            }
        }
        else if (buttonIndex == 1){
            UIDevice *device = [UIDevice currentDevice];
            if ([[device model] isEqualToString:@"iPhone"] ) {
                
                MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
                
                if([MFMessageComposeViewController canSendText]) {
                    [self.navigationController presentViewController:controller animated:YES completion:nil];
                    controller.body = @"";
                    controller.recipients = [workarray objectAtIndex:0];
                    controller.messageComposeDelegate = self;
                }
            }
            else
            {
                UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [notPermitted show];
            }
        }
    }else if (actionSheet.tag == 900){
        
        if (buttonIndex == 0) {
            
            // DebugLog(@"WhatsApp button clicked");
            //            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.whatsapp.com"]]];
            //            NSString * msg = @"YOUR MSG";
            
            DebugLog(@"here it is: %@",idnew);
            NSString * urlWhats = [NSString stringWithFormat:@"whatsapp://send?abid=%@&text=Hello World!",idnew];
            NSURL * whatsappURL = [NSURL URLWithString:[urlWhats stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
                [[UIApplication sharedApplication] openURL: whatsappURL];
            } else {
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"WhatsApp not installed." message:@"Your device has no WhatsApp installed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
            
        }else if (buttonIndex == 1){
            
            //            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.skype.com"]]];
            //
            //        }else if (buttonIndex == 2){
            
            // DebugLog(@"sms phonesarr: %@",phonesarr1);
            if ([phonesarr1 count] == 0)
            {
                [bgcallsms1 setBackgroundImage:[UIImage imageNamed:@"sms_profile"] forState:UIControlStateNormal];
                
                
                smsAlert1 = [[UIAlertView alloc] initWithTitle:@"No Number found for this user!"
                                                       message:nil
                                                      delegate:self
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                smsAlert1.tag = 6;
                [smsAlert1 show];
            }
            else
            {
                [bgcallsms1 setBackgroundImage:[UIImage imageNamed:@"sms_profile"] forState:UIControlStateNormal];
                
                UIDevice *device = [UIDevice currentDevice];
                if ([[device model] isEqualToString:@"iPhone"]) {
                    
                    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
                    if([MFMessageComposeViewController canSendText])
                    {
                        [self presentViewController:controller animated:YES completion:Nil];
                        controller.body = @"";
                        NSString *smsstring= [NSString stringWithFormat:@"%@",[phonesarr1 objectAtIndex:0]];
                        controller.recipients = [NSArray arrayWithObjects:smsstring, nil];
                        controller.messageComposeDelegate = self;
                        //[self presentModalViewController:controller animated:YES];
                    }
                }
                else
                {
                    smsAlert2=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    smsAlert2.tag = 66;
                    [smsAlert2 show];
                }
            }
            
        }
        
    }
    
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    if (result == MessageComposeResultCancelled) {
        // DebugLog(@"Message cancelled");
        
    } else if (result == MessageComposeResultSent) {
        // DebugLog(@"Message sent");
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

+(void)chngpostion
{
    //DebugLog(@"Change Position profile page");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Data" object:Nil];
}

- (UIImage *) contactPicture:(int)contactId {
    // Get contact from Address Book
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
    ABRecordID recordId = (ABRecordID)contactId;
    ABRecordRef person = ABAddressBookGetPersonWithRecordID(addressBook, recordId);
    
    // Check for contact picture
    if (person != nil && ABPersonHasImageData(person)) {
        if ( &ABPersonCopyImageDataWithFormat != nil ) {
            return [UIImage imageWithData:(__bridge NSData *)ABPersonCopyImageDataWithFormat(person, kABPersonImageFormatThumbnail)];
        } else {
            return [UIImage imageWithData:(__bridge NSData *)ABPersonCopyImageData(person)];
        }
    } else {
        return nil;
    }
}

- (UIImage *) contactPictureOriginal:(int)contactId {
    // Get contact from Address Book
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
    ABRecordID recordId = (ABRecordID)contactId;
    ABRecordRef person = ABAddressBookGetPersonWithRecordID(addressBook, recordId);
    
    // Check for contact picture
    if (person != nil && ABPersonHasImageData(person)) {
        if ( &ABPersonCopyImageDataWithFormat != nil ) {
            // iOS >= 4.1
            return [UIImage imageWithData:(__bridge NSData *)ABPersonCopyImageDataWithFormat(person, kABPersonImageFormatOriginalSize)];
        } else {
            // iOS < 4.1
            return [UIImage imageWithData:(__bridge NSData *)ABPersonCopyImageData(person)];
        }
    } else {
        return nil;
    }
}

-(void)detailpic: (UIGestureRecognizer *)sender
{
    ConPictureProfViewController *con = [[ConPictureProfViewController alloc]init];
    con.profilepic = image_prof_original;
    //   con.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:con animated:NO completion:nil];
}


-(void)closedetailpic: (UIGestureRecognizer *)sender
{
    CATransition *animation = [CATransition animation];
    [animation setType:kCATransitionFade];
    [animation setSubtype:kCATransitionFade];
    animation.duration = 0.7f;
    [detailimg.layer addAnimation:animation forKey:nil];
    if(detailimg.hidden==NO)
    {
        detailimg.hidden=YES;
    }
    [tapbgview removeFromSuperview];
}

-(void)gobackoption//: (id)sender
{
    DebugLog(@"QWETYIOPASDFGHJ");
    
    
    CATransition *transition = [CATransition animation];
    
    transition.duration = 0.4f;
    
    transition.type = kCATransitionFade;
    
    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController popViewControllerAnimated:YES];
    //[self dismissViewControllerAnimated:YES completion:nil];
}
//-(void)goPlus:(id)sender
//{
//    DebugLog(@"Plus clicked");
//    ConAddFriendViewController *con = [[ConAddFriendViewController alloc]init];
//
//    self.navigationController.delegate = self;
//    //      [self presentViewController:con animated:NO completion:nil];
//    [self.navigationController pushViewController:con animated:NO];
//
//    //    [self dismissViewControllerAnimated:NO completion:nil];
//    //    [self.navigationController pushViewController:con animated:NO];
//}

@end