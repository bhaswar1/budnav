#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "SMCalloutView.h"
#import "ConParentTopBarViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <Superpin/Superpin.h>
#import "REVClusterMapView.h"
#import "SMCalloutView.h"

//
// This controller demonstrates how to use SMCalloutView with MKMapView. It is a bit more complex
// than using SMCalloutView with a simple UIScrollView. We need to subclass MKMapView in order
// to provide all our features such as allowing touches on our callout.
//

@class CustomMapView;

@interface MapKitComparisonController : UIViewController <MKMapViewDelegate, SMCalloutViewDelegate,UIScrollViewDelegate,UIWebViewDelegate,UIGestureRecognizerDelegate,CLLocationManagerDelegate>
{
    NSDictionary *json1;
    UIAlertView *alert;
    NSMutableArray *requestslist;
    UITableView *tab_request;
    //    UIView *mainview;
    
    NSMutableArray *newarr;
    NSMutableArray *newarr3;
    NSMutableArray *newarr1;
    NSMutableArray *newarr2;
    NSMutableArray *newarr4;
    int taxic,eventc,tradec,listc,typec;
    
    REVClusterMapView *_mapView;
}
+(void)chngpostion;

@property(nonatomic,retain) NSString *getmapvalue;
@property (nonatomic, strong) UIView *mainview;

@property (nonatomic, strong) UISegmentedControl *segmentedControl;
@property (nonatomic, strong) CustomMapView *mapKitWithSMCalloutView;
@property (nonatomic, strong) MKMapView *mapKitWithUICalloutView;
@property (nonatomic, strong) SMCalloutView *calloutView;
@property (nonatomic, strong) MKPointAnnotation *annotationForSMCalloutView, *annotationForUICalloutView;

@end
