//  ConProfileOwnViewController.m
//  Contacter
//  Created by Bhaswar's MacBook Air on 07/05/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
#import "ConProfileOwnViewController.h"
#import "AppDelegate.h"
#import "SocialWebViewController.h"
#import "UIImageView+WebCache.h"
#import "ConAccountSettingsViewController.h"
#import "ConInviteViewController.h"
#import "ConAddFriendViewController.h"
#import "iToast.h"
#import <GoogleMaps/GoogleMaps.h>
#import "ConNavigateViewController.h"
#import "ConAddBackUpViewController.h"
#import "SVProgressHUD.h"
#import <AddressBook/AddressBook.h>
#import "DBManager.h"
#import "ConEditProfileViewController.h"
#import "ConPictureProfViewController.h"
#import "ConSyncLoaderViewController.h"
#import "ConRequestViewController.h"
#import "ConNewRequestsViewController.h"
#import "UIImage+animatedGIF.h"
#import "ConQROptionsViewController.h"
#import "QuartzCore/QuartzCore.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"


@interface ConProfileOwnViewController () <UIAlertViewDelegate>
{
    NSDictionary *json1;
    UIAlertView *alert;
    UIButton *removeconbt, *togglebt;
    int switchprof;
    UILabel *personaltext, *businesstext, *website_lb, *companylb, *desiglb, *street_number_lbl, *city_lbl, *zipcode_lbl, *country_lbl, *backlbl;
    
    UIButton *mobileno, *landphonelb, *mail_lb, *address_lb;
    
    UIImageView *toggleimage, *address_img, *landphone_img, *mobileimg, *detailimg,*website_img, *mailimg, *companyimg, *desigimg,*moreImage,*businessImg,*removeImg, *bck_img, *bck_img1;
    NSString *mobile,*landline,*mail,*website,*address, *businessAddress,*base64String, *googleMapUrlString, *appleMapUrlString, *desig, *company;
    UIImage *profilepic;
    int move;
    double latitude,longitude;
    UIView *mapview,*tapbgview;
    UIButton *crossbt,*navigatebt,*navigatebtapple;
    UIActivityIndicatorView *act;
    NSArray *uniquearray;
    NSMutableArray *con_array, *app_contacts, *check_app_cont;
    UIScrollView *boxscroll, *b_boxscroll;
    UIButton *twitterbt, *pinterestbt, *facebookbt, *gplusbt, *youtubebt, *instagrambt, *linkedinbt, *skypebt, *soundcloudbt, *vimeobt;
    int counter, profile_insertion, profile_fetch;
    UIView *divider_1, *divider_2, *divider_3, *divider_0, *divider_4, *divider_5;
    NSMutableArray *mobileArray;
    NSMutableData *responseData;
    NSString *user_id;
    UILabel *prof_name;
    UIButton *backBtn;
    UITapGestureRecognizer *tap;
    UIView *coverview1;
    NSTimer *timea, *timeb;
    int allowpersonal,nobusiness;
    UIView *grayback,*totalview;
    UIView *coverView, *business_view, *personal_view;
    BOOL rightmenuOpen;
    int y,y1;
    UIView *blackView;
    UILabel *lineDivMenu, *labelHead;
    UIView *divider_9, *divider_10;
    UIButton *bgcallsms, *bgcallsms1;
    UIAlertView *callAlert1, *callAlert2, *smsAlert1, *smsAlert2;
    UIButton *rightMenuBack, *rightMenu;
    UIView *overlayMenu;
    UITapGestureRecognizer *overlayTap;
    UIView *mobileback, *landlineback, *mailback, *addressback;
    AppDelegate *del;
    NSUserDefaults *prefs;
    NSString *street,*city,*zipcode,*country, *number;
    UIView *change;
    NSString *fullname,*numberstring;
    NSString *business_street,*business_city,*business_zipcode,*business_country, *business_number;
    CAGradientLayer *gradient;
    UIImageView *logo_img;
    
}
@end

@implementation ConProfileOwnViewController
@synthesize other,uid,mainview,dict_profile,personal_array, business_array, social_array, phonearr,profdict, mapReturn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)hidesBottomBarWhenPushed {
    return YES;
}

- (void)viewDidLoad
{
    //    self.navigationController.navigationBarHidden=YES;
//    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"searchafterreg"] isEqualToString:@"yes"])
//    {
//        ConAddFriendViewController *con = [[ConAddFriendViewController alloc]init];
//        [self.navigationController pushViewController:con animated:YES];
// }
    
    switchprof=0;
    [super viewDidLoad];
    
       NSLog(@"From ConProfileOwnViewController");
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"error"];
    self.tabBarController.tabBar.hidden = YES;
    // Do any additional setup after loading the view.
    
    prefs = [NSUserDefaults standardUserDefaults];
    DebugLog(@"user id %@",[prefs objectForKey:@"userid"]);
    
    self.view.backgroundColor=[UIColor whiteColor];
    //    self.tabBarController.tabBar.frame = CGRectMake(0, 0, 0, 0);
    //    self.tabBarController.tabBar.hidden = YES;
    
    //    del.tabBarController.tabBar.frame = CGRectMake(0, [[UIScreen mainScreen]bounds].size.height, 320,54);
    self.tabBarController.tabBar.translucent = YES;
    
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //    [del showTabStatus:NO];
    //    del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //    [del showTabStatus:YES];
    //    [del showTabValues:YES];
    //    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    
    self.navigationController.navigationBarHidden=YES;
    
    
    coverView = [[UIView alloc]init];
    coverView.frame = CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width,64);
    //coverView.backgroundColor = [UIColor blackColor];
    coverView.userInteractionEnabled=YES;
    [self.view addSubview:coverView];
    
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, coverView.frame.size.height);
    
    // Add the gradient to the view
    [coverView.layer insertSublayer:gradient atIndex:0];
    
    
     logo_img = [[UIImageView alloc]init];
     logo_img.frame = CGRectMake(27+([UIScreen mainScreen].bounds.size.width-160)/2, 32, 101, 20);
    logo_img.backgroundColor = [UIColor clearColor];
    
    logo_img.image = [UIImage imageNamed:@"new_logo"];
    [coverView addSubview:logo_img];
    
    
    if ([other isEqualToString:@"yes"]) //=========OTHER'S PROFILE=======//
    {
        user_id =uid;
        
    }
    else
    {
        user_id= [prefs objectForKey:@"userid"];
        
    }
    
    if ([user_id intValue] == [[prefs objectForKey:@"userid"] intValue])
    {
        DebugLog(@"IFFFFFF=============>");
        self.tabBarController.tabBar.frame = CGRectMake(0, [[UIScreen mainScreen]bounds].size.height-44, [UIScreen mainScreen].bounds.size.width, 44);
        self.tabBarController.tabBar.hidden = NO;
        //        [del showTabStatus:NO];
        
        change = [[UIView alloc]initWithFrame:CGRectMake(8,32,115,30)];
        [change setBackgroundColor:[UIColor clearColor]];
        [coverView addSubview:change];
        
        
        bck_img=[[UIImageView alloc]initWithFrame:CGRectMake(7, 0, 12, 20)];//(9,33,10,20)];//(13, 39, 10, 22)];
        bck_img.image=[UIImage imageNamed:@"back3"];
        [change addSubview:bck_img];
        
        backlbl=[[UILabel alloc]initWithFrame:CGRectMake(bck_img.frame.origin.x+bck_img.frame.size.width+5, 3, 50, 20)];//(bck_img.frame.origin.x+bck_img.frame.size.width+5, 28, 54, 30)];
        backlbl.text=@"Back";
        backlbl.textColor=[UIColor whiteColor];
        //        backlbl.font=[UIFont fontWithName:@"ProximaNova-Regular" size:20];
        backlbl.backgroundColor=[UIColor clearColor];
        //[change addSubview:backlbl];
        backlbl.userInteractionEnabled=YES;
        
        
        backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        backBtn.frame = change.frame;//CGRectMake(bck_img.frame.origin.x, bck_img.frame.origin.y, backlbl.frame.origin.x+backlbl.frame.size.width-bck_img.frame.origin.x, backlbl.frame.size.height);
        backBtn.backgroundColor=[UIColor clearColor];
        //        backBtn.tag=p;
        [backBtn setTitle:@"" forState:UIControlStateNormal];
        //            [backBtn setBackgroundImage:[UIImage imageNamed:@"backnew_profileSelected"] forState:UIControlStateSelected];
        //            [backBtn setBackgroundImage:[UIImage imageNamed:@"backnew_profileSelected"] forState:UIControlStateHighlighted];
        
        [backBtn addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDown];
        [backBtn addTarget:self action:@selector(changecoloragain) forControlEvents:UIControlEventTouchDragExit];
        [backBtn addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDragEnter];
        
        
        [backBtn addTarget:self action:@selector(gobackoption) forControlEvents:UIControlEventTouchUpInside];
        [coverView addSubview:backBtn];
        
        //        UILabel *contact_label = [[UILabel alloc] initWithFrame:CGRectMake(backBtn.frame.origin.x+backBtn.frame.size.width, backBtn.frame.origin.y, 100, backBtn.frame.size.height)];
        //        contact_label.backgroundColor = [UIColor clearColor];
        //        contact_label.text = @"All Contacts";
        //        contact_label.textColor = [UIColor whiteColor];
        //        contact_label.textAlignment = NSTextAlignmentCenter;
        //        [coverView addSubview:contact_label];
        
        
        
        
        
    }else{
        
        DebugLog(@"ELSEEEE=============>");
        
        [self.tabBarController.tabBar setHidden:YES]; //===========OTHER PROFILE REMOVE FOOTER BUTTON BAR==========//
        
        
        
        //===============Back button===========//
        
        if ([mapReturn isEqualToString:@"mapBack"]) {
            
            
            bck_img1=[[UIImageView alloc]initWithFrame:CGRectMake(15, 32, 12, 20)];//(9,33,10,20)];//(13, 39, 10, 22)];
            bck_img1.image=[UIImage imageNamed:@"back3"];
            [coverView addSubview:bck_img1];
            
            labelHead = [[UILabel alloc]initWithFrame:CGRectMake(32, 31, 50, 20)];
            labelHead.text = @"Map";
            labelHead.backgroundColor = [UIColor clearColor];
            labelHead.textColor = [UIColor whiteColor];
            //[coverView addSubview:labelHead];
            
            UIButton *backBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
            backBtn1.frame = CGRectMake(0, 27, 60, 37);
            backBtn1.backgroundColor=[UIColor clearColor];
            //            [backBtn1 setTitle:@"BACK" forState:UIControlStateNormal];
            //            [backBtn1 setBackgroundImage:[UIImage imageNamed:@"backnew_profileUnselected"] forState:UIControlStateNormal];
            //            [backBtn1 setBackgroundImage:[UIImage imageNamed:@"backnew_profileSelected"] forState:UIControlStateSelected];
            //            [backBtn1 setBackgroundImage:[UIImage imageNamed:@"backnew_profileSelected"] forState:UIControlStateHighlighted];
            
            [backBtn1 addTarget:self action:@selector(changecolor1) forControlEvents:UIControlEventTouchDown];
            [backBtn1 addTarget:self action:@selector(changecoloragain1) forControlEvents:UIControlEventTouchDragExit];
            [backBtn1 addTarget:self action:@selector(changecolor1) forControlEvents:UIControlEventTouchDragEnter];
            
            [backBtn1 addTarget:self action:@selector(gobackoption) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:backBtn1];
            
            
            
//            overlayMenu = [[UIView alloc]init];
//            overlayMenu.backgroundColor = [UIColor clearColor];
//            overlayMenu.frame = CGRectMake(10, 10, 90, 50);
//            overlayMenu.userInteractionEnabled = YES;
//            [coverView addSubview:overlayMenu];
            
//            UIButton *helpbt = [UIButton buttonWithType:UIButtonTypeCustom];
//            helpbt.frame = CGRectMake(10, 10, 90, 50);//(10, 35, 8, 17);
//            helpbt.backgroundColor=[UIColor clearColor];
//            [helpbt setTitle:nil forState:UIControlStateNormal];  //Back button.png
            
//            [helpbt addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDown];
//            [helpbt addTarget:self action:@selector(changecoloragain) forControlEvents:UIControlEventTouchDragExit];
//            [helpbt addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDragEnter];
            
//            [helpbt addTarget:self action:@selector(gobackoption) forControlEvents:UIControlEventTouchUpInside];
//            
//            [coverView addSubview:helpbt];
//            overlayTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gobackoption)];
//            [overlayMenu addGestureRecognizer:overlayTap];
            
            
        }else{
            change = [[UIView alloc]initWithFrame:CGRectMake(8,32,115,30)];
            [change setBackgroundColor:[UIColor clearColor]];
            [coverView addSubview:change];
            
            bck_img=[[UIImageView alloc]initWithFrame:CGRectMake(7, 0, 12, 20)];//(9,33,10,20)];//(13, 39, 10, 22)];
            bck_img.image=[UIImage imageNamed:@"back3"];
            [change addSubview:bck_img];
            
            backlbl=[[UILabel alloc]initWithFrame:CGRectMake(bck_img.frame.origin.x+bck_img.frame.size.width+5, 3, 50, 20)];
            backlbl.text=@"Back";
            backlbl.textColor=[UIColor whiteColor];
            backlbl.contentMode=UIViewContentModeScaleAspectFit;
            //    backlbl.font=[UIFont systemFontOfSize:17];//fontWithName:@"ProximaNova-Regular" size:17];
            backlbl.backgroundColor=[UIColor clearColor];
            //[change addSubview:backlbl];
            
            backBtn=[UIButton buttonWithType:UIButtonTypeCustom];//WithFrame:CGRectMake(bck_img.frame.origin.x, backlbl.frame.origin.y, backlbl.frame.origin.x+backlbl.frame.size.width-bck_img.frame.origin.x, backlbl.frame.size.height)];
            backBtn.frame=change.frame;
            [backBtn setTitle:@"" forState:UIControlStateNormal];
            backBtn.backgroundColor=[UIColor clearColor];
            [backBtn setTitle:@"" forState:UIControlStateNormal];
            //            [backBtn setBackgroundImage:[UIImage imageNamed:@"backnew_profileSelected"] forState:UIControlStateSelected];
            //            [backBtn setBackgroundImage:[UIImage imageNamed:@"backnew_profileSelected"] forState:UIControlStateHighlighted];
            [backBtn addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDown];
            [backBtn addTarget:self action:@selector(changecoloragain) forControlEvents:UIControlEventTouchDragExit];
            [backBtn addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDragEnter];
            [backBtn addTarget:self action:@selector(gobackoption) forControlEvents:UIControlEventTouchUpInside];
            [coverView addSubview:backBtn];
            
            
            //            backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            //            backBtn.frame = CGRectMake(bck_img.frame.origin.x, 28, [UIImage imageNamed:@"backnew_profileUnselected"].size.width, [UIImage imageNamed:@"backnew_profileUnselected"].size.height);
            //            backBtn.backgroundColor=[UIColor clearColor];
            //            //            [backBtn setBackgroundImage:[UIImage imageNamed:@"backnew_profileUnselected"] forState:UIControlStateNormal];
            //            //            [backBtn setBackgroundImage:[UIImage imageNamed:@"backnew_profileSelected"] forState:UIControlStateSelected];
            //            //            [backBtn setBackgroundImage:[UIImage imageNamed:@"backnew_profileSelected"] forState:UIControlStateHighlighted];
            //            [backBtn addTarget:self action:@selector(gobackoption) forControlEvents:UIControlEventTouchUpInside];
            //            backBtn.userInteractionEnabled=YES;
            //            [coverView addSubview:backBtn];
            
            //            UILabel *contact_label = [[UILabel alloc] initWithFrame:CGRectMake(backBtn.frame.origin.x+backBtn.frame.size.width, backBtn.frame.origin.y, 100, backBtn.frame.size.height)];
            //            contact_label.backgroundColor = [UIColor clearColor];
            //            contact_label.text = @"All Contacts";
            //            contact_label.textColor = [UIColor whiteColor];
            //            contact_label.textAlignment = NSTextAlignmentCenter;
            //            [coverView addSubview:contact_label];
            
            
        }
    }
    
    //======Selected menu button background color=======//
    
    //moreImage=[[UIImageView alloc]initWithFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-[UIImage imageNamed:@"menu-icon@2x.png"].size.width-10, 22, [UIImage imageNamed:@"menu-icon@2x.png"].size.width,[UIImage imageNamed:@"menu-icon@2x.png"].size.height)];
    moreImage=[[UIImageView alloc]initWithFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-37, 32, 22,19)];
    [moreImage setImage:[UIImage imageNamed:@"new_menu"]];
    [coverView addSubview:moreImage];
    
    rightMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    rightMenu.enabled=NO;
    [rightMenu setFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-55,[[UIApplication sharedApplication]statusBarFrame].size.height, 55,coverView.frame.size.height-[[UIApplication sharedApplication]statusBarFrame].size.height)];
    [rightMenu setBackgroundImage:nil forState:UIControlStateNormal];
    [rightMenu setBackgroundImage:nil forState:UIControlStateSelected];
    [rightMenu setBackgroundImage:nil forState:UIControlStateHighlighted];
    [rightMenu setBackgroundColor:[UIColor clearColor]];
    [rightMenu addTarget:self action:@selector(rightMenu:) forControlEvents:UIControlEventTouchUpInside];
    [coverView addSubview:rightMenu];
    
    
    
    //====================For back button==============//
    
//    UIView *backbuttonextended = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 100, 60)];
//    backbuttonextended.backgroundColor=[UIColor greenColor];
//    [self.view addSubview:backbuttonextended];
//    backbuttonextended.userInteractionEnabled=YES;
//    
//    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gobackoption)];
//    [backbuttonextended addGestureRecognizer:tap];
    
    
    
    
    rightmenuOpen = NO;
    
    
    //    [act startAnimating];
    
    //    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
}


-(void)changecoloragain
{
    //    change.backgroundColor=[UIColor clearColor];
    backlbl.alpha = 1.0f;
    labelHead.alpha = 1.0f;
    bck_img.alpha = 1.0f;
}
-(void)changecolor
{
    //    change.backgroundColor=[UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0];
    backlbl.alpha = 0.5f;
    labelHead.alpha = 1.0f;
    bck_img.alpha = 0.5f;
    
}



-(void)coverMethod: (NSTimer *)timeav
{
    [timeav invalidate];
    [coverview1 removeFromSuperview];
    [SVProgressHUD dismiss];
}

- (void) runSpinAnimationOnView:(UIImageView *)view duration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat;
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ * rotations * duration ];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = repeat;
    
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}


-(void)viewDidAppear:(BOOL)animated
{
    DebugLog(@"profile appears");
    
    [super viewDidAppear:YES];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    nobusiness=0;
    move=0;
    profile_insertion=0;
    profile_fetch=0;
    allowpersonal=0;
    
    
    
    mainview = [[UIView alloc]initWithFrame:CGRectMake(0, 65, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    [self.view addSubview:mainview];
    
    mainscroll=[[UIScrollView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, [UIScreen mainScreen].bounds.size.width, mainview.frame.size.height)];
    mainscroll.userInteractionEnabled=YES;
    mainscroll.scrollEnabled=YES;
    mainscroll.showsVerticalScrollIndicator=YES;
    //    mainscroll.contentSize=CGSizeMake(320.0f, 500.0f);
    mainscroll.showsVerticalScrollIndicator= NO;
    [mainview addSubview:mainscroll];
    
    act = [[UIActivityIndicatorView alloc] init];//WithFrame:CGRectMake(135, 190, 40, 40)];
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    act.center=mainscroll.center;
    [act setColor:[UIColor blackColor]];
    //    act.center = self.view.center;
    //    act.layer.zPosition=6;
    [mainscroll addSubview:act];
    [act startAnimating];
    
    mainview.backgroundColor=[UIColor clearColor];
    
    NSOperationQueue *queue=[NSOperationQueue new];
    NSInvocationOperation *op=[[NSInvocationOperation alloc] initWithTarget:self selector:@selector(loaddata) object:nil];
    [queue addOperation:op];
    
    
    //    [self loadViews];
    
    
    //================Personal starts here===================//
    
    //======================================================//
    
    //    mobileno = [UIButton buttonWithType:UIButtonTypeCustom];
    //
    //    [mobileno setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 40.0f, 0, 0)];
    //
    //    mobileno.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    //
    //    [mobileno setBackgroundImage:[UIImage imageNamed:@"detbuttonNormal"] forState:UIControlStateNormal];
    //
    //    [mobileno setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateHighlighted];
    //
    //    [mobileno setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateSelected];
    //
    //    [mobileno.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
    //
    //    [mobileno setTitleColor:[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f]forState:UIControlStateNormal];
    //
    //    [mobileno addTarget:self action:@selector(mobileFun:) forControlEvents:UIControlEventTouchUpInside];
    //
    //    [mainscroll addSubview:mobileno];
    //
    //
    //    mobileimg = [[UIImageView alloc]init];//WithFrame:CGRectMake(10.0f, 10.0f, [UIImage imageNamed:@"mobile_profile"].size.width, [UIImage imageNamed:@"mobile_profile"].size.height+12)];//WithFrame:CGRectMake(15, 195-60, 14, 13.5f)];
    //    mobileimg.image=[UIImage imageNamed:@"mobile_profile"];
    //    [mobileno addSubview:mobileimg];
    //
    //
    //    divider_9 = [[UIView alloc]init];//WithFrame:CGRectMake(14.5f, mail_lb.frame.origin.y+mail_lb.frame.size.height+15, 291, 1)];
    //
    //    [mainscroll addSubview:divider_9];
    //
    //    //==============Landline no. selection color==============//
    //
    //
    //    //========================================================//
    //
    //    landphonelb = [UIButton buttonWithType:UIButtonTypeCustom];
    //
    //    [landphonelb setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 40.0f, 0, 0)];
    //
    //    landphonelb.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    //
    //    [landphonelb setBackgroundImage:[UIImage imageNamed:@"detbuttonNormal"] forState:UIControlStateNormal];
    //
    //    [landphonelb setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateHighlighted];
    //
    //    [landphonelb setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateSelected];
    //
    //    [landphonelb.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
    //
    //    [landphonelb setTitleColor:[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f]forState:UIControlStateNormal];
    //
    //    [landphonelb addTarget:self action:@selector(landphoneFun:) forControlEvents:UIControlEventTouchUpInside];
    //
    //    [mainscroll addSubview:landphonelb];
    //
    //    landphone_img = [[UIImageView alloc]init];//WithFrame:CGRectMake(13, 18, [UIImage imageNamed:@"home_profile"].size.width, [UIImage imageNamed:@"home_profile"].size.height+12)];//WithFrame:CGRectMake(15, 220-60, 14, 13.5f)];
    //    landphone_img.image=[UIImage imageNamed:@"home_profile"];
    //    [landphonelb addSubview:landphone_img];
    //
    //
    //    divider_1 = [[UIView alloc]init];//WithFrame:CGRectMake(14.5f, 248-60, 291, 1)];
    //
    //
    //    [mainscroll addSubview:divider_1];
    //
    //    //==============Mail selection color==============//
    //
    //    //========================================================//
    //
    //    mail_lb = [UIButton buttonWithType:UIButtonTypeCustom];
    //
    //    [mail_lb setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 40, 0, 0)];
    //
    //    mail_lb.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    //
    //    [mail_lb setBackgroundImage:[UIImage imageNamed:@"detbuttonNormal"] forState:UIControlStateNormal];
    //
    //    [mail_lb setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateHighlighted];
    //
    //    [mail_lb setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateSelected];
    //
    //    [mail_lb.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
    //
    //    [mail_lb setTitleColor:[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f]forState:UIControlStateNormal];
    //
    //    [mail_lb addTarget:self action:@selector(mailFun:) forControlEvents:UIControlEventTouchUpInside];
    //
    //    [mainscroll addSubview:mail_lb];
    //
    //
    //    mailimg =[[UIImageView alloc]init];//WithFrame:CGRectMake(10, 10, [UIImage imageNamed:@"email_profile"].size.width, [UIImage imageNamed:@"email_profile"].size.height+10) ];//WithFrame:CGRectMake(15, divider_1.frame.origin.y+divider_1.frame.size.height+13, 16, 11)];
    //    mailimg.image=[UIImage imageNamed:@"email_profile"];
    //    [mail_lb addSubview:mailimg];
    //
    //
    //
    //
    //    //Checking for User Interaction
    //
    //    if ([other isEqualToString:@"yes"]) {
    //
    //        mobileno.userInteractionEnabled = YES;
    //
    //        landphonelb.userInteractionEnabled = YES;
    //
    //        mail_lb.userInteractionEnabled = YES;
    //
    //        website_lb.userInteractionEnabled = YES;
    //    }else{
    //
    //        mobileno.userInteractionEnabled = NO;
    //
    //        landphonelb.userInteractionEnabled = NO;
    //
    //        mail_lb.userInteractionEnabled = NO;
    //
    //        //        website_lb.userInteractionEnabled = NO;
    //    }
    //
    //    divider_2 = [[UIView alloc]init];//WithFrame:CGRectMake(14.5f, website_lb.frame.origin.y+website_lb.frame.size.height+11, 291, 1)];
    //
    //    [mainscroll addSubview:divider_2];
    //
    //    //==============Address selection color==============//
    //
    //    //========================================================//
    //
    //    address_lb = [UIButton buttonWithType:UIButtonTypeCustom];
    //
    //    [address_lb setTitleEdgeInsets:UIEdgeInsetsMake(14.0f, 40, 0, 0)];
    //
    //    address_lb.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    //
    //    [address_lb setBackgroundImage:[UIImage imageNamed:@"detbuttonNormal"] forState:UIControlStateNormal];
    //
    //    [address_lb setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateHighlighted];
    //
    //    [address_lb setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateSelected];
    //
    //    [address_lb.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
    //
    //    address_lb.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    //
    //    [address_lb setTitleColor:[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f]forState:UIControlStateNormal];
    //
    //    [address_lb addTarget:self action:@selector(openmap:) forControlEvents:UIControlEventTouchUpInside];
    //
    //    [mainscroll addSubview:address_lb];
    //
    //    street_number_lbl = [[UILabel alloc] init];//WithFrame:CGRectMake(40, address_lb.frame.origin.y,200,27)];
    //    street_number_lbl.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
    //    street_number_lbl.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    //    street_number_lbl.backgroundColor = [UIColor clearColor];
    //    [address_lb addSubview:street_number_lbl];
    //
    //    zipcode_lbl = [[UILabel alloc] init];//WithFrame:CGRectMake(40, street_number_lbl.frame.origin.y+street_number_lbl.frame.size.height,75,27)];
    //    zipcode_lbl.backgroundColor = [UIColor clearColor];
    //    zipcode_lbl.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
    //    zipcode_lbl.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    //    [address_lb addSubview:zipcode_lbl];
    //
    ////    city_lbl = [[UILabel alloc] initWithFrame:CGRectMake(zipcode_lbl.frame.origin.x+zipcode_lbl.frame.size.width, street_number_lbl.frame.origin.y+street_number_lbl.frame.size.height,100,27)];
    ////    city_lbl.backgroundColor = [UIColor clearColor];
    ////    city_lbl.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
    ////    city_lbl.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    ////    [address_lb addSubview:city_lbl];
    //
    //    country_lbl = [[UILabel alloc] init];//WithFrame:CGRectMake(40, zipcode_lbl.frame.origin.y+zipcode_lbl.frame.size.height,100,27)];
    //    country_lbl.backgroundColor = [UIColor clearColor];
    //    country_lbl.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
    //    country_lbl.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    //    [address_lb addSubview:country_lbl];
    //
    //    address_img=[[UIImageView alloc]init];//WithFrame:CGRectMake(15,divider_2.frame.origin.y+divider_2.frame.size.height+11,12,17)];
    //    address_img.image=[UIImage imageNamed:@"map_profile"];
    //    [address_lb addSubview:address_img];
    //
    //
    //
    //    divider_3 = [[UIView alloc]init];//WithFrame:CGRectMake(14.5f, address_lb.frame.origin.y+address_lb.frame.size.height+5, 291, 1)];
    //
    //    [mainscroll addSubview:divider_3];
    //
    //
    //    companyimg = [[UIImageView alloc]init];//WithFrame:CGRectMake(15, divider_1.frame.origin.y+divider_1.frame.size.height+35, 14, 13.5f)];
    //
    //    companyimg.image=[UIImage imageNamed:@"company"];
    //
    //    [mainscroll addSubview:companyimg];
    //
    //
    //    companylb = [[UILabel alloc]init];//WithFrame:CGRectMake(38, divider_1.frame.origin.y+divider_1.frame.size.height+32, 275, 19)];
    //
    //    [mainscroll addSubview:companylb];
    //
    //    companylb.backgroundColor=[UIColor clearColor];
    //    companylb.textColor = [UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
    //    companylb.font= [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    //
    //    desigimg = [[UIImageView alloc]init];//WithFrame:CGRectMake(15, divider_1.frame.origin.y+divider_1.frame.size.height+35, 14, 13.5f)];
    //
    //    desigimg.image=[UIImage imageNamed:@"Designation.png"];
    //
    //    [mainscroll addSubview:desigimg];
    //
    //
    //    desiglb = [[UILabel alloc]init];//WithFrame:CGRectMake(38, divider_1.frame.origin.y+divider_1.frame.size.height+32, 275, 19)];
    //
    //    [mainscroll addSubview:desiglb];
    //    desiglb.backgroundColor=[UIColor clearColor];
    //    desiglb.textColor = [UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
    //    desiglb.font= [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    //
    //
    //    [mobileno setTitle:[NSString stringWithFormat:@"%@", mobile] forState:UIControlStateNormal];
    //
    //    [landphonelb setTitle:[NSString stringWithFormat:@"%@", landline] forState:UIControlStateNormal];
    //
    //    [mail_lb setTitle:[NSString stringWithFormat:@"%@",mail] forState:UIControlStateNormal];
    //    website_lb.text= website;
    //    //    address_lb.text=address;
    //    //[address_lb setTitle:[NSString stringWithFormat:@"%@",address] forState:UIControlStateNormal];
    //
    //    [self setAddress];
    //
    //    //    if ([street isEqualToString: @""] && [number isEqualToString: @""])
    //    //    {
    //    //
    //    //        if ([zipcode isEqualToString: @""])
    //    //        {
    //    //            if ([city isEqualToString:@""]) {
    //    //                country_lbl.frame = street_number_lbl.frame;
    //    //                country_lbl.text = [NSString stringWithFormat:@"%@",country];
    //    //
    //    //            }
    //    //            else
    //    //            {
    //    //                city_lbl.frame = street_number_lbl.frame;
    //    //                country_lbl.frame = zipcode_lbl.frame;
    //    //                city_lbl.text = [NSString stringWithFormat:@"%@,",city];
    //    //                country_lbl.text = [NSString stringWithFormat:@"%@",country];
    //    //            }
    //    //
    //    //        }
    //    //
    //    //        else
    //    //        {
    //    //            //country_lbl.frame = zipcode_lbl.frame;
    //    //
    //    //            zipcode_lbl.frame = street_number_lbl.frame;
    //    //
    //    //
    //    //
    //    //
    //    //            // street_number_lbl.text =[NSString stringWithFormat:@"%@\n %@",street,number];
    //    //            zipcode_lbl.text = [NSString stringWithFormat:@"%@,",zipcode];
    //    //            [zipcode_lbl sizeToFit];
    //    //
    //    //            CGRect new_frame = city_lbl.frame;
    //    //            new_frame.origin.y = zipcode_lbl.frame.origin.y-3;
    //    //            new_frame.origin.x = zipcode_lbl.frame.origin.x+zipcode_lbl.frame.size.width+5;
    //    //            city_lbl.frame = new_frame;
    //    //
    //    //            CGRect new_country_frame = country_lbl.frame;
    //    //            new_country_frame.origin.y = zipcode_lbl.frame.origin.y+zipcode_lbl.frame.size.height;
    //    //            country_lbl.frame = new_country_frame;
    //    //
    //    //
    //    //            city_lbl.text = [NSString stringWithFormat:@"%@,",city];
    //    //            country_lbl.text = [NSString stringWithFormat:@"%@",country];
    //    //        }
    //    //
    //    //
    //    //
    //    //    }
    //    //    else
    //    //    {
    //    //
    //    //        if ([zipcode isEqualToString: @""]) {
    //    //
    //    //            if ([city isEqualToString:@""]) {
    //    //                country_lbl.frame = zipcode_lbl.frame;
    //    //                street_number_lbl.text =[NSString stringWithFormat:@"%@\n, %@",street,number];
    //    //                country_lbl.text = [NSString stringWithFormat:@"%@",country];
    //    //            }
    //    //            else
    //    //            {
    //    //                city_lbl.frame = zipcode_lbl.frame;
    //    //
    //    //                street_number_lbl.text =[NSString stringWithFormat:@"%@\n, %@",street,number];
    //    //
    //    //                city_lbl.text = [NSString stringWithFormat:@"%@,",city];
    //    //                country_lbl.text = [NSString stringWithFormat:@"%@",country];
    //    //            }
    //    //        }
    //    //
    //    //        else
    //    //        {
    //    //            street_number_lbl.text =[NSString stringWithFormat:@"%@\n, %@",street,number];
    //    //
    //    //            zipcode_lbl.text = [NSString stringWithFormat:@"%@,",zipcode];
    //    //            city_lbl.text = [NSString stringWithFormat:@"%@,",city];
    //    //            country_lbl.text = [NSString stringWithFormat:@"%@",country];
    //    //        }
    //    //
    //    //
    //    //    }
    //
    //
    //
    //    desiglb.text= desig;
    //    companylb.text= company;
    //
    //
    //
    //    [self performSelectorInBackground:@selector(yesSuccess)
    //                           withObject:nil];
    //    [del showTabValues:YES];
    //
    //    divider_4 = [[UIView alloc]init];//WithFrame:CGRectMake(14.5f, 248-60, 291, 1)];
    //
    //    //    divider_4.image=[UIImage imageNamed:@"divider-prof.png"];
    //
    //    [mainscroll addSubview:divider_4];
    //
    //
    //    divider_5 = [[UIView alloc]init];//WithFrame:CGRectMake(14.5f, 248-60, 291, 1)];
    //
    //    //    if ([uid intValue] != [[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"] intValue])
    //    [mainscroll addSubview:divider_5];
    //
    //    website_lb.userInteractionEnabled = YES;
    //    UITapGestureRecognizer *websiteTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(websiteFun:)];
    //
    //    [website_lb addGestureRecognizer:websiteTap];
    
    //    [self loadViews];
    
    
    divider_1.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
    divider_2.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
    divider_3.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
    divider_4.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
    divider_5.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
    [divider_9 setBackgroundColor:[UIColor colorWithRed:(239.0f / 255.0f) green:(239.0f / 255.0f) blue:(239.0f / 255.0f) alpha:1.0f]];
    
    
    [mainview bringSubviewToFront:boxscroll];
    rightMenu.enabled=YES;
    rightmenuOpen = YES;
}

-(void)loaddata
{
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"firstProfile"] isEqualToString:@"yes"])
    {
        NSError *error;
        NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=profile&id=%@&access_token=%@&device_id=%@&image=true",user_id,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
        
        DebugLog(@"profile url: %@",urlString1);
        
        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
        
        if (signeddataURL1 != nil)
            
            json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                    options:kNilOptions
                                                      error:&error];
        else
        {
            DebugLog(@"no connnn profile");
            alert = [[UIAlertView alloc] initWithTitle:@"Error in Profile Server Connection!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
        }
        
        NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
        DebugLog(@"err  %@",errornumber);
        
        if (![errornumber isEqualToString:@"0"])
        {
            [act removeFromSuperview];
            NSString *err_str = [json1 objectForKey:@"error"];
            alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                               message:err_str
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
        }
        
        else
            
        {
            
            dict_profile = [[NSMutableDictionary alloc]init];
            
            profdict= [json1 objectForKey:@"details"];
            
            dict_profile = [profdict objectForKey:@"profile"];
            
            [self performSelectorOnMainThread:@selector(startDBInserta:)
                                   withObject:nil
                                waitUntilDone:YES];
            
            //        [[DBManager getSharedInstance]insertProfile:profdict :[user_id intValue]];
            
            [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"firstProfile"];
            DebugLog(@"dict pro%@",dict_profile);
            
            if (![[NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]] isKindOfClass:[NSNull class]] && ![[NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]] isEqualToString:@""] && ![[NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]] isEqualToString:@"(null)"] && [[NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]] length]>0) {
                
                fullname= [NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]];
                
            }
            
            
            
            [prof_name removeFromSuperview];
            
            base64String= [profdict objectForKey:@"image"];
            
            
            //        [self performSelectorInBackground:@selector(restoreBackUp)
            //                               withObject:nil];
            
            if ([user_id intValue] == [[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]intValue])
            {
                switchprof=0;
                [self loadPersonalViews];
            }
            else
            {
                if ([[profdict objectForKey:@"friend"]intValue] == 1)
                {
                    switchprof=0;
                    [self loadPersonalViews];
                }
                
                else if ([[profdict objectForKey:@"business"] intValue]==1)
                {
                    switchprof=1;
                    allowpersonal=1;
                    [self loadBusinessView];
                }
                DebugLog(@"fields are: %d , %d",[[profdict objectForKey:@"own"]intValue], switchprof);
            }
            //            if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"isfriend"] integerValue]==2)
            //                [self loadBusinessView];
            //            else
            //                [self loadPersonalViews];
            grayback = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 110.0f)];
            [grayback setBackgroundColor:[UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f]];
            [mainscroll addSubview:grayback];
            
            [self performSelectorOnMainThread:@selector(displaydata) withObject:nil waitUntilDone:NO];
        }
        
    }
    
    
    
    /////////////////////////////////////////////   Load from Local DB   ////////////////////////////////////////////////////////////////////
    
    else
    {
        allowpersonal=0;
        DebugLog(@"eittttto etatei ");
        NSDictionary *localDBProfile = [[NSDictionary alloc]init];
        
        //        Mymodel *obj=[Mymodel getInstance];
        //        if (obj.db_busy == NO)
        //        {
        //            profile_fetch=1;
        localDBProfile = [[DBManager getSharedInstance]fetchProfile:[user_id intValue]];
        //        }
        
        DebugLog(@"this gives: %@",localDBProfile);
        DebugLog(@"BUSINESS BUSINESS:%@",[localDBProfile valueForKey:@"business"]);
        
        NSArray * allKeys = [localDBProfile allKeys];
        DebugLog(@"Count : %lu", (unsigned long)[allKeys count]);
        
        if ([allKeys count] == 0 || [[localDBProfile objectForKey:@"name"] isKindOfClass:[NSNull class]] || [[localDBProfile objectForKey:@"name"] length]==0 || [[localDBProfile objectForKey:@"name"] isEqualToString:@"(null)"])
            
        {
            DebugLog(@"this gives null profile");
            
            [self reloadDataFromWeb];
        }
        else
        {
            //        [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"profile_edited"];
            
            if ([user_id intValue] == [[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"] intValue] && [[[NSUserDefaults standardUserDefaults]objectForKey:@"profile_edited"] isEqualToString:@"yes"])
                
                [self reloadDataFromWeb];
            
            dict_profile = [[NSMutableDictionary alloc]init];
            
            //            dict_profile = [[NSMutableDictionary alloc]init];
            //
            //            dict_profile = [[NSMutableDictionary alloc]init];
            
            dict_profile = [localDBProfile mutableCopy];
            
            
            
            UIImageView *prof_img = [[UIImageView alloc]initWithFrame:CGRectMake(15, 75-64, 90, 90)];
            
            [grayback addSubview:prof_img];
            
            
            //        if ([user_id intValue] == [[prefs objectForKey:@"userid"] intValue])
            //        {
            
            
            //            [prof_name removeFromSuperview];
            
            //            prof_name = [[UILabel alloc]init]; //WithFrame:CGRectMake(112, 72, 196, 60)];
            //            prof_name.backgroundColor=[UIColor clearColor];
            //            prof_name.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
            //            prof_name.text=fullname;
            //            prof_name.font=[UIFont fontWithName:@"ProximaNova-Bold" size:18];
            //            prof_name.numberOfLines=0;
            //            prof_name.frame=CGRectMake(120, 75-70, 195, 60);
            //            //            prof_name.textAlignment= NSTextAlignmentCenter;
            //            [grayback addSubview:prof_name];
            
            //        }
            
            
            
            //            if ([base64String length] >6)
            //
            //            {
            //
            //                NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
            //                profilepic = [UIImage imageWithData:decodedData];
            //                prof_img.image=profilepic;
            //                prof_img.contentMode= UIViewContentModeScaleAspectFill;
            //                prof_img.clipsToBounds=YES;
            //            }
            //
            //            prof_img.userInteractionEnabled=YES;
            //
            //            UITapGestureRecognizer *propictap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(detailpic:)];
            //            [prof_img addGestureRecognizer:propictap];
            [self performSelector:@selector(reloadDataFromWeb)];
        }
        
        if (![[NSUserDefaults standardUserDefaults] boolForKey:@"error"])
        {
            fullname= [NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]];
            if ([profdict objectForKey:@"image"] == nil) {
                base64String= [profdict objectForKey:@"thumb"];
            }
            else
                base64String= [profdict objectForKey:@"image"];
            grayback = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 110.0f)];
            [grayback setBackgroundColor:[UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f]];
            [mainscroll addSubview:grayback];
            prof_name.frame = CGRectMake(0, 0, 0, 0);
            [grayback addSubview:prof_name];
            [self performSelectorOnMainThread:@selector(displaydata) withObject:nil waitUntilDone:YES];
        }
        else
        {
            [act removeFromSuperview];
            UIAlertView *showalert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Cannot load data. Please log in again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [showalert show];
        }
        
    }
    
    
    
    
}

-(void)displaydata
{
    
    [act removeFromSuperview];
    
    prof_name = [[UILabel alloc]init]; //WithFrame:CGRectMake(112, 72, 196, 60)];
    prof_name.backgroundColor=[UIColor clearColor];
    prof_name.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
    if ([fullname isKindOfClass:[NSNull class]] || [fullname isEqualToString:@""] || [fullname isEqualToString:@"(null) (null)"] || [fullname length]==0) {
        
        prof_name.text = @"";
        prof_name.frame = CGRectMake(0, 0, 0, 0);
        DebugLog(@"PROFILE NAME:%@",prof_name.text);
        
        
    }
    else
    {
        
        prof_name.text=fullname;
        DebugLog(@"PROFILE NAME:%@",prof_name.text);
        prof_name.font=[UIFont fontWithName:@"ProximaNova-Bold" size:18];
        prof_name.numberOfLines=0;
        prof_name.frame=CGRectMake(120, 75-70, 195, 60);
        [grayback addSubview:prof_name];
        // [prof_name removeFromSuperview];
        
    }
    // prof_name.text=fullname;
    
    
    UIImageView *prof_img = [[UIImageView alloc]initWithFrame:CGRectMake(15, 75-64, 90, 90)];
    
    [grayback addSubview:prof_img];
    
    if ([base64String length] >6)
    {
        NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
        
        //    NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
        
        profilepic = [UIImage imageWithData:decodedData];
        prof_img.image=profilepic;
        prof_img.contentMode= UIViewContentModeScaleAspectFill;
        prof_img.clipsToBounds=YES;
    }
    
    prof_img.userInteractionEnabled=YES;
    
    UITapGestureRecognizer *propictap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(detailpic:)];
    
    [prof_img addGestureRecognizer:propictap];
    
    
    
    toggleimage = [[UIImageView alloc]init];
    
    toggleimage.backgroundColor=[UIColor clearColor];
    
    [mainscroll addSubview:toggleimage];
    
    
    personaltext = [[UILabel alloc]initWithFrame:CGRectMake(132, 138-60, 75, 25)];
    [mainscroll addSubview:personaltext];
    personaltext.backgroundColor=[UIColor clearColor];
    if (switchprof == 0)
        personaltext.textColor=[UIColor whiteColor];
    else
        personaltext.textColor=[UIColor lightGrayColor];
    
    personaltext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:15];
    
    businesstext = [[UILabel alloc]initWithFrame:CGRectMake(225, 138-60, 75, 25)];
    
    [mainscroll addSubview:businesstext];
    
    businesstext.backgroundColor=[UIColor clearColor];
    
    if (switchprof == 0)
        businesstext.textColor=[UIColor lightGrayColor];
    
    else
        businesstext.textColor=[UIColor whiteColor];
    
    businesstext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:15];
    
    if ([user_id intValue] == [[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"] intValue])
    {
        UIButton *editprofbt= [UIButton buttonWithType:UIButtonTypeCustom];
        [grayback addSubview:editprofbt];
        [editprofbt setBackgroundImage:[UIImage imageNamed:@"editprofbt.png"] forState:UIControlStateNormal];
        editprofbt.frame = CGRectMake(120, 136.5f-60, 185, 28.5f);
        [editprofbt setTitle:@"edit profile" forState:UIControlStateNormal];
        [editprofbt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [editprofbt addTarget:self action:@selector(editprofile_func:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        //============================CALL & SMS BUTTON FOR OTHER USER==================//
        
        bgcallsms = [[UIButton alloc]initWithFrame:CGRectMake(120, 136.5f-72.0f, 91.3f, 35)];
        [grayback addSubview:bgcallsms];
        bgcallsms.userInteractionEnabled=YES;
        [bgcallsms setBackgroundImage:[UIImage imageNamed:@"call_profile"] forState:UIControlStateNormal];
        //        [bgcallsms setBackgroundImage:[UIImage imageNamed:@"callend_profile"] forState:UIControlStateSelected];
        //        [bgcallsms setBackgroundImage:[UIImage imageNamed:@"callend_profile"] forState:UIControlStateHighlighted];
        [bgcallsms addTarget:self action:@selector(phonefunc) forControlEvents:UIControlEventTouchUpInside];
        
        bgcallsms1 = [[UIButton alloc]initWithFrame:CGRectMake(212.3f, 136.5f-72.0f, 91.3f, 35)];
        [grayback addSubview:bgcallsms1];
        bgcallsms1.userInteractionEnabled=YES;
        [bgcallsms1 setBackgroundImage:[UIImage imageNamed:@"sms_profile"] forState:UIControlStateNormal];
        
        [bgcallsms1 addTarget:self action:@selector(chatfunc:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    if ([user_id intValue] == [[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]intValue])
    {
        switchprof=0;
        [self loadPersonalViews];
    }
    else
    {
        if ([[profdict objectForKey:@"friend"]intValue] == 1)
        {
            switchprof=0;
            [self loadPersonalViews];
        }
        
        else if ([[profdict objectForKey:@"business"] intValue]==1)
        {
            switchprof=1;
            [self loadBusinessView];
        }
        DebugLog(@"fields are: %d , %d",[[profdict objectForKey:@"own"]intValue], switchprof);
    }
    
}


-(void)loadViews
{
    DebugLog(@"SWITCHPROF===============>%d",switchprof);
    
    [act removeFromSuperview];
    
    // NSString *street,*city,*zipcode,*country, *number;
    NSString *mobile_num1, *landline_num1, *business_num1;
    phonearr=[[NSMutableArray alloc]init];
    
    [boxscroll removeFromSuperview];
    [divider_4 removeFromSuperview];
    //    [divider_5 removeFromSuperview];
    [b_boxscroll removeFromSuperview];
    
    
    [mobileimg setHidden:NO];
    
    [mobileno setHidden:NO];
    
    if (switchprof ==0)
    {
        [business_view removeFromSuperview];
        
        companyimg.hidden=YES;
        companylb.hidden=YES;
        desigimg.hidden=YES;
        desiglb.hidden=YES;
        
        [mobileno setHidden:NO];
        [mail_lb setHidden:NO];
        [address_lb setHidden:NO];
        [boxscroll setHidden:NO];
        
        
        [divider_0 removeFromSuperview];
        [divider_10 removeFromSuperview];
        
        [self loadPersonalViews];
        
        //        if ([dict_profile objectForKey:@"mobile_num"] != false && ![[dict_profile objectForKey:@"mobile_num"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"mobile_num"] length] > 1 && ![[dict_profile objectForKey:@"mobile_num"] isEqualToString:@"(null)"])
        //        {
        //
        //            mobile= [NSString stringWithFormat:@"%@%@",[dict_profile objectForKey:@"mobile_pre"],[dict_profile objectForKey:@"mobile_num"]];
        //            mobileArray = [[NSMutableArray alloc]init];
        //            [mobileArray addObject:mobile];
        //            [mobileno setHidden:NO];
        //            [mobileimg setHidden:NO];
        //            mobileimg.frame = CGRectMake(13, 20, [UIImage imageNamed:@"mobile_profile"].size.width/1.5,[UIImage imageNamed:@"mobile_profile"].size.height/1.5);
        //            //            mobileimg.frame = CGRectMake(13, 18, 14, 18);
        //            mobileno.frame = CGRectMake(0, businesstext.frame.origin.y+businesstext.frame.size.height+7.5f, 320, [UIImage imageNamed:@"detbuttonNormal"].size.height + 8);
        //            //            mobileback.frame = CGRectMake(0, businesstext.frame.origin.y+businesstext.frame.size.height+7, [UIScreen mainScreen].bounds.size.width, 43);
        //            divider_9.frame = CGRectMake(0,  mobileno.frame.origin.y+mobileno.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.4f);
        //
        //            DebugLog(@"IF PART A ENTERING");
        //        }else{
        //
        //            mobile=@"";
        //
        //            mobileimg.frame = CGRectMake(13, 20, [UIImage imageNamed:@"mobile_profile"].size.width/1.5,[UIImage imageNamed:@"mobile_profile"].size.height/1.5);
        //            //            mobileimg.frame = CGRectMake(13, 15, 14, 0);
        //
        //            mobileno.frame = CGRectMake(0, businesstext.frame.origin.y+businesstext.frame.size.height+7.5f, 320, 0);
        //            divider_9.frame = CGRectMake(0,  mobileno.frame.origin.y+mobileno.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        //        }
        //
        //
        //        if ([dict_profile objectForKey:@"phone_num"] != false && ![[dict_profile objectForKey:@"phone_num"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"phone_num"] length] > 1 && ![[dict_profile objectForKey:@"phone_num"] isEqualToString:@"(null)"])
        //        {
        //            landline= [NSString stringWithFormat:@"%@%@",[dict_profile objectForKey:@"phone_pre"],[dict_profile objectForKey:@"phone_num"]];
        //
        //            [landphonelb setHidden:NO];
        //
        //            [landphone_img setHidden:NO];
        //
        //            landphone_img.frame = CGRectMake(13, 10, [UIImage imageNamed:@"home_profile"].size.width/1.5, [UIImage imageNamed:@"home_profile"].size.height/1.5);
        //            //            landphone_img.frame = CGRectMake(13, 18, 14, 18);
        //
        //            landphonelb.frame = CGRectMake(0, divider_9.frame.origin.y+divider_9.frame.size.height, [UIImage imageNamed:@"detbuttonNormal"].size.width, [UIImage imageNamed:@"detbuttonNormal"].size.height+10);
        //
        //            //            divider_1.frame = CGRectMake(0,  landphonelb.frame.origin.y+landphonelb.frame.size.height+3, [UIScreen mainScreen].bounds.size.width, 0.7f);
        //            //            landlineback.frame = CGRectMake(0, divider_9.frame.origin.y+divider_9.frame.size.height, [UIScreen mainScreen].bounds.size.width, 47);
        //
        //        }else{
        //
        //            landline=@"";
        //
        //            landphone_img.frame = CGRectMake(13, 15, 14, 0);
        //
        //            landphonelb.frame = CGRectMake(0, divider_9.frame.origin.y+divider_9.frame.size.height, [UIImage imageNamed:@"detbuttonNormal"].size.width, 0);
        //
        //            //            divider_1.frame = CGRectMake(0,  landphonelb.frame.origin.y+landphonelb.frame.size.height+3, [UIScreen mainScreen].bounds.size.width, 0);
        //        }
        //
        //
        //
        //        if (![mobile isEqualToString:@""] && ![landline isEqualToString:@""]) {
        //
        //            divider_1.frame = CGRectMake(0,  landphonelb.frame.origin.y+landphonelb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        //
        //        }
        //
        //        //        else if (![mobile isEqualToString:@""] || ![landline isEqualToString:@""]) {
        //        //
        //        //            divider_1.frame = CGRectMake(0,  landphonelb.frame.origin.y+landphonelb.frame.size.height+3, [UIScreen mainScreen].bounds.size.width, 0.7f);
        //        //        }
        //
        //        else if (![landline isEqualToString:@""]) {
        //
        //            divider_1.frame = CGRectMake(0,  landphonelb.frame.origin.y+landphonelb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        //        }
        //
        //        else {
        //
        //            divider_1.frame = CGRectMake(0,  landphonelb.frame.origin.y+landphonelb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        //        }
        //
        //
        //        if (![mobile isEqualToString:@""] && ![landline isEqualToString:@""] && [dict_profile objectForKey:@"email"] != false && ![[dict_profile objectForKey:@"email"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"email"] length] > 1 && ![[dict_profile objectForKey:@"email"] isEqualToString:@"(null)"])
        //        {
        //            mail= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"email"]];
        //
        //            [mail_lb setHidden:NO];
        //
        //            [mailimg setHidden:NO];
        //
        //            mailimg.frame= CGRectMake(13, 20, [UIImage imageNamed:@"email_profile"].size.width/1.5, [UIImage imageNamed:@"email_profile"].size.height/1.5);
        //            //            mailimg.frame = CGRectMake(13, 18, 14.5f, 14);
        //
        //            mail_lb.frame = CGRectMake(0, divider_1.frame.origin.y+divider_1.frame.size.height, [UIImage imageNamed:@"detbuttonNormal"].size.width,[UIImage imageNamed:@"detbuttonNormal"].size.height+10);
        //
        //            mailback.frame = CGRectMake(0, divider_1.frame.origin.y+divider_1.frame.size.height, [UIScreen mainScreen].bounds.size.width, 51);
        //
        //        }else if ([landline isEqualToString:@""] && [dict_profile objectForKey:@"email"] != false && ![[dict_profile objectForKey:@"email"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"email"] length] > 1 && ![[dict_profile objectForKey:@"email"] isEqualToString:@"(null)"])
        //        {
        //            mail= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"email"]];
        //
        //            [mail_lb setHidden:NO];
        //
        //            [mailimg setHidden:NO];
        //
        //            mailimg.frame= CGRectMake(13, 20, [UIImage imageNamed:@"email_profile"].size.width/1.5, [UIImage imageNamed:@"email_profile"].size.height/1.5);
        //            //            mailimg.frame = CGRectMake(13, 18, 14.5f, 14);
        //
        //            mail_lb.frame = CGRectMake(0, divider_9.frame.origin.y+divider_9.frame.size.height, [UIImage imageNamed:@"detbuttonNormal"].size.width, [UIImage imageNamed:@"detbuttonNormal"].size.height+10);
        //
        //            mailback.frame = CGRectMake(0, divider_9.frame.origin.y+divider_9.frame.size.height, [UIScreen mainScreen].bounds.size.width, 51);
        //
        //        } else{
        //
        //            mail=@"";
        //
        //            mailimg.frame = CGRectMake(13, 16, 16, 0);
        //
        //            mail_lb.frame = CGRectMake(0, divider_1.frame.origin.y+divider_1.frame.size.height, [UIImage imageNamed:@"detbuttonNormal"].size.width, 0);
        //        }
        //
        //
        //        //        if ([dict_profile objectForKey:@"website"] != false && ![[dict_profile objectForKey:@"website"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"website"] length] > 1 && ![[dict_profile objectForKey:@"website"] isEqualToString:@"(null)"])
        //        //        {
        //        //            website= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"website"]];
        //        //
        //        //            [website_lb setHidden:NO];
        //        //
        //        //            [website_img setHidden:NO];
        //        //
        //        //            website_img.frame = CGRectMake(15, mail_lb.frame.origin.y+mail_lb.frame.size.height+17, 14.5f, 14);
        //        //
        //        //            website_lb.frame = CGRectMake(38, mail_lb.frame.origin.y+mail_lb.frame.size.height+13.5f, 275, 19);
        //        //        }else{
        //        //
        //        //            website=@"";
        //        //
        //        //            website_img.frame = CGRectMake(15, mail_lb.frame.origin.y+mail_lb.frame.size.height+17, 16, 0);
        //        //
        //        //            website_lb.frame = CGRectMake(38, mail_lb.frame.origin.y+mail_lb.frame.size.height+13.5f, 275, 0);
        //        //        }
        //
        //
        //        //        if (![mail isEqualToString:@""]){   //&& ![website isEqualToString:@""]) {
        //        //
        //        //            divider_2.frame = CGRectMake(0,  mail_lb.frame.origin.y+mail_lb.frame.size.height+16, [UIScreen mainScreen].bounds.size.width, 0.7f);
        //        //        }else
        //
        //        if (![mail isEqualToString:@""]){  //|| ![website isEqualToString:@""]) {
        //
        //            divider_2.frame = CGRectMake(0,  mail_lb.frame.origin.y+mail_lb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        //        }
        //
        //        //        else if (![website isEqualToString:@""]) {
        //        //
        //        //            divider_2.frame = CGRectMake(14.5f,  website_lb.frame.origin.y+website_lb.frame.size.height+16, 291, 0.5f);
        //        //        }
        //
        //        else {
        //
        //            divider_2.frame = CGRectMake(0,  mail_lb.frame.origin.y+mail_lb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        //        }
        //
        //        //======================================PERSONAL ADDRESS =============================//
        //
        //        if ([dict_profile objectForKey:@"city"] != false && ![[dict_profile objectForKey:@"city"] isEqualToString:@"(null)"])
        //        {
        //            city= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"city"]];
        //        }
        //        else
        //
        //            city=@"";
        //
        //        if ([dict_profile objectForKey:@"street"] != false && ![[dict_profile objectForKey:@"street"] isEqualToString:@"(null)"])
        //        {
        //            street= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"street"]];
        //        }
        //        else
        //
        //            street=@"";
        //
        //        if ([dict_profile objectForKey:@"zipcode"] != false && ![[dict_profile objectForKey:@"zipcode"] isEqualToString:@"(null)"])
        //        {
        //            zipcode= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"zipcode"]];
        //        }
        //
        //        else
        //            zipcode=@"";
        //
        //        if ([dict_profile objectForKey:@"country"] != false && ![[dict_profile objectForKey:@"country"] isEqualToString:@"(null)"])
        //        {
        //            country= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"country"]];
        //        }
        //        else
        //            country=@"";
        //
        //        if ([dict_profile objectForKey:@"housenumber"] != false && ![[dict_profile objectForKey:@"housenumber"] isEqualToString:@"(null)"])
        //        {
        //            number= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"housenumber"]];
        //        }
        //        else
        //            number= @"";
        //
        //        if ( ![street isEqualToString: @""] || ![number isEqualToString: @""] || ![city isEqualToString: @""] || ![zipcode isEqualToString: @""] || ![country isEqualToString: @""])
        //        {
        //
        //            [self setAddress];
        //            //            if ([street isEqualToString: @""] && [number isEqualToString: @""])
        //            //            {
        //            //
        //            //                if ([zipcode isEqualToString: @""])
        //            //                {
        //            //                    if ([city isEqualToString:@""]) {
        //            //                        country_lbl.frame = street_number_lbl.frame;
        //            //                        country_lbl.text = [NSString stringWithFormat:@"%@",country];
        //            //                    }
        //            //                    else
        //            //                    {
        //            //                        city_lbl.frame = street_number_lbl.frame;
        //            //                        country_lbl.frame = zipcode_lbl.frame;
        //            //                        city_lbl.text = [NSString stringWithFormat:@"%@,",city];
        //            //                        country_lbl.text = [NSString stringWithFormat:@"%@",country];
        //            //                    }
        //            //                }
        //            //
        //            //                else
        //            //                {
        //            //                    //country_lbl.frame = zipcode_lbl.frame;
        //            //
        //            //                    zipcode_lbl.frame = street_number_lbl.frame;
        //            //
        //            //
        //            //
        //            //
        //            //                    // street_number_lbl.text =[NSString stringWithFormat:@"%@\n %@",street,number];
        //            //                    zipcode_lbl.text = [NSString stringWithFormat:@"%@,",zipcode];
        //            //                    [zipcode_lbl sizeToFit];
        //            //
        //            //                    CGRect new_frame = city_lbl.frame;
        //            //                    new_frame.origin.y = zipcode_lbl.frame.origin.y-3;
        //            //                    new_frame.origin.x = zipcode_lbl.frame.origin.x+zipcode_lbl.frame.size.width+5;
        //            //                    city_lbl.frame = new_frame;
        //            //
        //            //                    CGRect new_country_frame = country_lbl.frame;
        //            //                    new_country_frame.origin.y = zipcode_lbl.frame.origin.y+zipcode_lbl.frame.size.height;
        //            //                    country_lbl.frame = new_country_frame;
        //            //
        //            //                    city_lbl.text = [NSString stringWithFormat:@"%@,",city];
        //            //                    country_lbl.text = [NSString stringWithFormat:@"%@",country];
        //            //                }
        //            //
        //            //
        //            //            }
        //            //            else
        //            //            {
        //            //
        //            //                if ([zipcode isEqualToString: @""]) {
        //            //
        //            //                    if ([city isEqualToString:@""]) {
        //            //                        country_lbl.frame = zipcode_lbl.frame;
        //            //                        street_number_lbl.text =[NSString stringWithFormat:@"%@\n, %@",street,number];
        //            //                        country_lbl.text = [NSString stringWithFormat:@"%@",country];
        //            //                    }
        //            //                    else
        //            //                    {
        //            //                        city_lbl.frame = zipcode_lbl.frame;
        //            //
        //            //                        street_number_lbl.text =[NSString stringWithFormat:@"%@\n, %@",street,number];
        //            //
        //            //                        city_lbl.text = [NSString stringWithFormat:@"%@,",city];
        //            //                        country_lbl.text = [NSString stringWithFormat:@"%@",country];
        //            //                    }
        //            //                }
        //            //
        //            //                else
        //            //                {
        //            //                    street_number_lbl.text =[NSString stringWithFormat:@"%@\n, %@",street,number];
        //            //
        //            //                    zipcode_lbl.text = [NSString stringWithFormat:@"%@,",zipcode];
        //            //                    city_lbl.text = [NSString stringWithFormat:@"%@,",city];
        //            //                    country_lbl.text = [NSString stringWithFormat:@"%@",country];
        //            //                }
        //            //
        //            //
        //            //
        //            //            }
        //
        //            //            address = [NSString stringWithFormat:@"%@\n %@ %@\n %@\n %@\n",street,number,zipcode,city,country];
        //            //            address_lb.backgroundColor = [UIColor greenColor];
        //
        //            DebugLog(@"ADRESSSSSSSSSSSS========================>%@   STREET===>%@ NUMBER===>%@ ZIP===>%@ CITY===>%@ COUNTRY===>%@",address,street,number,zipcode,city,country);
        //
        //            address_img.frame = CGRectMake(13, 5,[UIImage imageNamed:@"map_profile"].size.width/1.5,[UIImage imageNamed:@"map_profile"].size.height/1.5);
        //            //            address_img.frame = CGRectMake(13, 4,12,17);
        //
        //            //        address_lb.frame = CGRectMake(38, divider_2.frame.origin.y+divider_2.frame.size.height, 280, 90);
        //
        //            //            address_lb.lineBreakMode = NSLineBreakByWordWrapping;
        //
        //            //============================Dynamic label height with corresponding to adress string count======================//
        //
        //            CGSize maximumLabelSize = CGSizeMake(296, FLT_MAX);
        //
        //            CGSize expectedLabelSize = [address sizeWithFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]
        //
        //                                           constrainedToSize:maximumLabelSize
        //
        //                                               lineBreakMode:NSLineBreakByWordWrapping];
        //
        //            //adjust the label the the new height.
        //
        //            CGRect newFrame = CGRectMake(0, divider_2.frame.origin.y+1,[UIImage imageNamed:@"detbuttonNormal"].size.width,[UIImage imageNamed:@"detbuttonNormal"].size.height);//address_lb.frame;
        //
        //            newFrame.size.height = expectedLabelSize.height;
        //
        //            address_lb.frame = newFrame;//CGRectMake(38, address_img.frame.origin.y,280,newFrame.size.height);
        //
        //            [address_lb sizeToFit];
        //
        //            //            addressback.frame = CGRectMake(0, divider_2.frame.origin.y+divider_2.frame.size.height, [UIScreen mainScreen].bounds.size.width, 90);
        //
        //        }
        //        else{
        //
        //            address_img.frame = CGRectMake(13, 4,12,0);
        //
        //            address_lb.frame = CGRectMake(0, divider_2.frame.origin.y+divider_2.frame.size.height+1, [UIImage imageNamed:@"detbuttonNormal"].size.width, 0);
        //
        //        }
        //
        //        //Divider 3=======================|||
        //
        //        if (![number isEqualToString:@""] ) {
        //
        //            if (![city isEqualToString:@""]) {
        //
        //                if (![country isEqualToString:@""]) {
        //
        //                    divider_3.frame = CGRectMake(0,  address_lb.frame.origin.y+street_number_lbl.frame.size.height+city_lbl.frame.size.height+country_lbl.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        //
        //                    address_lb.frame = CGRectMake(0, divider_2.frame.origin.y+2, [UIScreen mainScreen].bounds.size.width, divider_3.frame.origin.y-divider_2.frame.origin.y+2);
        //                }
        //                else
        //                {
        //                    divider_3.frame = CGRectMake(0,  address_lb.frame.origin.y+street_number_lbl.frame.size.height+city_lbl.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        //                    address_lb.frame = CGRectMake(0, divider_2.frame.origin.y+2, [UIScreen mainScreen].bounds.size.width, divider_3.frame.origin.y-divider_2.frame.origin.y+2);
        //                }
        //
        //            }
        //            else
        //            {
        //                if (![country isEqualToString:@""]) {
        //
        //                    divider_3.frame = CGRectMake(0,  address_lb.frame.origin.y+street_number_lbl.frame.size.height+country_lbl.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        //
        //                    address_lb.frame = CGRectMake(0, divider_2.frame.origin.y+2, [UIScreen mainScreen].bounds.size.width, divider_3.frame.origin.y-divider_2.frame.origin.y+2);
        //
        //                }
        //                else
        //                {
        //                    divider_3.frame = CGRectMake(0,  address_lb.frame.origin.y+street_number_lbl.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        //
        //                    address_lb.frame = CGRectMake(0, divider_2.frame.origin.y+2, [UIScreen mainScreen].bounds.size.width, divider_3.frame.origin.y-divider_2.frame.origin.y+2);
        //
        //                }
        //
        //
        //            }
        //
        //
        //        }
        //        else
        //        {
        //
        //            if (![city isEqualToString:@""]) {
        //
        //                if (![country isEqualToString:@""]) {
        //
        //                    divider_3.frame = CGRectMake(0,  address_lb.frame.origin.y+city_lbl.frame.size.height+country_lbl.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        //
        //                    address_lb.frame = CGRectMake(0, divider_2.frame.origin.y+2, [UIScreen mainScreen].bounds.size.width, divider_3.frame.origin.y-divider_2.frame.origin.y+2);
        //
        //                }
        //                else
        //                {
        //                    divider_3.frame = CGRectMake(0,  address_lb.frame.origin.y+city_lbl.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        //
        //                    address_lb.frame = CGRectMake(0, divider_2.frame.origin.y+2, [UIScreen mainScreen].bounds.size.width, divider_3.frame.origin.y-divider_2.frame.origin.y+2);
        //
        //                }
        //
        //            }
        //            else
        //            {
        //                if (![country isEqualToString:@""]) {
        //
        //                    divider_3.frame = CGRectMake(0,  address_lb.frame.origin.y+country_lbl.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        //
        //                    address_lb.frame = CGRectMake(0, divider_2.frame.origin.y+2, [UIScreen mainScreen].bounds.size.width, divider_3.frame.origin.y-divider_2.frame.origin.y+2);
        //
        //                }
        //                else
        //                {
        //                    divider_3.frame = CGRectMake(0,  address_lb.frame.origin.y+address_lb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        //
        //                    address_lb.frame = CGRectMake(0, divider_2.frame.origin.y+2, [UIScreen mainScreen].bounds.size.width, divider_3.frame.origin.y-divider_2.frame.origin.y+2);
        //
        //                }
        //
        //
        //            }
        //
        //        }
        
        
        counter = 0;
        
        
        
        //////////////////////////////   Whether Business Button will be shown   /////////////////////////////////////////////////
        //        if ([dict_profile objectForKey:@"b_company"] != false && ![[dict_profile objectForKey:@"b_company"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_company"] length] > 1 && ![[dict_profile objectForKey:@"b_company"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"b_company"] isEqualToString:@""])
        //        {
        //            DebugLog(@"company business");
        //            nobusiness=1;
        //        }
        //        if ([dict_profile objectForKey:@"b_function"] != false && ![[dict_profile objectForKey:@"b_function"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_function"] length] > 1 && ![[dict_profile objectForKey:@"b_function"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"b_function"] isEqualToString:@""])
        //        {
        //            DebugLog(@"function business");
        //
        //            nobusiness = 1;
        //        }
        //        if ([dict_profile objectForKey:@"b_phone_num"] != false && ![[dict_profile objectForKey:@"b_phone_num"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_phone_num"] length] > 1 && ![[dict_profile objectForKey:@"b_phone_num"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"b_phone_num"] isEqualToString:@"(null)"])
        //        {
        //            DebugLog(@"phone business");
        //
        //            nobusiness=1;
        //        }
        //        if ([dict_profile objectForKey:@"b_email"] != false && ![[dict_profile objectForKey:@"b_email"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_email"] length] > 1 && ![[dict_profile objectForKey:@"b_email"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"b_email"] isEqualToString:@""])
        //        {
        //            DebugLog(@"email business");
        //
        //            nobusiness =1;
        //        }
        //        if ([dict_profile objectForKey:@"b_website"] != false && ![[dict_profile objectForKey:@"b_website"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_website"] length] > 1 && ![[dict_profile objectForKey:@"b_website"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"b_website"] isEqualToString:@""])
        //        {
        //            DebugLog(@"website business");
        //
        //            nobusiness=1;
        //        }
        //
        //        if ([dict_profile objectForKey:@"b_city"] != false && ![[dict_profile objectForKey:@"b_city"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_city"] length] >= 1 && ![[dict_profile objectForKey:@"b_city"] isEqualToString:@"0"] && ![[dict_profile objectForKey:@"b_city"] isEqualToString:@""])
        //        {
        //            DebugLog(@"city business");
        //
        //            nobusiness=1;
        //        }
        //        if ([dict_profile objectForKey:@"b_street"] != false && ![[dict_profile objectForKey:@"b_street"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_street"] length] >= 1 && ![[dict_profile objectForKey:@"b_street"] isEqualToString:@"0"] && ![[dict_profile objectForKey:@"b_street"] isEqualToString:@""])
        //        {
        //            DebugLog(@"street business");
        //
        //            nobusiness=1;
        //        }
        //        if ([dict_profile objectForKey:@"b_zipcode"] != false && ![[dict_profile objectForKey:@"b_zipcode"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_zipcode"] length] >= 1 && ![[dict_profile objectForKey:@"b_zipcode"] isEqualToString:@"0"] && ![[dict_profile objectForKey:@"b_zipcode"] isEqualToString:@""])
        //        {
        //            DebugLog(@"zipcode business");
        //
        //            nobusiness=1;
        //        }
        //        if ([dict_profile objectForKey:@"b_country"] != false && ![[dict_profile objectForKey:@"b_country"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_country"] length] >= 1 && ![[dict_profile objectForKey:@"b_country"] isEqualToString:@"0"] && ![[dict_profile objectForKey:@"b_country"] isEqualToString:@""])
        //        {
        //            DebugLog(@"country business");
        //
        //            nobusiness = 1;
        //        }
        //        if ([dict_profile objectForKey:@"b_housenumber"] != false && ![[dict_profile objectForKey:@"b_housenumber"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_housenumber"] length] >= 1 && ![[dict_profile objectForKey:@"b_housenumber"] isEqualToString:@"0"] && ![[dict_profile objectForKey:@"b_housenumber"] isEqualToString:@""])
        //        {
        //            DebugLog(@"housenumber business");
        //
        //            nobusiness = 1;
        //        }
        //
        
        ////////////////////////////////////BOXSCROLL FOR SOCIAL GOES HERE...
        
        boxscroll = [[UIScrollView alloc] init];
        
        boxscroll.delegate=self;
        
        boxscroll.scrollEnabled=YES;
        
        boxscroll.showsHorizontalScrollIndicator = NO;
        
        //    CGRect new_boxscroll_frame = boxscroll.frame;
        //    new_boxscroll_frame.origin.y = country_lbl.frame.origin.y+country_lbl.frame.size.height+10;
        //    boxscroll.frame = new_boxscroll_frame;
        boxscroll.backgroundColor =[UIColor clearColor];
        
        boxscroll.userInteractionEnabled=YES;
        
        //    boxscroll.tag = 1;
        
        //    boxscroll.pagingEnabled = YES;
        boxscroll.autoresizingMask=UIViewAutoresizingNone;
        
        
        
        twitterbt = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [twitterbt setBackgroundImage:[UIImage imageNamed:@"twitter1.png"] forState:UIControlStateNormal];
        
        [twitterbt addTarget:self action:@selector(twitterfunc:) forControlEvents:UIControlEventTouchUpInside];
        
        [boxscroll addSubview:twitterbt];
        
        
        
        pinterestbt = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [pinterestbt setBackgroundImage:[UIImage imageNamed:@"pinterest@2x1.png"] forState:UIControlStateNormal];
        
        [pinterestbt addTarget:self action:@selector(pinterestfunc:) forControlEvents:UIControlEventTouchUpInside];
        
        [boxscroll addSubview:pinterestbt];
        
        
        
        facebookbt = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [facebookbt setBackgroundImage:[UIImage imageNamed:@"facebook@2x1.png"] forState:UIControlStateNormal];
        
        [facebookbt addTarget:self action:@selector(facebookfunc:) forControlEvents:UIControlEventTouchUpInside];
        
        [boxscroll addSubview:facebookbt];
        
        
        
        gplusbt = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [gplusbt setBackgroundImage:[UIImage imageNamed:@"g+1.png"] forState:UIControlStateNormal];
        
        [gplusbt addTarget:self action:@selector(gplusfunc:) forControlEvents:UIControlEventTouchUpInside];
        
        [boxscroll addSubview:gplusbt];
        
        
        
        youtubebt = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [youtubebt setBackgroundImage:[UIImage imageNamed:@"you-tube@2x1.png"] forState:UIControlStateNormal];
        
        [youtubebt addTarget:self action:@selector(youtubefunc:) forControlEvents:UIControlEventTouchUpInside];
        
        [boxscroll addSubview:youtubebt];
        
        
        
        instagrambt = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [instagrambt setBackgroundImage:[UIImage imageNamed:@"instagram@2x1.png"] forState:UIControlStateNormal];
        
        [instagrambt addTarget:self action:@selector(instagramfunc:) forControlEvents:UIControlEventTouchUpInside];
        
        [boxscroll addSubview:instagrambt];
        
        
        linkedinbt = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [linkedinbt setBackgroundImage:[UIImage imageNamed:@"linkedin1@2x1.png"] forState:UIControlStateNormal];
        
        [linkedinbt addTarget:self action:@selector(linkedinfunc:) forControlEvents:UIControlEventTouchUpInside];
        
        [boxscroll addSubview:linkedinbt];
        
        
        
        skypebt = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [skypebt setBackgroundImage:[UIImage imageNamed:@"skype@2x1.png"] forState:UIControlStateNormal];
        
        [skypebt addTarget:self action:@selector(skypefunc:) forControlEvents:UIControlEventTouchUpInside];
        
        [boxscroll addSubview:skypebt];
        
        
        
        soundcloudbt = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [soundcloudbt setBackgroundImage:[UIImage imageNamed:@"soundcloud@2x1.png"] forState:UIControlStateNormal];
        
        [soundcloudbt addTarget:self action:@selector(soundcloudfunc:) forControlEvents:UIControlEventTouchUpInside];
        
        [boxscroll addSubview:soundcloudbt];
        
        
        
        vimeobt = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [vimeobt setBackgroundImage:[UIImage imageNamed:@"vimeo@2x1.png"] forState:UIControlStateNormal];
        
        [vimeobt addTarget:self action:@selector(vimeofunc:) forControlEvents:UIControlEventTouchUpInside];
        
        [boxscroll addSubview:vimeobt];
        
        DebugLog(@"DICT PROFILE++++++=============>%@",dict_profile);
        
        if ([dict_profile objectForKey:@"s_twitter"] != false  && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_twitter"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_twitter"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_twitter"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_twitter"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_twitter"] isEqualToString:@"(null)"])
        {
            counter += 1;
            twitterbt.frame = CGRectMake(5, 4,31.5f,31);
        }else{
            
            twitterbt.frame = CGRectMake(5, 4,0,0);
        }
        
        
        if ([dict_profile objectForKey:@"s_pinterest"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_pinterest"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_pinterest"] isEqualToString:@""]  && ![[dict_profile objectForKey:@"s_pinterest"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_pinterest"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_pinterest"] isEqualToString:@"(null)"])
        {
            counter += 1;
            pinterestbt.frame = CGRectMake(twitterbt.frame.origin.x + twitterbt.frame.size.width + 5.0f, 4,31.5f,31);
        }else{
            
            pinterestbt.frame = CGRectMake(twitterbt.frame.origin.x + twitterbt.frame.size.width, 4,0,0);
        }
        
        
        if ([dict_profile objectForKey:@"s_facebook"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_facebook"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_facebook"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_facebook"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_facebook"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_facebook"] isEqualToString:@"(null)"])
        {
            counter += 1;
            facebookbt.frame = CGRectMake(pinterestbt.frame.origin.x + pinterestbt.frame.size.width + 5.0f, 4,31.5f,31);
        }else{
            
            facebookbt.frame = CGRectMake(pinterestbt.frame.origin.x + pinterestbt.frame.size.width, 4,0,0);
        }
        
        if ([dict_profile objectForKey:@"s_google"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_google"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_google"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_google"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_google"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_google"] isEqualToString:@"(null)"])
        {
            counter += 1;
            gplusbt.frame = CGRectMake(facebookbt.frame.origin.x + facebookbt.frame.size.width + 5.0f, 4,31.5f,31);
        }else{
            
            gplusbt.frame = CGRectMake(facebookbt.frame.origin.x + facebookbt.frame.size.width, 4,0,0);
        }
        
        if ([dict_profile objectForKey:@"s_youtube"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_youtube"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_youtube"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_youtube"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_youtube"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_youtube"] isEqualToString:@"(null)"])
        {
            counter += 1;
            youtubebt.frame = CGRectMake(gplusbt.frame.origin.x + gplusbt.frame.size.width + 5.0f, 4,31.5f,31);
        }else{
            
            youtubebt.frame = CGRectMake(gplusbt.frame.origin.x + gplusbt.frame.size.width, 4,0,0);
        }
        
        
        if ([dict_profile objectForKey:@"s_instagram"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_instagram"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_instagram"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_instagram"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_instagram"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_instagram"] isEqualToString:@"(null)"])
        {
            counter += 1;
            instagrambt.frame = CGRectMake(youtubebt.frame.origin.x + youtubebt.frame.size.width + 5.0f, 4,31.5f,31);
        }else{
            
            instagrambt.frame = CGRectMake(youtubebt.frame.origin.x + youtubebt.frame.size.width , 4,0,0);
        }
        
        if ([dict_profile objectForKey:@"s_linkedin"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_linkedin"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_linkedin"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_linkedin"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_linkedin"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_linkedin"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_linkedin"] isEqualToString:@"<null>"])
        {
            counter += 1;
            linkedinbt.frame = CGRectMake(instagrambt.frame.origin.x + instagrambt.frame.size.width + 5.0f, 4,31.5f,31);
        }else{
            
            linkedinbt.frame = CGRectMake(instagrambt.frame.origin.x + instagrambt.frame.size.width, 4,0,0);
        }
        
        
        if ([dict_profile objectForKey:@"s_skype"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_skype"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_skype"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_skype"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_skype"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_skype"] isEqualToString:@"(null)"])
        {
            counter += 1;
            skypebt.frame = CGRectMake(linkedinbt.frame.origin.x + linkedinbt.frame.size.width + 5.0f, 4,31.5f,31);
        }else{
            
            skypebt.frame = CGRectMake(linkedinbt.frame.origin.x + linkedinbt.frame.size.width, 4,0,0);
        }
        
        if ([dict_profile objectForKey:@"s_soundcloud"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_soundcloud"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_soundcloud"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_soundcloud"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_soundcloud"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_soundcloud"] isEqualToString:@"(null)"])
        {
            counter += 1;
            soundcloudbt.frame = CGRectMake(skypebt.frame.origin.x + skypebt.frame.size.width + 5.0f, 4,31.5f,31);
        }else{
            
            soundcloudbt.frame = CGRectMake(skypebt.frame.origin.x + skypebt.frame.size.width, 4,0,0);
        }
        
        if ([dict_profile objectForKey:@"s_vimeo"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_vimeo"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_vimeo"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_vimeo"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_vimeo"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_vimeo"] isEqualToString:@"(null)"])
        {
            counter += 1;
            vimeobt.frame = CGRectMake(soundcloudbt.frame.origin.x + soundcloudbt.frame.size.width + 5.0f, 4,31.5f,31);
        }else{
            
            vimeobt.frame = CGRectMake(soundcloudbt.frame.origin.x + soundcloudbt.frame.size.width, 4,0,0);
        }
        
        
        boxscroll.contentSize = CGSizeMake(counter*36.5f, 40);
        
        //     CGRect new_boxscroll_frame1 = boxscroll.frame;
        //    new_boxscroll_frame1.origin.y = country_lbl.frame.origin.y+country_lbl.frame.size.height+10;
        //    boxscroll.frame = new_boxscroll_frame1;
        
        
        [boxscroll removeFromSuperview];
        [mainview bringSubviewToFront:boxscroll];
        //    removeconbt = [UIButton buttonWithType:UIButtonTypeCustom];
        
        if (boxscroll.contentSize.width >= 300) {
            
            DebugLog(@"BOXSCROLL CONTENT >=300");
            DebugLog(@"boxscroll framesize===>%f",divider_3.frame.origin.y+divider_3.frame.size.height+10);
            //
            boxscroll.frame =CGRectMake(10,divider_3.frame.origin.y+divider_3.frame.size.height, [UIScreen mainScreen].bounds.size.width,40.0f);
            [mainscroll addSubview:boxscroll];
            
            divider_5.frame = CGRectMake(0, boxscroll.frame.origin.y + boxscroll.frame.size.height+6, [UIScreen mainScreen].bounds.size.width, 0.5f);
            [mainscroll addSubview:divider_5];
            
            //
            //        toggleimage.frame= CGRectMake(66, boxscroll.frame.origin.y + boxscroll.frame.size.height+10, 188, 28.5f);  //115, 136.5f-60, 188.5f, 28.5f
            //        togglebt.frame = toggleimage.frame;
            //
            //        if ([user_id intValue] != [[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]intValue])
            //        {
            //            divider_5.frame = CGRectMake(0,  toggleimage.frame.origin.y+toggleimage.frame.size.height+10, [UIScreen mainScreen].bounds.size.width, 0.7f);
            //            [mainscroll addSubview:divider_5];
            ////            removeconbt.frame = CGRectMake(70, divider_5.frame.origin.y + divider_5.frame.size.height+10, 180, 30);
            //
            //            removeconbt.frame = CGRectMake(70, toggleimage.frame.origin.y + toggleimage.frame.size.height+20, 180, 30);
            //            removeconbt.layer.cornerRadius=5;
            //            removeconbt.titleLabel.font=[UIFont systemFontOfSize:16];
            //            [removeconbt setTitle:@"Remove Connection" forState:UIControlStateNormal];
            //        [removeconbt setTitleColor:[UIColor colorWithRed:141.0f/255.0f green:44.0f/255.0f blue:41.0f/255.0f alpha:1] forState:UIControlStateNormal];
            //            removeconbt.backgroundColor=[UIColor clearColor];
            //            [removeconbt addTarget:self action:@selector(removeConnection:) forControlEvents:UIControlEventTouchUpInside];
            ////            removeconbt.layer.borderColor=[[UIColor colorWithRed:141.0f/255.0f green:44.0f/255.0f blue:41.0f/255.0f alpha:1]CGColor];
            ////            removeconbt.layer.borderWidth= 2;
            //            [mainscroll addSubview:removeconbt];
            //        }
            //        else
            //        {
            //            removeconbt.frame = CGRectMake(70, boxscroll.frame.origin.y + boxscroll.frame.size.height+10, 0, 0);
            //        }
            mainscroll.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, divider_5.frame.origin.y + divider_5.frame.size.height+35.0f);
            //
        }else{
            
            DebugLog(@"BOXSCROLL CONTENT <=300 ---- ELSE part");
            
            //        boxscroll.frame = CGRectMake((310 - boxscroll.contentSize.width)/2,divider_3.frame.origin.y+divider_3.frame.size.height+38, 300,40.0f);
            boxscroll.frame = CGRectMake((310 - boxscroll.contentSize.width)/2,divider_3.frame.origin.y+divider_3.frame.size.height, 300,40.0f);
            [mainscroll addSubview:boxscroll];
            
            divider_5.frame = CGRectMake(0,boxscroll.frame.origin.y + boxscroll.frame.size.height+6, [UIScreen mainScreen].bounds.size.width, 0.5f);
            [mainscroll addSubview:divider_5];
            //
            //        toggleimage.frame= CGRectMake(66, boxscroll.frame.origin.y + boxscroll.frame.size.height+10, 188, 28.5f);  //115, 136.5f-60, 188.5f, 28.5f
            //        togglebt.frame = toggleimage.frame;
            //
            //        if ([user_id intValue] != [[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]intValue])
            //        {
            //            divider_5.frame = CGRectMake(0,  toggleimage.frame.origin.y+toggleimage.frame.size.height+10, [UIScreen mainScreen].bounds.size.width, 0.7f);
            //            [mainscroll addSubview:divider_5];
            //
            //            removeconbt.frame = CGRectMake(70, toggleimage.frame.origin.y + toggleimage.frame.size.height+20, 180, 30);
            //            removeconbt.layer.cornerRadius=5;
            //            removeconbt.titleLabel.font=[UIFont systemFontOfSize:16];
            //            [removeconbt setTitle:@"Remove Connection" forState:UIControlStateNormal];
            //        [removeconbt setTitleColor:[UIColor colorWithRed:141.0f/255.0f green:44.0f/255.0f blue:41.0f/255.0f alpha:1] forState:UIControlStateNormal];
            //            removeconbt.backgroundColor=[UIColor clearColor];
            //            [removeconbt addTarget:self action:@selector(removeConnection:) forControlEvents:UIControlEventTouchUpInside];
            //            [mainscroll addSubview:removeconbt];
            //        }
            //        else
            //        {
            //            removeconbt.frame = CGRectMake(70, boxscroll.frame.origin.y + boxscroll.frame.size.height+10, 0, 0);
            //        }
            mainscroll.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, divider_5.frame.origin.y + divider_5.frame.size.height+35.0f);
            //        DebugLog(@"mainscroll contentsize: %f",mainscroll.contentSize.height);
        }
        DebugLog(@"divider frame: %@",NSStringFromCGRect(divider_3.frame));
        DebugLog(@"boxscroll frame: %@",NSStringFromCGRect(boxscroll.frame));
        DebugLog(@"divider ends at: %f",divider_3.frame.origin.y+divider_3.frame.size.height);
        //
        if (counter != 0)
        {
            DebugLog(@"COUNTER========================>%d",counter);
            divider_5.frame = CGRectMake(0,  boxscroll.frame.origin.y+boxscroll.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
            [mainscroll addSubview:divider_5];
        }
        else
        {
            divider_5.frame = CGRectMake(0,  boxscroll.frame.origin.y+boxscroll.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
            [mainscroll addSubview:divider_5];
        }
        
        
        
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        if (switchprof == 0)
        {
            phonearr= [[NSMutableArray alloc] init];
            if ([dict_profile objectForKey:@"mobile_num"] != false && ![[dict_profile objectForKey:@"mobile_num"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"mobile_num"] length] > 1 && ![[dict_profile objectForKey:@"mobile_num"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"mobile_num"] isEqualToString:@""] && ![[dict_profile objectForKey:@"mobile_num"] isKindOfClass:[NSNull class]])
            {
                mobile_num1= [NSString stringWithFormat:@"%@%@",[dict_profile objectForKey:@"mobile_pre"],[dict_profile objectForKey:@"mobile_num"]];
                [phonearr addObject:mobile_num1];
            }
            
            else if ([dict_profile objectForKey:@"phone_num"] != false && ![[dict_profile objectForKey:@"phone_num"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"phone_num"] length] > 1 && ![[dict_profile objectForKey:@"phone_num"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"phone_num"] isEqualToString:@""] && ![[dict_profile objectForKey:@"phone_num"] isKindOfClass:[NSNull class]])
            {
                landline_num1= [NSString stringWithFormat:@"%@%@",[dict_profile objectForKey:@"phone_pre"],[dict_profile objectForKey:@"phone_num"]];
                [phonearr addObject:landline_num1];
            }
        }
        else
        {
            if ([dict_profile objectForKey:@"b_phone_num"] != false && ![[dict_profile objectForKey:@"b_phone_num"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_phone_num"] length] > 1 && ![[dict_profile objectForKey:@"b_phone_num"] isEqualToString:@"(null)"])
            {
                business_num1= [NSString stringWithFormat:@"%@%@",[dict_profile objectForKey:@"b_phone_pre"],[dict_profile objectForKey:@"b_phone_num"]];
                [phonearr addObject:business_num1];
            }
        }
        
        
        DebugLog(@"nobusiness: %d",nobusiness);
        if (nobusiness == 0)
        {
            //        [togglebt removeFromSuperview];
            //            [divider_5 removeFromSuperview];
        }
        
        
        
        
        
    }
    
    else            //BUSINESS STARTS HERE======================================>
    {
        //
        //        companyimg.hidden=NO;
        //        companylb.hidden=NO;
        //        desigimg.hidden=NO;
        //        desiglb.hidden=NO;
        //
        //        [divider_0 removeFromSuperview];
        //
        //
        //        if ([dict_profile objectForKey:@"b_company"] != false && ![[dict_profile objectForKey:@"b_company"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_company"] length] > 1 && ![[dict_profile objectForKey:@"b_company"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"b_company"] isEqualToString:@""]){
        //
        //            nobusiness=1;
        //            company= [NSString stringWithFormat:@"%@",[[dict_profile objectForKey:@"b_company"] stringByReplacingOccurrencesOfString:@"amp;" withString:@""]];
        //
        //            companyimg.frame = CGRectMake(15, businesstext.frame.origin.y+businesstext.frame.size.height+21, 14, 13.5f);
        //            companyimg.image = [UIImage imageNamed:@"company"];
        //
        //            companylb.frame = CGRectMake(38, businesstext.frame.origin.y+businesstext.frame.size.height+19, 275, 17.5f);
        //
        //            divider_9.frame = CGRectMake(0,  companylb.frame.origin.y+companylb.frame.size.height+13, [UIScreen mainScreen].bounds.size.width, 0.5f);
        //            y1=divider_9.frame.size.height+divider_9.frame.origin.y;
        //        }
        //        else{
        //            company = @"";
        //
        //            companyimg.frame = CGRectMake(15, businesstext.frame.origin.y+businesstext.frame.size.height+21, 14, 0);
        //
        //
        //            companylb.frame = CGRectMake(38, businesstext.frame.origin.y+businesstext.frame.size.height+19, 275, 0);
        //
        //            divider_9.frame = CGRectMake(0,  companylb.frame.origin.y+companylb.frame.size.height+13, [UIScreen mainScreen].bounds.size.width, 0);
        //        }
        //
        //        //===============BUSI-DESIGNATION==============//
        //
        //
        //        if ([dict_profile objectForKey:@"b_function"] != false && ![[dict_profile objectForKey:@"b_function"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_function"] length] > 1 && ![[dict_profile objectForKey:@"b_function"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"b_function"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"b_function"] isEqualToString:@""])
        //        {
        //
        //            desig= [NSString stringWithFormat:@"%@",[[dict_profile objectForKey:@"b_function"] stringByReplacingOccurrencesOfString:@"amp;" withString:@""]];
        //            DebugLog(@"company bhaswar: %@",desig);
        //
        //            desigimg.frame = CGRectMake(15, divider_9.frame.origin.y+divider_9.frame.size.height+12, 15, 15);
        //            desigimg.image = [UIImage imageNamed:@"designation"];
        //
        //
        //            desiglb.frame = CGRectMake(38, divider_9.frame.origin.y+divider_9.frame.size.height+12, 275, 17.5f);
        //            nobusiness=1;
        //
        //            divider_0 = [[UIView alloc]init];//WithFrame:CGRectMake(14.5f, 248-60, 291, 1)];
        //
        //            //    divider_1.image=[UIImage imageNamed:@"divider-prof.png"];
        //
        //            [mainscroll addSubview:divider_0];
        //
        //            divider_0.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
        //
        //
        //            divider_0.frame = CGRectMake(0,  desiglb.frame.origin.y+desiglb.frame.size.height+13, [UIScreen mainScreen].bounds.size.width, 0.5f);
        //            y1=divider_0.frame.size.height+divider_0.frame.origin.y;
        //        }
        //        else{
        //            desig=@"";
        //
        //            desigimg.frame = CGRectMake(15, divider_9.frame.origin.y+divider_9.frame.size.height+12, 15, 0);
        //
        //            desiglb.frame = CGRectMake(38, divider_9.frame.origin.y+divider_9.frame.size.height+12, 275, 0);
        //
        //            divider_0 = [[UIView alloc]init];//WithFrame:CGRectMake(14.5f, 248-60, 291, 1)];
        //
        //            //    divider_1.image=[UIImage imageNamed:@"divider-prof.png"];
        //
        //            [mainscroll addSubview:divider_0];
        //
        //            divider_0.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
        //
        //
        //            divider_0.frame = CGRectMake(0,  desiglb.frame.origin.y+desiglb.frame.size.height+13, [UIScreen mainScreen].bounds.size.width, 0);
        //        }
        //
        //
        //        //=================BUSI-MOBILE================//
        //
        //        if ([dict_profile objectForKey:@"b_phone_num"] != false && ![[dict_profile objectForKey:@"b_phone_num"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_phone_num"] length] > 1 && ![[dict_profile objectForKey:@"b_phone_num"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"b_phone_num"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"b_phone_num"] isEqualToString:@""]){
        //
        //
        //
        //            mobile = [NSString stringWithFormat:@"%@%@",[dict_profile objectForKey:@"b_phone_pre"],[dict_profile objectForKey:@"b_phone_num"]];
        //
        //            DebugLog(@"LANDLINE:%@",landline);
        //
        //            [landphone_img setHidden:YES];
        //
        //            [landphonelb setHidden:YES];
        //
        //            [mobileno setHidden:NO];
        //
        //            mobileimg.frame = CGRectMake(13, 22, [UIImage imageNamed:@"mobile_profile"].size.width/1.5,[UIImage imageNamed:@"mobile_profile"].size.height/1.5); //14, 18);
        //
        //            mobileno.frame = CGRectMake(0, divider_0.frame.origin.y+divider_0.frame.size.height, [UIScreen mainScreen].bounds.size.width, [UIImage imageNamed:@"mobile_profile"].size.height+30);
        //
        //
        //            divider_10 = [[UIView alloc]init];//WithFrame:CGRectMake(14.5f, mail_lb.frame.origin.y+mail_lb.frame.size.height+15, 291, 1)];
        //
        //            [divider_10 setBackgroundColor:[UIColor colorWithRed:(239.0f / 255.0f) green:(239.0f / 255.0f) blue:(239.0f / 255.0f) alpha:1.0f]];
        //
        //            [mainscroll addSubview:divider_10];
        //
        //
        //            divider_10.frame = CGRectMake(0,  mobileno.frame.origin.y+mobileno.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.7f);
        //            y1=divider_10.frame.size.height+divider_10.frame.origin.y;
        //
        //
        //            nobusiness=1;
        //
        //        }
        //
        //        else{
        //            mobile=@"";
        //
        //            [mobileimg setHidden:YES];
        //
        //            // mobileimg.frame = CGRectMake(13, 15, 14, 0);
        //
        //            mobileno.frame = CGRectMake(0, divider_0.frame.origin.y+divider_0.frame.size.height, 320, 0);
        //
        //            divider_10 = [[UIView alloc]init];//WithFrame:CGRectMake(14.5f, mail_lb.frame.origin.y+mail_lb.frame.size.height+15, 291, 1)];
        //            [divider_10 setBackgroundColor:[UIColor colorWithRed:(239.0f / 255.0f) green:(239.0f / 255.0f) blue:(239.0f / 255.0f) alpha:1.0f]];
        //            [mainscroll addSubview:divider_10];
        //
        //            divider_10.frame = CGRectMake(0,  mobileno.frame.origin.y+mobileno.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        //
        //            //            landphone_img.frame = CGRectMake(13, 15, 14, 0);
        //            //
        //            //            landphonelb.frame = CGRectMake(0, divider_10.frame.origin.y+divider_10.frame.size.height, [UIImage imageNamed:@"detbuttonNormal"].size.width, 0);
        //        }
        //        //Divider 1===============|
        //
        //        if (![landline isEqualToString:@""]) {
        //
        //            divider_1.frame = CGRectMake(0,  landphonelb.frame.origin.y+landphonelb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        //            y1=divider_1.frame.size.height+divider_1.frame.origin.y;
        //        }
        //        else {
        //
        //            divider_1.frame = CGRectMake(0,  landphonelb.frame.origin.y+landphonelb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        //        }
        //
        //        //============BUSI-MAIL===========//
        //
        //
        //        if ([dict_profile objectForKey:@"b_email"] != false && ![[dict_profile objectForKey:@"b_email"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_email"] length] > 1 && ![[dict_profile objectForKey:@"b_email"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"b_email"] isEqualToString:@""])
        //        {
        //            mail= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_email"]];
        //
        //            [mail_lb setHidden:NO];
        //
        //            mailimg.frame= CGRectMake(10, 20, [UIImage imageNamed:@"email_profile"].size.width/1.5, [UIImage imageNamed:@"email_profile"].size.height/1.5);
        //            //            mailimg.frame = CGRectMake(13, 18, 14.5f, 14);
        //
        //            mail_lb.frame = CGRectMake(0, divider_10.frame.origin.y+divider_10.frame.size.height, [UIScreen mainScreen].bounds.size.width, [UIImage imageNamed:@"detbuttonNormal"].size.height+10);
        //
        //            mailback.frame = CGRectMake(0, divider_1.frame.origin.y+divider_1.frame.size.height, [UIScreen mainScreen].bounds.size.width, 43);
        //            nobusiness=1;
        //
        //        }
        //        else{
        //
        //            mail= @"";
        //
        //            [mailimg setHidden:YES];
        //
        //            mailimg.frame= CGRectMake(10, 20, [UIImage imageNamed:@"email_profile"].size.width/1.5, 0);
        //            //            mailimg.frame = CGRectMake(13, 18, 14.5f, 14);
        //
        //            mail_lb.frame = CGRectMake(0, divider_10.frame.origin.y+divider_10.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        //
        //            mailback.frame = CGRectMake(0, divider_1.frame.origin.y+divider_1.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        //
        //        }
        //
        //        //        if ([dict_profile objectForKey:@"b_website"] != false && ![[dict_profile objectForKey:@"b_website"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_website"] length] > 1 && ![[dict_profile objectForKey:@"b_website"] isEqualToString:@"(null)"])
        //        //        {
        //        //            website= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_website"]];
        //        //
        //        //            [website_lb setHidden:NO];
        //        //
        //        //            website_img.frame = CGRectMake(15, mail_lb.frame.origin.y+mail_lb.frame.size.height+10, 14.5f, 14);
        //        //
        //        //            website_lb.frame = CGRectMake(38, mail_lb.frame.origin.y+mail_lb.frame.size.height+8.5f, 275, 15);
        //        //            nobusiness=1;
        //        //        }else{
        //        //
        //        //            website=@"";
        //        //
        //        //            website_img.frame = CGRectMake(15, mail_lb.frame.origin.y+mail_lb.frame.size.height+10, 16, 0);
        //        //
        //        //            website_lb.frame = CGRectMake(38, mail_lb.frame.origin.y+mail_lb.frame.size.height+8.5f, 275, 0);
        //        //
        //        //            //            divider_2.frame = CGRectMake(14.5f, website_lb.frame.origin.y+website_lb.frame.size.height+11, 291, 0);
        //        //        }
        //
        //        //Divider 2================================>
        //
        //        if (![mail isEqualToString:@""]){   //&& ![website isEqualToString:@""]) {
        //
        //            divider_2.frame = CGRectMake(0,  mail_lb.frame.origin.y+mail_lb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        //            y1=divider_2.frame.origin.y+divider_2.frame.size.height;
        //        }
        //
        //        //        else if (![website isEqualToString:@""]) {
        //        //
        //        //            divider_2.frame = CGRectMake(14.5f,  website_lb.frame.origin.y+website_lb.frame.size.height+16, 291, 0.5f);
        //        //        }
        //        else {
        //
        //            divider_2.frame = CGRectMake(0,  mail_lb.frame.origin.y+mail_lb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        //        }
        //
        //        if ([dict_profile objectForKey:@"b_city"] != false && ![[dict_profile objectForKey:@"b_city"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_city"] length]!=0 && ![[dict_profile objectForKey:@"b_city"] isEqualToString:@""])
        //        {
        //            city= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_city"]];
        //            nobusiness=1;
        //        }
        //
        //        else
        //            city=@"";
        //
        //        if ([dict_profile objectForKey:@"b_street"] != false && ![[dict_profile objectForKey:@"b_street"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_street"] length]!=0 && ![[dict_profile objectForKey:@"b_street"] isEqualToString:@""])
        //        {
        //            street= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_street"]];
        //            nobusiness=1;
        //        }
        //
        //        else
        //            street =@"";
        //
        //        if ([dict_profile objectForKey:@"b_zipcode"] != false && ![[dict_profile objectForKey:@"b_zipcode"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_zipcode"] length]!=0 && ![[dict_profile objectForKey:@"b_zipcode"] isEqualToString:@""])
        //
        //        {
        //            nobusiness=1;
        //            zipcode= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_zipcode"]];
        //
        //        }
        //        else
        //            zipcode=@"";
        //
        //        if ([dict_profile objectForKey:@"b_country"] != false && ![[dict_profile objectForKey:@"b_country"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_country"] length]!=0 && ![[dict_profile objectForKey:@"b_country"] isEqualToString:@""])
        //        {
        //            country= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_country"]];
        //            nobusiness=1;
        //        }
        //        else
        //            country=@"";
        //        if ([dict_profile objectForKey:@"b_housenumber"] != false && ![[dict_profile objectForKey:@"b_housenumber"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_housenumber"] length]!=0 && ![[dict_profile objectForKey:@"b_housenumber"] isEqualToString:@""])
        //        {
        //            number= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_housenumber"]];
        //            nobusiness=1;
        //        }
        //        else
        //            number= @"";
        //
        //
        //        if (![city  isEqualToString: @""] || ![street  isEqualToString: @""] || ![zipcode  isEqualToString: @""] || ![country  isEqualToString: @""] || ![number isEqualToString:@""]) {
        //
        //            nobusiness=1;
        //            NSString *line1,*line2,*line3;
        //
        //            y=0;
        //            if ([street length]!=0 && [number length]!=0)
        //            {
        //                line1= [NSString stringWithFormat:@"%@, %@,",street,number];
        //            }
        //            else if ([street length]==0 && [number length]!=0) {
        //                line1= [NSString stringWithFormat:@"%@,",number];
        //            }
        //            else if ([street length]!=0 && [number length]==0){
        //                line1 = [NSString stringWithFormat:@"%@,",street];
        //            }
        //            else{
        //                line1= @"";
        //            }
        //
        //            if ([zipcode length]!=0 && [city length]!=0) {
        //                line2 = [NSString stringWithFormat:@"%@, %@,",zipcode,city];
        //            }
        //            else if ([zipcode length]==0 && [city length]!=0)
        //            {
        //                line2 = [NSString stringWithFormat:@"%@,",city];
        //            }
        //            else if ([zipcode length]!=0 && [city length]==0)
        //            {
        //                line2 = [NSString stringWithFormat:@"%@,",zipcode];
        //            }
        //            else
        //            {
        //                line2 = @"";
        //            }
        //
        //            if ([country length]!=0)
        //            {
        //                line3 = [NSString stringWithFormat:@"%@",country];
        //            }
        //            else
        //            {
        //                line3 = @"";
        //            }
        //
        //            if ([line1 length]==0 && [line2 length]==0 && [line3 length]!=0) {
        //                businessAddress= [NSString stringWithFormat:@"%@",line3];
        //                y=30;
        //
        //            }
        //            else if ([line1 length]==0 && [line2 length]!=0 && [line3 length]!=0)
        //            {
        //                businessAddress=[NSString stringWithFormat:@"%@ \n %@",line2,line3];
        //                y=60;
        //            }
        //            else if ([line1 length]!=0 && [line2 length]==0 && [line3 length]!=0)
        //            {
        //                businessAddress= [NSString stringWithFormat:@"%@\n %@",line1,line3];
        //                y=60;
        //            }
        //            else if ([line1 length]==0 && [line2 length]==0 && [line3 length]==0)
        //            {
        //                businessAddress=[NSString stringWithFormat:@"%@",line2];
        //                y=30;
        //            }
        //            else if ([line1 length]!=0 && [line2 length]!=0 && [line3 length]==0)
        //            {
        //                businessAddress=[NSString stringWithFormat:@"%@ \n %@",line1,line2];
        //                y=60;
        //            }
        //            else if ([line1 length]!=0 && [line2 length]==0 && [line3 length]==0)
        //            {
        //                businessAddress=[NSString stringWithFormat:@"%@",line1];
        //                y=30;
        //            }
        //            else
        //            {
        //                businessAddress=[NSString stringWithFormat:@"%@ \n %@\n %@",line1,line2,line3];
        //                y=90;
        //            }
        //
        //            //            businessAddress = [NSString stringWithFormat:@"%@, %@,\n %@, %@,\n %@",street,number,zipcode,city,country];
        //
        //            address_img.frame = CGRectMake(10, 10,[UIImage imageNamed:@"map_profile"].size.width/1.5,[UIImage imageNamed:@"map_profile"].size.height/1.5);
        //            //            address_img.frame = CGRectMake(13, 4,12,17);
        //
        //            //===========Dynamic height for business address=========//
        //
        //            CGSize maximumLabelSize = CGSizeMake(296, FLT_MAX);
        //
        //            CGSize expectedLabelSize = [businessAddress sizeWithFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17] constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByWordWrapping];
        //
        //            CGRect newFrame = CGRectMake(0, y1+1, [UIImage imageNamed:@"detbuttonNormal"].size.width,[UIImage imageNamed:@"detbuttonNormal"].size.height); //divider_2.frame.origin.y+1,[UIImage imageNamed:@"detbuttonNormal"].size.width,[UIImage imageNamed:@"detbuttonNormal"].size.height);
        //            DebugLog(@"new frame: %@",NSStringFromCGRect(newFrame));
        //            newFrame.size.height = expectedLabelSize.height;
        //            DebugLog(@"%f", expectedLabelSize.height);
        //            address_lb.frame = CGRectMake(newFrame.origin.x, newFrame.origin.y, newFrame.size.width,y); //newFrame.size.height+20.0);
        //            DebugLog(@"new frame1: %@",NSStringFromCGRect(address_lb.frame));
        //
        ////            address_lb.backgroundColor=[UIColor redColor];
        ////            [address_lb sizeToFit];
        //
        //            //            addressback.frame = CGRectMake(0, divider_2.frame.origin.y+divider_2.frame.size.height, [UIScreen mainScreen].bounds.size.width, 90);
        //
        //        }else{
        //            address_img.frame = CGRectMake(13, 4,12,0);
        //
        //            address_lb.frame = CGRectMake(0, y1+1, [UIImage imageNamed:@"detbuttonNormal"].size.width, 0);
        ////            address_lb.backgroundColor=[UIColor redColor];
        //        }
        //
        //        if (businessAddress != false && ![businessAddress isEqualToString:@"(null)"] && ![businessAddress isEqualToString:@""]) {
        //
        //            [address_lb setTitle:[NSString stringWithFormat:@"%@", businessAddress] forState:UIControlStateNormal];
        //            divider_3 = [[UILabel alloc] init];
        //                        divider_3.frame = CGRectMake(0,  address_lb.frame.origin.y+address_lb.frame.size.height+10, [UIScreen mainScreen].bounds.size.width, 0.5f);
        ////            divider_3.frame = CGRectMake(0,  y+y1, [UIScreen mainScreen].bounds.size.width, 0.5f);//address_lb.frame.origin.y+street_number_lbl.frame.size.height+city_lbl.frame.size.height+country_lbl.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        //            //            divider_3.frame = CGRectMake(0,  300, [UIScreen mainScreen].bounds.size.width, 1.0f);//address_lb.frame.origin.y+street_number_lbl.frame.size.height+city_lbl.frame.size.height+country_lbl.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        //
        ////            divider_3.backgroundColor=[UIColor blueColor];
        //            [mainscroll addSubview:divider_3];
        //            // DebugLog(@"ADDRESS IF PART==========>");
        //
        //            DebugLog(@"ADD1=====================> %@", businessAddress);
        //        }else {
        //
        //            divider_3.frame = CGRectMake(0,  address_lb.frame.origin.y+street_number_lbl.frame.size.height+city_lbl.frame.size.height+country_lbl.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        //
        //            DebugLog(@"ADD1=====================ELSE> %@", businessAddress);
        //        }
        //
        
        [mobileno setHidden:YES];
        [mail_lb setHidden:YES];
        [address_lb setHidden:YES];
        [boxscroll setHidden:YES];
        
        [b_boxscroll removeFromSuperview];
        
        [self loadBusinessView];
        counter= 0 ;
    }
    
    
    // mobileno.text= mobile;
    [mobileno setTitle:[NSString stringWithFormat:@"%@", mobile] forState:UIControlStateNormal];
    
    //    landphonelb.text= landline;
    [landphonelb setTitle:[NSString stringWithFormat:@"%@", landline] forState:UIControlStateNormal];
    
    //    mail_lb.text= mail;
    [mail_lb setTitle:[NSString stringWithFormat:@"%@", mail] forState:UIControlStateNormal];
    
    website_lb.text= website;
    if (switchprof ==0){
        
        [self setAddress];
        
        //        address_lb.text=address;
        //[address_lb setTitle:[NSString stringWithFormat:@"%@", address] forState:UIControlStateNormal];
        
        //        if ([street isEqualToString: @""] && [number isEqualToString: @""])
        //        {
        //
        ////            if ([zipcode isEqualToString: @""])
        ////            {
        ////                if ([city isEqualToString:@""]) {
        ////                    country_lbl.frame = street_number_lbl.frame;
        ////                    country_lbl.text = [NSString stringWithFormat:@"%@",country];
        ////                }
        ////                else
        ////                {
        ////                    city_lbl.frame = street_number_lbl.frame;
        ////                    country_lbl.frame = zipcode_lbl.frame;
        ////                    city_lbl.text = [NSString stringWithFormat:@"%@,",city];
        ////                    country_lbl.text = [NSString stringWithFormat:@"%@",country];
        ////                }
        ////            }
        ////
        ////            else
        ////            {
        ////                //country_lbl.frame = zipcode_lbl.frame;
        ////
        ////                zipcode_lbl.frame = street_number_lbl.frame;
        ////
        ////
        ////
        ////                zipcode_lbl.text = [NSString stringWithFormat:@"%@,",zipcode];
        ////                [zipcode_lbl sizeToFit];
        ////
        ////                CGRect new_frame = city_lbl.frame;
        ////                new_frame.origin.y = zipcode_lbl.frame.origin.y-3;
        ////                new_frame.origin.x = zipcode_lbl.frame.origin.x+zipcode_lbl.frame.size.width+5;
        ////                city_lbl.frame = new_frame;
        ////
        ////                CGRect new_country_frame = country_lbl.frame;
        ////                new_country_frame.origin.y = zipcode_lbl.frame.origin.y+zipcode_lbl.frame.size.height;
        ////                country_lbl.frame = new_country_frame;
        ////
        ////                city_lbl.text = [NSString stringWithFormat:@"%@,",city];
        ////                country_lbl.text = [NSString stringWithFormat:@"%@",country];
        ////            }
        ////
        //
        //     }
        //        else
        //        {
        //
        ////            if ([zipcode isEqualToString: @""]) {
        ////
        ////                if ([city isEqualToString:@""]) {
        ////                    country_lbl.frame = zipcode_lbl.frame;
        ////                    street_number_lbl.text =[NSString stringWithFormat:@"%@\n, %@",street,number];
        ////                    country_lbl.text = [NSString stringWithFormat:@"%@",country];
        ////                }
        ////                else
        ////                {
        ////                    city_lbl.frame = zipcode_lbl.frame;
        ////                    street_number_lbl.text =[NSString stringWithFormat:@"%@\n, %@",street,number];
        ////
        ////                    city_lbl.text = [NSString stringWithFormat:@"%@,",city];
        ////                    country_lbl.text = [NSString stringWithFormat:@"%@",country];
        ////                }
        ////            }
        ////
        ////            else
        ////            {
        ////                street_number_lbl.text =[NSString stringWithFormat:@"%@\n, %@",street,number];
        ////                zipcode_lbl.text = [NSString stringWithFormat:@"%@,",zipcode];
        ////                city_lbl.text = [NSString stringWithFormat:@"%@,",city];
        ////                country_lbl.text = [NSString stringWithFormat:@"%@",country];
        ////            }
        //
        //        }
        
    }
    
    else{
        //        [street_number_lbl removeFromSuperview];
        //        [zipcode_lbl removeFromSuperview];
        //        [city_lbl removeFromSuperview];
        //        [country_lbl removeFromSuperview];
        //        [divider_3 removeFromSuperview];
        //        address_lb.text=businessAddress;
        //[address_lb setTitle:[NSString stringWithFormat:@"%@", businessAddress] forState:UIControlStateNormal];
        //        divider_3.frame = CGRectMake(0,  address_lb.frame.origin.y+address_lb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        
        
        
    }
    
    companylb.text = company;
    desiglb.text=desig;
    
    
}


-(void)twitterfunc: (id)sender
{
    NSURL *urlApp = [NSURL URLWithString: [NSString stringWithFormat:@"twitter://user?screen_name=%@", [dict_profile objectForKey:@"s_twitter"]]];
    
    if ([[UIApplication sharedApplication] canOpenURL:urlApp]) {
        
        [[UIApplication sharedApplication] openURL:urlApp];
    } else {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://twitter.com/%@",[dict_profile objectForKey:@"s_twitter"]]]];
        DebugLog(@"TWIT URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"http://twitter.com/%@",[dict_profile objectForKey:@"s_twitter"]]]);
    }
}


-(void)pinterestfunc: (id)sender
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"pinterest://user/%@",[dict_profile objectForKey:@"s_pinterest"]]]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"pinterest://user/%@",[dict_profile objectForKey:@"s_pinterest"]]]];
    } else {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://pinterest.com"]];
        DebugLog(@"PIN URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"http://pinterest.com/%@",[dict_profile objectForKey:@"s_pinterest"]]]);
    }
}



-(void)facebookfunc: (id)sender
{
    
    NSString *fb_app_str = [[NSString alloc]init];
    
    fb_app_str = [dict_profile objectForKey:@"s_facebook"];
    
//    NSString *string1=[NSString
//                       stringWithFormat:@"http://graph.facebook.com/%@",[dict_profile objectForKey:@"s_facebook"]];
//    
//    DebugLog(@"facebook graph %@",string1);
//    
//    NSError *localErr;
//    
//    
//    
//    NSData *result =  [NSData dataWithContentsOfURL:[NSURL URLWithString:string1] options:NSDataReadingUncached error:&localErr];
//    
//    
//    
//    if (result != nil)
//        
//    {
//        
//        NSDictionary *graphjson =[NSJSONSerialization JSONObjectWithData:result options:0 error:&localErr];
//        
//        if (![graphjson isKindOfClass:[NSNull class]])
//            
//        {
//            
//            if ([[graphjson allKeys]containsObject:@"id"])
//                
//            {
//                
//                fb_app_str = [graphjson objectForKey:@"id"];
//                
//            }
//            
//        }
//        
//    }
//    
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"fb://profile/%@",fb_app_str]];  //[dict_profile objectForKey:@"s_facebook"]
    
    [[UIApplication sharedApplication] openURL:url];
    
    
    
    if ([[UIApplication sharedApplication] canOpenURL:url]){
        
        [[UIApplication sharedApplication] openURL:url];
        
    }
    
    else {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://facebook.com/%@",[dict_profile objectForKey:@"s_facebook"]]]];  //100001925673906
        
        DebugLog(@"http://facebook.com/%@",[dict_profile objectForKey:@"s_facebook"]);
        
    }
    
}



-(void)gplusfunc: (id)sender
{
    NSString *string = [dict_profile objectForKey:@"s_google"];
    NSArray *stringArray = [string componentsSeparatedByString: @"com/"];
    NSString *gplusstr = [stringArray objectAtIndex:1];
    NSURL *GooglePlus = [NSURL URLWithString:[NSString stringWithFormat:@"gplus://plus.google.com/%@",gplusstr]];
    DebugLog(@"GOO+ URL=============>%@",GooglePlus);
    
    if ([[UIApplication sharedApplication] canOpenURL:GooglePlus])  //[NSURL URLWithString:@"gplus://plus.google.com/u/0/105819873801211194735/"]
        [[UIApplication sharedApplication] openURL:GooglePlus];
    else
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://plus.google.com/%@",gplusstr]]];
}


-(void)youtubefunc: (id)sender
{
    NSString* social   = [NSString stringWithFormat:@"http://www.youtube.com/user/%@",[dict_profile objectForKey:@"s_youtube"]];
    NSString* socialApp = [NSString stringWithFormat:@"youtube://user/%@",[dict_profile objectForKey:@"s_youtube"]];
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:socialApp]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:socialApp]];
        
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:social]];
    }
}



-(void)instagramfunc: (id)sender
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"instagram://user?username=%@",[dict_profile objectForKey:@"s_instagram"]]]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"instagram://user?username=%@",[dict_profile objectForKey:@"s_instagram"]]]];
        
        DebugLog(@"INSTA URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"instagram://user?username=%@",[dict_profile objectForKey:@"s_instagram"]]]);
    } else {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.instagram.com"]];
        DebugLog(@"INSTA URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"https://www.instagram.com/%@",[dict_profile objectForKey:@"s_instagram"]]]);
    }
}


-(void)linkedinfunc: (id)sender
{
    NSString *linkedinstra = [dict_profile objectForKey:@"s_linkedin"];
    NSArray *linkedinarr= [linkedinstra componentsSeparatedByString:@"="];
    NSString *linkedinstr = [linkedinarr objectAtIndex:1];
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"linkedin://profile/%@",linkedinstr]]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"linkedin://profile/%@",linkedinstr]]];
        
        DebugLog(@"LINKED URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"linkedin://profile/%@",linkedinstr]]);
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.linkedin.com"]];
        DebugLog(@"LINKED URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_linkedin"]]]);
    }
}


-(void)skypefunc: (id)sender
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"skype:%@",[dict_profile objectForKey:@"s_skype"]]]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"skype:%@",[dict_profile objectForKey:@"s_skype"]]]];
        
        DebugLog(@"SKYPE URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"skype:%@",[dict_profile objectForKey:@"s_skype"]]]);
    } else {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.skype.com/%@",[dict_profile objectForKey:@"s_skype"]]]];
        
        DebugLog(@"SKYPE URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"https://www.skype.com/%@",[dict_profile objectForKey:@"s_skype"]]]);
    }
}
-(void)skypeAction{
    
    if ([dict_profile objectForKey:@"s_skype"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_skype"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_skype"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_skype"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_skype"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_skype"] isEqualToString:@"(null)"]) {
        
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"skype:%@",[dict_profile objectForKey:@"s_skype"]]]]) {
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"skype:%@",[dict_profile objectForKey:@"s_skype"]]]];
            
            DebugLog(@"SKYPE URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"skype:%@",[dict_profile objectForKey:@"s_skype"]]]);
        } else {
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.skype.com/%@",[dict_profile objectForKey:@"s_skype"]]]];
            
            DebugLog(@"SKYPE URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"https://www.skype.com/%@",[dict_profile objectForKey:@"s_skype"]]]);
        }
        
    }else{
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.skype.com"]]];
        
    }
    
    
    
}


-(void)soundcloudfunc: (id)sender
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.soundcloud.com/%@",[dict_profile objectForKey:@"s_soundcloud"]]]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.soundcloud.com/%@",[dict_profile objectForKey:@"s_soundcloud"]]]];
        
        DebugLog(@"SOUNDC URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"https://www.soundcloud.com/%@",[dict_profile objectForKey:@"s_soundcloud"]]]);
        
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.soundcloud.com"]];
        
        DebugLog(@"SOUNDC URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"https://www.soundcloud.com/%@",[dict_profile objectForKey:@"s_soundcloud"]]]);
    }
}



-(void)vimeofunc: (id)sender
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.vimeo.com/%@",[dict_profile objectForKey:@"s_vimeo"]]]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.vimeo.com/%@",[dict_profile objectForKey:@"s_vimeo"]]]];
        
        DebugLog(@"VIMEO URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"https://www.vimeo.com/%@",[dict_profile objectForKey:@"s_vimeo"]]]);
        
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.vimeo.com"]];
        
        DebugLog(@"VIMEO URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"https://www.vimeo.com/%@",[dict_profile objectForKey:@"s_vimeo"]]]);
    }
}


-(void)togglefunc: (UIButton *)sender
{
    DebugLog(@"TOGGLE FUNC CaLl HoChChE");
    
    if (switchprof == 0)
    {
        [UIView animateWithDuration:0.3 animations:^{
            
            //coverView.backgroundColor = [UIColor blackColor];
            
            
            // Add the gradient to the view
            [coverView.layer insertSublayer:gradient atIndex:0];
            
            
            [blackView setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0)];
            
            [businessImg setFrame:CGRectMake(15, 0, 61.0f/2.0f, 0)];
            
            [togglebt setFrame:CGRectMake(50, 0, 180, 0)];
            
            [lineDivMenu setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0)];
            
            [removeImg setFrame:CGRectMake(15, 0, 61.0f/2.0f, 0)];
            
            removeconbt.frame = CGRectMake(36, 0, 180, 0);
            
        }
         
         //        }];
         
                         completion:^(BOOL finished){
                             
                             rightmenuOpen = YES;
                             
                         }];
        
        switchprof = 1;
        [business_view removeFromSuperview];
        [self loadBusinessView];
        //        [self loadViews];
    }
    else
    {
        [UIView animateWithDuration:0.3 animations:^{
            
            //coverView.backgroundColor = [UIColor blackColor];
            
            
            // Add the gradient to the view
            [coverView.layer insertSublayer:gradient atIndex:0];
            
            
            [blackView setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0)];
            
            [businessImg setFrame:CGRectMake(15, 0, 61.0f/2.0f, 0)];
            
            [togglebt setFrame:CGRectMake(50, 0, 180, 0)];
            
            [lineDivMenu setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0)];
            
            [removeImg setFrame:CGRectMake(15, 0, 61.0f/2.0f, 0)];
            
            removeconbt.frame = CGRectMake(36, 0, 180, 0);
            
        }
                         completion:^(BOOL finished){
                             
                             rightmenuOpen = YES;
                             
                         }];
        
        switchprof = 0;
        [self loadPersonalViews];
        //        [self loadViews];
    }
}


-(void)mobileFun:(UITapGestureRecognizer *)sender {
    
    //    mobileback.backgroundColor = [UIColor colorWithRed:(238.0f/255.0f) green:(238.0f/255.0f) blue:(238.0f/255.0f) alpha:1.0f];
    
    UIActionSheet *phoneAction = [[UIActionSheet alloc]initWithTitle:Nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles:@"Call",@"SMS", nil];
    
    phoneAction.tag = 100;
    [phoneAction showInView:mainview];
}

-(void)landphoneFun:(UITapGestureRecognizer *)sender {
    
    landlineback.backgroundColor = [UIColor colorWithRed:(238.0f/255.0f) green:(238.0f/255.0f) blue:(238.0f/255.0f) alpha:1.0f];
    
    UIActionSheet *landphoneAction = [[UIActionSheet alloc]initWithTitle:Nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles:@"Call", nil];
    landphoneAction.tag = 200;
    [landphoneAction showInView:mainview];
}


-(void)mailFun:(UITapGestureRecognizer *)sender {
    
    mailback.backgroundColor = [UIColor colorWithRed:(238.0f/255.0f) green:(238.0f/255.0f) blue:(238.0f/255.0f) alpha:1.0f];
    
    mailComposer = [[MFMailComposeViewController alloc]init];
    mailComposer.mailComposeDelegate = self;
    NSArray *recipentsArray = [NSArray arrayWithObject:mail];
    [mailComposer setToRecipients:recipentsArray];
    [mailComposer setMessageBody:@"" isHTML:NO];
    [self.navigationController presentViewController:mailComposer animated:YES completion:nil];
}

#pragma mark - mail compose delegate

-(void)mailComposeController:(MFMailComposeViewController *)controller

         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (result) {
        DebugLog(@"Result : %d",result);
    }
    if (error) {
        DebugLog(@"Error : %@",error);
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
    mailback.backgroundColor = [UIColor clearColor];
}



- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 100) {
        
        if (buttonIndex == 0) {
            UIDevice *device = [UIDevice currentDevice];
            
            if ([[device model] isEqualToString:@"iPhone"] ) {
                NSString *phoneNumber = [@"telprompt://" stringByAppendingString:[NSString stringWithFormat:@"%@",mobile]];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
                
                //                [[UIApplication sharedApplication] openURL:[NSURL URLWithString: [NSString stringWithFormat:@"tel:%@",[phonesarr objectAtIndex:buttonIndex]]]];
                //                mobileback.backgroundColor = [UIColor clearColor];
            } else {
                
                UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                [notPermitted show];
                //                mobileback.backgroundColor = [UIColor clearColor];
            }
        }
        
        else if(buttonIndex == 1){
            
            UIDevice *device = [UIDevice currentDevice];
            
            if ([[device model] isEqualToString:@"iPhone"] ) {
                
                
                MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
                
                if([MFMessageComposeViewController canSendText])
                {
                    [self presentViewController:controller animated:YES completion:Nil];
                    controller.body = @"";
                    
                    NSString *smsstring= [NSString stringWithFormat:@"%@",[mobileArray objectAtIndex:0]];
                    
                    controller.recipients = [NSArray arrayWithObjects:smsstring, nil];
                    
                    controller.messageComposeDelegate = self;
                    
                    //[self presentModalViewController:controller animated:YES];
                    //                    mobileback.backgroundColor = [UIColor clearColor];
                }
            }
            else
            {
                UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                [notPermitted show];
                //                mobileback.backgroundColor = [UIColor clearColor];
            }
        }else{
            
            //            mobileback.backgroundColor = [UIColor clearColor];
            
        }
    }
    
    else if (actionSheet.tag == 200){
        
        
        
        if (buttonIndex == 0) {
            
            UIDevice *device = [UIDevice currentDevice];
            
            if ([[device model] isEqualToString:@"iPhone"] ) {
                
                NSString *phoneNumber = [@"telprompt://" stringByAppendingString:[NSString stringWithFormat:@"%@",landline]];
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
                
                
                
                //                [[UIApplication sharedApplication] openURL:[NSURL URLWithString: [NSString stringWithFormat:@"tel:%@",[phonesarr objectAtIndex:buttonIndex]]]];
                landlineback.backgroundColor = [UIColor clearColor];
                
            } else {
                
                UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                [notPermitted show];
                landlineback.backgroundColor = [UIColor clearColor];
                
            }
        }else{
            
            landlineback.backgroundColor = [UIColor clearColor];
            
        }
    }else if (actionSheet.tag == 900){
        
        if (buttonIndex == 0) {
            
            DebugLog(@"WhatsApp button clicked");
            [self showPersonViewController];
            //            //            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.whatsapp.com"]]];
            //            //            NSString * msg = @"YOUR MSG";
            //            DebugLog(@"number: %@",numberstring);
            //            NSString * urlWhats = [NSString stringWithFormat:@"whatsapp://send?abid=%@&text=",numberstring];
            //            NSURL * whatsappURL = [NSURL URLWithString:[urlWhats stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            //            if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
            //                [[UIApplication sharedApplication] openURL: whatsappURL];
            //            } else {
            //                UIAlertView * alert1 = [[UIAlertView alloc] initWithTitle:@"WhatsApp not installed." message:@"Your device has no WhatsApp installed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            //                [alert1 show];
            //            }
            
            
        }else if (buttonIndex == 1){
            if ([phonearr count] == 0)
            {
                [bgcallsms1 setBackgroundImage:[UIImage imageNamed:@"smsend_profile"] forState:UIControlStateNormal];
                
                
                smsAlert1 = [[UIAlertView alloc] initWithTitle:@"No Number found for this user!"
                                                       message:nil
                                                      delegate:self
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                smsAlert1.tag = 6;
                [smsAlert1 show];
            }
            else
            {
                
                [bgcallsms1 setBackgroundImage:[UIImage imageNamed:@"smsend_profile"] forState:UIControlStateNormal];
                
                UIDevice *device = [UIDevice currentDevice];
                if ([[device model] isEqualToString:@"iPhone"]) {
                    
                    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
                    if([MFMessageComposeViewController canSendText])
                    {
                        [self presentViewController:controller animated:YES completion:Nil];
                        controller.body = @"";
                        NSString *smsstring= [NSString stringWithFormat:@"%@",[phonearr objectAtIndex:0]];
                        controller.recipients = [NSArray arrayWithObjects:smsstring, nil];
                        controller.messageComposeDelegate = self;
                        //[self presentModalViewController:controller animated:YES];
                    }
                }
                else
                {
                    smsAlert2=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    smsAlert2.tag = 66;
                    [smsAlert2 show];
                }
            }
        }
        
        
    }
    
}


//    if (actionSheet.tag==1)
//    {
//        DebugLog(@"actionsheet call");
//        if (buttonIndex != [phonearr count])
//        {
//            UIDevice *device = [UIDevice currentDevice];
//            if ([[device model] isEqualToString:@"iPhone"] ) {
//                [[UIApplication sharedApplication] openURL:[NSURL URLWithString: [NSString stringWithFormat:@"tel:%@",[phonearr objectAtIndex:buttonIndex]]]];
//            } else {
//                UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                [notPermitted show];
//            }
//        }
//    }
//    if (actionSheet.tag==2)
//    {
//        DebugLog(@"actionsheet sms");
//        if (buttonIndex != [phonearr count])
//        {
//            UIDevice *device = [UIDevice currentDevice];
//            if ([[device model] isEqualToString:@"iPhone"]) {
//
//                MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
//
//                if([MFMessageComposeViewController canSendText])
//                {
//                    [self presentViewController:controller animated:YES completion:Nil];
//                    controller.body = @"";
//
//                    NSString *smsstring= [NSString stringWithFormat:@"%@",[phonearr objectAtIndex:buttonIndex]];
//                    controller.recipients = [NSArray arrayWithObjects:smsstring, nil];
//
//                    controller.messageComposeDelegate = self;
//
//                    //[self presentModalViewController:controller animated:YES];
//                }
//            }
//            else
//            {
//                UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                [notPermitted show];
//            }
//
//        }
//    }


-(void)showPersonViewController
{
    DebugLog(@"profile name field text: %@",prof_name.text);
    // Fetch the address book
    CFErrorRef *errorab = nil;
    
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, errorab);
    
    // Search for the person named "Appleseed" in the address book
    //    CFArrayRef people = ABAddressBookCopyPeopleWithName(addressBook, CFSTR("Appleseed"));
    
    CFArrayRef people = ABAddressBookCopyPeopleWithName(addressBook,
                                                        (__bridge CFStringRef)prof_name.text);
    
    // Display "Appleseed" information if found in the address book
    if ((people != nil) && (CFArrayGetCount(people) > 0))
    {
        for (int m = 0; m < (CFArrayGetCount(people)); m++){
            DebugLog(@"count e dichhe: %ld == %@", (CFArrayGetCount(people)),people);
            ABRecordRef person = CFArrayGetValueAtIndex(people, 0);
            
            ABRecordID recordID = ABRecordGetRecordID(person);
            
            DebugLog(@"ab id: %d",recordID);
            DebugLog(@"phone array; %@", phonearr);
            
            ABMultiValueRef phoneNumberProperty = ABRecordCopyValue(person, kABPersonPhoneProperty);
            NSArray *_phoneNumbers = (__bridge NSArray*)ABMultiValueCopyArrayOfAllValues(phoneNumberProperty);
            
            NSMutableArray *mutableph= [_phoneNumbers mutableCopy];
            int pp;
            for (pp =0; pp< [mutableph count]; pp++)
            {
                if ([[mutableph objectAtIndex:pp] isKindOfClass:[NSNull class]] || [mutableph objectAtIndex:pp] == (id)[NSNull null] || [[mutableph objectAtIndex:pp] length] < 3)
                {
                    [mutableph removeObjectAtIndex:pp];
                    pp= pp-1;
                }
            }
            NSMutableSet* set1 = [NSMutableSet setWithArray:mutableph];
            NSMutableSet* set2 = [NSMutableSet setWithArray:phonearr];
            [set1 intersectSet:set2]; //this will give you only the obejcts that are in both sets
            
            NSArray* result = [set1 allObjects];
            DebugLog(@"result pelo: %@",result);
            
            if ([result count] > 0)
            {
                NSString * urlWhats = [NSString stringWithFormat:@"whatsapp://send?abid=%d&text=",recordID];
                NSURL * whatsappURL = [NSURL URLWithString:[urlWhats stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
                    [[UIApplication sharedApplication] openURL: whatsappURL];
                } else {
                    UIAlertView * alert1 = [[UIAlertView alloc] initWithTitle:@"WhatsApp not installed." message:@"Your device has no WhatsApp installed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert1 show];
                }
                break;
            }
        }
    }
    else
    {
        // Show an alert if "Appleseed" is not in Contacts
    }
    //CFRelease(addressBook);
    //CFRelease(people);
}


- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    if (result == MessageComposeResultCancelled) {
        DebugLog(@"Message cancelled");
    } else if (result == MessageComposeResultSent) {
        DebugLog(@"Message sent");
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


+(void)chngpostion
{
    DebugLog(@"Change Position profile page");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Data" object:Nil];
}



//-(void)viewWillAppear:(BOOL)animated{
//
////    coverview1 = [[UIView alloc]initWithFrame:CGRectMake(5, 65, 310, self.view.bounds.size.height -65)];
////    [self.view addSubview:coverview1];
////    coverview1.backgroundColor=[UIColor whiteColor];
////    coverview1.layer.zPosition=5;
////
////
////    CGRect frame = CGRectMake(110,165,100,100);
////    act = [[UIActivityIndicatorView alloc] initWithFrame:frame];
////    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
////    [act setColor:[UIColor blackColor]];
////    [coverview1 addSubview:act];
////    [coverview1 bringSubviewToFront:act];
////    act.layer.zPosition=6;
////    [act startAnimating];
//
//
////    timeb = [NSTimer scheduledTimerWithTimeInterval:3.1f
////                                             target:self
////                                           selector:@selector(coverMethod:)
////                                           userInfo:nil
////                                            repeats:NO];
////
//
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getData:) name:@"Data" object:nil];
//
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(navtopage:) name:@"pagenav" object:nil];
//
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startDBInserta:) name:@"db_busy" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestreceivedaction) name:@"Requestreceived_push" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(acceptedrequestaction) name:@"Accepted_push" object:nil];
//}

-(void)requestreceivedaction
{
    //    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
    con.other=@"yes";
    con.uid=[NSString stringWithFormat:@"%@",del.pushtoprofileid];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}

-(void)acceptedrequestaction
{
    ConRequestViewController *con = [[ConRequestViewController alloc]init];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}



-(void)navtopage: (NSNotification *)notification
{
    DebugLog(@"navtopage");
    prefs =[NSUserDefaults standardUserDefaults];
    if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"4"])
    {
        ConAccountSettingsViewController *con = [[ConAccountSettingsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"6"])
    {
        ConInviteViewController *con = [[ConInviteViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"5"])
    {
        ConSyncLoaderViewController *con = [[ConSyncLoaderViewController alloc]init];
        //        [self.navigationController pushViewController:con animated:NO];
        
        [self presentViewController:con animated:NO completion:nil];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"2"])
    {
        ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"3"])
    {
        ConQROptionsViewController *con = [[ConQROptionsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    
    
    
    //    [leftmenu setFrame:CGRectMake(-280, leftmenu.frame.origin.y, leftmenu.frame.size.width, leftmenu.frame.size.height)];
    
    [mainview setFrame:CGRectMake(0, mainview.frame.origin.y, mainview.frame.size.width, mainview.frame.size.height)];
    
    del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    
    
    
    //    ConParentTopBarViewController *con = [[ConParentTopBarViewController alloc]init];
    //
    //    //    con.topbarparent.frame= CGRectMake(0, 0, con.topbarparent.bounds.size.width, con.topbarparent.bounds.size.height);
    //
    //    con.topbarparent.backgroundColor=[UIColor blackColor];
    
    move=1;
    
}



-(void)getData:(NSNotification *)notification {
    
    DebugLog(@"mainview.frame.origin.x = %f",mainview.frame.origin.x);
    
    if(move == 0) {
        
        DebugLog(@"if prof move");
        
        [UIView animateWithDuration:0.25
         
                         animations:^{
                             
                             [mainview setFrame:CGRectMake(280, (mainview.frame.origin.y), mainview.frame.size.width, mainview.frame.size.height)];
                             
                         }
         
                         completion:^(BOOL finished){
                             
                             move=1;
                         }];
        
    } else {
        
        DebugLog(@"else prof move");
        
        [UIView animateWithDuration:0.25
         
                         animations:^{
                             
                             [mainview setFrame:CGRectMake(0, (mainview.frame.origin.y), mainview.frame.size.width, mainview.frame.size.height)];
                         }
         
                         completion:^(BOOL finished){
                             move=0;
                         }];
        
    }
    
}



-(void)viewDidDisappear:(BOOL)animated
{
    move=0;
    personal_array = Nil;
    business_array =Nil;
    social_array = Nil;
    dict_profile = nil;
    dict_profile = nil;
    dict_profile = nil;
    profdict = nil;
    dict_profile = nil;
    [timea invalidate];
    [coverview1 removeFromSuperview];
    [SVProgressHUD dismiss];
    [mainview removeFromSuperview];
    [super viewDidDisappear:YES];
}


-(void)openmap: (UIGestureRecognizer *)sender
{
    addressback.backgroundColor = [UIColor colorWithRed:(238.0f/255.0f) green:(238.0f/255.0f) blue:(238.0f/255.0f) alpha:1.0f];
    
    if (switchprof == 0)
    {
        latitude=[[dict_profile objectForKey:@"lat"] doubleValue];
        longitude=[[dict_profile objectForKey:@"lng"] doubleValue];
    }
    else
    {
        latitude=[[dict_profile objectForKey:@"b_lat"] doubleValue];
        longitude=[[dict_profile objectForKey:@"b_lng"] doubleValue];
    }
    DebugLog(@"lati=== %f",latitude);
    DebugLog(@"long=== %f",longitude);
    [self generateRoute];
}

- (void)getDirections
{
}

-(void)showRoute:(MKDirectionsResponse *)response
{
    for (MKRoute *route in response.routes)
    {
        [map_View addOverlay:route.polyline level:MKOverlayLevelAboveLabels];
        
        for (MKRouteStep *step in route.steps)
        {
            DebugLog(@" here %@", step.instructions);
        }
    }
}


- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id < MKOverlay >)overlay
{
    MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
    renderer.strokeColor = [UIColor blueColor];
    renderer.lineWidth = 4.0;
    return renderer;
}


- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    CLLocation *location = map_View.userLocation.location;
    DebugLog(@"lat current: %f - long current: %f", location.coordinate.latitude, location.coordinate.longitude);
}


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    // Handle any custom annotations.
    
    if ([annotation isKindOfClass:[MKPointAnnotation class]])
    {
        // Try to dequeue an existing pin view first.
        
        MKPinAnnotationView *pinView = (MKPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomPinAnnotationView"];
        //        if (!pinView)
        //        {
        pinView = [[MKPinAnnotationView alloc] init];
        //            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomPinAnnotationView"];
        
        pinView.animatesDrop = YES;
        pinView.canShowCallout = YES;
        pinView.image = [UIImage imageNamed:@"locator1.png"];
        pinView.calloutOffset = CGPointMake(0, 0);
        
        
        UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        
        pinView.rightCalloutAccessoryView = rightButton;
        
        
        UIImageView *iconView = [[UIImageView alloc] init];
        
        iconView.frame = CGRectMake(0.0f, 0.0f,20, 20);
        
        pinView.leftCalloutAccessoryView = iconView;
        
        iconView.backgroundColor=[UIColor whiteColor];
        
        
        if (annotation == mapView.userLocation)
            
        {
            pinView.image= [UIImage imageNamed:@"locatorown.png"];
            
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomPinAnnotationView"];
        }
        
        DebugLog(@"(int)annotation.subtitle===%@",annotation.subtitle);
        
        iconView.image= profilepic;
        
        if (switchprof == 0)
            pinView.image = [UIImage imageNamed:@"locator1.png"];
        else
            pinView.image = [UIImage imageNamed:@"locator2.png"];
        
        //        }
        return pinView;
    }
    return nil;
}





-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    DebugLog(@"annotation selected");
}

- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)aUserLocation {
    
    MKCoordinateRegion region;
    
    MKCoordinateSpan span;
    
    span.latitudeDelta = 0.005;
    
    span.longitudeDelta = 0.005;
    
    CLLocationCoordinate2D location;
    
    location.latitude =  latitude;       //aUserLocation.coordinate.latitude;
    
    location.longitude = longitude;      //aUserLocation.coordinate.longitude;
    
    region.span = span;
    
    region.center = location;
    
    googleMapUrlString = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%f,%f&daddr=%f,%f", aUserLocation.location.coordinate.latitude,aUserLocation.location.coordinate.longitude, location.latitude, location.longitude];
    
    DebugLog(@"url fired map: %@",googleMapUrlString);
    
    //    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapUrlString]];
    appleMapUrlString = [NSString stringWithFormat:@"http://maps.apple.com/maps?saddr=%f,%f&daddr=%f,%f", aUserLocation.location.coordinate.latitude,aUserLocation.location.coordinate.longitude, location.latitude, location.longitude];
    [self generateRoute];
}

-(void)crossmap
{
    [crossbt removeFromSuperview];
    [mapview removeFromSuperview];
    [tapbgview removeFromSuperview];
    [navigatebt removeFromSuperview];
    [navigatebtapple removeFromSuperview];
}


- (void)generateRoute {
    CLLocationCoordinate2D end = {latitude, longitude};
    
    MKMapItem *destination_mapitem = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:end addressDictionary:nil]];
    
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
    
    request.source = [MKMapItem mapItemForCurrentLocation];
    
    request.destination = destination_mapitem;
    
    [request setTransportType:MKDirectionsTransportTypeAny]; // This can be limited to automobile and walking directions.
    
    [request setRequestsAlternateRoutes:YES];
    
    MKDirections *directions = [[MKDirections alloc] initWithRequest:request];
    
    [directions calculateDirectionsWithCompletionHandler:
     
     ^(MKDirectionsResponse *response, NSError *error) {
         
         if (error) {
             DebugLog(@"error generate route----  %@",error);
             
         } else {
             [self showRoute:response];
         }
     }];
    [self navigateinapple];
}


-(void)navigatetoloc
{
    ConNavigateViewController *con = [[ConNavigateViewController alloc]init];
    con.fireurl = googleMapUrlString;
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}


-(void)navigateinapple
{
    CLLocationCoordinate2D endingCoord = CLLocationCoordinate2DMake(latitude, longitude);
    MKPlacemark *endLocation = [[MKPlacemark alloc] initWithCoordinate:endingCoord addressDictionary:nil];
    MKMapItem *endingItem = [[MKMapItem alloc] initWithPlacemark:endLocation];
    NSMutableDictionary *launchOptions = [[NSMutableDictionary alloc] init];
    [launchOptions setObject:MKLaunchOptionsDirectionsModeDriving forKey:MKLaunchOptionsDirectionsModeKey];
    [endingItem openInMapsWithLaunchOptions:launchOptions];
}


-(void)detailpic: (UIGestureRecognizer *)sender
{
    ConPictureProfViewController *con = [[ConPictureProfViewController alloc]init];
    con.profilepic = profilepic;
    //    con.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:con animated:NO completion:nil];
    //    tapbgview = [[UIView alloc]initWithFrame:self.view.frame];
    //    [self.view addSubview:tapbgview];
    //    tapbgview.backgroundColor=[UIColor blackColor];
    //    tapbgview.alpha=0.95f;
    //    UITapGestureRecognizer *tapback = [[UITapGestureRecognizer alloc]
    //                                       initWithTarget:self
    //                                       action:@selector(closedetailpic:)];
    //    tapbgview.userInteractionEnabled=YES;
    //    [tapbgview addGestureRecognizer:tapback];
    //    detailimg = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 300, mainview.bounds.size.height-20)];
    //
    //    [mainview addSubview:detailimg];
    //
    //    detailimg.hidden=YES;
    //
    //    detailimg.image = profilepic;
    //
    //    detailimg.contentMode=UIViewContentModeScaleAspectFit;
    //
    //    detailimg.userInteractionEnabled=YES;
    //    UITapGestureRecognizer *tapimg1 = [[UITapGestureRecognizer alloc]
    //                                       initWithTarget:self
    //                                       action:@selector(closedetailpic:)];
    //    detailimg.userInteractionEnabled=YES;
    //    [detailimg addGestureRecognizer:tapimg1];
    //    CATransition *animation = [CATransition animation];
    //    [animation setType:kCATransitionReveal];
    //    [animation setSubtype:kCATransitionReveal];
    //    animation.duration = 1.5f;
    //    [detailimg.layer addAnimation:animation forKey:nil];
    //    if(detailimg.hidden==YES)
    //    {
    //        detailimg.hidden=NO;
    //    }
}



-(void)closedetailpic: (UIGestureRecognizer *)sender

{
    
    CATransition *animation = [CATransition animation];
    
    
    
    [animation setType:kCATransitionFade];
    
    
    
    [animation setSubtype:kCATransitionFade];
    
    
    
    animation.duration = 0.7f;
    
    
    
    [detailimg.layer addAnimation:animation forKey:nil];
    
    
    
    if(detailimg.hidden==NO)
        
    {
        
        detailimg.hidden=YES;
        
    }
    
    [tapbgview removeFromSuperview];
    
}





- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data

{
    
    [responseData appendData:data];
    
    
    
}



- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error

{
    
    DebugLog(@"connection failed");
    
}



- (void)connectionDidFinishLoading:(NSURLConnection *)connection

{
    
    DebugLog(@"connection finished loading");
    
    
    
    DebugLog(@"response data - %@", [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
    
    
    
    NSError *err;
    
    
    
    //    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString1]];
    
    
    
    if (responseData != nil)
        
    {
        
        NSDictionary  *json1aa=[NSJSONSerialization JSONObjectWithData:responseData options:0 error:&err];
        
        
        
        DebugLog(@"this now has json string url: %@",json1aa);
        
    }
    
    else
        
    {
        
        alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                 
                                           message:nil
                 
                                          delegate:self
                 
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        
        alert.tag=7;
        
        //        [alert show];
    }
}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    responseData = [NSMutableData data];
}

-(void)servererror
{
    alert = [[UIAlertView alloc] initWithTitle:@"Error in Profile Server Connection!"
                                       message:nil delegate:self
                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
    [alert show];
}

-(void) reloadDataFromWeb
{
    DebugLog(@"reload profile from web");
    prefs = [NSUserDefaults standardUserDefaults];
    [mainview addSubview:act];
    act.center= self.view.center;
    act.tintColor=[UIColor grayColor];
    [act startAnimating];
    
    //    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    NSString *errornumber;
    
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=profile&id=%@&access_token=%@&device_id=%@&image=true",user_id,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
    
    DebugLog(@"profile url: %@",urlString1);
    
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL11a =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    
    if (signeddataURL11a == nil)
    {
        DebugLog(@"no connnn profile");
        
        // [self performSelectorOnMainThread:@selector(servererror) withObject:nil waitUntilDone:YES];
        [self reloadFromLocalDB];
    }
    //    DebugLog(@"json returns: %@",json1);
    else
    {
        //            [alert show];
        
        
        NSError *error;
        json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL11a //1
                 
                                                options:kNilOptions
                 
                                                  error:&error];
        errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
        
        DebugLog(@"err  %@",errornumber);
        
        
        if (![errornumber isEqualToString:@"0"])
        {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"error"];
            [act removeFromSuperview];
            NSString *err_str = [json1 objectForKey:@"error"];
            
            alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                     
                                               message:err_str
                     
                                              delegate:self
                     
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            //            [alert show];
        }
        else
        {
            dict_profile = [NSMutableDictionary new];
            
            profdict = [NSMutableDictionary new];
            //
            //            dict_profile = [NSMutableDictionary new];
            
            
            
            profdict = [json1 objectForKey:@"details"];
            
            dict_profile = [profdict objectForKey:@"profile"];
            
            //        DebugLog(@"array gives: %@",dict_profile);
            
            //                for( NSString *aKey in [dict_profile allKeys])
            //                {
            //                    NSString *newString = [aKey substringToIndex:2];
            //
            //                    if ([newString isEqualToString:@"b_"])
            //                    {
            //                        if ([dict_profile isKindOfClass:[NSMutableDictionary class]] && [dict_profile isKindOfClass:[NSDictionary class]])
            //                        {
            //                            [dict_profile setObject:[dict_profile valueForKey:[NSString stringWithFormat:@"%@",aKey]] forKey:[NSString stringWithFormat:@"%@",aKey]];
            //                        }
            //                    }
            //                    else if ([newString isEqualToString:@"s_"])
            //                    {
            //                        if ([dict_profile isKindOfClass:[NSMutableDictionary class]] && [dict_profile isKindOfClass:[NSDictionary class]])
            //                        {
            //                            [dict_profile setObject:[dict_profile valueForKey:[NSString stringWithFormat:@"%@",aKey]] forKey:[NSString stringWithFormat:@"%@",aKey]];
            //                        }
            //                    }
            //                    else
            //                    {
            //                        if ([dict_profile isKindOfClass:[NSMutableDictionary class]] && [dict_profile isKindOfClass:[NSDictionary class]])
            //                        {
            //                            [dict_profile setObject:[dict_profile valueForKey:[NSString stringWithFormat:@"%@",aKey]] forKey:[NSString stringWithFormat:@"%@",aKey]];
            //                        }
            //                    }
            //                }
            
            //            DebugLog(@"business dict; %@",dict_profile);
            //            DebugLog(@"social dict; %@",dict_profile);
            //            DebugLog(@"personal dict; %@",dict_profile);
            //            NSString *fullname2= [NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]];
            //
            //            [prof_name removeFromSuperview];
            //            prof_name = [[UILabel alloc]init]; //WithFrame:CGRectMake(112, 72, 196, 60)];
            //            prof_name.backgroundColor=[UIColor clearColor];
            //            prof_name.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
            //            prof_name.text=fullname2;
            //            //    prof_name.font=[UIFont fontWithName:@"Mark Simonson - Proxima Nova Alt Condensed Bold" size:19];
            //            prof_name.font=[UIFont fontWithName:@"ProximaNova-Bold" size:18];
            //            prof_name.numberOfLines=0;
            //            prof_name.frame=CGRectMake(120, 75-70, 195, 60);
            ////            [grayback addSubview:prof_name];
            //
            //            //        prof_name.textAlignment= NSTextAlignmentCenter;
            //
            //            UIImageView *prof_img = [[UIImageView alloc]initWithFrame:CGRectMake(15, 75-65, 90, 90)];
            //
            //            [grayback addSubview:prof_img];
            //
            //
            //
            //            base64String= [profdict objectForKey:@"image"];
            //
            //            if ([base64String length] >6)
            //            {
            //                NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
            //
            //                //    NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
            //
            //                profilepic = [UIImage imageWithData:decodedData];
            //
            //                prof_img.image=profilepic;
            //
            //                prof_img.contentMode= UIViewContentModeScaleAspectFill;
            //
            //                prof_img.clipsToBounds=YES;
            //            }
            //
            //            prof_img.userInteractionEnabled=YES;
            //
            //            UITapGestureRecognizer *propictap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(detailpic:)];
            //
            //            [prof_img addGestureRecognizer:propictap];
            
            //            [self performSelectorInBackground:@selector(restoreBackUp)
            //                                   withObject:nil];
            
            if ([user_id intValue] == [[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]intValue])
                switchprof=0;
            else
            {
                if ([[dict_profile objectForKey:@"friend"]intValue] == 1)
                {
                    switchprof=0;
                }
                
                else if ([[dict_profile objectForKey:@"business"] intValue]==1)
                {
                    switchprof=1;
                }
                DebugLog(@"fields are: %d , %d",[[dict_profile objectForKey:@"own"]intValue], switchprof);
            }
        }
        
        
        
        //        if ([user_id intValue] == [[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]intValue])
        //            switchprof=0;
        //        else
        //        {
        //            if ([[profdict objectForKey:@"friend"]intValue] == 1)
        //            {
        //                switchprof=0;
        //            }
        //
        //            else if ([[profdict objectForKey:@"business"] intValue]==1)
        //            {
        //                switchprof=1;
        //
        //                allowpersonal=1;
        //            }
        //            DebugLog(@"fields are: %d , %d",[[profdict objectForKey:@"own"]intValue], switchprof);
        //        }
        //        DebugLog(@"Show business udhao2: %d .... %d",[[profdict objectForKey:@"friend"]intValue], [[profdict objectForKey:@"business"] intValue]);
        //
        //        if (allowpersonal == 1)
        //        {
        //            //                [togglebt removeFromSuperview];
        //            //                togglebt.enabled=NO;
        //        }
        //
        //        else if ([[profdict objectForKey:@"friend"]intValue] == 0 && [[profdict objectForKey:@"business"] intValue]==1)
        //        {
        //            //                [togglebt removeFromSuperview];
        //            //                togglebt.enabled=NO;
        //        }
        
    }
    //        dispatch_async(dispatch_get_main_queue(), ^{
    
    if (signeddataURL11a != nil && [errornumber isEqualToString:@"0"] && [dict_profile count] > 0)
    {
        //                [self loadViews];
        
        
        
        //            Mymodel *obj=[Mymodel getInstance];
        //            if (obj.db_busy == NO)
        //            {
        //              profile_insertion =1;
        [[DBManager getSharedInstance]insertProfile:profdict :[user_id intValue]];
        
        //                [self startDBInsert];
        //            }
        //        [self reloadFromLocalDB];
        
    }
    //        });
    //    });
    
}



-(void) reloadFromLocalDB
{
    
    
    NSDictionary *localDBProfile = [[NSDictionary alloc]init];
    
    localDBProfile = [[DBManager getSharedInstance]fetchProfile:[user_id intValue]];
    
    DebugLog(@"this gives: %@",localDBProfile);
     DebugLog(@"BUSINESS BUSINESS:%@",[localDBProfile valueForKey:@"business"]);
    
    if ([localDBProfile isKindOfClass:[NSNull class]] || localDBProfile == (id)[NSNull null])
    {
        //        [self reloadDataFromWeb];
        [self performSelectorOnMainThread:@selector(servererror) withObject:nil waitUntilDone:YES];
        
    }
    else{
        
        dict_profile = [[NSMutableDictionary alloc]init];
        
        dict_profile = [[NSMutableDictionary alloc]init];
        
        dict_profile = [[NSMutableDictionary alloc]init];
        
        dict_profile = [localDBProfile mutableCopy];
        profdict = [dict_profile mutableCopy];
        DebugLog(@"BUSINESS BUSINESS:%@",[profdict objectForKey:@"business"]);
        //    for( NSString *aKey in [localDBProfile allKeys])
        //    {
        //        NSString *newString = [aKey substringToIndex:2];
        //
        //    if ([newString isEqualToString:@"b_"])
        //
        //        [dict_profile setObject:[localDBProfile valueForKey:[NSString stringWithFormat:@"%@",aKey]] forKey:[NSString stringWithFormat:@"%@",aKey]];
        //
        //    else if ([newString isEqualToString:@"s_"])
        //
        //        [dict_profile setObject:[localDBProfile valueForKey:[NSString stringWithFormat:@"%@",aKey]] forKey:[NSString stringWithFormat:@"%@",aKey]];
        //
        //    else
        //
        //        [dict_profile setObject:[localDBProfile valueForKey:[NSString stringWithFormat:@"%@",aKey]] forKey:[NSString stringWithFormat:@"%@",aKey]];
        //    }
        //
        //    DebugLog(@"business dict; %@",dict_profile);
        //
        //    DebugLog(@"social dict; %@",dict_profile);
        //
        //    DebugLog(@"personal dict; %@",dict_profile);
        
        NSString *fullname1;
        
        if (![[NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]] isKindOfClass:[NSNull class]] && ![[NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]] isEqualToString:@""] && ![[NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]] isEqualToString:@"(null)"] && [[NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]] length]>0)
        {
            
            fullname1= [NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]];
        }
        
        [prof_name removeFromSuperview];
        
        prof_name = [[UILabel alloc]init]; //WithFrame:CGRectMake(112, 72, 196, 60)];
        prof_name.backgroundColor=[UIColor clearColor];
        prof_name.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
        
        
        if ([fullname1 isKindOfClass:[NSNull class]] || [fullname1 isEqualToString:@""] || [fullname1 isEqualToString:@"(null) (null)"] || [fullname1 length]==0) {
            
            prof_name.text = @"";
            DebugLog(@"PROFILE NAME1:%@",prof_name);
            prof_name.frame=CGRectMake(0, 0, 0, 0);
            // [prof_name removeFromSuperview];
            
        }
        else
        {
            prof_name.text=fullname1;
            prof_name.frame=CGRectMake(120, 75-70, 195, 60);
        }
        
        
        
        // prof_name.text=fullname1;
        //    prof_name.font=[UIFont fontWithName:@"Mark Simonson - Proxima Nova Alt Condensed Bold" size:19];
        prof_name.font=[UIFont fontWithName:@"ProximaNova-Bold" size:18];
        prof_name.numberOfLines=0;
        
        //    CGSize maximumSize = CGSizeMake(196, 60);
        //
        //    UIFont *myFont = [UIFont boldSystemFontOfSize:20];
        //
        //    CGSize myStringSize = [fullname1 sizeWithFont:myFont
        //
        //                                constrainedToSize:maximumSize
        //
        //                                    lineBreakMode:NSLineBreakByWordWrapping];
        
        //myStringSize.height
        //    [grayback addSubview:prof_name];
        
        //    prof_name.textAlignment= NSTextAlignmentCenter;
        
        
        UIImageView *prof_img = [[UIImageView alloc]initWithFrame:CGRectMake(15, 75-64, 90, 90)];
        
        [grayback addSubview:prof_img];
        
        base64String= [localDBProfile objectForKey:@"thumb"];
        
        if ([base64String length] >6)
        {
            NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
            
            //    NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
            
            profilepic = [UIImage imageWithData:decodedData];
            
            prof_img.image=profilepic;
            
            prof_img.contentMode= UIViewContentModeScaleAspectFill;
            
            prof_img.clipsToBounds=YES;
        }
        
        prof_img.userInteractionEnabled=YES;
        
        UITapGestureRecognizer *propictap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(detailpic:)];
        
        [prof_img addGestureRecognizer:propictap];
        
        if ([user_id intValue] == [[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]intValue])
        {
            switchprof=0;
            [self loadPersonalViews];
        }
        else
        {
            if ([[dict_profile objectForKey:@"friend"]intValue] == 1)
            {
                switchprof=0;
                [self loadPersonalViews];
            }
            
            else if ([[dict_profile objectForKey:@"business"] intValue]==1)
            {
                switchprof=1;
                [self loadBusinessView];
            }
            DebugLog(@"fields are: %d , %d",[[dict_profile objectForKey:@"own"]intValue], switchprof);
        }
        
        
    }
    
    //    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"isfriend"] integerValue]==2)
    //        [self loadBusinessView];
    //    else
    //        [self loadPersonalViews];
    
    //    [self loadViews];
}

-(void)startDBInsert
{
    //    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //    [del loadProfileIntoDB:profdict:[user_id intValue]];
    //    if (profile_insertion == 0)
    //    {
    //        profile_insertion=1;
    //        DebugLog(@"startdbinsert called");
    //        [[DBManager getSharedInstance]insertProfile:profdict :[user_id intValue]];
    //    }
}

-(void)startDBInserta:(NSNotification *)notification
{
    //    if (![[[NSUserDefaults standardUserDefaults]objectForKey:@"firstProfile"] isEqualToString:@"yes"] && profile_fetch == 0)
    //    {
    //        profile_fetch=1;
    //        [self reloadFromLocalDB];
    //    }
    
    if (profile_insertion == 0)
    {
        profile_insertion =1;
        DebugLog(@"startdbinserta called");
        [[DBManager getSharedInstance]insertProfile:profdict :[user_id intValue]];
    }
}

-(void)removeConnection: (id)sender
{
    [UIView animateWithDuration:0.3 animations:^{
        
       // coverView.backgroundColor = [UIColor blackColor];
        
        // Add the gradient to the view
        [coverView.layer insertSublayer:gradient atIndex:0];
        
        
        
        [blackView setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0)];
        
        [businessImg setFrame:CGRectMake(15, 0, 61.0f/2.0f, 0)];
        
        [togglebt setFrame:CGRectMake(50, 0, 180, 0)];
        
        [lineDivMenu setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0)];
        
        [removeImg setFrame:CGRectMake(15, 0, 61.0f/2.0f, 0)];
        
        removeconbt.frame = CGRectMake(36, 0, 180, 0);
        
    }
     
     //        }];
     
                     completion:^(BOOL finished){
                         
                         rightmenuOpen = YES;
                         
                     }];
    
    
    alert = [[UIAlertView alloc] initWithTitle:@"Remove this contact?"
                                       message:nil
                                      delegate:self
                             cancelButtonTitle:@"Cancel"  otherButtonTitles:@"Ok", nil];
    alert.tag=123;
    [alert show];
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    DebugLog(@"ALERT . TAG====%ld",(long)alertView.tag);
    if (alertView.tag == 123) {
        
        if(buttonIndex == 0)
        {
            // Do something
        }
        else
        {
            prefs = [NSUserDefaults standardUserDefaults];
            NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=removeconnection&id=%d&access_token=%@&device_id=%@&business=true",[user_id intValue],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
            
            DebugLog(@"deny url: %@",urlString1);
            NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
            NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
            
            if (signeddataURL1 != nil)
            {
                NSError *error=nil;
                NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                          options:kNilOptions
                                                                            error:&error];
                DebugLog(@"deny json returns: %@",json_deny);
                if ([[json_deny objectForKey:@"success"]intValue] == 1)
                {
                    alert = [[UIAlertView alloc] initWithTitle:@"Connection Successfully Removed!"
                                                       message:nil
                                                      delegate:self
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                    [alert show];
                    [removeconbt removeFromSuperview];
                    //                togglebt.userInteractionEnabled = NO;
                    
                    ConNewRequestsViewController *mng = [[ConNewRequestsViewController alloc]init];
                    //                mng.imported_arr = [filtered_arr objectAtIndex:indexPath.row];
                    mng.userid= [user_id intValue];
                    DebugLog(@"selected user id : %d",mng.userid);
                    
//                    CATransition* transition = [CATransition animation];
//                    
//                    transition.duration = 0.4;
//                    transition.type = kCATransitionPush;
//                    transition.subtype = kCATransitionFade;
//                    
//                    [[self navigationController].view.layer addAnimation:transition forKey:nil];
                    
                    [self.navigationController pushViewController:mng animated:YES];
                }
                else
                {
                    alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                                       message:[NSString stringWithFormat:@"%@",[json_deny objectForKey:@"error"]]
                                                      delegate:self
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                    [alert show];
                }
            }
            else
            {
                alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                                   message:nil
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                [alert show];
            }
        }
    }else if (alertView.tag == 9 || alertView.tag == 99){
        
        DebugLog(@"BUTTON INDEX 0");
        if (buttonIndex == 0)
        {
            
            [bgcallsms setBackgroundImage:[UIImage imageNamed:@"call_profile"] forState:UIControlStateNormal];
            
        }
        
    }else if (alertView.tag == 6 || alertView.tag == 66){
        
        if(buttonIndex == 0)
        {
            [bgcallsms1 setBackgroundImage:[UIImage imageNamed:@"sms_profile"] forState:UIControlStateNormal];
            
        }
        
    }
}

-(void)websiteFun: (id)sender
{
    DebugLog(@"dcdc");
    SocialWebViewController *social= [[SocialWebViewController alloc]init];
    if (switchprof == 0)
        //        social.type_social=[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"website"]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"website"]]]];
    else
        //        social.type_social=[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_website"]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_website"]]]];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:social animated:YES];
}

-(void)yesSuccess
{
    //    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //    [del showTabValues:YES];
}

-(void)editprofile_func: (id)sender
{
    ConEditProfileViewController *con =[[ConEditProfileViewController alloc]init];
    [self presentViewController:con animated:YES completion:nil];
}

-(void)phonefunc
{
    DebugLog(@"call phonesarr: %@",phonearr);
    if ([phonearr count] == 0)
    {
        [bgcallsms setBackgroundImage:[UIImage imageNamed:@"callend_profile"] forState:UIControlStateNormal];
        
        callAlert1 = [[UIAlertView alloc] initWithTitle:@"No Number found for this user!"
                                                message:nil
                                               delegate:self
                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        callAlert1.tag = 9;
        [callAlert1 show];
    }
    else
    {
        [bgcallsms setBackgroundImage:[UIImage imageNamed:@"callend_profile"] forState:UIControlStateNormal];
        
        
        UIDevice *device = [UIDevice currentDevice];
        if ([[device model] isEqualToString:@"iPhone"] ) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString: [NSString stringWithFormat:@"tel:%@",[phonearr objectAtIndex:0]]]];
        } else {
            callAlert2=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            callAlert2.tag = 99;
            [callAlert2 show];
        }
    }
}


-(void)chatfunc:(UIButton *)sender
{
    
    //========================Original code=====================//
    
    //    DebugLog(@"sms phonesarr: %@",phonearr);
    //    if ([phonearr count] == 0)
    //    {
    //        [bgcallsms1 setBackgroundImage:[UIImage imageNamed:@"smsend_profile"] forState:UIControlStateNormal];
    //
    //
    //        smsAlert1 = [[UIAlertView alloc] initWithTitle:@"No Number found for this user!"
    //                                               message:nil
    //                                              delegate:self
    //                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
    //        smsAlert1.tag = 6;
    //        [smsAlert1 show];
    //    }
    //    else
    //    {
    //
    //        [bgcallsms1 setBackgroundImage:[UIImage imageNamed:@"smsend_profile"] forState:UIControlStateNormal];
    //
    //        UIDevice *device = [UIDevice currentDevice];
    //        if ([[device model] isEqualToString:@"iPhone"]) {
    //
    //            MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
    //            if([MFMessageComposeViewController canSendText])
    //            {
    //                [self presentViewController:controller animated:YES completion:Nil];
    //                controller.body = @"";
    //                NSString *smsstring= [NSString stringWithFormat:@"%@",[phonearr objectAtIndex:0]];
    //                controller.recipients = [NSArray arrayWithObjects:smsstring, nil];
    //                controller.messageComposeDelegate = self;
    //                //[self presentModalViewController:controller animated:YES];
    //            }
    //        }
    //        else
    //        {
    //            smsAlert2=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //            smsAlert2.tag = 66;
    //            [smsAlert2 show];
    //        }
    //    }
    
    //========================================================================
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select a option"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"WhatsApp", @"SMS", nil];
    
    //            [actionSheet showInView:self.view];
    actionSheet.tag = 900;
    [actionSheet showInView:mainview];
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

-(void)gobackoption//: (id)sender
{
    //    backBground.backgroundColor = [UIColor colorWithRed:(30.0f/255.0f) green:(30.0f/255.0f) blue:(30.0f/255.0f) alpha:1.0f];
    
    labelHead.alpha = 0.5f;
    bck_img1.alpha = 0.5f;

    tap.enabled=NO;
    int index;
    NSArray* navarr = [[NSArray alloc] initWithArray:self.navigationController.viewControllers];
    for(int i=0 ; i<[navarr count] ; i++)
    {
        //        backBtn.enabled=NO;
        if(![[navarr objectAtIndex:i] isKindOfClass:NSClassFromString(@"ConProfileOwnViewController")])
        {
            index = i;
        }
    }
    
//    CATransition *transition = [CATransition animation];
//    
//    transition.duration = 0.4f;
//    
//    transition.type = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    [self.navigationController popToViewController:[navarr objectAtIndex:index] animated:YES];
    
}

-(void)rightMenu:(UIButton *)sender{
    
    DebugLog(@"rightMenu: %x", rightmenuOpen);
    
    [overlayTap setEnabled:NO];
    //    menuBground.backgroundColor = [UIColor clearColor];
    
    if(rightmenuOpen == YES) {
        
        //        menuBground.backgroundColor = [UIColor colorWithRed:(30.0f/255.0f) green:(30.0f/255.0f) blue:(30.0f/255.0f) alpha:1.0f];
        
        blackView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0)];
        [blackView setBackgroundColor:[UIColor blackColor]];
        [blackView setAlpha:0.9f];
        blackView.layer.zPosition = 3;
        [blackView setUserInteractionEnabled:YES];
        [grayback addSubview:blackView];
        
        //===========================SHOW BUSINESS CARD==========================//
        
        
        businessImg = [[UIImageView alloc]initWithFrame:CGRectMake(15, 0, 61.0f/3.0f, 0)];
        businessImg.image=[UIImage imageNamed:@"business_profile"];
        [blackView addSubview:businessImg];
        
        togglebt = [UIButton buttonWithType:UIButtonTypeCustom];
        togglebt.frame = CGRectMake(36, 0, 180, 40);
        togglebt.backgroundColor=[UIColor clearColor];
        togglebt.layer.zPosition = 4.0f;
        if (switchprof == 0)
            [togglebt setTitle:@"View Business Card" forState:UIControlStateNormal];
        else
            [togglebt setTitle:@"View Personal Card" forState:UIControlStateNormal];
        
        [togglebt setTitleColor:[UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f] forState:UIControlStateNormal];
        togglebt.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Normal" size:16];
        [togglebt addTarget:self action:@selector(togglefunc:) forControlEvents:UIControlEventTouchUpInside];
        [blackView addSubview:togglebt];
        
        DebugLog(@"show business udhao: %d .... %d", [[dict_profile objectForKey:@"friend"]intValue], [[dict_profile objectForKey:@"business"] intValue]);
        
        //=================================LINE=================================//
        
        lineDivMenu = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0)];
        lineDivMenu.backgroundColor = [UIColor colorWithRed:(102.0f/255.0f) green:(102.0f/255.0f) blue:(102.0f/255.0f) alpha:1.0f];
        [blackView addSubview:lineDivMenu];
        
        //===========================REMOVE CONNECTION==========================//
        
        removeImg = [[UIImageView alloc]initWithFrame:CGRectMake(15, 0, 61.0f/3.0f, 0)];
        removeImg.image=[UIImage imageNamed:@"remove_profile"];
        [blackView addSubview:removeImg];
        
        removeconbt = [UIButton buttonWithType:UIButtonTypeCustom];
        removeconbt.frame = CGRectMake(36, 0, 180, 40);
        removeconbt.titleLabel.font=[UIFont fontWithName:@"ProximaNova-Normal" size:16];
        [removeconbt setTitle:@"Remove Connection" forState:UIControlStateNormal];
        [removeconbt setTitleColor:[UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f] forState:UIControlStateNormal];
        removeconbt.backgroundColor=[UIColor clearColor];
        [removeconbt addTarget:self action:@selector(removeConnection:) forControlEvents:UIControlEventTouchUpInside];
        [blackView addSubview:removeconbt];
        
        [UIView animateWithDuration:0.3 animations:^{
            
            //            coverView.backgroundColor = [UIColor colorWithRed:(30.0f/255.0f) green:(30.0f/255.0f) blue:(30.0f/255.0f) alpha:1.0f];
            
            [blackView setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 115)];
            
            [businessImg setFrame:CGRectMake(16, 20, 61.0f/3.5f, 54.0f/3.0f)];
            
            [togglebt setFrame:CGRectMake(36, 10, 180, 40)];
            
            [lineDivMenu setFrame:CGRectMake(0, blackView.frame.size.height/2, [UIScreen mainScreen].bounds.size.width, 1)];
            
            [removeImg setFrame:CGRectMake(16, businessImg.frame.origin.y +55, 61.0f/3.5f, 54.0f/3.0f)];
            
            removeconbt.frame = CGRectMake(36, togglebt.frame.origin.y + togglebt.frame.size.height + 15, 180, 40);
            
        }
         
                         completion:^(BOOL finished){
                             
                             rightmenuOpen = NO;
                             [overlayTap setEnabled:YES];
                             
                         }];
        
    } else {
        
        //        menuBground.backgroundColor = [UIColor clearColor];
        
        [UIView animateWithDuration:0.3 animations:^{
            
            //            coverView.backgroundColor = [UIColor blackColor];
            
            [blackView setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0)];
            
            [businessImg setFrame:CGRectMake(15, 0, 61.0f/2.0f, 0)];
            
            [togglebt setFrame:CGRectMake(36, 0, 180, 0)];
            
            [lineDivMenu setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0)];
            
            [removeImg setFrame:CGRectMake(15, 0, 61.0f/2.0f, 0)];
            
            removeconbt.frame = CGRectMake(36, 0, 180, 0);
            
            
        }
         
                         completion:^(BOOL finished){
                             
                             rightmenuOpen = YES;
                             [overlayTap setEnabled:YES];
                             
                         }];
        
    }
    
}
-(void)changeButtonBackGroundColor:(id)sender{
    
    [rightMenuBack setBackgroundColor:[UIColor colorWithRed:(30.0f/255.0f) green:(30.0f/255.0f) blue:(30.0f/255.0f) alpha:1.0f]];
    
}
-(void)resetButtonBackGroundColor:(id)sender{
    
    [rightMenuBack setBackgroundColor:[UIColor blackColor]];
    
}

-(void)setAddress
{
    y1=0;
    
    if (![street isEqualToString: @""] && [number isEqualToString: @""])
    {
        street_number_lbl.text=[NSString stringWithFormat:@"%@,",street];
        street_number_lbl.frame = CGRectMake(38, divider_4.frame.origin.y+divider_4.frame.size.height+8, 250, 20);
        y1=street_number_lbl.frame.size.height+8;
    }
    else if ([street isEqualToString: @""] && ![number isEqualToString: @""])
    {
        street_number_lbl.text=[NSString stringWithFormat:@"%@,",number];
        street_number_lbl.frame = CGRectMake(38, divider_4.frame.origin.y+divider_4.frame.size.height+8, 250, 20);
        y1=street_number_lbl.frame.size.height+8;
    }
    else if (![street isEqualToString: @""] && ![number isEqualToString: @""])
    {
        street_number_lbl.text=[NSString stringWithFormat:@"%@, %@,",street,number];
        street_number_lbl.frame = CGRectMake(38, divider_4.frame.origin.y+divider_4.frame.size.height+8, 250, 20);
        y1=street_number_lbl.frame.size.height+8;
    }
    else if ([street isEqualToString: @""] && [number isEqualToString: @""])
    {
        street_number_lbl.frame = CGRectMake(38, divider_4.frame.origin.y+divider_4.frame.size.height+8, 250, 0);
        y1=street_number_lbl.frame.size.height+8;
    }
    if (![zipcode isEqualToString: @""] && [city isEqualToString:@""])
    {
        zipcode_lbl.text=[NSString stringWithFormat:@"%@",zipcode];
        zipcode_lbl.frame = CGRectMake(38, street_number_lbl.frame.origin.y+street_number_lbl.frame.size.height, 250, 20);
        y1=y1+zipcode_lbl.frame.size.height;
    }
    else if ([zipcode isEqualToString: @""] && ![city isEqualToString:@""])
    {
        zipcode_lbl.text=[NSString stringWithFormat:@"%@",city];
        zipcode_lbl.frame = CGRectMake(38, street_number_lbl.frame.origin.y+street_number_lbl.frame.size.height, 250, 20);
        y1=y1+zipcode_lbl.frame.size.height;
    }
    else if (![zipcode isEqualToString: @""] && ![city isEqualToString:@""])
    {
        zipcode_lbl.text=[NSString stringWithFormat:@"%@, %@",zipcode,city];
        zipcode_lbl.frame = CGRectMake(38, street_number_lbl.frame.origin.y+street_number_lbl.frame.size.height, 250, 20);
        y1=y1+zipcode_lbl.frame.size.height;
    }
    else if ([zipcode isEqualToString: @""] && [city isEqualToString:@""])
    {
        zipcode_lbl.frame = CGRectMake(38, street_number_lbl.frame.origin.y+street_number_lbl.frame.size.height, 250, 0);
        y1=y1+zipcode_lbl.frame.size.height;
    }
    if (![country isEqualToString: @""]) {
        country_lbl.text=[NSString stringWithFormat:@"%@",country];
        country_lbl.frame = CGRectMake(38, zipcode_lbl.frame.origin.y+zipcode_lbl.frame.size.height, 250, 20);
        y1=y1+country_lbl.frame.size.height;
    }
    else if ([country isEqualToString: @""])
    {
        country_lbl.frame = CGRectMake(38, zipcode_lbl.frame.origin.y+zipcode_lbl.frame.size.height, 250, 0);
        y1=y1+country_lbl.frame.size.height;
    }
    
    //    if ([street isEqualToString: @""] && [number isEqualToString: @""])
    //    {
    //        [street_number_lbl setHighlighted:YES];
    //
    //
    //        if ([zipcode isEqualToString: @""])
    //        {
    //            [zipcode_lbl setHighlighted:YES];
    //            if ([city isEqualToString:@""]) {
    //                country_lbl.frame = street_number_lbl.frame;
    //                country_lbl.text = [NSString stringWithFormat:@"%@",country];
    //
    //            }
    //            else
    //            {
    //                city_lbl.frame = street_number_lbl.frame;
    //                country_lbl.frame = zipcode_lbl.frame;
    //                city_lbl.text = [NSString stringWithFormat:@"%@",city];
    //                country_lbl.text = [NSString stringWithFormat:@"%@",country];
    //            }
    //
    //        }
    //
    //        else
    //        {
    //            //country_lbl.frame = zipcode_lbl.frame;
    //
    //            zipcode_lbl.frame = street_number_lbl.frame;
    //
    //
    //
    //
    //            // street_number_lbl.text =[NSString stringWithFormat:@"%@\n %@",street,number];
    //            zipcode_lbl.text = [NSString stringWithFormat:@"%@,",zipcode];
    ////            [zipcode_lbl sizeToFit];
    //
    //            CGRect new_frame = city_lbl.frame;
    //            new_frame.origin.y = zipcode_lbl.frame.origin.y-3;
    //            new_frame.origin.x = zipcode_lbl.frame.origin.x+zipcode_lbl.frame.size.width+5;
    //            city_lbl.frame = new_frame;
    //
    //            CGRect new_country_frame = country_lbl.frame;
    //            new_country_frame.origin.y = zipcode_lbl.frame.origin.y+zipcode_lbl.frame.size.height;
    //            country_lbl.frame = new_country_frame;
    //
    //
    //            city_lbl.text = [NSString stringWithFormat:@"%@",city];
    //            country_lbl.text = [NSString stringWithFormat:@"%@",country];
    //        }
    //
    //
    //
    //    }
    //    else
    //    {
    //
    //        if ([zipcode isEqualToString: @""]) {
    //
    //            [zipcode_lbl setHighlighted:YES];
    //
    //            if ([city isEqualToString:@""]) {
    //                country_lbl.frame = zipcode_lbl.frame;
    //                street_number_lbl.text =[NSString stringWithFormat:@"%@\n, %@",street,number];
    //                country_lbl.text = [NSString stringWithFormat:@"%@",country];
    //            }
    //            else
    //            {
    //                city_lbl.frame = zipcode_lbl.frame;
    //
    //                street_number_lbl.text =[NSString stringWithFormat:@"%@\n, %@",street,number];
    //
    //                city_lbl.text = [NSString stringWithFormat:@"%@",city];
    //                country_lbl.text = [NSString stringWithFormat:@"%@",country];
    //            }
    //        }
    //
    //        else
    //        {
    //            street_number_lbl.text =[NSString stringWithFormat:@"%@\n, %@",street,number];
    //
    //            zipcode_lbl.text = [NSString stringWithFormat:@"%@,",zipcode];
    //            city_lbl.text = [NSString stringWithFormat:@"%@",city];
    //            country_lbl.text = [NSString stringWithFormat:@"%@",country];
    //        }
    //
    //
    //    }
    //
    //    [street_number_lbl setHighlighted:NO];
    //    [zipcode_lbl setHighlighted:NO];
    
}

-(void)loadBusinessView
{
    
    phonearr=[[NSMutableArray alloc]init];
    
    [act removeFromSuperview];
    
    business_view = [[UIView alloc] initWithFrame:CGRectMake(0, coverView.frame.origin.y+coverView.frame.size.height+45, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - coverView.frame.size.height)];
    
    business_view.backgroundColor = [UIColor whiteColor];
    
    
    
    [mainview addSubview:business_view];
    
    
    
    [business_view bringSubviewToFront:b_boxscroll];
    
    
    
    UIView *company_view = [[UIView alloc] init];
    
    company_view.backgroundColor = [UIColor clearColor];
    
    [business_view addSubview:company_view];
    
    
    
    UIView *designation_view = [[UIView alloc] init];
    
    designation_view.backgroundColor = [UIColor clearColor];
    
    [business_view addSubview:designation_view];
    
    
    
    UIView *mobile_view = [[UIView alloc] init];
    
    mobile_view.backgroundColor = [UIColor clearColor];
    
    [business_view addSubview:mobile_view];
    
    
    
    UIView *mail_view = [[UIView alloc] init];
    
    mail_view.backgroundColor = [UIColor clearColor];
    
    [business_view addSubview:mail_view];
    
    
    
    
    
    
    
    UILabel *b_street_label, *b_number_label, *b_zipcode_label, *b_city_label, *b_country_label;
    
    
    
    b_street_label = [[UILabel alloc] init];
    
    [business_view addSubview:b_street_label];
    
    
    
    b_number_label = [[UILabel alloc] init];
    
    [business_view addSubview:b_number_label];
    
    
    
    b_zipcode_label = [[UILabel alloc] init];
    
    [business_view addSubview:b_zipcode_label];
    
    
    
    b_city_label = [[UILabel alloc] init];
    
    [business_view addSubview:b_city_label];
    
    
    
    b_country_label = [[UILabel alloc] init];
    
    [business_view addSubview:b_country_label];
    
    
    
    
    
    UILabel *business_div1 = [[UILabel alloc] init];
    
    [company_view addSubview:business_div1];
    
    
    
    UILabel *business_div2 = [[UILabel alloc] init];
    
    [business_view addSubview:business_div2];
    
    
    
    UILabel *business_div3 = [[UILabel alloc] init];
    
    [business_view addSubview:business_div3];
    
    
    
    UILabel *business_div4 = [[UILabel alloc] init];
    
    [business_view addSubview:business_div4];
    
    
    
    UILabel *business_div5;
    
    
    
    UILabel *business_div6;
    
    if ([dict_profile objectForKey:@"b_company"] != false && ![[dict_profile objectForKey:@"b_company"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_company"] length] > 1 && ![[dict_profile objectForKey:@"b_company"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"b_company"] isEqualToString:@""] && ![[dict_profile objectForKey:@"b_company"] isKindOfClass:[NSNull class]]){
        
        
        
        nobusiness=1;
        
        
        
        company_view.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50.0f);
        
        
        
        
        
        UILabel *company_name = [[UILabel alloc] initWithFrame:CGRectMake(38, 0, [UIScreen mainScreen].bounds.size.width-35, 50.0f)];
        
        company_name.backgroundColor = [UIColor clearColor];
        
        [company_view addSubview:company_name];
        
        
        
        
        
        company_name.text = [NSString stringWithFormat:@"%@",[[dict_profile objectForKey:@"b_company"] stringByReplacingOccurrencesOfString:@"amp;" withString:@""]];
        
        [company_name setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
        
        
        
        [company_name setTextColor:[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f]];
        
        
        
        UIImageView *company_image = [[UIImageView alloc]initWithFrame:CGRectMake(13, 14, [UIImage imageNamed:@"company"].size.width/1.7, [UIImage imageNamed:@"company"].size.height/1.7)];
        
        
        
        company_image.image = [UIImage imageNamed:@"company"];
        
        [company_view addSubview:company_image];
        
        
        
        business_div1.frame = CGRectMake(0,  company_view.frame.origin.y+company_view.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        
        
        
        [business_div1 setBackgroundColor:[UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f]];
        
        
        
    }
    
    
    
    else
        
    {
        
        
        
        company_view.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0);
        
        
        
        business_div1.frame = CGRectMake(0,  company_view.frame.origin.y+company_view.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        
    }
    
    
    
    
    
    
    
    if ([dict_profile objectForKey:@"b_function"] != false && ![[dict_profile objectForKey:@"b_function"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_function"] length] > 1 && ![[dict_profile objectForKey:@"b_function"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"b_function"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"b_function"] isEqualToString:@""])
        
    {
        
        
        
        designation_view.frame = CGRectMake(0, business_div1.frame.origin.y+business_div1.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50.0f);
        
        
        
        UIImageView *desig_image = [[UIImageView alloc] initWithFrame:CGRectMake(13, 13, [UIImage imageNamed:@"designation"].size.width/1.7, [UIImage imageNamed:@"designation"].size.height/1.7)];
        
        desig_image.image = [UIImage imageNamed:@"designation"];
        
        [designation_view addSubview:desig_image];
        
        
        
        UILabel *desig_name = [[UILabel alloc] initWithFrame:CGRectMake(38, 0, [UIScreen mainScreen].bounds.size.width-35, 50.0f)];
        
        desig_name.backgroundColor = [UIColor clearColor];
        
        [designation_view addSubview:desig_name];
        
        
        
        desig_name.text = [NSString stringWithFormat:@"%@",[[dict_profile objectForKey:@"b_function"] stringByReplacingOccurrencesOfString:@"amp;" withString:@""]];
        
        
        
        [desig_name setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
        
        
        
        [desig_name setTextColor:[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f]];
        
        
        
        nobusiness=1;
        
        
        
        business_div2.frame = CGRectMake(0, designation_view.frame.origin.y+designation_view.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        
        business_div2.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f];
        
        //business_div2.backgroundColor = [UIColor redColor];
        
        
        
        DebugLog(@"DIVIDER2:%@",NSStringFromCGRect(business_div2.frame));
        
        
        
        //        divider_0.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
        
        
        
    }
    
    
    
    else
        
    {
        
        designation_view.frame = CGRectMake(0, business_div1.frame.origin.y+business_div1.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        
        
        
        business_div2.frame = CGRectMake(0, designation_view.frame.origin.y+designation_view.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        
        
        
    }
    
    
    
    
    
    
    
    if ([dict_profile objectForKey:@"b_phone_num"] != false && ![[dict_profile objectForKey:@"b_phone_num"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_phone_num"] length] > 1 && ![[dict_profile objectForKey:@"b_phone_num"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"b_phone_num"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"b_phone_num"] isEqualToString:@""]){
        
        
        
        
        
        mobile_view.frame = CGRectMake(0, business_div2.frame.origin.y+business_div2.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50.0f);
        
        
        
        UIImageView *mobile_image = [[UIImageView alloc] init];
        
        mobile_image.frame = CGRectMake(13, 17, [UIImage imageNamed:@"mobile_profile"].size.width/1.5,[UIImage imageNamed:@"mobile_profile"].size.height/1.5);
        
        
        
        mobile_image.image = [UIImage imageNamed:@"mobile_profile"];
        
        [mobile_view addSubview:mobile_image];
        
        
        
        UILabel *mobile_number = [[UILabel alloc] initWithFrame:CGRectMake(38, 0, [UIScreen mainScreen].bounds.size.width-35, 50.0f)];
        
        mobile_number.backgroundColor = [UIColor clearColor];
        
        mobile_number.text = [NSString stringWithFormat:@"%@%@",[dict_profile objectForKey:@"b_phone_pre"],[dict_profile objectForKey:@"b_phone_num"]];
        
        
        
        [mobile_number setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
        
        
        
        [mobile_number setTextColor:[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f]];
        
        
        
        [mobile_view addSubview:mobile_number];
        
        
        
        
        
        UIButton *mobile_button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, mobile_view.frame.size.height)];
        
        [mobile_button setBackgroundColor:[UIColor clearColor]];
        
        mobile_button.userInteractionEnabled = YES;
        
        [mobile_button setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        
        [mobile_button addTarget:self action:@selector(mobileFun:) forControlEvents:UIControlEventTouchUpInside];
        
        [mobile_view addSubview:mobile_button];
        
        
        
        mobile = [NSString stringWithFormat:@"%@%@",[dict_profile objectForKey:@"b_phone_pre"],[dict_profile objectForKey:@"b_phone_num"]];
        
        
        
        
        
        business_div3.frame = CGRectMake(0, mobile_view.frame.origin.y+mobile_view.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        
        business_div3.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f];
        
        
        
        numberstring=mobile_number.text;
        
        [phonearr addObject:numberstring];
        
        nobusiness=1;
        
        
        
    }
    
    
    
    else{
        
        
        
        mobile_view.frame = CGRectMake(0, business_div2.frame.origin.y+business_div2.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        
        
        
        business_div3.frame = CGRectMake(0, mobile_view.frame.origin.y+mobile_view.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        
        
        
    }
    
    
    
    
    
    if ([dict_profile objectForKey:@"b_email"] != false && ![[dict_profile objectForKey:@"b_email"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_email"] length] > 1 && ![[dict_profile objectForKey:@"b_email"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"b_email"] isEqualToString:@""])
        
    {
        
        DebugLog(@"BUSINESS DIVIDER3:%@",business_div3);
        
        mail_view.frame = CGRectMake(0, business_div3.frame.origin.y+business_div3.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50.0f);
        
        
        
        UIImageView *mail_image = [[UIImageView alloc] init];
        
        mail_image.frame= CGRectMake(13, 20, [UIImage imageNamed:@"email_profile"].size.width/1.5, [UIImage imageNamed:@"email_profile"].size.height/1.5);
        
        
        
        mail_image.backgroundColor = [UIColor clearColor];
        
        mail_image.image = [UIImage imageNamed:@"email_profile"];
        
        [mail_view addSubview:mail_image];
        
        
        
        UILabel *mail_id = [[UILabel alloc] initWithFrame:CGRectMake(38, 0, [UIScreen mainScreen].bounds.size.width-35, 50.0f)];
        
        mail_id.backgroundColor = [UIColor clearColor];
        
        [mail_view addSubview:mail_id];
        
        
        
        UIButton *mail_button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, mail_view.frame.size.height)];
        
        [mail_button setBackgroundColor:[UIColor clearColor]];
        
        mail_button.userInteractionEnabled = YES;
        
        [mail_button setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        
        [mail_button addTarget:self action:@selector(mailFun:) forControlEvents:UIControlEventTouchUpInside];
        
        [mail_view addSubview:mail_button];
        
        
        
        
        
        mail_id.text = [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_email"]];
        
        
        
        [mail_id setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
        
        
        
        [mail_id setTextColor:[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f]];
        
        
        
        
        
        business_div4.frame = CGRectMake(0, mail_view.frame.origin.y+mail_view.frame.size.height+2, [UIScreen mainScreen].bounds.size.width, 0.5f);
        
        business_div4.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f];
        
        
        
        DebugLog(@"DIVIDER4:%@",NSStringFromCGRect(business_div4.frame));
        
        
        
        nobusiness=1;
        
        
        
    }
    
    
    
    else
        
    {
        
        
        
        mail_view.frame = CGRectMake(0, business_div3.frame.origin.y+business_div3.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        
        
        
        business_div4.frame = CGRectMake(0, mail_view.frame.origin.y+mail_view.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        
        
        
    }
    
    
    
    
    
    //    address_view.frame = CGRectMake(0, business_div4.frame.origin.y
    
    //                                    +business_div4.frame.size.height, [UIScreen mainScreen].bounds.size.width, 100);
    
    
    
    
    
    
    
    if ([dict_profile objectForKey:@"b_street"] != false && ![[dict_profile objectForKey:@"b_street"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"b_street"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_street"] length]!=0 && ![[dict_profile objectForKey:@"b_street"] isEqualToString:@""] && ![[dict_profile objectForKey:@"b_street"] isEqualToString:@"0"])
        
    {
        
        business_street = [NSString stringWithFormat:@"%@,",[dict_profile objectForKey:@"b_street"]];
        
        
        
        b_street_label.frame = CGRectMake(38, business_div4.frame.origin.y+business_div4.frame.size.height+8, 130, 20);
        
        b_street_label.text = business_street;
        
        
        
        [b_street_label setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
        
        
        
        [b_street_label setTextColor:[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f]];
        
        
        
        [b_street_label sizeToFit];
        
        nobusiness=1;
        
    }
    
    
    
    else
        
    {
        
        business_street = @"";
        
        b_street_label.frame = CGRectMake(38, business_div4.frame.origin.y+business_div4.frame.size.height+8, 0, 0);
        
    }
    
    
    
    if ([dict_profile objectForKey:@"b_housenumber"] != false && ![[dict_profile objectForKey:@"b_housenumber"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_housenumber"] length]!=0 && ![[dict_profile objectForKey:@"b_housenumber"] isEqualToString:@""] && ![[dict_profile objectForKey:@"b_housenumber"] isEqualToString:@"0"])
        
    {
        
        business_number = [NSString stringWithFormat:@"%@,",[dict_profile objectForKey:@"b_housenumber"]];
        
        
        
        if ([business_street length]>0)
            
            b_number_label.frame = CGRectMake(b_street_label.frame.origin.x+b_street_label.frame.size.width+5, business_div4.frame.origin.y+business_div4.frame.size.height+8, 130, 20);
        
        else
            
            b_number_label.frame = CGRectMake(b_street_label.frame.origin.x+b_street_label.frame.size.width, business_div4.frame.origin.y+business_div4.frame.size.height+8, 130, 20);
        
        b_number_label.text = business_number;
        
        
        
        [b_number_label setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
        
        
        
        [b_number_label setTextColor:[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f]];
        
        
        
        [b_number_label sizeToFit];
        
        
        
        nobusiness=1;
        
    }
    
    else
        
    {
        
        business_number = @"";
        
        b_number_label.frame = CGRectMake(b_street_label.frame.origin.x+b_street_label.frame.size.width, b_street_label.frame.origin.y, 0, 0);
        
        
        
    }
    
    
    
    
    
    if ([dict_profile objectForKey:@"b_zipcode"] != false && ![[dict_profile objectForKey:@"b_zipcode"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_zipcode"] length]!=0 && ![[dict_profile objectForKey:@"b_zipcode"] isEqualToString:@""] && ![[dict_profile objectForKey:@"b_zipcode"] isEqualToString:@"0"])
        
        
        
    {
        
        nobusiness=1;
        
        business_zipcode = [NSString stringWithFormat:@"%@,",[dict_profile objectForKey:@"b_zipcode"]];
        
        if ([business_street length]>0) {
            
            
            
            b_zipcode_label.frame = CGRectMake(38, b_street_label.frame.origin.y+b_street_label.frame.size.height, 130, 20);
            
        }
        
        else
            
        {
            
            b_zipcode_label.frame = CGRectMake(38, b_number_label.frame.origin.y+b_number_label.frame.size.height, 130, 20);
            
        }
        
        
        
        b_zipcode_label.text = business_zipcode;
        
        
        
        [b_zipcode_label setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
        
        
        
        [b_zipcode_label setTextColor:[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f]];
        
        
        
        
        
        [b_zipcode_label sizeToFit];
        
        
        
    }
    
    else
        
    {
        
        business_zipcode = @"";
        
        
        
        if ([business_street length]>0) {
            
            
            
            b_zipcode_label.frame = CGRectMake(38, b_street_label.frame.origin.y+b_street_label.frame.size.height, 0, 0);
            
        }
        
        else
            
        {
            
            b_zipcode_label.frame = CGRectMake(38, b_number_label.frame.origin.y+b_number_label.frame.size.height, 0, 0);
            
        }
        
        
        
        
        
    }
    
    
    
    
    
    if ([dict_profile objectForKey:@"b_city"] != false && ![[dict_profile objectForKey:@"b_city"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"b_city"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_city"] length]!=0 && ![[dict_profile objectForKey:@"b_city"] isEqualToString:@""] && ![[dict_profile objectForKey:@"b_city"] isEqualToString:@"0"])
        
    {
        
        business_city = [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_city"]];
        
        if ([business_zipcode length]>0)
            
            b_city_label.frame = CGRectMake(b_zipcode_label.frame.origin.x+b_zipcode_label.frame.size.width+5, b_zipcode_label.frame.origin.y, 130, 20);
        
        else
            
            b_city_label.frame = CGRectMake(b_zipcode_label.frame.origin.x+b_zipcode_label.frame.size.width, b_zipcode_label.frame.origin.y, 130, 20);
        
        b_city_label.text = business_city;
        
        
        
        [b_city_label setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
        
        
        
        [b_city_label setTextColor:[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f]];
        
        
        
        
        
        
        
        [b_city_label sizeToFit];
        
        
        
        nobusiness=1;
        
    }
    
    
    
    else
        
    {
        
        business_city = @"";
        
        b_city_label.frame = CGRectMake(b_zipcode_label.frame.origin.x+b_zipcode_label.frame.size.width, b_zipcode_label.frame.origin.y, 0, 0);
        
        
        
    }
    
    
    
    
    
    if ([dict_profile objectForKey:@"b_country"] != false && ![[dict_profile objectForKey:@"b_country"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_country"] length]!=0 && ![[dict_profile objectForKey:@"b_country"] isEqualToString:@""] && ![[dict_profile objectForKey:@"b_country"] isEqualToString:@"0"])
        
    {
        
        business_country = [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_country"]];
        
        
        
        if ([business_zipcode length]>0) {
            
            
            
            b_country_label.frame = CGRectMake(38, b_zipcode_label.frame.origin.y+b_zipcode_label.frame.size.height, 130, 20);
            
        }
        
        else
            
        {
            
            b_country_label.frame = CGRectMake(38, b_city_label.frame.origin.y+b_city_label.frame.size.height, 130, 20);
            
            
            
        }
        
        
        
        b_country_label.text = business_country;
        
        
        
        [b_country_label setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
        
        
        
        [b_country_label setTextColor:[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f]];
        
        
        
        
        
        //        [b_country_label sizeToFit];
        
        
        
        //        address_view.frame = CGRectMake(0, business_div4.frame.origin.y+business_div4.frame.size.height, [UIScreen mainScreen].bounds.size.width,b_country_label.frame.origin.y+b_country_label.frame.size.height-(business_div4.frame.origin.y+business_div4.frame.size.height));
        
        
        
        nobusiness=1;
        
    }
    
    else
        
    {
        
        business_country = @"";
        
        
        
        if ([business_zipcode length]>0) {
            
            
            
            b_country_label.frame = CGRectMake(38, b_zipcode_label.frame.origin.y+b_zipcode_label.frame.size.height, 0, 0);
            
        }
        
        else
            
        {
            
            b_country_label.frame = CGRectMake(38, b_city_label.frame.origin.y+b_city_label.frame.size.height, 0, 0);
            
            
            
        }
        
        
        
    }
    
    
    
    
    
    if (![business_city  isEqualToString: @""] || ![business_street  isEqualToString: @""] || ![business_zipcode  isEqualToString: @""] || ![business_country  isEqualToString: @""] || ![business_number isEqualToString:@""])
                
    {
        
        
        
        //        address_view.frame = CGRectMake(0, business_div4.frame.origin.y+business_div4.frame.size.height, [UIScreen mainScreen].bounds.size.width, business_div4.frame.origin.y+business_div4.frame.size.height+b_street_label.frame.size.height+b_zipcode_label.frame.size.height+b_country_label.frame.size.height);
        
        
        
        UIView *address_view = [[UIView alloc] init];
        
        address_view.frame = CGRectMake(0, business_div4.frame.origin.y+business_div4.frame.size.height, [UIScreen mainScreen].bounds.size.width,b_country_label.frame.origin.y+b_country_label.frame.size.height-(business_div4.frame.origin.y+business_div4.frame.size.height)+8);
        
        address_view.backgroundColor = [UIColor clearColor];
        
        [business_view addSubview:address_view];
        
        
        
        UIImageView *address_image = [[UIImageView alloc] initWithFrame:CGRectMake(13, 8, [UIImage imageNamed:@"map_profile"].size.width/1.5, [UIImage imageNamed:@"map_profile"].size.height/1.5)];
        
        address_image.image = [UIImage imageNamed:@"map_profile"];
        
        [address_image setBackgroundColor:[UIColor clearColor]];
        
        [address_view addSubview:address_image];
        
        
        
        UIButton *address_button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, address_view.frame.size.height)];
        
        [address_button setBackgroundColor:[UIColor clearColor]];
        
        address_button.userInteractionEnabled = YES;
        
        [address_button setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        
        [address_button addTarget:self action:@selector(openmap:) forControlEvents:UIControlEventTouchUpInside];
        
        [address_view addSubview:address_button];
        
        
        
        business_div5 = [[UILabel alloc] init];
        
        business_div5.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f];
        
        [business_view addSubview:business_div5];
        
        //[business_view addSubview:business_div5];
        
        
        
        
        
        business_div5.frame = CGRectMake(0, address_view.frame.origin.y+address_view.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        
        
        
        //        if ([business_street length]>0)
        
        //        {
        
        //
        
        //            if ([business_zipcode length]>0)
        
        //            {
        
        //
        
        //                if ([business_country length]>0)
        
        //                {
        
        //
        
        //                    address_view.frame = CGRectMake(0, business_div4.frame.origin.y+business_div4.frame.size.height, [UIScreen mainScreen].bounds.size.width, business_div4.frame.origin.y+business_div4.frame.size.height+b_street_label.frame.size.height+b_zipcode_label.frame.size.height+b_country_label.frame.size.height);
        
        //
        
        //                }
        
        //                else
        
        //                {
        
        //                    address_view.frame = CGRectMake(0, business_div4.frame.origin.y+business_div4.frame.size.height, [UIScreen mainScreen].bounds.size.width, business_div4.frame.origin.y+business_div4.frame.size.height+b_street_label.frame.size.height+b_zipcode_label.frame.size.height);
        
        //                }
        
        //
        
        //            }
        
        //            else
        
        //            {
        
        //                if ([business_city length]>0)
        
        //                {
        
        //                    if ([business_country length]>0)
        
        //                    {
        
        //
        
        //                        address_view.frame = CGRectMake(0, business_div4.frame.origin.y+business_div4.frame.size.height, [UIScreen mainScreen].bounds.size.width, business_div4.frame.origin.y+business_div4.frame.size.height+b_street_label.frame.size.height+b_city_label.frame.size.height+b_country_label.frame.size.height);
        
        //
        
        //                    }
        
        //                    else
        
        //                    {
        
        //                        address_view.frame = CGRectMake(0, business_div4.frame.origin.y+business_div4.frame.size.height, [UIScreen mainScreen].bounds.size.width, business_div4.frame.origin.y+business_div4.frame.size.height+b_street_label.frame.size.height+b_city_label.frame.size.height);
        
        //                    }
        
        //
        
        //                }
        
        //                else
        
        //                {
        
        //                    if ([business_country length]>0)
        
        //                    {
        
        //
        
        //                        address_view.frame = CGRectMake(0, business_div4.frame.origin.y+business_div4.frame.size.height, [UIScreen mainScreen].bounds.size.width, business_div4.frame.origin.y+business_div4.frame.size.height+b_street_label.frame.size.height+b_country_label.frame.size.height);
        
        //
        
        //                    }
        
        //                    else
        
        //                    {
        
        //                        address_view.frame = CGRectMake(0, business_div4.frame.origin.y+business_div4.frame.size.height, [UIScreen mainScreen].bounds.size.width, business_div4.frame.origin.y+business_div4.frame.size.height+b_street_label.frame.size.height);
        
        //                    }
        
        //
        
        //                }
        
        //
        
        //            }
        
        //
        
        //
        
        //        }
        
        //        else
        
        //        {
        
        //            if ([business_number length]>0) {
        
        //
        
        //                if ([business_zipcode length]>0)
        
        //                {
        
        //
        
        //                    if ([business_country length]>0)
        
        //                    {
        
        //
        
        //                        address_view.frame = CGRectMake(0, business_div4.frame.origin.y+business_div4.frame.size.height, [UIScreen mainScreen].bounds.size.width, business_div4.frame.origin.y+business_div4.frame.size.height+b_number_label.frame.size.height+b_zipcode_label.frame.size.height+b_country_label.frame.size.height);
        
        //
        
        //                    }
        
        //                    else
        
        //                    {
        
        //                        address_view.frame = CGRectMake(0, business_div4.frame.origin.y+business_div4.frame.size.height, [UIScreen mainScreen].bounds.size.width, business_div4.frame.origin.y+business_div4.frame.size.height+b_number_label.frame.size.height+b_zipcode_label.frame.size.height);
        
        //                    }
        
        //
        
        //                }
        
        //                else
        
        //                {
        
        //                    if ([business_city length]>0)
        
        //                    {
        
        //                        if ([business_country length]>0)
        
        //                        {
        
        //
        
        //                            address_view.frame = CGRectMake(0, business_div4.frame.origin.y+business_div4.frame.size.height, [UIScreen mainScreen].bounds.size.width, business_div4.frame.origin.y+business_div4.frame.size.height+b_number_label.frame.size.height+b_city_label.frame.size.height+b_country_label.frame.size.height);
        
        //
        
        //                        }
        
        //                        else
        
        //                        {
        
        //                            address_view.frame = CGRectMake(0, business_div4.frame.origin.y+business_div4.frame.size.height, [UIScreen mainScreen].bounds.size.width, business_div4.frame.origin.y+business_div4.frame.size.height+b_number_label.frame.size.height+b_city_label.frame.size.height);
        
        //                        }
        
        //
        
        //                    }
        
        //                    else
        
        //                    {
        
        //                        if ([business_country length]>0)
        
        //                        {
        
        //
        
        //                            address_view.frame = CGRectMake(0, business_div4.frame.origin.y+business_div4.frame.size.height, [UIScreen mainScreen].bounds.size.width, business_div4.frame.origin.y+business_div4.frame.size.height+b_number_label.frame.size.height+b_country_label.frame.size.height);
        
        //
        
        //                        }
        
        //                        else
        
        //                        {
        
        //                            address_view.frame = CGRectMake(0, business_div4.frame.origin.y+business_div4.frame.size.height, [UIScreen mainScreen].bounds.size.width, business_div4.frame.origin.y+business_div4.frame.size.height+b_number_label.frame.size.height);
        
        //                        }
        
        //
        
        //                    }
        
        //
        
        //                }
        
        //
        
        //
        
        //            }
        
        //            else
        
        //            {
        
        //
        
        //                if ([business_zipcode length]>0)
        
        //                {
        
        //
        
        //                    if ([business_country length]>0)
        
        //                    {
        
        //
        
        //                        address_view.frame = CGRectMake(0, business_div4.frame.origin.y+business_div4.frame.size.height, [UIScreen mainScreen].bounds.size.width, business_div4.frame.origin.y+business_div4.frame.size.height+b_zipcode_label.frame.size.height+b_country_label.frame.size.height);
        
        //
        
        //                    }
        
        //                    else
        
        //                    {
        
        //                        address_view.frame = CGRectMake(0, business_div4.frame.origin.y+business_div4.frame.size.height, [UIScreen mainScreen].bounds.size.width, business_div4.frame.origin.y+business_div4.frame.size.height+b_zipcode_label.frame.size.height);
        
        //                    }
        
        //
        
        //                }
        
        //                else
        
        //                {
        
        //                    if ([business_city length]>0)
        
        //                    {
        
        //                        if ([business_country length]>0)
        
        //                        {
        
        //
        
        //                            address_view.frame = CGRectMake(0, business_div4.frame.origin.y+business_div4.frame.size.height, [UIScreen mainScreen].bounds.size.width, business_div4.frame.origin.y+business_div4.frame.size.height+b_city_label.frame.size.height+b_country_label.frame.size.height);
        
        //
        
        //                        }
        
        //                        else
        
        //                        {
        
        //                            address_view.frame = CGRectMake(0, business_div4.frame.origin.y+business_div4.frame.size.height, [UIScreen mainScreen].bounds.size.width, business_div4.frame.origin.y+business_div4.frame.size.height+b_city_label.frame.size.height);
        
        //                        }
        
        //
        
        //                    }
        
        //                    else
        
        //                    {
        
        //                        if ([business_country length]>0)
        
        //                        {
        
        //
        
        //                            address_view.frame = CGRectMake(0, business_div4.frame.origin.y+business_div4.frame.size.height, [UIScreen mainScreen].bounds.size.width, business_div4.frame.origin.y+business_div4.frame.size.height+b_country_label.frame.size.height);
        
        //
        
        //                        }
        
        //                        else
        
        //                        {
        
        //                            address_view.frame = CGRectMake(0, business_div4.frame.origin.y+business_div4.frame.size.height, [UIScreen mainScreen].bounds.size.width, business_div4.frame.origin.y+business_div4.frame.size.height);
        
        //                        }
        
        //
        
        //                    }
        
        //
        
        //                }
        
        //
        
        //
        
        //            }
        
        //
        
        //        }
        
        //
        
        //
        
        //
        
        
        
        
        
    }
    
    
    
    
    
    //////////////////////////////////// BUSINESS BOXSCROLL FOR SOCIAL GOES HERE...
    
    
    
    b_boxscroll = [[UIScrollView alloc] init];
    
    
    
    b_boxscroll.delegate=self;
    
    
    
    b_boxscroll.scrollEnabled=YES;
    
    
    
    b_boxscroll.showsHorizontalScrollIndicator = NO;
    
    
    
    //    CGRect new_boxscroll_frame = boxscroll.frame;
    
    //    new_boxscroll_frame.origin.y = country_lbl.frame.origin.y+country_lbl.frame.size.height+10;
    
    //    boxscroll.frame = new_boxscroll_frame;
    
    b_boxscroll.backgroundColor =[UIColor clearColor];
    
    
    
    b_boxscroll.userInteractionEnabled=YES;
    
    
    
    //    boxscroll.tag = 1;
    
    
    
    //    boxscroll.pagingEnabled = YES;
    
    b_boxscroll.autoresizingMask=UIViewAutoresizingNone;
    
    
    
    
    
    
    
    twitterbt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    
    
    [twitterbt setBackgroundImage:[UIImage imageNamed:@"twitter1.png"] forState:UIControlStateNormal];
    
    
    
    [twitterbt addTarget:self action:@selector(twitterfunc:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [b_boxscroll addSubview:twitterbt];
    
    
    
    
    
    
    
    pinterestbt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    
    
    [pinterestbt setBackgroundImage:[UIImage imageNamed:@"pinterest@2x1.png"] forState:UIControlStateNormal];
    
    
    
    [pinterestbt addTarget:self action:@selector(pinterestfunc:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [b_boxscroll addSubview:pinterestbt];
    
    
    
    
    
    
    
    facebookbt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    
    
    [facebookbt setBackgroundImage:[UIImage imageNamed:@"facebook@2x1.png"] forState:UIControlStateNormal];
    
    
    
    [facebookbt addTarget:self action:@selector(facebookfunc:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [b_boxscroll addSubview:facebookbt];
    
    
    
    
    
    
    
    gplusbt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    
    
    [gplusbt setBackgroundImage:[UIImage imageNamed:@"g+1.png"] forState:UIControlStateNormal];
    
    
    
    [gplusbt addTarget:self action:@selector(gplusfunc:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [b_boxscroll addSubview:gplusbt];
    
    
    
    
    
    
    
    youtubebt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    
    
    [youtubebt setBackgroundImage:[UIImage imageNamed:@"you-tube@2x1.png"] forState:UIControlStateNormal];
    
    
    
    [youtubebt addTarget:self action:@selector(youtubefunc:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [b_boxscroll addSubview:youtubebt];
    
    
    
    
    
    
    
    instagrambt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    
    
    [instagrambt setBackgroundImage:[UIImage imageNamed:@"instagram@2x1.png"] forState:UIControlStateNormal];
    
    
    
    [instagrambt addTarget:self action:@selector(instagramfunc:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [b_boxscroll addSubview:instagrambt];
    
    
    
    
    
    linkedinbt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    
    
    [linkedinbt setBackgroundImage:[UIImage imageNamed:@"linkedin1@2x1.png"] forState:UIControlStateNormal];
    
    
    
    [linkedinbt addTarget:self action:@selector(linkedinfunc:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [b_boxscroll addSubview:linkedinbt];
    
    
    
    
    
    
    
    skypebt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    
    
    [skypebt setBackgroundImage:[UIImage imageNamed:@"skype@2x1.png"] forState:UIControlStateNormal];
    
    
    
    [skypebt addTarget:self action:@selector(skypefunc:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [b_boxscroll addSubview:skypebt];
    
    
    
    
    
    
    
    soundcloudbt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    
    
    [soundcloudbt setBackgroundImage:[UIImage imageNamed:@"soundcloud@2x1.png"] forState:UIControlStateNormal];
    
    
    
    [soundcloudbt addTarget:self action:@selector(soundcloudfunc:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [b_boxscroll addSubview:soundcloudbt];
    
    
    
    
    
    
    
    vimeobt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    
    
    [vimeobt setBackgroundImage:[UIImage imageNamed:@"vimeo@2x1.png"] forState:UIControlStateNormal];
    
    
    
    [vimeobt addTarget:self action:@selector(vimeofunc:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [b_boxscroll addSubview:vimeobt];
    
    
    
    DebugLog(@"DICT PROFILE++++++=============>%@",dict_profile);
    
    
    
    if ([dict_profile objectForKey:@"s_twitter"] != false  && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_twitter"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_twitter"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_twitter"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_twitter"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_twitter"] isEqualToString:@"(null)"])
        
    {
        
        counter += 1;
        
        twitterbt.frame = CGRectMake(5, 4,31.5f,31);
        
    }else{
        
        
        
        twitterbt.frame = CGRectMake(5, 4,0,0);
        
    }
    
    
    
    
    
    if ([dict_profile objectForKey:@"s_pinterest"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_pinterest"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_pinterest"] isEqualToString:@""]  && ![[dict_profile objectForKey:@"s_pinterest"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_pinterest"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_pinterest"] isEqualToString:@"(null)"])
        
    {
        
        counter += 1;
        
        pinterestbt.frame = CGRectMake(twitterbt.frame.origin.x + twitterbt.frame.size.width + 5.0f, 4,31.5f,31);
        
    }else{
        
        
        
        pinterestbt.frame = CGRectMake(twitterbt.frame.origin.x + twitterbt.frame.size.width, 4,0,0);
        
    }
    
    
    
    
    
    if ([dict_profile objectForKey:@"s_facebook"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_facebook"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_facebook"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_facebook"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_facebook"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_facebook"] isEqualToString:@"(null)"])
        
    {
        
        counter += 1;
        
        facebookbt.frame = CGRectMake(pinterestbt.frame.origin.x + pinterestbt.frame.size.width + 5.0f, 4,31.5f,31);
        
    }else{
        
        
        
        facebookbt.frame = CGRectMake(pinterestbt.frame.origin.x + pinterestbt.frame.size.width, 4,0,0);
        
    }
    
    
    
    if ([dict_profile objectForKey:@"s_google"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_google"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_google"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_google"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_google"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_google"] isEqualToString:@"(null)"])
        
    {
        
        counter += 1;
        
        gplusbt.frame = CGRectMake(facebookbt.frame.origin.x + facebookbt.frame.size.width + 5.0f, 4,31.5f,31);
        
    }else{
        
        
        
        gplusbt.frame = CGRectMake(facebookbt.frame.origin.x + facebookbt.frame.size.width, 4,0,0);
        
    }
    
    
    
    if ([dict_profile objectForKey:@"s_youtube"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_youtube"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_youtube"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_youtube"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_youtube"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_youtube"] isEqualToString:@"(null)"])
        
    {
        
        counter += 1;
        
        youtubebt.frame = CGRectMake(gplusbt.frame.origin.x + gplusbt.frame.size.width + 5.0f, 4,31.5f,31);
        
    }else{
        
        
        
        youtubebt.frame = CGRectMake(gplusbt.frame.origin.x + gplusbt.frame.size.width, 4,0,0);
        
    }
    
    
    
    
    
    if ([dict_profile objectForKey:@"s_instagram"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_instagram"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_instagram"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_instagram"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_instagram"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_instagram"] isEqualToString:@"(null)"])
        
    {
        
        counter += 1;
        
        instagrambt.frame = CGRectMake(youtubebt.frame.origin.x + youtubebt.frame.size.width + 5.0f, 4,31.5f,31);
        
    }else{
        
        
        
        instagrambt.frame = CGRectMake(youtubebt.frame.origin.x + youtubebt.frame.size.width , 4,0,0);
        
    }
    
    
    
    if ([dict_profile objectForKey:@"s_linkedin"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_linkedin"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_linkedin"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_linkedin"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_linkedin"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_linkedin"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_linkedin"] isEqualToString:@"<null>"])
        
    {
        
        counter += 1;
        
        linkedinbt.frame = CGRectMake(instagrambt.frame.origin.x + instagrambt.frame.size.width + 5.0f, 4,31.5f,31);
        
    }else{
        
        
        
        linkedinbt.frame = CGRectMake(instagrambt.frame.origin.x + instagrambt.frame.size.width, 4,0,0);
        
    }
    
    
    
    
    
    if ([dict_profile objectForKey:@"s_skype"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_skype"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_skype"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_skype"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_skype"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_skype"] isEqualToString:@"(null)"])
        
    {
        
        counter += 1;
        
        skypebt.frame = CGRectMake(linkedinbt.frame.origin.x + linkedinbt.frame.size.width + 5.0f, 4,31.5f,31);
        
    }else{
        
        
        
        skypebt.frame = CGRectMake(linkedinbt.frame.origin.x + linkedinbt.frame.size.width, 4,0,0);
        
    }
    
    
    
    if ([dict_profile objectForKey:@"s_soundcloud"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_soundcloud"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_soundcloud"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_soundcloud"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_soundcloud"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_soundcloud"] isEqualToString:@"(null)"])
        
    {
        
        counter += 1;
        
        soundcloudbt.frame = CGRectMake(skypebt.frame.origin.x + skypebt.frame.size.width + 5.0f, 4,31.5f,31);
        
    }else{
        
        
        
        soundcloudbt.frame = CGRectMake(skypebt.frame.origin.x + skypebt.frame.size.width, 4,0,0);
        
    }
    
    
    
    if ([dict_profile objectForKey:@"s_vimeo"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_vimeo"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_vimeo"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_vimeo"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_vimeo"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_vimeo"] isEqualToString:@"(null)"])
        
    {
        
        counter += 1;
        
        vimeobt.frame = CGRectMake(soundcloudbt.frame.origin.x + soundcloudbt.frame.size.width + 5.0f, 4,31.5f,31);
        
    }else{
        
        
        
        vimeobt.frame = CGRectMake(soundcloudbt.frame.origin.x + soundcloudbt.frame.size.width, 4,0,0);
        
    }
    
    
    
    
    
    b_boxscroll.contentSize = CGSizeMake(counter*36.5f, 40);
    
    
    
    //     CGRect new_boxscroll_frame1 = boxscroll.frame;
    
    //    new_boxscroll_frame1.origin.y = country_lbl.frame.origin.y+country_lbl.frame.size.height+10;
    
    //    boxscroll.frame = new_boxscroll_frame1;
    
    
    
    
    
    [b_boxscroll removeFromSuperview];
    
    [business_view bringSubviewToFront:boxscroll];
    
    //    removeconbt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    
    
    if (b_boxscroll.contentSize.width >= 300) {
        
        
        
        DebugLog(@"BOXSCROLL CONTENT >=300");
        
        DebugLog(@"boxscroll framesize===>%f",business_div5.frame.origin.y+business_div5.frame.size.height+10);
        
        //
        
        b_boxscroll.frame =CGRectMake(10,business_div5.frame.origin.y+business_div5.frame.size.height, 300,40.0f);
        
        [business_view addSubview:b_boxscroll];
        
        
        
        business_div6 = [[UILabel alloc] init];
        
        business_div6.frame = CGRectMake(0, b_boxscroll.frame.origin.y + boxscroll.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        
        business_div6.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f];
        
        //        [business_view addSubview:business_div6];
        
        
        
        //
        
        //        toggleimage.frame= CGRectMake(66, boxscroll.frame.origin.y + boxscroll.frame.size.height+10, 188, 28.5f);  //115, 136.5f-60, 188.5f, 28.5f
        
        //        togglebt.frame = toggleimage.frame;
        
        //
        
        //        if ([user_id intValue] != [[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]intValue])
        
        //        {
        
        //            divider_5.frame = CGRectMake(0,  toggleimage.frame.origin.y+toggleimage.frame.size.height+10, [UIScreen mainScreen].bounds.size.width, 0.7f);
        
        //            [mainscroll addSubview:divider_5];
        
        ////            removeconbt.frame = CGRectMake(70, divider_5.frame.origin.y + divider_5.frame.size.height+10, 180, 30);
        
        //
        
        //            removeconbt.frame = CGRectMake(70, toggleimage.frame.origin.y + toggleimage.frame.size.height+20, 180, 30);
        
        //            removeconbt.layer.cornerRadius=5;
        
        //            removeconbt.titleLabel.font=[UIFont systemFontOfSize:16];
        
        //            [removeconbt setTitle:@"Remove Connection" forState:UIControlStateNormal];
        
        //        [removeconbt setTitleColor:[UIColor colorWithRed:141.0f/255.0f green:44.0f/255.0f blue:41.0f/255.0f alpha:1] forState:UIControlStateNormal];
        
        //            removeconbt.backgroundColor=[UIColor clearColor];
        
        //            [removeconbt addTarget:self action:@selector(removeConnection:) forControlEvents:UIControlEventTouchUpInside];
        
        ////            removeconbt.layer.borderColor=[[UIColor colorWithRed:141.0f/255.0f green:44.0f/255.0f blue:41.0f/255.0f alpha:1]CGColor];
        
        ////            removeconbt.layer.borderWidth= 2;
        
        //            [mainscroll addSubview:removeconbt];
        
        //        }
        
        //        else
        
        //        {
        
        //            removeconbt.frame = CGRectMake(70, boxscroll.frame.origin.y + boxscroll.frame.size.height+10, 0, 0);
        
        //        }
        
        // mainscroll.contentSize = CGSizeMake(320.0f, business_div6.frame.origin.y + business_div6.frame.size.height+35.0f);
        
        //
        
    }else{
        
        
        
        DebugLog(@"BOXSCROLL CONTENT <=300 ---- ELSE part");
        
        
        
        //        boxscroll.frame = CGRectMake((310 - boxscroll.contentSize.width)/2,divider_3.frame.origin.y+divider_3.frame.size.height+38, 300,40.0f);
        
        b_boxscroll.frame = CGRectMake((310 - b_boxscroll.contentSize.width)/2,business_div5.frame.origin.y+business_div5.frame.size.height, 300,40.0f);
        
        [business_view addSubview:b_boxscroll];
        
        
        
        business_div6 = [[UILabel alloc] init];
        
        business_div6.frame = CGRectMake(0,b_boxscroll.frame.origin.y + b_boxscroll.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        
        business_div6.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f];
        
        
        
        //        [business_view addSubview:business_div6];
        
        //
        
        //        toggleimage.frame= CGRectMake(66, boxscroll.frame.origin.y + boxscroll.frame.size.height+10, 188, 28.5f);  //115, 136.5f-60, 188.5f, 28.5f
        
        //        togglebt.frame = toggleimage.frame;
        
        //
        
        //        if ([user_id intValue] != [[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]intValue])
        
        //        {
        
        //            divider_5.frame = CGRectMake(0,  toggleimage.frame.origin.y+toggleimage.frame.size.height+10, [UIScreen mainScreen].bounds.size.width, 0.7f);
        
        //            [mainscroll addSubview:divider_5];
        
        //
        
        //            removeconbt.frame = CGRectMake(70, toggleimage.frame.origin.y + toggleimage.frame.size.height+20, 180, 30);
        
        //            removeconbt.layer.cornerRadius=5;
        
        //            removeconbt.titleLabel.font=[UIFont systemFontOfSize:16];
        
        //            [removeconbt setTitle:@"Remove Connection" forState:UIControlStateNormal];
        
        //        [removeconbt setTitleColor:[UIColor colorWithRed:141.0f/255.0f green:44.0f/255.0f blue:41.0f/255.0f alpha:1] forState:UIControlStateNormal];
        
        //            removeconbt.backgroundColor=[UIColor clearColor];
        
        //            [removeconbt addTarget:self action:@selector(removeConnection:) forControlEvents:UIControlEventTouchUpInside];
        
        //            [mainscroll addSubview:removeconbt];
        
        //        }
        
        //        else
        
        //        {
        
        //            removeconbt.frame = CGRectMake(70, boxscroll.frame.origin.y + boxscroll.frame.size.height+10, 0, 0);
        
        //        }
        
        // mainscroll.contentSize = CGSizeMake(320.0f, business_div6.frame.origin.y + business_div6.frame.size.height+35.0f);
        
        //        DebugLog(@"mainscroll contentsize: %f",mainscroll.contentSize.height);
        
    }
    
    DebugLog(@"divider frame: %@",NSStringFromCGRect(business_div5.frame));
    
    DebugLog(@"boxscroll frame: %@",NSStringFromCGRect(business_div5.frame));
    
    DebugLog(@"divider ends at: %f",business_div5.frame.origin.y+business_div5.frame.size.height);
    
    //
    
    if (counter != 0)
        
    {
        
        DebugLog(@"COUNTER========================>%d",counter);
        
        business_div6 = [[UILabel alloc] init];
        
        business_div6.frame = CGRectMake(0,  b_boxscroll.frame.origin.y+b_boxscroll.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        
        business_div6.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f];
        
        [business_view addSubview:business_div6];
        
    }
    
    else
        
    {
        
        business_div6 = [[UILabel alloc] init];
        
        business_div6.frame = CGRectMake(0,  b_boxscroll.frame.origin.y+b_boxscroll.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        
        business_div6.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f];
        
        [business_div6 setBackgroundColor:[UIColor redColor]];
        
        [business_view addSubview:business_div6];
        
    }
}

-(void)loadPersonalViews
{
    [act removeFromSuperview];
    phonearr=[[NSMutableArray alloc]init];
    personal_view = [[UIView alloc] initWithFrame:CGRectMake(0, coverView.frame.origin.y+coverView.frame.size.height+45, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - coverView.frame.size.height)];
    personal_view.backgroundColor = [UIColor whiteColor];
    
    [mainview addSubview:personal_view];
    
    [business_view bringSubviewToFront:b_boxscroll];
    
    mobileno = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [mobileno setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 40.0f, 0, 0)];
    
    mobileno.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    [mobileno setBackgroundImage:[UIImage imageNamed:@"detbuttonNormal"] forState:UIControlStateNormal];
    
    [mobileno setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateHighlighted];
    
    [mobileno setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateSelected];
    
    [mobileno.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
    
    [mobileno setTitleColor:[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f]forState:UIControlStateNormal];
    
    [mobileno addTarget:self action:@selector(mobileFun:) forControlEvents:UIControlEventTouchUpInside];
    
    [personal_view addSubview:mobileno];
    
    
    mobileimg = [[UIImageView alloc]init];//WithFrame:CGRectMake(10.0f, 10.0f, [UIImage imageNamed:@"mobile_profile"].size.width, [UIImage imageNamed:@"mobile_profile"].size.height+12)];//WithFrame:CGRectMake(15, 195-60, 14, 13.5f)];
    mobileimg.image=[UIImage imageNamed:@"mobile_profile"];
    [mobileno addSubview:mobileimg];
    
    
    divider_9 = [[UIView alloc]init];//WithFrame:CGRectMake(14.5f, mail_lb.frame.origin.y+mail_lb.frame.size.height+15, 291, 1)];
    
    [personal_view addSubview:divider_9];
    
    //==============Landline no. selection color==============//
    
    
    //========================================================//
    
    landphonelb = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [landphonelb setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 40.0f, 0, 0)];
    
    landphonelb.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    [landphonelb setBackgroundImage:[UIImage imageNamed:@"detbuttonNormal"] forState:UIControlStateNormal];
    
    [landphonelb setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateHighlighted];
    
    [landphonelb setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateSelected];
    
    [landphonelb.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
    
    [landphonelb setTitleColor:[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f]forState:UIControlStateNormal];
    
    [landphonelb addTarget:self action:@selector(landphoneFun:) forControlEvents:UIControlEventTouchUpInside];
    
    [personal_view addSubview:landphonelb];
    
    landphone_img = [[UIImageView alloc]init];//WithFrame:CGRectMake(13, 18, [UIImage imageNamed:@"home_profile"].size.width, [UIImage imageNamed:@"home_profile"].size.height+12)];//WithFrame:CGRectMake(15, 220-60, 14, 13.5f)];
    landphone_img.image=[UIImage imageNamed:@"home_profile"];
    [landphonelb addSubview:landphone_img];
    
    
    divider_1 = [[UIView alloc]init];//WithFrame:CGRectMake(14.5f, 248-60, 291, 1)];
    
    
    [personal_view addSubview:divider_1];
    
    //==============Mail selection color==============//
    
    //========================================================//
    
    mail_lb = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [mail_lb setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 40, 0, 0)];
    
    mail_lb.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    [mail_lb setBackgroundImage:[UIImage imageNamed:@"detbuttonNormal"] forState:UIControlStateNormal];
    
    [mail_lb setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateHighlighted];
    
    [mail_lb setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateSelected];
    
    [mail_lb.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
    
    [mail_lb setTitleColor:[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f]forState:UIControlStateNormal];
    
    [mail_lb addTarget:self action:@selector(mailFun:) forControlEvents:UIControlEventTouchUpInside];
    
    [personal_view addSubview:mail_lb];
    
    
    mailimg =[[UIImageView alloc]init];//WithFrame:CGRectMake(10, 10, [UIImage imageNamed:@"email_profile"].size.width, [UIImage imageNamed:@"email_profile"].size.height+10) ];//WithFrame:CGRectMake(15, divider_1.frame.origin.y+divider_1.frame.size.height+13, 16, 11)];
    mailimg.image=[UIImage imageNamed:@"email_profile"];
    [mail_lb addSubview:mailimg];
    
    
    
    
    //Checking for User Interaction
    
    if ([other isEqualToString:@"yes"]) {
        
        mobileno.userInteractionEnabled = YES;
        
        landphonelb.userInteractionEnabled = YES;
        
        mail_lb.userInteractionEnabled = YES;
        
        website_lb.userInteractionEnabled = YES;
    }else{
        
        mobileno.userInteractionEnabled = NO;
        
        landphonelb.userInteractionEnabled = NO;
        
        mail_lb.userInteractionEnabled = NO;
        
        //        website_lb.userInteractionEnabled = NO;
    }
    
    divider_2 = [[UIView alloc]init];//WithFrame:CGRectMake(14.5f, website_lb.frame.origin.y+website_lb.frame.size.height+11, 291, 1)];
    
    [personal_view addSubview:divider_2];
    
    //==============Address selection color==============//
    
    //========================================================//
    
    address_lb = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [address_lb setTitleEdgeInsets:UIEdgeInsetsMake(14.0f, 40, 0, 0)];
    
    address_lb.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    [address_lb setBackgroundImage:[UIImage imageNamed:@"detbuttonNormal"] forState:UIControlStateNormal];
    
    [address_lb setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateHighlighted];
    
    [address_lb setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateSelected];
    
    [address_lb.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
    
    address_lb.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    [address_lb setTitleColor:[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f]forState:UIControlStateNormal];
    
    [address_lb addTarget:self action:@selector(openmap:) forControlEvents:UIControlEventTouchUpInside];
    //
    [personal_view addSubview:address_lb];
    
    street_number_lbl = [[UILabel alloc] init];//WithFrame:CGRectMake(40, address_lb.frame.origin.y,200,27)];
    street_number_lbl.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
    street_number_lbl.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    street_number_lbl.backgroundColor = [UIColor clearColor];
    [address_lb addSubview:street_number_lbl];
    
    zipcode_lbl = [[UILabel alloc] init];//WithFrame:CGRectMake(40, street_number_lbl.frame.origin.y+street_number_lbl.frame.size.height,75,27)];
    zipcode_lbl.backgroundColor = [UIColor clearColor];
    zipcode_lbl.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
    zipcode_lbl.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    [address_lb addSubview:zipcode_lbl];
    
    //    city_lbl = [[UILabel alloc] initWithFrame:CGRectMake(zipcode_lbl.frame.origin.x+zipcode_lbl.frame.size.width, street_number_lbl.frame.origin.y+street_number_lbl.frame.size.height,100,27)];
    //    city_lbl.backgroundColor = [UIColor clearColor];
    //    city_lbl.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
    //    city_lbl.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    //    [address_lb addSubview:city_lbl];
    
    country_lbl = [[UILabel alloc] init];//WithFrame:CGRectMake(40, zipcode_lbl.frame.origin.y+zipcode_lbl.frame.size.height,100,27)];
    country_lbl.backgroundColor = [UIColor clearColor];
    country_lbl.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
    country_lbl.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    [address_lb addSubview:country_lbl];
    
    address_img=[[UIImageView alloc]init];//WithFrame:CGRectMake(15,divider_2.frame.origin.y+divider_2.frame.size.height+11,12,17)];
    address_img.image=[UIImage imageNamed:@"map_profile"];
    [address_lb addSubview:address_img];
    
    
    
    divider_3 = [[UIView alloc]init];//WithFrame:CGRectMake(14.5f, address_lb.frame.origin.y+address_lb.frame.size.height+5, 291, 1)];
    
    //    [mainscroll addSubview:divider_3];
    
    
    
    //
    //    street_number_lbl = [[UILabel alloc] initWithFrame:CGRectMake(40, address_lb.frame.origin.y,200,27)];
    //    street_number_lbl.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
    //    street_number_lbl.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    //    street_number_lbl.backgroundColor = [UIColor clearColor];
    //  [address_lb addSubview:street_number_lbl];
    //
    //    zipcode_lbl = [[UILabel alloc] initWithFrame:CGRectMake(40, street_number_lbl.frame.origin.y+street_number_lbl.frame.size.height,75,27)];
    //    zipcode_lbl.backgroundColor = [UIColor clearColor];
    //    zipcode_lbl.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
    //    zipcode_lbl.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    //    [address_lb addSubview:zipcode_lbl];
    //
    //    city_lbl = [[UILabel alloc] initWithFrame:CGRectMake(zipcode_lbl.frame.origin.x+zipcode_lbl.frame.size.width, street_number_lbl.frame.origin.y+street_number_lbl.frame.size.height,100,27)];
    //    city_lbl.backgroundColor = [UIColor clearColor];
    //    city_lbl.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
    //    city_lbl.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    //    [address_lb addSubview:city_lbl];
    //
    //    country_lbl = [[UILabel alloc] initWithFrame:CGRectMake(40, zipcode_lbl.frame.origin.y+zipcode_lbl.frame.size.height,100,27)];
    //    country_lbl.backgroundColor = [UIColor clearColor];
    //    country_lbl.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
    //    country_lbl.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    //    [address_lb addSubview:country_lbl];
    //
    //    address_img=[[UIImageView alloc]init];//WithFrame:CGRectMake(15,divider_2.frame.origin.y+divider_2.frame.size.height+11,12,17)];
    //    address_img.image=[UIImage imageNamed:@"map_profile"];
    //    [address_lb addSubview:address_img];
    //
    //
    //
    //    divider_3 = [[UIView alloc]init];//WithFrame:CGRectMake(14.5f, address_lb.frame.origin.y+address_lb.frame.size.height+5, 291, 1)];
    //
    [personal_view addSubview:divider_3];
    
    //    UILabel *business_div5;
    //
    //    UILabel *business_div6;
    
    //    [mainview removeFromSuperview];
    // [business_view removeFromSuperview];
    divider_5 = [[UIView alloc]init];
    divider_1.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
    divider_2.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
    divider_3.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
    divider_4.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
    divider_5.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
    [divider_9 setBackgroundColor:[UIColor colorWithRed:(239.0f / 255.0f) green:(239.0f / 255.0f) blue:(239.0f / 255.0f) alpha:1.0f]];
    
    
    if ([dict_profile objectForKey:@"mobile_num"] != false && ![[dict_profile objectForKey:@"mobile_num"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"mobile_num"] length] > 1 && ![[dict_profile objectForKey:@"mobile_num"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"mobile_num"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"mobile_num"] isEqualToString:@""])
    {
        
        mobile= [NSString stringWithFormat:@"%@%@",[dict_profile objectForKey:@"mobile_pre"],[dict_profile objectForKey:@"mobile_num"]];
        mobileArray = [[NSMutableArray alloc]init];
        [mobileArray addObject:mobile];
        [mobileno setHidden:NO];
        [mobileimg setHidden:NO];
        mobileimg.frame = CGRectMake(13, 20, [UIImage imageNamed:@"mobile_profile"].size.width/1.5,[UIImage imageNamed:@"mobile_profile"].size.height/1.5);
        //            mobileimg.frame = CGRectMake(13, 18, 14, 18);
        [mobileno setTitle:mobile forState:UIControlStateNormal];
        mobileno.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 50);
        //            mobileback.frame = CGRectMake(0, businesstext.frame.origin.y+businesstext.frame.size.height+7, [UIScreen mainScreen].bounds.size.width, 43);
        divider_9.frame = CGRectMake(0,  mobileno.frame.origin.y+mobileno.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        
        DebugLog(@"IF PART A ENTERING");
    }else{
        
        mobile=@"";
        
        mobileimg.frame = CGRectMake(13, 20, [UIImage imageNamed:@"mobile_profile"].size.width/1.5,0);
        //            mobileimg.frame = CGRectMake(13, 15, 14, 0);
        
        mobileno.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0);
        divider_9.frame = CGRectMake(0,  mobileno.frame.origin.y+mobileno.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
    }
    
    
    if ([dict_profile objectForKey:@"phone_num"] != false && ![[dict_profile objectForKey:@"phone_num"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"phone_num"] length] > 1 && ![[dict_profile objectForKey:@"phone_num"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"phone_num"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"phone_num"] isEqualToString:@""])
    {
        landline= [NSString stringWithFormat:@"%@%@",[dict_profile objectForKey:@"phone_pre"],[dict_profile objectForKey:@"phone_num"]];
        
        [landphonelb setHidden:NO];
        
        [landphone_img setHidden:NO];
        
        landphone_img.frame = CGRectMake(13, 20, [UIImage imageNamed:@"home_profile"].size.width/1.5, [UIImage imageNamed:@"home_profile"].size.height/1.5);
        //            landphone_img.frame = CGRectMake(13, 18, 14, 18);
        
        landphonelb.frame = CGRectMake(0, divider_9.frame.origin.y+divider_9.frame.size.height, [UIImage imageNamed:@"detbuttonNormal"].size.width, 50);
        [landphonelb setTitle:landline forState:UIControlStateNormal];
        divider_1.frame = CGRectMake(0,  landphonelb.frame.origin.y+landphonelb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        
        //            divider_1.frame = CGRectMake(0,  landphonelb.frame.origin.y+landphonelb.frame.size.height+3, [UIScreen mainScreen].bounds.size.width, 0.7f);
        //            landlineback.frame = CGRectMake(0, divider_9.frame.origin.y+divider_9.frame.size.height, [UIScreen mainScreen].bounds.size.width, 47);
        
    }else{
        
        landline=@"";
        
        landphone_img.frame = CGRectMake(13, 15, 14, 0);
        
        landphonelb.frame = CGRectMake(0, divider_9.frame.origin.y+divider_9.frame.size.height, [UIImage imageNamed:@"detbuttonNormal"].size.width, 0);
        
        divider_1.frame = CGRectMake(0,  landphonelb.frame.origin.y+landphonelb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        
        //        divider_1.frame = CGRectMake(0,  landphonelb.frame.origin.y+landphonelb.frame.size.height+3, [UIScreen mainScreen].bounds.size.width, 0);
    }
    
    if (mobile) {
        numberstring=mobile;
        [phonearr addObject:mobile];
    }
    else if (landline)
    {
        numberstring=landline;
        [phonearr addObject:landline];
    }
    
    
    
    //    if (![mobile isEqualToString:@""] && ![landline isEqualToString:@""]) {
    //
    //        divider_1.frame = CGRectMake(0,  landphonelb.frame.origin.y+landphonelb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
    //
    //    }
    
    //        else if (![mobile isEqualToString:@""] || ![landline isEqualToString:@""]) {
    //
    //            divider_1.frame = CGRectMake(0,  landphonelb.frame.origin.y+landphonelb.frame.size.height+3, [UIScreen mainScreen].bounds.size.width, 0.7f);
    //        }
    
    //    else if (![landline isEqualToString:@""]) {
    //
    //        divider_1.frame = CGRectMake(0,  landphonelb.frame.origin.y+landphonelb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
    //    }
    //
    //    else {
    //
    //        divider_1.frame = CGRectMake(0,  landphonelb.frame.origin.y+landphonelb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
    //    }
    
    
    if ([dict_profile objectForKey:@"email"] != false && ![[dict_profile objectForKey:@"email"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"email"] length] > 1 && ![[dict_profile objectForKey:@"email"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"email"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"email"] isEqualToString:@""])
    {
        mail= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"email"]];
        
        [mail_lb setHidden:NO];
        
        [mailimg setHidden:NO];
        
        mailimg.frame= CGRectMake(13, 20, [UIImage imageNamed:@"email_profile"].size.width/1.5, [UIImage imageNamed:@"email_profile"].size.height/1.5);
        //            mailimg.frame = CGRectMake(13, 18, 14.5f, 14);
        
        mail_lb.frame = CGRectMake(0, divider_1.frame.origin.y+divider_1.frame.size.height, [UIImage imageNamed:@"detbuttonNormal"].size.width,50);
        [mail_lb setTitle:mail forState:UIControlStateNormal];
        divider_2.frame = CGRectMake(0,  mail_lb.frame.origin.y+mail_lb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5);
        
        //        mailback.frame = CGRectMake(0, divider_1.frame.origin.y+divider_1.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50);
        
    }
    //    else if ([landline isEqualToString:@""] && [dict_profile objectForKey:@"email"] != false && ![[dict_profile objectForKey:@"email"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"email"] length] > 1 && ![[dict_profile objectForKey:@"email"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"email"] isEqualToString:@""] && ![[dict_profile objectForKey:@"email"] isKindOfClass:[NSNull class]])
    //    {
    //        mail= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"email"]];
    //
    //        [mail_lb setHidden:NO];
    //
    //        [mailimg setHidden:NO];
    //
    //        mailimg.frame= CGRectMake(13, 20, [UIImage imageNamed:@"email_profile"].size.width/1.5, [UIImage imageNamed:@"email_profile"].size.height/1.5);
    //        //            mailimg.frame = CGRectMake(13, 18, 14.5f, 14);
    //
    //        mail_lb.frame = CGRectMake(0, divider_9.frame.origin.y+divider_9.frame.size.height, [UIImage imageNamed:@"detbuttonNormal"].size.width, 50);
    //
    //        mailback.frame = CGRectMake(0, divider_9.frame.origin.y+divider_9.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50);
    //
    //        divider_2.frame = CGRectMake(0,  mail_lb.frame.origin.y+mail_lb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
    //
    //    }
    else{
        
        mail=@"";
        
        mailimg.frame = CGRectMake(13, 16, 16, 0);
        
        mail_lb.frame = CGRectMake(0, divider_1.frame.origin.y+divider_1.frame.size.height, [UIImage imageNamed:@"detbuttonNormal"].size.width, 0);
        divider_2.frame = CGRectMake(0,  mail_lb.frame.origin.y+mail_lb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
    }
    
    
    //        if ([dict_profile objectForKey:@"website"] != false && ![[dict_profile objectForKey:@"website"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"website"] length] > 1 && ![[dict_profile objectForKey:@"website"] isEqualToString:@"(null)"])
    //        {
    //            website= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"website"]];
    //
    //            [website_lb setHidden:NO];
    //
    //            [website_img setHidden:NO];
    //
    //            website_img.frame = CGRectMake(15, mail_lb.frame.origin.y+mail_lb.frame.size.height+17, 14.5f, 14);
    //
    //            website_lb.frame = CGRectMake(38, mail_lb.frame.origin.y+mail_lb.frame.size.height+13.5f, 275, 19);
    //        }else{
    //
    //            website=@"";
    //
    //            website_img.frame = CGRectMake(15, mail_lb.frame.origin.y+mail_lb.frame.size.height+17, 16, 0);
    //
    //            website_lb.frame = CGRectMake(38, mail_lb.frame.origin.y+mail_lb.frame.size.height+13.5f, 275, 0);
    //        }
    
    
    //        if (![mail isEqualToString:@""]){   //&& ![website isEqualToString:@""]) {
    //
    //            divider_2.frame = CGRectMake(0,  mail_lb.frame.origin.y+mail_lb.frame.size.height+16, [UIScreen mainScreen].bounds.size.width, 0.7f);
    //        }else
    
    //    if (![mail isEqualToString:@""]){  //|| ![website isEqualToString:@""]) {
    //
    //        divider_2.frame = CGRectMake(0,  mail_lb.frame.origin.y+mail_lb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
    //    }
    //
    //    //        else if (![website isEqualToString:@""]) {
    //    //
    //    //            divider_2.frame = CGRectMake(14.5f,  website_lb.frame.origin.y+website_lb.frame.size.height+16, 291, 0.5f);
    //    //        }
    //
    //    else {
    //
    //        divider_2.frame = CGRectMake(0,  mail_lb.frame.origin.y+mail_lb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
    //    }
    
    //======================================PERSONAL ADDRESS =============================//
    
    if ([dict_profile objectForKey:@"city"] != false && ![[dict_profile objectForKey:@"city"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"city"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"city"] isEqualToString:@""])
    {
        city= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"city"]];
    }
    else
        
        city=@"";
    
    if ([dict_profile objectForKey:@"street"] != false && ![[dict_profile objectForKey:@"street"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"street"] isEqualToString:@""] && ![[dict_profile objectForKey:@"street"] isKindOfClass:[NSNull class]])
    {
        street= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"street"]];
    }
    else
        
        street=@"";
    
    if ([dict_profile objectForKey:@"zipcode"] != false && ![[dict_profile objectForKey:@"zipcode"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"zipcode"] isEqualToString:@""] && ![[dict_profile objectForKey:@"zipcode"] isKindOfClass:[NSNull class]])
    {
        zipcode= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"zipcode"]];
    }
    
    else
        zipcode=@"";
    
    if ([dict_profile objectForKey:@"country"] != false && ![[dict_profile objectForKey:@"country"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"country"] isEqualToString:@""] && ![[dict_profile objectForKey:@"country"] isKindOfClass:[NSNull class]])
    {
        country= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"country"]];
    }
    else
        country=@"";
    
    if ([dict_profile objectForKey:@"housenumber"] != false && ![[dict_profile objectForKey:@"housenumber"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"housenumber"] isEqualToString:@""] && ![[dict_profile objectForKey:@"housenumber"] isKindOfClass:[NSNull class]])
    {
        number= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"housenumber"]];
    }
    else
        number= @"";
    
    if ( ![street isEqualToString: @""] || ![number isEqualToString: @""] || ![city isEqualToString: @""] || ![zipcode isEqualToString: @""] || ![country isEqualToString: @""])
    {
        
        [self setAddress];
        
        
        DebugLog(@"ADRESSSSSSSSSSSS========================>%@   STREET===>%@ NUMBER===>%@ ZIP===>%@ CITY===>%@ COUNTRY===>%@",address,street,number,zipcode,city,country);
        
        address_img.frame = CGRectMake(13, 8,[UIImage imageNamed:@"map_profile"].size.width/1.5,[UIImage imageNamed:@"map_profile"].size.height/1.5);
        
        
        //============================Dynamic label height with corresponding to adress string count======================//
        
        //        CGSize maximumLabelSize = CGSizeMake(296, FLT_MAX);
        //
        //        CGSize expectedLabelSize = [address sizeWithFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]
        //
        //                                       constrainedToSize:maximumLabelSize
        //
        //                                           lineBreakMode:NSLineBreakByWordWrapping];
        //
        //        //adjust the label the the new height.
        //
        //        CGRect newFrame = CGRectMake(0, divider_2.frame.origin.y+divider_2.frame.size.height,[UIImage imageNamed:@"detbuttonNormal"].size.width,[UIImage imageNamed:@"detbuttonNormal"].size.height);//address_lb.frame;
        //
        //        newFrame.size.height = expectedLabelSize.height;
        
        //        address_lb.frame = newFrame;//CGRectMake(38, address_img.frame.origin.y,280,newFrame.size.height);
        
        //        [address_lb sizeToFit];
        
        //            addressback.frame = CGRectMake(0, divider_2.frame.origin.y+divider_2.frame.size.height, [UIScreen mainScreen].bounds.size.width, 90);
        
        address_lb.frame = CGRectMake(0, divider_2.frame.origin.y+divider_2.frame.size.height, [[UIScreen mainScreen] bounds].size.width, y1+8);
        divider_3.frame = CGRectMake(0, address_lb.frame.origin.y+address_lb.frame.size.height, [[UIScreen mainScreen] bounds].size.width, 0.5);
    }
    else{
        
        address_img.frame = CGRectMake(13, 4,12,0);
        
        address_lb.frame = CGRectMake(0, divider_2.frame.origin.y+divider_2.frame.size.height, [UIImage imageNamed:@"detbuttonNormal"].size.width, 0);
        divider_3.frame = CGRectMake(0, address_lb.frame.origin.y+address_lb.frame.size.height, [[UIScreen mainScreen] bounds].size.width, 0);
    }
    
    //Divider 3=======================|||
    
    //    if (![number isEqualToString:@""] ) {
    //
    //        if (![city isEqualToString:@""]) {
    //
    //            if (![country isEqualToString:@""]) {
    //
    //                divider_3.frame = CGRectMake(0,  address_lb.frame.origin.y+street_number_lbl.frame.size.height+city_lbl.frame.size.height+country_lbl.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
    //
    //                address_lb.frame = CGRectMake(0, divider_2.frame.origin.y+divider_2.frame.size.height, [UIScreen mainScreen].bounds.size.width, divider_3.frame.origin.y-divider_2.frame.origin.y+2);
    //            }
    //            else
    //            {
    //                divider_3.frame = CGRectMake(0,  address_lb.frame.origin.y+street_number_lbl.frame.size.height+city_lbl.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
    //                address_lb.frame = CGRectMake(0, divider_2.frame.origin.y+divider_2.frame.size.height, [UIScreen mainScreen].bounds.size.width, divider_3.frame.origin.y-divider_2.frame.origin.y+2);
    //            }
    //
    //        }
    //        else
    //        {
    //            if (![country isEqualToString:@""]) {
    //
    //                divider_3.frame = CGRectMake(0,  address_lb.frame.origin.y+street_number_lbl.frame.size.height+country_lbl.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
    //
    //                address_lb.frame = CGRectMake(0, divider_2.frame.origin.y+divider_2.frame.size.height, [UIScreen mainScreen].bounds.size.width, divider_3.frame.origin.y-divider_2.frame.origin.y+2);
    //
    //            }
    //            else
    //            {
    //                divider_3.frame = CGRectMake(0,  address_lb.frame.origin.y+street_number_lbl.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
    //
    //                address_lb.frame = CGRectMake(0, divider_2.frame.origin.y+divider_2.frame.size.height, [UIScreen mainScreen].bounds.size.width, divider_3.frame.origin.y-divider_2.frame.origin.y+2);
    //
    //            }
    //
    //
    //        }
    //
    //
    //    }
    //    else
    //    {
    //
    //        if (![city isEqualToString:@""]) {
    //
    //            if (![country isEqualToString:@""]) {
    //
    //                divider_3.frame = CGRectMake(0,  address_lb.frame.origin.y+city_lbl.frame.size.height+country_lbl.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
    //
    //                address_lb.frame = CGRectMake(0, divider_2.frame.origin.y+divider_2.frame.size.height, [UIScreen mainScreen].bounds.size.width, divider_3.frame.origin.y-divider_2.frame.origin.y+2);
    //
    //            }
    //            else
    //            {
    //                divider_3.frame = CGRectMake(0,  address_lb.frame.origin.y+city_lbl.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
    //
    //                address_lb.frame = CGRectMake(0, divider_2.frame.origin.y+divider_2.frame.size.height, [UIScreen mainScreen].bounds.size.width, divider_3.frame.origin.y-divider_2.frame.origin.y+2);
    //
    //            }
    //
    //        }
    //        else
    //        {
    //            if (![country isEqualToString:@""]) {
    //
    //                divider_3.frame = CGRectMake(0,  address_lb.frame.origin.y+country_lbl.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
    //
    //                address_lb.frame = CGRectMake(0, divider_2.frame.origin.y+divider_2.frame.size.height, [UIScreen mainScreen].bounds.size.width, divider_3.frame.origin.y-divider_2.frame.origin.y+2);
    //
    //            }
    //            else
    //            {
    //                divider_3.frame = CGRectMake(0,  address_lb.frame.origin.y+address_lb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
    //
    //                address_lb.frame = CGRectMake(0, divider_2.frame.origin.y+divider_2.frame.size.height, [UIScreen mainScreen].bounds.size.width, divider_3.frame.origin.y-divider_2.frame.origin.y+2);
    //
    //            }
    //
    //
    //        }
    //
    //    }
    
    
    
    ////////////////////////////////////BOXSCROLL FOR SOCIAL GOES HERE...
    
    boxscroll = [[UIScrollView alloc] init];
    
    boxscroll.delegate=self;
    
    boxscroll.scrollEnabled=YES;
    
    boxscroll.showsHorizontalScrollIndicator = NO;
    
    //    CGRect new_boxscroll_frame = boxscroll.frame;
    //    new_boxscroll_frame.origin.y = country_lbl.frame.origin.y+country_lbl.frame.size.height+10;
    //    boxscroll.frame = new_boxscroll_frame;
    boxscroll.backgroundColor =[UIColor clearColor];
    
    boxscroll.userInteractionEnabled=YES;
    
    //    boxscroll.tag = 1;
    
    //    boxscroll.pagingEnabled = YES;
    boxscroll.autoresizingMask=UIViewAutoresizingNone;
    
    
    
    twitterbt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [twitterbt setBackgroundImage:[UIImage imageNamed:@"twitter1.png"] forState:UIControlStateNormal];
    
    [twitterbt addTarget:self action:@selector(twitterfunc:) forControlEvents:UIControlEventTouchUpInside];
    
    [boxscroll addSubview:twitterbt];
    
    
    
    pinterestbt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [pinterestbt setBackgroundImage:[UIImage imageNamed:@"pinterest@2x1.png"] forState:UIControlStateNormal];
    
    [pinterestbt addTarget:self action:@selector(pinterestfunc:) forControlEvents:UIControlEventTouchUpInside];
    
    [boxscroll addSubview:pinterestbt];
    
    
    
    facebookbt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [facebookbt setBackgroundImage:[UIImage imageNamed:@"facebook@2x1.png"] forState:UIControlStateNormal];
    
    [facebookbt addTarget:self action:@selector(facebookfunc:) forControlEvents:UIControlEventTouchUpInside];
    
    [boxscroll addSubview:facebookbt];
    
    
    
    gplusbt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [gplusbt setBackgroundImage:[UIImage imageNamed:@"g+1.png"] forState:UIControlStateNormal];
    
    [gplusbt addTarget:self action:@selector(gplusfunc:) forControlEvents:UIControlEventTouchUpInside];
    
    [boxscroll addSubview:gplusbt];
    
    
    
    youtubebt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [youtubebt setBackgroundImage:[UIImage imageNamed:@"you-tube@2x1.png"] forState:UIControlStateNormal];
    
    [youtubebt addTarget:self action:@selector(youtubefunc:) forControlEvents:UIControlEventTouchUpInside];
    
    [boxscroll addSubview:youtubebt];
    
    
    
    instagrambt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [instagrambt setBackgroundImage:[UIImage imageNamed:@"instagram@2x1.png"] forState:UIControlStateNormal];
    
    [instagrambt addTarget:self action:@selector(instagramfunc:) forControlEvents:UIControlEventTouchUpInside];
    
    [boxscroll addSubview:instagrambt];
    
    
    linkedinbt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [linkedinbt setBackgroundImage:[UIImage imageNamed:@"linkedin1@2x1.png"] forState:UIControlStateNormal];
    
    [linkedinbt addTarget:self action:@selector(linkedinfunc:) forControlEvents:UIControlEventTouchUpInside];
    
    [boxscroll addSubview:linkedinbt];
    
    
    
    skypebt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [skypebt setBackgroundImage:[UIImage imageNamed:@"skype@2x1.png"] forState:UIControlStateNormal];
    
    [skypebt addTarget:self action:@selector(skypefunc:) forControlEvents:UIControlEventTouchUpInside];
    
    [boxscroll addSubview:skypebt];
    
    
    
    soundcloudbt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [soundcloudbt setBackgroundImage:[UIImage imageNamed:@"soundcloud@2x1.png"] forState:UIControlStateNormal];
    
    [soundcloudbt addTarget:self action:@selector(soundcloudfunc:) forControlEvents:UIControlEventTouchUpInside];
    
    [boxscroll addSubview:soundcloudbt];
    
    
    
    vimeobt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [vimeobt setBackgroundImage:[UIImage imageNamed:@"vimeo@2x1.png"] forState:UIControlStateNormal];
    
    [vimeobt addTarget:self action:@selector(vimeofunc:) forControlEvents:UIControlEventTouchUpInside];
    
    [boxscroll addSubview:vimeobt];
    
    DebugLog(@"DICT PROFILE++++++=============>%@",dict_profile);
    
    if ([dict_profile objectForKey:@"s_twitter"] != false  && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_twitter"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_twitter"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_twitter"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_twitter"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_twitter"] isEqualToString:@"(null)"])
    {
        counter += 1;
        twitterbt.frame = CGRectMake(5, 4,31.5f,31);
    }else{
        
        twitterbt.frame = CGRectMake(5, 4,0,0);
    }
    
    
    if ([dict_profile objectForKey:@"s_pinterest"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_pinterest"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_pinterest"] isEqualToString:@""]  && ![[dict_profile objectForKey:@"s_pinterest"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_pinterest"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_pinterest"] isEqualToString:@"(null)"])
    {
        counter += 1;
        pinterestbt.frame = CGRectMake(twitterbt.frame.origin.x + twitterbt.frame.size.width + 5.0f, 4,31.5f,31);
    }else{
        
        pinterestbt.frame = CGRectMake(twitterbt.frame.origin.x + twitterbt.frame.size.width, 4,0,0);
    }
    
    
    if ([dict_profile objectForKey:@"s_facebook"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_facebook"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_facebook"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_facebook"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_facebook"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_facebook"] isEqualToString:@"(null)"])
    {
        counter += 1;
        facebookbt.frame = CGRectMake(pinterestbt.frame.origin.x + pinterestbt.frame.size.width + 5.0f, 4,31.5f,31);
    }else{
        
        facebookbt.frame = CGRectMake(pinterestbt.frame.origin.x + pinterestbt.frame.size.width, 4,0,0);
    }
    
    if ([dict_profile objectForKey:@"s_google"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_google"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_google"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_google"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_google"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_google"] isEqualToString:@"(null)"])
    {
        counter += 1;
        gplusbt.frame = CGRectMake(facebookbt.frame.origin.x + facebookbt.frame.size.width + 5.0f, 4,31.5f,31);
    }else{
        
        gplusbt.frame = CGRectMake(facebookbt.frame.origin.x + facebookbt.frame.size.width, 4,0,0);
    }
    
    if ([dict_profile objectForKey:@"s_youtube"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_youtube"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_youtube"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_youtube"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_youtube"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_youtube"] isEqualToString:@"(null)"])
    {
        counter += 1;
        youtubebt.frame = CGRectMake(gplusbt.frame.origin.x + gplusbt.frame.size.width + 5.0f, 4,31.5f,31);
    }else{
        
        youtubebt.frame = CGRectMake(gplusbt.frame.origin.x + gplusbt.frame.size.width, 4,0,0);
    }
    
    
    if ([dict_profile objectForKey:@"s_instagram"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_instagram"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_instagram"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_instagram"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_instagram"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_instagram"] isEqualToString:@"(null)"])
    {
        counter += 1;
        instagrambt.frame = CGRectMake(youtubebt.frame.origin.x + youtubebt.frame.size.width + 5.0f, 4,31.5f,31);
    }else{
        
        instagrambt.frame = CGRectMake(youtubebt.frame.origin.x + youtubebt.frame.size.width , 4,0,0);
    }
    
    if ([dict_profile objectForKey:@"s_linkedin"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_linkedin"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_linkedin"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_linkedin"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_linkedin"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_linkedin"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_linkedin"] isEqualToString:@"<null>"])
    {
        counter += 1;
        linkedinbt.frame = CGRectMake(instagrambt.frame.origin.x + instagrambt.frame.size.width + 5.0f, 4,31.5f,31);
    }else{
        
        linkedinbt.frame = CGRectMake(instagrambt.frame.origin.x + instagrambt.frame.size.width, 4,0,0);
    }
    
    
    if ([dict_profile objectForKey:@"s_skype"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_skype"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_skype"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_skype"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_skype"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_skype"] isEqualToString:@"(null)"])
    {
        counter += 1;
        skypebt.frame = CGRectMake(linkedinbt.frame.origin.x + linkedinbt.frame.size.width + 5.0f, 4,31.5f,31);
    }else{
        
        skypebt.frame = CGRectMake(linkedinbt.frame.origin.x + linkedinbt.frame.size.width, 4,0,0);
    }
    
    if ([dict_profile objectForKey:@"s_soundcloud"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_soundcloud"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_soundcloud"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_soundcloud"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_soundcloud"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_soundcloud"] isEqualToString:@"(null)"])
    {
        counter += 1;
        soundcloudbt.frame = CGRectMake(skypebt.frame.origin.x + skypebt.frame.size.width + 5.0f, 4,31.5f,31);
    }else{
        
        soundcloudbt.frame = CGRectMake(skypebt.frame.origin.x + skypebt.frame.size.width, 4,0,0);
    }
    
    if ([dict_profile objectForKey:@"s_vimeo"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_vimeo"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_vimeo"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_vimeo"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_vimeo"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_vimeo"] isEqualToString:@"(null)"])
    {
        counter += 1;
        vimeobt.frame = CGRectMake(soundcloudbt.frame.origin.x + soundcloudbt.frame.size.width + 5.0f, 4,31.5f,31);
    }else{
        
        vimeobt.frame = CGRectMake(soundcloudbt.frame.origin.x + soundcloudbt.frame.size.width, 4,0,0);
    }
    
    
    boxscroll.contentSize = CGSizeMake(counter*36.5f, 40);
    
    //     CGRect new_boxscroll_frame1 = boxscroll.frame;
    //    new_boxscroll_frame1.origin.y = country_lbl.frame.origin.y+country_lbl.frame.size.height+10;
    //    boxscroll.frame = new_boxscroll_frame1;
    
    
    [boxscroll removeFromSuperview];
    [personal_view bringSubviewToFront:boxscroll];
    //    removeconbt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    if (boxscroll.contentSize.width >= 300) {
        
        DebugLog(@"BOXSCROLL CONTENT >=300");
        DebugLog(@"boxscroll framesize===>%f",divider_3.frame.origin.y+divider_3.frame.size.height+10);
        //
        boxscroll.frame =CGRectMake(10,divider_3.frame.origin.y+divider_3.frame.size.height, [UIScreen mainScreen].bounds.size.width,40.0f);
        [personal_view addSubview:boxscroll];
        divider_5 = [[UIView alloc]init];
        divider_5.frame = CGRectMake(0, boxscroll.frame.origin.y + boxscroll.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        
        divider_5.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f];
        //        [personal_view addSubview:divider_5];
        
        //
        //        toggleimage.frame= CGRectMake(66, boxscroll.frame.origin.y + boxscroll.frame.size.height+10, 188, 28.5f);  //115, 136.5f-60, 188.5f, 28.5f
        //        togglebt.frame = toggleimage.frame;
        //
        //        if ([user_id intValue] != [[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]intValue])
        //        {
        //            divider_5.frame = CGRectMake(0,  toggleimage.frame.origin.y+toggleimage.frame.size.height+10, [UIScreen mainScreen].bounds.size.width, 0.7f);
        //            [mainscroll addSubview:divider_5];
        ////            removeconbt.frame = CGRectMake(70, divider_5.frame.origin.y + divider_5.frame.size.height+10, 180, 30);
        //
        //            removeconbt.frame = CGRectMake(70, toggleimage.frame.origin.y + toggleimage.frame.size.height+20, 180, 30);
        //            removeconbt.layer.cornerRadius=5;
        //            removeconbt.titleLabel.font=[UIFont systemFontOfSize:16];
        //            [removeconbt setTitle:@"Remove Connection" forState:UIControlStateNormal];
        //        [removeconbt setTitleColor:[UIColor colorWithRed:141.0f/255.0f green:44.0f/255.0f blue:41.0f/255.0f alpha:1] forState:UIControlStateNormal];
        //            removeconbt.backgroundColor=[UIColor clearColor];
        //            [removeconbt addTarget:self action:@selector(removeConnection:) forControlEvents:UIControlEventTouchUpInside];
        ////            removeconbt.layer.borderColor=[[UIColor colorWithRed:141.0f/255.0f green:44.0f/255.0f blue:41.0f/255.0f alpha:1]CGColor];
        ////            removeconbt.layer.borderWidth= 2;
        //            [mainscroll addSubview:removeconbt];
        //        }
        //        else
        //        {
        //            removeconbt.frame = CGRectMake(70, boxscroll.frame.origin.y + boxscroll.frame.size.height+10, 0, 0);
        //        }
        // mainscroll.contentSize = CGSizeMake(320.0f, divider_5.frame.origin.y + divider_5.frame.size.height+35.0f);
        //
    }else{
        
        //DebugLog(@"BOXSCROLL CONTENT <=300 ---- ELSE part");
        
        //        boxscroll.frame = CGRectMake((310 - boxscroll.contentSize.width)/2,divider_3.frame.origin.y+divider_3.frame.size.height+38, 300,40.0f);
        boxscroll.frame = CGRectMake((310 - boxscroll.contentSize.width)/2,divider_3.frame.origin.y+divider_3.frame.size.height, [UIScreen mainScreen].bounds.size.width,40.0f);
        [personal_view addSubview:boxscroll];
        divider_5 = [[UIView alloc]init];
        divider_5.frame = CGRectMake(0,boxscroll.frame.origin.y + boxscroll.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        divider_5.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f];
        //        [personal_view addSubview:divider_5];
        //
        //        toggleimage.frame= CGRectMake(66, boxscroll.frame.origin.y + boxscroll.frame.size.height+10, 188, 28.5f);  //115, 136.5f-60, 188.5f, 28.5f
        //        togglebt.frame = toggleimage.frame;
        //
        //        if ([user_id intValue] != [[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]intValue])
        //        {
        //            divider_5.frame = CGRectMake(0,  toggleimage.frame.origin.y+toggleimage.frame.size.height+10, [UIScreen mainScreen].bounds.size.width, 0.7f);
        //            [mainscroll addSubview:divider_5];
        //
        //            removeconbt.frame = CGRectMake(70, toggleimage.frame.origin.y + toggleimage.frame.size.height+20, 180, 30);
        //            removeconbt.layer.cornerRadius=5;
        //            removeconbt.titleLabel.font=[UIFont systemFontOfSize:16];
        //            [removeconbt setTitle:@"Remove Connection" forState:UIControlStateNormal];
        //        [removeconbt setTitleColor:[UIColor colorWithRed:141.0f/255.0f green:44.0f/255.0f blue:41.0f/255.0f alpha:1] forState:UIControlStateNormal];
        //            removeconbt.backgroundColor=[UIColor clearColor];
        //            [removeconbt addTarget:self action:@selector(removeConnection:) forControlEvents:UIControlEventTouchUpInside];
        //            [mainscroll addSubview:removeconbt];
        //        }
        //        else
        //        {
        //            removeconbt.frame = CGRectMake(70, boxscroll.frame.origin.y + boxscroll.frame.size.height+10, 0, 0);
        //        }
        // mainscroll.contentSize = CGSizeMake(320.0f, divider_5.frame.origin.y + divider_5.frame.size.height+35.0f);
        //        DebugLog(@"mainscroll contentsize: %f",mainscroll.contentSize.height);
    }
    DebugLog(@"divider frame: %@",NSStringFromCGRect(divider_3.frame));
    DebugLog(@"boxscroll frame: %@",NSStringFromCGRect(boxscroll.frame));
    DebugLog(@"divider ends at: %f",divider_3.frame.origin.y+divider_3.frame.size.height);
    //
    if (counter != 0)
    {
        DebugLog(@"COUNTER========================>%d",counter);
        divider_5.frame = CGRectMake(0,  boxscroll.frame.origin.y+boxscroll.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        divider_5.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f];
        [personal_view addSubview:divider_5];
    }
    else
    {
        divider_5.frame = CGRectMake(0,  boxscroll.frame.origin.y+boxscroll.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        divider_5.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f];
        [personal_view addSubview:divider_5];
    }
    
}

-(void)changecoloragain1
{
     DebugLog(@"CHANGE COLOR");
    //    change.backgroundColor=[UIColor clearColor];
    labelHead.alpha = 1.0f;
    bck_img1.alpha = 1.0f;
}

-(void)changecolor1
{
    DebugLog(@"CHANGE COLOR1");
    //    change.backgroundColor=[UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0];
    labelHead.alpha = 0.5f;
    bck_img1.alpha = 0.5f;
    
}


@end