//
//  ConHomeViewController.h
//  Budnav
//
//  Created by intel on 17/03/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "GAITrackedViewController.h"


@interface ConHomeViewController : GAITrackedViewController<CLLocationManagerDelegate,UIWebViewDelegate>
{
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    NSString *dialCode;
}
@end
