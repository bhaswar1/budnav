//
//  ConPersonalProfileViewController.h
//  Budnav
//
//  Created by intel on 14/05/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <CoreLocation/CoreLocation.h>
#import "GAITrackedViewController.h"

@interface ConPersonalProfileViewController : GAITrackedViewController<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate,UIGestureRecognizerDelegate,CLLocationManagerDelegate>
{
    int check;
    NSString *flagCode, *isdCode;
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
}

@property (strong, nonatomic) NSString *toUpdateInfo;

@end
