//
//  ConBusinessViewController.h
//  Budnav
//
//  Created by intel on 14/05/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <CoreLocation/CoreLocation.h>
#import "GAITrackedViewController.h"

@interface ConBusinessViewController : GAITrackedViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,CLLocationManagerDelegate,UIGestureRecognizerDelegate>
{
    int check;
    NSString *flagCode, *isdCode;
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
}

@end
