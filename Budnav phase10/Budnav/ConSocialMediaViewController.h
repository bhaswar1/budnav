//
//  ConSocialMediaViewController.h
//  Budnav
//
//  Created by intel on 15/05/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "GAITrackedViewController.h"

@interface ConSocialMediaViewController : GAITrackedViewController<UIAlertViewDelegate,UITextFieldDelegate,UIScrollViewDelegate>
{
    UIView *topbar;
    UIScrollView *scrollView;
    NSMutableArray *imagesiconarr,*toptextarr;
    UITextField *twittertext, *pinteresttext, *facebooktext, *googleplustext, *youtubetext, *skypetext, *instagramtext, *linkedintext, *vimeotext,  *soundcloudtext;
    UIAlertView *alert;
    NSDictionary *json1,*profdict;
    NSMutableDictionary *personal_dict, *business_dict, *social_dict;
    UIActivityIndicatorView *act;
}
@end
