//
//  ConGroupSettingsViewController.h
//  Contacter
//
//  Created by ios on 10/01/15.
//  Copyright (c) 2015 Esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConGroupChangeAdminViewController.h"
#import "ConGroupsViewController.h"
#import "ConGroupPicViewController.h"
#import "VPImageCropperViewController.h"
#import "ImageCropperViewController.h"
#import "GAITrackedViewController.h"

@interface ConGroupSettingsViewController : GAITrackedViewController<UITextFieldDelegate,UITextViewDelegate,UITableViewDataSource,UITableViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,VPImageCropperDelegate,UIActionSheetDelegate,NSURLConnectionDelegate,UIAlertViewDelegate,ImageCropperDelegate>
{
}
@property(nonatomic, readwrite)NSString *group_id,*adminname,*groupname,*membercount;
@end
