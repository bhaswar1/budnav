//
//  AddMemberViewController.h
//  Budnav
//
//  Created by ios on 21/09/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface AddMemberViewController : GAITrackedViewController

@property(nonatomic, readwrite)NSString *group_id;
@property(strong,nonatomic) NSDictionary *grpdict;

@end
