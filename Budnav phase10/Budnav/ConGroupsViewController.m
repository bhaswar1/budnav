//
//  ConGroupsViewController.m
//  Contacter
//
//  Created by Prosenjit_K on 28/10/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

#import "ConGroupsViewController.h"
#import "ConAccountSettingsViewController.h"
#import "ConInviteViewController.h"
#import "ConSyncLoaderViewController.h"
#import "ConProfileOwnViewController.h"
#import "ConQROptionsViewController.h"
#import "ConRequestViewController.h"
#import "AppDelegate.h"
#import "GroupDetailsViewController.h"
#import "ConNewProfileViewController.h"
#import "UIImageView+WebCache.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

@interface ConGroupsViewController (){
    
    UIView *mainview,*blackBack;
    int move,c;
    UIButton*addContact;
    UISearchBar *searchBar;
    NSArray *cpeopleIndexTitles;
    NSOperationQueue *groups;
    NSDictionary*groupsdict,*temp,*names_groups;
    NSDictionary*groupnames;
    UITableView*groupstable;
    NSMutableArray *groupsarray,*groupnames_array,*filtered_arr,*searchresults;
    NSString *isadmin,*base64String;
    BOOL filter;
    NSCharacterSet *whitespace;
    NSString *st,*userid;
    UIActivityIndicatorView *act;
    MDRadialProgressView *radialView3;
    NSTimer *timerrload, *timerresist,*timerr;
    NSData *decodedData;
    AppDelegate *del;
    UIImageView *create_group;
    UIRefreshControl *refreshcontrolnew;
    NSMutableAttributedString *refreshString;
    
}

@end

@implementation ConGroupsViewController
//@synthesize accept_check;

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"From ConGroupsViewController");
    
}

-(void)goPlus
{
    [searchBar resignFirstResponder];
    ConCreateGroupViewController *concreate=[[ConCreateGroupViewController alloc]init];
    if ([searchBar.text length]>0)
    {
        concreate.groupname=searchBar.text;
    }
    [searchBar resignFirstResponder];
    searchBar.text=@"";
    [act stopAnimating];
    
    [groupstable removeFromSuperview];
    del.tabBarController.tabBar.frame = CGRectMake(0, 0, 0, 0);
    
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:concreate animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    self.tabBarController.hidesBottomBarWhenPushed = NO;

    self.view.frame=CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
    del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    [del showTabValues:YES];

    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    self.navigationController.navigationBarHidden=YES;
    
    del.tabBarController.hidesBottomBarWhenPushed = YES;
    
    del.groupnameUpdate = @"";
    
    DebugLog(@"frame : %@",NSStringFromCGRect(del.tabBarController.tabBar.frame));
    DebugLog(@"here at groups ");
    filter=NO;
    
    //=====================Search bar black view with add groups button=======================//
    
    blackBack = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64)];
    //[blackBack setBackgroundColor:[UIColor blackColor]];
    [self.view addSubview:blackBack];
    
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
    
    // Add the gradient to the view
    [blackBack.layer insertSublayer:gradient atIndex:0];
    
    
    
    mainview = [[UIView alloc]initWithFrame:CGRectMake(0,blackBack.frame.origin.y+blackBack.frame.size.height, self.view.frame.size.width, self.view.frame.size.height-blackBack.frame.size.height)];
    mainview.backgroundColor=[UIColor whiteColor];
    //    mainview.layer.zPosition=3;
    [self.view addSubview:mainview];
    
    
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 15, [UIScreen mainScreen].bounds.size.width-41, 55)];
    searchBar.delegate=self;
    searchBar.tintColor=[UIColor whiteColor];
    searchBar.barTintColor= [UIColor clearColor];
    searchBar.backgroundImage=[UIImage new];
    searchBar.placeholder= @"Search";
    searchBar.keyboardAppearance=UIKeyboardAppearanceDark;
    
    [searchBar setImage:[UIImage imageNamed:@"searchIcon"]
       forSearchBarIcon:UISearchBarIconSearch
                  state:UIControlStateNormal];
    UITextField *searchField = [searchBar valueForKey:@"_searchField"];
    
    searchField.textColor = [UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:1.0f];
    searchField.backgroundColor = [UIColor colorWithRed:(20.0f/255.0f) green:(102.0f/255.0f) blue:(110.0f/255.0f) alpha:1.0f];
    NSAttributedString *str = [[NSAttributedString alloc] initWithString:@"Search" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:(153.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f] }];
    searchField.attributedPlaceholder = str;    searchBar.tag=1;
    
    [blackBack addSubview:searchBar];
    searchBar.hidden=NO;
    
    
    create_group = [[UIImageView alloc] initWithFrame:CGRectMake(searchBar.frame.origin.x+searchBar.frame.size.width-2, 32, 35, 20)];
    create_group.image = [UIImage imageNamed:@"Create Group"];
    [create_group setBackgroundColor:[UIColor clearColor]];
    
    [blackBack addSubview:create_group];
    
    
    addContact = [[UIButton alloc]init];
    [addContact setFrame:CGRectMake(searchBar.frame.origin.x+searchBar.frame.size.width, 30, 60, 30)];
    [addContact setBackgroundColor:[UIColor clearColor]];
    
    [addContact addTarget:self action:@selector(changealpha) forControlEvents:UIControlEventTouchDragEnter];
    [addContact addTarget:self action:@selector(changealpha) forControlEvents:UIControlEventTouchDragExit];
    [addContact addTarget:self action:@selector(changealpha1) forControlEvents:UIControlEventTouchDown];
    [addContact addTarget:self action:@selector(goPlus) forControlEvents:UIControlEventTouchUpInside];
    [blackBack addSubview:addContact];
    

    
    act = [[UIActivityIndicatorView alloc] init];
    act.center = self.view.center;
    [act startAnimating];
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [act setColor:[UIColor blackColor]];
    [self.view addSubview:act];
    act.layer.zPosition=2;
    
    groups = [NSOperationQueue new];
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(loaddata)
                                        object:nil];
    [groups addOperation:operation];
    
    
}

-(void)loaddata
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *groups_url =[NSString stringWithFormat:@"https://budnav.com/ext/?action=group-getgroups&thumb=true&access_token=%@&device_id=%@",[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
    DebugLog(@"groups url string ------ %@",groups_url);
    
    
    NSError *error1=nil;
    @try {
        NSData *data1=[NSData dataWithContentsOfURL:[NSURL URLWithString:groups_url]options:NSDataReadingUncached error:&error1];
        if (data1==nil)
        {
            DebugLog(@"no connnnn");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                                            message:nil
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            alert.tag=5;
            [alert show];
            
            
            
        }
        
        if (data1 != nil)
            groupsdict=[NSJSONSerialization JSONObjectWithData:data1 //1
                                                       options:kNilOptions
                                                         error:&error1];
        DebugLog(@"-----Groups info ----- %@",groupsdict);
    }
    @catch (NSException *exception) {
        DebugLog(@"exception %@",exception);
    }
    [self performSelectorOnMainThread:@selector(datadisplay) withObject:nil waitUntilDone:NO];
    
}


- (void)datadisplay
{
    groupsarray =[[NSMutableArray alloc]init];
    groupnames_array=[[NSMutableArray alloc]init];
    groupnames = [groupsdict objectForKey:@"details"];
    if (groupnames.count==0)
    {
        UILabel *nouserlb= [[UILabel alloc]initWithFrame:CGRectMake(10, 70, [UIScreen mainScreen].bounds.size.width-20, 60)];
        [mainview addSubview:nouserlb];
        nouserlb.numberOfLines=3;
        nouserlb.text=@"You are not in any group currently. Create a group to connect with multiple people at once!";
        nouserlb.backgroundColor=[UIColor clearColor];
        nouserlb.font=[UIFont fontWithName:@"ProximaNova-Bold" size:16];
        nouserlb.textAlignment=NSTextAlignmentCenter;
        nouserlb.textColor=[UIColor blackColor];
        [act removeFromSuperview];
    }
    else
    {
        if ([[groupsdict valueForKey:@"errorno"] isEqual:@"L2"])
        {
            [act stopAnimating];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please login again!"
                                                            message:nil
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
        }
        groupsarray=[groupnames mutableCopy];
        
        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)];
        groupsarray=[[groupsarray sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]] mutableCopy];
        
        DebugLog(@"groups array ---- %@",groupsarray);
    
        
        groupstable =[[UITableView alloc]initWithFrame:CGRectMake(0,0,mainview.frame.size.width,mainview.frame.size.height-44)];
        groupstable.delegate=self;
        groupstable.tag=1;
        groupstable.dataSource=self;
        [groupstable setBackgroundColor:[UIColor clearColor]];
        groupstable.separatorStyle = UITableViewCellSeparatorStyleNone;
        groupstable.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
        [mainview addSubview:groupstable];
        
        
        UITableViewController *tableViewController = [[UITableViewController alloc] init];
        tableViewController.tableView = groupstable;
        
        refreshcontrolnew = [[UIRefreshControl alloc] init];
        [refreshString addAttributes:@{NSForegroundColorAttributeName : [UIColor grayColor]} range:NSMakeRange(0, refreshString.length)];
        refreshcontrolnew.attributedTitle = refreshString;
        [refreshcontrolnew addTarget:self action:@selector(reloadAgain) forControlEvents:UIControlEventValueChanged];
        tableViewController.refreshControl = refreshcontrolnew;
        refreshcontrolnew.tintColor=[UIColor grayColor];
        
        
    }
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    
}
-(void)viewDidDisappear:(BOOL)animated
{
    addContact.alpha = 1.0f;
    //    [groups cancelAllOperations];
    [NSOperationQueue cancelPreviousPerformRequestsWithTarget:self];
    self.tabBarController.hidesBottomBarWhenPushed = YES;
    [self.view removeFromSuperview];
    [groupstable removeFromSuperview];
    [act removeFromSuperview];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!filter)
    {
        //DebugLog(@"count of groups ---- %lu",(unsigned long)groupsarray.count);
        return groupsarray.count;
    }
    if (filter)
    {
        //DebugLog(@"retrn: %ld",filtered_arr.count);
        return filtered_arr.count;
    }
    
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MyReuseIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    cell=nil;
    
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:MyIdentifier];
    }
    
    //cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (!filter) {
        
        
        cell.backgroundColor = [UIColor clearColor];
        
        
        // userid =[NSString stringWithFormat:@"%@",[[groupsarray objectAtIndex:indexPath.row]objectForKey:@"id"]];
        //  DebugLog(@"id id id ----- %@",userid);
        [act stopAnimating];
        namelbl = [[UILabel alloc]initWithFrame:CGRectMake(74.0f,5.0f, [UIScreen mainScreen].bounds.size.width-30, 30.0f)];
        [namelbl setText:[NSString stringWithFormat:@"%@",[[groupsarray objectAtIndex:indexPath.row]objectForKey:@"name"]]];
        [namelbl setTextAlignment:NSTextAlignmentLeft];
        [namelbl setTextColor:[UIColor blackColor]];
        [namelbl setFont:[UIFont fontWithName:@"ProximaNova-Bold" size:17]];
        [cell addSubview:namelbl];
        UILabel *separatorlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 59, [UIScreen mainScreen].bounds.size.width,0.5f)];
        separatorlabel.backgroundColor=[UIColor colorWithRed:(204.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f];
        [cell addSubview:separatorlabel];
        
        isadmin =[NSString stringWithFormat:@"%@",[[groupsarray objectAtIndex:indexPath.row]objectForKey:@"isAdmin"]];
        
        if([isadmin isEqualToString:@"1"])
        {
            //            UILabel *adminlbl = [[UILabel alloc]initWithFrame:CGRectMake(50.0f,25.0f,150.0f,30.0f)];
            //            [adminlbl setText:@"Members - Admin"];
            
            UILabel *adminlbl = [[UILabel alloc]initWithFrame:CGRectMake(74.0f,25.0f, 150.0f, 30.0f)];//(35.0f,25.0f,150.0f,30.0f)];
            if ([[[groupsarray objectAtIndex:indexPath.row]objectForKey:@"count"] intValue]>1)
            {
                [adminlbl setText:[NSString stringWithFormat:@"%@ Members",[[groupsarray objectAtIndex:indexPath.row]objectForKey:@"count"]]];
            }
            else
            {
            [adminlbl setText:[NSString stringWithFormat:@"%@ Member",[[groupsarray objectAtIndex:indexPath.row]objectForKey:@"count"]]];
            }
            [adminlbl setTextAlignment:NSTextAlignmentLeft];
            [adminlbl setTextColor:[UIColor lightGrayColor]];
            [adminlbl setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:13]];
            [cell addSubview:adminlbl];
            
            
            UILabel *adminnamelbl = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width-117.0f,17.0f, 100.0f, 30.0f)];//(35.0f,25.0f,150.0f,30.0f)];
            [adminnamelbl setText:[NSString stringWithFormat:@"ADMIN"]];
            [adminnamelbl setTextAlignment:NSTextAlignmentRight];
            [adminnamelbl setTextColor:[UIColor colorWithRed:(50.0f/255.0f) green:(170.0f/255.0f) blue:(184.0f/255.0f) alpha:1.0f]];
            [adminnamelbl setFont:[UIFont fontWithName:@"ProximaNova-Bold" size:13]];
            [cell addSubview:adminnamelbl];
            
            
          UIImageView  *img =[[UIImageView alloc] initWithFrame:CGRectMake(17.0f,9.0f,40.0f,40.0f)];
             img.image=[UIImage imageNamed:@"Backup profile pic"];
            base64String= [NSString stringWithFormat:@"%@",[[groupsarray objectAtIndex:indexPath.row]objectForKey:@"thumb"]];
            if ([base64String length] >6)
                
            {
                
                decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
            }
            
            img.layer.cornerRadius = img.frame.size.width/2;
            img.image = [UIImage imageWithData:[NSData dataWithData:decodedData]];
            img.clipsToBounds=YES;
            [cell addSubview:img];
            
        }
        else
            
        {
            
           // UILabel *adminlbl = [[UILabel alloc]initWithFrame:CGRectMake(15.0f,25.0f, 150.0f, 30.0f)];//(35.0f,25.0f, 150.0f, 30.0f)];
            UILabel *adminlbl = [[UILabel alloc]initWithFrame:CGRectMake(74.0f,25.0f, 150.0f, 30.0f)];//(35.0f,25.0f, 150.0f, 30.0f)];
            
            if ([[[groupsarray objectAtIndex:indexPath.row]objectForKey:@"count"] intValue]>1)
            {
                
            [adminlbl setText:[NSString stringWithFormat:@"%@ Members",[[groupsarray objectAtIndex:indexPath.row]objectForKey:@"count"]]];
                
            }
            else
            {
                [adminlbl setText:[NSString stringWithFormat:@"%@ Member",[[groupsarray objectAtIndex:indexPath.row]objectForKey:@"count"]]];
            }
            [adminlbl setTextAlignment:NSTextAlignmentLeft];
            [adminlbl setTextColor:[UIColor lightGrayColor]];
            [adminlbl setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:13]];
            [cell addSubview:adminlbl];
            
            
            UIImageView  *img =[[UIImageView alloc] initWithFrame:CGRectMake(17.0f,9.0f,40.0f,40.0f)];
            img.image=[UIImage imageNamed:@"Backup profile pic"];
            base64String= [NSString stringWithFormat:@"%@",[[groupsarray objectAtIndex:indexPath.row]objectForKey:@"thumb"]];
            if ([base64String length] >6)
                
            {
                
                decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
            }
            
            img.layer.cornerRadius = img.frame.size.width/2;
            img.image = [UIImage imageWithData:[NSData dataWithData:decodedData]];
            img.clipsToBounds=YES;
            [cell addSubview:img];
            
            
        }
        
        
    }
    else if(filter)
    {
        cell.backgroundColor = [UIColor clearColor];
        
        namelbl = [[UILabel alloc]initWithFrame:CGRectMake(74.0f,5.0f, [UIScreen mainScreen].bounds.size.width-30, 30.0f)];
        [namelbl setText:[NSString stringWithFormat:@"%@",[[filtered_arr valueForKey:@"name"]objectAtIndex:indexPath.row]]];
        [namelbl setTextAlignment:NSTextAlignmentLeft];
        [namelbl setTextColor:[UIColor blackColor]];
        [namelbl setFont:[UIFont fontWithName:@"ProximaNova-Bold" size:15]];
        [cell addSubview:namelbl];
        
        isadmin =[NSString stringWithFormat:@"%@",[[filtered_arr objectAtIndex:indexPath.row]objectForKey:@"isAdmin"]];
        
        UILabel *separatorlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 59, [UIScreen mainScreen].bounds.size.width,0.5f)];
        separatorlabel.backgroundColor=[UIColor colorWithRed:(204.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f];
        //[cell addSubview:separatorlabel];
        
        if([isadmin isEqualToString:@"1"])
        {
            //            UILabel *adminlbl = [[UILabel alloc]initWithFrame:CGRectMake(50.0f,25.0f,150.0f,30.0f)];
            //            [adminlbl setText:@"Members - Admin"];
            
            UILabel *adminlbl = [[UILabel alloc]initWithFrame:CGRectMake(74.0f,25.0f, 150.0f, 30.0f)];
            [adminlbl setText:[NSString stringWithFormat:@"%@ Members - Admin",[[filtered_arr objectAtIndex:indexPath.row]objectForKey:@"count"]]];
            [adminlbl setTextAlignment:NSTextAlignmentLeft];
            [adminlbl setTextColor:[UIColor lightGrayColor]];
            [adminlbl setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:13]];
            [cell addSubview:adminlbl];
            
            
            UILabel *adminnamelbl = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width-117.0f,17.0f, 100.0f, 30.0f)];//(35.0f,25.0f,150.0f,30.0f)];
            [adminnamelbl setText:[NSString stringWithFormat:@"ADMIN"]];
            [adminnamelbl setTextAlignment:NSTextAlignmentRight];
            [adminnamelbl setTextColor:[UIColor colorWithRed:(50.0f/255.0f) green:(170.0f/255.0f) blue:(184.0f/255.0f) alpha:1.0f]];
            [adminnamelbl setFont:[UIFont fontWithName:@"ProximaNova-Bold" size:13]];
            [cell addSubview:adminnamelbl];
            
        }
        else
            
        {
            
            UILabel *adminlbl = [[UILabel alloc]initWithFrame:CGRectMake(74.0f,25.0f, 150.0f, 30.0f)];
            
            if ([[[filtered_arr objectAtIndex:indexPath.row]objectForKey:@"count"] intValue]>1)
            {
                [adminlbl setText:[NSString stringWithFormat:@"%@ Members",[[filtered_arr objectAtIndex:indexPath.row]objectForKey:@"count"]]];
            }
            else
            {
            [adminlbl setText:[NSString stringWithFormat:@"%@ Member",[[filtered_arr objectAtIndex:indexPath.row]objectForKey:@"count"]]];
            }
            [adminlbl setTextAlignment:NSTextAlignmentLeft];
            [adminlbl setTextColor:[UIColor lightGrayColor]];
            [adminlbl setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:13]];
            [cell addSubview:adminlbl];
            
            
        }
        
        UIImageView  *img =[[UIImageView alloc] initWithFrame:CGRectMake(17.0f,9.0f,40.0f,40.0f)];
        img.image=[UIImage imageNamed:@"Backup profile pic"];
        base64String= [NSString stringWithFormat:@"%@",[[filtered_arr objectAtIndex:indexPath.row]objectForKey:@"thumb"]];
        if ([base64String length] >6)
            
        {
            
            decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
        }
        
        img.layer.cornerRadius = img.frame.size.width/2;
        img.image = [UIImage imageWithData:[NSData dataWithData:decodedData]];
        img.clipsToBounds=YES;
        [cell addSubview:img];
        
        
    }
    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    base64String= [NSString stringWithFormat:@"%@",[[groupsarray valueForKey:@"thumb"] objectAtIndex:indexPath.row]];
    [searchBar resignFirstResponder];
    if ([base64String length] >6)
    {
        decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
        
    }
    //  DebugLog(@"user id ------ %@",userid);
    
    GroupDetailsViewController *group=[[GroupDetailsViewController alloc]init];
    group.copydic=[[groupsarray objectAtIndex:indexPath.row ] mutableCopy];
    group.mydata=decodedData;
    
    if (filter)
    {
        group.member_count=[NSString stringWithFormat:@"%@",[[filtered_arr objectAtIndex:indexPath.row]objectForKey:@"count"]];
        userid =[NSString stringWithFormat:@"%@",[[filtered_arr objectAtIndex:indexPath.row]objectForKey:@"id"]];
        group.groupname=[NSString stringWithFormat:@"%@",[[filtered_arr objectAtIndex:indexPath.row]objectForKey:@"name"]];
        group.isadmin=[NSString stringWithFormat:@"%@",[[filtered_arr objectAtIndex:indexPath.row]objectForKey:@"isAdmin"]];
    }
    else if (!filter)
    {
        userid =[NSString stringWithFormat:@"%@",[[groupsarray objectAtIndex:indexPath.row]objectForKey:@"id"]];
        group.member_count=[NSString stringWithFormat:@"%@",[[groupsarray objectAtIndex:indexPath.row]objectForKey:@"count"]];
        group.groupname=[NSString stringWithFormat:@"%@",[[groupsarray objectAtIndex:indexPath.row]objectForKey:@"name"]];
        group.isadmin=[NSString stringWithFormat:@"%@",[[groupsarray objectAtIndex:indexPath.row]objectForKey:@"isAdmin"]];
    }
    group.groupid=[NSString stringWithFormat:@"%d",[userid intValue]];
    DebugLog(@"group id: %@",group.groupid);
    
    group.grouppic=[UIImage imageWithData:decodedData];
    
    del.tabBarController.tabBar.frame = CGRectMake(0, 0, 0, 0);
    [groupstable removeFromSuperview];
    
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:group animated:YES];
    
}


+(void)chngpostion
{
    DebugLog(@"Change pos requests page");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Data" object:Nil];
}

-(void)requestreceivedaction
{
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
    ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
    con.other=@"yes";
    con.uid=[NSString stringWithFormat:@"%@",del.pushtoprofileid];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}

-(void)acceptedrequestaction
{
    ConRequestViewController *con = [[ConRequestViewController alloc]init];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}
-(void)navtopage: (NSNotification *)notification
{
    DebugLog(@"navtopage");
    NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
    if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"4"])
    {
        ConAccountSettingsViewController *con = [[ConAccountSettingsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"6"])
    {
        ConInviteViewController *con = [[ConInviteViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"5"])
    {
        ConSyncLoaderViewController *con = [[ConSyncLoaderViewController alloc]init];
        //        [self.navigationController pushViewController:con animated:NO];
        
        [self presentViewController:con animated:NO completion:nil];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"2"])
    {
//        ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
        ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"3"])
    {
        ConQROptionsViewController *con = [[ConQROptionsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    
    
    //[mainview setFrame:CGRectMake(0, mainview.frame.origin.y, mainview.frame.size.width, mainview.frame.size.height)];
    //    [prefs setObject:@"111" forKey:@"whichpage"];
}

- (BOOL)ShouldBeginEditing:(UISearchBar *)searchBarlocal
{
    //searchBarlocal.showsCancelButton=YES;
    searchBar.frame = CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width, 51);
    [addContact setHidden:YES];
    
    return YES;
}

- (BOOL)searchBarDidBeginEditing:(UISearchBar *)searchBarlocal
{
    searchBarlocal.showsCancelButton=YES;
    [searchBarlocal becomeFirstResponder];
    
    return YES;
}


- (BOOL)searchBarDidEndEditing:(UISearchBar *)searchBar2
{
    searchBar2.showsCancelButton=YES;
    filter= NO;
    
    groupstable.userInteractionEnabled=YES;
    groupstable.scrollEnabled=YES;
    searchBar2.text=@"";
    [searchBar2 resignFirstResponder];
    return YES;
}

- (BOOL)searchBar:(UISearchBar *)searchBarlocal shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSCharacterSet *invalidCharSet = [[NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 ,-\n"] invertedSet];
    NSString *filtered = [[text componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
    return [text isEqualToString:filtered];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBarlocal
{
    searchBarlocal.showsCancelButton = NO;
    [searchBarlocal resignFirstResponder];
    groupstable.userInteractionEnabled=YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBarlocal
{
    [addContact setHidden:NO];
    searchBar.frame = CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width-55, 51);
    //txtSearchField.layer.borderColor = [[UIColor clearColor]CGColor];
    
    filter= NO;
    [groupstable reloadData];
    groupstable.hidden=NO;
    // nouserlb.hidden=YES;
    groupstable.userInteractionEnabled=YES;
    groupstable.scrollEnabled=YES;
    [searchBarlocal resignFirstResponder];
    searchBarlocal.text=@"";
    searchBarlocal.showsCancelButton=NO;
    c=0;
    
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar1
{
    //[searchBar1 setShowsCancelButton:YES animated:YES];
    filter = NO;
    //c=0;
    
}
- (BOOL)textFieldShouldClear:(UITextField *)textField {
    
    filter =NO;
    [self performSelector:@selector(searchBarCancelButtonClicked:) withObject:searchBar afterDelay: 0.05];
    return YES;
}
- (void)searchBar:(UISearchBar *)searchBarl textDidChange:(NSString *)searchText {
    
    //    searchview.hidden=NO;
    whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    st = [searchText stringByTrimmingCharactersInSet:whitespace];
    //Remove all objects first.
    //    [filtered_arr removeAllObjects];
    if ([st length] == 0)
    {
        [searchBarl performSelector:@selector(resignFirstResponder) withObject:nil afterDelay:0];
    }
    if([st length]>=1)
    {
        [filtered_arr removeAllObjects];
        filter = YES;
        c=1;
        NSPredicate *resultPredicate = [NSPredicate
                                        predicateWithFormat:@"name contains[c] %@",searchText];
        //      [NSPredicate predicateWithFormat:@"(first CONTAINS[c] %@) OR (last CONTAINS[c] %@)", keyword, keyword]
        searchresults = [NSMutableArray arrayWithArray:[groupsarray filteredArrayUsingPredicate:resultPredicate]];
        
        filtered_arr=[[NSMutableArray alloc] initWithArray:searchresults];
        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)];
        filtered_arr=[[filtered_arr sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]] mutableCopy];
        DebugLog(@"aaa %@",filtered_arr);
        //[groupstable reloadData];
        
        if ([filtered_arr count] ==0)
        {
            [filtered_arr removeAllObjects];
            groupstable.hidden=YES;
            //nouserlb.hidden=NO;
        }
        else
        {
            // nouserlb.hidden=YES;
            groupstable.hidden=NO;
            [groupstable reloadData];
        }
        
        
    }
    else {
        //        filter = NO;
        //nouserlb.hidden=YES;
        groupstable.hidden=NO;
        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
        filtered_arr=[[groupsarray sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]] mutableCopy];
        //        [filtered_arr removeAllObjects];
        [groupstable reloadData];
    }
    //    tab_contact.userInteractionEnabled=NO;
    //    tab_contact.scrollEnabled=NO;
    
}

-(void)changealpha
{
    addContact.alpha = 0.5f;
    create_group.alpha = 0.5f;
}

-(void)changealpha1
{
    addContact.alpha = 0.5f;
    create_group.alpha = 0.5f;
}


-(void)reloadAgain
{
    [groupstable removeFromSuperview];
    [self loaddata];
    [refreshcontrolnew endRefreshing];
   // [groupstable reloadData];
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
