//
//  ConSyncNewViewController.h
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 27/08/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MDRadialProgressView.h"
#import <AddressBook/AddressBook.h>
#import "GAITrackedViewController.h"

@interface ConSyncNewViewController : GAITrackedViewController
{
    MDRadialProgressView *radialView3;
    NSTimer *timerr;
}
@property(nonatomic, assign) float length;
@property(nonatomic, readwrite) NSMutableArray *cell_objects,*about_cell_objects;
@property(nonatomic, readwrite) UITableView *itemsTable,*about_Table;
@property(nonatomic, readwrite) UIScrollView *mainScroll;
@property(nonatomic, readwrite) int i;
@property(nonatomic, readwrite) UIView *table_view,*about_sep,*aboutview;

@property(nonatomic, readwrite) int move, moveparent;
@property(nonatomic, readwrite) UIView *mainview,*topbar;
@property(nonatomic, readwrite) UITextField *myTextField;
@property(nonatomic, readwrite) NSString *pass,*phnstr,*finalph;
@property(nonatomic, readwrite) NSMutableArray *con_array,*final_con_array,*filtered_arr, *con_array1;
@property(nonatomic, readwrite) NSMutableDictionary *add_contacts_dict;
@property(nonatomic, readwrite) NSMutableArray *app_contacts, *check_app_cont, *phonesarr, *check_app_cont1, *backuparr, *testarr,*addtophonebook, *existingphn;
@property(nonatomic, readwrite) NSMutableArray *uniquearray, *part_array;
@property(nonatomic, readwrite) NSMutableData *responseData;
@property(nonatomic, readwrite) UIImageView *logoimg;
@property(nonatomic, readwrite) NSDictionary *getbackupdict;
@property(nonatomic, readwrite) NSArray *backarrnumbers;
@property(nonatomic, readwrite) NSMutableString *conhome,*conwork, *conmobile;

typedef void(^myCompletion)(BOOL);

@end

