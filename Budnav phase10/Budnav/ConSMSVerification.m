//
//  ConSMSVerification.m
//  Budnav
//
//  Created by Sohini's on 23/03/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "ConSMSVerification.h"
#import "ConAddressSignupViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

@interface ConSMSVerification ()
{
    UIActivityIndicatorView *loader;
    NSCharacterSet *whitespace;
}

@end

@implementation ConSMSVerification

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    self.screenName = @"Sign up verify";
    
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createEventWithCategory:@"Home"
                                             action:@"Sign up verify"
                                              label:nil
                                              value:nil] build]];

    
    coverview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 64)];
    //coverview.backgroundColor = [UIColor blackColor];
    [self.view addSubview:coverview];
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
    
    // Add the gradient to the view
    [coverview.layer insertSublayer:gradient atIndex:0];
    
    
    
    back = [[UIImageView alloc] initWithFrame:CGRectMake(15, 32, 12, 20)];
    back.image = [UIImage imageNamed:@"back3"];
    [coverview addSubview:back];
    
    bck = [[UILabel alloc] initWithFrame:CGRectMake(back.frame.size.width+back.frame.origin.x+5, 31, 40, 20)];
    bck.text=@"Back";
    bck.textColor = [UIColor whiteColor];
    //        backlbl.font=[UIFont fontWithName:@"ProximaNova-Regular" size:20];
    bck.backgroundColor=[UIColor clearColor];
    [coverview addSubview:bck];
    bck.userInteractionEnabled = YES;
    
    backbutton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 150, 64)];
    [backbutton setTitle:nil forState:UIControlStateNormal];
    backbutton.backgroundColor = [UIColor clearColor];
    [coverview addSubview:backbutton];
    
    [backbutton addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDown];
    [backbutton addTarget:self action:@selector(changecoloragain) forControlEvents:UIControlEventTouchDragExit];
    [backbutton addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDragEnter];
    [backbutton addTarget:self action:@selector(backmethod) forControlEvents:UIControlEventTouchUpInside];


    
    next = [[UIImageView alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-27, 32, 12, 20)];
    next.image= [UIImage imageWithCGImage:back.image.CGImage
                                    scale:back.image.scale
                              orientation:UIImageOrientationUpMirrored];
    [coverview addSubview:next];
    
    nxt = [[UILabel alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-68, 31, 36, 20)];
    nxt.text = @"Next";
    nxt.textColor = [UIColor whiteColor];
    nxt.backgroundColor = [UIColor clearColor];
    [coverview addSubview:nxt];
    nxt.userInteractionEnabled = YES;
    
    nextbutton = [[UIButton alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-65, 27, 65, 30)];
    [nextbutton setTitle:nil forState:UIControlStateNormal];
    nextbutton.backgroundColor = [UIColor clearColor];
    
    [nextbutton addTarget:self action:@selector(changecolor1) forControlEvents:UIControlEventTouchDown];
    [nextbutton addTarget:self action:@selector(changecoloragain1) forControlEvents:UIControlEventTouchDragExit];
    [nextbutton addTarget:self action:@selector(changecolor1) forControlEvents:UIControlEventTouchDragEnter];
    [nextbutton addTarget:self action:@selector(nextmethod) forControlEvents:UIControlEventTouchUpInside];
    [coverview addSubview:nextbutton];
    
    mainview = [[UIView alloc] initWithFrame:CGRectMake(0, coverview.frame.origin.y+coverview.frame.size.height, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height-coverview.frame.size.height)];
    mainview.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:mainview];
    
    label1 = [[UILabel alloc] initWithFrame:CGRectMake(50, 40, [UIScreen mainScreen].bounds.size.width-100, 30)];
    label1.text=@"We have sent an sms with a code to the number:";
    label1.numberOfLines=0;
    label1.backgroundColor = [UIColor clearColor];
    label1.contentMode=UIViewContentModeCenter;
    label1.textAlignment= NSTextAlignmentCenter;
    label1.lineBreakMode=NSLineBreakByWordWrapping;
    [label1 sizeToFit];
    label1.textColor=[UIColor blackColor];//colorWithRed:101.0/255.0 green:101.0/255.0 blue:101.0/255.0 alpha:1.0];
    label1.font=[UIFont fontWithName:@"ProximaNova-Regular" size:18];
    
//    NSMutableAttributedString* attrString = [[NSMutableAttributedString alloc] initWithString:@"We have sent an sms with \n    a code to the number:"];
//    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
//    [style setLineSpacing:3];
//    [attrString addAttribute:NSParagraphStyleAttributeName
//                       value:style
//                       range:NSMakeRange(0, [label1.text length])];
//    label1.attributedText = attrString;
    
    [mainview addSubview:label1];
    
    number = [[UILabel alloc]initWithFrame:CGRectMake(0, 100, [[UIScreen mainScreen] bounds].size.width, 30)];
    number.textAlignment=NSTextAlignmentCenter;
    number.text=[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"countrycode"],[[NSUserDefaults standardUserDefaults] objectForKey:@"phone"]];
    number.textColor=[UIColor blackColor];//colorWithRed:101.0/255.0 green:101.0/255.0 blue:101.0/255.0 alpha:1.0];
    number.font=[UIFont fontWithName:@"ProximaNova-Regular" size:20];
    [mainview addSubview:number];
    
    divider1 = [[UIView alloc] initWithFrame:CGRectMake(0, 150, [[UIScreen mainScreen] bounds].size.width, 0.5)];
    divider1.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
    [mainview addSubview:divider1];
    
    //    UIView *additionalview=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 130, 50)];
    
    code = [[UITextField alloc] initWithFrame:CGRectMake(0, divider1.frame.origin.y+divider1.frame.size.height, self.view.frame.size.width, 50)];
    code.backgroundColor=[UIColor clearColor];
    UIColor *color = [UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0];
    code.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"code" attributes:@{NSForegroundColorAttributeName: color}];
    code.delegate=self;
    code.textColor = [UIColor blackColor];
    code.userInteractionEnabled=YES;
    code.autocorrectionType=UITextAutocorrectionTypeNo;
    code.keyboardAppearance=UIKeyboardAppearanceDark;
    //    code.leftView=additionalview;
    //    code.leftViewMode=UITextFieldViewModeAlways;
    code.font=[UIFont fontWithName:@"ProximaNova-Regular" size:18];
    code.tintColor=[UIColor grayColor];
    code.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    //    [code setClearButtonMode:UITextFieldViewModeNever];
    //    [code setReturnKeyType:UIReturnKeyDone];
    code.textAlignment=NSTextAlignmentCenter;
    [mainview addSubview:code];
    
    if ([_defaultCode length]>0) {
        code.text = _defaultCode;
    }
    
    divider2 = [[UIView alloc] initWithFrame:CGRectMake(0, code.frame.origin.y+code.frame.size.height, [[UIScreen mainScreen] bounds].size.width, 0.5)];
    divider2.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
    [mainview addSubview:divider2];
    
}

-(void)backmethod
{
    DebugLog(@"back");
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"back"];
    
//    CATransition* transition = [CATransition animation];
//    transition.duration = 0.4;
   // transition.type = kCATransitionPush;
//    transition.type = kCATransitionFade;
   // transition.subtype = kCATransitionFromLeft;
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)nextmethod
{
    
    
    
    whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    
    nextbutton.userInteractionEnabled = NO;
    
    
    
    //////////////////////////////////////// SMS CODE /////////////////////////////////////////////
    
    //    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
    
    //
    
    //        nextbutton.userInteractionEnabled = NO;
    
    //
    
    //        loader = [[UIActivityIndicatorView alloc]init];
    
    //        loader.center=self.view.center;
    
    //        [loader startAnimating];
    
    //        loader.hidesWhenStopped=YES;
    
    //        loader.hidden=NO;
    
    //        loader.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    
    //        [loader setColor:[UIColor blackColor]];
    
    //        [self.view addSubview:loader];
    
    //
    
    //
    
    //    }];
    
    //
    
    //    [[NSOperationQueue new] addOperationWithBlock:^{
    
    //
    
    //
    
    //    NSString *sms_code = code.text;
    
    //  //  NSUserDefaults *prefsa = [NSUserDefaults standardUserDefaults];
    
    //
    
    //    NSString *urlString =[NSString stringWithFormat:@"https://budnav.com/ext/?action=register&smscode=%@",sms_code];
    
    //            DebugLog(@"SMS VERIFICATION URL:%@",urlString);
    
    //
    
    //            NSString *newString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    //
    
    //            NSData *signeddataURL =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString]];
    
    //            if (signeddataURL == nil)
    
    //            {
    
    //                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
    
    //
    
    //                    [loader removeFromSuperview];
    
    //                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please Check Internet Connection!"
    
    //                                                                    message:Nil
    
    //                                                                   delegate:self
    
    //                                                          cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
    
    //                    [alert show];
    
    //
    
    //                    nxt.alpha = 1.0f;
    
    //                    next.alpha = 1.0f;
    
    //                     nextbutton.userInteractionEnabled = YES;
    
    //
    
    //                }];
    
    //
    
    //
    
    //            }
    
    //            else
    
    //            {
    
    //                 [[NSOperationQueue mainQueue] addOperationWithBlock:^{
    
    //
    
    //                NSError *error;
    
    //                NSDictionary *json;
    
    //
    
    //                json = [NSJSONSerialization JSONObjectWithData:signeddataURL
    
    //                                                         options:kNilOptions
    
    //                                                           error:&error];
    
    //                DebugLog(@"SMS JSON:%@",json);
    
    //                if ([[json objectForKey:@"success"] intValue] == 1)
    
    //                {
    
    //                   ConAddressSignupViewController *add = [[ConAddressSignupViewController alloc] init];
    
    //                    add.Code = code.text;
    
    //                    CATransition* transition = [CATransition animation];
    
    //
    
    //                    transition.duration = 0.4;
    
    //                    transition.type = kCATransitionPush;
    
    //                    transition.subtype = kCATransitionFade;
    
    //                    transition.subtype = kCATransitionFromRight;
    
    //
    
    //                    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    //
    
    //                    [self.navigationController pushViewController:add animated:NO];
    
    //                }
    
    //                else
    
    //                {
    
    //                    [loader removeFromSuperview];
    
    //
    
    //                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please Check The Code!"
    
    //                                                       message:Nil
    
    //                                                      delegate:self
    
    //                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
    
    //                    [alert show];
    
    //
    
    //                    nxt.alpha = 1.0f;
    
    //                    next.alpha = 1.0f;
    
    //                    nextbutton.userInteractionEnabled = YES;
    
    //                }
    
    //
    
    //                 }];
    
    //            }
    
    //
    
    //}];
    
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    DebugLog(@"next");
    
    
    
    if (![code.text isKindOfClass:[NSNull class]] && ![code.text isEqualToString:@""] && [[code.text stringByTrimmingCharactersInSet:whitespace] length]>0)
        
    {
        
        nextbutton.userInteractionEnabled = YES;
        
        ConAddressSignupViewController *add = [[ConAddressSignupViewController alloc] init];
        
        add.Code = code.text;
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        
//        transition.type = kCATransitionPush;
//        
//        transition.subtype = kCATransitionFade;
//        
//        transition.subtype = kCATransitionFromRight;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:add animated:NO];
        
    }
    
    else
        
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please enter your SMS verification code!" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
        
        nxt.alpha = 1.0f;
        
        next.alpha = 1.0f;
        
        nextbutton.userInteractionEnabled = YES;
        
    }
    
}

#pragma mark- textfield delegates

//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
//{
//    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
//    textField.placeholder=@"";
//    return YES;
//}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //DebugLog(@"Dismiss keyboard");
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)changecoloragain
{
    DebugLog(@"CHANGE COLOR");
    bck.alpha = 1.0f;
    back.alpha = 1.0f;
}

-(void)changecolor
{
    DebugLog(@"CHANGE COLOR1");
    bck.alpha = 0.5f;
    back.alpha = 0.5f;
    
}

-(void)changecoloragain1

{
    
    DebugLog(@"CHANGE COLOR");
    
    //    change.backgroundColor=[UIColor clearColor];
    
    nxt.alpha = 1.0f;
    
    next.alpha = 1.0f;
    
}



-(void)changecolor1

{
    
    DebugLog(@"CHANGE COLOR1");
    
    //    change.backgroundColor=[UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0];
    
    nxt.alpha = 0.5f;
    
    next.alpha = 0.5f;
    
    
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
