//  ConSettingsView.m
//  Contacter
//  Created by Bhaswar's MacBook Air on 08/05/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
#import "ConSettingsView.h"
#import "AppDelegate.h"
#import "ConLoginViewController.h"
#import "ConStartViewController.h"
#import "ConAccountSettingsViewController.h"
#import "ConAddBackUpViewController.h"
#import <AddressBook/AddressBook.h>
#import "SVProgressHUD.h"
#import "MDRadialProgressView.h"
#import "MDRadialProgressTheme.h"
#import "MDRadialProgressLabel.h"
#import "ConNewContactsViewController.h"
#import "ConHomeViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

@implementation ConSettingsView
@synthesize length,cell_objects,about_cell_objects,itemsTable,about_Table,mainScroll,i,table_view,about_sep,aboutview,move, moveparent,mainview,topbar,myTextField,pass,con_array,final_con_array,filtered_arr, con_array1,add_contacts_dict,app_contacts, check_app_cont, phonesarr, check_app_cont1, backuparr, testarr,addtophonebook, existingphn,uniquearray, part_array,responseData,logoimg;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        i=0;
        cell_objects = [[NSMutableArray alloc] initWithObjects:@"Help",@"About",@"Profile",@"Scan",@"Account Settings",@"Synchronise Contacts",@"Invite Friends",@"Rate in App Store",@"Sign Out", nil]; //,@"Business Premium"//===
        about_cell_objects = [[NSMutableArray alloc] init];
        //UIView *background=[[UIView alloc]initWithFrame:CGRectMake(0, 0, .280, 568)];
        UIView *background=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-40, [UIScreen mainScreen].bounds.size.height)];
        background.backgroundColor=[UIColor blackColor];
        [self addSubview:background];
        
        //        UILabel *heading = [[UILabel alloc]initWithFrame:CGRectMake(0, 23, 280, 37)];
        UILabel *heading = [[UILabel alloc]initWithFrame:CGRectMake(0, 23, [UIScreen mainScreen].bounds.size.width-40, 37)];
        [background addSubview:heading];
        heading.backgroundColor=[UIColor clearColor];
        heading.text=@"SETTINGS";
        heading.font=[UIFont fontWithName:@"ProximaNova-Regular" size:15.5f];
        heading.textColor=[UIColor whiteColor];
        heading.textAlignment=NSTextAlignmentCenter;
        
        //        itemsTable = [[UITableView alloc] initWithFrame:CGRectMake(0.0f, 65.0f, 280,503)];
        itemsTable = [[UITableView alloc] initWithFrame:CGRectMake(0.0f, 65.0f, [UIScreen mainScreen].bounds.size.width-40,[UIScreen mainScreen].bounds.size.height-65)];
        [itemsTable setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bgsettings.png"]]];
        [itemsTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        itemsTable.delegate= self;
        [itemsTable setDataSource:self];
        itemsTable.userInteractionEnabled=YES;
        itemsTable.scrollEnabled=NO;
        [background addSubview:itemsTable];
        
        mainScroll.contentSize = CGSizeMake(0,table_view.frame.size.height+aboutview.frame.size.height+about_sep.frame.size.height);
    }
    return self;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat row_height;
    if (indexPath.row < 4)
        row_height =0;
    else
        row_height=55;
    return row_height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 9;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell==nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    [cell setBackgroundColor:[UIColor clearColor]]; //colorWithPatternImage:[UIImage imageNamed:@"menu-single-bga.png"]]];
    
    UIImage *image;
    switch (indexPath.row) {
        case 0:
            image=[UIImage imageNamed:@"profile_white.png"];
            break;
        case 1:
            image=[UIImage imageNamed:@"scan_white.png"];
            break;
        case 2:
            image = [UIImage imageNamed:@"profile_white.png"];
            break;
        case 3:
            image = [UIImage imageNamed:@"scan_white.png"];
            break;
        case 4:
            image = [UIImage imageNamed:@"account-settings.png"];
            break;
        case 5:
            image = [UIImage imageNamed:@"sync.png"];
            break;
        case 6:
            image = [UIImage imageNamed:@"inviting-friend.png"];
            break;
        case 7:
            image = [UIImage imageNamed:@"rate-in-app-store.png"];
            break;
        case 8:
            image = [UIImage imageNamed:@"sign-out.png"];
            break;
    }
    
    cell.imageView.image=image;
    
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17.0f];
    cell.textLabel.textColor = [UIColor whiteColor];
    //    cell.textLabel.textAlignment=NSTextAlignmentCenter;
    
    NSString *title_text=[cell_objects objectAtIndex:indexPath.row];
    // UIImage *div;
    cell.textLabel.text=title_text;
    cell.textLabel.textColor=[UIColor whiteColor];
    
    UIView *sep = [[UIView alloc] initWithFrame:CGRectMake(0, 54, 280, 1)];
    sep.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"divider.png"]];
    [cell addSubview:sep];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DebugLog(@"indexpath.row = %ld",(long)indexPath.row);
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row] forKey:@"whichpage"];
    if(indexPath.row==0)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"pagenav" object:Nil];
    }
    
    if(indexPath.row==1)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"pagenav" object:Nil];
    }
    
    if(indexPath.row==2)
    {
        //        ConAccountSettingsViewController *login = [[ConAccountSettingsViewController alloc] init];
        //        AppDelegate *mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        ////         [mainDelegate.navigationController pushViewController:login animated:YES];
        //        mainDelegate.navigationController = [[UINavigationController alloc] initWithRootViewController:login];
        //        mainDelegate.navigationController.navigationBar.hidden= YES;
        //        [mainDelegate showTabStatus:YES];
        //        mainDelegate.window.rootViewController = mainDelegate.navigationController;
        //        [mainDelegate.window makeKeyAndVisible];
        ////        [[[[self window] rootViewController] navigationController] pushViewController:login animated:YES];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"pagenav" object:Nil];
    }
    
    if(indexPath.row==3)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"pagenav" object:Nil];
    }
    
    if(indexPath.row==4)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"pagenav" object:Nil];
    }
    
    if(indexPath.row==5)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"pagenav" object:Nil];
    }
    if(indexPath.row==6)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"pagenav" object:Nil];
    }
    if(indexPath.row==7)
    {
        
    }
    
    if(indexPath.row==8)
    {
        
        actincir = [[UIActivityIndicatorView alloc] init];
        
        actincir.center = self.center;
        
        [actincir startAnimating];
        
        actincir.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        
        [actincir setColor:[UIColor whiteColor]];
        
        [self addSubview:actincir];
        
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSString *urlString_qr =[NSString stringWithFormat:@"https://budnav.com/ext/?action=logout&id=%@&access_token=%@&device_id=%@",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
            
            NSString *newString_qr = [urlString_qr stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            DebugLog(@"STRING=> %@",newString_qr);
            
            //***********
            
            NSError *localErr;
            
            
            
            NSData *dataURL =  [NSData dataWithContentsOfURL:[NSURL URLWithString:newString_qr] options:NSDataReadingUncached error:&localErr];
            
            if (dataURL != nil)
            {
                if(localErr!=nil)
                {
                    DebugLog(@" nil");
                }
                
                jsonparsed = [NSJSONSerialization JSONObjectWithData:dataURL //1
                              
                                                             options:kNilOptions
                              
                                                               error:&localErr];
                
                DebugLog(@"JSON PARSED%@",jsonparsed);
                
                NSString *errornumber= [NSString stringWithFormat:@"%@",[jsonparsed objectForKey:@"errorno"]];
                
                DebugLog(@"error number  %@",errornumber);
                
                if (![errornumber isEqualToString:@"0"])
                {
                    [self performSelectorOnMainThread:@selector(failure) withObject:nil waitUntilDone:YES]    ;
                }
                else
                {
                    [self performSelectorOnMainThread:@selector(yesSuccess)
                     
                                           withObject:nil
                     
                                        waitUntilDone:YES];
                }
            }
            else
            {
                DebugLog(@"server not responding");
                
                [SVProgressHUD showErrorWithStatus:@"Error in Server Connection"];
                
                [actincir stopAnimating];
            }
            
        });
        
        
        
        //*************************************
        
        
        NSDictionary * dict = [prefs dictionaryRepresentation];
        for (id key in dict) {
            [prefs removeObjectForKey:key];
        }
        [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"isloggedin"];
        [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"autologin"];
        [prefs synchronize];
        //        ConLoginViewController *login = [[ConLoginViewController alloc] init];
        ConHomeViewController *sign_out = [[ConHomeViewController alloc] init];
        AppDelegate *mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        //        [mainDelegate.navigationController pushViewController:login animated:NO];
        
        mainDelegate.navigationController = [[UINavigationController alloc] initWithRootViewController:sign_out];
        mainDelegate.navigationController.navigationBar.hidden= YES;
        [mainDelegate showTabStatus:NO];
        mainDelegate.window.rootViewController = mainDelegate.navigationController;
        [mainDelegate.window makeKeyAndVisible];
    }
}


-(void)yesSuccess{
    
    NSString *dict_details = [jsonparsed objectForKey:@"success"];
    
    DebugLog(@" dict detaisls %@",dict_details);
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    if([dict_details intValue]== 1)
    {
        NSDictionary * dict = [prefs dictionaryRepresentation];
        for (id key in dict) {
            
            [prefs removeObjectForKey:key];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"isloggedin"];
        [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"autologin"];
        [prefs synchronize];
        
    }
    
    [actincir stopAnimating];
    [actincir removeFromSuperview];
    //[SVProgressHUD showSuccessWithStatus:@"Successfully Logged Out!"];
    //    ConLoginViewController *login = [[ConLoginViewController alloc] init];
    //    ConHomeViewController *sign_out = [[ConHomeViewController alloc] init];
    //    AppDelegate *mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //
    //    [mainDelegate.navigationController pushViewController:sign_out animated:NO];
    //
    //    //    mainDelegate.navigationController = [[UINavigationController alloc] initWithRootViewController:sign_out];
    //    //    mainDelegate.navigationController.navigationBar.hidden= YES;
    //    //    [mainDelegate showTabStatus:NO];
    //    //    mainDelegate.window.rootViewController = mainDelegate.navigationController;
    //    //    [mainDelegate.window makeKeyAndVisible];
}


-(void)failure{
    
    //NSString *err_str = [jsonparsed objectForKey:@"error"];
    //    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!"
    //
    //                                                    message:err_str
    //                                                   delegate:self
    //                                          cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
    //
    //    [alert show];
    
    //[SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",err_str]];
    
    // ConLoginViewController *login = [[ConLoginViewController alloc] init];
    //    ConHomeViewController *sign_out = [[ConHomeViewController alloc] init];
    //    AppDelegate *mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //
    //    [mainDelegate.navigationController pushViewController:sign_out animated:NO];
    //
    //    mainDelegate.navigationController = [[UINavigationController alloc] initWithRootViewController:sign_out];
    //    mainDelegate.navigationController.navigationBar.hidden= YES;
    //    [mainDelegate showTabStatus:NO];
    //    mainDelegate.window.rootViewController = mainDelegate.navigationController;
    //    [mainDelegate.window makeKeyAndVisible];
    
    [actincir stopAnimating];
    [actincir removeFromSuperview];
}

- (void) runSpinAnimationOnView:(UIImageView *)view duration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat;
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ * rotations * duration ];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = repeat;
    
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data
{
    [responseData appendData:data];
    float progress =  (float)[responseData length]/(float)self.length;
    DebugLog(@"progress: %ld",(unsigned long)[responseData length]);
    DebugLog(@" percentage: %f",progress);
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    DebugLog(@"connection failed");
    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"backupstatus"];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    DebugLog(@"connection finished loading");
    DebugLog(@"response data - %@", [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
    
    NSError *err;
    
    //    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString1]];
    
    if (responseData != nil)
    {
        NSDictionary  *json1aa=[NSJSONSerialization JSONObjectWithData:responseData options:0 error:&err];
        
        DebugLog(@"this now has json string url: %@",json1aa);
        
        if ([[json1aa objectForKey:@"success"] intValue] ==1)
        {
            [logoimg removeFromSuperview];
            //             [SVProgressHUD showSuccessWithStatus:@"Success!"];
            [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"backupstatus"];
        }
        else
        {
            [logoimg removeFromSuperview];
            NSString *err_str = [json1aa objectForKey:@"error"];
            DebugLog(@"error backing up: %@",err_str);
            //           UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!"
            //                                               message:err_str
            //                                              delegate:self
            //                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            //            [alert show];
            //            [SVProgressHUD showErrorWithStatus:err_str];
        }
        //            [SVProgressHUD showSuccessWithStatus:@"Success!"];
    }
    else
    {
        [logoimg removeFromSuperview];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                                        message:nil
                                                       delegate:self
                                              cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        alert.tag=7;
        //        [alert show];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    responseData = [NSMutableData data];
    self.length = [response expectedContentLength];
    DebugLog(@"expectd length= %f",self.length);
}

-(NSString*) encodeToPercentEscapeString:(NSString *)string {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                              (CFStringRef) string,
                                                              NULL,
                                                              (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
}

// Decode a percent escape encoded string.
-(NSString*) decodeFromPercentEscapeString:(NSString *)string {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL,
                                                                              (CFStringRef) string,
                                                                              CFSTR(""),
                                                                              kCFStringEncodingUTF8));
}


-(void) myMethod:(myCompletion) compblock{
    //do stuff
    DebugLog(@"do the stuff in this block");
    @autoreleasepool {
        
        //    UIBackgroundTaskIdentifier BGIdentifier = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{}];
        testarr = [existingphn mutableCopy];
        addtophonebook = [[NSMutableArray alloc]init];
        int dev, apps;
        NSString *conhome,*conwork, *conmobile;
        //            NSMutableDictionary *testdict;
        int testback;
        
        for (apps=0; apps< [backuparr count]; apps++)
        {
            testback = 0;
            NSDictionary *getbackupdict = [backuparr objectAtIndex:apps];
            NSString *phnstr = [NSString stringWithFormat:@"%@",[[backuparr objectAtIndex:apps]objectForKey:@"mobile_num"]];
            NSArray *backarrnumbers = [phnstr componentsSeparatedByString:@","];
            //                    DebugLog(@"backarrnumbers: %@",backarrnumbers);
            int ph_int;
            ph_int = 0;
            //                    testdict = [testarr objectAtIndex:dev];
            
            for (ph_int =0; ph_int< [backarrnumbers count]; ph_int++)
            {
                NSString *finalph = [backarrnumbers objectAtIndex:ph_int];
                finalph = [[finalph componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
                if ([finalph length] > 6)
                    finalph = [finalph substringFromIndex:[finalph length] - 6];
                
                //                        DebugLog(@"finalphone: %@ , == %ld",finalph,(long)[finalph intValue]);
                
                for (dev=0; dev< [testarr count]; dev++)
                {
                    conhome = [NSString stringWithFormat:@"%@",[[testarr objectAtIndex:dev]objectForKey:@"home"]];
                    conhome = [[conhome componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
                    if ([conhome length] > 6)
                        conhome = [conhome substringFromIndex:[conhome length] - 6];
                    conmobile = [NSString stringWithFormat:@"%@",[[testarr objectAtIndex:dev]objectForKey:@"mobile"]];
                    conmobile = [[conmobile componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
                    
                    if ([conmobile length] > 6)
                        conmobile = [conmobile substringFromIndex:[conmobile length] - 6];
                    
                    conwork = [NSString stringWithFormat:@"%@",[[testarr objectAtIndex:dev]objectForKey:@"work"]];
                    conwork = [[conwork componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
                    if ([conwork length] > 6)
                        conwork = [conwork substringFromIndex:[conwork length] - 6];
                    
                    //                            DebugLog(@"conhome, conmobile, conwork restore: %ld, %ld, %ld",(long)[conhome intValue],(long)[conmobile intValue],(long)[conwork intValue]);
                    //                    if ([conhome rangeOfString:finalph].location == NSNotFound)
                    if (([conhome intValue] != 0 && [conhome intValue] != 2147483647 && [finalph intValue] != 0 && [finalph intValue] != 2147483647 && [conhome rangeOfString:finalph].location != NSNotFound) || ([conmobile intValue] != 0 && [conmobile intValue] != 2147483647 && [finalph intValue] != 0 && [finalph intValue] != 2147483647 && [conmobile rangeOfString:finalph].location != NSNotFound) ||  ([conwork intValue] != 0 && [conwork intValue] != 2147483647 && [finalph intValue] != 0 && [finalph intValue] != 2147483647 && [conwork rangeOfString:finalph].location != NSNotFound))
                    {
                        testback=0;
                        break;
                    }
                    else
                    {
                        testback=1;
                        continue;
                    }
                    if (testback == 0)
                        break;
                }
                if (testback == 0)
                    break;
            }
            
            if (testback ==1)
            {
                [addtophonebook addObject:getbackupdict];
                //                 break;
            }
        }
        
        if ([testarr count] == 0)
            addtophonebook = [backuparr mutableCopy];
        
        //        [[UIApplication sharedApplication] endBackgroundTask:BGIdentifier];
    }
    compblock(YES);
}
@end