//
//  WildcardGestureRecognizer.h
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 15/07/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

//#import <UIKit/UIKit.h>
//
//@interface WildcardGestureRecognizer : UIGestureRecognizer
//
//@end


//
//  WildcardGestureRecognizer.h
//  Copyright 2010 Floatopian LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^TouchesEventBlock)(NSSet * touches, UIEvent * event);

@interface WildcardGestureRecognizer : UIGestureRecognizer {
    TouchesEventBlock touchesBeganCallback;
}
@property(copy) TouchesEventBlock touchesBeganCallback;


@end