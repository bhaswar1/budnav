//
//  ConNewDetailsViewController.m
//  Contacter
//
//  Created by ios on 10/01/15.
//  Copyright (c) 2015 Esolz. All rights reserved.
//

#import "ConNewDetailsViewController.h"
#import "AppDelegate.h"
#import "SocialWebViewController.h"
#import "UIImageView+WebCache.h"
#import "ConAccountSettingsViewController.h"
#import "ConInviteViewController.h"
#import "ConAddFriendViewController.h"
#import "iToast.h"
#import <GoogleMaps/GoogleMaps.h>
#import "ConNavigateViewController.h"
#import "ConAddBackUpViewController.h"
#import "SVProgressHUD.h"
#import <AddressBook/AddressBook.h>
#import "DBManager.h"
#import "ConEditProfileViewController.h"
#import "ConPictureProfViewController.h"
#import "ConSyncLoaderViewController.h"
#import "ConRequestViewController.h"
#import "ConNewRequestsViewController.h"
#import "UIImage+animatedGIF.h"
#import "ConQROptionsViewController.h"
#import "QuartzCore/QuartzCore.h"
#import "ConFullSettingsViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"


@interface ConNewDetailsViewController ()  <UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate>
{
    NSDictionary *json1;
    UIAlertView *alert;
    UIButton *removeconbt, *togglebt;
    int switchprof;
    UILabel *personaltext, *businesstext, *website_lb, *companylb, *desiglb, *backlbl, *mobile_num, *emailId, *land_num;
    UITextView *addressText;
    UIButton *mobileno, *landphonelb, *mail_lb, *address_lb,*mobileno_busi,*address_lb_busi,*mail_lb_busi;
    
    UIImageView *toggleimage, *address_img, *landphone_img, *mobileimg, *detailimg,*website_img, *mailimg, *companyimg, *desigimg, *address_img_busi, *landphone_img_busi, *mobileimg_busi, *mailimg_busi,*moreImage, *logo_img, *prof_img;
    NSString *mobile,*landline,*mail,*website,*address, *businessAddress,*base64String, *googleMapUrlString, *appleMapUrlString, *desig, *company,*mobile_busi,*mail_busi,*fullname;
    UIImage *profilepic;
    int move;
    double latitude,longitude;
    UIView *mapview,*tapbgview,*businessview,*personalview, *b_companyView, *designationView, *b_emailView, *b_phoneView, *b_addressView;
    UIButton *crossbt,*navigatebt,*navigatebtapple;
    UIActivityIndicatorView *activity;
    NSArray *uniquearray;
    NSMutableArray *con_array, *app_contacts, *check_app_cont,*array_menu,*array_image,*arraymenu_busi;
    UIScrollView *boxscroll,*b_boxscroll;
    UIButton *twitterbt, *pinterestbt, *facebookbt, *gplusbt, *youtubebt, *instagrambt, *linkedinbt, *skypebt, *soundcloudbt, *vimeobt;
    int counter, profile_insertion, profile_fetch;
    UIView *divider_1, *divider_2, *divider_3, *divider_4, *divider_5,*busi_comp_divi,*busi_desig_divi,*busi_mob_divi,*busi_mail_divi,*busi_add_divi,*busi_box_divi;
    NSMutableArray *mobileArray, *socialImage;
    NSMutableData *responseData;
    NSString *user_id,*numberstring,*group_id;
    UILabel *prof_name, *street_number_lbl, *zipcode_lbl, *city_lbl, *country_lbl,*zipcode_lbl_busi,*street_number_lbl_busi,*country_lbl_busi;
    UIView *coverview1, *mobileView;
    NSTimer *timea, *timeb;
    int allowpersonal,nobusiness;
    UIView *headingView, *socialView, *b_websiteView, *websiteView;
    UIView *coverView, *landphoneView, *emailView, *addressView;
    BOOL rightmenuOpen;
    UIView *blackView;
    UIImageView *businessImg,*removeImg, *bck_img, *call_img, *sms_img, *email_img, *location_img;
    UITableView *righttable;
    UILabel *lineDivMenu, *location, *Website, *b_website;
    UIView *divider_9;
    UIButton *bgcallsms, *bgcallsms1;
    UIAlertView *callAlert1, *callAlert2, *smsAlert1, *smsAlert2;
    UIButton *rightMenuBack, *rightMenu, *call_btn, *sms_btn, *email_btn, *location_btn;
    UIView *overlayMenu;
    UITapGestureRecognizer *overlayTap;
    UIView *mobileback, *landlineback, *mailback, *addressback;
    AppDelegate *del;
    UIView *change;
    NSString *street,*city,*zipcode,*country, *number,*street_busi,*city_busi,*zipcode_busi,*country_busi, *number_busi;
    CGFloat y,x,y1;
    BOOL business_con;
    
}

@end

@implementation ConNewDetailsViewController
@synthesize other,uid,mainview,dict_profile,personal_array, business_array, social_array, phonearr,profdict, mapReturn,response,userid,backBtn,p,filter_arr,groupid,groupname,isadmin,membercount,search;

- (BOOL)hidesBottomBarWhenPushed {
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"From ConNewDetailsViewController");
    // Do any additional setup after loading the view.
    del.tabBarController.tabBar.frame = CGRectMake(0, [[UIScreen mainScreen]bounds].size.height, [UIScreen mainScreen].bounds.size.width,44);
    self.tabBarController.tabBar.translucent = YES;
    del.tabBarController.tabBar.hidden = YES;
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    switchprof=0;
    y=0.0;
    x=0.0;
    self.view.backgroundColor=[UIColor whiteColor];
    
    self.navigationController.navigationBarHidden=YES;
    
    if ([response isEqualToString:@"yes"]) //=========OTHER'S PROFILE=======//
    {
        user_id =userid;
        
    }
    
    
    coverView = [[UIView alloc]init];
    coverView.frame = CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.origin.y+64);
    //coverView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:coverView];
    
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
    
    // Add the gradient to the view
    [coverView.layer insertSublayer:gradient atIndex:0];
    
    
    logo_img = [[UIImageView alloc]init];
    logo_img.frame = CGRectMake(27+([UIScreen mainScreen].bounds.size.width-160)/2, 32, 101, 20);
    logo_img.backgroundColor = [UIColor clearColor];
    
    logo_img.image = [UIImage imageNamed:@"new_logo"];
    [coverView addSubview:logo_img];
    
    
    //===============Back button===========//
    
    if ([mapReturn isEqualToString:@"mapBack"]) {
        
        backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        backBtn.frame = CGRectMake(9, 28, [UIImage imageNamed:@"backnew_profileUnselected"].size.width, [UIImage imageNamed:@"backnew_profileUnselected"].size.height);
        backBtn.backgroundColor=[UIColor clearColor];
        [backBtn setBackgroundImage:[UIImage imageNamed:@"backnew_profileUnselected"] forState:UIControlStateNormal];
        [backBtn setBackgroundImage:[UIImage imageNamed:@"backnew_profileSelected"] forState:UIControlStateSelected];
        [backBtn setBackgroundImage:[UIImage imageNamed:@"backnew_profileSelected"] forState:UIControlStateHighlighted];
        [backBtn addTarget:self action:@selector(gobackoption:) forControlEvents:UIControlEventTouchUpInside];
        [coverView addSubview:backBtn];
        
        UILabel *labelHead = [[UILabel alloc]initWithFrame:CGRectMake(30, 22, 100, 40)];
        labelHead.text = @"Map";
        labelHead.backgroundColor = [UIColor clearColor];
        labelHead.textColor = [UIColor whiteColor];
        [coverView addSubview:labelHead];
        
        
    }
    else{
        
        bck_img=[[UIImageView alloc]initWithFrame:CGRectMake(15,32,12,20)];//(13, 39, 10, 22)];
        bck_img.image=[UIImage imageNamed:@"back3"];
        [coverView addSubview:bck_img];
        
        backlbl=[[UILabel alloc]initWithFrame:CGRectMake(bck_img.frame.origin.x+bck_img.frame.size.width+5, 31, 54, 20)];
        backlbl.text=@"Back";
        backlbl.textColor=[UIColor whiteColor];
        backlbl.backgroundColor=[UIColor clearColor];
        //[coverView addSubview:backlbl];
        
        backBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, logo_img.frame.origin.x
                                                          -1, 64)];
        [backBtn setTitle:@"" forState:UIControlStateNormal];
        backBtn.backgroundColor=[UIColor clearColor];
        backBtn.tag=p;
        [backBtn setTitle:@"" forState:UIControlStateNormal];
        [backBtn addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDown];
        [backBtn addTarget:self action:@selector(changecoloragain) forControlEvents:UIControlEventTouchDragExit];
        [backBtn addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDragEnter];
        [backBtn addTarget:self action:@selector(gobackoption:) forControlEvents:UIControlEventTouchUpInside];
        [coverView addSubview:backBtn];
        
    }
    
    
    moreImage=[[UIImageView alloc]initWithFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-37, 32, 22,19)];
    [moreImage setImage:[UIImage imageNamed:@"new_menu"]];
    [coverView addSubview:moreImage];
    
    rightMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightMenu setFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-55,[[UIApplication sharedApplication]statusBarFrame].size.height, 55,coverView.frame.size.height-[[UIApplication sharedApplication]statusBarFrame].size.height)];
    [rightMenu setBackgroundImage:nil forState:UIControlStateNormal];
    [rightMenu setBackgroundImage:nil forState:UIControlStateSelected];
    [rightMenu setBackgroundImage:nil forState:UIControlStateHighlighted];
    [rightMenu setBackgroundColor:[UIColor clearColor]];
    //[rightMenu addTarget:self action:@selector(colorChange:) forControlEvents:UIControlEventTouchDown];
    [rightMenu addTarget:self action:@selector(rightMenu:) forControlEvents:UIControlEventTouchUpInside];
    [coverView addSubview:rightMenu];
    rightMenu.enabled=NO;
    rightmenuOpen = NO;
    
    if ([user_id intValue] == [[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"] intValue])
    {
        moreImage.hidden = YES;
        rightMenu.hidden = YES;
        
    }
    
    
}

-(void)coverMethod: (NSTimer *)timeav
{
    [timeav invalidate];
    [coverview1 removeFromSuperview];
    [SVProgressHUD dismiss];
}

- (void) runSpinAnimationOnView:(UIImageView *)view duration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat;
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ * rotations * duration ];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = repeat;
    
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

-(void)viewDidAppear:(BOOL)animated
{
    
    [super viewDidAppear:YES];
    
    del.tabBarController.tabBar.hidden = YES;
    
    [self.tabBarController.tabBar setHidden:YES];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    nobusiness=0;
    move=0;
    profile_insertion=0;
    profile_fetch=0;
    allowpersonal=0;
    
    self.view.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
    
    del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    /////////////////////////////////////////// New Added //////////////////////////////////////////////
    socialView = [[UIView alloc] init];
    headingView = [[UIView alloc] initWithFrame:CGRectMake(0, 65, [UIScreen mainScreen].bounds.size.width, 101)];
    [headingView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:headingView];
    
    UILabel *heading_div = [[UILabel alloc] initWithFrame:CGRectMake(0, 100.5f, [UIScreen mainScreen].bounds.size.width, 0.5f)];
    [heading_div setBackgroundColor:[UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f]];
    [headingView addSubview:heading_div];
    
    
    
    mainview = [[UIView alloc]initWithFrame:CGRectMake(0, 166, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    [self.view addSubview:mainview];
    ///////////////////////////////////////////////////////////////////////////////////////////////
    mainview.backgroundColor=[UIColor clearColor];
    
    
    mainscroll=[[UIScrollView alloc]initWithFrame:CGRectMake(0.0f, 56, self.view.frame.size.width, mainview.frame.size.height)];
    mainscroll.userInteractionEnabled=YES;
    mainscroll.delegate = self;
    mainscroll.scrollEnabled=YES;
    [mainscroll setContentSize:CGSizeMake(mainscroll.frame.size.width,mainscroll.frame.size.height+55)];
    mainscroll.contentSize = CGSizeMake(self.view.frame.size.width, mainscroll.frame.size.height+55);
    //mainscroll.showsVerticalScrollIndicator=YES;
    mainscroll.showsVerticalScrollIndicator= NO;
    //    [mainscroll setBackgroundColor:[UIColor redColor]];
    [mainview addSubview:mainscroll];
    mainscroll.contentOffset = CGPointMake(0, 0);
    
    DebugLog(@"mainscroll frame: %@",NSStringFromCGRect(mainscroll.frame));
    
    activity = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2.38, 190, 40, 40)];
    activity.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [activity setColor:[UIColor blackColor]];
    //    activity.layer.zPosition=6;
    [mainscroll addSubview:activity];
    [activity startAnimating];
    
    NSOperationQueue *queue=[NSOperationQueue new];
    NSInvocationOperation *op=[[NSInvocationOperation alloc] initWithTarget:self selector:@selector(loaddata) object:nil];
    [queue addOperation:op];
    
    //[mainview bringSubviewToFront:boxscroll];
    rightMenu.enabled=YES;
    rightmenuOpen = YES;
}


-(void)loaddata
{
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"firstProfile"] isEqualToString:@"yes"])
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSError *error;
        NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=profile&id=%@&access_token=%@&device_id=%@&image=true",user_id,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
        
        //DebugLog(@"profile url: %@",urlString1);
        
        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
        
        if (signeddataURL1 != nil)
            
            json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                    options:kNilOptions
                                                      error:&error];
        
        else
        {
            DebugLog(@"no connnn profile");
            alert = [[UIAlertView alloc] initWithTitle:@"Error in Profile Server Connection!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
        }
        
        NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
        //DebugLog(@"err  %@",errornumber);
        
        if (![errornumber isEqualToString:@"0"])
        {
            [activity removeFromSuperview];
            NSString *err_str = [json1 objectForKey:@"error"];
            alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                               message:err_str
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
        }
        
        else
            
        {
            //DebugLog(@"data: %@",json1);
            dict_profile = [[NSMutableDictionary alloc]init];
            
            profdict= [json1 objectForKey:@"details"];
            
            dict_profile = [profdict objectForKey:@"profile"];
            
            [self performSelectorOnMainThread:@selector(startDBInserta:)
                                   withObject:nil
                                waitUntilDone:YES];
            
            
            [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"firstProfile"];
            //DebugLog(@"dict pro%@",dict_profile);
            
            if (![[NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]] isKindOfClass:[NSNull class]] && ![[NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]] isEqualToString:@""] && ![[NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]] isEqualToString:@"(null)"] && [[NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]] length]>0)
                
                fullname= [NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]];
            
            [prof_name removeFromSuperview];
            base64String= [profdict objectForKey:@"image"];
            
            if ([user_id intValue] == [[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]intValue])
            {
                switchprof=0;
                [self loadViews];
            }
            else
            {
                if ([[profdict objectForKey:@"friend"]intValue] == 1)
                {
                    switchprof=0;
                    [self loadViews];
                }
                
                else if ([[profdict objectForKey:@"business"] intValue]==1)
                {
                    switchprof=1;
                    allowpersonal=1;
                    [self business];
                }
                // DebugLog(@"fields are: %d , %d",[[profdict objectForKey:@"own"]intValue], switchprof);
            }
            [self performSelectorOnMainThread:@selector(displaydata) withObject:nil waitUntilDone:NO];
        }
        
        
    }
    
    /////////////////////////////////////////////   Load from Local DB   ////////////////////////////////////////////////////////////////////
    
    else
    {
        allowpersonal=0;
        DebugLog(@"eittttto etatei ");
        NSDictionary *localDBProfile = [[NSDictionary alloc]init];
        
        localDBProfile = [[DBManager getSharedInstance]fetchProfile:[user_id intValue]];
        
        DebugLog(@"this gives: %@",localDBProfile);
        
        if ([[localDBProfile objectForKey:@"own"] intValue]==0) {
            
            if ([[localDBProfile objectForKey:@"business"] intValue]==1) {
                business_con = TRUE;
            }
        }
        
        NSArray * allKeys = [localDBProfile allKeys];
        // DebugLog(@"Count : %lu", (unsigned long)[allKeys count]);
        
        if ([allKeys count] == 0 || [[localDBProfile objectForKey:@"name"] isKindOfClass:[NSNull class]] || [[localDBProfile objectForKey:@"name"] length]==0 || [[localDBProfile objectForKey:@"name"] isEqualToString:@"(null)"])
            
        {
            DebugLog(@"this gives null profile");
            
            [self reloadDataFromWeb];
        }
        else
        {
            //        [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"profile_edited"];
            
            if ([user_id intValue] == [[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"] intValue] && [[[NSUserDefaults standardUserDefaults]objectForKey:@"profile_edited"] isEqualToString:@"yes"])
                
                [self reloadDataFromWeb];
            
            dict_profile = [[NSMutableDictionary alloc]init];
            
            
            dict_profile = [localDBProfile mutableCopy];
            
            
            
            prof_img = [[UIImageView alloc]initWithFrame:CGRectMake(12, 12, 76, 76)];
            prof_img.layer.cornerRadius = prof_img.frame.size.width/2;
            
            [headingView addSubview:prof_img];
            
            
            [self performSelector:@selector(reloadDataFromWeb)];
        }
        if (![[NSUserDefaults standardUserDefaults] boolForKey:@"error"])
        {
            fullname= [NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]];
            if ([profdict objectForKey:@"image"] == nil) {
                base64String= [profdict objectForKey:@"thumb"];
            }
            else
                base64String= [profdict objectForKey:@"image"];
            
            prof_name.frame = CGRectMake(0, 0, 0, 0);
            [headingView addSubview:prof_name];
            [self performSelectorOnMainThread:@selector(displaydata) withObject:nil waitUntilDone:YES];
        }
        else
        {
            [activity removeFromSuperview];
            UIAlertView *showalert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Cannot load data. Please log in again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [showalert show];
        }
        
    }
    
}

-(void)displaydata
{
    [activity removeFromSuperview];
    
    prof_img = [[UIImageView alloc]initWithFrame:CGRectMake(12, 12, 76, 76)];
    prof_img.layer.cornerRadius = prof_img.frame.size.width/2;
    
    
    prof_name = [[UILabel alloc]init]; //WithFrame:CGRectMake(112, 72, 196, 60)];
    prof_name.backgroundColor=[UIColor clearColor];
    prof_name.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
    if ([fullname isKindOfClass:[NSNull class]] || [fullname isEqualToString:@""] || [fullname isEqualToString:@"(null) (null)"] || [fullname length]==0) {
        
        prof_name.text = @"";
        prof_name.frame = CGRectMake(0, 0, 0, 0);
        DebugLog(@"PROFILE NAME:%@",prof_name.text);
    }
    else
    {
        prof_name.text=fullname;
        DebugLog(@"PROFILE NAME:%@",prof_name.text);
        prof_name.frame=CGRectMake(prof_img.frame.origin.x+prof_img.frame.size.width+24, 17, [UIScreen mainScreen].bounds.size.width-120, 35);
        [headingView addSubview:prof_name];
        prof_name.font=[UIFont fontWithName:@"ProximaNova-Bold" size:17];
        [prof_name setBackgroundColor:[UIColor clearColor]];
        prof_name.numberOfLines = 2;
        //        prof_name.text = [NSString stringWithFormat:@"%@",[_Condetails_dict objectForKey:@"fullname"]];
        prof_name.lineBreakMode = NSLineBreakByWordWrapping;
        prof_name.textColor = [UIColor blackColor];
        prof_name.textAlignment = NSTextAlignmentLeft;
        [headingView addSubview:prof_name];
        [prof_name adjustsFontSizeToFitWidth];
        
        // [prof_name removeFromSuperview];
        
        UIView *locationView = [[UIView alloc] init];
        locationView.frame = CGRectMake(prof_img.frame.origin.x+prof_img.frame.size.width+24, 52, [UIScreen mainScreen].bounds.size.width-120, 35);
        locationView.backgroundColor = [UIColor clearColor];
        [headingView addSubview:locationView];
        
        UIImageView *loc_img = [[UIImageView alloc] init];
        loc_img.frame = CGRectMake(0, 8, 7, 9);
        loc_img.image = [UIImage imageNamed:@"Loction"];
        [locationView addSubview:loc_img];
        
        location = [[UILabel alloc] init];
        location.frame = CGRectMake(12, 5, [UIScreen mainScreen].bounds.size.width-100, 17);
        location.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
        location.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        location.text = [[NSString stringWithFormat:@"%@, %@",[dict_profile objectForKey:@"city"],[dict_profile objectForKey:@"country"]] uppercaseString];
        [locationView addSubview:location];
        
    }
    
    
    [headingView addSubview:prof_img];
    
    if ([base64String length] >6)
    {
        NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
        
        
        profilepic = [UIImage imageWithData:decodedData];
        prof_img.image=profilepic;
        prof_img.contentMode= UIViewContentModeScaleAspectFill;
        prof_img.clipsToBounds=YES;
    }
    
    prof_img.userInteractionEnabled=YES;
    
    UITapGestureRecognizer *propictap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(detailpic:)];
    
    [prof_img addGestureRecognizer:propictap];
    
    
    
    toggleimage = [[UIImageView alloc]init];
    
    toggleimage.backgroundColor=[UIColor clearColor];
    
    [mainscroll addSubview:toggleimage];
    
    
    personaltext = [[UILabel alloc]initWithFrame:CGRectMake(132, 138-60, 75, 25)];
    [mainscroll addSubview:personaltext];
    personaltext.backgroundColor=[UIColor clearColor];
    if (switchprof == 0)
        personaltext.textColor=[UIColor whiteColor];
    else
        personaltext.textColor=[UIColor lightGrayColor];
    
    personaltext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:15];
    
    businesstext = [[UILabel alloc]initWithFrame:CGRectMake(225, 138-60, 75, 25)];
    
    [mainscroll addSubview:businesstext];
    
    businesstext.backgroundColor=[UIColor clearColor];
    
    if (switchprof == 0)
        businesstext.textColor=[UIColor lightGrayColor];
    
    else
        businesstext.textColor=[UIColor whiteColor];
    
    businesstext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:15];
    
    if ([user_id intValue] == [[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"] intValue])
    {
        UIButton *editprofbt= [UIButton buttonWithType:UIButtonTypeCustom];
        [mainview addSubview:editprofbt];
        //[editprofbt setBackgroundImage:[UIImage imageNamed:@"editprofbt.png"] forState:UIControlStateNormal];
        //        editprofbt.frame = CGRectMake(120, 136.5f-60, 185, 28.5f);
        editprofbt.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 56);
        [editprofbt setBackgroundColor:[UIColor colorWithRed:(49.0f/255.0f) green:(160.0f/255.0f) blue:(183.0f/255.0f) alpha:1.0f]];
        [editprofbt setTitle:@"EDIT PROFILE" forState:UIControlStateNormal];
        editprofbt.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
        [editprofbt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [editprofbt addTarget:self action:@selector(editprofile_func:) forControlEvents:UIControlEventTouchUpInside];
        
//        moreImage.hidden = YES;
//        rightMenu.hidden = YES;
        
    }
    else
    {
        
        UIView *actionView = [[UIView alloc] init];
        actionView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 56);
        [mainview addSubview:actionView];
        
        UILabel *actionView_div = [[UILabel alloc] init];
        actionView_div.frame = CGRectMake(0, 55.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
        actionView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        [actionView addSubview:actionView_div];
        
        call_img = [[UIImageView alloc] init];
        call_img.frame = CGRectMake(([UIScreen mainScreen].bounds.size.width/4-23)/2, 16.5, 23, 23);
        call_img.image = [UIImage imageNamed:@"Call"];
        [actionView addSubview:call_img];
        
        UILabel *call_div = [[UILabel alloc] init];
        call_div.frame = CGRectMake(call_img.frame.origin.x+call_img.frame.size.width+([UIScreen mainScreen].bounds.size.width/4-23)/2-0.5f, 0, 0.5f, 56);
        call_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        [actionView addSubview:call_div];
        
        call_btn = [[UIButton alloc] init];
        call_btn.frame = CGRectMake(0, 0, call_img.frame.origin.x+call_img.frame.size.width+35-0, 56);
        [call_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [call_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
        [call_btn addTarget:self action:@selector(phonefunc:) forControlEvents:UIControlEventTouchUpInside];
        [call_btn setBackgroundColor:[UIColor clearColor]];
        [actionView addSubview:call_btn];
        
        sms_img = [[UIImageView alloc] init];
        sms_img.frame = CGRectMake(call_div.frame.origin.x+call_div.frame.size.width+([UIScreen mainScreen].bounds.size.width/4-23)/2, 16.5, 23, 23);
        sms_img.image = [UIImage imageNamed:@"SMS"];
        [actionView addSubview:sms_img];
        
        UILabel *sms_div = [[UILabel alloc] init];
        sms_div.frame = CGRectMake(sms_img.frame.origin.x+sms_img.frame.size.width+([UIScreen mainScreen].bounds.size.width/4-23)/2-0.5f, 0, 0.5f, 56);
        sms_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        [actionView addSubview:sms_div];
        
        sms_btn = [[UIButton alloc] init];
        sms_btn.frame = CGRectMake(call_div.frame.origin.x+call_div.frame.size.width, 0, sms_img.frame.origin.x+sms_img.frame.size.width+40-(call_div.frame.origin.x+call_div.frame.size.width+5), 56);
        [sms_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [sms_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
        [sms_btn addTarget:self action:@selector(smsfunc) forControlEvents:UIControlEventTouchUpInside];
        [sms_btn setBackgroundColor:[UIColor clearColor]];
        [actionView addSubview:sms_btn];
        
        email_img = [[UIImageView alloc] init];
        email_img.frame = CGRectMake(sms_div.frame.origin.x+sms_div.frame.size.width+([UIScreen mainScreen].bounds.size.width/4-23)/2, 16.5, 23, 23);
        email_img.image = [UIImage imageNamed:@"Email"];
        [actionView addSubview:email_img];
        
        UILabel *email_div = [[UILabel alloc] init];
        email_div.frame = CGRectMake(email_img.frame.origin.x+email_img.frame.size.width+([UIScreen mainScreen].bounds.size.width/4-23)/2-0.5f, 0, 0.5f, 56);
        email_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        [actionView addSubview:email_div];
        
        email_btn = [[UIButton alloc] init];
        email_btn.frame = CGRectMake(sms_div.frame.origin.x+sms_div.frame.size.width, 0, email_img.frame.origin.x+email_img.frame.size.width+40-(sms_div.frame.origin.x+sms_div.frame.size.width+5), 56);
        [email_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [email_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
        [email_btn addTarget:self action:@selector(mailFun:) forControlEvents:UIControlEventTouchUpInside];
        [email_btn setBackgroundColor:[UIColor clearColor]];
        [actionView addSubview:email_btn];
        
        location_img = [[UIImageView alloc] init];
        location_img.frame = CGRectMake(email_div.frame.origin.x+email_div.frame.size.width+([UIScreen mainScreen].bounds.size.width/4-23)/2, 16.5, 23, 23);
        location_img.image = [UIImage imageNamed:@"Navigate"];
        [actionView addSubview:location_img];
        
        location_btn = [[UIButton alloc] init];
        location_btn.frame = CGRectMake(email_div.frame.origin.x+email_div.frame.size.width, 0, location_img.frame.origin.x+location_img.frame.size.width+40-(email_div.frame.origin.x+email_div.frame.size.width+5), 56);
        [location_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [location_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
        [location_btn addTarget:self action:@selector(openmap:) forControlEvents:UIControlEventTouchUpInside];
        [location_btn setBackgroundColor:[UIColor clearColor]];
        [actionView addSubview:location_btn];
        
        
        
    }
    if ([user_id intValue] == [[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]intValue])
    {
        switchprof=0;
        [activity stopAnimating];
        if (business_con) {
            [self business];
        }
        else
        {
        [self loadViews];
        }
    }
    else
    {
        if ([[profdict objectForKey:@"friend"]intValue] == 1)
        {
            switchprof=0;
            [activity stopAnimating];
            [self loadViews];
        }
        
        else if ([[profdict objectForKey:@"business"] intValue]==1)
        {
            switchprof=1;
            [activity stopAnimating];
            [self business];
        }
        DebugLog(@"fields are: %d , %d",[[profdict objectForKey:@"own"]intValue], switchprof);
    }
    
}



-(void)changecoloragain
{
    //    change.backgroundColor=[UIColor clearColor];
    backlbl.alpha = 1.0f;
    bck_img.alpha = 1.0f;
    
}
-(void)changecolor
{
    //    change.backgroundColor=[UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0];
    backlbl.alpha = 0.5f;
    bck_img.alpha = 0.5f;
}

-(void)business
{
    phonearr=[[NSMutableArray alloc]init];
    //    [divider_5 removeFromSuperview];
    [personalview removeFromSuperview];
    [mobileView removeFromSuperview];
    [landphoneView removeFromSuperview];
    [emailView removeFromSuperview];
    [addressView removeFromSuperview];
    //    [boxscroll removeFromSuperview];
    [activity removeFromSuperview];
    
    mobile=nil;
    landline=nil;
    mail=nil;
    street=nil;
    number=nil;
    zipcode=nil;
    city=nil;
    country=nil;
    boxscroll=nil;
    personalview=nil;
    //[mobileno removeFromSuperview];
    //[landphonelb removeFromSuperview];
    //[mail_lb removeFromSuperview];
    //[address_lb removeFromSuperview];
    personalview=nil;
    [activity stopAnimating];
    businessview=[[UIScrollView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, [UIScreen mainScreen].bounds.size.width, mainscroll.frame.size.height)];
    businessview.userInteractionEnabled=YES;
    businessview.backgroundColor=[UIColor clearColor];
    [mainscroll addSubview:businessview];
    
    
    b_companyView = [[UIView alloc] init];
    
    
    if ([dict_profile objectForKey:@"b_company"] != false && ![[dict_profile objectForKey:@"b_company"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_company"] length] > 1 && ![[dict_profile objectForKey:@"b_company"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"b_company"] isEqualToString:@""]){
        
        nobusiness=1;
        company= [NSString stringWithFormat:@"%@",[[dict_profile objectForKey:@"b_company"] stringByReplacingOccurrencesOfString:@"amp;" withString:@""]];
        
        DebugLog(@"company divider frame: %@",NSStringFromCGRect(busi_comp_divi.frame));
        
        b_companyView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 62);
        b_companyView.backgroundColor = [UIColor clearColor];
        [businessview addSubview:b_companyView];
        
        
        UILabel *b_companyView_div = [[UILabel alloc] init];
        b_companyView_div.frame = CGRectMake(0, 61.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
        b_companyView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        [b_companyView addSubview:b_companyView_div];
        
        UILabel *companyLabel = [[UILabel alloc] init];
        companyLabel.frame = CGRectMake(20, 10, 100, 22);
        companyLabel.text = @"COMPANY";
        companyLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        companyLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
        [b_companyView addSubview:companyLabel];
        
        UILabel *company_name = [[UILabel alloc] init];
        company_name.frame = CGRectMake(20, 32, 150, 22);
        company_name.textColor = [UIColor blackColor];
        company_name.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
        company_name.text = company;
        
        [b_companyView addSubview:company_name];
        
        
        
    }
    else
    {
        b_companyView.frame = CGRectMake(0, 0, 0, 0);
    }
    //===============BUSI-DESIGNATION==============//
    
    designationView = [[UIView alloc] init];
    
    
    if ([dict_profile objectForKey:@"b_function"] != false && ![[dict_profile objectForKey:@"b_function"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_function"] length] > 1 && ![[dict_profile objectForKey:@"b_function"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"b_function"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"b_function"] isEqualToString:@""])
    {
        
        desig= [NSString stringWithFormat:@"%@",[[dict_profile objectForKey:@"b_function"] stringByReplacingOccurrencesOfString:@"amp;" withString:@""]];
        DebugLog(@"company bhaswar: %@",desig);
        
        
        
        
        designationView.frame = CGRectMake(0, b_companyView.frame.origin.y+b_companyView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 62);
        
        //    designationView.frame = CGRectMake(0, b_companyView_div.frame.origin.y+b_companyView_div.frame.size.height, [UIScreen mainScreen].bounds.size.width, 62);
        designationView.backgroundColor = [UIColor clearColor];
        [businessview addSubview:designationView];
        
        
        
        UILabel *designationView_div = [[UILabel alloc] init];
        designationView_div.frame = CGRectMake(0, 61.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
        designationView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        [designationView addSubview:designationView_div];
        
        UILabel *desigLabel = [[UILabel alloc] init];
        desigLabel.frame = CGRectMake(20, 10, 100, 22);
        desigLabel.text = @"DESIGNATION";
        desigLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        desigLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
        [designationView addSubview:desigLabel];
        
        UILabel *designation = [[UILabel alloc] init];
        designation.frame = CGRectMake(20, 32, 270, 22);
        designation.textColor = [UIColor blackColor];
        designation.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
        designation.text = desig;
        [designationView addSubview:designation];
        
        
    }
    else
    {
        DebugLog(@" DEISG:%@",desig);
        designationView.frame = CGRectMake(0, b_companyView.frame.origin.y+b_companyView.frame.size.height, 0, 0);
    }
    
    //=================BUSI-MOBILE================//
    b_phoneView = [[UIView alloc] init];
    
    
    if ([dict_profile objectForKey:@"b_phone_num"] != false && ![[dict_profile objectForKey:@"b_phone_num"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_phone_num"] length] > 1 && ![[dict_profile objectForKey:@"b_phone_num"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"b_phone_num"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"b_phone_num"] isEqualToString:@""]){
        
        
        
        mobile_busi = [NSString stringWithFormat:@"%@%@",[dict_profile objectForKey:@"b_phone_pre"],[dict_profile objectForKey:@"b_phone_num"]];
        
        DebugLog(@"LANDLINE:%@",mobile_busi);
        
        b_phoneView.frame = CGRectMake(0, designationView.frame.origin.y+designationView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 62);
        [phonearr addObject:mobile_busi];
        b_phoneView.backgroundColor = [UIColor clearColor];
        [businessview addSubview:b_phoneView];
        
        UILabel *b_phoneView_div = [[UILabel alloc] init];
        b_phoneView_div.frame = CGRectMake(0, 61.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
        b_phoneView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        [b_phoneView addSubview:b_phoneView_div];
        
        UILabel *b_phoneLabel = [[UILabel alloc] init];
        b_phoneLabel.frame = CGRectMake(20, 10, 100, 22);
        b_phoneLabel.text = @"PHONE";
        b_phoneLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        b_phoneLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
        [b_phoneView addSubview:b_phoneLabel];
        
        UILabel *b_phone = [[UILabel alloc] init];
        b_phone.frame = CGRectMake(20, 32, 200, 22);
        b_phone.backgroundColor = [UIColor clearColor];
        b_phone.textColor = [UIColor blackColor];
        b_phone.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
        b_phone.text = mobile_busi;
        
        [b_phoneView addSubview:b_phone];
        
        UIImageView *whatsapp_img = [[UIImageView alloc] init];
        whatsapp_img.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-50, 20, 22, 22);
        whatsapp_img.image = [UIImage imageNamed:@"Whatsapp"];
        [b_phoneView addSubview:whatsapp_img];
        
        UIButton *whatsapp_btn = [[UIButton alloc] init];
        whatsapp_btn.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-65, 10, 52, 42);
        [whatsapp_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [whatsapp_btn setBackgroundColor:[UIColor clearColor]];
        [whatsapp_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
        [whatsapp_btn addTarget:self action:@selector(whatsapp_func) forControlEvents:UIControlEventTouchUpInside];
        [b_phoneView addSubview:whatsapp_btn];
        
        UIButton *b_phoneView_btn = [[UIButton alloc] init];
        b_phoneView_btn.tag = 3;
        b_phoneView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-65, 62);
        [b_phoneView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [b_phoneView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
        [b_phoneView_btn addTarget:self action:@selector(phonefunc:) forControlEvents:UIControlEventTouchUpInside];
        [b_phoneView addSubview:b_phoneView_btn];
        
    }
    else
    {
        b_phoneView.frame = CGRectMake(0, designationView.frame.origin.y+designationView.frame.size.height, 0, 0);
    }
    
    //============BUSI-MAIL===========//
    NSString *addressValue1;
    b_emailView = [[UIView alloc] init];
    
    
    if ([dict_profile objectForKey:@"b_email"] != false && ![[dict_profile objectForKey:@"b_email"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_email"] length] > 1 && ![[dict_profile objectForKey:@"b_email"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"b_email"] isEqualToString:@""])
    {
        mail_busi= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_email"]];
        mail = mail_busi;
        
        b_emailView.frame = CGRectMake(0, b_phoneView.frame.origin.y+b_phoneView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 62);
        b_emailView.backgroundColor = [UIColor clearColor];
        [businessview addSubview:b_emailView];
        
        UILabel *b_emailView_div = [[UILabel alloc] init];
        b_emailView_div.frame = CGRectMake(0, 61.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
        b_emailView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        [b_emailView addSubview:b_emailView_div];
        
        UILabel *b_emailLabel = [[UILabel alloc] init];
        b_emailLabel.frame = CGRectMake(20, 10, 100, 22);
        b_emailLabel.text = @"EMAIL";
        b_emailLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        b_emailLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
        [b_emailView addSubview:b_emailLabel];
        
        UILabel *b_email = [[UILabel alloc] init];
        b_email.frame = CGRectMake(20, 32, 270, 22);
        b_email.backgroundColor = [UIColor clearColor];
        b_email.textColor = [UIColor blackColor];
        b_email.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
        b_email.text = mail_busi;
        [b_emailView addSubview:b_email];
        
        UIButton *b_emailView_btn = [[UIButton alloc] init];
        b_emailView_btn.tag =3;
        b_emailView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 62);
        [b_emailView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [b_emailView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
        [b_emailView_btn addTarget:self action:@selector(mailFun:) forControlEvents:UIControlEventTouchUpInside];
        [b_emailView addSubview:b_emailView_btn];
        
    }
    else
    {
        b_emailView.frame = CGRectMake(0, b_phoneView.frame.origin.y+b_phoneView.frame.size.height, 0, 0);
    }
    
    /////////////////////////////////////////////// WEBSITE /////////////////////////////////////////
    
    b_websiteView = [[UIView alloc] init];
    
    if ([dict_profile objectForKey:@"b_website"] != false && ![[dict_profile objectForKey:@"b_website"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_website"] length] > 1 && ![[dict_profile objectForKey:@"b_website"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"b_website"] isEqualToString:@""]) {
        
        b_websiteView.frame = CGRectMake(0, b_emailView.frame.origin.y+b_emailView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 62);
        b_websiteView.backgroundColor = [UIColor clearColor];
        [businessview addSubview:b_websiteView];
        
        UILabel *b_websiteView_div = [[UILabel alloc] init];
        b_websiteView_div.frame = CGRectMake(0, 61.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
        b_websiteView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        [b_websiteView addSubview:b_websiteView_div];
        
        UILabel *b_websiteLabel = [[UILabel alloc] init];
        b_websiteLabel.frame = CGRectMake(20, 10, 100, 22);
        b_websiteLabel.text = @"WEBSITE";
        b_websiteLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        b_websiteLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
        [b_websiteView addSubview:b_websiteLabel];
        
        
        //[landphonelb addTarget:self action:@selector(landphoneFun:) forControlEvents:UIControlEventTouchUpInside];
        
        b_website = [[UILabel alloc] init];
        b_website.frame = CGRectMake(20, 32, 180, 22);
        b_website.text = [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_website"]];
        b_website.textColor = [UIColor blackColor];
        b_website.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
        [b_websiteView addSubview:b_website];
        
        UIButton *b_websiteView_btn = [[UIButton alloc] init];
        b_websiteView_btn.tag = 1;
        b_websiteView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 62);
        [b_websiteView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [b_websiteView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
        [b_websiteView_btn addTarget:self action:@selector(websiteFun:) forControlEvents:UIControlEventTouchUpInside];
        [b_websiteView addSubview:b_websiteView_btn];
        
    }
    else
    {
        b_websiteView.frame = CGRectMake(0, b_emailView.frame.origin.y+b_emailView.frame.size.height, 0,0);
    }

    
    b_addressView = [[UIView alloc] init];
    
    if ([dict_profile objectForKey:@"b_street"] != false && ![[dict_profile objectForKey:@"b_street"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_street"] length]!=0 && ![[dict_profile objectForKey:@"b_street"] isEqualToString:@""])
    {
        //        b_addressView.frame = CGRectMake(0, b_emailView.frame.origin.y+b_emailView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 106);
        addressValue1 = [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_street"]];
        
    }
    
    else
    {
        addressValue1 = @"";
        
    }
    if ([dict_profile objectForKey:@"b_housenumber"] != false && ![[dict_profile objectForKey:@"b_housenumber"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_housenumber"] length]!=0 && ![[dict_profile objectForKey:@"b_housenumber"] isEqualToString:@""] && ![[dict_profile objectForKey:@"b_housenumber"] isEqualToString:@"0"])
    {
        addressValue1 = [addressValue1 stringByAppendingString:[NSString stringWithFormat:@"%@<br>",[dict_profile objectForKey:@"b_housenumber"]]];
    }
    else
    {
        addressValue1 = [addressValue1 stringByAppendingString:[NSString stringWithFormat:@"<br>"]];
        
    }
    if ([dict_profile objectForKey:@"b_zipcode"] != false && ![[dict_profile objectForKey:@"b_zipcode"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_zipcode"] length]!=0 && ![[dict_profile objectForKey:@"b_zipcode"] isEqualToString:@""] && ![[dict_profile objectForKey:@"b_zipcode"] isEqualToString:@"0"])
    {
        
        nobusiness=1;
        addressValue1 = [addressValue1 stringByAppendingString:[NSString stringWithFormat:@"%@ ",[dict_profile objectForKey:@"b_zipcode"]]];
        
    }
    if ([dict_profile objectForKey:@"b_city"] != false && ![[dict_profile objectForKey:@"b_city"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"b_city"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_city"] length]!=0 && ![[dict_profile objectForKey:@"b_city"] isEqualToString:@""] && ![[dict_profile objectForKey:@"b_city"] isEqualToString:@"0"])
    {
        addressValue1 = [addressValue1 stringByAppendingString:[NSString stringWithFormat:@"%@<br>",[dict_profile objectForKey:@"b_city"]]];
        
    }
    
    if ([dict_profile objectForKey:@"b_country"] != false && ![[dict_profile objectForKey:@"b_country"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_country"] length]!=0 && ![[dict_profile objectForKey:@"b_country"] isEqualToString:@""] && ![[dict_profile objectForKey:@"b_country"] isEqualToString:@"0"])
    {
        addressValue1 = [addressValue1 stringByAppendingString:[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_country"]]];
        
        //addressValue = [NSString stringWithFormat:@"%@<br>%@<br>%@",[dict_profile objectForKey:@"street"],[dict_profile objectForKey:@"city"],[dict_profile objectForKey:@"country"]];
        //        [b_address setValue:addressValue forKey:@"contentToHTMLString"];
        
        
    }
    
    if ([addressValue1 isEqualToString:@""] || [addressValue1 isEqualToString:@"<br>"]) {
        b_addressView.frame = CGRectMake(0, b_websiteView.frame.origin.y+b_websiteView.frame.size.height, 0, 0);
    }
    else
    {
        DebugLog(@"AddressValue:%@",addressValue1);
        
        b_addressView.frame = CGRectMake(0, b_websiteView.frame.origin.y+b_websiteView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 106);
        b_addressView.backgroundColor = [UIColor clearColor];
        [businessview addSubview:b_addressView];
        
        UILabel *b_addressView_div = [[UILabel alloc] init];
        b_addressView_div.frame = CGRectMake(0, 105.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
        b_addressView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        [b_addressView addSubview:b_addressView_div];
        
        UILabel *b_addressLabel = [[UILabel alloc] init];
        b_addressLabel.frame = CGRectMake(20, 10, 100, 22);
        b_addressLabel.text = @"ADDRESS";
        b_addressLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        b_addressLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
        [b_addressView addSubview:b_addressLabel];
        
        UITextView *b_address = [[UITextView alloc] init];
        b_address.frame = CGRectMake(15, 32, [UIScreen mainScreen].bounds.size.width-15, 70);
        b_address.backgroundColor = [UIColor clearColor];
        b_address.text = addressValue1;
        
        [b_address setValue:addressValue1 forKey:@"contentToHTMLString"];
        [b_address setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
        b_address.editable = NO;
        //b_address.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
        [b_addressView addSubview:b_address];
        
        UIButton *b_addressView_btn = [[UIButton alloc] init];
        b_addressView_btn.tag = 3;
        b_addressView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 106);
        [b_addressView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [b_addressView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
        [b_addressView_btn addTarget:self action:@selector(openmap:) forControlEvents:UIControlEventTouchUpInside];
        [b_addressView addSubview:b_addressView_btn];
        
        
    }
    
    socialImage = [[NSMutableArray alloc] init];
    
    socialView.frame = CGRectMake(0,b_addressView.frame.origin.y+b_addressView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 118);
    [businessview addSubview:socialView];
    
    if ([dict_profile objectForKey:@"s_facebook"] != false && ![[dict_profile objectForKey:@"s_facebook"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"s_facebook"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_facebook"] isEqualToString:@""]) {
        
       // fb = 1;
        [socialImage addObject:@"Facebook"];
        
        //count++;
    }
    
    if ([dict_profile objectForKey:@"s_google"] != false && ![[dict_profile objectForKey:@"s_google"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"s_google"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_google"] isEqualToString:@""]) {
        
        //google = 1;
        [socialImage addObject:@"Google+"];
        //count++;
        
    }
    if ([dict_profile objectForKey:@"s_linkedin"] != false && ![[dict_profile objectForKey:@"s_linkedin"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"s_linkedin"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_linkedin"] isEqualToString:@""]) {
        
        //link = 1;
        [socialImage addObject:@"Linkedin"];
        //count++;
    }
    if ([dict_profile objectForKey:@"s_instagram"] != false && ![[dict_profile objectForKey:@"s_instagram"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"s_facebook"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_instagram"] isEqualToString:@""]) {
        
        //inst = 1;
        [socialImage addObject:@"Instagram"];
        //count++;
    }
    if ([dict_profile objectForKey:@"s_pinterest"] != false && ![[dict_profile objectForKey:@"s_pinterest"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"s_pinterest"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_pinterest"] isEqualToString:@""]) {
        
        //pint = 1;
        [socialImage addObject:@"Pinterest"];
        //count++;
    }
    if ([dict_profile objectForKey:@"s_skype"] != false && ![[dict_profile objectForKey:@"s_skype"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"s_skype"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_skype"] isEqualToString:@""]) {
        
        //sky = 1;
        [socialImage addObject:@"Skype"];
       // count++;
    }
    if ([dict_profile objectForKey:@"s_vimeo"] != false && ![[dict_profile objectForKey:@"s_vimeo"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"s_vimeo"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_vimeo"] isEqualToString:@""]) {
        
        //vim = 1;
        [socialImage addObject:@"Vimeo"];
       // count++;
    }
    
    if ([dict_profile objectForKey:@"s_twitter"] != false && ![[dict_profile objectForKey:@"s_twitter"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"s_twitter"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_twitter"] isEqualToString:@""]) {
        
        //twt = 1;
        [socialImage addObject:@"Twitter"];
        //count++;
    }
    
    if ([dict_profile objectForKey:@"s_soundcloud"] != false && ![[dict_profile objectForKey:@"s_soundcloud"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"s_soundcloud"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_soundcloud"] isEqualToString:@""]) {
        
        //sound = 1;
        [socialImage addObject:@"Soundcloud"];
        
        //count++;
    }
    if ([dict_profile objectForKey:@"s_youtube"] != false && ![[dict_profile objectForKey:@"s_youtube"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"s_youtube"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_youtube"] isEqualToString:@""]) {
        
        //you = 1;
        [socialImage addObject:@"YouTube"];
        //count++;
    }
    
    float x_axis = 18*(float)([UIScreen mainScreen].bounds.size.width/320), y_axis;
    for (int i=0; i<[socialImage count]; i++) {
        
        if (i<=5) {
            y_axis = 18;
        }
        else
        {
            y_axis = 68;
        }
        DebugLog(@"IMAGE:%@",[socialImage objectAtIndex:i]);
        UIImageView *social_Imgview = [[UIImageView alloc] init];
        //social_Imgview.backgroundColor = [UIColor greenColor];
        social_Imgview.frame = CGRectMake(x_axis, y_axis, 32, 32);
        social_Imgview.image = [UIImage imageNamed:[socialImage objectAtIndex:i]];
        [socialView addSubview:social_Imgview];
        
        UIButton *social_btn = [[UIButton alloc] init];
        social_btn.frame = CGRectMake(social_Imgview.frame.origin.x-5, social_Imgview.frame.origin.y-5, social_Imgview.frame.size.width+10, social_Imgview.frame.size.height+10);
        social_btn.tag = i;
        [social_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [social_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
        [social_btn addTarget:self action:@selector(socialFunc:) forControlEvents:UIControlEventTouchUpInside];
        [socialView addSubview:social_btn];
        if (i==5) {
            
            x_axis = 18*(float)([UIScreen mainScreen].bounds.size.width/320);
        }
        else{
            x_axis = social_Imgview.frame.origin.x + social_Imgview.frame.size.width+18*(float)([UIScreen mainScreen].bounds.size.width/320);
        }
    }
    
    
    DebugLog(@"SOCIAL MEDIA:%@",socialImage);
    
    



}

-(void)loadViews
{
    //    DebugLog(@"SWITCHPROF===============>%d",switchprof);
    [companyimg removeFromSuperview];
    [companylb removeFromSuperview];
    [desigimg removeFromSuperview];
    [desiglb removeFromSuperview];
    [activity removeFromSuperview];
    
    [mobileimg_busi removeFromSuperview];
    [mobileno_busi removeFromSuperview];
    [mailimg_busi removeFromSuperview];
    [mail_lb_busi removeFromSuperview];
    [address_img_busi removeFromSuperview];
    [address_lb_busi removeFromSuperview];
    [businessview removeFromSuperview];
    //[b_boxscroll removeFromSuperview];
    zipcode_busi=nil;
    company=nil;
    desig=nil;
    street_busi=nil;
    country_busi=nil;
    businessview=nil;
    [activity stopAnimating];
    
    //    personalview=[[UIView alloc]initWithFrame:CGRectMake(0.0f, 110.0f, [UIScreen mainScreen].bounds.size.width, mainscroll.frame.size.height)];
    personalview=[[UIView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, [UIScreen mainScreen].bounds.size.width, mainscroll.frame.size.height)];
    
    personalview.userInteractionEnabled=YES;
    personalview.backgroundColor=[UIColor clearColor];
    [mainscroll addSubview:personalview];
    
    
    mobileView = [[UIView alloc] init];
    mobileView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 62);
    mobileView.backgroundColor = [UIColor clearColor];
    [personalview addSubview:mobileView];
    
    UILabel *mobileView_div = [[UILabel alloc] init];
    mobileView_div.frame = CGRectMake(0, 61.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
    mobileView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
    [mobileView addSubview:mobileView_div];
    
    UILabel *mobileLabel = [[UILabel alloc] init];
    mobileLabel.frame = CGRectMake(20, 10, 100, 22);
    mobileLabel.text = @"MOBILE";
    mobileLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
    mobileLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
    [mobileView addSubview:mobileLabel];
    
    UIButton *mobileView_btn = [[UIButton alloc] init];
    mobileView_btn.tag = 2;
    mobileView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-65, 62);
    [mobileView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [mobileView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
    //[mobileView_btn addTarget:self action:@selector(phonefunc:) forControlEvents:UIControlEventTouchUpInside];
    [mobileView addSubview:mobileView_btn];
    
    landphoneView = [[UIView alloc] init];
    
    if ([dict_profile objectForKey:@"phone_num"] != false && ![[dict_profile objectForKey:@"phone_num"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"phone_num"] length] > 1 && ![[dict_profile objectForKey:@"phone_num"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"phone_num"] isEqualToString:@""]) {
        
            landphoneView.frame = CGRectMake(0, mobileView.frame.origin.y+mobileView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 62);
            landphoneView.backgroundColor = [UIColor clearColor];
            [personalview addSubview:landphoneView];
        
            UILabel *landphoneView_div = [[UILabel alloc] init];
            landphoneView_div.frame = CGRectMake(0, 61.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
            landphoneView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
            [landphoneView addSubview:landphoneView_div];
        
                UILabel *landLabel = [[UILabel alloc] init];
                landLabel.frame = CGRectMake(20, 10, 100, 22);
                landLabel.text = @"HOME";
                landLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
                landLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
                [landphoneView addSubview:landLabel];
        
        
                //[landphonelb addTarget:self action:@selector(landphoneFun:) forControlEvents:UIControlEventTouchUpInside];
        
                land_num = [[UILabel alloc] init];
                land_num.frame = CGRectMake(20, 32, 180, 22);
                land_num.text = [NSString stringWithFormat:@"%@ %@",[dict_profile objectForKey:@"phone_pre"],[dict_profile objectForKey:@"phone_num"]];
                land_num.textColor = [UIColor blackColor];
                land_num.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
                [landphoneView addSubview:land_num];
        
            UIButton *landView_btn = [[UIButton alloc] init];
            landView_btn.tag = 1;
            landView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 62);
            [landView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [landView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
            [landView_btn addTarget:self action:@selector(phonefunc:) forControlEvents:UIControlEventTouchUpInside];
            [landphoneView addSubview:landView_btn];

    }
    else
    {
        landphoneView.frame = CGRectMake(0, mobileView.frame.origin.y+mobileView.frame.size.height, 0,0);
    }
 
    
    emailView = [[UIView alloc] init];
    emailView.frame = CGRectMake(0, landphoneView.frame.origin.y+landphoneView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 62);
    emailView.backgroundColor = [UIColor clearColor];
    [personalview addSubview:emailView];
    
    UILabel *emailView_div = [[UILabel alloc] init];
    emailView_div.frame = CGRectMake(0, 61.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
    emailView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
    [emailView addSubview:emailView_div];
    
    UILabel *emailLabel = [[UILabel alloc] init];
    emailLabel.frame = CGRectMake(20, 10, 100, 22);
    emailLabel.text = @"EMAIL";
    emailLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
    emailLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
    [emailView addSubview:emailLabel];
    
    emailId = [[UILabel alloc] init];
    emailId.frame = CGRectMake(20, 32, 270, 22);
    //emailId.text = mail;
    emailId.textColor = [UIColor blackColor];
    emailId.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    [emailView addSubview:emailId];
    
    UIButton *emailView_btn = [[UIButton alloc] init];
    emailView_btn.tag = 2;
    emailView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 62);
    [emailView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [emailView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
    [emailView_btn addTarget:self action:@selector(mailFun:) forControlEvents:UIControlEventTouchUpInside];
    [emailView addSubview:emailView_btn];
    
    //Checking for User Interaction
    
    if ([other isEqualToString:@"yes"]) {
        
        website_lb.userInteractionEnabled = YES;
    }else{
        
    }
    
    /////////////////////////////////////////////// WEBSITE /////////////////////////////////////////
    
    websiteView = [[UIView alloc] init];
    
    if ([dict_profile objectForKey:@"website"] != false && ![[dict_profile objectForKey:@"website"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"website"] length] > 1 && ![[dict_profile objectForKey:@"website"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"website"] isEqualToString:@""]) {
        
        websiteView.frame = CGRectMake(0, emailView.frame.origin.y+emailView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 62);
        websiteView.backgroundColor = [UIColor clearColor];
        [personalview addSubview:websiteView];
        
        UILabel *websiteView_div = [[UILabel alloc] init];
        websiteView_div.frame = CGRectMake(0, 61.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
        websiteView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        [websiteView addSubview:websiteView_div];
        
        UILabel *websiteLabel = [[UILabel alloc] init];
        websiteLabel.frame = CGRectMake(20, 10, 100, 22);
        websiteLabel.text = @"WEBSITE";
        websiteLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
        websiteLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
        [websiteView addSubview:websiteLabel];
        
        
        //[landphonelb addTarget:self action:@selector(landphoneFun:) forControlEvents:UIControlEventTouchUpInside];
        
        Website = [[UILabel alloc] init];
        Website.frame = CGRectMake(20, 32, 180, 22);
        Website.text = [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"website"]];
        Website.textColor = [UIColor blackColor];
        Website.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
        [websiteView addSubview:Website];
        
        UIButton *websiteView_btn = [[UIButton alloc] init];
        websiteView_btn.tag = 1;
        websiteView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 62);
        [websiteView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [websiteView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
        [websiteView_btn addTarget:self action:@selector(websiteFun:) forControlEvents:UIControlEventTouchUpInside];
        [websiteView addSubview:websiteView_btn];
        
    }
    else
    {
        websiteView.frame = CGRectMake(0, emailView.frame.origin.y+emailView.frame.size.height, 0,0);
    }
    
    
    
    addressView = [[UIView alloc] init];
    addressView.frame = CGRectMake(0, websiteView.frame.origin.y+websiteView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 106);
    addressView.backgroundColor = [UIColor clearColor];
    [personalview addSubview:addressView];
    
    UILabel *addressView_div = [[UILabel alloc] init];
    addressView_div.frame = CGRectMake(0, 105.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
    addressView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
    [addressView addSubview:addressView_div];
    
    UILabel *addressLabel = [[UILabel alloc] init];
    addressLabel.frame = CGRectMake(20, 10, 100, 22);
    addressLabel.text = @"ADDRESS";
    addressLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
    addressLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
    [addressView addSubview:addressLabel];
    
    
    addressText = [[UITextView alloc] init];
    addressText.frame = CGRectMake(15, 32, [UIScreen mainScreen].bounds.size.width-15, 70);
    addressText.backgroundColor = [UIColor clearColor];
    //    address.text = [NSString stringWithFormat:@"%@ <p> %@ </p> <p>%@</p>",[_Condetails_dict objectForKey:@"street"],[_Condetails_dict objectForKey:@"city"],[_Condetails_dict objectForKey:@"country"]];
    NSString *addressValue = [NSString stringWithFormat:@"%@<br>%@ %@<br>%@",[dict_profile objectForKey:@"street"],[dict_profile objectForKey:@"zipcode"],[dict_profile objectForKey:@"city"],[dict_profile objectForKey:@"country"]];
    
    [addressText setValue:addressValue forKey:@"contentToHTMLString"];
    
    addressText.editable = NO;
    //    address.lineBreakMode = YES;
    //    address.lineBreakMode = NSLineBreakByWordWrapping;
    //    address.numberOfLines = 0;
    addressText.textColor = [UIColor blackColor];
    addressText.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    //[addressView addSubview:addressText];
    
    UIButton *addressView_btn = [[UIButton alloc] init];
    addressView_btn.tag = 2;
    addressView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 106);
    [addressView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [addressView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
    [addressView_btn addTarget:self action:@selector(openmap:) forControlEvents:UIControlEventTouchUpInside];
    [addressView addSubview:addressView_btn];
    
    DebugLog(@"ADDRESS VIEW Y:%f",addressView.frame.origin.y);
    [self performSelectorInBackground:@selector(yesSuccess)
                           withObject:nil];
    [del showTabValues:YES];
    
    //    website_lb.userInteractionEnabled = YES;
    //    UITapGestureRecognizer *websiteTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(websiteFun:)];
    //
    //    [website_lb addGestureRecognizer:websiteTap];
    //
    //    divider_1.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
    //    divider_2.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
    //    divider_3.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
    //    divider_4.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
    //    divider_5.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
    //    [divider_9 setBackgroundColor:[UIColor colorWithRed:(239.0f / 255.0f) green:(239.0f / 255.0f) blue:(239.0f / 255.0f) alpha:1.0f]];
    //
    
    
    //NSString *street,*city,*zipcode,*country, *number;
//    NSString *mobile_num1, *landline_num1, *business_num1;
    phonearr=[[NSMutableArray alloc]init];
    
    //    [boxscroll removeFromSuperview];
    //    [divider_4 removeFromSuperview];
    //    [divider_5 removeFromSuperview];
    //
    
    if (switchprof ==0)
    {
        companyimg.hidden=YES;
        companylb.hidden=YES;
        desigimg.hidden=YES;
        desiglb.hidden=YES;
        
        [busi_desig_divi removeFromSuperview];
        [busi_mob_divi removeFromSuperview];
        
        [activity removeFromSuperview];
        if ([dict_profile objectForKey:@"mobile_num"] != false && ![[dict_profile objectForKey:@"mobile_num"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"mobile_num"] length] > 1 && ![[dict_profile objectForKey:@"mobile_num"] isEqualToString:@"(null)"])
        {
            
            mobile= [NSString stringWithFormat:@"%@%@",[dict_profile objectForKey:@"mobile_pre"],[dict_profile objectForKey:@"mobile_num"]];
            mobileArray = [[NSMutableArray alloc]init];
            [mobileArray addObject:mobile];
            
            mobileView = [[UIView alloc] init];
            mobileView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 62);
            mobileView.backgroundColor = [UIColor clearColor];
            [personalview addSubview:mobileView];
            
            UILabel *mobileView_div = [[UILabel alloc] init];
            mobileView_div.frame = CGRectMake(0, 61.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
            mobileView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
            [mobileView addSubview:mobileView_div];
            
            UILabel *mobileLabel = [[UILabel alloc] init];
            mobileLabel.frame = CGRectMake(20, 10, 100, 22);
            mobileLabel.text = @"MOBILE";
            mobileLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
            mobileLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
            [mobileView addSubview:mobileLabel];
            
            
            mobile_num = [[UILabel alloc] init];
            mobile_num.frame = CGRectMake(20, 32, 150, 22);
            mobile_num.text = [NSString stringWithFormat:@"%@ %@",[dict_profile objectForKey:@"mobile_pre"],[dict_profile objectForKey:@"mobile_num"]];
            mobile_num.textColor = [UIColor blackColor];
            mobile_num.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
            [mobileView addSubview:mobile_num];
            //[phonearr addObject:mobile_num.text];
            [phonearr addObject:[NSString stringWithFormat:@"%@ %@",[dict_profile objectForKey:@"phone_pre"],[dict_profile objectForKey:@"phone_num"]]];
            
            UIImageView *whatsapp_img = [[UIImageView alloc] init];
            whatsapp_img.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-50, 20, 22, 22);
            whatsapp_img.image = [UIImage imageNamed:@"Whatsapp"];
            [mobileView addSubview:whatsapp_img];
            
            UIButton *whatsapp_btn = [[UIButton alloc] init];
            whatsapp_btn.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-65, 10, 52, 42);
            [whatsapp_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
            [whatsapp_btn setBackgroundColor:[UIColor clearColor]];
            [whatsapp_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
            [whatsapp_btn addTarget:self action:@selector(whatsapp_func) forControlEvents:UIControlEventTouchUpInside];
            [mobileView addSubview:whatsapp_btn];
            
            UIButton *mobileView_btn = [[UIButton alloc] init];
            mobileView_btn.tag = 2;
            mobileView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-65, 62);
            [mobileView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
            [mobileView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
            [mobileView_btn addTarget:self action:@selector(phonefunc:) forControlEvents:UIControlEventTouchUpInside];
            [mobileView addSubview:mobileView_btn];
            
        }else{
            
            mobile=@"";
            
        }
        
        landphoneView = [[UIView alloc] init];
        
        if ([dict_profile objectForKey:@"phone_num"] != false && ![[dict_profile objectForKey:@"phone_num"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"phone_num"] length] > 1 && ![[dict_profile objectForKey:@"phone_num"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"phone_num"] isEqualToString:@""])
        {
            landline= [NSString stringWithFormat:@"%@%@",[dict_profile objectForKey:@"phone_pre"],[dict_profile objectForKey:@"phone_num"]];
            
            
                        landphoneView.frame = CGRectMake(0, mobileView.frame.origin.y+mobileView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 62);
                        landphoneView.backgroundColor = [UIColor clearColor];
                        [personalview addSubview:landphoneView];
            
                        UILabel *landphoneView_div = [[UILabel alloc] init];
                        landphoneView_div.frame = CGRectMake(0, 61.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
                        landphoneView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
                        [landphoneView addSubview:landphoneView_div];
            
                        UILabel *landLabel = [[UILabel alloc] init];
                        landLabel.frame = CGRectMake(20, 10, 100, 22);
                        landLabel.text = @"HOME";
                        landLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
                        landLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
                        [landphoneView addSubview:landLabel];
            
            
                        //[landphonelb addTarget:self action:@selector(landphoneFun:) forControlEvents:UIControlEventTouchUpInside];
            
                        land_num = [[UILabel alloc] init];
                        land_num.frame = CGRectMake(20, 32, 150, 22);
                        land_num.text = [NSString stringWithFormat:@"%@ %@",[dict_profile objectForKey:@"phone_pre"],[dict_profile objectForKey:@"phone_num"]];
                        land_num.textColor = [UIColor blackColor];
                        land_num.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
                        [landphoneView addSubview:land_num];
            
            UIButton *landView_btn = [[UIButton alloc] init];
            landView_btn.tag = 1;
            landView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 62);
            [landView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
            [landView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
            [landView_btn addTarget:self action:@selector(phonefunc:) forControlEvents:UIControlEventTouchUpInside];
            [landphoneView addSubview:landView_btn];
            
            
            
        }else{
            
            landphoneView.frame = CGRectMake(0, mobileView.frame.origin.y+mobileView.frame.size.height, 0, 0);
            
        }
        
        
        
        if (![mobile isEqualToString:@""] && ![landline isEqualToString:@""]) {
            
            
        }
        
        
        else if (![landline isEqualToString:@""]) {
            
        }
        
        else {
            
        }
        
        if (mobile) {
            numberstring=mobile;
            [phonearr addObject:mobile];
        }
        else if (landline)
        {
            numberstring=landline;
            [phonearr addObject:landline];
        }
        
        
        if ([dict_profile objectForKey:@"email"] != false && ![[dict_profile objectForKey:@"email"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"email"] length] > 1 && ![[dict_profile objectForKey:@"email"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"email"] isEqualToString:@""])
        {
            mail= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"email"]];
            
            
            emailView = [[UIView alloc] init];
            emailView.frame = CGRectMake(0, landphoneView.frame.origin.y+landphoneView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 62);
            emailView.backgroundColor = [UIColor clearColor];
            [personalview addSubview:emailView];
            
            UILabel *emailView_div = [[UILabel alloc] init];
            emailView_div.frame = CGRectMake(0, 61.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
            emailView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
            [emailView addSubview:emailView_div];
            
            UILabel *emailLabel = [[UILabel alloc] init];
            emailLabel.frame = CGRectMake(20, 10, 100, 22);
            emailLabel.text = @"EMAIL";
            emailLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
            emailLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
            [emailView addSubview:emailLabel];
            
            
            emailId = [[UILabel alloc] init];
            emailId.frame = CGRectMake(20, 32, 270, 22);
            emailId.text = mail;
            emailId.textColor = [UIColor blackColor];
            emailId.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
            [emailView addSubview:emailId];
            
            UIButton *emailView_btn = [[UIButton alloc] init];
            emailView_btn.tag = 2;
            emailView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 62);
            [emailView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
            [emailView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
            [emailView_btn addTarget:self action:@selector(mailFun:) forControlEvents:UIControlEventTouchUpInside];
            [emailView addSubview:emailView_btn];
            
        }
        
        else{
            
            mail=@"";
            
        }
        
        
        
        if (![mail isEqualToString:@""]){  //|| ![website isEqualToString:@""]) {
            
            DebugLog(@"mail divider: %@",NSStringFromCGRect(divider_2.frame));
        }
        
        else {
            
        }
        
        /////////////////////////////////////////////// WEBSITE /////////////////////////////////////////
        
        websiteView = [[UIView alloc] init];
        
        if ([dict_profile objectForKey:@"website"] != false && ![[dict_profile objectForKey:@"website"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"website"] length] > 1 && ![[dict_profile objectForKey:@"website"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"website"] isEqualToString:@""]) {
            
            websiteView.frame = CGRectMake(0, emailView.frame.origin.y+emailView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 62);
            websiteView.backgroundColor = [UIColor clearColor];
            [personalview addSubview:websiteView];
            
            UILabel *websiteView_div = [[UILabel alloc] init];
            websiteView_div.frame = CGRectMake(0, 61.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
            websiteView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
            [websiteView addSubview:websiteView_div];
            
            UILabel *websiteLabel = [[UILabel alloc] init];
            websiteLabel.frame = CGRectMake(20, 10, 100, 22);
            websiteLabel.text = @"WEBSITE";
            websiteLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
            websiteLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
            [websiteView addSubview:websiteLabel];
            
            
            //[landphonelb addTarget:self action:@selector(landphoneFun:) forControlEvents:UIControlEventTouchUpInside];
            
            Website = [[UILabel alloc] init];
            Website.frame = CGRectMake(20, 32, 180, 22);
            Website.text = [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"website"]];
            Website.textColor = [UIColor blackColor];
            Website.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
            [websiteView addSubview:Website];
            
            UIButton *websiteView_btn = [[UIButton alloc] init];
            websiteView_btn.tag = 1;
            websiteView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 62);
            [websiteView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
            [websiteView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
            [websiteView_btn addTarget:self action:@selector(websiteFun:) forControlEvents:UIControlEventTouchUpInside];
            [websiteView addSubview:websiteView_btn];
            
        }
        else
        {
            websiteView.frame = CGRectMake(0, emailView.frame.origin.y+emailView.frame.size.height, 0,0);
        }
        
        
        
        
        
        //======================================PERSONAL ADDRESS =============================//
        
        if ([dict_profile objectForKey:@"street"] != false && ![[dict_profile objectForKey:@"street"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"street"] length]!=0)
        {
            street= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"street"]];
            x=x+27;
        }
        else
            
            street=@"";
        
        if ([dict_profile objectForKey:@"housenumber"] != false && ![[dict_profile objectForKey:@"housenumber"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"housenumber"] length]!=0 && ![[dict_profile objectForKey:@"housenumber"] isEqualToString:@""])
        {
            number= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"housenumber"]];
            if (!street)
                x=x+27;
        }
        else
            number= @"";
        
        if ([dict_profile objectForKey:@"zipcode"] != false && ![[dict_profile objectForKey:@"zipcode"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"zipcode"] length]!=0 && ![[dict_profile objectForKey:@"zipcode"] isEqualToString:@""])
        {
            zipcode= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"zipcode"]];
            x=x+27;
        }
        
        else
            zipcode=@"";
        
        
        if ([dict_profile objectForKey:@"city"] != false && ![[dict_profile objectForKey:@"city"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"city"] length]!=0 && ![[dict_profile objectForKey:@"city"] isEqualToString:@""])
        {
            city= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"city"]];
            if (!zipcode)
                x=x+27;
        }
        else
            
            city=@"";
        
        
        
        
        
        if ([dict_profile objectForKey:@"country"] != false && ![[dict_profile objectForKey:@"country"] isEqualToString:@"(null)"]&& [[dict_profile objectForKey:@"country"] length]!=0 && ![[dict_profile objectForKey:@"country"] isEqualToString:@""])
        {
            country= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"country"]];
            x=x+27;
        }
        else
            country=@"";
        
        
        
        if ( ![street isEqualToString: @""] || ![number isEqualToString: @""] || ![city isEqualToString: @""] || ![zipcode isEqualToString: @""] || ![country isEqualToString: @""])
        {
            [self setAddress];
            
            
            addressView.frame = CGRectMake(0, websiteView.frame.origin.y+websiteView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 106);
            addressView.backgroundColor = [UIColor clearColor];
            [personalview addSubview:addressView];
            
            UILabel *addressView_div = [[UILabel alloc] init];
            addressView_div.frame = CGRectMake(0, 105.5f, [UIScreen mainScreen].bounds.size.width, 0.5f);
            addressView_div.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
            [addressView addSubview:addressView_div];
            
            UILabel *addressLabel = [[UILabel alloc] init];
            addressLabel.frame = CGRectMake(20, 10, 100, 22);
            addressLabel.text = @"ADDRESS";
            addressLabel.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
            addressLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
            [addressView addSubview:addressLabel];
            
            
            addressText = [[UITextView alloc] init];
            addressText.frame = CGRectMake(15, 32, [UIScreen mainScreen].bounds.size.width-15, 70);
            addressText.backgroundColor = [UIColor clearColor];
            //    address.text = [NSString stringWithFormat:@"%@ <p> %@ </p> <p>%@</p>",[_Condetails_dict objectForKey:@"street"],[_Condetails_dict objectForKey:@"city"],[_Condetails_dict objectForKey:@"country"]];
            NSString *addressValue = [NSString stringWithFormat:@"%@<br>%@ %@<br>%@",[dict_profile objectForKey:@"street"],[dict_profile objectForKey:@"zipcode"],[dict_profile objectForKey:@"city"],[dict_profile objectForKey:@"country"]];
            
            [addressText setValue:addressValue forKey:@"contentToHTMLString"];
            
            addressText.editable = NO;
            //    address.lineBreakMode = YES;
            //    address.lineBreakMode = NSLineBreakByWordWrapping;
            //    address.numberOfLines = 0;
            addressText.textColor = [UIColor blackColor];
            addressText.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
            [addressView addSubview:addressText];
            DebugLog(@"ADDRESS VIEW Y:%f",addressView.frame.origin.y);
            UIButton *addressView_btn = [[UIButton alloc] init];
            addressView_btn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 106);
            [addressView_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
            [addressView_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
            [addressView_btn addTarget:self action:@selector(openmap:) forControlEvents:UIControlEventTouchUpInside];
            [addressView addSubview:addressView_btn];
            
            
        }else{
            
        }
        
        counter = 0;
        
        
        
        ///////////////////////////////////////Social VIEW ////////////////////////////////////////////
        UIImageView *fb_img, *google_img, *link_img, *inst_img, *pin_img, *skype_img, *vimeo_img, *twitter_img, *soundcloud_img, *youtube_img;
        fb_img = [[UIImageView alloc] init];
        google_img = [[UIImageView alloc] init];
        link_img = [[UIImageView alloc] init];
        inst_img = [[UIImageView alloc] init];
        pin_img = [[UIImageView alloc] init];
        skype_img = [[UIImageView alloc] init];
        vimeo_img = [[UIImageView alloc] init];
        twitter_img = [[UIImageView alloc] init];
        soundcloud_img = [[UIImageView alloc] init];
        youtube_img = [[UIImageView alloc] init];
        int fb, google, inst, link, pint, sky, sound, twt, vim, you,count=0;
        //float upperView_y=18, lowerView_y=68;
        
        socialImage = [[NSMutableArray alloc] init];
        socialView.frame = CGRectMake(0,addressView.frame.origin.y+addressView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 118);
        socialView.backgroundColor = [UIColor clearColor];
        [personalview addSubview:socialView];
        
        
        if ([dict_profile objectForKey:@"s_facebook"] != false && ![[dict_profile objectForKey:@"s_facebook"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"s_facebook"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_facebook"] isEqualToString:@""]) {
            
            fb = 1;
            [socialImage addObject:@"Facebook"];
            
            count++;
        }
        
        if ([dict_profile objectForKey:@"s_google"] != false && ![[dict_profile objectForKey:@"s_google"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"s_google"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_google"] isEqualToString:@""]) {
            
            google = 1;
            [socialImage addObject:@"Google+"];
            count++;
            
        }
        if ([dict_profile objectForKey:@"s_linkedin"] != false && ![[dict_profile objectForKey:@"s_linkedin"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"s_linkedin"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_linkedin"] isEqualToString:@""]) {
            
            link = 1;
            [socialImage addObject:@"Linkedin"];
            count++;
        }
        if ([dict_profile objectForKey:@"s_instagram"] != false && ![[dict_profile objectForKey:@"s_instagram"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"s_facebook"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_instagram"] isEqualToString:@""]) {
            
            inst = 1;
            [socialImage addObject:@"Instagram"];
            count++;
        }
        if ([dict_profile objectForKey:@"s_pinterest"] != false && ![[dict_profile objectForKey:@"s_pinterest"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"s_pinterest"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_pinterest"] isEqualToString:@""]) {
            
            pint = 1;
            [socialImage addObject:@"Pinterest"];
            count++;
        }
        if ([dict_profile objectForKey:@"s_skype"] != false && ![[dict_profile objectForKey:@"s_skype"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"s_skype"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_skype"] isEqualToString:@""]) {
            
            sky = 1;
            [socialImage addObject:@"Skype"];
            count++;
        }
        if ([dict_profile objectForKey:@"s_vimeo"] != false && ![[dict_profile objectForKey:@"s_vimeo"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"s_vimeo"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_vimeo"] isEqualToString:@""]) {
            
            vim = 1;
            [socialImage addObject:@"Vimeo"];
            count++;
        }
        
        if ([dict_profile objectForKey:@"s_twitter"] != false && ![[dict_profile objectForKey:@"s_twitter"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"s_twitter"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_twitter"] isEqualToString:@""]) {
            
            twt = 1;
            [socialImage addObject:@"Twitter"];
            count++;
        }
        
        if ([dict_profile objectForKey:@"s_soundcloud"] != false && ![[dict_profile objectForKey:@"s_soundcloud"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"s_soundcloud"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_soundcloud"] isEqualToString:@""]) {
            
            sound = 1;
            [socialImage addObject:@"Soundcloud"];
            
            count++;
        }
        if ([dict_profile objectForKey:@"s_youtube"] != false && ![[dict_profile objectForKey:@"s_youtube"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"s_youtube"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_youtube"] isEqualToString:@""]) {
            
            you = 1;
            [socialImage addObject:@"YouTube"];
            count++;
        }
        
        float x_axis = 18*(float)([UIScreen mainScreen].bounds.size.width/320), y_axis;
        for (int i=0; i<[socialImage count]; i++) {
            
            if (i<=5) {
                y_axis = 18;
            }
            else
            {
                y_axis = 68;
            }
            DebugLog(@"IMAGE:%@",[socialImage objectAtIndex:i]);
            UIImageView *social_Imgview = [[UIImageView alloc] init];
            //social_Imgview.backgroundColor = [UIColor greenColor];
            social_Imgview.frame = CGRectMake(x_axis, y_axis, 32, 32);
            social_Imgview.image = [UIImage imageNamed:[socialImage objectAtIndex:i]];
            [socialView addSubview:social_Imgview];
            
            UIButton *social_btn = [[UIButton alloc] init];
            social_btn.frame = CGRectMake(social_Imgview.frame.origin.x-5, social_Imgview.frame.origin.y-5, social_Imgview.frame.size.width+10, social_Imgview.frame.size.height+10);
            social_btn.tag = i;
            [social_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
            [social_btn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
            [social_btn addTarget:self action:@selector(socialFunc:) forControlEvents:UIControlEventTouchUpInside];
            [socialView addSubview:social_btn];
            if (i==5) {
                
                x_axis = 18*(float)([UIScreen mainScreen].bounds.size.width/320);
            }
            else{
                x_axis = social_Imgview.frame.origin.x + social_Imgview.frame.size.width+18*(float)([UIScreen mainScreen].bounds.size.width/320);
            }
        }
        
        
        DebugLog(@"SOCIAL MEDIA:%@",socialImage);
 
        
    }
    
    else            //BUSINESS STARTS HERE======================================>
    {
        
        //        companyimg.hidden=NO;
        //        companylb.hidden=NO;
        //        desigimg.hidden=NO;
        //        desiglb.hidden=NO;
        //
        //        [busi_desig_divi removeFromSuperview];
        
        
        //        counter= 0 ;
    }
    
    website_lb.text= website;
    if (switchprof ==0){
        
        [self setAddress];
        
        
    }
    
    else{
        [street_number_lbl removeFromSuperview];
        [zipcode_lbl removeFromSuperview];
        [city_lbl removeFromSuperview];
        [country_lbl removeFromSuperview];
        
    }
    
    
}

-(void)socialFunc:(UIButton *)sender
{
    if ([[socialImage objectAtIndex:sender.tag] isEqualToString:@"Facebook"]) {
        [self facebookfunc];
    }
    if ([[socialImage objectAtIndex:sender.tag] isEqualToString:@"Google+"]) {
        [self gplusfunc];
    }
    if ([[socialImage objectAtIndex:sender.tag] isEqualToString:@"Linkedin"]) {
        [self linkedinfunc];
    }
    if ([[socialImage objectAtIndex:sender.tag] isEqualToString:@"Instagram"]) {
        [self instagramfunc];
    }
    if ([[socialImage objectAtIndex:sender.tag] isEqualToString:@"Pinterest"]) {
        [self pinterestfunc];
    }
    if ([[socialImage objectAtIndex:sender.tag] isEqualToString:@"Skype"]) {
        [self skypefunc];
    }
    if ([[socialImage objectAtIndex:sender.tag] isEqualToString:@"Vimeo"]) {
        [self vimeofunc];
    }
    if ([[socialImage objectAtIndex:sender.tag] isEqualToString:@"Twitter"]) {
        //[self twt];
    }
    if ([[socialImage objectAtIndex:sender.tag] isEqualToString:@"Soundcloud"]) {
        [self soundcloudfunc];
    }
    if ([[socialImage objectAtIndex:sender.tag] isEqualToString:@"YouTube"]) {
        [self youtubefunc];
    }
}

-(void)twitterfunc
{
    NSURL *urlApp = [NSURL URLWithString: [NSString stringWithFormat:@"twitter://user?screen_name=%@", [dict_profile objectForKey:@"s_twitter"]]];
    
    if ([[UIApplication sharedApplication] canOpenURL:urlApp]) {
        
        [[UIApplication sharedApplication] openURL:urlApp];
    } else {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://twitter.com/%@",[dict_profile objectForKey:@"s_twitter"]]]];
        DebugLog(@"TWIT URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"http://twitter.com/%@",[dict_profile objectForKey:@"s_twitter"]]]);
    }
}


-(void)pinterestfunc
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"pinterest://user/%@",[dict_profile objectForKey:@"s_pinterest"]]]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"pinterest://user/%@",[dict_profile objectForKey:@"s_pinterest"]]]];
    } else {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://pinterest.com"]];
        DebugLog(@"PIN URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"http://pinterest.com/%@",[dict_profile objectForKey:@"s_pinterest"]]]);
    }
}



-(void)facebookfunc
{
    
    NSString *fb_app_str = [[NSString alloc]init];
    
    fb_app_str = [dict_profile objectForKey:@"s_facebook"];
    
//    NSString *string1=[NSString
//                       stringWithFormat:@"http://graph.facebook.com/%@",[dict_profile
//                                                                         objectForKey:@"s_facebook"]];
//    
//    DebugLog(@"facebook graph %@",string1);
//    
//    NSError *localErr;
//    
//    
//    
//    NSData *result =  [NSData dataWithContentsOfURL:[NSURL
//                                                     URLWithString:string1] options:NSDataReadingUncached error:&localErr];
//    
//    
//    
//    if (result != nil)
//        
//    {
//        
//        NSDictionary *graphjson =[NSJSONSerialization
//                                  JSONObjectWithData:result options:0 error:&localErr];
//        
//        if (![graphjson isKindOfClass:[NSNull class]])
//            
//        {
//            
//            if ([[graphjson allKeys]containsObject:@"id"])
//                
//            {
//                
//                fb_app_str = [graphjson objectForKey:@"id"];
//                
//            }
//            
//        }
//        
//    }
//    
    
    
    NSURL *url = [NSURL URLWithString:[NSString
                                       stringWithFormat:@"fb://profile/%@",fb_app_str]];  //[dict_profile
    //objectForKey:@"s_facebook"]
    
    [[UIApplication sharedApplication] openURL:url];
    
    
    
    if ([[UIApplication sharedApplication] canOpenURL:url]){
        
        [[UIApplication sharedApplication] openURL:url];
        
    }
    
    else {
        
        [[UIApplication sharedApplication] openURL:[NSURL
                                                    URLWithString:[NSString
                                                                   stringWithFormat:@"http://facebook.com/%@",[dict_profile
                                                                                                               objectForKey:@"s_facebook"]]]];  //100001925673906
        
        DebugLog(@"http://facebook.com/%@",[dict_profile
                                            objectForKey:@"s_facebook"]);
        
    }
    
}



-(void)gplusfunc
{
    NSString *string = [dict_profile objectForKey:@"s_google"];
    NSArray *stringArray = [string componentsSeparatedByString: @"com/"];
    NSString *gplusstr = [stringArray objectAtIndex:1];
    NSURL *GooglePlus = [NSURL URLWithString:[NSString stringWithFormat:@"gplus://plus.google.com/%@",gplusstr]];
    DebugLog(@"GOO+ URL=============>%@",GooglePlus);
    
    if ([[UIApplication sharedApplication] canOpenURL:GooglePlus])  //[NSURL URLWithString:@"gplus://plus.google.com/u/0/105819873801211194735/"]
        [[UIApplication sharedApplication] openURL:GooglePlus];
    else
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://plus.google.com/%@",gplusstr]]];
}


-(void)youtubefunc
{
    NSString* social   = [NSString stringWithFormat:@"http://www.youtube.com/user/%@",[dict_profile objectForKey:@"s_youtube"]];
    NSString* socialApp = [NSString stringWithFormat:@"youtube://user/%@",[dict_profile objectForKey:@"s_youtube"]];
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:socialApp]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:socialApp]];
        
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:social]];
    }
}



-(void)instagramfunc
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"instagram://user?username=%@",[dict_profile objectForKey:@"s_instagram"]]]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"instagram://user?username=%@",[dict_profile objectForKey:@"s_instagram"]]]];
        
        DebugLog(@"INSTA URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"instagram://user?username=%@",[dict_profile objectForKey:@"s_instagram"]]]);
    } else {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.instagram.com"]];
        DebugLog(@"INSTA URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"https://www.instagram.com/%@",[dict_profile objectForKey:@"s_instagram"]]]);
    }
}


-(void)linkedinfunc
{
    NSString *linkedinstra = [dict_profile objectForKey:@"s_linkedin"];
    NSArray *linkedinarr= [linkedinstra componentsSeparatedByString:@"="];
    NSString *linkedinstr = [linkedinarr objectAtIndex:1];
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"linkedin://profile/%@",linkedinstr]]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"linkedin://profile/%@",linkedinstr]]];
        
        DebugLog(@"LINKED URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"linkedin://profile/%@",linkedinstr]]);
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.linkedin.com"]];
        DebugLog(@"LINKED URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_linkedin"]]]);
    }
}


-(void)skypefunc
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"skype:%@",[dict_profile objectForKey:@"s_skype"]]]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"skype:%@",[dict_profile objectForKey:@"s_skype"]]]];
        
        DebugLog(@"SKYPE URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"skype:%@",[dict_profile objectForKey:@"s_skype"]]]);
    } else {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.skype.com/%@",[dict_profile objectForKey:@"s_skype"]]]];
        
        DebugLog(@"SKYPE URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"https://www.skype.com/%@",[dict_profile objectForKey:@"s_skype"]]]);
    }
}
-(void)skypeAction{
    
    if ([dict_profile objectForKey:@"s_skype"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_skype"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_skype"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_skype"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_skype"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_skype"] isEqualToString:@"(null)"]) {
        
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"skype:%@",[dict_profile objectForKey:@"s_skype"]]]]) {
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"skype:%@",[dict_profile objectForKey:@"s_skype"]]]];
            
            DebugLog(@"SKYPE URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"skype:%@",[dict_profile objectForKey:@"s_skype"]]]);
        } else {
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.skype.com/%@",[dict_profile objectForKey:@"s_skype"]]]];
            
            DebugLog(@"SKYPE URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"https://www.skype.com/%@",[dict_profile objectForKey:@"s_skype"]]]);
        }
        
    }else{
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.skype.com"]]];
        
    }
    
    
    
}


-(void)soundcloudfunc
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.soundcloud.com/%@",[dict_profile objectForKey:@"s_soundcloud"]]]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.soundcloud.com/%@",[dict_profile objectForKey:@"s_soundcloud"]]]];
        
        DebugLog(@"SOUNDC URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"https://www.soundcloud.com/%@",[dict_profile objectForKey:@"s_soundcloud"]]]);
        
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.soundcloud.com"]];
        
        DebugLog(@"SOUNDC URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"https://www.soundcloud.com/%@",[dict_profile objectForKey:@"s_soundcloud"]]]);
    }
}


-(void)vimeofunc
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.vimeo.com/%@",[dict_profile objectForKey:@"s_vimeo"]]]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.vimeo.com/%@",[dict_profile objectForKey:@"s_vimeo"]]]];
        
        DebugLog(@"VIMEO URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"https://www.vimeo.com/%@",[dict_profile objectForKey:@"s_vimeo"]]]);
        
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.vimeo.com"]];
        
        DebugLog(@"VIMEO URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"https://www.vimeo.com/%@",[dict_profile objectForKey:@"s_vimeo"]]]);
    }
}


-(void)togglefunc: (UIButton *)sender
{
    DebugLog(@"TOGGLE FUNC CaLl HoChChE");
    
    if (switchprof == 0)
    {
        [UIView animateWithDuration:0.3 animations:^{
            
            //coverView.backgroundColor = [UIColor blackColor];
            
            UIColor *darkOp =
            [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
            UIColor *lightOp =
            [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
            
            // Create the gradient
            CAGradientLayer *gradient = [CAGradientLayer layer];
            
            // Set colors
            gradient.colors = [NSArray arrayWithObjects:
                               (id)lightOp.CGColor,
                               (id)darkOp.CGColor,
                               nil];
            
            // Set bounds
            gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
            
            // Add the gradient to the view
            [coverView.layer insertSublayer:gradient atIndex:0];
            
            
            
            [blackView setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0)];
            
            [businessImg setFrame:CGRectMake(15, 0, 61.0f/2.0f, 0)];
            
            [togglebt setFrame:CGRectMake(50, 0, 180, 0)];
            
            [lineDivMenu setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0)];
            
            [removeImg setFrame:CGRectMake(15, 0, 61.0f/2.0f, 0)];
            
            removeconbt.frame = CGRectMake(36, 0, 180, 0);
            
        }
         
         //        }];
         
                         completion:^(BOOL finished){
                             
                             rightmenuOpen = YES;
                             
                         }];
        
        switchprof = 1;
        [self business];
    }
    else
    {
        [UIView animateWithDuration:0.3 animations:^{
            
            //coverView.backgroundColor = [UIColor blackColor];
            
            UIColor *darkOp =
            [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
            UIColor *lightOp =
            [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
            
            // Create the gradient
            CAGradientLayer *gradient = [CAGradientLayer layer];
            
            // Set colors
            gradient.colors = [NSArray arrayWithObjects:
                               (id)lightOp.CGColor,
                               (id)darkOp.CGColor,
                               nil];
            
            // Set bounds
            gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
            
            // Add the gradient to the view
            [coverView.layer insertSublayer:gradient atIndex:0];
            
            
            
            [blackView setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0)];
            
            [businessImg setFrame:CGRectMake(15, 0, 61.0f/2.0f, 0)];
            
            [togglebt setFrame:CGRectMake(50, 0, 180, 0)];
            
            [lineDivMenu setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0)];
            
            [removeImg setFrame:CGRectMake(15, 0, 61.0f/2.0f, 0)];
            
            removeconbt.frame = CGRectMake(36, 0, 180, 0);
            
        }
                         completion:^(BOOL finished){
                             
                             rightmenuOpen = YES;
                             
                         }];
        
        switchprof = 0;
        [self loadViews];
    }
}


-(void)mobileFun:(UITapGestureRecognizer *)sender {
   // email_btn.alpha=0.0f;
    email_img.alpha=1.0f;
    
    //    mobileback.backgroundColor = [UIColor colorWithRed:(238.0f/255.0f) green:(238.0f/255.0f) blue:(238.0f/255.0f) alpha:1.0f];
    
    UIActionSheet *phoneAction = [[UIActionSheet alloc]initWithTitle:Nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles:@"Call",@"SMS", nil];
    
    phoneAction.tag = 100;
    [phoneAction showInView:mainview];
}

-(void)landphoneFun:(UITapGestureRecognizer *)sender {
    
    landlineback.backgroundColor = [UIColor colorWithRed:(238.0f/255.0f) green:(238.0f/255.0f) blue:(238.0f/255.0f) alpha:1.0f];
    
    UIActionSheet *landphoneAction = [[UIActionSheet alloc]initWithTitle:Nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles:@"Call", nil];
    landphoneAction.tag = 200;
    [landphoneAction showInView:mainview];
}


-(void)mailFun:(UIButton *)sender {
//    if (sender.tag == 3) {
//        b_emailView.backgroundColor = [UIColor lightGrayColor];
//        
//    }
//    if (sender.tag == 2) {
//        emailView.backgroundColor = [UIColor lightGrayColor];
//        
//    }
    //email_btn.alpha=0.0f;
    email_img.alpha=1.0f;
    
    if ([mail length]>0) {
        
        mailback.backgroundColor = [UIColor colorWithRed:(238.0f/255.0f) green:(238.0f/255.0f) blue:(238.0f/255.0f) alpha:1.0f];
        
        mailComposer = [[MFMailComposeViewController alloc]init];
        mailComposer.mailComposeDelegate = self;
        NSArray *recipentsArray = [NSArray arrayWithObject:mail];
        [mailComposer setToRecipients:recipentsArray];
        [mailComposer setMessageBody:@"" isHTML:NO];
        [self.navigationController presentViewController:mailComposer animated:YES completion:nil];
    }
    else{
        smsAlert1 = [[UIAlertView alloc] initWithTitle:@"No Email Id found for this user!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        smsAlert1.tag = 6;
        b_emailView.backgroundColor = [UIColor clearColor];
        emailView.backgroundColor = [UIColor clearColor];
        [smsAlert1 show];
        
    }
}

#pragma mark - mail compose delegate

-(void)mailComposeController:(MFMailComposeViewController *)controller

         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (result) {
        DebugLog(@"Result : %d",result);
    }
    if (error) {
        DebugLog(@"Error : %@",error);
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
    mailback.backgroundColor = [UIColor clearColor];
}



- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 100) {
        
        if (buttonIndex == 0) {
            UIDevice *device = [UIDevice currentDevice];
            
            if ([[device model] isEqualToString:@"iPhone"] ) {
                NSString *phoneNumber = [@"telprompt://" stringByAppendingString:[NSString stringWithFormat:@"%@",mobile]];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
                
                //                [[UIApplication sharedApplication] openURL:[NSURL URLWithString: [NSString stringWithFormat:@"tel:%@",[phonesarr objectAtIndex:buttonIndex]]]];
                //                mobileback.backgroundColor = [UIColor clearColor];
            } else {
                
                UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                [notPermitted show];
                //                mobileback.backgroundColor = [UIColor clearColor];
            }
        }
        
        else if(buttonIndex == 1){
            
            UIDevice *device = [UIDevice currentDevice];
            
            if ([[device model] isEqualToString:@"iPhone"] ) {
                
                
                MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
                
                if([MFMessageComposeViewController canSendText])
                {
                    [self presentViewController:controller animated:YES completion:Nil];
                    controller.body = @"";
                    
                    NSString *smsstring= [NSString stringWithFormat:@"%@",[mobileArray objectAtIndex:0]];
                    
                    controller.recipients = [NSArray arrayWithObjects:smsstring, nil];
                    
                    controller.messageComposeDelegate = self;
                    
                    //[self presentModalViewController:controller animated:YES];
                    //                    mobileback.backgroundColor = [UIColor clearColor];
                }
            }
            else
            {
                UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                [notPermitted show];
                //                mobileback.backgroundColor = [UIColor clearColor];
            }
        }else{
            
            //            mobileback.backgroundColor = [UIColor clearColor];
            
        }
    }
    
    else if (actionSheet.tag == 200){
        
        
        
        if (buttonIndex == 0) {
            
            UIDevice *device = [UIDevice currentDevice];
            
            if ([[device model] isEqualToString:@"iPhone"] ) {
                
                NSString *phoneNumber = [@"telprompt://" stringByAppendingString:[NSString stringWithFormat:@"%@",landline]];
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
                
                
                
                //                [[UIApplication sharedApplication] openURL:[NSURL URLWithString: [NSString stringWithFormat:@"tel:%@",[phonesarr objectAtIndex:buttonIndex]]]];
                landlineback.backgroundColor = [UIColor clearColor];
                
            } else {
                
                UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                [notPermitted show];
                landlineback.backgroundColor = [UIColor clearColor];
                
            }
        }else{
            
            landlineback.backgroundColor = [UIColor clearColor];
            
        }
    }else if (actionSheet.tag == 900){
        
        if (buttonIndex == 0) {
            
            DebugLog(@"WhatsApp button clicked");
            [self showPersonViewController];
            
        }else if (buttonIndex == 1){
            if ([phonearr count] == 0)
            {
                [bgcallsms1 setBackgroundImage:[UIImage imageNamed:@"smsend_profile"] forState:UIControlStateNormal];
                
                
                smsAlert1 = [[UIAlertView alloc] initWithTitle:@"No Number found for this user!"
                                                       message:nil
                                                      delegate:self
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                smsAlert1.tag = 6;
                [smsAlert1 show];
            }
            else
            {
                
                [bgcallsms1 setBackgroundImage:[UIImage imageNamed:@"smsend_profile"] forState:UIControlStateNormal];
                
                UIDevice *device = [UIDevice currentDevice];
                if ([[device model] isEqualToString:@"iPhone"]) {
                    
                    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
                    if([MFMessageComposeViewController canSendText])
                    {
                        [self presentViewController:controller animated:YES completion:Nil];
                        controller.body = @"";
                        NSString *smsstring= [NSString stringWithFormat:@"%@",[phonearr objectAtIndex:0]];
                        controller.recipients = [NSArray arrayWithObjects:smsstring, nil];
                        controller.messageComposeDelegate = self;
                        //[self presentModalViewController:controller animated:YES];
                    }
                }
                else
                {
                    smsAlert2=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    smsAlert2.tag = 66;
                    [smsAlert2 show];
                }
            }
            
            
        }
        
    }
    
}


-(void)showPersonViewController
{
    DebugLog(@"profile name field text: %@",prof_name.text);
    // Fetch the address book
    CFErrorRef *errorab = nil;
    
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, errorab);
    
    // Search for the person named "Appleseed" in the address book
    //    CFArrayRef people = ABAddressBookCopyPeopleWithName(addressBook, CFSTR("Appleseed"));
    
    CFArrayRef people = ABAddressBookCopyPeopleWithName(addressBook,
                                                        (__bridge CFStringRef)prof_name.text);
    
    // Display "Appleseed" information if found in the address book
    if ((people != nil) && (CFArrayGetCount(people) > 0))
    {
        
        for (int m = 0; m < (CFArrayGetCount(people)); m++){
            DebugLog(@"count e dichhe: %ld == %@", (CFArrayGetCount(people)),people);
            ABRecordRef person = CFArrayGetValueAtIndex(people, 0);
            
            ABRecordID recordID = ABRecordGetRecordID(person);
            
            DebugLog(@"ab id: %d",recordID);
            DebugLog(@"phone array; %@", phonearr);
            
            ABMultiValueRef phoneNumberProperty = ABRecordCopyValue(person, kABPersonPhoneProperty);
            NSArray *_phoneNumbers = (__bridge NSArray*)ABMultiValueCopyArrayOfAllValues(phoneNumberProperty);
            
            NSMutableArray *mutableph= [_phoneNumbers mutableCopy];
            int pp;
            for (pp =0; pp< [mutableph count]; pp++)
            {
                if ([[mutableph objectAtIndex:pp] isKindOfClass:[NSNull class]] || [mutableph objectAtIndex:pp] == (id)[NSNull null] || [[mutableph objectAtIndex:pp] length] < 3)
                {
                    [mutableph removeObjectAtIndex:pp];
                    pp= pp-1;
                }
            }
            NSMutableSet* set1 = [NSMutableSet setWithArray:mutableph];
            NSMutableSet* set2 = [NSMutableSet setWithArray:phonearr];
            [set1 intersectSet:set2]; //this will give you only the obejcts that are in both sets
            
            NSArray* result = [set1 allObjects];
            DebugLog(@"result pelo: %@",result);
            
            if ([result count] > 0)
            {
                NSString * urlWhats = [NSString stringWithFormat:@"whatsapp://send?abid=%d&text=",recordID];
                NSURL * whatsappURL = [NSURL URLWithString:[urlWhats stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
                    [[UIApplication sharedApplication] openURL: whatsappURL];
                } else {
                    UIAlertView * alert1 = [[UIAlertView alloc] initWithTitle:@"WhatsApp not installed." message:@"Your device has no WhatsApp installed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert1 show];
                }
                break;
            }
        }
    }
    else
    {
        // Show an alert if "Appleseed" is not in Contacts
    }
    //CFRelease(addressBook);
    //CFRelease(people);
}


- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    if (result == MessageComposeResultCancelled) {
        DebugLog(@"Message cancelled");
    } else if (result == MessageComposeResultSent) {
        DebugLog(@"Message sent");
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


+(void)chngpostion
{
    DebugLog(@"Change Position profile page");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Data" object:Nil];
}


-(void)requestreceivedaction
{
    del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    ConNewDetailsViewController *con = [[ConNewDetailsViewController alloc]init];
    con.other=@"yes";
    con.uid=[NSString stringWithFormat:@"%@",del.pushtoprofileid];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}

-(void)acceptedrequestaction
{
    ConRequestViewController *con = [[ConRequestViewController alloc]init];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}



-(void)navtopage: (NSNotification *)notification
{
    DebugLog(@"navtopage");
    NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
    if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"4"])
    {
        ConAccountSettingsViewController *con = [[ConAccountSettingsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"6"])
    {
        ConInviteViewController *con = [[ConInviteViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"5"])
    {
        ConSyncLoaderViewController *con = [[ConSyncLoaderViewController alloc]init];
        //        [self.navigationController pushViewController:con animated:NO];
        
        [self presentViewController:con animated:NO completion:nil];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"2"])
    {
        ConNewDetailsViewController *con = [[ConNewDetailsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"3"])
    {
        ConQROptionsViewController *con = [[ConQROptionsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    
    
    
    //    [leftmenu setFrame:CGRectMake(-280, leftmenu.frame.origin.y, leftmenu.frame.size.width, leftmenu.frame.size.height)];
    
    [mainview setFrame:CGRectMake(0, mainview.frame.origin.y, mainview.frame.size.width, mainview.frame.size.height)];
    
    del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    
    move=1;
    
}



-(void)getData:(NSNotification *)notification {
    
    DebugLog(@"mainview.frame.origin.x = %f",mainview.frame.origin.x);
    
    if(move == 0) {
        
        DebugLog(@"if prof move");
        
        [UIView animateWithDuration:0.25
         
                         animations:^{
                             
                             [mainview setFrame:CGRectMake(280, (mainview.frame.origin.y), mainview.frame.size.width, mainview.frame.size.height)];
                             
                         }
         
                         completion:^(BOOL finished){
                             
                             move=1;
                         }];
        
    } else {
        
        DebugLog(@"else prof move");
        
        [UIView animateWithDuration:0.25
         
                         animations:^{
                             
                             [mainview setFrame:CGRectMake(0, (mainview.frame.origin.y), mainview.frame.size.width, mainview.frame.size.height)];
                         }
         
                         completion:^(BOOL finished){
                             move=0;
                         }];
        
    }
    
}



-(void)viewDidDisappear:(BOOL)animated
{
    move=0;
    personal_array = Nil;
    business_array =Nil;
    social_array = Nil;
    dict_profile = nil;
    dict_profile = nil;
    dict_profile = nil;
    profdict = nil;
    dict_profile = nil;
    [timea invalidate];
    [coverview1 removeFromSuperview];
    [SVProgressHUD dismiss];
    [mainview removeFromSuperview];
    
    b_phoneView.backgroundColor = [UIColor clearColor];
    mobileView.backgroundColor = [UIColor clearColor];
    landphoneView.backgroundColor = [UIColor clearColor];
    
    b_emailView.backgroundColor = [UIColor clearColor];
    emailView.backgroundColor = [UIColor clearColor];
    b_addressView.backgroundColor = [UIColor clearColor];
    addressView.backgroundColor = [UIColor clearColor];
    
    call_btn.userInteractionEnabled = YES;
    sms_btn.userInteractionEnabled = YES;
    email_btn.userInteractionEnabled = YES;
    location_btn.userInteractionEnabled =YES;
    
    self.navigationController.navigationBarHidden=YES;
    [super viewDidDisappear:YES];
    
}


-(void)openmap: (UIButton *)sender
{
    location_img.alpha=1.0f;
    addressback.backgroundColor = [UIColor colorWithRed:(238.0f/255.0f) green:(238.0f/255.0f) blue:(238.0f/255.0f) alpha:1.0f];
    
    if (switchprof == 0)
    {
        latitude=[[dict_profile objectForKey:@"lat"] doubleValue];
        longitude=[[dict_profile objectForKey:@"lng"] doubleValue];
    }
    else
    {
        latitude=[[dict_profile objectForKey:@"b_lat"] doubleValue];
        longitude=[[dict_profile objectForKey:@"b_lng"] doubleValue];
    }
    DebugLog(@"lati=== %f",latitude);
    DebugLog(@"long=== %f",longitude);
    [self generateRoute];
}

- (void)getDirections
{
}

-(void)showRoute:(MKDirectionsResponse *)response1
{
    for (MKRoute *route in response1.routes)
    {
        [map_View addOverlay:route.polyline level:MKOverlayLevelAboveLabels];
        
        for (MKRouteStep *step in route.steps)
        {
            DebugLog(@" here %@", step.instructions);
        }
    }
}


- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id < MKOverlay >)overlay
{
    MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
    renderer.strokeColor = [UIColor blueColor];
    renderer.lineWidth = 4.0;
    return renderer;
}


- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    CLLocation *location = map_View.userLocation.location;
    DebugLog(@"lat current: %f - long current: %f", location.coordinate.latitude, location.coordinate.longitude);
}


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    // Handle any custom annotations.
    
    if ([annotation isKindOfClass:[MKPointAnnotation class]])
    {
        // Try to dequeue an existing pin view first.
        
        MKPinAnnotationView *pinView = (MKPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomPinAnnotationView"];
        //        if (!pinView)
        //        {
        pinView = [[MKPinAnnotationView alloc] init];
        //            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomPinAnnotationView"];
        
        pinView.animatesDrop = YES;
        pinView.canShowCallout = YES;
        pinView.image = [UIImage imageNamed:@"locator1.png"];
        pinView.calloutOffset = CGPointMake(0, 0);
        
        
        UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        
        pinView.rightCalloutAccessoryView = rightButton;
        
        
        UIImageView *iconView = [[UIImageView alloc] init];
        
        iconView.frame = CGRectMake(0.0f, 0.0f,20, 20);
        
        pinView.leftCalloutAccessoryView = iconView;
        
        iconView.backgroundColor=[UIColor whiteColor];
        
        
        if (annotation == mapView.userLocation)
            
        {
            pinView.image= [UIImage imageNamed:@"locatorown.png"];
            
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomPinAnnotationView"];
        }
        
        DebugLog(@"(int)annotation.subtitle===%@",annotation.subtitle);
        
        iconView.image= profilepic;
        
        if (switchprof == 0)
            pinView.image = [UIImage imageNamed:@"locator1.png"];
        else
            pinView.image = [UIImage imageNamed:@"locator2.png"];
        
        //        }
        return pinView;
    }
    return nil;
}





-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    DebugLog(@"annotation selected");
}

- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)aUserLocation {
    
    MKCoordinateRegion region;
    
    MKCoordinateSpan span;
    
    span.latitudeDelta = 0.005;
    
    span.longitudeDelta = 0.005;
    
    CLLocationCoordinate2D location2d;
    
    location2d.latitude =  latitude;       //aUserLocation.coordinate.latitude;
    
    location2d.longitude = longitude;      //aUserLocation.coordinate.longitude;
    
    region.span = span;
    
    region.center = location2d;
    
    googleMapUrlString = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%f,%f&daddr=%f,%f", aUserLocation.location.coordinate.latitude,aUserLocation.location.coordinate.longitude, location2d.latitude, location2d.longitude];
    
    DebugLog(@"url fired map: %@",googleMapUrlString);
    
    //    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapUrlString]];
    appleMapUrlString = [NSString stringWithFormat:@"http://maps.apple.com/maps?saddr=%f,%f&daddr=%f,%f", aUserLocation.location.coordinate.latitude,aUserLocation.location.coordinate.longitude, location2d.latitude, location2d.longitude];
    [self generateRoute];
}

-(void)crossmap
{
    [crossbt removeFromSuperview];
    [mapview removeFromSuperview];
    [tapbgview removeFromSuperview];
    [navigatebt removeFromSuperview];
    [navigatebtapple removeFromSuperview];
}


- (void)generateRoute {
    CLLocationCoordinate2D end = {latitude, longitude};
    
    MKMapItem *destination_mapitem = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:end addressDictionary:nil]];
    
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
    
    request.source = [MKMapItem mapItemForCurrentLocation];
    
    request.destination = destination_mapitem;
    
    [request setTransportType:MKDirectionsTransportTypeAny]; // This can be limited to automobile and walking directions.
    
    [request setRequestsAlternateRoutes:YES];
    
    MKDirections *directions = [[MKDirections alloc] initWithRequest:request];
    
    [directions calculateDirectionsWithCompletionHandler:
     
     ^(MKDirectionsResponse *response1, NSError *error) {
         
         if (error) {
             DebugLog(@"error generate route----  %@",error);
             
         } else {
             [self showRoute:response1];
         }
     }];
    [self navigateinapple];
}


-(void)navigatetoloc
{
    ConNavigateViewController *con = [[ConNavigateViewController alloc]init];
    con.fireurl = googleMapUrlString;
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}


-(void)navigateinapple
{
    CLLocationCoordinate2D endingCoord = CLLocationCoordinate2DMake(latitude, longitude);
    MKPlacemark *endLocation = [[MKPlacemark alloc] initWithCoordinate:endingCoord addressDictionary:nil];
    MKMapItem *endingItem = [[MKMapItem alloc] initWithPlacemark:endLocation];
    NSMutableDictionary *launchOptions = [[NSMutableDictionary alloc] init];
    [launchOptions setObject:MKLaunchOptionsDirectionsModeDriving forKey:MKLaunchOptionsDirectionsModeKey];
    [endingItem openInMapsWithLaunchOptions:launchOptions];
}


-(void)detailpic: (UIGestureRecognizer *)sender
{
    ConPictureProfViewController *con = [[ConPictureProfViewController alloc]init];
    con.profilepic = profilepic;
    //    con.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:con animated:NO completion:nil];
}



-(void)closedetailpic: (UIGestureRecognizer *)sender

{
    
    CATransition *animation = [CATransition animation];
    
    
    
    [animation setType:kCATransitionFade];
    
    
    
    [animation setSubtype:kCATransitionFade];
    
    
    
    animation.duration = 0.7f;
    
    
    
    [detailimg.layer addAnimation:animation forKey:nil];
    
    
    
    if(detailimg.hidden==NO)
        
    {
        
        detailimg.hidden=YES;
        
    }
    
    [tapbgview removeFromSuperview];
    
}





- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data

{
    
    [responseData appendData:data];
    
    
    
}



- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error

{
    
    DebugLog(@"connection failed");
    
}



- (void)connectionDidFinishLoading:(NSURLConnection *)connection

{
    DebugLog(@"connection finished loading");
    
    DebugLog(@"response data - %@", [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
    
    
    NSError *err;
    
    if (responseData != nil)
        
    {
        
        NSDictionary  *json1aa=[NSJSONSerialization JSONObjectWithData:responseData options:0 error:&err];
        
        DebugLog(@"this now has json string url: %@",json1aa);
        
    }
    
    else
        
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                 
                                           message:nil
                 
                                          delegate:self
                 
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        
        alert.tag=7;
        
        //        [alert show];
    }
}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    responseData = [NSMutableData data];
}

-(void)servererror
{
    alert = [[UIAlertView alloc] initWithTitle:@"Error in Profile Server Connection!"
                                       message:nil delegate:self
                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
    [alert show];
}

-(void) reloadDataFromWeb
{
    DebugLog(@"reload profile from web");
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [mainview addSubview:activity];
    activity.center= self.view.center;
    activity.tintColor=[UIColor grayColor];
    //    [activity startAnimating];
    
    //    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    NSString *errornumber;
    
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=profile&id=%@&access_token=%@&device_id=%@&image=true",user_id,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
    
    DebugLog(@"profile url: %@",urlString1);
    
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL11a =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    
    if (signeddataURL11a == nil)
    {
        DebugLog(@"no connnn profile");
        
        // [self performSelectorOnMainThread:@selector(servererror) withObject:nil waitUntilDone:YES];
        [self reloadFromLocalDB];
    }
    //    DebugLog(@"json returns: %@",json1);
    else
    {
        //            [alert show];
        
        
        NSError *error;
        json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL11a //1
                 
                                                options:kNilOptions
                 
                                                  error:&error];
        errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
        
        DebugLog(@"err  %@",errornumber);
        
        
        if (![errornumber isEqualToString:@"0"])
        {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"error"];
            [activity removeFromSuperview];
            NSString *err_str = [json1 objectForKey:@"error"];
            
            alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                     
                                               message:err_str
                     
                                              delegate:self
                     
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            //            [alert show];
        }
        else
        {
            dict_profile = [NSMutableDictionary new];
            
            profdict = [NSMutableDictionary new];
            //
            //            dict_profile = [NSMutableDictionary new];
            
            
            
            profdict = [json1 objectForKey:@"details"];
            
            dict_profile = [profdict objectForKey:@"profile"];
            [activity removeFromSuperview];
            
            
            if ([user_id intValue] == [[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]intValue])
                switchprof=0;
            else
            {
                if ([[dict_profile objectForKey:@"friend"]intValue] == 1)
                {
                    switchprof=0;
                }
                
                else if ([[dict_profile objectForKey:@"business"] intValue]==1)
                {
                    switchprof=1;
                }
                DebugLog(@"fields are: %d , %d",[[dict_profile objectForKey:@"own"]intValue], switchprof);
            }
        }
        
        
    }
    //        dispatch_async(dispatch_get_main_queue(), ^{
    
    if (signeddataURL11a != nil && [errornumber isEqualToString:@"0"] && [dict_profile count] > 0)
    {
        [[DBManager getSharedInstance]insertProfile:profdict :[user_id intValue]];
        
    }
    
}



-(void) reloadFromLocalDB
{
    
    
    NSDictionary *localDBProfile = [[NSDictionary alloc]init];
    
    localDBProfile = [[DBManager getSharedInstance]fetchProfile:[user_id intValue]];
    
    DebugLog(@"this gives: %@",localDBProfile);
    
    if ([localDBProfile isKindOfClass:[NSNull class]] || localDBProfile == (id)[NSNull null])
    {
        //        [self reloadDataFromWeb];
        [self performSelectorOnMainThread:@selector(servererror) withObject:nil waitUntilDone:YES];
        
    }
    else{
        
        dict_profile = [[NSMutableDictionary alloc]init];
        
        dict_profile = [[NSMutableDictionary alloc]init];
        
        dict_profile = [[NSMutableDictionary alloc]init];
        
        dict_profile = [localDBProfile mutableCopy];
        profdict = [dict_profile mutableCopy];
        [activity removeFromSuperview];
        
        prof_img = [[UIImageView alloc]initWithFrame:CGRectMake(12, 12, 76, 76)];
        prof_img.layer.cornerRadius = prof_img.frame.size.width/2;
        
        [headingView addSubview:prof_img];
        
        
        NSString *fullname1;
        
        if (![[NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]] isKindOfClass:[NSNull class]] && ![[NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]] isEqualToString:@""] && ![[NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]] isEqualToString:@"(null)"] && [[NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]] length]>0)
        {
            
            fullname1= [NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]];
        }
        
        [prof_name removeFromSuperview];
        
        prof_name = [[UILabel alloc]init]; //WithFrame:CGRectMake(112, 72, 196, 60)];
        prof_name.backgroundColor=[UIColor clearColor];
        prof_name.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
        
        
        if ([fullname1 isKindOfClass:[NSNull class]] || [fullname1 isEqualToString:@""] || [fullname1 isEqualToString:@"(null) (null)"] || [fullname1 length]==0) {
            
            prof_name.text = @"";
            DebugLog(@"PROFILE NAME1:%@",prof_name);
            prof_name.frame=CGRectMake(0, 0, 0, 0);
            
            // [prof_name removeFromSuperview];
            
        }
        else
        {
            prof_name.text=fullname1;
            prof_name.frame=CGRectMake(prof_img.frame.origin.x+prof_img.frame.size.width+24, 17, [UIScreen mainScreen].bounds.size.width-120, 35);
            [prof_name setBackgroundColor:[UIColor clearColor]];
            prof_name.font = [UIFont fontWithName:@"ProximaNova-Bold" size:17];
            prof_name.numberOfLines = 2;
            prof_name.text = [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"fullname"]];
            prof_name.lineBreakMode = NSLineBreakByWordWrapping;
            prof_name.textColor = [UIColor blackColor];
            prof_name.textAlignment = NSTextAlignmentLeft;
            [headingView addSubview:prof_name];
            [prof_name adjustsFontSizeToFitWidth];
            
            UIView *locationView = [[UIView alloc] init];
            locationView.frame = CGRectMake(prof_img.frame.origin.x+prof_img.frame.size.width+24, 52, [UIScreen mainScreen].bounds.size.width-120, 35);
            locationView.backgroundColor = [UIColor clearColor];
            [headingView addSubview:locationView];
            
            UIImageView *loc_img = [[UIImageView alloc] init];
            loc_img.frame = CGRectMake(0, 8, 7, 9);
            loc_img.image = [UIImage imageNamed:@"Loction"];
            [locationView addSubview:loc_img];
            
            location = [[UILabel alloc] init];
            location.frame = CGRectMake(12, 5, [UIScreen mainScreen].bounds.size.width-100, 17);
            location.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
            location.textColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:0.8f];
            location.text = [[NSString stringWithFormat:@"%@, %@",[dict_profile objectForKey:@"city"],[dict_profile objectForKey:@"country"]] uppercaseString];
            [locationView addSubview:location];
        }
        
        
        
        // prof_name.text=fullname1;
        //    prof_name.font=[UIFont fontWithName:@"Mark Simonson - Proxima Nova Alt Condensed Bold" size:19];
        prof_name.font=[UIFont fontWithName:@"ProximaNova-Bold" size:17];
        prof_name.numberOfLines=0;
        
        
        base64String= [localDBProfile objectForKey:@"thumb"];
        
        if ([base64String length] >6)
        {
            NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
            
            //    NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
            
            profilepic = [UIImage imageWithData:decodedData];
            
            prof_img.image=profilepic;
            
            prof_img.contentMode= UIViewContentModeScaleAspectFill;
            
            prof_img.clipsToBounds=YES;
        }
        
        prof_img.userInteractionEnabled=YES;
        
        UITapGestureRecognizer *propictap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(detailpic:)];
        
        [prof_img addGestureRecognizer:propictap];
        
        
    }
    
}

-(void)startDBInsert
{
}

-(void)startDBInserta:(NSNotification *)notification
{
    
    if (profile_insertion == 0)
    {
        profile_insertion =1;
        DebugLog(@"startdbinserta called");
        [[DBManager getSharedInstance]insertProfile:profdict :[user_id intValue]];
    }
}

-(void)removeConnection//: (id)sender
{
    
    [UIView animateWithDuration:0.3 animations:^{
        
        //coverView.backgroundColor = [UIColor blackColor];
        
        UIColor *darkOp =
        [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
        UIColor *lightOp =
        [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
        
        // Create the gradient
        CAGradientLayer *gradient = [CAGradientLayer layer];
        
        // Set colors
        gradient.colors = [NSArray arrayWithObjects:
                           (id)lightOp.CGColor,
                           (id)darkOp.CGColor,
                           nil];
        
        // Set bounds
        gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
        
        // Add the gradient to the view
        [coverView.layer insertSublayer:gradient atIndex:0];
        
        
        
        
        [blackView setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0)];
        
        [businessImg setFrame:CGRectMake(15, 0, 61.0f/2.0f, 0)];
        
        [togglebt setFrame:CGRectMake(50, 0, 180, 0)];
        
        [lineDivMenu setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0)];
        
        [removeImg setFrame:CGRectMake(15, 0, 61.0f/2.0f, 0)];
        
        removeconbt.frame = CGRectMake(36, 0, 180, 0);
        
    }
     
     //        }];
     
                     completion:^(BOOL finished){
                         
                         rightmenuOpen = YES;
                         
                     }];
    
    
    
    alert = [[UIAlertView alloc] initWithTitle:@"Remove this contact?"
                                       message:nil
                                      delegate:self
                             cancelButtonTitle:@"Cancel"  otherButtonTitles:@"Ok", nil];
    alert.tag=123;
    [alert show];
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    DebugLog(@"ALERT . TAG====%ld",(long)alertView.tag);
    if (alertView.tag == 123) {
        
        if(buttonIndex == 0)
        {
            // Do something
        }
        else
        {
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            //            NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=removeconnection&id=%d&access_token=%@&device_id=%@&business=true",[user_id intValue],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
            
            NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action= group-removemember&group_id=%d&user_id=%d&access_token=%@&device_id=%@&business=true",[groupid intValue], [user_id intValue],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
            
            
            DebugLog(@"deny url: %@",urlString1);
            NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
            NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
            
            if (signeddataURL1 != nil)
            {
                NSError *error=nil;
                NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                          options:kNilOptions
                                                                            error:&error];
                DebugLog(@"deny json returns: %@",json_deny);
                if ([[json_deny objectForKey:@"success"]intValue] == 1)
                {
                    alert = [[UIAlertView alloc] initWithTitle:@"Connection Successfully Removed!"
                                                       message:nil
                                                      delegate:self
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                    [alert show];
                    [removeconbt removeFromSuperview];
                    //                togglebt.userInteractionEnabled = NO;
                    
                    ConNewRequestsViewController *mng = [[ConNewRequestsViewController alloc]init];
                    //                mng.imported_arr = [filtered_arr objectAtIndex:indexPath.row];
                    mng.userid= [user_id intValue];
                    DebugLog(@"selected user id : %d",mng.userid);
                    
//                    CATransition* transition = [CATransition animation];
//                    
//                    transition.duration = 0.4;
//                    transition.type = kCATransitionPush;
//                    transition.subtype = kCATransitionFade;
//                    
//                    [[self navigationController].view.layer addAnimation:transition forKey:nil];
                    
                    [self.navigationController pushViewController:mng animated:YES];
                }
                else
                {
                    alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                                       message:[NSString stringWithFormat:@"%@",[json_deny objectForKey:@"error"]]
                                                      delegate:self
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                    [alert show];
                }
            }
            else
            {
                alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                                   message:nil
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                [alert show];
            }
        }
    }else if (alertView.tag == 9 || alertView.tag == 99){
        
        DebugLog(@"BUTTON INDEX 0");
        if (buttonIndex == 0)
        {
            
            [bgcallsms setBackgroundImage:[UIImage imageNamed:@"call_profile"] forState:UIControlStateNormal];
            
        }
        
    }else if (alertView.tag == 6 || alertView.tag == 66){
        
        if(buttonIndex == 0)
        {
            [bgcallsms1 setBackgroundImage:[UIImage imageNamed:@"sms_profile"] forState:UIControlStateNormal];
            
        }
        
    }
}

-(void)websiteFun: (id)sender
{
    DebugLog(@"dcdc");
    
//    NSUserDefaults *webcheck=[NSUserDefaults standardUserDefaults];
//    [webcheck setObject:@"WebViewOpen" forKey:@"webview"];
//    [webcheck synchronize];
    
    SocialWebViewController *social= [[SocialWebViewController alloc]init];
    if (switchprof == 0)
        //        social.type_social=[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"website"]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"website"]]]];
    else
        //        social.type_social=[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_website"]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_website"]]]];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
//    
//    [self.navigationController pushViewController:social animated:YES];
}

-(void)yesSuccess
{
    //    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //    [del showTabValues:YES];
}

-(void)editprofile_func: (id)sender
{
    ConFullSettingsViewController *con =[[ConFullSettingsViewController alloc]init];
    con._control_check = TRUE;
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    transition.subtype = kCATransitionFromRight;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:NO];
    //[self presentViewController:con animated:NO completion:nil];
}

-(void)smsfunc
{
    //sms_btn.alpha=0.0f;
    sms_img.alpha=1.0f;
   
    if ([phonearr count] == 0)
    {
        [bgcallsms1 setBackgroundImage:[UIImage imageNamed:@"smsend_profile"] forState:UIControlStateNormal];
        
        
        smsAlert1 = [[UIAlertView alloc] initWithTitle:@"No Number found for this user!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        smsAlert1.tag = 6;
        [smsAlert1 show];
    }
    else
    {
        
        [bgcallsms1 setBackgroundImage:[UIImage imageNamed:@"smsend_profile"] forState:UIControlStateNormal];
        
        UIDevice *device = [UIDevice currentDevice];
        if ([[device model] isEqualToString:@"iPhone"]) {
            
            MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
            if([MFMessageComposeViewController canSendText])
            {
                [self presentViewController:controller animated:YES completion:Nil];
                controller.body = @"";
                NSString *smsstring= [NSString stringWithFormat:@"%@",[phonearr objectAtIndex:0]];
                controller.recipients = [NSArray arrayWithObjects:smsstring, nil];
                controller.messageComposeDelegate = self;
                //[self presentModalViewController:controller animated:YES];
            }
        }
        else
        {
            smsAlert2=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            smsAlert2.tag = 66;
            [smsAlert2 show];
        }
    }
    
    
}

-(void)phonefunc:(UIButton *)sender
{
    call_img.alpha=1.0f;
    DebugLog(@"call phonesarr: %@",phonearr);
    if ([phonearr count] == 0)
    {
        [bgcallsms setBackgroundImage:[UIImage imageNamed:@"callend_profile"] forState:UIControlStateNormal];
        
        callAlert1 = [[UIAlertView alloc] initWithTitle:@"No Number found for this user!"
                                                message:nil
                                               delegate:self
                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        callAlert1.tag = 9;
        
        [callAlert1 show];
        
    }
    else
    {
        
        UIDevice *device = [UIDevice currentDevice];
        if ([[device model] isEqualToString:@"iPhone"] ) {
            NSString *phoneNumber = [[[phonearr objectAtIndex:0] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"] invertedSet]] componentsJoinedByString:@""];
            phoneNumber = [@"tel://" stringByAppendingString:phoneNumber];
            DebugLog(@"PHONE NUMBER: %@",phoneNumber);
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
        } else {
            callAlert2=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            callAlert2.tag = 99;
            [callAlert2 show];

        }
    }
}

-(void)tapphonefunc
{
    mobileView.backgroundColor = [UIColor grayColor];
    //call_btn.alpha=0.0f;
    call_img.alpha=1.0f;
    DebugLog(@"call phonesarr: %@",phonearr);
    if ([phonearr count] == 0)
    {
        [bgcallsms setBackgroundImage:[UIImage imageNamed:@"callend_profile"] forState:UIControlStateNormal];
        
        callAlert1 = [[UIAlertView alloc] initWithTitle:@"No Number found for this user!"
                                                message:nil
                                               delegate:self
                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        callAlert1.tag = 9;
        [callAlert1 show];
    }
    else
    {
        [bgcallsms setBackgroundImage:[UIImage imageNamed:@"callend_profile"] forState:UIControlStateNormal];
        
        
        UIDevice *device = [UIDevice currentDevice];
        if ([[device model] isEqualToString:@"iPhone"] ) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString: [NSString stringWithFormat:@"tel:%@",[phonearr objectAtIndex:0]]]];
        } else {
            callAlert2=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            callAlert2.tag = 99;
            [callAlert2 show];
        }
    }
}


-(void)chatfunc:(UIButton *)sender
{
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select a option"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"WhatsApp", @"SMS", nil];
    
    //            [actionSheet showInView:self.view];
    actionSheet.tag = 900;
    [actionSheet showInView:mainview];
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

-(void)gobackoption:(UIButton *)button
{
    //    backBground.backgroundColor = [UIColor colorWithRed:(30.0f/255.0f) green:(30.0f/255.0f) blue:(30.0f/255.0f) alpha:1.0f];
    if (button.tag==0)
    {
        
        CATransition *transition = [CATransition animation];
        
        transition.duration = 0.4f;
        
        transition.type = kCATransitionFade;
        
        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    if (button.tag==1)
    {
        
        CATransition *transition = [CATransition animation];
        
        transition.duration = 0.4f;
        
        transition.type = kCATransitionFade;
        
        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    if (button.tag==2)
    {
        GroupDetailsViewController *grp=[[GroupDetailsViewController alloc]init];
        grp.filter=YES;
        grp.filtered_arr=[filter_arr mutableCopy];
        grp.isadmin=isadmin;
        grp.groupname=groupname;
        grp.groupimage.hidden=YES;
        grp.group_namelbl.hidden=YES;
        //grp.searchBar.hidden=NO;
        grp.check_search=@"YES";
        grp.groupid=groupid;
        DebugLog(@"member count 4 %@",membercount);
        grp.member_count=membercount;
        grp.group_array=search;
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:grp animated:YES];
        //        [self.navigationController popViewControllerAnimated:NO];
    }
    
    if (button.tag==3)
    {
        GroupDetailsViewController *grp=[[GroupDetailsViewController alloc]init];
        grp.search_result=[filter_arr mutableCopy];
        DebugLog(@"copied array 2: %@",grp.search_result);
        grp.groupstable.hidden=YES;
        //        grp.searchtableback.hidden=NO;
        grp.isadmin=isadmin;
        [grp.invite setEnabled:YES];
        DebugLog(@"member count 4 %@",membercount);
        grp.member_count=membercount;
        grp.groupname=groupname;
        grp.groupid=groupid;
        grp.check_member=@"YES";
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        transition.subtype = kCATransitionFromLeft;
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:grp animated:NO];
        //[self.navigationController popViewControllerAnimated:YES];
    }
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return array_menu.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MyReuseIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    cell=nil;
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:MyIdentifier];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.backgroundColor=[UIColor clearColor];
    if (switchprof == 0)
    {
        UILabel *menulabel = [[UILabel alloc]initWithFrame:CGRectMake(55.0f,0.0f, righttable.frame.size.width-55, 57.5f)];
        [menulabel setText:[NSString stringWithFormat:@"%@",[array_menu objectAtIndex:indexPath.row]]];
        [menulabel setTextAlignment:NSTextAlignmentLeft];
        [menulabel setTextColor:[UIColor whiteColor]];
        menulabel.userInteractionEnabled=YES;
        [menulabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
        [cell addSubview:menulabel];
    }
    else
    {
        UILabel *menulabel = [[UILabel alloc]initWithFrame:CGRectMake(55.0f,0.0f, righttable.frame.size.width-55, 57.5f)];
        [menulabel setText:[NSString stringWithFormat:@"%@",[arraymenu_busi objectAtIndex:indexPath.row]]];
        [menulabel setTextAlignment:NSTextAlignmentLeft];
        [menulabel setTextColor:[UIColor whiteColor]];
        menulabel.userInteractionEnabled=YES;
        [menulabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
        [cell addSubview:menulabel];
        
    }
    UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(15.0f,18.75f,20.0f,20.0f)];
    image.backgroundColor=[UIColor clearColor];
    image.userInteractionEnabled=YES;
    [image setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_image objectAtIndex:indexPath.row]]]];
    image.contentMode=UIViewContentModeScaleAspectFit;
    [cell addSubview:image];
    
    UILabel *separatorlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 57, [UIScreen mainScreen].bounds.size.width,0.5)];
    separatorlabel.backgroundColor = [UIColor colorWithRed:(102.0f/255.0f) green:(102.0f/255.0f) blue:(102.0f/255.0f) alpha:1.0f];
    [cell addSubview:separatorlabel];
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 57.5f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
        //        [self performSelector:@selector(removeConnection:)];
        
        [self removeConnection];
        blackView.hidden=YES;
        rightmenuOpen=YES;
    }
}

-(void)rightMenu:(UIButton *)sender{
    
    DebugLog(@"rightMenu: %x", rightmenuOpen);
    
    [overlayTap setEnabled:NO];
    //    menuBground.backgroundColor = [UIColor clearColor];
    array_menu= [[NSMutableArray alloc] initWithObjects:@"Remove Connection", nil];
    arraymenu_busi= [[NSMutableArray alloc] initWithObjects:@"Remove Connection", nil];
    array_image= [[NSMutableArray alloc] initWithObjects:@"remove_profile", nil];
    
    if(rightmenuOpen == YES) {
        
        //        menuBground.backgroundColor = [UIColor colorWithRed:(30.0f/255.0f) green:(30.0f/255.0f) blue:(30.0f/255.0f) alpha:1.0f];
        
        blackView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 55)];
        //        [blackView setBackgroundColor:[UIColor blackColor]];
        blackView.layer.zPosition = 5;
        blackView.backgroundColor=[[UIColor blackColor] colorWithAlphaComponent:0.9f];
        //        [blackView setAlpha:0.9f];
        //        blackView.layer.zPosition = 3;
        blackView.opaque=NO;
        [blackView setUserInteractionEnabled:YES];
        [headingView addSubview:blackView];
        
        righttable=[[UITableView alloc] initWithFrame:CGRectMake(0, blackView.frame.origin.y, blackView.frame.size.width, blackView.frame.size.height)];
        righttable.delegate=self;
        righttable.dataSource=self;
        [righttable setBackgroundColor:[UIColor clearColor]];
        righttable.separatorStyle=UITableViewCellSeparatorStyleNone;
        righttable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        [blackView addSubview:righttable];
        [righttable reloadData];
        rightmenuOpen = NO;
        DebugLog(@"blackview color %@", blackView.backgroundColor);
    }
    else if (rightmenuOpen == NO)
    {
        
        blackView.hidden=YES;
        rightmenuOpen = YES;
    }
    
}
-(void)changeButtonBackGroundColor:(id)sender{
    
    [rightMenuBack setBackgroundColor:[UIColor colorWithRed:(30.0f/255.0f) green:(30.0f/255.0f) blue:(30.0f/255.0f) alpha:1.0f]];
    
}
-(void)resetButtonBackGroundColor:(id)sender{
    
    [rightMenuBack setBackgroundColor:[UIColor blackColor]];
    
}

-(void)setAddress
{
    
    
    y1=0;
    
    if (![street isEqualToString: @""] && [number isEqualToString: @""])
    {
        street_number_lbl.text=[NSString stringWithFormat:@"%@,",street];
        street_number_lbl.frame = CGRectMake(38, divider_2.frame.origin.y+divider_2.frame.size.height+8, 250, 20);
        y1=street_number_lbl.frame.size.height+8;
    }
    else if ([street isEqualToString: @""] && ![number isEqualToString: @""])
    {
        street_number_lbl.text=[NSString stringWithFormat:@"%@,",number];
        street_number_lbl.frame = CGRectMake(38, divider_2.frame.origin.y+divider_2.frame.size.height+8, 250, 20);
        y1=street_number_lbl.frame.size.height+8;
    }
    else if (![street isEqualToString: @""] && ![number isEqualToString: @""])
    {
        street_number_lbl.text=[NSString stringWithFormat:@"%@, %@,",street,number];
        street_number_lbl.frame = CGRectMake(38, divider_2.frame.origin.y+divider_2.frame.size.height+8, 250, 20);
        y1=street_number_lbl.frame.size.height+8;
    }
    else if ([street isEqualToString: @""] && [number isEqualToString: @""])
    {
        street_number_lbl.frame = CGRectMake(38, divider_2.frame.origin.y+divider_2.frame.size.height+8, 250, 0);
        y1=street_number_lbl.frame.size.height+8;
    }
    if (![zipcode isEqualToString: @""] && [city isEqualToString:@""])
    {
        zipcode_lbl.text=[NSString stringWithFormat:@"%@",zipcode];
        zipcode_lbl.frame = CGRectMake(38, street_number_lbl.frame.origin.y+street_number_lbl.frame.size.height, 250, 20);
        y1=y1+zipcode_lbl.frame.size.height;
    }
    else if ([zipcode isEqualToString: @""] && ![city isEqualToString:@""])
    {
        zipcode_lbl.text=[NSString stringWithFormat:@"%@",city];
        zipcode_lbl.frame = CGRectMake(38, street_number_lbl.frame.origin.y+street_number_lbl.frame.size.height, 250, 20);
        y1=y1+zipcode_lbl.frame.size.height;
    }
    else if (![zipcode isEqualToString: @""] && ![city isEqualToString:@""])
    {
        zipcode_lbl.text=[NSString stringWithFormat:@"%@, %@",zipcode,city];
        zipcode_lbl.frame = CGRectMake(38, street_number_lbl.frame.origin.y+street_number_lbl.frame.size.height, 250, 20);
        y1=y1+zipcode_lbl.frame.size.height;
    }
    else if ([zipcode isEqualToString: @""] && [city isEqualToString:@""])
    {
        zipcode_lbl.frame = CGRectMake(38, street_number_lbl.frame.origin.y+street_number_lbl.frame.size.height, 250, 0);
        y1=y1+zipcode_lbl.frame.size.height;
    }
    if (![country isEqualToString: @""]) {
        country_lbl.text=[NSString stringWithFormat:@"%@",country];
        country_lbl.frame = CGRectMake(38, zipcode_lbl.frame.origin.y+zipcode_lbl.frame.size.height, 250, 20);
        y1=y1+country_lbl.frame.size.height;
    }
    else if ([country isEqualToString: @""])
    {
        country_lbl.frame = CGRectMake(38, zipcode_lbl.frame.origin.y+zipcode_lbl.frame.size.height, 250, 0);
        y1=y1+country_lbl.frame.size.height;
    }
    DebugLog(@"street frame: %@",NSStringFromCGRect(street_number_lbl.frame));
    
    
}

-(void)phone_color
{
    call_img.alpha = 0.5f;
    
}
-(void)phone_colorAgain
{
    call_img.alpha = 1.0f;
}
-(void)sms_color
{
    sms_img.alpha = 0.5f;
}
-(void)sms_colorAgain
{
    sms_img.alpha = 1.0f;
}
-(void)email_color
{
    email_img.alpha = 0.5f;
}
-(void)email_colorAgain
{
    email_img.alpha = 1.0f;
}
-(void)loc_color
{
    location_img.alpha = 0.5f;
}
-(void)loc_colorAgain
{
    location_img.alpha = 1.0f;
}

-(void)whatsapp_func
{
    DebugLog(@"profile name field text: %@",prof_name.text);
    // Fetch the address book
    CFErrorRef *errorab = nil;
    
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, errorab);
    
    CFArrayRef people = ABAddressBookCopyPeopleWithName(addressBook,
                                                        (__bridge CFStringRef)prof_name.text);
    
    // Display "Appleseed" information if found in the address book
    if ((people != nil) && (CFArrayGetCount(people) > 0))
    {
        for (int m = 0; m < (CFArrayGetCount(people)); m++){
            DebugLog(@"count e dichhe: %ld == %@", (CFArrayGetCount(people)),people);
            ABRecordRef person = CFArrayGetValueAtIndex(people, 0);
            
            ABRecordID recordID = ABRecordGetRecordID(person);
            
            DebugLog(@"ab id: %d",recordID);
            DebugLog(@"phone array; %@", phonearr);
            
            ABMultiValueRef phoneNumberProperty = ABRecordCopyValue(person, kABPersonPhoneProperty);
            NSArray *_phoneNumbers = (__bridge NSArray*)ABMultiValueCopyArrayOfAllValues(phoneNumberProperty);
            
            NSMutableArray *mutableph= [_phoneNumbers mutableCopy];
            int pp;
            for (pp =0; pp< [mutableph count]; pp++)
            {
                if ([[mutableph objectAtIndex:pp] isKindOfClass:[NSNull class]] || [mutableph objectAtIndex:pp] == (id)[NSNull null] || [[mutableph objectAtIndex:pp] length] < 3)
                {
                    [mutableph removeObjectAtIndex:pp];
                    pp= pp-1;
                }
            }
            NSMutableSet* set1 = [NSMutableSet setWithArray:mutableph];
            NSMutableSet* set2 = [NSMutableSet setWithArray:phonearr];
            [set1 intersectSet:set2]; //this will give you only the obejcts that are in both sets
            
            NSArray* result = [set1 allObjects];
            DebugLog(@"result pelo: %@",result);
            
            if ([phonearr count] > 0)
            {
                NSString * urlWhats = [NSString stringWithFormat:@"whatsapp://send?abid=%d&text=",recordID];
                NSURL * whatsappURL = [NSURL URLWithString:[urlWhats stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
                    [[UIApplication sharedApplication] openURL: whatsappURL];
                } else {
                    UIAlertView * alert1 = [[UIAlertView alloc] initWithTitle:@"WhatsApp not installed." message:@"Your device has no WhatsApp installed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert1 show];
                }
                break;
            }
        }
    }
    else
    {
        // Show an alert if "Appleseed" is not in Contacts
    }
    //CFRelease(addressBook);
    //CFRelease(people);
}

-(UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
