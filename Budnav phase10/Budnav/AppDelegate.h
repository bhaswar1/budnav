//
//  AppDelegate.h
//  Budnav
//
//  Created by Soumarsi Kundu on 30/01/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConLoginViewController.h"
#import <sqlite3.h>
#import <AddressBookUI/AddressBookUI.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate,UITabBarControllerDelegate,UITabBarDelegate,UIAlertViewDelegate,UIApplicationDelegate, NSURLConnectionDataDelegate>
{
    UIAlertView *alert;
    sqlite3 *contactDB;
    
    __block int dbstatusq;
    
    NSDictionary *json1, *indexedSections;
    NSMutableArray *app_contacts,  *testarr, *final_Bhaswar_array, *check_app_cont,*appdelwebarray, *con_array;
    NSMutableDictionary *newns, *add_contacts_dict;
    int loadcheck, number_of_iterations, connectiondoublefiler, restriction;
    NSString *con_count,*version, *request_count;
    NSMutableData *responseData;
    ABAddressBookRef addressBookRef;
    BOOL isAtLeast7,isAtLeast8;
    NSData *decodedData;
    UIImage *profilepic;
    NSTimer *timer;
    
}
@property (strong, nonatomic) UIWindow *window;
@property(strong,nonatomic) NSString *groupnameUpdate;

@property (readwrite,strong, nonatomic) NSString *signup;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) UINavigationController *navigationController;
@property (strong, nonatomic) UITabBarController *tabBarController;
@property (nonatomic, assign) int previousTabIndex, dbstatus;
@property(nonatomic,retain)NSMutableArray *arr_contacts_ad, *recom_search_array, *Bhaswar_array, *now_Bhaswar_array,*uniquearray_new, *part_array, *newarr_contacts_ad, *device_phonebook_array, *selected_array, *deletedIndex;
@property(nonatomic,retain)NSString *recom_search_string, *pushtoprofileid;
@property(nonatomic,readwrite)long writetodbcount;
@property (nonatomic) NSInteger scrollsection, scrollrow;
@property (nonatomic,readwrite)float scrolloffsety;

@property (nonatomic, strong) NSUserDefaults *login_user;
@property (nonatomic, strong) NSMutableDictionary *business_dict, *personal_dict, *social_dict, *profdict;
@property (nonatomic, assign) BOOL profileRedirect, newrequestRedirect, newUser;


-(void)showTabStatus:(BOOL)status;
-(void)showTabValues:(BOOL)status;
-(void)createEditableCopyOfDatabaseIfNeeded;
- (NSURL *)applicationDocumentsDirectory;

-(void) loadContactsIntoDB :(NSMutableArray *)array;
-(void) loadRequestsIntoDB :(NSMutableArray *)array;
-(void) loadMapIntoDB :(NSMutableArray *)array;
-(void) loadProfileIntoDB :(NSDictionary *)dict :(int)userid;
-(void) reloadNewContactsFromWeb;
-(void) reloadContactsFromWeb;
-(void) selectedIcon;
-(void)ownDetails;
-(void)reloadFurther;

-(void)sendingRequest;
@end

/*
 count no. of app contacts (n)
 count total array
 delete first n items from total array
 add app contacts array in place of that deleted array
 save the array in defaults
 display in tableview
 // address book id of app contacts for whatsapp
 */
