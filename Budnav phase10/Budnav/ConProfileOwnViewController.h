//
//  ConProfileOwnViewController.h
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 07/05/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConParentTopBarViewController.h"
#import "ConSettingsView.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "Mymodel.h"
#import "GAITrackedViewController.h"

@interface ConProfileOwnViewController : GAITrackedViewController<UIGestureRecognizerDelegate,MKMapViewDelegate, NSURLConnectionDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,UIScrollViewDelegate,UIActionSheetDelegate,UIAlertViewDelegate>
{
    //    UIView *topbar;
    MKMapView *map_View;
    MFMailComposeViewController *mailComposer;
    UIScrollView *mainscroll;
    UIActionSheet *anActionSheet;
}
@property (nonatomic, strong) NSString *other,*uid, *mapReturn;
@property (nonatomic, strong) UIView *mainview;
@property(nonatomic,retain) NSString *getmapvalue;
@property(nonatomic,retain) NSMutableDictionary *personal_dict, *business_dict, *social_dict;
@property(nonatomic,retain) NSMutableArray *personal_array, *business_array, *social_array, *phonearr;
@property(nonatomic,retain) NSDictionary *dict_profile,*profdict;

+(void)chngpostion;
@end
