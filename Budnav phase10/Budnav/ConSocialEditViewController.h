//
//  ConSocialEditViewController.h
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 30/05/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface ConSocialEditViewController : GAITrackedViewController<UIAlertViewDelegate,UITextFieldDelegate,UIScrollViewDelegate>
{
    UIView *topbar;
    UIScrollView *scviewbudget;
    NSMutableArray *imagesiconarr,*toptextarr;
    UITextField *twittertext, *pinteresttext, *facebooktext, *googleplustext, *youtubetext, *skypetext, *instagramtext, *linkedintext, *vimeotext,  *soundcloudtext;
    UIAlertView *alert;
    NSDictionary *json1,*profdict;
    NSMutableDictionary *personal_dict, *business_dict, *social_dict;
    UIActivityIndicatorView *act;
}
@end
