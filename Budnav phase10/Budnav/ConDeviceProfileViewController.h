//
//  ConDeviceProfileViewController.h
//  Contacter
//
//  Created by Soumarsi_kundu on 23/06/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConParentTopBarViewController.h"
#import <MessageUI/MessageUI.h>
#import "GAITrackedViewController.h"

@interface ConDeviceProfileViewController : GAITrackedViewController<UIActionSheetDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate, UINavigationControllerDelegate>{
    
    
    UIView *mainview;
    int move;
    float y;
    NSString *base64String;
    UIImage *profilepic;
    UIView *divider_1,*divider_2,*divider_9;
    UIImageView *mobileimg,*homeimg,*workimg,*mailimg,*address_img;
    NSString *address_str;
    MFMailComposeViewController *mailComposer;
    NSMutableArray *mobilearray,*workarray;
}

@property(nonatomic,retain)NSDictionary *ContactDetails_Dict;
+(void)chngpostion;

@end