//
//  ConSyncLoaderViewController.m
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 12/07/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

#import "ConSyncNewViewController.h"
#import "MDRadialProgressView.h"
#import "MDRadialProgressTheme.h"
#import "MDRadialProgressLabel.h"
#import "SVProgressHUD.h"
#import "ConSettingsView.h"
#import "AppDelegate.h"
#import "ConNewContactsViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

@interface ConSyncNewViewController ()<NSURLConnectionDelegate,NSURLConnectionDataDelegate>
{
    UILabel *syncstatus;
    int number_of_iterations;
    int successTag;
    int connectionsuccess;
}
@end

@implementation ConSyncNewViewController
@synthesize length,cell_objects,about_cell_objects,itemsTable,about_Table,mainScroll,i,table_view,about_sep,aboutview,move, moveparent,mainview,topbar,myTextField,pass,con_array,final_con_array,filtered_arr, con_array1,add_contacts_dict,app_contacts, check_app_cont, phonesarr, check_app_cont1, backuparr, testarr,addtophonebook, existingphn,uniquearray, part_array,responseData,logoimg,phnstr,finalph,getbackupdict,backarrnumbers,conhome,conwork,conmobile;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

-(void)memoryfunction
{
    DebugLog(@"Memory warning function called");
    addtophonebook = nil;
    backuparr = nil;
    phnstr = nil;
    finalph = nil;
    conhome = nil;
    conmobile = nil;
    conwork = nil;
    testarr = nil;
    getbackupdict = nil;
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(memoryfunction) name:@"applicationDidReceiveMemoryWarning" object:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    successTag = 0;
    number_of_iterations = 0;
    connectionsuccess = 0;
    responseData = [NSMutableData new];
    //        CGRect *frame = CGRectMake(60, 200, 100, 100);
    
    self.view.backgroundColor=[UIColor blackColor];
    
    UIView *whiteview = [[UIView alloc]initWithFrame:CGRectMake(55, 150, 210, 210)];
    [self.view addSubview:whiteview];
    whiteview.backgroundColor=[UIColor clearColor];
    //        whiteview.center=self.center;
    UIImageView *logo_img = [[UIImageView alloc]initWithFrame:CGRectMake(114, 31, 92.5f, 16)];
    logo_img.image=[UIImage imageNamed:@"logomod.png"];
    [self.view addSubview:logo_img];
    
    radialView3 = [self progressViewWithFrame:CGRectMake(0, 140, 200, 200)];
    radialView3.progressTotal = 100;
    radialView3.progressCounter = 0;
    radialView3.theme.completedColor = [UIColor whiteColor];//colorWithRed:132/255.0 green:23/255.0 blue:23/255.0 alpha:1.0
    radialView3.theme.incompletedColor = [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]; //lightGrayColor
    radialView3.theme.thickness = 9.5f;
    radialView3.theme.sliceDividerHidden = YES;
    radialView3.theme.centerColor = [UIColor clearColor];
    //        radialView3.label.text
    radialView3.center= self.view.center;
    [self.view addSubview:radialView3];
    
    syncstatus = [[UILabel alloc]initWithFrame:CGRectMake(0, self.view.bounds.size.height-90, [UIScreen mainScreen].bounds.size.width, 40)];
    syncstatus.text=@"Backing up...";
    syncstatus.textColor=[UIColor whiteColor];
    syncstatus.textAlignment=NSTextAlignmentCenter;
    syncstatus.font= [UIFont fontWithName:@"ProximaNova-Regular" size:18];
    [self.view addSubview:syncstatus];
    
    timerr = [NSTimer scheduledTimerWithTimeInterval:1.5f
                                              target:self
                                            selector:@selector(targetMethod:)
                                            userInfo:nil
                                             repeats:YES];
    
    
    ////////////////////////////////////////////////////////////     GET BACK UP      ///////////////////////////////////////////////////////////
    
    @autoreleasepool {
        
        //            dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        //            dispatch_async(q, ^{
        
        [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"FromRestoreToBackup"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        //            NSMutableArray *contactpickarr = [[NSMutableArray alloc]init];
        existingphn = [[NSMutableArray alloc]init];
        
        AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        existingphn =[del.arr_contacts_ad mutableCopy];
        DebugLog(@"array content part 2 = %@", existingphn);
        
        backuparr = [[NSMutableArray alloc]init];
        
        @try {
            
            //===========================================CON ARRAY CHANGES========================================//
            
            con_array =[[NSMutableArray alloc]init];
            CFErrorRef *errorab = nil;
            
            ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, errorab);
            
            __block BOOL accessGranted = NO;
            
            
            if (&ABAddressBookRequestAccessWithCompletion != NULL) { // we're on iOS 6
                dispatch_semaphore_t sema = dispatch_semaphore_create(0);
                ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
                    accessGranted = granted;
                    dispatch_semaphore_signal(sema);
                });
                dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
                //        dispatch_release(sema);
            }
            else { // we're on iOS 5 or older
                accessGranted = YES;
            }
            if (accessGranted) {
                
                NSMutableArray *contactpickarr = [[NSMutableArray alloc]init];
                
                ABAddressBookRef UsersAddressBook = ABAddressBookCreateWithOptions(NULL, NULL);
                
                //contains details for all the contacts
                CFArrayRef ContactInfoArray = ABAddressBookCopyArrayOfAllPeople(UsersAddressBook);
                
                //get the total number of count of the users contact
                CFIndex numberofPeople = CFArrayGetCount(ContactInfoArray);
                DebugLog(@"%ld",numberofPeople);
                con_array = [[NSMutableArray alloc]init];
                
                //iterate through each record and add the value in the array
                for (int ik =0; ik<numberofPeople; ik++) {
                    ABRecordRef ref = CFArrayGetValueAtIndex(ContactInfoArray, ik);
                    ABMultiValueRef names = (__bridge ABMultiValueRef)((__bridge NSString*)ABRecordCopyValue(ref, kABPersonFirstNameProperty));
                    ABMultiValueRef lastnames = (__bridge ABMultiValueRef)((__bridge NSString*)ABRecordCopyValue(ref, kABPersonLastNameProperty));
                    
                    //        DebugLog(@"name from address book = %@ %@",names,lastnames);  // works fine.
                    NSString *fullname;
                    NSString *fnm = [NSString stringWithFormat:@"%@",names];
                    NSString *lnm = [NSString stringWithFormat:@"%@",lastnames];
                    //        DebugLog(@"fnm : %@",fnm);
                    if ([fnm length] ==0 || [fnm isKindOfClass:[NSNull class]] || fnm == (id)[NSNull null] || [fnm isEqualToString:@"(null)"])
                        fullname = [NSString stringWithFormat:@"%@",lastnames];
                    
                    else if ([lnm length] ==0 || [lnm isKindOfClass:[NSNull class]] || lnm == (id)[NSNull null] || [lnm isEqualToString:@"(null)"])
                        fullname = [NSString stringWithFormat:@"%@",names];
                    else
                        fullname = [NSString stringWithFormat:@"%@ %@",names,lastnames];
                    
                    //        NSString *contactName = (__bridge NSString *)(names);
                    
                    [contactpickarr addObject:fullname];
                    
                    //            NSData  *imgData = (__bridge NSData *)ABPersonCopyImageData(ref);
                    //            UIImage  *img = [UIImage imageWithData:imgData];
                    //            DebugLog(@"Image : %@",img);
                    
                    ABMultiValueRef emailProperty = ABRecordCopyValue(ref, kABPersonEmailProperty);
                    
                    NSArray *emailArray = (__bridge NSArray *)ABMultiValueCopyArrayOfAllValues(emailProperty);
                    NSString *newstringemail= [emailArray componentsJoinedByString:@","];
                    
                    NSMutableDictionary *ns = [[NSMutableDictionary alloc]init];
                    
                    [ns setObject:fullname forKey:@"name"];
                    //            [ns setObject:fullname forKey:@"fullname"];
                    [ns setValue:newstringemail forKey:@"email"];
                    
                    if ([newstringemail isKindOfClass:[NSNull class]] || newstringemail == (id)[NSNull null] || [newstringemail length] < 4)
                        [ns setValue:@"" forKey:@"email"];
                    
                    
                    ABMultiValueRef phoneNumberProperty = ABRecordCopyValue(ref, kABPersonPhoneProperty);
                    NSArray *_phoneNumbers = (__bridge NSArray*)ABMultiValueCopyArrayOfAllValues(phoneNumberProperty);
                    
                    NSMutableArray *mutableph= [_phoneNumbers mutableCopy];
                    int pp;
                    //                            DebugLog(@"mutable first: %@",mutableph);
                    for (pp =0; pp< [mutableph count]; pp++)
                    {
                        if ([[mutableph objectAtIndex:pp] isKindOfClass:[NSNull class]] || [mutableph objectAtIndex:pp] == (id)[NSNull null] || [[mutableph objectAtIndex:pp] length] < 3)
                        {
                            [mutableph removeObjectAtIndex:pp];
                            pp= pp-1;
                        }
                    }
                    //                            DebugLog(@"mutable second: %@",mutableph);
                    NSString *newstringph= [mutableph componentsJoinedByString:@","];
                    
                    [ns setValue:newstringph forKey:@"phone"];
                    
                    
                    ABMultiValueRef phones = ABRecordCopyValue(ref, kABPersonPhoneProperty);
                    for(CFIndex j = 0; j < ABMultiValueGetCount(phones); j++)
                    {
                        CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(phones, j);
                        CFStringRef locLabel = ABMultiValueCopyLabelAtIndex(phones, j);
                        NSString *phoneLabel =(__bridge NSString*) ABAddressBookCopyLocalizedLabel(locLabel);
                        //CFRelease(phones);
                        NSString *phoneNumber = (__bridge NSString *)phoneNumberRef;
                        //CFRelease(phoneNumberRef);
                        //CFRelease(locLabel);
                        DebugLog(@"phno:  - %@ (%@)", phoneNumber, phoneLabel);
                        [ns setValue:phoneNumber forKey:phoneLabel];
                    }
                    
                    //            NSMutableArray *addressarr = [[NSMutableArray alloc]init];
                    
                    //            ABMultiValueRef addrss = ABRecordCopyValue(ref, kABPersonAddressProperty);
                    //            for(CFIndex k = 0; k < ABMultiValueGetCount(addrss); k++)
                    //            {
                    ABMultiValueRef stt = ABRecordCopyValue(ref, kABPersonAddressProperty);
                    if (ABMultiValueGetCount(stt) > 0) {
                        CFDictionaryRef dict = ABMultiValueCopyValueAtIndex(stt, 0);
                        //                    DebugLog(@"dict address contacts: %@",dict);
                        NSString *streetp = CFDictionaryGetValue(dict, kABPersonAddressStreetKey);
                        NSString *cityp = CFDictionaryGetValue(dict, kABPersonAddressCityKey);
                        NSString *zipcdep = CFDictionaryGetValue(dict, kABPersonAddressZIPKey);
                        //                    NSString *statep = CFDictionaryGetValue(dict, kABPersonAddressStateKey);
                        NSString *countryp = CFDictionaryGetValue(dict, kABPersonAddressCountryKey);
                        //                    DebugLog(@"contacts phonebook address: %@ %@ %@ %@ %@",streetp,cityp,zipcdep,statep,countryp);
                        //                    NSString *address = [NSString stringWithFormat:@"%@,%@,%@,%@,%@",streetp,cityp,zipcdep,statep,countryp];
                        //                    [addressarr addObject:address];
                        
                        if ([streetp isKindOfClass:[NSNull class]] || streetp == (id)[NSNull null])
                            [ns setValue:@"" forKey:@"street"];
                        else
                            [ns setValue:streetp forKey:@"street"];
                        
                        if ([cityp isKindOfClass:[NSNull class]] || cityp == (id)[NSNull null])
                            [ns setValue:@"" forKey:@"city"];
                        else
                            [ns setValue:cityp forKey:@"city"];
                        
                        if ([zipcdep isKindOfClass:[NSNull class]] || zipcdep == (id)[NSNull null])
                            [ns setValue:@"" forKey:@"zip"];
                        else
                            [ns setValue:zipcdep forKey:@"street"];
                        
                        if ([countryp isKindOfClass:[NSNull class]] || countryp == (id)[NSNull null])
                            [ns setValue:@"" forKey:@"country"];
                        else
                            [ns setValue:countryp forKey:@"country"];
                        
                        [ns setValue:@"" forKey:@"number"];
                    }
                    else
                    {
                        [ns setValue:@"" forKey:@"number"];
                        [ns setValue:@"" forKey:@"city"];
                        [ns setValue:@"" forKey:@"street"];
                        [ns setValue:@"" forKey:@"zip"];
                        [ns setValue:@"" forKey:@"country"];
                    }
                    //            }
                    
                    //            [ns setObject:addressarr forKey:@"address"];
                    [con_array addObject:ns];
                }
            }
            DebugLog(@"CON_ARRAY=============================================> %@",con_array);
            
            
            
            //================================================================REMOVE FILTER BACKUP===================================//
            
            NSUserDefaults *prefsa = [NSUserDefaults standardUserDefaults];
            NSError *errora;
            NSDictionary *json1a;
            NSString *urlString1a =[NSString stringWithFormat:@"https://budnav.com/ext/?action=removefilterbackup&access_token=%@&device_id=%@",[prefsa objectForKey:@"access_token"],[prefsa objectForKey:@"deviceToken"]];
            
            DebugLog(@"REMOVE_FILTER_BACKUP=======================================> %@",urlString1a);
            
            NSString *newString1a = [urlString1a stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            NSData *signeddataURL1a =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1a]];
            if (signeddataURL1a == nil)
            {
                DebugLog(@"removebackup data nil");
            }
            else
            {
                json1a = [NSJSONSerialization JSONObjectWithData:signeddataURL1a //1
                          
                                                         options:kNilOptions
                          
                                                           error:&errora];
                if ([[json1a objectForKey:@"success"] intValue] == 1)
                {
                    successTag = 900;
                    
                    @try {
                        
                        NSError *err1 = nil;
                        
                        //                        NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"File" ofType:@"json"]];
                        //                        NSError *localError = nil;
                        //                        NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
                        //                       NSArray *countriesList = (NSArray *)parsedObject;
                        
                        
                        part_array = [[NSMutableArray alloc]init];
                        if ([con_array count] <= 300)
                        {
                            //                            part_array = [uniquearray mutableCopy];
                            number_of_iterations=1;
                        }
                        else
                        {
                            if ([con_array count] % 300 == 0)
                            {
                                number_of_iterations = (int)[con_array count]/300;
                            }
                            else
                            {
                                number_of_iterations = (int)[con_array count]/300 +1 ;
                            }
                        }
                        DebugLog(@"number of iterations are: %d", number_of_iterations);
                        
                        for (int numb =0; numb < number_of_iterations; numb++)
                        {
                            part_array = [[NSMutableArray alloc]init];
                            
                            if (numb == number_of_iterations -1)
                            {
                                NSArray *tempArray = [con_array subarrayWithRange:NSMakeRange(numb*300, [con_array count]-(300*numb))];
                                [part_array addObjectsFromArray:tempArray];
                            }
                            else
                            {
                                NSArray *tempArray = [con_array subarrayWithRange:NSMakeRange(numb*300, 299)];
                                [part_array addObjectsFromArray:tempArray];
                            }
                            
                            
                            //                            NSError *err1 = nil;
                            
                            DebugLog(@"unique array count: %lu",(unsigned long)[con_array count]);
                            
                            DebugLog(@"number of elements in part array are: %lu", (unsigned long)[part_array count]);
                            
                            NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:(NSArray *)part_array options:NSJSONWritingPrettyPrinted error:&err1];
                            NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
                            NSString *encodedText = [self encodeToPercentEscapeString:jsonString];
                            
                            DebugLog(@"jsonData Filter Prosy as string:\n%@", jsonString);
                            
                            
                            //===========================================ADD_FILTER_BACKUP======================================
                            
                            
                            NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=addfilterbackup&access_token=%@&device_id=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"access_token"],[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"]];   //&contacts=%@  ,[jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                            
                            DebugLog(@"ADD_FILTERBACKUP=========================================> %@",urlString1);
                            //            NSError *errorjs= nil;
                            
                            //        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
                            
                            //            NSString *firid = [NSString stringWithFormat:@"800"];
                            
                            responseData = [NSMutableData data];
                            
                            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString1]];
                            
                            NSString *params = [[NSString alloc] initWithFormat:@"contacts=%@",encodedText]; //[jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                            [request setHTTPMethod:@"POST"];
                            [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
                            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                            //            [[NSURLConnection alloc] initWithRequest:request delegate:self];
                            NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
                            
                            if(theConnection)
                            {
                                //                                responseData = [NSMutableData data];
                            }
                            else
                            {
                                DebugLog(@"theConnection is null");
                            }
                            
                        }
                    }
                    
                    @catch (NSException *exception) {
                        DebugLog(@"Exception: %@", exception.description);
                    }
                    @finally {
                    }
                }
                else
                {
                    DebugLog(@"removebackup not successful");
                }
                
                
                
            }
            
            //============================GET BACKUP===============================//
            
            
            //                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            //                    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=getbackup&access_token=%@&device_id=%@",[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
            //
            //                    DebugLog(@"profile url: %@",urlString1);
            //                    NSError *errorjs= nil;
            //
            //                    //        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
            //
            //                    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString1]];
            //
            //                    if (signeddataURL1 == nil)
            //                    {
            //                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
            //                                                                        message:nil
            //                                                                       delegate:self
            //                                                              cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            //                        alert.tag=8;
            //                        //                [alert show];
            //                    }
            //                    else
            //                    {
            //                        NSDictionary *json1aa = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
            //
            //                                                                                options:kNilOptions
            //
            //                                                                                  error:&errorjs];
            //                        //                DebugLog(@"this now has json string url: %@",json1aa);
            //
            //                        if ([[json1aa objectForKey:@"success"]intValue] == 1)
            //                        {
            //                            NSDictionary *contacts_temp_dict = [json1aa objectForKey:@"details"];
            //
            //                            for (NSDictionary *dict in [contacts_temp_dict objectForKey:@"contacts"])
            //                            {
            //                                [backuparr addObject:dict];
            //                            }
            //                            DebugLog(@"backuparr: %lu",(unsigned long)[backuparr count]);
            //
            //
            //#pragma mark Start of block consuming high memory
            //
            //                            [self myMethod:^(BOOL finished) {
            //                                if(finished){
            //                                    DebugLog(@"kaj hoechhe");
            //
            //                                    int finalwrite;
            //                                    for (finalwrite =0; finalwrite < [addtophonebook count]; finalwrite++)
            //                                    {
            //                                        NSDictionary *finalwritedict = [addtophonebook objectAtIndex:finalwrite];
            //                                        NSArray *phonesfinal = [[NSArray alloc]init];
            //                                        if (![[finalwritedict objectForKey:@"mobile_num"] isKindOfClass:[NSNull class]])
            //                                        {
            //                                            phonesfinal = [[finalwritedict objectForKey:@"mobile_num"] componentsSeparatedByString:@","];
            //                                            NSArray *fullnamearr = [[finalwritedict objectForKey:@"name"] componentsSeparatedByString:@" "];
            //
            //                                            NSString *petFirstName;
            //                                            NSString *petLastName;
            //                                            petLastName=@"";
            //                                            NSString *petPhoneNumber;
            //                                            petFirstName = [fullnamearr objectAtIndex:0];
            //                                            for (int ln=1; ln < [fullnamearr count]; ln++)
            //                                            {
            //                                                if (ln==1)
            //                                                    petLastName = [NSString stringWithFormat:@"%@",[fullnamearr objectAtIndex:ln]];
            //                                                else
            //                                                    petLastName = [NSString stringWithFormat:@"%@ %@",petLastName,[fullnamearr objectAtIndex:ln]];
            //                                            }
            //                                            petPhoneNumber = @"3331560987";
            //                                            ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, nil);
            //                                            ABRecordRef pet = ABPersonCreate();
            //                                            //                NSString *firstname=
            //                                            ABRecordSetValue(pet, kABPersonFirstNameProperty, (__bridge CFStringRef)petFirstName, nil);
            //                                            ABRecordSetValue(pet, kABPersonLastNameProperty, (__bridge CFStringRef)petLastName, nil);
            //
            //                                            ABMutableMultiValueRef phoneNumbers = ABMultiValueCreateMutable(kABMultiStringPropertyType);
            //                                            ABMultiValueAddValueAndLabel(phoneNumbers, (__bridge CFStringRef)[phonesfinal objectAtIndex:0], kABPersonPhoneMobileLabel, NULL);
            //                                            ABRecordSetValue(pet, kABPersonPhoneProperty, phoneNumbers, nil);
            //
            //                                            ABAddressBookAddRecord(addressBookRef, pet, nil);
            //
            //                                            NSArray *allContacts = (__bridge NSArray *)ABAddressBookCopyArrayOfAllPeople(addressBookRef);
            //                                            for (id record in allContacts){
            //                                                ABRecordRef thisContact = (__bridge ABRecordRef)record;
            //
            //                                                NSString *namefull = (__bridge NSString *)(ABRecordCopyCompositeName(thisContact));
            //                                                //                        DebugLog(@"namefull : %@ -- %lu",namefull,(unsigned long)[namefull length]);
            //                                                if ([namefull isKindOfClass:[NSNull class]] || namefull == (id)[NSNull null] || [namefull isEqualToString:@"(null)"] || [namefull length] == 0)
            //                                                {
            //                                                }
            //                                                else
            //                                                {
            //                                                    //                            DebugLog(@"etay etay");
            //                                                    if (CFStringCompare(ABRecordCopyCompositeName(thisContact),
            //                                                                        ABRecordCopyCompositeName(pet), 0) == kCFCompareEqualTo){
            //                                                        //The contact already exists!
            //                                                        //                                UIAlertView *contactExistsAlert = [[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"%@ %@ already exists", petFirstName,petLastName] message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            //                                                        //                                [contactExistsAlert show];
            //                                                        //                        return;
            //                                                    }
            //
            //                                                }
            //                                            }
            //                                            ABAddressBookSave(addressBookRef, nil);
            //                                        }
            //                                    }
            //                                }
            //                            }];
            
#pragma mark -- End of block consuming high memory
            //
            
            
            
            
        }
        @catch (NSException *exception) {
            DebugLog(@"Exception: %@", exception.description);
        }
        dispatch_async(dispatch_get_main_queue(), ^{});
        //                dispatch_async(dispatch_get_main_queue(), ^{
        //
        //                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        //                    [prefs setObject:@"yes" forKey:@"FromRestoreToBackup"];
        //                    [prefs synchronize];
        //
        //                    NSError *error = nil;
        //                    NSDictionary *json1;
        //                    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=contacts&amount=-1&id=%@&access_token=%@&device_id=%@",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
        //
        //                    DebugLog(@"profile url: %@",urlString1);
        //
        //                    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        //                    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
        //                    if (signeddataURL1 == nil)
        //                    {
        //                    }
        //                    else
        //                    {
        //                        json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
        //                                                                options:kNilOptions
        //                                                                  error:&error];
        //
        //                        NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
        //                        DebugLog(@"err  %@",errornumber);
        //
        //                        if (![errornumber isEqualToString:@"0"])
        //                        {
        //                            DebugLog(@"if if");
        //                            NSString *err_str = [json1 objectForKey:@"error"];
        //                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error in Retrieving Data!"
        //                                                                            message:err_str
        //                                                                           delegate:self
        //                                                                  cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        //                            alert.tag=200;
        //                        }
        //                        else
        //                        {
        //                            app_contacts = [[NSMutableArray alloc]init];
        //                            //        DebugLog(@"details req= %@",[json1 objectForKey:@"details"]);
        //                            if ([[json1 objectForKey:@"details"] isKindOfClass:[NSNull class]] || [json1 objectForKey:@"details"] == (id)[NSNull null] ||[[json1 objectForKey:@"details"] count] ==0)
        //                            {
        //                                DebugLog(@"App Contacts = 0");
        //                            }
        //                            else
        //                            {
        //                                NSDictionary *contacts_temp_dict = [json1 objectForKey:@"details"];
        //                                for (NSDictionary *dict in [contacts_temp_dict objectForKey:@"contacts"])
        //                                {
        //                                    [app_contacts addObject:dict];
        //                                }
        //                            }
        //                        }
        ////
        ////////////////////////////////////////////     Back Up er jonne eta     //////////////////////////////////////////////////////////////////////
        //
        //                        con_array =[[NSMutableArray alloc]init];
        //                        CFErrorRef *errorab = nil;
        //
        //                        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, errorab);
        //
        //                        __block BOOL accessGranted = NO;
        //
        //
        //                        if (ABAddressBookRequestAccessWithCompletion != NULL) { // we're on iOS 6
        //                            dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        //                            ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
        //                                accessGranted = granted;
        //                                dispatch_semaphore_signal(sema);
        //                            });
        //                            dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        //                            //        dispatch_release(sema);
        //                        }
        //                        else { // we're on iOS 5 or older
        //                            accessGranted = YES;
        //                        }
        //                        if (accessGranted) {
        //
        //                            NSMutableArray *contactpickarr = [[NSMutableArray alloc]init];
        //
        //                            ABAddressBookRef UsersAddressBook = ABAddressBookCreateWithOptions(NULL, NULL);
        //
        //                            //contains details for all the contacts
        //                            CFArrayRef ContactInfoArray = ABAddressBookCopyArrayOfAllPeople(UsersAddressBook);
        //
        //                            //get the total number of count of the users contact
        //                            CFIndex numberofPeople = CFArrayGetCount(ContactInfoArray);
        //                            DebugLog(@"%ld",numberofPeople);
        //                            con_array = [[NSMutableArray alloc]init];
        //
        //                            //iterate through each record and add the value in the array
        //                            for (int ik =0; ik<numberofPeople; ik++) {
        //                                ABRecordRef ref = CFArrayGetValueAtIndex(ContactInfoArray, ik);
        //                                ABMultiValueRef names = (__bridge ABMultiValueRef)((__bridge NSString*)ABRecordCopyValue(ref, kABPersonFirstNameProperty));
        //                                ABMultiValueRef lastnames = (__bridge ABMultiValueRef)((__bridge NSString*)ABRecordCopyValue(ref, kABPersonLastNameProperty));
        //
        //                                //        DebugLog(@"name from address book = %@ %@",names,lastnames);  // works fine.
        //                                NSString *fullname;
        //                                NSString *fnm = [NSString stringWithFormat:@"%@",names];
        //                                NSString *lnm = [NSString stringWithFormat:@"%@",lastnames];
        //                                //        DebugLog(@"fnm : %@",fnm);
        //                                if ([fnm length] ==0 || [fnm isKindOfClass:[NSNull class]] || fnm == (id)[NSNull null] || [fnm isEqualToString:@"(null)"])
        //                                    fullname = [NSString stringWithFormat:@"%@",lastnames];
        //
        //                                else if ([lnm length] ==0 || [lnm isKindOfClass:[NSNull class]] || lnm == (id)[NSNull null] || [lnm isEqualToString:@"(null)"])
        //                                    fullname = [NSString stringWithFormat:@"%@",names];
        //                                else
        //                                    fullname = [NSString stringWithFormat:@"%@ %@",names,lastnames];
        //
        //                                //        NSString *contactName = (__bridge NSString *)(names);
        //
        //                                [contactpickarr addObject:fullname];
        //
        //                                //            NSData  *imgData = (__bridge NSData *)ABPersonCopyImageData(ref);
        //                                //            UIImage  *img = [UIImage imageWithData:imgData];
        //                                //            DebugLog(@"Image : %@",img);
        //
        //                                ABMultiValueRef emailProperty = ABRecordCopyValue(ref, kABPersonEmailProperty);
        //
        //                                NSArray *emailArray = (__bridge NSArray *)ABMultiValueCopyArrayOfAllValues(emailProperty);
        //                                NSString *newstringemail= [emailArray componentsJoinedByString:@","];
        //
        //                                NSMutableDictionary *ns = [[NSMutableDictionary alloc]init];
        //
        //                                [ns setObject:fullname forKey:@"name"];
        //                                //            [ns setObject:fullname forKey:@"fullname"];
        //                                [ns setValue:newstringemail forKey:@"email"];
        //
        //                                if ([newstringemail isKindOfClass:[NSNull class]] || newstringemail == (id)[NSNull null] || [newstringemail length] < 4)
        //                                    [ns setValue:@"" forKey:@"email"];
        //
        //
        //                                ABMultiValueRef phoneNumberProperty = ABRecordCopyValue(ref, kABPersonPhoneProperty);
        //                                NSArray *_phoneNumbers = (__bridge NSArray*)ABMultiValueCopyArrayOfAllValues(phoneNumberProperty);
        //
        //                                NSMutableArray *mutableph= [_phoneNumbers mutableCopy];
        //                                int pp;
        //                                //                            DebugLog(@"mutable first: %@",mutableph);
        //                                for (pp =0; pp< [mutableph count]; pp++)
        //                                {
        //                                    if ([[mutableph objectAtIndex:pp] isKindOfClass:[NSNull class]] || [mutableph objectAtIndex:pp] == (id)[NSNull null] || [[mutableph objectAtIndex:pp] length] < 3)
        //                                    {
        //                                        [mutableph removeObjectAtIndex:pp];
        //                                        pp= pp-1;
        //                                    }
        //                                }
        //                                //                            DebugLog(@"mutable second: %@",mutableph);
        //                                NSString *newstringph= [mutableph componentsJoinedByString:@","];
        //
        //                                [ns setValue:newstringph forKey:@"phone"];
        //
        //
        //                                ABMultiValueRef phones = ABRecordCopyValue(ref, kABPersonPhoneProperty);
        //                                for(CFIndex j = 0; j < ABMultiValueGetCount(phones); j++)
        //                                {
        //                                    CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(phones, j);
        //                                    CFStringRef locLabel = ABMultiValueCopyLabelAtIndex(phones, j);
        //                                    NSString *phoneLabel =(__bridge NSString*) ABAddressBookCopyLocalizedLabel(locLabel);
        //                                    //CFRelease(phones);
        //                                    NSString *phoneNumber = (__bridge NSString *)phoneNumberRef;
        //                                    //CFRelease(phoneNumberRef);
        //                                    //CFRelease(locLabel);
        //                                    DebugLog(@"phno:  - %@ (%@)", phoneNumber, phoneLabel);
        //                                    [ns setValue:phoneNumber forKey:phoneLabel];
        //                                }
        //
        //                                //            NSMutableArray *addressarr = [[NSMutableArray alloc]init];
        //
        //                                //            ABMultiValueRef addrss = ABRecordCopyValue(ref, kABPersonAddressProperty);
        //                                //            for(CFIndex k = 0; k < ABMultiValueGetCount(addrss); k++)
        //                                //            {
        //                                ABMultiValueRef stt = ABRecordCopyValue(ref, kABPersonAddressProperty);
        //                                if (ABMultiValueGetCount(stt) > 0) {
        //                                    CFDictionaryRef dict = ABMultiValueCopyValueAtIndex(stt, 0);
        //                                    //                    DebugLog(@"dict address contacts: %@",dict);
        //                                    NSString *streetp = CFDictionaryGetValue(dict, kABPersonAddressStreetKey);
        //                                    NSString *cityp = CFDictionaryGetValue(dict, kABPersonAddressCityKey);
        //                                    NSString *zipcdep = CFDictionaryGetValue(dict, kABPersonAddressZIPKey);
        //                                    //                    NSString *statep = CFDictionaryGetValue(dict, kABPersonAddressStateKey);
        //                                    NSString *countryp = CFDictionaryGetValue(dict, kABPersonAddressCountryKey);
        //                                    //                    DebugLog(@"contacts phonebook address: %@ %@ %@ %@ %@",streetp,cityp,zipcdep,statep,countryp);
        //                                    //                    NSString *address = [NSString stringWithFormat:@"%@,%@,%@,%@,%@",streetp,cityp,zipcdep,statep,countryp];
        //                                    //                    [addressarr addObject:address];
        //
        //                                    if ([streetp isKindOfClass:[NSNull class]] || streetp == (id)[NSNull null])
        //                                        [ns setValue:@"" forKey:@"street"];
        //                                    else
        //                                        [ns setValue:streetp forKey:@"street"];
        //
        //                                    if ([cityp isKindOfClass:[NSNull class]] || cityp == (id)[NSNull null])
        //                                        [ns setValue:@"" forKey:@"city"];
        //                                    else
        //                                        [ns setValue:cityp forKey:@"city"];
        //
        //                                    if ([zipcdep isKindOfClass:[NSNull class]] || zipcdep == (id)[NSNull null])
        //                                        [ns setValue:@"" forKey:@"zip"];
        //                                    else
        //                                        [ns setValue:zipcdep forKey:@"street"];
        //
        //                                    if ([countryp isKindOfClass:[NSNull class]] || countryp == (id)[NSNull null])
        //                                        [ns setValue:@"" forKey:@"country"];
        //                                    else
        //                                        [ns setValue:countryp forKey:@"country"];
        //
        //                                    [ns setValue:@"" forKey:@"number"];
        //                                }
        //                                else
        //                                {
        //                                    [ns setValue:@"" forKey:@"number"];
        //                                    [ns setValue:@"" forKey:@"city"];
        //                                    [ns setValue:@"" forKey:@"street"];
        //                                    [ns setValue:@"" forKey:@"zip"];
        //                                    [ns setValue:@"" forKey:@"country"];
        //                                }
        //                                //            }
        //
        //                                //            [ns setObject:addressarr forKey:@"address"];
        //                                [con_array addObject:ns];
        //                            }
        //                        }
        //                        //    DebugLog(@"array content = %@", contactpickarr);
        //                        //                    DebugLog(@"array content part 2 = %@", con_array);
        //
        //
        //
        //                        check_app_cont = [[NSMutableArray alloc]init];
        //                        testarr = [con_array mutableCopy];
        //                        NSMutableArray *invite_array = [[NSMutableArray alloc]init];
        //                        int apps,dev;
        //                        int test;
        //                        test=0;
        ////                        NSString *conhome,*conmobile,*conwork; //*conmain,*coniphone,*conpager,*conother;
        //                        NSMutableDictionary *testdict;
        //
        //                        for (dev=0; dev< [testarr count]; dev++)
        //                        {
        //                            conhome = [[[[[testarr objectAtIndex:dev]objectForKey:@"home"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""] mutableCopy];
        //                            conmobile = [[[[[testarr objectAtIndex:dev]objectForKey:@"mobile"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""] mutableCopy];
        //                            conwork = [[[[[testarr objectAtIndex:dev]objectForKey:@"work"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""] mutableCopy];
        //
        //                            testdict = [testarr objectAtIndex:dev];
        //
        //                            for (apps=0; apps< [app_contacts count]; apps++)
        //                            {
        //                                //            if ([[con_array objectAtIndex:dev]objectForKey:@"home"])
        //                                //                {
        //                                NSString *homestr = [[[[app_contacts objectAtIndex:apps]objectForKey:@"mobile_num"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
        //
        //                                NSString *b_phn = [[[[app_contacts objectAtIndex:apps]objectForKey:@"b_phone_num"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
        //
        //                                NSString *landphnstr = [[[[app_contacts objectAtIndex:apps]objectForKey:@"phone_num"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
        //
        //                                if (([conhome longLongValue] != 0 && [homestr longLongValue] != 0 && [conhome rangeOfString:homestr].location != NSNotFound) || ([conhome longLongValue] != 0 && [b_phn longLongValue] != 0 && [conhome rangeOfString:b_phn].location != NSNotFound) || ([conmobile longLongValue] != 0 && [homestr longLongValue] != 0 && [conmobile rangeOfString:homestr].location != NSNotFound) || ([conmobile longLongValue] != 0 && [b_phn longLongValue] != 0 && [conmobile rangeOfString:b_phn].location != NSNotFound) || ([conwork longLongValue] != 0 && [homestr longLongValue] != 0 && [conwork rangeOfString:homestr].location != NSNotFound) || ([conwork longLongValue] != 0 && [b_phn longLongValue] != 0 && [conwork rangeOfString:b_phn].location != NSNotFound) || ([conhome longLongValue] != 0 && [landphnstr longLongValue] != 0 && [conhome rangeOfString:landphnstr].location != NSNotFound) || ([conmobile longLongValue] != 0 && [landphnstr longLongValue] != 0 && [conmobile rangeOfString:landphnstr].location != NSNotFound) || ([conwork longLongValue] != 0 && [landphnstr longLongValue] != 0 && [conwork rangeOfString:landphnstr].location != NSNotFound))
        //                                {
        //                                    //                        DebugLog(@"home no. not found");
        //                                    test=0;
        //                                    break;
        //                                }
        //                                else
        //                                {
        //                                    //                        DebugLog(@"home no. found");
        //                                    test=1;
        //                                    continue;
        //                                }
        //                            }
        //                            if (test ==1)
        //                            {
        //                                [testdict removeObjectForKey:@"home"];
        //                                [testdict removeObjectForKey:@"main"];
        //                                [testdict removeObjectForKey:@"mobile"];
        //                                [testdict removeObjectForKey:@"pager"];
        //                                [testdict removeObjectForKey:@"work"];
        //                                [testdict removeObjectForKey:@"other"];
        //
        //                                [invite_array addObject:testdict];
        //                            }
        //                        }
        //
        //                        uniquearray = [[[NSSet setWithArray:invite_array] allObjects] mutableCopy];
        //
        //                        //                    DebugLog(@"check contacts app has: %@",uniquearray);
        //                        if ([uniquearray isKindOfClass:[NSArray class]]) {
        //                            DebugLog(@"=====================> YES");
        //                        } else {
        //                            DebugLog(@"=====================> NO");
        //                        }
        //
        //                        for (int ij =0; ij< [uniquearray count]; ij++)
        //                        {
        //                            NSString *newString = [[[[uniquearray objectAtIndex:ij] objectForKey:@"name"] substringToIndex:1] lowercaseString];
        //
        //                            NSCharacterSet *strCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"];//1234567890_"];
        //
        //                            strCharSet = [strCharSet invertedSet];
        //                            //And you can then use a string method to find if your string contains anything in the inverted set:
        //                            NSString *strphones = [[uniquearray objectAtIndex:ij] objectForKey:@"phone"];
        //                            NSArray *numbersarray = [strphones componentsSeparatedByString:@","];
        //                            DebugLog(@"numbers array count: %lu",(unsigned long)[numbersarray count]);
        //
        //                            NSRange r = [newString rangeOfCharacterFromSet:strCharSet];
        //                            if (r.location != NSNotFound || [numbersarray count] == 0) {
        //                                DebugLog(@"the string contains illegal characters");
        //
        //                                [uniquearray removeObjectAtIndex:ij];
        //                                ij= ij-1;
        //                            }
        //                        }
        //
        //                        ConNewContactsViewController *con = [[ConNewContactsViewController alloc]init];
        //                        [con loadrequestsfromweb];
        //
        //                        //===================================================REMOVE BACKUP======================================//
        //
        //                        NSUserDefaults *prefsa = [NSUserDefaults standardUserDefaults];
        //                        NSError *errora;
        //                        NSDictionary *json1a;
        //                        NSString *urlString1a =[NSString stringWithFormat:@"https://budnav.com/ext/?action=removebackup&access_token=%@&device_id=%@",[prefsa objectForKey:@"access_token"],[prefsa objectForKey:@"deviceToken"]];
        //
        //                        DebugLog(@"removebackup url: %@",urlString1a);
        //
        //                        NSString *newString1a = [urlString1a stringByReplacingOccurrencesOfString:@" " withString:@""];
        //
        //                        NSData *signeddataURL1a =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1a]];
        //                        if (signeddataURL1a == nil)
        //                        {
        //                            DebugLog(@"removebackup data nil");
        //                        }
        //                        else
        //                        {
        //                            json1a = [NSJSONSerialization JSONObjectWithData:signeddataURL1a //1
        //
        //                                                                     options:kNilOptions
        //
        //                                                                       error:&errora];
        //                            if ([[json1a objectForKey:@"success"] intValue] == 1)
        //                            {
        //                                @try {
        //                                    NSError *err1 = nil;
        //
        //                                    DebugLog(@"unique array count: %lu",(unsigned long)[uniquearray count]);
        //                                    //                        NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"File" ofType:@"json"]];
        //                                    //                        NSError *localError = nil;
        //                                    //                        NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
        //                                    //                       NSArray *countriesList = (NSArray *)parsedObject;
        ////                                    int number_of_iterations;
        //
        //                                    part_array = [[NSMutableArray alloc]init];
        //                                    if ([uniquearray count] <= 300)
        //                                    {
        //                                        //                            part_array = [uniquearray mutableCopy];
        //                                        number_of_iterations=1;
        //                                    }
        //                                    else
        //                                    {
        //                                        if ([uniquearray count] % 300 == 0)
        //                                        {
        //                                            number_of_iterations = (int)[uniquearray count]/300;
        //                                        }
        //                                        else
        //                                        {
        //                                            number_of_iterations = (int)[uniquearray count]/300 +1 ;
        //                                        }
        //                                    }
        //                                    DebugLog(@"number of iterations are: %d", number_of_iterations);
        //
        //                                    for (int numb =0; numb < number_of_iterations; numb++)
        //                                    {
        //                                        part_array = [[NSMutableArray alloc]init];
        //
        //                                        if (numb == number_of_iterations -1)
        //                                        {
        //                                            NSArray *tempArray = [uniquearray subarrayWithRange:NSMakeRange(numb*300, [uniquearray count]-(300*numb))];
        //                                            [part_array addObjectsFromArray:tempArray];
        //                                        }
        //                                        else
        //                                        {
        //                                            NSArray *tempArray = [uniquearray subarrayWithRange:NSMakeRange(numb*300, 299)];
        //                                            [part_array addObjectsFromArray:tempArray];
        //                                        }
        //
        //                                        DebugLog(@"number of elements in part array are: %lu", (unsigned long)[part_array count]);
        //
        //                                        //=========================================================ADD BACKUP===========================================================//
        //
        //                                        NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:(NSArray *)part_array options:NSJSONWritingPrettyPrinted error:&err1];
        //                                        NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
        //                                        NSString *encodedText = [self encodeToPercentEscapeString:jsonString];
        //
        //                                        //                            DebugLog(@"jsonData as string:\n%@", jsonString);
        //
        //                                        NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=addbackuppart&access_token=%@&device_id=%@",[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];   //&contacts=%@  ,[jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
        //
        //                                        DebugLog(@"addbackuppart url: %@",urlString1);
        //                                        //            NSError *errorjs= nil;
        //
        //                                        //        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        //
        //                                        //            NSString *firid = [NSString stringWithFormat:@"800"];
        //
        //                                        responseData = [NSMutableData data];
        //
        //                                        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString1]];
        //
        //                                        NSString *params = [[NSString alloc] initWithFormat:@"contacts=%@",encodedText]; //[jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
        //                                        [request setHTTPMethod:@"POST"];
        //                                        [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
        //                                        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        //                                        //            [[NSURLConnection alloc] initWithRequest:request delegate:self];
        //                                        NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        //
        //                                        if(theConnection)
        //                                        {
        //                                            responseData = [NSMutableData data];
        //                                        }
        //                                        else
        //                                        {
        //                                            DebugLog(@"theConnection is null");
        //                                        }
        //                                    }
        //                                }
        //                                @catch (NSException *exception) {
        //                                    DebugLog(@"Exception: %@", exception.description);
        //                                }
        //                                @finally {
        //                                }
        //                            }
        //                            else
        //                            {
        //                                DebugLog(@"removebackup not successful");
        //                            }
        //                        }
        //                    }
        //                });
        
        
        //            });
#pragma Mark End of Adding back Up to Web
    }
}


- (MDRadialProgressView *)progressViewWithFrame:(CGRect)frame
{
    MDRadialProgressView *view = [[MDRadialProgressView alloc] initWithFrame:frame];
    // Only required in this demo to align vertically the progress views.
    view.center = CGPointMake(self.view.center.x, view.center.y+40);
    return view;
}

-(void)targetMethod: (NSTimer *)timer
{
    radialView3.progressCounter=radialView3.progressCounter+1;
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"FromRestoreToBackup"] isEqualToString:@"yes"])
    {
        syncstatus.text = @"Backing up...";
    }
    
    if (radialView3.progressCounter == 100)
    {
        [timer invalidate];
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"backupstatus"] isEqualToString:@"yes"])
        {
            [SVProgressHUD showSuccessWithStatus:@"Success!"];
            //            [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"firstContacts"];
            [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"backupstatus"];
            
            addtophonebook = nil;
            backuparr = nil;
            phnstr = nil;
            finalph = nil;
            conhome = nil;
            conmobile = nil;
            conwork = nil;
            testarr = nil;
            getbackupdict = nil;
        }
        [self dismissViewControllerAnimated:NO completion:nil];
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
    }
}


-(void) myMethod:(myCompletion) compblock{
    //do stuff
    DebugLog(@"do the stuff in this block");
    @autoreleasepool {
        
        //        UIBackgroundTaskIdentifier BGIdentifier = [[UIApplication sharedApplication]   beginBackgroundTaskWithExpirationHandler:^{}];
        
        testarr = [existingphn mutableCopy];
        addtophonebook = [[NSMutableArray alloc]init];
        int dev, apps;
        //        NSString *conhome,*conwork, *conmobile;
        //            NSMutableDictionary *testdict;
        int testback;
        
        for (apps=0; apps< [backuparr count]; apps++)
        {
            @autoreleasepool {
                
                testback = 0;
                getbackupdict=nil;
                phnstr= nil;
                backarrnumbers= nil;
                
                getbackupdict = [backuparr objectAtIndex:apps];
                phnstr = [NSMutableString stringWithFormat:@"%@",[[backuparr objectAtIndex:apps]objectForKey:@"mobile_num"]];
                phnstr = [[[phnstr componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""] mutableCopy];
                //            backarrnumbers = [phnstr componentsSeparatedByString:@","];
                //                    DebugLog(@"backarrnumbers: %@",backarrnumbers);
                int ph_int;
                ph_int = 0;
                //                    testdict = [testarr objectAtIndex:dev];
                
                //            for (ph_int =0; ph_int< [backarrnumbers count]; ph_int++)
                //            {
                //                finalph = nil;
                //
                //                finalph = [backarrnumbers objectAtIndex:ph_int];
                //                finalph = [[finalph componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
                //                if ([finalph length] > 6)
                //                    finalph = [finalph substringFromIndex:[finalph length] - 6];
                //
                //                //                        DebugLog(@"finalphone: %@ , == %ld",finalph,(long)[finalph intValue]);
                
                for (dev=0; dev< [testarr count]; dev++)
                {
                    @autoreleasepool
                    {
                        conhome= nil;
                        conmobile= nil;
                        conwork= nil;
                        
                        conhome = [NSMutableString stringWithFormat:@"%@",[[testarr objectAtIndex:dev]objectForKey:@"home"]];
                        
                        conmobile = [NSMutableString stringWithFormat:@"%@",[[testarr objectAtIndex:dev]objectForKey:@"mobile"]];
                        
                        conwork = [NSMutableString stringWithFormat:@"%@",[[testarr objectAtIndex:dev]objectForKey:@"work"]];
                        
                        if (([conhome longLongValue] != 0 && [conhome longLongValue] != 2147483647 && [phnstr rangeOfString:conhome].location != NSNotFound) || ([conmobile longLongValue] != 0 && [conmobile longLongValue] != 2147483647 && [phnstr rangeOfString:conmobile].location != NSNotFound) ||  ([conwork longLongValue] != 0 && [conwork longLongValue] != 2147483647 && [phnstr rangeOfString:conwork].location != NSNotFound))
                        {
                            testback=0;
                            break;
                        }
                        else
                        {
                            testback=1;
                            continue;
                        }
                        if (testback == 0)
                            break;
                        
                    } //autoreleasepool
                }
                
                if (testback ==1)
                {
                    [addtophonebook addObject:getbackupdict];
                    //                 break;
                }
            }//autoreleasepool
        }
        if ([testarr count] == 0)
            addtophonebook = [backuparr mutableCopy];
    }
    compblock(YES);
}


-(NSString*) encodeToPercentEscapeString:(NSString *)string {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                              (CFStringRef) string,
                                                              NULL,
                                                              (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
}


- (void)didReceiveMemoryWarning {
    
    DebugLog(@"hdjksfjsdfjkdsjkf -------- MEMORY WARNING");
    // Releases the view if it doesn't have a superview
    [super didReceiveMemoryWarning];
    addtophonebook = nil;
    backuparr = nil;
    phnstr = nil;
    finalph = nil;
    conhome = nil;
    conmobile = nil;
    conwork = nil;
    testarr = nil;
    getbackupdict = nil;
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data
{
    //    [responseData appendData:data];
    responseData = [data mutableCopy];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    DebugLog(@"connection failed");
    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"backupstatus"];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    DebugLog(@"connection finished loading %d", successTag);
    
    DebugLog(@"response data - %@", [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
    
    NSString *datatostring = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSError *err;
    
    NSDictionary *urgentdict = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&err];
    DebugLog(@"urgent dict gives: %@",urgentdict);
    
    //    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString1]];
    
    if (successTag == 900) {
        
        if (responseData != nil)
        {
            NSMutableDictionary  *json1aa=[urgentdict mutableCopy];
            
            DebugLog(@"this now has json string url: %@",json1aa);
            
            //           if ([json1aa objectForKey:@"success"])
            if([datatostring rangeOfString:@"\"success\":true"].location != NSNotFound)
            {
                connectionsuccess = connectionsuccess + 1;
            }
            DebugLog(@"connectionsuccess: %d ,,,, numberofterations: %d",connectionsuccess,number_of_iterations);
            if (connectionsuccess == number_of_iterations) {
                
                successTag = 100;
                //=========================================================GET FILTER BACKUP===============================================================//
                
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=getfilterbackup&access_token=%@&device_id=%@",[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
                
                DebugLog(@"GET_FILTER_BACKUP==================================> %@",urlString1);
                NSError *errorjs= nil;
                
                
                //        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
                
                NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString1]];
                
                if (signeddataURL1 == nil)
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                                                    message:nil
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                    alert.tag=8;
                    //                [alert show];
                }
                else
                {
                    NSDictionary *json1aa = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                             
                                                                            options:kNilOptions
                                             
                                                                              error:&errorjs];
                    //                DebugLog(@"this now has json string url: %@",json1aa);
                    
                    if ([[json1aa objectForKey:@"success"]intValue] == 1)
                    {
                        
                        NSDictionary *contacts_temp_dict = [json1aa objectForKey:@"details"];
                        
                        for (NSDictionary *dict in [contacts_temp_dict objectForKey:@"contacts"])
                        {
                            [backuparr addObject:dict];
                        }
                        DebugLog(@"backuparr: %lu",(unsigned long)[backuparr count]);
                        
                        
#pragma mark Start of block consuming high memory
                        
                        //                            [self myMethod:^(BOOL finished) {
                        //                                if(finished){
                        DebugLog(@"kaj hoechhe");
                        addtophonebook = [[NSMutableArray alloc]init];
                        addtophonebook = [backuparr mutableCopy];
                        int finalwrite;
                        for (finalwrite =0; finalwrite < [addtophonebook count]; finalwrite++)
                        {
                            NSDictionary *finalwritedict = [addtophonebook objectAtIndex:finalwrite];
                            NSArray *phonesfinal = [[NSArray alloc]init];
                            if (![[finalwritedict objectForKey:@"mobile_num"] isKindOfClass:[NSNull class]])
                            {
                                phonesfinal = [[finalwritedict objectForKey:@"mobile_num"] componentsSeparatedByString:@","];
                                NSArray *fullnamearr = [[finalwritedict objectForKey:@"name"] componentsSeparatedByString:@" "];
                                
                                NSString *petFirstName;
                                NSString *petLastName;
                                petLastName=@"";
                                NSString *petPhoneNumber;
                                petFirstName = [fullnamearr objectAtIndex:0];
                                for (int ln=1; ln < [fullnamearr count]; ln++)
                                {
                                    if (ln==1)
                                        petLastName = [NSString stringWithFormat:@"%@",[fullnamearr objectAtIndex:ln]];
                                    else
                                        petLastName = [NSString stringWithFormat:@"%@ %@",petLastName,[fullnamearr objectAtIndex:ln]];
                                }
                                petPhoneNumber = @"3331560987";
                                ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, nil);
                                ABRecordRef pet = ABPersonCreate();
                                //                NSString *firstname=
                                ABRecordSetValue(pet, kABPersonFirstNameProperty, (__bridge CFStringRef)petFirstName, nil);
                                ABRecordSetValue(pet, kABPersonLastNameProperty, (__bridge CFStringRef)petLastName, nil);
                                
                                ABMutableMultiValueRef phoneNumbers = ABMultiValueCreateMutable(kABMultiStringPropertyType);
                                ABMultiValueAddValueAndLabel(phoneNumbers, (__bridge CFStringRef)[phonesfinal objectAtIndex:0], kABPersonPhoneMobileLabel, NULL);
                                ABRecordSetValue(pet, kABPersonPhoneProperty, phoneNumbers, nil);
                                
                                ABAddressBookAddRecord(addressBookRef, pet, nil);
                                
                                NSArray *allContacts = (__bridge NSArray *)ABAddressBookCopyArrayOfAllPeople(addressBookRef);
                                for (id record in allContacts){
                                    ABRecordRef thisContact = (__bridge ABRecordRef)record;
                                    
                                    NSString *namefull = (__bridge NSString *)(ABRecordCopyCompositeName(thisContact));
                                    //                        DebugLog(@"namefull : %@ -- %lu",namefull,(unsigned long)[namefull length]);
                                    if ([namefull isKindOfClass:[NSNull class]] || namefull == (id)[NSNull null] || [namefull isEqualToString:@"(null)"] || [namefull length] == 0)
                                    {
                                    }
                                    else
                                    {
                                        //                            DebugLog(@"etay etay");
                                        if (CFStringCompare(ABRecordCopyCompositeName(thisContact),
                                                            ABRecordCopyCompositeName(pet), 0) == kCFCompareEqualTo){
                                            //The contact already exists!
                                            //                                UIAlertView *contactExistsAlert = [[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"%@ %@ already exists", petFirstName,petLastName] message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                            //                                [contactExistsAlert show];
                                            //                        return;
                                        }
                                        
                                    }
                                }
                                ABAddressBookSave(addressBookRef, nil);
                            }
                        }
                        
                        
                        //////////////////////////////////////////////   NOTUN BHABE DISPATCH MAIN ER CODE   //////////////////////////////////////////////////////
                        
                        
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        [prefs setObject:@"yes" forKey:@"FromRestoreToBackup"];
                        [prefs synchronize];
                        
                        NSError *error = nil;
                        NSDictionary *json1;
                        NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=contacts&amount=-1&id=%@&access_token=%@&device_id=%@",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
                        
                        DebugLog(@"profile url: %@",urlString1);
                        
                        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
                        NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
                        if (signeddataURL1 == nil)
                        {
                        }
                        else
                        {
                            json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                    options:kNilOptions
                                                                      error:&error];
                            
                            NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
                            DebugLog(@"err  %@",errornumber);
                            
                            if (![errornumber isEqualToString:@"0"])
                            {
                                DebugLog(@"if if");
                                NSString *err_str = [json1 objectForKey:@"error"];
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error in Retrieving Data!"
                                                                                message:err_str
                                                                               delegate:self
                                                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                                alert.tag=200;
                            }
                            else
                            {
                                app_contacts = [[NSMutableArray alloc]init];
                                //        DebugLog(@"details req= %@",[json1 objectForKey:@"details"]);
                                if ([[json1 objectForKey:@"details"] isKindOfClass:[NSNull class]] || [json1 objectForKey:@"details"] == (id)[NSNull null] ||[[json1 objectForKey:@"details"] count] ==0)
                                {
                                    DebugLog(@"App Contacts = 0");
                                }
                                else
                                {
                                    NSDictionary *contacts_temp_dict = [json1 objectForKey:@"details"];
                                    for (NSDictionary *dict in [contacts_temp_dict objectForKey:@"contacts"])
                                    {
                                        [app_contacts addObject:dict];
                                    }
                                }
                            }
                            //
                            //////////////////////////////////////////     Back Up er jonne eta     //////////////////////////////////////////////////////////////////////
                            
                            con_array =[[NSMutableArray alloc]init];
                            CFErrorRef *errorab = nil;
                            
                            ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, errorab);
                            
                            __block BOOL accessGranted = NO;
                            
                            
                            if (&ABAddressBookRequestAccessWithCompletion != NULL) { // we're on iOS 6
                                dispatch_semaphore_t sema = dispatch_semaphore_create(0);
                                ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
                                    accessGranted = granted;
                                    dispatch_semaphore_signal(sema);
                                });
                                dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
                                //        dispatch_release(sema);
                            }
                            else { // we're on iOS 5 or older
                                accessGranted = YES;
                            }
                            if (accessGranted) {
                                
                                NSMutableArray *contactpickarr = [[NSMutableArray alloc]init];
                                
                                ABAddressBookRef UsersAddressBook = ABAddressBookCreateWithOptions(NULL, NULL);
                                
                                //contains details for all the contacts
                                CFArrayRef ContactInfoArray = ABAddressBookCopyArrayOfAllPeople(UsersAddressBook);
                                
                                //get the total number of count of the users contact
                                CFIndex numberofPeople = CFArrayGetCount(ContactInfoArray);
                                DebugLog(@"%ld",numberofPeople);
                                con_array = [[NSMutableArray alloc]init];
                                
                                //iterate through each record and add the value in the array
                                for (int ik =0; ik<numberofPeople; ik++) {
                                    ABRecordRef ref = CFArrayGetValueAtIndex(ContactInfoArray, ik);
                                    ABMultiValueRef names = (__bridge ABMultiValueRef)((__bridge NSString*)ABRecordCopyValue(ref, kABPersonFirstNameProperty));
                                    ABMultiValueRef lastnames = (__bridge ABMultiValueRef)((__bridge NSString*)ABRecordCopyValue(ref, kABPersonLastNameProperty));
                                    
                                    //        DebugLog(@"name from address book = %@ %@",names,lastnames);  // works fine.
                                    NSString *fullname;
                                    NSString *fnm = [NSString stringWithFormat:@"%@",names];
                                    NSString *lnm = [NSString stringWithFormat:@"%@",lastnames];
                                    //        DebugLog(@"fnm : %@",fnm);
                                    if ([fnm length] ==0 || [fnm isKindOfClass:[NSNull class]] || fnm == (id)[NSNull null] || [fnm isEqualToString:@"(null)"])
                                        fullname = [NSString stringWithFormat:@"%@",lastnames];
                                    
                                    else if ([lnm length] ==0 || [lnm isKindOfClass:[NSNull class]] || lnm == (id)[NSNull null] || [lnm isEqualToString:@"(null)"])
                                        fullname = [NSString stringWithFormat:@"%@",names];
                                    else
                                        fullname = [NSString stringWithFormat:@"%@ %@",names,lastnames];
                                    
                                    //        NSString *contactName = (__bridge NSString *)(names);
                                    
                                    [contactpickarr addObject:fullname];
                                    
                                    //            NSData  *imgData = (__bridge NSData *)ABPersonCopyImageData(ref);
                                    //            UIImage  *img = [UIImage imageWithData:imgData];
                                    //            DebugLog(@"Image : %@",img);
                                    
                                    ABMultiValueRef emailProperty = ABRecordCopyValue(ref, kABPersonEmailProperty);
                                    
                                    NSArray *emailArray = (__bridge NSArray *)ABMultiValueCopyArrayOfAllValues(emailProperty);
                                    NSString *newstringemail= [emailArray componentsJoinedByString:@","];
                                    
                                    NSMutableDictionary *ns = [[NSMutableDictionary alloc]init];
                                    
                                    [ns setObject:fullname forKey:@"name"];
                                    //            [ns setObject:fullname forKey:@"fullname"];
                                    [ns setValue:newstringemail forKey:@"email"];
                                    
                                    if ([newstringemail isKindOfClass:[NSNull class]] || newstringemail == (id)[NSNull null] || [newstringemail length] < 4)
                                        [ns setValue:@"" forKey:@"email"];
                                    
                                    
                                    ABMultiValueRef phoneNumberProperty = ABRecordCopyValue(ref, kABPersonPhoneProperty);
                                    NSArray *_phoneNumbers = (__bridge NSArray*)ABMultiValueCopyArrayOfAllValues(phoneNumberProperty);
                                    
                                    NSMutableArray *mutableph= [_phoneNumbers mutableCopy];
                                    int pp;
                                    //                            DebugLog(@"mutable first: %@",mutableph);
                                    for (pp =0; pp< [mutableph count]; pp++)
                                    {
                                        if ([[mutableph objectAtIndex:pp] isKindOfClass:[NSNull class]] || [mutableph objectAtIndex:pp] == (id)[NSNull null] || [[mutableph objectAtIndex:pp] length] < 3)
                                        {
                                            [mutableph removeObjectAtIndex:pp];
                                            pp= pp-1;
                                        }
                                    }
                                    //                            DebugLog(@"mutable second: %@",mutableph);
                                    NSString *newstringph= [mutableph componentsJoinedByString:@","];
                                    
                                    [ns setValue:newstringph forKey:@"phone"];
                                    
                                    
                                    ABMultiValueRef phones = ABRecordCopyValue(ref, kABPersonPhoneProperty);
                                    for(CFIndex j = 0; j < ABMultiValueGetCount(phones); j++)
                                    {
                                        CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(phones, j);
                                        CFStringRef locLabel = ABMultiValueCopyLabelAtIndex(phones, j);
                                        NSString *phoneLabel =(__bridge NSString*) ABAddressBookCopyLocalizedLabel(locLabel);
                                        //CFRelease(phones);
                                        NSString *phoneNumber = (__bridge NSString *)phoneNumberRef;
                                        //CFRelease(phoneNumberRef);
                                        //CFRelease(locLabel);
                                        DebugLog(@"phno:  - %@ (%@)", phoneNumber, phoneLabel);
                                        [ns setValue:phoneNumber forKey:phoneLabel];
                                    }
                                    
                                    //            NSMutableArray *addressarr = [[NSMutableArray alloc]init];
                                    
                                    //            ABMultiValueRef addrss = ABRecordCopyValue(ref, kABPersonAddressProperty);
                                    //            for(CFIndex k = 0; k < ABMultiValueGetCount(addrss); k++)
                                    //            {
                                    ABMultiValueRef stt = ABRecordCopyValue(ref, kABPersonAddressProperty);
                                    if (ABMultiValueGetCount(stt) > 0) {
                                        CFDictionaryRef dict = ABMultiValueCopyValueAtIndex(stt, 0);
                                        //                    DebugLog(@"dict address contacts: %@",dict);
                                        NSString *streetp = CFDictionaryGetValue(dict, kABPersonAddressStreetKey);
                                        NSString *cityp = CFDictionaryGetValue(dict, kABPersonAddressCityKey);
                                        NSString *zipcdep = CFDictionaryGetValue(dict, kABPersonAddressZIPKey);
                                        //                    NSString *statep = CFDictionaryGetValue(dict, kABPersonAddressStateKey);
                                        NSString *countryp = CFDictionaryGetValue(dict, kABPersonAddressCountryKey);
                                        //                    DebugLog(@"contacts phonebook address: %@ %@ %@ %@ %@",streetp,cityp,zipcdep,statep,countryp);
                                        //                    NSString *address = [NSString stringWithFormat:@"%@,%@,%@,%@,%@",streetp,cityp,zipcdep,statep,countryp];
                                        //                    [addressarr addObject:address];
                                        
                                        if ([streetp isKindOfClass:[NSNull class]] || streetp == (id)[NSNull null])
                                            [ns setValue:@"" forKey:@"street"];
                                        else
                                            [ns setValue:streetp forKey:@"street"];
                                        
                                        if ([cityp isKindOfClass:[NSNull class]] || cityp == (id)[NSNull null])
                                            [ns setValue:@"" forKey:@"city"];
                                        else
                                            [ns setValue:cityp forKey:@"city"];
                                        
                                        if ([zipcdep isKindOfClass:[NSNull class]] || zipcdep == (id)[NSNull null])
                                            [ns setValue:@"" forKey:@"zip"];
                                        else
                                            [ns setValue:zipcdep forKey:@"street"];
                                        
                                        if ([countryp isKindOfClass:[NSNull class]] || countryp == (id)[NSNull null])
                                            [ns setValue:@"" forKey:@"country"];
                                        else
                                            [ns setValue:countryp forKey:@"country"];
                                        
                                        [ns setValue:@"" forKey:@"number"];
                                    }
                                    else
                                    {
                                        [ns setValue:@"" forKey:@"number"];
                                        [ns setValue:@"" forKey:@"city"];
                                        [ns setValue:@"" forKey:@"street"];
                                        [ns setValue:@"" forKey:@"zip"];
                                        [ns setValue:@"" forKey:@"country"];
                                    }
                                    //            }
                                    
                                    //            [ns setObject:addressarr forKey:@"address"];
                                    [con_array addObject:ns];
                                }
                            }
                            //    DebugLog(@"array content = %@", contactpickarr);
                            //                    DebugLog(@"array content part 2 = %@", con_array);
                            
                            
                            
                            check_app_cont = [[NSMutableArray alloc]init];
                            testarr = [con_array mutableCopy];
                            NSMutableArray *invite_array = [[NSMutableArray alloc]init];
                            int apps,dev;
                            int test;
                            test=0;
                            //                        NSString *conhome,*conmobile,*conwork; //*conmain,*coniphone,*conpager,*conother;
                            NSMutableDictionary *testdict;
                            
                            for (dev=0; dev< [testarr count]; dev++)
                            {
                                conhome = [[[[[testarr objectAtIndex:dev]objectForKey:@"home"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""] mutableCopy];
                                conmobile = [[[[[testarr objectAtIndex:dev]objectForKey:@"mobile"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""] mutableCopy];
                                conwork = [[[[[testarr objectAtIndex:dev]objectForKey:@"work"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""] mutableCopy];
                                
                                testdict = [testarr objectAtIndex:dev];
                                
                                for (apps=0; apps< [app_contacts count]; apps++)
                                {
                                    //            if ([[con_array objectAtIndex:dev]objectForKey:@"home"])
                                    //                {
                                    NSString *homestr = [[[[app_contacts objectAtIndex:apps]objectForKey:@"mobile_num"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
                                    
                                    NSString *b_phn = [[[[app_contacts objectAtIndex:apps]objectForKey:@"b_phone_num"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
                                    
                                    NSString *landphnstr = [[[[app_contacts objectAtIndex:apps]objectForKey:@"phone_num"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
                                    
                                    if (([conhome longLongValue] != 0 && [homestr longLongValue] != 0 && [conhome rangeOfString:homestr].location != NSNotFound) || ([conhome longLongValue] != 0 && [b_phn longLongValue] != 0 && [conhome rangeOfString:b_phn].location != NSNotFound) || ([conmobile longLongValue] != 0 && [homestr longLongValue] != 0 && [conmobile rangeOfString:homestr].location != NSNotFound) || ([conmobile longLongValue] != 0 && [b_phn longLongValue] != 0 && [conmobile rangeOfString:b_phn].location != NSNotFound) || ([conwork longLongValue] != 0 && [homestr longLongValue] != 0 && [conwork rangeOfString:homestr].location != NSNotFound) || ([conwork longLongValue] != 0 && [b_phn longLongValue] != 0 && [conwork rangeOfString:b_phn].location != NSNotFound) || ([conhome longLongValue] != 0 && [landphnstr longLongValue] != 0 && [conhome rangeOfString:landphnstr].location != NSNotFound) || ([conmobile longLongValue] != 0 && [landphnstr longLongValue] != 0 && [conmobile rangeOfString:landphnstr].location != NSNotFound) || ([conwork longLongValue] != 0 && [landphnstr longLongValue] != 0 && [conwork rangeOfString:landphnstr].location != NSNotFound))
                                    {
                                        //                        DebugLog(@"home no. not found");
                                        test=0;
                                        break;
                                    }
                                    else
                                    {
                                        //                        DebugLog(@"home no. found");
                                        test=1;
                                        continue;
                                    }
                                }
                                if (test ==1)
                                {
                                    [testdict removeObjectForKey:@"home"];
                                    [testdict removeObjectForKey:@"main"];
                                    [testdict removeObjectForKey:@"mobile"];
                                    [testdict removeObjectForKey:@"pager"];
                                    [testdict removeObjectForKey:@"work"];
                                    [testdict removeObjectForKey:@"other"];
                                    
                                    [invite_array addObject:testdict];
                                }
                            }
                            
                            uniquearray = [[[NSSet setWithArray:invite_array] allObjects] mutableCopy];
                            
                            //                    DebugLog(@"check contacts app has: %@",uniquearray);
                            if ([uniquearray isKindOfClass:[NSArray class]]) {
                                DebugLog(@"=====================> YES");
                            } else {
                                DebugLog(@"=====================> NO");
                            }
                            
                            for (int ij =0; ij< [uniquearray count]; ij++)
                            {
                                NSString *newString = [[[[uniquearray objectAtIndex:ij] objectForKey:@"name"] substringToIndex:1] lowercaseString];
                                
                                NSCharacterSet *strCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"];//1234567890_"];
                                
                                strCharSet = [strCharSet invertedSet];
                                //And you can then use a string method to find if your string contains anything in the inverted set:
                                NSString *strphones = [[uniquearray objectAtIndex:ij] objectForKey:@"phone"];
                                NSArray *numbersarray = [strphones componentsSeparatedByString:@","];
                                DebugLog(@"numbers array count: %lu",(unsigned long)[numbersarray count]);
                                
                                NSRange r = [newString rangeOfCharacterFromSet:strCharSet];
                                if (r.location != NSNotFound || [numbersarray count] == 0) {
                                    DebugLog(@"the string contains illegal characters");
                                    
                                    [uniquearray removeObjectAtIndex:ij];
                                    ij= ij-1;
                                }
                            }
                            
                            ConNewContactsViewController *con = [[ConNewContactsViewController alloc]init];
                            [con loadContactsFromWeb];
                            
                            //===================================================REMOVE BACKUP======================================//
                            
                            NSUserDefaults *prefsa = [NSUserDefaults standardUserDefaults];
                            NSError *errora;
                            NSDictionary *json1a;
                            NSString *urlString1a =[NSString stringWithFormat:@"https://budnav.com/ext/?action=removebackup&access_token=%@&device_id=%@",[prefsa objectForKey:@"access_token"],[prefsa objectForKey:@"deviceToken"]];
                            
                            DebugLog(@"removebackup url: %@",urlString1a);
                            
                            NSString *newString1a = [urlString1a stringByReplacingOccurrencesOfString:@" " withString:@""];
                            
                            NSData *signeddataURL1a =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1a]];
                            if (signeddataURL1a == nil)
                            {
                                DebugLog(@"removebackup data nil");
                            }
                            else
                            {
                                json1a = [NSJSONSerialization JSONObjectWithData:signeddataURL1a //1
                                          
                                                                         options:kNilOptions
                                          
                                                                           error:&errora];
                                if ([[json1a objectForKey:@"success"] intValue] == 1)
                                {
                                    @try {
                                        NSError *err1 = nil;
                                        
                                        DebugLog(@"unique array count: %lu",(unsigned long)[uniquearray count]);
                                        //                        NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"File" ofType:@"json"]];
                                        //                        NSError *localError = nil;
                                        //                        NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
                                        //                       NSArray *countriesList = (NSArray *)parsedObject;
                                        //                                    int number_of_iterations;
                                        
                                        part_array = [[NSMutableArray alloc]init];
                                        if ([uniquearray count] <= 300)
                                        {
                                            //                            part_array = [uniquearray mutableCopy];
                                            number_of_iterations=1;
                                        }
                                        else
                                        {
                                            if ([uniquearray count] % 300 == 0)
                                            {
                                                number_of_iterations = (int)[uniquearray count]/300;
                                            }
                                            else
                                            {
                                                number_of_iterations = (int)[uniquearray count]/300 +1 ;
                                            }
                                        }
                                        DebugLog(@"number of iterations are: %d", number_of_iterations);
                                        
                                        for (int numb =0; numb < number_of_iterations; numb++)
                                        {
                                            part_array = [[NSMutableArray alloc]init];
                                            
                                            if (numb == number_of_iterations -1)
                                            {
                                                NSArray *tempArray = [uniquearray subarrayWithRange:NSMakeRange(numb*300, [uniquearray count]-(300*numb))];
                                                [part_array addObjectsFromArray:tempArray];
                                            }
                                            else
                                            {
                                                NSArray *tempArray = [uniquearray subarrayWithRange:NSMakeRange(numb*300, 299)];
                                                [part_array addObjectsFromArray:tempArray];
                                            }
                                            
                                            DebugLog(@"number of elements in part array are: %lu", (unsigned long)[part_array count]);
                                            
                                            //=========================================================ADD BACKUP===========================================================//
                                            
                                            NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:(NSArray *)part_array options:NSJSONWritingPrettyPrinted error:&err1];
                                            NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
                                            NSString *encodedText = [self encodeToPercentEscapeString:jsonString];
                                            
                                            //                            DebugLog(@"jsonData as string:\n%@", jsonString);
                                            
                                            NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=addbackuppart&access_token=%@&device_id=%@",[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];   //&contacts=%@  ,[jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                                            
                                            DebugLog(@"addbackuppart url: %@",urlString1);
                                            //            NSError *errorjs= nil;
                                            
                                            //        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
                                            
                                            //            NSString *firid = [NSString stringWithFormat:@"800"];
                                            
                                            responseData = [NSMutableData data];
                                            
                                            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString1]];
                                            
                                            NSString *params = [[NSString alloc] initWithFormat:@"contacts=%@",encodedText]; //[jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                                            [request setHTTPMethod:@"POST"];
                                            [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
                                            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                                            //            [[NSURLConnection alloc] initWithRequest:request delegate:self];
                                            NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
                                            
                                            if(theConnection)
                                            {
                                                responseData = [NSMutableData data];
                                            }
                                            else
                                            {
                                                DebugLog(@"theConnection is null");
                                            }
                                        }
                                    }
                                    @catch (NSException *exception) {
                                        DebugLog(@"Exception: %@", exception.description);
                                    }
                                    @finally {
                                    }
                                }
                                else
                                {
                                    DebugLog(@"removebackup not successful");
                                }
                            }
                        }
                    }
                }
                
            }
            
            else
            {
                NSString *err_str = [json1aa objectForKey:@"error"];
                DebugLog(@"error backing up: %@",err_str);
            }
            //            [SVProgressHUD showSuccessWithStatus:@"Success!"];
        }
        else
        {
        }
        
    }else{
        
        NSString *datatostring = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        
        if (responseData != nil)
        {
            NSDictionary  *json1aa=[NSJSONSerialization JSONObjectWithData:responseData options:0 error:&err];
            
            DebugLog(@"this now has json string url: %@",json1aa);
            
            //            if ([[json1aa objectForKey:@"success"] intValue] ==1)
            if([datatostring rangeOfString:@"\"success\":true"].location != NSNotFound)
            {
                [logoimg removeFromSuperview];
                //             [SVProgressHUD showSuccessWithStatus:@"Success!"];
                [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"backupstatus"];
            }
            else
            {
                [logoimg removeFromSuperview];
                NSString *err_str = [json1aa objectForKey:@"error"];
                DebugLog(@"error backing up: %@",err_str);
            }
            //            [SVProgressHUD showSuccessWithStatus:@"Success!"];
        }
        else
        {
            [logoimg removeFromSuperview];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                                            message:nil
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            alert.tag=7;
            //        [alert show];
        }
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    responseData = [NSMutableData new];
}
@end