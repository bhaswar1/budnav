//
//  ConCodeViewController.h
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 06/05/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface ConCodeViewController : GAITrackedViewController<UITextFieldDelegate,UIAlertViewDelegate,UIGestureRecognizerDelegate>

@property (strong, nonatomic) NSString *mob_num;
@end