//
//  AppDelegate.m
//  Budnav
//
//  Created by Soumarsi Kundu on 30/01/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "AppDelegate.h"
#import "ConNewRequestsViewController.h"
#import "ConLoginViewController.h"
#import "ConAddFriendViewController.h"
#import "ConPreSignUpViewController.h"
#import "ConProfileOwnViewController.h"
#import "ConCodeViewController.h"
#import "AMScanViewController.h"
#import "ConOptionsPicViewController.h"
#import "ConRequestViewController.h"
#import "ConSettingsView.h"
#import "SCViewController.h"
#import "ConMapViewController.h"
#import "ConQROptionsViewController.h"
#import "REVMapViewController.h"
#import "ConGoogleMapViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import <sqlite3.h>
#import "DBManager.h"
#import "ConNewContactsViewController.h"
#import "iToast.h"
#import "UIView+Toast.h"
#import "ConGroupsViewController.h"
#import "ConFullSettingsViewController.h"
#import "ConHomeViewController.h"
#import "ConAccountSettingsViewController.h"
#import "ConNewProfileViewController.h"
#import "ConNewDetailsViewController.h"
#import "GroupDetailsViewController.h"
#import "ConLocateGroupViewController.h"
#import "GAI.h"
#import "ConInviteViewController.h"
#import "ConPersonalProfileViewController.h"

@implementation AppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize navigationController,tabBarController,dbstatus,arr_contacts_ad,writetodbcount,recom_search_array,recom_search_string, pushtoprofileid,Bhaswar_array, uniquearray_new, part_array, newarr_contacts_ad,scrollsection, scrollrow, device_phonebook_array, scrolloffsety, now_Bhaswar_array, business_dict, personal_dict, social_dict, profdict;

static NSString *const kAPIKey = @"AIzaSyBe-FEpwb3T3A-uDqjnbGu1hX3i2eOFoKI";
NSMutableArray *newcontactsapp, *deleted_contact;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    if ([application respondsToSelector:@selector(registerForRemoteNotifications)]) {
        
        
        
        //////////////////////////////////////// Notification with Options /////////////////////////////////////////
        
        NSString * const NotificationCategoryIdent  = @"Monthly_Notification";
        NSString * const NotificationActionOneIdent = @"ACTION_YES";
        NSString * const NotificationActionTwoIdent = @"ACTION_NO";
        
        
        
        UIMutableUserNotificationAction *action1;
        action1 = [[UIMutableUserNotificationAction alloc] init];
        [action1 setActivationMode:UIUserNotificationActivationModeBackground];
        [action1 setTitle:@"YES"];
        [action1 setIdentifier:NotificationActionOneIdent];
        [action1 setDestructive:YES];
        [action1 setAuthenticationRequired:NO];
        
        UIMutableUserNotificationAction *action2;
        action2 = [[UIMutableUserNotificationAction alloc] init];
        [action2 setActivationMode:UIUserNotificationActivationModeForeground];
        [action2 setTitle:@"NO"];
        [action2 setIdentifier:NotificationActionTwoIdent];
        [action2 setDestructive:NO];
        [action2 setAuthenticationRequired:NO];
        
        UIMutableUserNotificationCategory *actionCategory;
        actionCategory = [[UIMutableUserNotificationCategory alloc] init];
        [actionCategory setIdentifier:NotificationCategoryIdent];
        [actionCategory setActions:@[action1, action2]
                        forContext:UIUserNotificationActionContextDefault];
        
        NSSet *categories = [NSSet setWithObject:actionCategory];
        UIUserNotificationType types = (UIUserNotificationTypeAlert|
                                        UIUserNotificationTypeSound|
                                        UIUserNotificationTypeBadge);
        
        UIUserNotificationSettings *settings;
        settings = [UIUserNotificationSettings settingsForTypes:types
                                                     categories:categories];
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        
        
        
        
//        // Register device for iOS8
//        
//        UIUserNotificationSettings *notificationSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil];
//        
//        [application registerUserNotificationSettings:notificationSettings];
//        
//        [application registerForRemoteNotifications];
//        
//        
//        //        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:@"registerForRemoteNotifications ios8" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//        
//        //
//        
//        //        [alert show];
        
    } else {
        
        // Register device for iOS7
        
        [application registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeBadge];
    }
    
    
    
   
    
    

    
    //#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
    //    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)])
    //    {
    //        // use registerUserNotificationSettings
    //        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
    //
    //        [application registerForRemoteNotifications];
    //    }
    //    else
    //    {
    //        // use registerForRemoteNotifications
    //        [[UIApplication sharedApplication] registerForRemoteNotifications];
    //
    //    }
    //
    //
    //#else
    //    {
    //        [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge |UIRemoteNotificationTypeSound)];
    //    }
    //#endif
    
    
    self.window.backgroundColor = [UIColor whiteColor];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    ConHomeViewController *homeView = [[ConHomeViewController alloc] init];
    navigationController = [[UINavigationController alloc] initWithRootViewController:homeView];
    navigationController.navigationBar.hidden= YES;
    self.window.rootViewController = navigationController;
    [self.window makeKeyAndVisible];
    dbstatusq=0;
    
    
    
    [GMSServices provideAPIKey:@"AIzaSyBe-FEpwb3T3A-uDqjnbGu1hX3i2eOFoKI"];
    [self createEditableCopyOfDatabaseIfNeeded];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-70494985-1"];
    
    // Provide unhandled exceptions reports.
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // Enable Remarketing, Demographics & Interests reports. Requires the libAdIdAccess library
    // and the AdSupport framework.
    // https://developers.google.com/analytics/devguides/collection/ios/display-features
    tracker.allowIDFACollection = YES;
    
    [[GAI sharedInstance].logger setLogLevel:kGAILogLevelVerbose];
    
    [GAI sharedInstance].dispatchInterval = 20;
    
    _selected_array = [[NSMutableArray alloc] init];
    
//    timer = [NSTimer scheduledTimerWithTimeInterval:30.0f target:self
//                                           selector:@selector(sendingRequest)
//                                           userInfo:nil
//                                            repeats:NO];
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];
    
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [FBSDKAppEvents activateApp];
    
    
}

#pragma mark - Push Notification
//#ifdef __IPHONE_8_0
//
//- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
//
//{
//
//    //register to receive notifications
//
//    [application registerForRemoteNotifications];
//
//}
//
//
//
//- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
//
//{
//    //handle the actions
//
//    DebugLog(@"handle action");
//
//    if ([identifier isEqualToString:@"declineAction"])
//    {
//
//    }
//
//    else if ([identifier isEqualToString:@"answerAction"])
//    {
//
//    }
//
//}
//
//#endif

- (void)applicationWillResignActive:(UIApplication *)application
{
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.window endEditing:YES];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    //DebugLog(@"sourav");
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    
}


- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    //[self saveContext];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[DBManager getSharedInstance]closeDB];
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


-(void)showTabStatus:(BOOL)status
{
    UIViewController *new_settings_tab = nil;
    UIViewController *contacts_tab = nil;
    UIViewController *requests_tab = nil;
    UIViewController *map_tab = nil;
    UIViewController *groups_tab = nil;
    
    //=======================BELOW CHANGED BY ME==================//
    
    [[UITabBar appearance] setTintColor:[UIColor colorWithRed:49.0f/255.0f green:170.0f/255.0f blue:184.0f/255.0f alpha:1]];
    
    
    //=================FOR TAB BAR FONT HIGHLIGHTED=============//
    
    [[UITabBarItem appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor clearColor], NSForegroundColorAttributeName,
      [UIFont fontWithName:@"ProximaNova-Regular" size:13.0], NSFontAttributeName,
      nil] forState:UIControlStateHighlighted];
    
    //================FOR TAB BAR FONT SELECTED=================//
    
    [[UITabBarItem appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor colorWithRed:192.0f/255.0f green:250.0f/255.0f blue:193.0f/255.0f alpha:1], NSForegroundColorAttributeName,
      [UIFont fontWithName:@"ProximaNova-Regular" size:13.0], NSFontAttributeName,
      nil] forState:UIControlStateSelected];
    
    //===============FOR TAB BAR FONT NORMAL===================//
    //[[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    
    [[UITabBarItem appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor whiteColor], NSForegroundColorAttributeName ,
      [UIFont fontWithName:@"ProximaNova-Regular" size:13.0], NSFontAttributeName,
      nil] forState:UIControlStateNormal];  //colorWithRed:.5 green:.5 blue:.5 alpha:1
    
    
    [[UITabBar appearance] setBackgroundColor:[UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:1]];
    [[UITabBar appearance] setBarTintColor:[UIColor whiteColor]];
    
    if(status)
    {
        if(self.tabBarController==nil)
        {
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            [prefs synchronize];
            
            self.tabBarController = [[UITabBarController alloc] init];
            self.tabBarController.delegate = self;
            
            new_settings_tab = [[ConFullSettingsViewController alloc] initWithNibName:nil bundle:nil];
            new_settings_tab.edgesForExtendedLayout = YES;
            
            [new_settings_tab.tabBarItem setImageInsets:UIEdgeInsetsMake(5, 0, -5, 0)];
            new_settings_tab.tabBarItem.image = [[UIImage imageNamed:@"new_setting"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            new_settings_tab.tabBarItem.selectedImage = [[UIImage imageNamed:@"new_setting(active)"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            UINavigationController *settings_nav_controller = [[UINavigationController alloc] initWithRootViewController:new_settings_tab];
            settings_nav_controller.navigationController.navigationBar.hidden = YES;
            
            contacts_tab = [[ConNewContactsViewController alloc] initWithNibName:nil bundle:nil];
            
            [contacts_tab.tabBarItem setImageInsets:UIEdgeInsetsMake(5, 0, -5, 0)];
            contacts_tab.tabBarItem.image = [[UIImage imageNamed:@"new_contacts"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            contacts_tab.tabBarItem.selectedImage = [UIImage imageNamed:@"new_contacts(active)"]; //imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            UINavigationController *contacts_NavController = [[UINavigationController alloc] initWithRootViewController:contacts_tab];
            contacts_NavController.navigationBar.hidden= YES;
            
            requests_tab = [[ConRequestViewController alloc] initWithNibName:nil bundle:nil];
            [requests_tab.tabBarItem setImageInsets:UIEdgeInsetsMake(5, 0, -5, 0)];
            requests_tab.tabBarItem.image = [[UIImage imageNamed:@"new_request"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            requests_tab.tabBarItem.selectedImage = [[UIImage imageNamed:@"new_request(active)"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            UINavigationController *requests_NavController = [[UINavigationController alloc] initWithRootViewController:requests_tab];
            requests_NavController.navigationBar.hidden= YES;
            
            map_tab = [[ConMapViewController alloc] initWithNibName:nil bundle:nil];
            [map_tab.tabBarItem setImageInsets:UIEdgeInsetsMake(5, 0, -5, 0)];
            map_tab.tabBarItem.image = [[UIImage imageNamed:@"new_map"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            map_tab.tabBarItem.selectedImage = [[UIImage imageNamed:@"new_map(active)"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            UINavigationController *map_NavController =[[UINavigationController alloc] initWithRootViewController:map_tab];
            map_NavController.navigationBar.hidden= YES;
            
            groups_tab = [[ConGroupsViewController alloc] initWithNibName:nil bundle:nil];
            [groups_tab.tabBarItem setImageInsets:UIEdgeInsetsMake(5, 0, -5, 0)];
            groups_tab.tabBarItem.image = [[UIImage imageNamed:@"Groups-1"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            groups_tab.tabBarItem.selectedImage = [[UIImage imageNamed:@"Groups (active)"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            UINavigationController *scan_NavController =[[UINavigationController alloc] initWithRootViewController:groups_tab];
            scan_NavController.navigationBar.hidden= YES;
            
            NSArray *controllers = [NSArray arrayWithObjects:contacts_NavController, scan_NavController,map_NavController,requests_NavController,settings_nav_controller,nil];
            
            self.tabBarController.viewControllers = controllers;
            
            self.window.rootViewController= self.tabBarController;
            
            if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"nowlogsin"] isEqualToString:@"yes"])
            {
                self.tabBarController.selectedIndex = 0;
                [prefs setObject:@"no" forKey:@"nowlogsin"];
            }
            else
                self.tabBarController.selectedIndex = 0;
            
        }
        else
        {
            self.tabBarController.tabBar.hidden = NO;
        }
        self.tabBarController.tabBar.frame = CGRectMake(0, [[UIScreen mainScreen]bounds].size.height-44, [UIScreen mainScreen].bounds.size.width, 44);
    }
    else
    {
        for(UIView *view in self.tabBarController.view.subviews)
        {
            if([view isKindOfClass:[UITabBar class]])
            {
                [view setFrame:CGRectMake(view.frame.origin.x,[[UIScreen mainScreen]bounds].size.height,view.frame.size.width,view.frame.size.height)];
            }
            else
            {
                [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, [[UIScreen mainScreen]bounds].size.height)];
            }
        }
        self.tabBarController = nil;
    }
    
}


- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    DebugLog(@"indeksh: %d",(int)self.tabBarController.selectedIndex);
    self.previousTabIndex = (int)self.tabBarController.selectedIndex;
    return YES;
}

-(void)showTabValues:(BOOL)status
{
    if (status)
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        
        [[tabBarController.viewControllers objectAtIndex:0] tabBarItem].badgeValue = Nil;
        
        NSOperationQueue *bgQueue = [[NSOperationQueue alloc] init];
        [bgQueue addOperationWithBlock:^{
            
            NSString *urlString_reqcount =[NSString stringWithFormat:@"https://budnav.com/ext/?action=notification_count&id=%@&access_token=%@&device_id=%@",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
            
            NSString *newString_reqcount = [urlString_reqcount stringByReplacingOccurrencesOfString:@" " withString:@""];
            DebugLog(@"Request Count URl:%@",newString_reqcount);
            NSData *signeddataURL_reqcount =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString_reqcount]];
            
            
            NSError *error=nil;
            if (signeddataURL_reqcount != nil)
            {
                NSDictionary *jsona_reqcount = [NSJSONSerialization JSONObjectWithData:signeddataURL_reqcount //1
                                                
                                                                               options:kNilOptions
                                                
                                                                                 error:&error];
                
                NSString *errornumber_reqcount= [NSString stringWithFormat:@"%@",[jsona_reqcount objectForKey:@"errorno"]];
                if (![errornumber_reqcount isEqualToString:@"0"])
                {
                    NSString *err_str_reqcount = [jsona_reqcount objectForKey:@"error"];
                    alert = [[UIAlertView alloc] initWithTitle:@"Error in Retrieving Data!"
                                                       message:err_str_reqcount
                                                      delegate:self
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                    //            [alert show];
                }
                else
                {
                    NSDictionary *countreq= [jsona_reqcount objectForKey:@"details"];
                    request_count = [NSString stringWithFormat:@"%d",[[countreq objectForKey:@"unread"] intValue]];
                    DebugLog(@"Request Count:%@",request_count);
                }
            }
            else
            {
                DebugLog(@"no connnn 2");
            }
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                DebugLog(@"coming in main queue show tab values");
                if ([request_count intValue] == 0)
                    [[tabBarController.viewControllers objectAtIndex:3] tabBarItem].badgeValue = Nil;
                else
                    [[tabBarController.viewControllers objectAtIndex:3] tabBarItem].badgeValue =request_count;
                
            }];
        }];
        
    }
    
    
}

//- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)devToken {
//    NSString *deviceToken = [[[[devToken description]
//                               stringByReplacingOccurrencesOfString:@"<"withString:@""]
//                              stringByReplacingOccurrencesOfString:@">" withString:@""]
//                             stringByReplacingOccurrencesOfString: @" " withString: @""];
//    if ([deviceToken length] == 0 || [deviceToken isKindOfClass:[NSNull class]])
//    {
//        deviceToken = @"YX78VtvXaA7vKVyLX2";
//    }
//    [[NSUserDefaults standardUserDefaults] setObject:deviceToken forKey:@"deviceToken"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//
//
//    DebugLog(@"today deviceToken %@",deviceToken);
//}


-(void)createEditableCopyOfDatabaseIfNeeded {
    // First, test for existence.
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"contacts.db"];
    success = [fileManager fileExistsAtPath:writableDBPath];
    
    // The writable database does not exist, so copy the default to the appropriate location.
    NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"contacts.db"];
    success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
    
    if (!success) {
        
        DebugLog (@"Failed to create writable database file with message '%@'.", [error localizedDescription]);
    }
    else{
    }
}

- (NSString *) getDBPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    return [documentsDir stringByAppendingPathComponent:@"contacts.db"];
}

-(void)loadContactsIntoDB :(NSMutableArray *)array
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //1
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"data.plist"];
    
    NSMutableArray *data ;
    data= [array mutableCopy];
    
    
    [data writeToFile: path atomically:YES];
}


-(void)loadRequestsIntoDB :(NSMutableArray *)array
{
    if (dbstatusq == 0)
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            
            dbstatusq = 1;
            [[DBManager getSharedInstance]insertRequests:array];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                dbstatusq=0;
            });
        });
    }
}

-(void)loadMapIntoDB :(NSMutableArray *)array
{
    if (dbstatusq == 0)
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            
            dbstatusq = 1;
            [[DBManager getSharedInstance]insertMap:array];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                dbstatusq=0;
            });
        });
    }
}

-(void)loadProfileIntoDB :(NSDictionary *)dicts :(int)userid
{
    if (dbstatusq == 0)
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dbstatusq = 1;
            [[DBManager getSharedInstance]insertProfile:dicts :userid];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                dbstatusq=0;
            });
        });
    }
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    DebugLog(@"controller class: %@", NSStringFromClass([viewController class]));
    DebugLog(@"controller title: %@", viewController.title);
    
    scrolloffsety = 0;
    
    NSArray *array = [self.tabBarController viewControllers];
    if([[array objectAtIndex:self.tabBarController.selectedIndex] isKindOfClass:[UINavigationController class]])
        [(UINavigationController *)[array objectAtIndex:self.tabBarController.selectedIndex] popToRootViewControllerAnimated: YES];
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"applicationDidReceiveMemoryWarning" object:Nil];
}

- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DataPush" object:Nil];
    
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive)
    {
        [self showTabValues:YES];
     //   NSString *alertstr=[NSString stringWithFormat:@"%@",[[userInfo objectForKey:@"aps"]objectForKey:@"alert"]];
        
        DebugLog(@"Push1:%@",userInfo);
        
        int type=[[[userInfo objectForKey:@"aps"]objectForKey:@"push_type"] intValue];
        
       // NSString *class=[NSString stringWithFormat:@"%@",[self.navigationController.visibleViewController class]];
        
        UIViewController *visible = [self visibleViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
        
        alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"%@",[userInfo objectForKey:@"aps"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
      //  [alert show];
        
         DebugLog(@"CLASS:%@",visible);
        
        [self showTabValues:YES];
        
            if (![visible isKindOfClass:[ConRequestViewController class]])
            {
                [[tabBarController.viewControllers objectAtIndex:3] tabBarItem].badgeValue = @"1";
                [[NSNotificationCenter defaultCenter] postNotificationName:@"ActivePush" object:Nil];
            }
        
        if (type==1 || type==8 || type == 7)
        {
            
            //            UINavigationController *navController = (UINavigationController *)self.window.rootViewController;
            //            [navController.visibleViewController.view makeToast:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] duration:3 position:CSToastPositionTop title:nil];
            // [iToast makeText:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]];
            
            UIViewController *vc = [self visibleViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
            
            
            [vc.view makeToast:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] duration:3 position:CSToastPositionTop title:nil];
            
            //            UIAlertView *toast = [[UIAlertView alloc] initWithTitle:nil                                                            message:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]
            //                                                           delegate:nil                                                  cancelButtonTitle:nil                                                  otherButtonTitles:nil, nil];
            //            toast.backgroundColor=[UIColor redColor];
            //            [toast show];
            //            int duration = 2; // duration in seconds
            //            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{                [toast dismissWithClickedButtonIndex:0 animated:YES];            });
            
            //            ConRequestViewController *con = [[ConRequestViewController alloc]init];
            //            [self.navigationController pushViewController:con animated:YES];
            //            self.tabBarController.selectedIndex = 3;
            //            [[NSNotificationCenter defaultCenter] postNotificationName:@"Accepted_push" object:Nil];
            
            
        }
        else if(type==2)
        {
            NSLog(@"output okkk");
            //            ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
            //            con.request = YES;
            //            //            con.request = TRUE;
            //            //  con.other=@"yes";
            //            con.uid=[NSString stringWithFormat:@"%@",[[userInfo objectForKey:@"aps"] objectForKey:@"user_id"]];
            //            [self.navigationController pushViewController:con animated:YES];
            //            [[NSNotificationCenter defaultCenter] postNotificationName:@"Accepted_request" object:Nil];
            //[iToast makeText:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]];
            
            UIViewController *vc = [self visibleViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
            
            
            [vc.view makeToast:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] duration:3 position:CSToastPositionTop title:nil];
            
        }
        else if(type==6)
        {
            //            ConAddFriendViewController *con = [[ConAddFriendViewController alloc]init];
            //
            //            //            CATransition* transition = [CATransition animation];
            //            //
            //            //            transition.duration = 0.4;
            //            //            transition.type = kCATransitionPush;
            //            //            transition.subtype = kCATransitionFade;
            //            //
            //            //            [[self navigationController].view.layer addAnimation:transition forKey:nil];
            //            [[self navigationController] pushViewController:con animated:YES];
            //            [[NSNotificationCenter defaultCenter] postNotificationName:@"NewConnection" object:Nil];
            // [iToast makeText:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]];
            UIViewController *vc = [self visibleViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
            
            
            [vc.view makeToast:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] duration:3 position:CSToastPositionTop title:nil];
            
        }
        
        else if(type==4)
        {
            //            GroupDetailsViewController *grpdtls=[[GroupDetailsViewController alloc]init];
            //            grpdtls.groupid=[NSString stringWithFormat:@"%@",[[userInfo objectForKey:@"aps"] objectForKey:@"group_id"]];
            //            //            grpdtls.groupname=groupname;
            //            //            grpdtls.member_count=member_count;
            //            //            grpdtls.isadmin=@"0";
            //            //
            //            //            CATransition* transition = [CATransition animation];
            //            //
            //            //            transition.duration = 0.4;
            //            //            transition.type = kCATransitionPush;
            //            //            transition.subtype = kCATransitionFade;
            //            //            transition.subtype = kCATransitionFromRight;
            //            //
            //            //            [[self navigationController].view.layer addAnimation:transition forKey:nil];
            //
            //            [self.navigationController pushViewController:grpdtls animated:YES];
            //            [[NSNotificationCenter defaultCenter] postNotificationName:@"RequestFromGroup" object:Nil];
            
            //            ConRequestViewController *con = [[ConRequestViewController alloc]init];
            //            self.tabBarController.selectedIndex = 3;
            //            [self.navigationController pushViewController:con animated:YES];
            //[iToast makeText:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]];
            
            UIViewController *vc = [self visibleViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
            
            
            [vc.view makeToast:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] duration:3 position:CSToastPositionTop title:nil];
            
            // [[NSNotificationCenter defaultCenter] postNotificationName:@"Accepted_push" object:Nil];
            
        }
        //        else
        //        {
        //
        //        }
        else if(type==5)
        {
            
            UIViewController *vc = [self visibleViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
            
            
            [vc.view makeToast:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] duration:3 position:CSToastPositionTop title:nil];
            
            //            ConNewRequestsViewController *mng = [[ConNewRequestsViewController alloc]init];
            //
            //            // mng.userid= [[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"id"] intValue];
            //            mng.userid=[[NSString stringWithFormat:@"%@",[[userInfo objectForKey:@"aps"] objectForKey:@"user_id"]]intValue];
            //            //DebugLog(@"selected user id : %d",mng.userid);
            //            // filter=YES;//searchBar.text=@"";
            //            //
            //            //            CATransition* transition = [CATransition animation];
            //            //
            //            //            transition.duration = 0.4;
            //            //            transition.type = kCATransitionPush;
            //            //            transition.subtype = kCATransitionFade;
            //            //
            //            //            [[self navigationController].view.layer addAnimation:transition forKey:nil];
            //
            //            [self.navigationController pushViewController:mng animated:YES];
            //            [[NSNotificationCenter defaultCenter] postNotificationName:@"NewConnectionType" object:Nil];
        }
        else if(type==3)
        {
            
            UIViewController *vc = [self visibleViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
            
            
            [vc.view makeToast:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] duration:3 position:CSToastPositionTop title:nil];
            //            ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
            //            con.request = YES;
            //            //            con.request = TRUE;
            //            //  con.other=@"yes";
            //            con.uid=[NSString stringWithFormat:@"%@",[[userInfo objectForKey:@"aps"] objectForKey:@"user_id"]];
            //            [self.navigationController pushViewController:con animated:YES];
            //            [[NSNotificationCenter defaultCenter] postNotificationName:@"TypeChange" object:Nil];
        }
        else if (type==9)
        {
            UIViewController *vc = [self visibleViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
            
            
            [vc.view makeToast:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] duration:3 position:CSToastPositionTop title:nil];
        }
        else if (type==10)
        {
            
            UIViewController *vc = [self visibleViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
            
            
            [vc.view makeToast:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] duration:3 position:CSToastPositionTop title:nil];

        }
        
    }
    
    
    else if (state == UIApplicationStateInactive || state == UIApplicationStateBackground)
    {
        //What you want to do when your app was in background and it got push notification
        
        NSString *alertstr=[NSString stringWithFormat:@"%@",[[userInfo objectForKey:@"aps"]objectForKey:@"alert"]];
        
        DebugLog(@"PUSH:%@",userInfo);
        
        int type=[[[userInfo objectForKey:@"aps"]objectForKey:@"push_type"] intValue];
        
        
        if (type==1 || type==8 || type == 7)
        {
            
            ConRequestViewController *con = [[ConRequestViewController alloc]init];
            [self.navigationController pushViewController:con animated:YES];
            self.tabBarController.selectedIndex = 3;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Accepted_push" object:Nil];
            
            
        }
        else if(type==2)
        {
            NSLog(@"output okkk");
            ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
            con.request = YES;
            //            con.request = TRUE;
            //  con.other=@"yes";
            
            _profileRedirect = YES;
            con.other = @"yes";
            
            [[NSUserDefaults standardUserDefaults] setObject:[[userInfo objectForKey:@"aps"] objectForKey:@"user_id"] forKey:@"other_user"];
            
            // con.uid=[NSString stringWithFormat:@"%@",[[userInfo objectForKey:@"aps"] objectForKey:@"user_id"]];
            con.uid = [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"other_user"]];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Accepted_request" object:Nil];
            
            [self.navigationController pushViewController:con animated:YES];
            
        }
        else if(type==6)
        {
            ConAddFriendViewController *con = [[ConAddFriendViewController alloc]init];
            
            _newUser = YES;
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NewConnection" object:Nil];
            [[self navigationController] pushViewController:con animated:YES];
        }
        
        else if(type==4)
        {
            
            ConRequestViewController *con = [[ConRequestViewController alloc]init];
            self.tabBarController.selectedIndex = 3;
            [self.navigationController pushViewController:con animated:YES];
            
            // [[NSNotificationCenter defaultCenter] postNotificationName:@"Accepted_push" object:Nil];
            
        }
        else if(type==5)
        {
            ConNewRequestsViewController *mng = [[ConNewRequestsViewController alloc]init];
            
            [[NSUserDefaults standardUserDefaults] setObject:[[userInfo objectForKey:@"aps"] objectForKey:@"user_id"] forKey:@"userforrequest"];
            
            //            UIAlertView *type_5 = [[UIAlertView alloc] initWithTitle:@"Push" message:[NSString stringWithFormat:@"%@",[userInfo objectForKey:@"aps"] ] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //            [type_5 show];
            _newrequestRedirect = YES;
            mng.userid = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userforrequest"] intValue];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NewConnectionType" object:Nil];
            [self.navigationController pushViewController:mng animated:YES];
            
        }
        else if(type==3)
        {
            ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
            con.request = YES;
            //            con.request = TRUE;
            //  con.other=@"yes";
            _profileRedirect = YES;
            con.uid=[NSString stringWithFormat:@"%@",[[userInfo objectForKey:@"aps"] objectForKey:@"user_id"]];
            [self.navigationController pushViewController:con animated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TypeChange" object:Nil];
        }
        else if (type==9)
        {
            ConLocateGroupViewController *conLocate = [[ConLocateGroupViewController alloc]init];
            [[NSUserDefaults standardUserDefaults] setObject:[[userInfo objectForKey:@"aps"] objectForKey:@"group"] forKey:@"sharedLocGrpId"];
            conLocate.group_id=[NSString stringWithFormat:@"%@",[[userInfo objectForKey:@"aps"] objectForKey:@"group"]];
            [self.navigationController pushViewController:conLocate animated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ShareLocation" object:Nil];
        }
        else if (type==10)
        {
//            ConPersonalProfileViewController *FSVC = [[ConPersonalProfileViewController alloc]init];
//            [self.navigationController pushViewController:FSVC animated:YES];
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateInfo" object:Nil];
        }
        
        
    }
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void (^)())completionHandler {
    
    if ([identifier isEqualToString:@"ACTION_YES"]) {
        
        NSLog(@"You chose action 1.");
        
//        ConFullSettingsViewController *FSVC = [[ConFullSettingsViewController alloc]init];
//        [self.navigationController pushViewController:FSVC animated:YES];
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"Update Info" object:Nil];
    }
    else if ([identifier isEqualToString:@"ACTION_NO"]) {
        
        NSLog(@"You chose action 2.");
        
        ConPersonalProfileViewController *PVC = [[ConPersonalProfileViewController alloc]init];
        PVC.toUpdateInfo = @"YES";
        [self.navigationController pushViewController:PVC animated:YES];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateInfo" object:Nil];

        
    }
    if (completionHandler) {
        
        completionHandler();
    }
}



-(void)reloadNewContactsFromWeb
{
    DebugLog(@"topviewcontroller: %@", navigationController.topViewController);
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    DebugLog(@"app delegate prefs: %@", [prefs objectForKey:@"backgroundContactsRefresh"]);
    @autoreleasepool {
        
        [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"backgroundContactsRefresh"];
        connectiondoublefiler =0;
        
        NSError *error = nil;
        NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=contacts&amount=-1&id=%@&access_token=%@&device_id=%@&thumb=true",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
        
        DebugLog(@"eshe gechhe from loadrequests from web contacts profile url: %@",urlString1);
        
        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
        
        if (signeddataURL1 == nil)
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        }
        else
        {
            json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                    options:kNilOptions
                                                      error:&error];
            
            NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
            
            if (![errornumber isEqualToString:@"0"])
            {
                NSString *err_str = [json1 objectForKey:@"error"];
                alert = [[UIAlertView alloc] initWithTitle:@"Error in Retrieving Data!"
                                                   message:err_str
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                //                [alert show];
            }
            else
            {
                app_contacts = [[NSMutableArray alloc]init];
                
                if ([[json1 objectForKey:@"details"] isKindOfClass:[NSNull class]] || [json1 objectForKey:@"details"] == (id)[NSNull null] ||[[json1 objectForKey:@"details"] count] ==0)
                {
                }
                else
                {
                    NSDictionary *contacts_temp_dict = [json1 objectForKey:@"details"];
                    
                    DebugLog(@"WEB LOAD AFTER SYNC:%@",[json1 objectForKey:@"details"]);
                    
                    newcontactsapp = [[NSMutableArray alloc]init];
                    int con_count1;
                    con_count1 = 0;
                    for (NSDictionary *dictc in [contacts_temp_dict objectForKey:@"contacts"])
                    {
                        [newcontactsapp addObject:dictc];  //[dictc objectForKey:@"fullname"]
                    }
                    
                    int finalwrite;
                    for (finalwrite =0; finalwrite < [newcontactsapp count]; finalwrite++)
                    {
                        int traverselooptocheck;
                        traverselooptocheck=0;
                        NSDictionary *finalwritedict = [newcontactsapp objectAtIndex:finalwrite];
                        
                        NSString *petFirstName = [finalwritedict objectForKey:@"name"];
                        NSString *petLastName = [finalwritedict objectForKey:@"surname"];
                        NSString *petfullname= [finalwritedict objectForKey:@"fullname"];
                        NSString *workphone = @"";
                        NSString *mobilephone = @"";
                        NSString *homephone = @"";
                        NSString *strretp = @"";
                        NSString *cityp= @"";
                        NSString *housenumberp = @"";
                        NSString *countryp = @"";
                        NSString *zipCode = @"";
                        NSString *petEmail = @"";
                        UIImage *profilepic = [[UIImage alloc] init];
                        //        NSString *apphme, *appmobl, *appwrk, *devhme, *devmob, *devwrk;
                        
                        NSString *base64String1= [finalwritedict objectForKey:@"thumb"];
                        if ([base64String1 length] > 6)
                        {
                            NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String1 options:0];
                            profilepic = [UIImage imageWithData:decodedData];
                            
                        }
                        
                        
                        NSData *petImageData = UIImageJPEGRepresentation(profilepic, 0.7f);
                        
                        
                        if ([[finalwritedict objectForKey:@"b_phone_num"] length] > 2)
                            workphone = [NSString stringWithFormat:@"%@%@",[finalwritedict objectForKey:@"b_phone_pre"],[finalwritedict objectForKey:@"b_phone_num"]];
                        if ([[finalwritedict objectForKey:@"mobile_num"] length] > 2)
                            mobilephone = [NSString stringWithFormat:@"%@%@",[finalwritedict objectForKey:@"mobile_pre"],[finalwritedict objectForKey:@"mobile_num"]];
                        if ([[finalwritedict objectForKey:@"phone_num"] length] > 2)
                            homephone = [NSString stringWithFormat:@"%@%@",[finalwritedict objectForKey:@"phone_pre"],[finalwritedict objectForKey:@"phone_num"]];
                        if ([[finalwritedict objectForKey:@"city"] length] > 2)
                            cityp = [NSString stringWithFormat:@"%@",[finalwritedict objectForKey:@"city"]];
                        if ([[finalwritedict objectForKey:@"country"] length] > 2)
                            countryp = [NSString stringWithFormat:@"%@",[finalwritedict objectForKey:@"country"]];
                        
                        
                        if ([[finalwritedict objectForKey:@"street"]length]>2) {
                            strretp = [finalwritedict objectForKey:@"street"];
                            
                        }
                        
                        if (![[finalwritedict objectForKey:@"housenumber"] isKindOfClass:[NSNull class]] &&  ![[finalwritedict objectForKey:@"housenumber"] isEqualToString:@"(null)"] && ![[finalwritedict objectForKey:@"housenumber"] isEqualToString:@""] && [[finalwritedict objectForKey:@"housenumber"] length] > 0) {
                            
                            housenumberp = [finalwritedict objectForKey:@"housenumber"];
                            
                            strretp = [NSString stringWithFormat:@"%@,%@",strretp,housenumberp];
                            
                        }
                        
                        if ([[finalwritedict objectForKey:@"zipcode"] length]>2) {
                            zipCode = [NSString stringWithFormat:@"%@", [finalwritedict objectForKey:@"zipcode"] ];
                            
                        }
                        
                        if ([[finalwritedict objectForKey:@"email"] length]>2) {
                            petEmail = [finalwritedict objectForKey:@"email"];
                            
                        }
                        
                        addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
                        ABRecordRef pet = ABPersonCreate();
                        ABRecordSetValue(pet, kABPersonFirstNameProperty, (__bridge CFStringRef)petFirstName, nil);
                        ABRecordSetValue(pet, kABPersonLastNameProperty, (__bridge CFStringRef)petLastName, nil);
                        
                        
                        ABMutableMultiValueRef phoneNumbers = ABMultiValueCreateMutable(kABMultiStringPropertyType);
                        ABMultiValueAddValueAndLabel(phoneNumbers, (__bridge CFStringRef)workphone, kABWorkLabel, NULL);
                        ABMultiValueAddValueAndLabel(phoneNumbers, (__bridge CFStringRef)homephone, kABHomeLabel, NULL);
                        ABMultiValueAddValueAndLabel(phoneNumbers, (__bridge CFStringRef)mobilephone, kABPersonPhoneMobileLabel, NULL);
                        ABRecordSetValue(pet, kABPersonPhoneProperty, phoneNumbers, nil);
                        ABPersonSetImageData(pet, (__bridge CFDataRef)petImageData, nil);
                        
                        
                        /////////////////////// Email & Address Adding /////////////////
                        
                        
                        ABMutableMultiValueRef emails = ABMultiValueCreateMutable(kABMultiStringPropertyType);
                        ABMultiValueAddValueAndLabel(emails,  (__bridge CFStringRef)petEmail, kABWorkLabel, NULL);
                        
                        ABRecordSetValue(pet, kABPersonEmailProperty, emails, nil);
                        //                    DebugLog(@"ADDRESS PROPERTY:%@",emails);
                        
                        //CFRelease(emails);
                        
                        
                        //////////////////////// Adding Address //////////////////////////////
                        
                        ABMutableMultiValueRef multiAddress1 = ABMultiValueCreateMutable(kABMultiDictionaryPropertyType);
                        
                        NSMutableDictionary *addressDictionary2 = [[NSMutableDictionary alloc] init];
                        [addressDictionary2 setObject:strretp forKey:(NSString *) kABPersonAddressStreetKey];
                        [addressDictionary2 setObject:zipCode forKey:(NSString *)kABPersonAddressZIPKey];
                        
                        [addressDictionary2 setObject:cityp forKey:(NSString *)kABPersonAddressCityKey];
                        
                        [addressDictionary2 setObject:countryp forKey:(NSString *)kABPersonAddressCountryKey];
                        //[addressDictionary setObject:@"us" forKey:(NSString *)kABPersonAddressCountryCodeKey];
                        
                        
                        ABMultiValueAddValueAndLabel(multiAddress1, (__bridge CFTypeRef)(addressDictionary2), kABHomeLabel, NULL);
                        
                        
                        //(@"home address added");
                        
                        ABRecordSetValue(pet, kABPersonAddressProperty, multiAddress1, nil);
                        
                        
                        NSArray *allADContacts = (__bridge NSArray *)ABAddressBookCopyArrayOfAllPeople(addressBookRef);
                        for (id record in allADContacts){
                            ABRecordRef thisContact = (__bridge ABRecordRef)record;
                            
                            if (ABRecordCopyCompositeName(thisContact) != NULL)
                            {
                                if (CFStringCompare(ABRecordCopyCompositeName(thisContact),
                                                    (__bridge CFStringRef)(petfullname), 0) == kCFCompareEqualTo){
                                    traverselooptocheck =1;
                                }
                                else
                                {
                                }
                            }
                        }
                        
                        if (traverselooptocheck == 0)
                        {
                            ABAddressBookAddRecord(addressBookRef, pet, nil);
                            ABAddressBookSave(addressBookRef, nil);
                        }
                    }
                    
                    DebugLog(@"new app contacts:%@ %ld",newcontactsapp,(unsigned long)[newcontactsapp count]);
                    DebugLog(@"new app contacts count:%ld",(unsigned long)[newcontactsapp count]);
                }
            }
        }
        
        NSString *previous_app_contacts_str = [[NSUserDefaults standardUserDefaults]objectForKey:@"app_contacts_count"];
        long previous_app_contacts = (long)[previous_app_contacts_str longLongValue];
        
        DebugLog(@"koto elo: %ld == %@ == %@",previous_app_contacts, [[NSUserDefaults standardUserDefaults]objectForKey:@"app_contacts_count"], previous_app_contacts_str);
        
        NSData *dataRepresentingSavedArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"Contactslocaldb"];
        if (dataRepresentingSavedArray != nil)
        {
            NSArray *oldSavedArray = [NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
            if (oldSavedArray != nil)
                Bhaswar_array = [[NSMutableArray alloc] initWithArray:oldSavedArray];
            else
                Bhaswar_array = [[NSMutableArray alloc] init];
        }
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //1
        NSString *documentsDirectory = [paths objectAtIndex:0]; //2
        NSString *path = [documentsDirectory stringByAppendingPathComponent:@"data.plist"]; //3
        
        NSMutableArray *savedStock = [[NSMutableArray alloc] initWithContentsOfFile: path];
        if ([Bhaswar_array count]== 0)
        {
            Bhaswar_array = [savedStock mutableCopy];
            DebugLog(@"yyyyyyyyyy");
        }
        if ([Bhaswar_array count]== 0)
        {
            Bhaswar_array = [newarr_contacts_ad mutableCopy];
            DebugLog(@"zzzzzzzzzz");
        }
        
        for (long ijk =0; ijk < previous_app_contacts; ijk++)
        {
            [Bhaswar_array removeObjectAtIndex:ijk];
        }
        
        now_Bhaswar_array = [[NSMutableArray alloc]init];
        now_Bhaswar_array = [newcontactsapp mutableCopy];
        now_Bhaswar_array = [[newcontactsapp arrayByAddingObjectsFromArray:Bhaswar_array] mutableCopy];
        
        
        for (NSDictionary *dicts1 in now_Bhaswar_array){
            
            DebugLog(@"ARRAY ARRAY: %@",dicts1);
            int traverselooptocheck;
            traverselooptocheck=0;
            NSString *petfullname= [dicts1 valueForKey:@"fullname"];
            NSString *mobilephone = @"";
            NSString *cityp= @"";
            NSString *countryp = @"";
            NSString *zipCode = @"";
            NSString *petEmail = @"";
            NSString *strretp = @"";
            NSString *housenumberp = @"";
            //        NSString *apphme, *appmobl, *appwrk, *devhme, *devmob, *devwrk;
            
            if ([[dicts1 objectForKey:@"mobile_num"] length] > 2)
                mobilephone = [NSString stringWithFormat:@"%@",[dicts1 valueForKey:@"mobile_num"]];
            
            if (![[dicts1 objectForKey:@"city"] isKindOfClass:[NSNull class]] &&  ![[dicts1 objectForKey:@"city"] isEqualToString:@"(null)"] && ![[dicts1 objectForKey:@"city"] isEqualToString:@""] && [[dicts1 objectForKey:@"city"] length] > 2)
                cityp = [NSString stringWithFormat:@"%@",[dicts1 objectForKey:@"city"]];
            if (![[dicts1 objectForKey:@"country"] isKindOfClass:[NSNull class]] &&  ![[dicts1 objectForKey:@"country"] isEqualToString:@"(null)"] && ![[dicts1 objectForKey:@"country"] isEqualToString:@""] && [[dicts1 objectForKey:@"country"] length] > 2)
                countryp = [NSString stringWithFormat:@"%@",[dicts1 objectForKey:@"country"]];
            
            
            if (![[dicts1 objectForKey:@"street"] isKindOfClass:[NSNull class]] &&  ![[dicts1 objectForKey:@"street"] isEqualToString:@"(null)"] && ![[dicts1 objectForKey:@"street"] isEqualToString:@""] && [[dicts1 objectForKey:@"street"] length] > 2) {
                strretp = [dicts1 objectForKey:@"street"];
                
            }
            
            if (![[dicts1 objectForKey:@"housenumber"] isKindOfClass:[NSNull class]] &&  ![[dicts1 objectForKey:@"housenumber"] isEqualToString:@"(null)"] && ![[dicts1 objectForKey:@"housenumber"] isEqualToString:@""] && [[dicts1 objectForKey:@"housenumber"] length] > 0) {
                
                housenumberp = [dicts1 objectForKey:@"housenumber"];
                
                strretp = [NSString stringWithFormat:@"%@,%@",strretp,housenumberp];
                
            }
            
            if (![[dicts1 objectForKey:@"zipcode"] isKindOfClass:[NSNull class]] &&  ![[dicts1 objectForKey:@"zipcode"] isEqualToString:@"(null)"] && ![[dicts1 objectForKey:@"zipcode"] isEqualToString:@""] && [[dicts1 objectForKey:@"zipcode"] length] > 2) {
                zipCode = [NSString stringWithFormat:@"%@", [dicts1 objectForKey:@"zipcode"] ];
                
            }
            
            if (![[dicts1 objectForKey:@"email"] isKindOfClass:[NSNull class]] &&  ![[dicts1 objectForKey:@"email"] isEqualToString:@"(null)"] && ![[dicts1 objectForKey:@"email"] isEqualToString:@""] && [[dicts1 objectForKey:@"email"] length] > 2) {
                petEmail = [dicts1 objectForKey:@"email"];
                
            }
            
            addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
            ABRecordRef pet = ABPersonCreate();
            ABRecordSetValue(pet, kABPersonFirstNameProperty, (__bridge CFStringRef)petfullname, nil);
            ABMutableMultiValueRef phoneNumbers = ABMultiValueCreateMutable(kABMultiStringPropertyType);
            ABMultiValueAddValueAndLabel(phoneNumbers, (__bridge CFStringRef)mobilephone, kABPersonPhoneMobileLabel, NULL);
            ABRecordSetValue(pet, kABPersonPhoneProperty, phoneNumbers, nil);
            
            
            /////////////////////// Email & Address Adding /////////////////
            
            
            ABMutableMultiValueRef emails = ABMultiValueCreateMutable(kABMultiStringPropertyType);
            ABMultiValueAddValueAndLabel(emails,  (__bridge CFStringRef)petEmail, kABWorkLabel, NULL);
            
            ABRecordSetValue(pet, kABPersonEmailProperty, emails, nil);
            
            //CFRelease(emails);
            
            //////////////////////// Adding Address //////////////////////////////
            
            ABMutableMultiValueRef multiAddress1 = ABMultiValueCreateMutable(kABMultiDictionaryPropertyType);
            
            NSMutableDictionary *addressDictionary2 = [[NSMutableDictionary alloc] init];
            [addressDictionary2 setObject:strretp forKey:(NSString *) kABPersonAddressStreetKey];
            [addressDictionary2 setObject:zipCode forKey:(NSString *)kABPersonAddressZIPKey];
            
            [addressDictionary2 setObject:cityp forKey:(NSString *)kABPersonAddressCityKey];
            
            [addressDictionary2 setObject:countryp forKey:(NSString *)kABPersonAddressCountryKey];
            
            
            ABMultiValueAddValueAndLabel(multiAddress1, (__bridge CFTypeRef)(addressDictionary2), kABHomeLabel, NULL);
            
            
            ABRecordSetValue(pet, kABPersonAddressProperty, multiAddress1, nil);
            
            
            NSArray *allADContacts = (__bridge NSArray *)ABAddressBookCopyArrayOfAllPeople(addressBookRef);
            for (id record in allADContacts){
                ABRecordRef thisContact = (__bridge ABRecordRef)record;
                
                if (ABRecordCopyCompositeName(thisContact) != NULL)
                {
                    if (CFStringCompare(ABRecordCopyCompositeName(thisContact),(__bridge CFStringRef)(petfullname), 0) == kCFCompareEqualTo){
                        traverselooptocheck =1;
                    }
                    else
                    {
                    }
                }
            }
            
            if (traverselooptocheck == 0)
            {
                ABAddressBookAddRecord(addressBookRef, pet, nil);
                ABAddressBookSave(addressBookRef, nil);
            }
            
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%ld",(unsigned long)[newcontactsapp count]] forKey:@"app_contacts_count"];
    }
    
}


#pragma encode string function
-(NSString*) encodeToPercentEscapeString:(NSString *)string {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                              (CFStringRef) string,
                                                              NULL,
                                                              (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
}


-(void)selectedIcon
{
    BOOL status = TRUE;
    
    UIViewController *new_settings_tab = nil;
    UIViewController *contacts_tab = nil;
    UIViewController *requests_tab = nil;
    UIViewController *map_tab = nil;
    UIViewController *groups_tab = nil;
    
    //=======================BELOW CHANGED BY ME==================//
    
    [[UITabBar appearance] setTintColor:[UIColor colorWithRed:49.0f/255.0f green:170.0f/255.0f blue:184.0f/255.0f alpha:1]];
    
    
    //=================FOR TAB BAR FONT HIGHLIGHTED=============//
    
    [[UITabBarItem appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor clearColor], NSForegroundColorAttributeName,
      [UIFont fontWithName:@"ProximaNova-Regular" size:13.0], NSFontAttributeName,
      nil] forState:UIControlStateHighlighted];
    
    //================FOR TAB BAR FONT SELECTED=================//
    
    [[UITabBarItem appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor colorWithRed:192.0f/255.0f green:250.0f/255.0f blue:193.0f/255.0f alpha:1], NSForegroundColorAttributeName,
      [UIFont fontWithName:@"ProximaNova-Regular" size:13.0], NSFontAttributeName,
      nil] forState:UIControlStateSelected];
    
    //===============FOR TAB BAR FONT NORMAL===================//
    
    [[UITabBarItem appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor whiteColor], NSForegroundColorAttributeName ,
      [UIFont fontWithName:@"ProximaNova-Regular" size:13.0], NSFontAttributeName,
      nil] forState:UIControlStateNormal];  //colorWithRed:.5 green:.5 blue:.5 alpha:1
    
    
    [[UITabBar appearance] setBackgroundColor:[UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:1]];
    [[UITabBar appearance] setBarTintColor:[UIColor whiteColor]];
    
    if(status)
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs synchronize];
        
        self.tabBarController = [[UITabBarController alloc] init];
        self.tabBarController.delegate = self;
        
        new_settings_tab = [[ConFullSettingsViewController alloc] initWithNibName:nil bundle:nil];
        new_settings_tab.edgesForExtendedLayout = YES;
        
        [new_settings_tab.tabBarItem setImageInsets:UIEdgeInsetsMake(5, 0, -5, 0)];
        new_settings_tab.tabBarItem.image = [[UIImage imageNamed:@"new_setting"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        new_settings_tab.tabBarItem.selectedImage = [[UIImage imageNamed:@"new_setting(active)"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        UINavigationController *settings_nav_controller = [[UINavigationController alloc] initWithRootViewController:new_settings_tab];
        settings_nav_controller.navigationController.navigationBar.hidden = YES;
        
        contacts_tab = [[ConNewContactsViewController alloc] initWithNibName:nil bundle:nil];
        
        [contacts_tab.tabBarItem setImageInsets:UIEdgeInsetsMake(5, 0, -5, 0)];
        contacts_tab.tabBarItem.image = [[UIImage imageNamed:@"new_contacts"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        contacts_tab.tabBarItem.selectedImage = [UIImage imageNamed:@"new_contacts(active)"]; //imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        UINavigationController *contacts_NavController = [[UINavigationController alloc] initWithRootViewController:contacts_tab];
        contacts_NavController.navigationBar.hidden= YES;
        
        requests_tab = [[ConRequestViewController alloc] initWithNibName:nil bundle:nil];
        [requests_tab.tabBarItem setImageInsets:UIEdgeInsetsMake(5, 0, -5, 0)];
        requests_tab.tabBarItem.image = [[UIImage imageNamed:@"new_request"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        requests_tab.tabBarItem.selectedImage = [[UIImage imageNamed:@"new_request(active)"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        
        UINavigationController *requests_NavController = [[UINavigationController alloc] initWithRootViewController:requests_tab];
        requests_NavController.navigationBar.hidden= YES;
        
        map_tab = [[ConMapViewController alloc] initWithNibName:nil bundle:nil];
        [map_tab.tabBarItem setImageInsets:UIEdgeInsetsMake(5, 0, -5, 0)];
        map_tab.tabBarItem.image = [[UIImage imageNamed:@"new_map"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        map_tab.tabBarItem.selectedImage = [[UIImage imageNamed:@"new_map(active)"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        
        UINavigationController *map_NavController =[[UINavigationController alloc] initWithRootViewController:map_tab];
        map_NavController.navigationBar.hidden= YES;
        
        groups_tab = [[ConGroupsViewController alloc] initWithNibName:nil bundle:nil];
        [groups_tab.tabBarItem setImageInsets:UIEdgeInsetsMake(5, 0, -5, 0)];
        groups_tab.tabBarItem.image = [[UIImage imageNamed:@"Groups-1"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        groups_tab.tabBarItem.selectedImage = [[UIImage imageNamed:@"Groups (active)"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        
        UINavigationController *scan_NavController =[[UINavigationController alloc] initWithRootViewController:groups_tab];
        scan_NavController.navigationBar.hidden= YES;
        
        NSArray *controllers = [NSArray arrayWithObjects:contacts_NavController, scan_NavController,map_NavController,requests_NavController,settings_nav_controller,nil];
        
        self.tabBarController.viewControllers = controllers;
        
        self.window.rootViewController= self.tabBarController;
        
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"nowlogsin"] isEqualToString:@"yes"])
        {
            self.tabBarController.selectedIndex = 0;
            [prefs setObject:@"no" forKey:@"nowlogsin"];
        }
        else
            self.tabBarController.selectedIndex = 4;
        
        
        self.tabBarController.tabBar.hidden = NO;
        self.tabBarController.tabBar.frame = CGRectMake(0, [[UIScreen mainScreen]bounds].size.height-44, [UIScreen mainScreen].bounds.size.width, 44);
    }
    else
    {
        for(UIView *view in self.tabBarController.view.subviews)
        {
            if([view isKindOfClass:[UITabBar class]])
            {
                [view setFrame:CGRectMake(view.frame.origin.x,[[UIScreen mainScreen]bounds].size.height,view.frame.size.width,view.frame.size.height)];
            }
            else
            {
                [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, [[UIScreen mainScreen]bounds].size.height)];
            }
        }
        self.tabBarController = nil;
    }
    
}

-(void)ownDetails
{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSError *error = nil;
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=profile&id=%@&access_token=%@&device_id=%@&image=true", [prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
    
    DebugLog(@"own profile url: %@",urlString1);
    
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    
    if (signeddataURL1 != nil)
    {
        json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                 
                                                options:kNilOptions
                 
                                                  error:&error];
        DebugLog(@"own json returns: %@",json1);
        
        NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
        DebugLog(@"err  %@",errornumber);
        
        if (![errornumber isEqualToString:@"0"])
        {
            DebugLog(@"if if");
            NSString *err_str = [json1 objectForKey:@"error"];
            alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                               message:err_str
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
        }
        else
        {
            personal_dict = [[NSMutableDictionary alloc]init];
            business_dict = [[NSMutableDictionary alloc]init];
            social_dict = [[NSMutableDictionary alloc]init];
            
            profdict= [[json1 objectForKey:@"details"] mutableCopy];
            
            [[NSUserDefaults standardUserDefaults] setObject:profdict forKey:@"loadedProfDict"];
            DebugLog(@"FROM WEB:%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"loadedProfDict"]);
            NSDictionary *dict_profile = [profdict objectForKey:@"profile"];
            DebugLog(@"array gives: %@",dict_profile);
            for( NSString *aKey in [dict_profile allKeys])
            {
                NSString *newString = [aKey substringToIndex:2];
                if ([newString isEqualToString:@"b_"])
                {
                    [business_dict setValue:[dict_profile valueForKey:[NSString stringWithFormat:@"%@",aKey]] forKey:[NSString stringWithFormat:@"%@",aKey]];
                    
                }
                else if ([newString isEqualToString:@"s_"])
                {
                    [social_dict setValue:[dict_profile valueForKey:[NSString stringWithFormat:@"%@",aKey]] forKey:[NSString stringWithFormat:@"%@",aKey]];
                    
                }
                else
                {
                    [personal_dict setValue:[dict_profile valueForKey:[NSString stringWithFormat:@"%@",aKey]] forKey:[NSString stringWithFormat:@"%@",aKey]];
                    
                    
                }
            }
            
        }
    }
    
}

-(void)reloadFurther
{
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    @autoreleasepool {
        
        [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"backgroundContactsRefresh"];
        connectiondoublefiler =0;
        
        NSError *error = nil;
        NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=contacts&amount=-1&id=%@&access_token=%@&device_id=%@&thumb=true",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
        
        DebugLog(@"eshe gechhe from loadrequests from web contacts profile url: %@",urlString1);
        
        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
        
        if (signeddataURL1 == nil)
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        }
        else
        {
            json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                    options:kNilOptions
                                                      error:&error];
            
            NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
            
            if (![errornumber isEqualToString:@"0"])
            {
                NSString *err_str = [json1 objectForKey:@"error"];
                alert = [[UIAlertView alloc] initWithTitle:@"Error in Retrieving Data!"
                                                   message:err_str
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                //                [alert show];
            }
            else
            {
                app_contacts = [[NSMutableArray alloc]init];
                
                if ([[json1 objectForKey:@"details"] isKindOfClass:[NSNull class]] || [json1 objectForKey:@"details"] == (id)[NSNull null] ||[[json1 objectForKey:@"details"] count] ==0)
                {
                }
                else
                {
                    
                    DebugLog(@"PULL TO REFRESH:%@",[json1 objectForKey:@"details"]);
                    
                    NSDictionary *contacts_temp_dict = [json1 objectForKey:@"details"];
                    newcontactsapp = [[NSMutableArray alloc]init];
                    int con_count1;
                    con_count1 = 0;
                    for (NSDictionary *dictc in [contacts_temp_dict objectForKey:@"contacts"])
                    {
                        [newcontactsapp addObject:dictc];  //[dictc objectForKey:@"fullname"]
                    }
                    
                    int finalwrite;
                    for (finalwrite =0; finalwrite < [newcontactsapp count]; finalwrite++)
                    {
                        int traverselooptocheck;
                        traverselooptocheck=0;
                        
                        ///////////////// Newly Added //////////////////
                        NSDictionary *finalwritedict = [newcontactsapp objectAtIndex:finalwrite];
                        
                        NSString *petFirstName = [finalwritedict objectForKey:@"name"];
                        NSString *petLastName = [finalwritedict objectForKey:@"surname"];
                        NSString *petfullname= [finalwritedict objectForKey:@"fullname"];
                        NSString *workphone = @"";
                        NSString *mobilephone = @"";
                        NSString *homephone = @"";
                        NSString *strretp = @"";
                        NSString *cityp= @"";
                        NSString *housenumberp = @"";
                        NSString *countryp = @"";
                        NSString *zipCode = @"";
                        NSString *petEmail = @"";
                        
                        
                        ////////////////// Base64 Conversion //////////////////////
                        
                        
                        NSString *base64String1= [[newcontactsapp objectAtIndex:finalwrite] objectForKey:@"thumb"];
                        if ([base64String1 length] > 6)
                        {
                            decodedData = [[NSData alloc] initWithBase64EncodedString:base64String1 options:0];
                            //decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
                            profilepic = [UIImage imageWithData:decodedData];
                            
                        }
                        
                        
                        
                        NSData *petImageData = UIImageJPEGRepresentation(profilepic, 0.7f);
                        //        NSString *apphme, *appmobl, *appwrk, *devhme, *devmob, *devwrk;
                        
                        if ([[finalwritedict objectForKey:@"b_phone_num"] length] > 2)
                            workphone = [NSString stringWithFormat:@"%@%@",[finalwritedict objectForKey:@"b_phone_pre"],[finalwritedict objectForKey:@"b_phone_num"]];
                        if ([[finalwritedict objectForKey:@"mobile_num"] length] > 2)
                            mobilephone = [NSString stringWithFormat:@"%@%@",[finalwritedict objectForKey:@"mobile_pre"],[finalwritedict objectForKey:@"mobile_num"]];
                        if ([[finalwritedict objectForKey:@"phone_num"] length] > 2)
                            homephone = [NSString stringWithFormat:@"%@%@",[finalwritedict objectForKey:@"phone_pre"],[finalwritedict objectForKey:@"phone_num"]];
                        if ([[finalwritedict objectForKey:@"city"] length] > 2)
                            cityp = [NSString stringWithFormat:@"%@",[finalwritedict objectForKey:@"city"]];
                        if ([[finalwritedict objectForKey:@"country"] length] > 2)
                            countryp = [NSString stringWithFormat:@"%@",[finalwritedict objectForKey:@"country"]];
                        
                        
                        if ([[finalwritedict objectForKey:@"street"]length]>2) {
                            strretp = [finalwritedict objectForKey:@"street"];
                            
                        }
                        
                        if (![[finalwritedict objectForKey:@"housenumber"] isKindOfClass:[NSNull class]] &&  ![[finalwritedict objectForKey:@"housenumber"] isEqualToString:@"(null)"] && ![[finalwritedict objectForKey:@"housenumber"] isEqualToString:@""] && [[finalwritedict objectForKey:@"housenumber"] length] > 0) {
                            housenumberp = [finalwritedict objectForKey:@"housenumber"];
                            NSLog(@"HOUSENUMBER:%@",housenumberp);
                            
                            strretp = [NSString stringWithFormat:@"%@,%@",strretp,housenumberp];
                        }
                        
                        
                        if ([[finalwritedict objectForKey:@"zipcode"] length]>2) {
                            zipCode = [NSString stringWithFormat:@"%@", [finalwritedict objectForKey:@"zipcode"] ];
                            
                        }
                        
                        if ([[finalwritedict objectForKey:@"email"] length]>2) {
                            petEmail = [finalwritedict objectForKey:@"email"];
                            
                        }
                        
                        
                        
                        
                        
                        ABRecordRef pet = ABPersonCreate();
                        ABRecordSetValue(pet, kABPersonFirstNameProperty, (__bridge CFStringRef)petFirstName, nil);
                        ABRecordSetValue(pet, kABPersonLastNameProperty, (__bridge CFStringRef)petLastName, nil);
                        
                        
                        ABMutableMultiValueRef phoneNumbers = ABMultiValueCreateMutable(kABMultiStringPropertyType);
                        ABMultiValueAddValueAndLabel(phoneNumbers, (__bridge CFStringRef)workphone, kABWorkLabel, NULL);
                        ABMultiValueAddValueAndLabel(phoneNumbers, (__bridge CFStringRef)homephone, kABHomeLabel, NULL);
                        ABMultiValueAddValueAndLabel(phoneNumbers, (__bridge CFStringRef)mobilephone, kABPersonPhoneMobileLabel, NULL);
                        ABRecordSetValue(pet, kABPersonPhoneProperty, phoneNumbers, nil);
                        ABPersonSetImageData(pet, (__bridge CFDataRef)petImageData, nil);
                        
                        
                        /////////////////////// Email & Address Adding /////////////////
                        
                        
                        ABMutableMultiValueRef emails = ABMultiValueCreateMutable(kABMultiStringPropertyType);
                        ABMultiValueAddValueAndLabel(emails,  (__bridge CFStringRef)petEmail, kABWorkLabel, NULL);
                        
                        ABRecordSetValue(pet, kABPersonEmailProperty, emails, nil);
                        
                        //CFRelease(emails);
                        
                        
                        //////////////////////// Adding Address //////////////////////////////
                        
                        ABMutableMultiValueRef multiAddress1 = ABMultiValueCreateMutable(kABMultiDictionaryPropertyType);
                        
                        NSMutableDictionary *addressDictionary2 = [[NSMutableDictionary alloc] init];
                        [addressDictionary2 setObject:strretp forKey:(NSString *) kABPersonAddressStreetKey];
                        [addressDictionary2 setObject:zipCode forKey:(NSString *)kABPersonAddressZIPKey];
                        
                        [addressDictionary2 setObject:cityp forKey:(NSString *)kABPersonAddressCityKey];
                        
                        [addressDictionary2 setObject:countryp forKey:(NSString *)kABPersonAddressCountryKey];
                        
                        
                        ABMultiValueAddValueAndLabel(multiAddress1, (__bridge CFTypeRef)(addressDictionary2), kABHomeLabel, NULL);
                        
                        
                        ABRecordSetValue(pet, kABPersonAddressProperty, multiAddress1, nil);
                        
                        
                        NSArray *allADContacts = (__bridge NSArray *)ABAddressBookCopyArrayOfAllPeople(addressBookRef);
                        for (id record in allADContacts){
                            ABRecordRef thisContact = (__bridge ABRecordRef)record;
                            
                            if (ABRecordCopyCompositeName(thisContact) != NULL)
                            {
                                if (CFStringCompare(ABRecordCopyCompositeName(thisContact),
                                                    (__bridge CFStringRef)(petfullname), 0) == kCFCompareEqualTo){
                                    traverselooptocheck =1;
                                }
                                else
                                {
                                }
                            }
                        }
                        
                        if (traverselooptocheck == 0)
                        {
                            ABAddressBookAddRecord(addressBookRef, pet, nil);
                            ABAddressBookSave(addressBookRef, nil);
                        }
                    }
                    
                    DebugLog(@"new app contacts:%@ %ld",newcontactsapp,[newcontactsapp count]);
                    DebugLog(@"new app contacts count:%ld",[newcontactsapp count]);
                }
                
                
                
                NSString *previous_app_contacts_str = [[NSUserDefaults standardUserDefaults]objectForKey:@"app_contacts_count"];
                long previous_app_contacts = [previous_app_contacts_str longLongValue];
                
                
                DebugLog(@"koto elo: %ld == %@ == %@",previous_app_contacts, [[NSUserDefaults standardUserDefaults]objectForKey:@"app_contacts_count"], previous_app_contacts_str);
                
                del.Bhaswar_array = [[NSMutableArray alloc]init];
                NSData *dataRepresentingSavedArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"Contactslocaldb"];
                if (dataRepresentingSavedArray != nil)
                {
                    NSArray *oldSavedArray = [NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
                    if (oldSavedArray != nil)
                        del.Bhaswar_array = [[NSMutableArray alloc] initWithArray:oldSavedArray];
                    else
                        del.Bhaswar_array = [[NSMutableArray alloc] init];
                }
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //1
                NSString *documentsDirectory = [paths objectAtIndex:0]; //2
                NSString *path = [documentsDirectory stringByAppendingPathComponent:@"data.plist"]; //3
                
                NSMutableArray *savedStock = [[NSMutableArray alloc] initWithContentsOfFile: path];
                if ([del.Bhaswar_array count]== 0)
                {
                    del.Bhaswar_array = [savedStock mutableCopy];
                    DebugLog(@"Check one");
                }
                if ([del.Bhaswar_array count]== 0)
                {
                    del.Bhaswar_array = [del.newarr_contacts_ad mutableCopy];
                    DebugLog(@"Check two");
                }
                
                DebugLog(@"Contact Array: %@",del.Bhaswar_array);
                
                for (long ijk =0; ijk < previous_app_contacts; ijk++)
                {
                    [del.Bhaswar_array removeObjectAtIndex:0];
                }
                
                DebugLog(@"Contact Array: %@",del.Bhaswar_array);
                
                DebugLog(@"final Contact Array: %@",del.Bhaswar_array);
                
                del.now_Bhaswar_array = [[NSMutableArray alloc]init];
                del.now_Bhaswar_array = [newcontactsapp mutableCopy];
                del.now_Bhaswar_array = [[newcontactsapp arrayByAddingObjectsFromArray:del.Bhaswar_array] mutableCopy];
                
                if ([newcontactsapp count] == 0)
                {
                    del.now_Bhaswar_array = [del.Bhaswar_array mutableCopy];
                }
                
                DebugLog(@"final Contact Array: %@",del.now_Bhaswar_array);
                [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%ld",(unsigned long)[newcontactsapp count]] forKey:@"app_contacts_count"];
            }
            con_array = [[NSMutableArray alloc]init];
            con_array = del.now_Bhaswar_array;
            
#pragma mark -- this is the Beginning of NSLOCALIZEDINDEXEDCOLLATION SECTION - Bhaswar 16/7/2014
            
            // create a dictionary to store an array of objects for each section
            NSMutableDictionary *tempSections = [NSMutableDictionary dictionary];
            
            // iterate through each dictionaey in the list, and put them into the correct section
            for (NSDictionary *item in con_array)
            {
                // get the index of the section (Assuming the table index is showing A-#)
                NSInteger indexName = [[UILocalizedIndexedCollation currentCollation] sectionForObject:[item valueForKey:@"fullname"] collationStringSelector:@selector(description)];
                
                NSNumber *keyName = [NSNumber numberWithInteger:indexName];
                
                // if an array doesnt exist for the key, create one
                NSMutableArray *arrayName = [tempSections objectForKey:keyName];
                if (arrayName == nil)
                {
                    arrayName = [NSMutableArray array];
                }
                
                // add the dictionary to the array (add the actual value as we need this object to sort the array later)
                [arrayName addObject: item];   //[item valueForKey:@"fullname"]];
                
                // put the array with new object in, back into the dictionary for the correct key
                [tempSections setObject:arrayName forKey:keyName];
            }
            
            /* now to do the sorting of each index */
            
            NSMutableDictionary *sortedSections = [NSMutableDictionary dictionary];
            
            // sort each index array (A..Z)
            [tempSections enumerateKeysAndObjectsUsingBlock:^(id key, id array, BOOL *stop)
             {
                 // sort the array - again, we need to tell it which selctor to sort each object by
                 NSArray *sortedArray = [[UILocalizedIndexedCollation currentCollation] sortedArrayFromArray:array collationStringSelector:@selector(description)];
                 
                 NSSortDescriptor *sort =[NSSortDescriptor sortDescriptorWithKey:@"fullname" ascending:YES selector:@selector(caseInsensitiveCompare:)];
                 sortedArray=[[sortedArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]] mutableCopy];
                 [sortedSections setObject:[NSMutableArray arrayWithArray:sortedArray] forKey:key];
             }];
            
            // set the global sectioned dictionary
            indexedSections = sortedSections;
            DebugLog(@"indexed sections & count:%ld...... %@ ", (unsigned long)[indexedSections count], indexedSections);
            
#pragma mark -- this is the End of NSLOCALIZEDINDEXEDCOLLATION SECTION - Bhaswar 16/7/2014
            
            [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:con_array] forKey:@"Contactslocaldb"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //1
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString *path = [documentsDirectory stringByAppendingPathComponent:@"data.plist"];
            
            NSMutableArray *data = [[NSMutableArray alloc]init] ;
            data= [con_array mutableCopy];
            DebugLog(@"bbbbb data: %lu",(unsigned long)[data count]);
            [data writeToFile: path atomically:YES];
            
        }
    }
    
    DebugLog(@"TABLE CONTACTS:%@",indexedSections);
    
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{
    
    NSLog(@"error in did fail to regidter remote notification %@",error.description);
    
    [[NSUserDefaults standardUserDefaults] setObject:@"cb8143a8becd78c317b8e0c722c9177a4b9579ab25f5e2f5f4fe806dc2937a3e" forKey:@"deviceToken"];
    
}


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken

{
    
    
    
    NSError *error;
    
    NSString *deviceToken1 = [[[[deviceToken description]
                                
                                
                                
                                stringByReplacingOccurrencesOfString:@"<"withString:@""]
                               
                               
                               
                               stringByReplacingOccurrencesOfString:@">" withString:@""]
                              
                              
                              
                              stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    
    
    if ([deviceToken1 length] == 0 || [deviceToken1 isKindOfClass:[NSNull class]])
        
    {
        
        deviceToken1 = @"cb8143a8becd78c317b8e0c722c9177a4b9579ab25f5e2f5f4fe806dc2937a3e";
        
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:deviceToken1 forKey:@"deviceToken"];
    
    
    
    NSLog(@"device token here: %@",deviceToken1);
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

#ifdef __IPHONE_8_0

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings

{
    //register to receive notifications
    
    [application registerForRemoteNotifications];
    
}


- (UIViewController *)visibleViewController:(UIViewController *)rootViewController
{
    if (rootViewController.presentedViewController == nil)
    {
        return rootViewController;
    }
    if ([rootViewController.presentedViewController isKindOfClass:[UINavigationController class]])
    {
        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
        
        return [self visibleViewController:lastViewController];
    }
    if ([rootViewController.presentedViewController isKindOfClass:[UITabBarController class]])
    {
        UITabBarController *tabBarController = (UITabBarController *)rootViewController.presentedViewController;
        UIViewController *selectedViewController = tabBarController.selectedViewController;
        
        return [self visibleViewController:selectedViewController];
    }
    
    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
    
    return [self visibleViewController:presentedViewController];
    
}


-(void)sendingRequest
{
    if ([_selected_array count]>0)
    {
        DebugLog(@"Request Fired");
        
        [[NSOperationQueue new] addOperationWithBlock:^{
            
        
        NSString *selectedIDs = [_selected_array componentsJoinedByString:@","];
        
        //   int userid = [[[app_contacts objectAtIndex:sender.tag] objectForKey:@"id"] intValue];
        DebugLog(@"user id to be requested: %@",selectedIDs);
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=addrequest&id=%@&access_token=%@&device_id=%@",selectedIDs,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
        
        DebugLog(@"deny url: %@",urlString1);
        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
        
        NSError *error=nil;
        NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                  options:kNilOptions
                                                                    error:&error];
        DebugLog(@"deny json returns: %@",json_deny);
        
        if ([[json_deny objectForKey:@"success"]intValue] == 1)
        {
//            if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"searchfriends"] isEqualToString:@"yes"]) {
//                ConInviteViewController *con = [[ConInviteViewController alloc]init];
//                con.searchfriends= @"yes";
//                
////                [_selected_array removeAllObjects];
////                [_deletedIndex removeAllObjects];
//                
//               // _selected_array = [[NSMutableArray alloc] init];
//               // _deletedIndex = [[NSMutableArray alloc] init];
//                selectedIDs = @"";
//                
//                [self.navigationController pushViewController:con animated:YES];
//                [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"searchafterreg"];
//                
//                //[SVProgressHUD dismiss];
//            }
//            else
//            {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    [_selected_array removeAllObjects];
                    [_deletedIndex removeAllObjects];
                  //  [self deletetherow];
                    
                    
                    timer = [NSTimer scheduledTimerWithTimeInterval:30.0f target:self
                                                           selector:@selector(sendingRequest)
                                                           userInfo:nil
                                                            repeats:NO];

                    
                }];
                
               // [self performSelector:@selector(deletetherow) withObject:nil afterDelay:1];
                
               // [act stopAnimating];
          //  }
        }
        else
        {
            timer = [NSTimer scheduledTimerWithTimeInterval:30.0f target:self
                                                   selector:@selector(sendingRequest)
                                                   userInfo:nil
                                                    repeats:NO];

            
            alert = [[UIAlertView alloc] initWithTitle:@"Sorry!, Failed to Process Request!"
                                               message:[json_deny objectForKey:@"error"]
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
           // [alert show];
           // [act stopAnimating];
            
        }
            
             }];
        
    }
    else
    {
        DebugLog(@"Request Not Fired");
        
        timer = [NSTimer scheduledTimerWithTimeInterval:30.0f target:self
                                               selector:@selector(sendingRequest)
                                               userInfo:nil
                                                repeats:NO];

    }
    
    
}

-(void)deletetherow
{
    timer = [NSTimer scheduledTimerWithTimeInterval:30.0f target:self
                                           selector:@selector(sendingRequest)
                                           userInfo:nil
                                            repeats:NO];
}



#endif

@end