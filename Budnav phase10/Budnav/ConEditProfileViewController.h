//
//  ConEditProfileViewController.h
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 29/05/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "GAITrackedViewController.h"

@interface ConEditProfileViewController : GAITrackedViewController<UITextFieldDelegate,UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,UIGestureRecognizerDelegate,UIActionSheetDelegate,UIAlertViewDelegate,CLLocationManagerDelegate>
{
    UIScrollView *scviewbudget;
    UIAlertView *alert;
    NSString *base64String,*mobile;
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
}
@property(nonatomic, assign) BOOL controll_check;

@end
