//
//  myAnnotation.h
//  MapView
//
//  Created by dev27 on 5/30/13.
//  Copyright (c) 2013 codigator. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface myAnnotation : NSObject <MKAnnotation>
@property (nonatomic,copy) NSString *title;
@property (nonatomic,assign) CLLocationCoordinate2D coordinate;

-(id) initWithCoordinate:(CLLocationCoordinate2D)coordinate title:(NSString *)title;
@end

/*
 Contacts screen:
 1. Remove dark line below search bar
 2. Swipe to right effect on contact instead of dissolve effect when swiping over a contact to the right to see quick reach icons
 3. Still has to load with the small loader after the big loader, I don’t like that. Make it like on android
 4. Search results don’t have to location design property, they need to be light grey and more up to the centre, like contacts on the contacts screen. (this also goes for recommended results and + screen search bar results)
 5. Show search results directly, not after three letters
 
 Other profile screen:
 1. Always first show personal profile, not business profile, if you have a personal connection  -- done
 2. Load better, now you see an empty profile screen with as name “null” “null”, the first two seconds of loading the screen
 3. Integrate social media links like on android
 4. Replace "Show Normal Card” with “Show Personal Card”
 5. Make “Show Personal Card” and “Remove Connection” text a bit smaller
 6. Make fade out effect for enlarged image smoother, fade in is good

 
 Add screen:
 1. Remove dark line below search bar
 2. Recommended friends list design is not good. Make it the same as app contacts on the contacts screen. Now the last name is the wrong color and blurry, looks like an image actually. Location needs to move up a bit and lighter color grey.
 3. Show search results directly, not after three letters
 
 Then the app crashes a too often and the loader often gets stuck on a certain percentage
 
 
 
 Always first show personal profile, not business profile, if you have a personal connection  -- done
 Load better, now you see an empty profile screen with as name “null” “null”, the first two seconds of loading the screen  -- done
 Only the bottom navigation menu needs to be light instead of bold  -- done
 I will send you new call/sms icons for the green buttons because they are blurry  -- done
 The address display on the profile screen is wrong   -- done
 It needs to be:
 (street) (number)
 (zip code) (city)
 (country)
 
 Some map contacts opens another contact with the same first name  -- done
 Twitter,Soundcloud links open in web browser -- need to open in app  -- Twitter done
 Make fade out effect for enlarged image smoother, fade in is good  -- done
 Call/Sms Buttons are also responding very slow I noticed -- Modified

 The memory issue, which is leading to the crashes, the loader after the first time login
 How are we on filtering?
 Also I noticed all app contact after the letter O are not showing in the contacts screen
*/