//
//  CalloutView.h
//  MapView
//
//  Created by dev27 on 5/30/13.
//  Copyright (c) 2013 codigator. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalloutView : UIView
@property (weak, nonatomic) IBOutlet UILabel *calloutLabel;
@property (weak, nonatomic) IBOutlet UIButton *calloutbutton;
-(IBAction)tapmap:(id)sender; //)tapmap:(id)sender;
@end
