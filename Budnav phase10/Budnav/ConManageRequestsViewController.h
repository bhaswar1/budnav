//
//  ConManageRequestsViewController.h
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 24/05/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConParentTopBarViewController.h"
#import "GAITrackedViewController.h"

@interface ConManageRequestsViewController : ConParentTopBarViewController<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
{
    NSString *base64String,*fullname;
    UIAlertView *alert;
}
@property (nonatomic,retain) NSMutableArray *imported_arr;
@property (nonatomic) int userid;
@end
