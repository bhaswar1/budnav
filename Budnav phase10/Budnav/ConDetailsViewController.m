//
//  ConDetailsViewController.m
//  Contacter
//
//  Created by ios on 10/01/15.
//  Copyright (c) 2015 Esolz. All rights reserved.
//

#import "ConDetailsViewController.h"
#import "AppDelegate.h"
#import "SocialWebViewController.h"
#import "UIImageView+WebCache.h"
#import "ConAccountSettingsViewController.h"
#import "ConInviteViewController.h"
#import "ConAddFriendViewController.h"
#import "iToast.h"
#import <GoogleMaps/GoogleMaps.h>
#import "ConNavigateViewController.h"
#import "ConAddBackUpViewController.h"
#import "SVProgressHUD.h"
#import <AddressBook/AddressBook.h>
#import "DBManager.h"
#import "ConEditProfileViewController.h"
#import "ConPictureProfViewController.h"
#import "ConSyncLoaderViewController.h"
#import "ConRequestViewController.h"
#import "ConNewRequestsViewController.h"
#import "UIImage+animatedGIF.h"
#import "ConQROptionsViewController.h"
#import "QuartzCore/QuartzCore.h"
#import "ConFullSettingsViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"


@interface ConDetailsViewController ()  <UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate>
{
    NSDictionary *json1;
    UIAlertView *alert;
    UIButton *removeconbt, *togglebt;
    int switchprof;
    UILabel *personaltext, *businesstext, *website_lb, *companylb, *desiglb, *backlbl;
    
    UIButton *mobileno, *landphonelb, *mail_lb, *address_lb,*mobileno_busi,*address_lb_busi,*mail_lb_busi;
    
    UIImageView *toggleimage, *address_img, *landphone_img, *mobileimg, *detailimg,*website_img, *mailimg, *companyimg, *desigimg, *address_img_busi, *landphone_img_busi, *mobileimg_busi, *mailimg_busi,*moreImage, *logo_img;
    NSString *mobile,*landline,*mail,*website,*address, *businessAddress,*base64String, *googleMapUrlString, *appleMapUrlString, *desig, *company,*mobile_busi,*mail_busi,*fullname;
    UIImage *profilepic;
    int move;
    double latitude,longitude;
    UIView *mapview,*tapbgview,*businessview,*personalview;
    UIButton *crossbt,*navigatebt,*navigatebtapple;
    UIActivityIndicatorView *activity;
    NSArray *uniquearray;
    NSMutableArray *con_array, *app_contacts, *check_app_cont,*array_menu,*array_image,*arraymenu_busi;
    UIScrollView *boxscroll,*b_boxscroll;
    UIButton *twitterbt, *pinterestbt, *facebookbt, *gplusbt, *youtubebt, *instagrambt, *linkedinbt, *skypebt, *soundcloudbt, *vimeobt;
    int counter, profile_insertion, profile_fetch;
    UIView *divider_1, *divider_2, *divider_3, *divider_4, *divider_5,*busi_comp_divi,*busi_desig_divi,*busi_mob_divi,*busi_mail_divi,*busi_add_divi,*busi_box_divi;
    NSMutableArray *mobileArray;
    NSMutableData *responseData;
    NSString *user_id,*numberstring,*group_id;
    UILabel *prof_name, *street_number_lbl, *zipcode_lbl, *city_lbl, *country_lbl,*zipcode_lbl_busi,*street_number_lbl_busi,*country_lbl_busi;
    UIView *coverview1;
    NSTimer *timea, *timeb;
    int allowpersonal,nobusiness;
    UIView *grayback;
    UIView *coverView;
    BOOL rightmenuOpen;
    UIView *blackView;
    UIImageView *businessImg,*removeImg, *bck_img;
    UITableView *righttable;
    UILabel *lineDivMenu;
    UIView *divider_9;
    UIButton *bgcallsms, *bgcallsms1;
    UIAlertView *callAlert1, *callAlert2, *smsAlert1, *smsAlert2;
    UIButton *rightMenuBack, *rightMenu;
    UIView *overlayMenu;
    UITapGestureRecognizer *overlayTap;
    UIView *mobileback, *landlineback, *mailback, *addressback;
    AppDelegate *del;
    UIView *change;
    NSString *street,*city,*zipcode,*country, *number,*street_busi,*city_busi,*zipcode_busi,*country_busi, *number_busi;
    CGFloat y,x,y1;
    
}

@end

@implementation ConDetailsViewController
@synthesize other,uid,mainview,dict_profile,personal_array, business_array, social_array, phonearr,profdict, mapReturn,response,userid,backBtn,p,filter_arr,groupid,groupname,isadmin,membercount,search;

- (BOOL)hidesBottomBarWhenPushed {
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"From ConDetailsViewController");
    
    del.tabBarController.tabBar.frame = CGRectMake(0, [[UIScreen mainScreen]bounds].size.height, [UIScreen mainScreen].bounds.size.width,44);
    self.tabBarController.tabBar.translucent = YES;
    del.tabBarController.tabBar.hidden = YES;
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    switchprof=0;
    y=0.0;
    x=0.0;
    self.view.backgroundColor=[UIColor whiteColor];
    
    
    self.navigationController.navigationBarHidden=YES;
    
    if ([response isEqualToString:@"yes"]) //=========OTHER'S PROFILE=======//
    {
        user_id =userid;
        
    }
    
    coverView = [[UIView alloc]init];
    coverView.frame = CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.origin.y+64);
    [self.view addSubview:coverView];
    
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
    
    // Add the gradient to the view
    [coverView.layer insertSublayer:gradient atIndex:0];
    
    
    logo_img = [[UIImageView alloc]init];
    logo_img.frame = CGRectMake(27+([UIScreen mainScreen].bounds.size.width-160)/2, 32, 101, 20);
    logo_img.backgroundColor = [UIColor clearColor];
    
    logo_img.image = [UIImage imageNamed:@"new_logo"];
    [coverView addSubview:logo_img];
    
    
    //===============Back button===========//
    
    if ([mapReturn isEqualToString:@"mapBack"]) {
        
        backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        backBtn.frame = CGRectMake(9, 28, [UIImage imageNamed:@"backnew_profileUnselected"].size.width, [UIImage imageNamed:@"backnew_profileUnselected"].size.height);
        backBtn.backgroundColor=[UIColor clearColor];
        [backBtn setBackgroundImage:[UIImage imageNamed:@"backnew_profileUnselected"] forState:UIControlStateNormal];
        [backBtn setBackgroundImage:[UIImage imageNamed:@"backnew_profileSelected"] forState:UIControlStateSelected];
        [backBtn setBackgroundImage:[UIImage imageNamed:@"backnew_profileSelected"] forState:UIControlStateHighlighted];
        [backBtn addTarget:self action:@selector(gobackoption:) forControlEvents:UIControlEventTouchUpInside];
        [coverView addSubview:backBtn];
        
        UILabel *labelHead = [[UILabel alloc]initWithFrame:CGRectMake(30, 22, 100, 40)];
        labelHead.text = @"Map";
        labelHead.backgroundColor = [UIColor clearColor];
        labelHead.textColor = [UIColor whiteColor];
        [coverView addSubview:labelHead];
        
        
    }
    else{
        
        bck_img=[[UIImageView alloc]initWithFrame:CGRectMake(15,32,12,20)];//(13, 39, 10, 22)];
        bck_img.image=[UIImage imageNamed:@"back3"];
        [coverView addSubview:bck_img];
        
        backlbl=[[UILabel alloc]initWithFrame:CGRectMake(bck_img.frame.origin.x+bck_img.frame.size.width+5, 31, 54, 20)];
        backlbl.text=@"Back";
        backlbl.textColor=[UIColor whiteColor];
        backlbl.backgroundColor=[UIColor clearColor];
        
        backBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, bck_img.frame.origin.y-5, backlbl.frame.origin.x+backlbl.frame.size.width, 64-bck_img.frame.origin.y+5)];
        [backBtn setTitle:@"" forState:UIControlStateNormal];
        backBtn.backgroundColor=[UIColor clearColor];
        backBtn.tag=p;
        [backBtn setTitle:@"" forState:UIControlStateNormal];
        [backBtn addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDown];
        [backBtn addTarget:self action:@selector(changecoloragain) forControlEvents:UIControlEventTouchDragExit];
        [backBtn addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDragEnter];
        [backBtn addTarget:self action:@selector(gobackoption:) forControlEvents:UIControlEventTouchUpInside];
        [coverView addSubview:backBtn];
        
    }
    
    
    moreImage=[[UIImageView alloc]initWithFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-37, 32, 22,19)];
    [moreImage setImage:[UIImage imageNamed:@"new_menu"]];
    [coverView addSubview:moreImage];
    
    rightMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [rightMenu setFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-55,[[UIApplication sharedApplication]statusBarFrame].size.height, 55,coverView.frame.size.height-[[UIApplication sharedApplication]statusBarFrame].size.height)];
    [rightMenu setBackgroundImage:nil forState:UIControlStateNormal];
    [rightMenu setBackgroundImage:nil forState:UIControlStateSelected];
    [rightMenu setBackgroundImage:nil forState:UIControlStateHighlighted];
    [rightMenu setBackgroundColor:[UIColor clearColor]];
    [rightMenu addTarget:self action:@selector(rightMenu:) forControlEvents:UIControlEventTouchUpInside];
    [coverView addSubview:rightMenu];
    rightMenu.enabled=NO;
    rightmenuOpen = NO;
 
    
}

-(void)coverMethod: (NSTimer *)timeav
{
    [timeav invalidate];
    [coverview1 removeFromSuperview];
    [SVProgressHUD dismiss];
}

- (void) runSpinAnimationOnView:(UIImageView *)view duration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat;
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ * rotations * duration ];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = repeat;
    
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

-(void)viewDidAppear:(BOOL)animated
{
    
    [super viewDidAppear:YES];
    
    del.tabBarController.tabBar.hidden = YES;
    
    [self.tabBarController.tabBar setHidden:YES];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    nobusiness=0;
    move=0;
    profile_insertion=0;
    profile_fetch=0;
    allowpersonal=0;
    
    self.view.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
    
    del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainview = [[UIView alloc]initWithFrame:CGRectMake(0, 65, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    [self.view addSubview:mainview];
    
    mainview.backgroundColor=[UIColor clearColor];
    
    
    mainscroll=[[UIScrollView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, [UIScreen mainScreen].bounds.size.width, mainview.frame.size.height)];
    mainscroll.userInteractionEnabled=YES;
    mainscroll.scrollEnabled=YES;
    mainscroll.showsVerticalScrollIndicator=YES;
    //    mainscroll.contentSize=CGSizeMake(320.0f, 500.0f);
    mainscroll.showsVerticalScrollIndicator= NO;
    //    [mainscroll setBackgroundColor:[UIColor redColor]];
    [mainview addSubview:mainscroll];
    
    DebugLog(@"mainscroll frame: %@",NSStringFromCGRect(mainscroll.frame));
    activity = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2.38, 190, 40, 40)];
    activity.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [activity setColor:[UIColor blackColor]];
    //    activity.layer.zPosition=6;
    [mainscroll addSubview:activity];
    [activity startAnimating];
    
    NSOperationQueue *queue=[NSOperationQueue new];
    NSInvocationOperation *op=[[NSInvocationOperation alloc] initWithTarget:self selector:@selector(loaddata) object:nil];
    [queue addOperation:op];
    
    [mainview bringSubviewToFront:boxscroll];
    rightMenu.enabled=YES;
    rightmenuOpen = YES;
}


-(void)loaddata
{
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"firstProfile"] isEqualToString:@"yes"])
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSError *error;
        NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=profile&id=%@&access_token=%@&device_id=%@&image=true",user_id,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
        
        //DebugLog(@"profile url: %@",urlString1);
        
        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
        
        if (signeddataURL1 != nil)
            
            json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                    options:kNilOptions
                                                      error:&error];
        
        else
        {
            DebugLog(@"no connnn profile");
            alert = [[UIAlertView alloc] initWithTitle:@"Error in Profile Server Connection!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
        }
        
        NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
        //DebugLog(@"err  %@",errornumber);
        
        if (![errornumber isEqualToString:@"0"])
        {
            [activity removeFromSuperview];
            NSString *err_str = [json1 objectForKey:@"error"];
            alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                               message:err_str
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
        }
        
        else
            
        {
            //DebugLog(@"data: %@",json1);
            dict_profile = [[NSMutableDictionary alloc]init];
            
            profdict= [json1 objectForKey:@"details"];
            
            dict_profile = [profdict objectForKey:@"profile"];
            
            [self performSelectorOnMainThread:@selector(startDBInserta:)
                                   withObject:nil
                                waitUntilDone:YES];
            
            
            [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"firstProfile"];
            //DebugLog(@"dict pro%@",dict_profile);
            
            if (![[NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]] isKindOfClass:[NSNull class]] && ![[NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]] isEqualToString:@""] && ![[NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]] isEqualToString:@"(null)"] && [[NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]] length]>0)
                
                fullname= [NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]];
            
                      [prof_name removeFromSuperview];
            base64String= [profdict objectForKey:@"image"];
            
            if ([user_id intValue] == [[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]intValue])
            {
                switchprof=0;
                [self loadViews];
            }
            else
            {
                if ([[profdict objectForKey:@"friend"]intValue] == 1)
                {
                    switchprof=0;
                    [self loadViews];
                }
                
                else if ([[profdict objectForKey:@"business"] intValue]==1)
                {
                    switchprof=1;
                    allowpersonal=1;
                    [self business];
                }
            }
            grayback = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 110.0f)];
            [grayback setBackgroundColor:[UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f]];
            [mainscroll addSubview:grayback];
            
            [self performSelectorOnMainThread:@selector(displaydata) withObject:nil waitUntilDone:NO];
        }
        
        
    }
    
     /////////////////////////////////////////////   Load from Local DB   ////////////////////////////////////////////////////////////////////
    
    else
    {
        allowpersonal=0;
        DebugLog(@"eittttto etatei ");
        NSDictionary *localDBProfile = [[NSDictionary alloc]init];
        
        localDBProfile = [[DBManager getSharedInstance]fetchProfile:[user_id intValue]];
        
        DebugLog(@"this gives: %@",localDBProfile);
        
        NSArray * allKeys = [localDBProfile allKeys];
        
        if ([allKeys count] == 0 || [[localDBProfile objectForKey:@"name"] isKindOfClass:[NSNull class]] || [[localDBProfile objectForKey:@"name"] length]==0 || [[localDBProfile objectForKey:@"name"] isEqualToString:@"(null)"])
            
        {
            DebugLog(@"this gives null profile");
            
            [self reloadDataFromWeb];
        }
        else
        {
            
            if ([user_id intValue] == [[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"] intValue] && [[[NSUserDefaults standardUserDefaults]objectForKey:@"profile_edited"] isEqualToString:@"yes"])
                
                [self reloadDataFromWeb];
            
            dict_profile = [[NSMutableDictionary alloc]init];
            
            
            dict_profile = [localDBProfile mutableCopy];
            
            
            
            UIImageView *prof_img = [[UIImageView alloc]initWithFrame:CGRectMake(15, 75-64, 90, 90)];
            
            [grayback addSubview:prof_img];
            
            
            [self performSelector:@selector(reloadDataFromWeb)];
        }
        if (![[NSUserDefaults standardUserDefaults] boolForKey:@"error"])
        {
            fullname= [NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]];
            if ([profdict objectForKey:@"image"] == nil) {
                base64String= [profdict objectForKey:@"thumb"];
            }
            else
                base64String= [profdict objectForKey:@"image"];
            grayback = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 110.0f)];
            [grayback setBackgroundColor:[UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f]];
            [mainscroll addSubview:grayback];
            prof_name.frame = CGRectMake(0, 0, 0, 0);
            [grayback addSubview:prof_name];
            [self performSelectorOnMainThread:@selector(displaydata) withObject:nil waitUntilDone:YES];
        }
        else
        {
            [activity removeFromSuperview];
            UIAlertView *showalert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Cannot load data. Please log in again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [showalert show];
        }
        
        
    }
    
    
}

-(void)displaydata
{
    [activity removeFromSuperview];
    
    prof_name = [[UILabel alloc]init]; //WithFrame:CGRectMake(112, 72, 196, 60)];
    prof_name.backgroundColor=[UIColor clearColor];
    prof_name.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
    if ([fullname isKindOfClass:[NSNull class]] || [fullname isEqualToString:@""] || [fullname isEqualToString:@"(null) (null)"] || [fullname length]==0) {
        
        prof_name.text = @"";
        prof_name.frame = CGRectMake(0, 0, 0, 0);
        DebugLog(@"PROFILE NAME:%@",prof_name.text);
    }
    else
    {
        prof_name.text=fullname;
        DebugLog(@"PROFILE NAME:%@",prof_name.text);
        prof_name.font=[UIFont fontWithName:@"ProximaNova-Bold" size:18];
        prof_name.numberOfLines=0;
        prof_name.frame=CGRectMake(120, 75-70, 195, 60);
        [grayback addSubview:prof_name];
        // [prof_name removeFromSuperview];
        
    }
    
    
    UIImageView *prof_img = [[UIImageView alloc]initWithFrame:CGRectMake(15, 75-64, 90, 90)];
    
    [grayback addSubview:prof_img];
    
    if ([base64String length] >6)
    {
        NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
        
        
        profilepic = [UIImage imageWithData:decodedData];
        prof_img.image=profilepic;
        prof_img.contentMode= UIViewContentModeScaleAspectFill;
        prof_img.clipsToBounds=YES;
    }
    
    prof_img.userInteractionEnabled=YES;
    
    UITapGestureRecognizer *propictap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(detailpic:)];
    
    [prof_img addGestureRecognizer:propictap];
    
    
    
    toggleimage = [[UIImageView alloc]init];
    
    toggleimage.backgroundColor=[UIColor clearColor];
    
    [mainscroll addSubview:toggleimage];
    
    
    personaltext = [[UILabel alloc]initWithFrame:CGRectMake(132, 138-60, 75, 25)];
    [mainscroll addSubview:personaltext];
    personaltext.backgroundColor=[UIColor clearColor];
    if (switchprof == 0)
        personaltext.textColor=[UIColor whiteColor];
    else
        personaltext.textColor=[UIColor lightGrayColor];
    
    personaltext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:15];
    
    businesstext = [[UILabel alloc]initWithFrame:CGRectMake(225, 138-60, 75, 25)];
    
    [mainscroll addSubview:businesstext];
    
    businesstext.backgroundColor=[UIColor clearColor];
    
    if (switchprof == 0)
        businesstext.textColor=[UIColor lightGrayColor];
    
    else
        businesstext.textColor=[UIColor whiteColor];
    
    businesstext.font=[UIFont fontWithName:@"ProximaNova-Regular" size:15];
    
    if ([user_id intValue] == [[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"] intValue])
    {
        UIButton *editprofbt= [UIButton buttonWithType:UIButtonTypeCustom];
        [grayback addSubview:editprofbt];
        [editprofbt setBackgroundImage:[UIImage imageNamed:@"editprofbt.png"] forState:UIControlStateNormal];
        editprofbt.frame = CGRectMake(120, 136.5f-60, 185, 28.5f);
        [editprofbt setTitle:@"edit profile" forState:UIControlStateNormal];
        [editprofbt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [editprofbt addTarget:self action:@selector(editprofile_func:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        //============================CALL & SMS BUTTON FOR OTHER USER==================//
        
        bgcallsms = [[UIButton alloc]initWithFrame:CGRectMake(120, 136.5f-72.0f, 91.3f, 35)];
        [grayback addSubview:bgcallsms];
        bgcallsms.userInteractionEnabled=YES;
        [bgcallsms setBackgroundImage:[UIImage imageNamed:@"call_profile"] forState:UIControlStateNormal];
        //        [bgcallsms setBackgroundImage:[UIImage imageNamed:@"callend_profile"] forState:UIControlStateSelected];
        //        [bgcallsms setBackgroundImage:[UIImage imageNamed:@"callend_profile"] forState:UIControlStateHighlighted];
        [bgcallsms addTarget:self action:@selector(phonefunc) forControlEvents:UIControlEventTouchUpInside];
        
        bgcallsms1 = [[UIButton alloc]initWithFrame:CGRectMake(212.3f, 136.5f-72.0f, 91.3f, 35)];
        [grayback addSubview:bgcallsms1];
        bgcallsms1.userInteractionEnabled=YES;
        [bgcallsms1 setBackgroundImage:[UIImage imageNamed:@"sms_profile"] forState:UIControlStateNormal];
        
        [bgcallsms1 addTarget:self action:@selector(chatfunc:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    if ([user_id intValue] == [[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]intValue])
    {
        switchprof=0;
        [activity stopAnimating];
        [self loadViews];
    }
    else
    {
        if ([[profdict objectForKey:@"friend"]intValue] == 1)
        {
            switchprof=0;
            [activity stopAnimating];
            [self loadViews];
        }
        
        else if ([[profdict objectForKey:@"business"] intValue]==1)
        {
            switchprof=1;
            [activity stopAnimating];
            [self business];
        }
        DebugLog(@"fields are: %d , %d",[[profdict objectForKey:@"own"]intValue], switchprof);
    }
    
}



-(void)changecoloragain
{
    backlbl.alpha = 1.0f;
    bck_img.alpha = 1.0f;
    
}
-(void)changecolor
{
    backlbl.alpha = 0.5f;
    bck_img.alpha = 0.5f;
}

-(void)business
{
    phonearr=[[NSMutableArray alloc]init];
    [divider_5 removeFromSuperview];
    [personalview removeFromSuperview];
    [boxscroll removeFromSuperview];
    [activity removeFromSuperview];
    
    mobile=nil;
    landline=nil;
    mail=nil;
    street=nil;
    number=nil;
    zipcode=nil;
    city=nil;
    country=nil;
    boxscroll=nil;
    personalview=nil;
    [mobileno removeFromSuperview];
    [landphonelb removeFromSuperview];
    [mail_lb removeFromSuperview];
    [address_lb removeFromSuperview];
    personalview=nil;
    [activity stopAnimating];
    businessview=[[UIScrollView alloc]initWithFrame:CGRectMake(0.0f, 110.0f, [UIScreen mainScreen].bounds.size.width, mainscroll.frame.size.height)];
    businessview.userInteractionEnabled=YES;
    [mainscroll addSubview:businessview];
    
    mobileno_busi = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [mobileno_busi setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 40.0f, 0, 0)];
    
    mobileno_busi.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    [mobileno_busi setBackgroundImage:[UIImage imageNamed:@"detbuttonNormal"] forState:UIControlStateNormal];
    
    [mobileno_busi setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateHighlighted];
    
    [mobileno_busi setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateSelected];
    
    [mobileno_busi.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
    
    [mobileno_busi setTitleColor:[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f]forState:UIControlStateNormal];
    mobileno_busi.userInteractionEnabled = YES;
    
    [mobileno_busi addTarget:self action:@selector(mobileFun:) forControlEvents:UIControlEventTouchUpInside];
    
    [businessview addSubview:mobileno_busi];
    
    //////////////////////////////>>>
    
    mobileimg_busi = [[UIImageView alloc]init];//WithFrame:CGRectMake(10.0f, 10.0f, [UIImage imageNamed:@"mobile_profile"].size.width, [UIImage imageNamed:@"mobile_profile"].size.height+12)];//WithFrame:CGRectMake(15, 195-60, 14, 13.5f)];
    mobileimg_busi.image=[UIImage imageNamed:@"mobile_profile"];
    [mobileno_busi addSubview:mobileimg_busi];
    
    
    mail_lb_busi = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [mail_lb_busi setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 40, 0, 0)];
    
    mail_lb_busi.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    [mail_lb_busi setBackgroundImage:[UIImage imageNamed:@"detbuttonNormal"] forState:UIControlStateNormal];
    
    [mail_lb_busi setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateHighlighted];
    
    [mail_lb_busi setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateSelected];
    
    [mail_lb_busi.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
    
    [mail_lb_busi setTitleColor:[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f]forState:UIControlStateNormal];
    
    [mail_lb_busi addTarget:self action:@selector(mailFun:) forControlEvents:UIControlEventTouchUpInside];
    
    [businessview addSubview:mail_lb_busi];
    
    
    mailimg_busi =[[UIImageView alloc]init];//WithFrame:CGRectMake(10, 10, [UIImage imageNamed:@"email_profile"].size.width, [UIImage imageNamed:@"email_profile"].size.height+12) ];//WithFrame:CGRectMake(15, divider_1.frame.origin.y+divider_1.frame.size.height+13, 16, 11)];
    mailimg_busi.image=[UIImage imageNamed:@"email_profile"];
    [mail_lb_busi addSubview:mailimg_busi];
    
    if ([other isEqualToString:@"yes"]) {
        
        mobileno_busi.userInteractionEnabled = YES;
        
        landphonelb.userInteractionEnabled = YES;
        
        mail_lb_busi.userInteractionEnabled = YES;
        
        website_lb.userInteractionEnabled = YES;
    }else{
        
        mobileno_busi.userInteractionEnabled = NO;
        
        landphonelb.userInteractionEnabled = NO;
        
        mail_lb_busi.userInteractionEnabled = NO;
        
        //        website_lb.userInteractionEnabled = NO;
    }
    
    busi_mail_divi = [[UIView alloc]init];//WithFrame:CGRectMake(14.5f, website_lb.frame.origin.y+website_lb.frame.size.height+11, 291, 1)];
    
    [busi_mail_divi setBackgroundColor:[UIColor colorWithRed:(239.0f / 255.0f) green:(239.0f / 255.0f) blue:(239.0f / 255.0f) alpha:1.0f]];
    [businessview addSubview:busi_mail_divi];
    
    address_lb_busi = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [address_lb_busi setTitleEdgeInsets:UIEdgeInsetsMake(14.0f, 40, 0, 0)];
    
    address_lb_busi.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    
    [address_lb_busi.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
    
    address_lb_busi.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    [address_lb_busi setTitleColor:[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f]forState:UIControlStateNormal];
    
    [address_lb_busi addTarget:self action:@selector(openmap:) forControlEvents:UIControlEventTouchUpInside];
    
    [businessview addSubview:address_lb_busi];
    
    street_number_lbl_busi = [[UILabel alloc] init];
    street_number_lbl_busi.backgroundColor = [UIColor clearColor];
    street_number_lbl_busi.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
    street_number_lbl_busi.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    [address_lb_busi addSubview:street_number_lbl_busi];
    
    zipcode_lbl_busi = [[UILabel alloc] init];
    zipcode_lbl_busi.backgroundColor = [UIColor clearColor];
    zipcode_lbl_busi.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
    zipcode_lbl_busi.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    [address_lb_busi addSubview:zipcode_lbl_busi];
    
    country_lbl_busi = [[UILabel alloc] init];
    country_lbl_busi.backgroundColor = [UIColor clearColor];
    country_lbl_busi.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
    country_lbl_busi.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    [address_lb_busi addSubview:country_lbl_busi];
    
    address_img_busi=[[UIImageView alloc]init];//WithFrame:CGRectMake(15,divider_2.frame.origin.y+divider_2.frame.size.height+11,12,17)];
    address_img_busi.image=[UIImage imageNamed:@"map_profile"];
    [address_lb_busi addSubview:address_img_busi];
    
    divider_3 = [[UIView alloc]init];//WithFrame:CGRectMake(14.5f, address_lb_busi.frame.origin.y+address_lb_busi.frame.size.height+5, 291, 1)];
    
    
    [businessview addSubview:divider_3];
    
    companyimg = [[UIImageView alloc]init];//WithFrame:CGRectMake(15, divider_1.frame.origin.y+divider_1.frame.size.height+35, 14, 13.5f)];
    
    companyimg.image=[UIImage imageNamed:@"business name.png"];
    
    [businessview addSubview:companyimg];
    
    
    
    companylb = [[UILabel alloc]init];//WithFrame:CGRectMake(38, divider_1.frame.origin.y+divider_1.frame.size.height+32, 275, 19)];
    
    [businessview addSubview:companylb];
    
    companylb.backgroundColor=[UIColor clearColor];
    companylb.textColor = [UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
    companylb.font= [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    
    busi_comp_divi = [[UIView alloc] init];
    [busi_comp_divi setBackgroundColor:[UIColor colorWithRed:(239.0f / 255.0f) green:(239.0f / 255.0f) blue:(239.0f / 255.0f) alpha:1.0f]];
    [businessview addSubview:busi_comp_divi];
    
    desigimg = [[UIImageView alloc]init];//WithFrame:CGRectMake(15, divider_1.frame.origin.y+divider_1.frame.size.height+35, 14, 13.5f)];
    
    desigimg.image=[UIImage imageNamed:@"Designation.png"];
    
    [businessview addSubview:desigimg];
    
    
    
    desiglb = [[UILabel alloc]init];//WithFrame:CGRectMake(38, divider_1.frame.origin.y+divider_1.frame.size.height+32, 275, 19)];
    
    [businessview addSubview:desiglb];
    desiglb.backgroundColor=[UIColor clearColor];
    desiglb.textColor = [UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
    desiglb.font= [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    
    busi_desig_divi = [[UIView alloc] init];
    [busi_desig_divi setBackgroundColor:[UIColor colorWithRed:(239.0f / 255.0f) green:(239.0f / 255.0f) blue:(239.0f / 255.0f) alpha:1.0f]];
    [businessview addSubview:busi_desig_divi];
    
    if ([dict_profile objectForKey:@"b_company"] != false && ![[dict_profile objectForKey:@"b_company"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_company"] length] > 1 && ![[dict_profile objectForKey:@"b_company"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"b_company"] isEqualToString:@""]){
        
        nobusiness=1;
        company= [NSString stringWithFormat:@"%@",[[dict_profile objectForKey:@"b_company"] stringByReplacingOccurrencesOfString:@"amp;" withString:@""]];
        
        companyimg.frame = CGRectMake(13, 17, 14, 13.5f);
        companyimg.image = [UIImage imageNamed:@"company"];
        DebugLog(@"grayback frame: %@",NSStringFromCGRect(grayback.frame));
        companylb.frame = CGRectMake(38, 0, 275, 50); // businesstext.frame.origin.y+businesstext.frame.size.height+19, 275, 30.0f);
        companylb.text=company;
        busi_comp_divi.frame = CGRectMake(0,  companylb.frame.origin.y+companylb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        y1=busi_comp_divi.frame.size.height+busi_comp_divi.frame.origin.y;
        DebugLog(@"company divider frame: %@",NSStringFromCGRect(busi_comp_divi.frame));
    }
    else{
        company = @"";
        
        companyimg.frame = CGRectMake(13, businesstext.frame.origin.y+businesstext.frame.size.height+24, 14, 0);
        
        
        companylb.frame = CGRectMake(38, grayback.frame.origin.y+grayback.frame.size.height, 275, 0);
        
        busi_comp_divi.frame = CGRectMake(0,  companylb.frame.origin.y+companylb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
    }
    
    //===============BUSI-DESIGNATION==============//
    
    
    if ([dict_profile objectForKey:@"b_function"] != false && ![[dict_profile objectForKey:@"b_function"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_function"] length] > 1 && ![[dict_profile objectForKey:@"b_function"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"b_function"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"b_function"] isEqualToString:@""])
    {
        
        desig= [NSString stringWithFormat:@"%@",[[dict_profile objectForKey:@"b_function"] stringByReplacingOccurrencesOfString:@"amp;" withString:@""]];
        DebugLog(@"company bhaswar: %@",desig);
        
        desigimg.frame = CGRectMake(13, busi_comp_divi.frame.origin.y+busi_comp_divi.frame.size.height+15.8, 15, 15);
        desigimg.image = [UIImage imageNamed:@"designation"];
        
        desiglb.text=desig;
        desiglb.frame = CGRectMake(38, busi_comp_divi.frame.origin.y+busi_comp_divi.frame.size.height, 275, 50);
        
        //        desiglb.backgroundColor=[UIColor redColor];
        nobusiness=1;
        
        busi_desig_divi = [[UIView alloc]init];//WithFrame:CGRectMake(14.5f, 248-60, 291, 1)];
        
        //    divider_1.image=[UIImage imageNamed:@"divider-prof.png"];
        
        [businessview addSubview:busi_desig_divi];
        
        busi_desig_divi.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
        
        
        busi_desig_divi.frame = CGRectMake(0,  desiglb.frame.origin.y+desiglb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        y1=busi_desig_divi.frame.size.height+busi_desig_divi.frame.origin.y;
        
    }
    else{
        desig=@"";
        
        desigimg.frame = CGRectMake(13, busi_comp_divi.frame.origin.y+busi_comp_divi.frame.size.height+15.8, 15, 0);
        
        desiglb.frame = CGRectMake(38, busi_comp_divi.frame.origin.y+busi_comp_divi.frame.size.height, 275, 0);
        
        busi_desig_divi = [[UIView alloc]init];//WithFrame:CGRectMake(14.5f, 248-60, 291, 1)];
        
        //    divider_1.image=[UIImage imageNamed:@"divider-prof.png"];
        
        [businessview addSubview:busi_desig_divi];
        
        busi_desig_divi.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
        
        
        busi_desig_divi.frame = CGRectMake(0,  desiglb.frame.origin.y+desiglb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
    }
    
    
    //=================BUSI-MOBILE================//
    
    if ([dict_profile objectForKey:@"b_phone_num"] != false && ![[dict_profile objectForKey:@"b_phone_num"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_phone_num"] length] > 1 && ![[dict_profile objectForKey:@"b_phone_num"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"b_phone_num"] isKindOfClass:[NSNull class]] && ![[dict_profile objectForKey:@"b_phone_num"] isEqualToString:@""]){
        
        
        
        mobile_busi = [NSString stringWithFormat:@"%@%@",[dict_profile objectForKey:@"b_phone_pre"],[dict_profile objectForKey:@"b_phone_num"]];
        
        DebugLog(@"LANDLINE:%@",mobile_busi);
        
        //            [landphone_img setHidden:YES];
        //
        //            [landphonelb setHidden:YES];
        
        [mobileno_busi setHidden:NO];
        
        mobileimg_busi.frame = CGRectMake(13, 19, [UIImage imageNamed:@"mobile_profile"].size.width/1.5,[UIImage imageNamed:@"mobile_profile"].size.height/1.5);
        DebugLog(@"mobile img busi %@",NSStringFromCGRect(mobileimg_busi.frame));
        mobileno_busi.frame = CGRectMake(0, busi_desig_divi.frame.origin.y+busi_desig_divi.frame.size.height, [UIScreen mainScreen].bounds.size.width, 50);
        mobileno_busi.titleLabel.textAlignment=NSTextAlignmentCenter;
        [mobileno_busi setBackgroundImage:nil forState:UIControlStateNormal]; //[UIImage imageNamed:@""] forState:UIControlStateNormal];
        mobileno_busi.userInteractionEnabled = YES;
        [mobileno_busi addTarget:self action:@selector(mobileFun:) forControlEvents:UIControlEventTouchUpInside];
        [mobileno_busi setTitle:[NSString stringWithFormat:@"%@", mobile_busi] forState:UIControlStateNormal];
        
        busi_mob_divi = [[UIView alloc]init];//WithFrame:CGRectMake(14.5f, mail_lb_busi.frame.origin.y+mail_lb_busi.frame.size.height+15, 291, 1)];
        
        [busi_mob_divi setBackgroundColor:[UIColor colorWithRed:(239.0f / 255.0f) green:(239.0f / 255.0f) blue:(239.0f / 255.0f) alpha:1.0f]];
        
        [businessview addSubview:busi_mob_divi];
        
        
        busi_mob_divi.frame = CGRectMake(0,  mobileno_busi.frame.origin.y+mobileno_busi.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        y1=busi_mob_divi.frame.size.height+busi_mob_divi.frame.origin.y;
        
        numberstring=mobile_busi;
        [phonearr addObject:mobile_busi];
        
        nobusiness=1;
        
    }
    
    else{
        mobile_busi=@"";
        
        mobileimg_busi.frame = CGRectMake(13, 22, [UIImage imageNamed:@"mobile_profile"].size.width/1.5,0);
        mobileno_busi.frame = CGRectMake(0, busi_desig_divi.frame.origin.y+busi_desig_divi.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        
        busi_mob_divi = [[UIView alloc]init];//WithFrame:CGRectMake(14.5f, mail_lb_busi.frame.origin.y+mail_lb_busi.frame.size.height+15, 291, 1)];
        [busi_mob_divi setBackgroundColor:[UIColor colorWithRed:(239.0f / 255.0f) green:(239.0f / 255.0f) blue:(239.0f / 255.0f) alpha:1.0f]];
        [businessview addSubview:busi_mob_divi];
        
        busi_mob_divi.frame = CGRectMake(0,  mobileno_busi.frame.origin.y+mobileno_busi.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        
    }
    
    //============BUSI-MAIL===========//
    
    
    if ([dict_profile objectForKey:@"b_email"] != false && ![[dict_profile objectForKey:@"b_email"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_email"] length] > 1 && ![[dict_profile objectForKey:@"b_email"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"b_email"] isEqualToString:@""])
    {
        mail_busi= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_email"]];
        
        [mail_lb_busi setHidden:NO];
        
        mailimg_busi.frame= CGRectMake(13, 19, [UIImage imageNamed:@"email_profile"].size.width/1.5, [UIImage imageNamed:@"email_profile"].size.height/1.5);
        //            mailimg_busi.frame = CGRectMake(13, 18, 14.5f, 14);
        
        mail_lb_busi.frame = CGRectMake(0, busi_mob_divi.frame.origin.y+busi_mob_divi.frame.size.height, [UIScreen mainScreen].bounds.size.width, [UIImage imageNamed:@"detbuttonNormal"].size.height+7.5);
        //        mail_lb_busi.backgroundColor=[UIColor redColor];
        DebugLog(@"Mail frame: %@",NSStringFromCGRect(mail_lb_busi.frame));
        mail_lb_busi.userInteractionEnabled = YES;
        [mail_lb_busi setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [mail_lb_busi addTarget:self action:@selector(mailFun:) forControlEvents:UIControlEventTouchUpInside];
        [mail_lb_busi setTitle:mail_busi forState:UIControlStateNormal];
        mailback.frame = CGRectMake(0, divider_1.frame.origin.y+divider_1.frame.size.height, [UIScreen mainScreen].bounds.size.width, 43);
        nobusiness=1;
        
    }
    else{
        
        mail_busi= @"";
        
        [mailimg_busi setHidden:YES];
        
        mailimg_busi.frame= CGRectMake(10, 20, [UIImage imageNamed:@"email_profile"].size.width/1.5, 0);
        //            mailimg_busi.frame = CGRectMake(13, 18, 14.5f, 14);
        
        mail_lb_busi.frame = CGRectMake(0, busi_mob_divi.frame.origin.y+busi_mob_divi.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        
        mailback.frame = CGRectMake(0, divider_1.frame.origin.y+divider_1.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        
    }
    
    
    if (![mail_busi isEqualToString:@""]){   //&& ![website isEqualToString:@""]) {
        
        busi_mail_divi.frame = CGRectMake(0,  mail_lb_busi.frame.origin.y+mail_lb_busi.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        y1=busi_mail_divi.frame.origin.y+busi_mail_divi.frame.size.height;
    }
    
    else {
        
        busi_mail_divi.frame = CGRectMake(0,  mail_lb_busi.frame.origin.y+mail_lb_busi.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
    }
    
    if ([dict_profile objectForKey:@"b_city"] != false && ![[dict_profile objectForKey:@"b_city"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_city"] length]!=0 && ![[dict_profile objectForKey:@"b_city"] isEqualToString:@""])
    {
        city_busi= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_city"]];
        nobusiness=1;
    }
    
    else
        city_busi=@"";
    
    if ([dict_profile objectForKey:@"b_street"] != false && ![[dict_profile objectForKey:@"b_street"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_street"] length]!=0 && ![[dict_profile objectForKey:@"b_street"] isEqualToString:@""])
    {
        street_busi= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_street"]];
        nobusiness=1;
    }
    
    else
        street_busi =@"";
    
    if ([dict_profile objectForKey:@"b_zipcode"] != false && ![[dict_profile objectForKey:@"b_zipcode"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_zipcode"] length]!=0 && ![[dict_profile objectForKey:@"b_zipcode"] isEqualToString:@""])
        
    {
        nobusiness=1;
        zipcode_busi= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_zipcode"]];
        
    }
    else
        zipcode_busi=@"";
    
    if ([dict_profile objectForKey:@"b_country"] != false && ![[dict_profile objectForKey:@"b_country"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_country"] length]!=0 && ![[dict_profile objectForKey:@"b_country"] isEqualToString:@""])
    {
        country_busi= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_country"]];
        nobusiness=1;
    }
    else
        country_busi=@"";
    if ([dict_profile objectForKey:@"b_housenumber"] != false && ![[dict_profile objectForKey:@"b_housenumber"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_housenumber"] length]!=0 && ![[dict_profile objectForKey:@"b_housenumber"] isEqualToString:@""])
    {
        number_busi= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_housenumber"]];
        nobusiness=1;
    }
    else
        number_busi= @"";
    
    
    if (![city_busi  isEqualToString: @""] || ![street_busi  isEqualToString: @""] || ![zipcode_busi  isEqualToString: @""] || ![country_busi  isEqualToString: @""] || ![number_busi isEqualToString:@""]) {
        
        nobusiness=1;
        NSString *line1,*line2,*line3;
        
        y=0;
        if ([street_busi length]!=0 && [number_busi length]!=0)
        {
            line1= [NSString stringWithFormat:@"%@, %@,",street_busi,number_busi];
        }
        else if ([street_busi length]==0 && [number_busi length]!=0) {
            line1= [NSString stringWithFormat:@"%@,",number_busi];
        }
        else if ([street_busi length]!=0 && [number_busi length]==0){
            line1 = [NSString stringWithFormat:@"%@,",street_busi];
        }
        else{
            line1= @"";
        }
        
        if ([zipcode_busi length]!=0 && [city_busi length]!=0) {
            line2 = [NSString stringWithFormat:@"%@, %@",zipcode_busi,city_busi];
        }
        else if ([zipcode_busi length]==0 && [city_busi length]!=0)
        {
            line2 = [NSString stringWithFormat:@"%@",city_busi];
        }
        else if ([zipcode_busi length]!=0 && [city_busi length]==0)
        {
            line2 = [NSString stringWithFormat:@"%@,",zipcode_busi];
        }
        else
        {
            line2 = @"";
        }
        
        if ([country_busi length]!=0)
        {
            line3 = [NSString stringWithFormat:@"%@",country_busi];
        }
        else
        {
            line3 = @"";
        }
        
        if ([line1 length]==0 && [line2 length]==0 && [line3 length]!=0) {
            businessAddress= [NSString stringWithFormat:@"%@",line3];
            country_lbl_busi.frame=CGRectMake(38, 10, 250, 20);
            country_lbl_busi.text=line3;
            [address_lb_busi addSubview:country_lbl_busi];
            y=30;
            
        }
        else if ([line1 length]==0 && [line2 length]!=0 && [line3 length]!=0)
        {
            businessAddress=[NSString stringWithFormat:@"%@ \n %@",line2,line3];
            zipcode_lbl_busi.frame=CGRectMake(38, 10, 250, 20);
            country_lbl_busi.frame=CGRectMake(38, zipcode_lbl_busi.frame.origin.y+zipcode_lbl_busi.frame.size.height+10, 250, 20);
            zipcode_lbl_busi.text=[NSString stringWithFormat:@"%@",line2];
            country_lbl_busi.text=line3;
            [address_lb_busi addSubview:zipcode_lbl_busi];
            [address_lb_busi addSubview:country_lbl_busi];
            y=60;
        }
        else if ([line1 length]!=0 && [line2 length]==0 && [line3 length]!=0)
        {
            businessAddress= [NSString stringWithFormat:@"%@\n %@",line1,line3];
            street_number_lbl_busi.frame=CGRectMake(38, 10, 250, 20);
            country_lbl_busi.frame=CGRectMake(38, street_number_lbl_busi.frame.origin.y+street_number_lbl_busi.frame.size.height+10, 250, 20);
            street_number_lbl_busi.text=line1;
            country_lbl_busi.text=line3;
            [address_lb_busi addSubview:street_number_lbl_busi];
            [address_lb_busi addSubview:country_lbl_busi];
            y=60;
        }
        else if ([line1 length]==0 && [line2 length]!=0 && [line3 length]==0)
        {
            businessAddress=[NSString stringWithFormat:@"%@",line2];
            zipcode_lbl_busi.frame=CGRectMake(38, 10, 250, 20);
            zipcode_lbl_busi.text=line2;
            [address_lb_busi addSubview:zipcode_lbl_busi];
            y=60;
        }
        else if ([line1 length]!=0 && [line2 length]!=0 && [line3 length]==0)
        {
            businessAddress=[NSString stringWithFormat:@"%@ \n %@",line1,line2];
            street_number_lbl_busi.frame=CGRectMake(38, 10, 250, 20);
            zipcode_lbl_busi.frame=CGRectMake(38, street_number_lbl_busi.frame.origin.y+street_number_lbl_busi.frame.size.height+10, 250, 20);
            street_number_lbl_busi.text=line1;
            zipcode_lbl_busi.text=line2;
            [address_lb_busi addSubview:street_number_lbl_busi];
            [address_lb_busi addSubview:zipcode_lbl_busi];
            y=60;
        }
        else if ([line1 length]!=0 && [line2 length]==0 && [line3 length]==0)
        {
            businessAddress=[NSString stringWithFormat:@"%@",line1];
            street_number_lbl_busi.frame=CGRectMake(38, 10, 250, 20);
            street_number_lbl_busi.text=line1;
            [address_lb_busi addSubview:street_number_lbl_busi];
            y=30;
        }
        else
        {
            businessAddress=[NSString stringWithFormat:@"%@ \n %@\n %@",line1,line2,line3];
            street_number_lbl_busi.frame=CGRectMake(38, 10, 250, 20);
            zipcode_lbl_busi.frame=CGRectMake(38, street_number_lbl_busi.frame.origin.y+street_number_lbl_busi.frame.size.height, 250, 20);
            country_lbl_busi.frame=CGRectMake(38, zipcode_lbl_busi.frame.origin.y+zipcode_lbl_busi.frame.size.height, 250, 20);
            street_number_lbl_busi.text=line1;
            zipcode_lbl_busi.text=line2;
            country_lbl_busi.text=line3;
            [address_lb_busi addSubview:street_number_lbl_busi];
            [address_lb_busi addSubview:zipcode_lbl_busi];
            [address_lb_busi addSubview:country_lbl_busi];
            y=80;
        }
        
        
        address_img_busi.frame = CGRectMake(13, 10,[UIImage imageNamed:@"map_profile"].size.width/1.5,[UIImage imageNamed:@"map_profile"].size.height/1.5);
      
        
        CGRect newFrame = CGRectMake(0, busi_mail_divi.frame.origin.y+0.5,[UIImage imageNamed:@"detbuttonNormal"].size.width,y);//[UIImage imageNamed:@"detbuttonNormal"].size.height);
        
        //            newFrame.size.height = expectedLabelSize.height;
        address_lb_busi.frame = newFrame;
        DebugLog(@"address frame: %@",NSStringFromCGRect(address_lb_busi.frame));
        }else{
        address_img_busi.frame = CGRectMake(13, 4,12,0);
        
        address_lb_busi.frame = CGRectMake(0, busi_mail_divi.frame.origin.y+busi_mail_divi.frame.size.height, [UIImage imageNamed:@"detbuttonNormal"].size.width, 0);
        //            address_lb_busi.backgroundColor=[UIColor redColor];
    }
    
    if (businessAddress != false && ![businessAddress isEqualToString:@"(null)"] && ![businessAddress isEqualToString:@""])
    {
        
        busi_add_divi = [[UILabel alloc] init];
        [busi_add_divi setBackgroundColor:[UIColor colorWithRed:(239.0f / 255.0f) green:(239.0f / 255.0f) blue:(239.0f / 255.0f) alpha:1.0f]];
        //        [busi_add_divi setBackgroundColor:[UIColor redColor]];
        busi_add_divi.frame = CGRectMake(0,  address_lb_busi.frame.origin.y+address_lb_busi.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        [businessview addSubview:busi_add_divi];
        
        DebugLog(@"ADD1=====================> %@", businessAddress);
    }else {
        
        busi_add_divi.frame = CGRectMake(0,  address_lb_busi.frame.origin.y+street_number_lbl_busi.frame.size.height+city_lbl.frame.size.height+country_lbl_busi.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        
        DebugLog(@"ADD1=====================ELSE> %@", businessAddress);
    }
    counter=0;
    b_boxscroll = [[UIScrollView alloc] init];
    
    b_boxscroll.delegate=self;
    
    b_boxscroll.scrollEnabled=YES;
    
    b_boxscroll.showsHorizontalScrollIndicator = NO;
    
    b_boxscroll.backgroundColor =[UIColor clearColor];
    
    b_boxscroll.userInteractionEnabled=YES;
    
    //    b_boxscroll.tag = 1;
    
    //    b_boxscroll.pagingEnabled = YES;
    b_boxscroll.autoresizingMask=UIViewAutoresizingNone;
    
    
    
    twitterbt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [twitterbt setBackgroundImage:[UIImage imageNamed:@"twitter1.png"] forState:UIControlStateNormal];
    
    [twitterbt addTarget:self action:@selector(twitterfunc:) forControlEvents:UIControlEventTouchUpInside];
    
    [b_boxscroll addSubview:twitterbt];
    
    
    
    pinterestbt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [pinterestbt setBackgroundImage:[UIImage imageNamed:@"pinterest@2x1.png"] forState:UIControlStateNormal];
    
    [pinterestbt addTarget:self action:@selector(pinterestfunc:) forControlEvents:UIControlEventTouchUpInside];
    
    [b_boxscroll addSubview:pinterestbt];
    
    
    
    facebookbt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [facebookbt setBackgroundImage:[UIImage imageNamed:@"facebook@2x1.png"] forState:UIControlStateNormal];
    
    [facebookbt addTarget:self action:@selector(facebookfunc:) forControlEvents:UIControlEventTouchUpInside];
    
    [b_boxscroll addSubview:facebookbt];
    
    
    
    gplusbt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [gplusbt setBackgroundImage:[UIImage imageNamed:@"g+1.png"] forState:UIControlStateNormal];
    
    [gplusbt addTarget:self action:@selector(gplusfunc:) forControlEvents:UIControlEventTouchUpInside];
    
    [b_boxscroll addSubview:gplusbt];
    
    
    
    youtubebt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [youtubebt setBackgroundImage:[UIImage imageNamed:@"you-tube@2x1.png"] forState:UIControlStateNormal];
    
    [youtubebt addTarget:self action:@selector(youtubefunc:) forControlEvents:UIControlEventTouchUpInside];
    
    [b_boxscroll addSubview:youtubebt];
    
    
    
    instagrambt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [instagrambt setBackgroundImage:[UIImage imageNamed:@"instagram@2x1.png"] forState:UIControlStateNormal];
    
    [instagrambt addTarget:self action:@selector(instagramfunc:) forControlEvents:UIControlEventTouchUpInside];
    
    [b_boxscroll addSubview:instagrambt];
    
    
    linkedinbt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [linkedinbt setBackgroundImage:[UIImage imageNamed:@"linkedin1@2x1.png"] forState:UIControlStateNormal];
    
    [linkedinbt addTarget:self action:@selector(linkedinfunc:) forControlEvents:UIControlEventTouchUpInside];
    
    [b_boxscroll addSubview:linkedinbt];
    
    
    
    skypebt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [skypebt setBackgroundImage:[UIImage imageNamed:@"skype@2x1.png"] forState:UIControlStateNormal];
    
    [skypebt addTarget:self action:@selector(skypefunc:) forControlEvents:UIControlEventTouchUpInside];
    
    [b_boxscroll addSubview:skypebt];
    
    
    
    soundcloudbt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [soundcloudbt setBackgroundImage:[UIImage imageNamed:@"soundcloud@2x1.png"] forState:UIControlStateNormal];
    
    [soundcloudbt addTarget:self action:@selector(soundcloudfunc:) forControlEvents:UIControlEventTouchUpInside];
    
    [b_boxscroll addSubview:soundcloudbt];
    
    
    
    vimeobt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [vimeobt setBackgroundImage:[UIImage imageNamed:@"vimeo@2x1.png"] forState:UIControlStateNormal];
    
    [vimeobt addTarget:self action:@selector(vimeofunc:) forControlEvents:UIControlEventTouchUpInside];
    
    [b_boxscroll addSubview:vimeobt];
    
    DebugLog(@"DICT PROFILE++++++=============>%@",dict_profile);
    
    if ([dict_profile objectForKey:@"s_twitter"] != false  && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_twitter"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_twitter"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_twitter"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_twitter"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_twitter"] isEqualToString:@"(null)"])
    {
        counter += 1;
        twitterbt.frame = CGRectMake(5, 4,31.5f,31);
    }else{
        
        twitterbt.frame = CGRectMake(5, 4,0,0);
    }
    
    
    if ([dict_profile objectForKey:@"s_pinterest"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_pinterest"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_pinterest"] isEqualToString:@""]  && ![[dict_profile objectForKey:@"s_pinterest"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_pinterest"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_pinterest"] isEqualToString:@"(null)"])
    {
        counter += 1;
        pinterestbt.frame = CGRectMake(twitterbt.frame.origin.x + twitterbt.frame.size.width + 5.0f, 4,31.5f,31);
    }else{
        
        pinterestbt.frame = CGRectMake(twitterbt.frame.origin.x + twitterbt.frame.size.width, 4,0,0);
    }
    
    
    if ([dict_profile objectForKey:@"s_facebook"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_facebook"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_facebook"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_facebook"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_facebook"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_facebook"] isEqualToString:@"(null)"])
    {
        counter += 1;
        facebookbt.frame = CGRectMake(pinterestbt.frame.origin.x + pinterestbt.frame.size.width + 5.0f, 4,31.5f,31);
    }else{
        
        facebookbt.frame = CGRectMake(pinterestbt.frame.origin.x + pinterestbt.frame.size.width, 4,0,0);
    }
    
    if ([dict_profile objectForKey:@"s_google"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_google"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_google"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_google"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_google"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_google"] isEqualToString:@"(null)"])
    {
        counter += 1;
        gplusbt.frame = CGRectMake(facebookbt.frame.origin.x + facebookbt.frame.size.width + 5.0f, 4,31.5f,31);
    }else{
        
        gplusbt.frame = CGRectMake(facebookbt.frame.origin.x + facebookbt.frame.size.width, 4,0,0);
    }
    
    if ([dict_profile objectForKey:@"s_youtube"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_youtube"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_youtube"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_youtube"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_youtube"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_youtube"] isEqualToString:@"(null)"])
    {
        counter += 1;
        youtubebt.frame = CGRectMake(gplusbt.frame.origin.x + gplusbt.frame.size.width + 5.0f, 4,31.5f,31);
    }else{
        
        youtubebt.frame = CGRectMake(gplusbt.frame.origin.x + gplusbt.frame.size.width, 4,0,0);
    }
    
    
    if ([dict_profile objectForKey:@"s_instagram"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_instagram"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_instagram"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_instagram"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_instagram"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_instagram"] isEqualToString:@"(null)"])
    {
        counter += 1;
        instagrambt.frame = CGRectMake(youtubebt.frame.origin.x + youtubebt.frame.size.width + 5.0f, 4,31.5f,31);
    }else{
        
        instagrambt.frame = CGRectMake(youtubebt.frame.origin.x + youtubebt.frame.size.width , 4,0,0);
    }
    
    if ([dict_profile objectForKey:@"s_linkedin"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_linkedin"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_linkedin"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_linkedin"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_linkedin"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_linkedin"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_linkedin"] isEqualToString:@"<null>"])
    {
        counter += 1;
        linkedinbt.frame = CGRectMake(instagrambt.frame.origin.x + instagrambt.frame.size.width + 5.0f, 4,31.5f,31);
    }else{
        
        linkedinbt.frame = CGRectMake(instagrambt.frame.origin.x + instagrambt.frame.size.width, 4,0,0);
    }
    
    
    if ([dict_profile objectForKey:@"s_skype"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_skype"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_skype"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_skype"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_skype"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_skype"] isEqualToString:@"(null)"])
    {
        counter += 1;
        skypebt.frame = CGRectMake(linkedinbt.frame.origin.x + linkedinbt.frame.size.width + 5.0f, 4,31.5f,31);
    }else{
        
        skypebt.frame = CGRectMake(linkedinbt.frame.origin.x + linkedinbt.frame.size.width, 4,0,0);
    }
    
    if ([dict_profile objectForKey:@"s_soundcloud"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_soundcloud"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_soundcloud"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_soundcloud"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_soundcloud"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_soundcloud"] isEqualToString:@"(null)"])
    {
        counter += 1;
        soundcloudbt.frame = CGRectMake(skypebt.frame.origin.x + skypebt.frame.size.width + 5.0f, 4,31.5f,31);
    }else{
        
        soundcloudbt.frame = CGRectMake(skypebt.frame.origin.x + skypebt.frame.size.width, 4,0,0);
    }
    
    if ([dict_profile objectForKey:@"s_vimeo"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_vimeo"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_vimeo"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_vimeo"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_vimeo"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_vimeo"] isEqualToString:@"(null)"])
    {
        counter += 1;
        vimeobt.frame = CGRectMake(soundcloudbt.frame.origin.x + soundcloudbt.frame.size.width + 5.0f, 4,31.5f,31);
    }else{
        
        vimeobt.frame = CGRectMake(soundcloudbt.frame.origin.x + soundcloudbt.frame.size.width, 4,0,0);
    }
    
    
    b_boxscroll.contentSize = CGSizeMake(counter*36.5f, 40);
    b_boxscroll.backgroundColor = [UIColor clearColor];
    [b_boxscroll removeFromSuperview];
    [mainview bringSubviewToFront:b_boxscroll];
    //    removeconbt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    if (b_boxscroll.contentSize.width >= 300) {
        
        DebugLog(@"b_boxscroll CONTENT >=300");
        DebugLog(@"b_boxscroll framesize===>%f",busi_add_divi.frame.origin.y+busi_add_divi.frame.size.height+10);
        //
        b_boxscroll.frame =CGRectMake(10,busi_add_divi.frame.origin.y+busi_add_divi.frame.size.height, 300,40.0f);
        [businessview addSubview:b_boxscroll];
        
        divider_4 = [[UIView alloc]init];
        divider_4.frame = CGRectMake(0, b_boxscroll.frame.origin.y + b_boxscroll.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        divider_4.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
        //        divider_4.backgroundColor=[UIColor redColor];
        [businessview addSubview:divider_4];
        
               mainscroll_busi.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, divider_4.frame.origin.y + divider_4.frame.size.height+35.0f);
        //
    }else{
        
        DebugLog(@"b_boxscroll CONTENT <=300 ---- ELSE part");
        
        b_boxscroll.frame = CGRectMake((310 - b_boxscroll.contentSize.width)/2,busi_add_divi.frame.origin.y+busi_add_divi.frame.size.height, 300,40.0f);
        [businessview addSubview:b_boxscroll];
        DebugLog(@"b_boxscroll frame: %@",NSStringFromCGRect(b_boxscroll.frame));
        divider_4 = [[UIView alloc]init];
        divider_4.frame = CGRectMake(0,b_boxscroll.frame.origin.y + b_boxscroll.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        divider_4.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
        //        divider_4.backgroundColor=[UIColor redColor];
        DebugLog(@"divider5 frame: %@",NSStringFromCGRect(divider_4.frame));
        [businessview addSubview:divider_4];
               mainscroll_busi.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, divider_4.frame.origin.y + divider_4.frame.size.height+35.0f);
        //        DebugLog(@"mainscroll_busi contentsize: %f",mainscroll_busi.contentSize.height);
    }
    if (counter != 0)
    {
        DebugLog(@"COUNTER========================>%d",counter);
        divider_4.frame = CGRectMake(0,  b_boxscroll.frame.origin.y+b_boxscroll.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        divider_4.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
        [businessview addSubview:divider_4];
        DebugLog(@"divider4 frame: %@",NSStringFromCGRect(divider_4.frame));
    }
    else
    {
        divider_4.frame = CGRectMake(0,  b_boxscroll.frame.origin.y+b_boxscroll.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        [businessview addSubview:divider_4];
    }
    [activity removeFromSuperview];
    
    
    DebugLog(@"address font %@",address_lb_busi.titleLabel.font);
    DebugLog(@"street font %@",street_number_lbl_busi.font);
    DebugLog(@"zipcode font %@",zipcode_lbl_busi.font);
    DebugLog(@"country font %@",country_lbl_busi.font);
    DebugLog(@"mail font %@",mail_lb_busi.titleLabel.font);
    DebugLog(@"company font %@",companylb.font);
    DebugLog(@"designation font %@",desiglb.font);
    DebugLog(@"mobile font %@",mobileno_busi.titleLabel.font);
    
}

-(void)loadViews
{
    //    DebugLog(@"SWITCHPROF===============>%d",switchprof);
    [companyimg removeFromSuperview];
    [companylb removeFromSuperview];
    [desigimg removeFromSuperview];
    [desiglb removeFromSuperview];
    [activity removeFromSuperview];
    
    [mobileimg_busi removeFromSuperview];
    [mobileno_busi removeFromSuperview];
    [mailimg_busi removeFromSuperview];
    [mail_lb_busi removeFromSuperview];
    [address_img_busi removeFromSuperview];
    [address_lb_busi removeFromSuperview];
    [businessview removeFromSuperview];
    [b_boxscroll removeFromSuperview];
    zipcode_busi=nil;
    company=nil;
    desig=nil;
    street_busi=nil;
    country_busi=nil;
    businessview=nil;
    [activity stopAnimating];
    
    personalview=[[UIView alloc]initWithFrame:CGRectMake(0.0f, 110.0f, [UIScreen mainScreen].bounds.size.width, mainscroll.frame.size.height)];
    personalview.userInteractionEnabled=YES;
    [mainscroll addSubview:personalview];
    
    mobileno = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [mobileno setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 40.0f, 0, 0)];
    
    mobileno.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    [mobileno setBackgroundImage:[UIImage imageNamed:@"detbuttonNormal"] forState:UIControlStateNormal];
    
    [mobileno setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateHighlighted];
    
    [mobileno setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateSelected];
    [mobileno.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
    
    [mobileno setTitleColor:[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f]forState:UIControlStateNormal];
    mobileno.userInteractionEnabled = YES;
    
    [mobileno addTarget:self action:@selector(mobileFun:) forControlEvents:UIControlEventTouchUpInside];
    
    [personalview addSubview:mobileno];
    
    
    mobileimg = [[UIImageView alloc]init];//WithFrame:CGRectMake(10.0f, 10.0f, [UIImage imageNamed:@"mobile_profile"].size.width, [UIImage imageNamed:@"mobile_profile"].size.height+12)];//WithFrame:CGRectMake(15, 195-60, 14, 13.5f)];
    mobileimg.image=[UIImage imageNamed:@"mobile_profile"];
    [mobileno addSubview:mobileimg];
    
    
    divider_9 = [[UIView alloc]init];//WithFrame:CGRectMake(14.5f, mail_lb.frame.origin.y+mail_lb.frame.size.height+15, 291, 1)];
    //            divider_2.image=[UIImage imageNamed:@"divider-prof.png"];
    [personalview addSubview:divider_9];
    
    
    landphonelb = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [landphonelb setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 40.0f, 0, 0)];
    
    landphonelb.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    [landphonelb setBackgroundImage:[UIImage imageNamed:@"detbuttonNormal"] forState:UIControlStateNormal];
    
    [landphonelb setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateHighlighted];
    
    [landphonelb setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateSelected];
    
    [landphonelb.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
    
    [landphonelb setTitleColor:[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f]forState:UIControlStateNormal];
    
    [landphonelb addTarget:self action:@selector(landphoneFun:) forControlEvents:UIControlEventTouchUpInside];
    
    [personalview addSubview:landphonelb];
    
    landphone_img = [[UIImageView alloc]init];//WithFrame:CGRectMake(10, 10, [UIImage imageNamed:@"home_profile"].size.width/1.5, [UIImage imageNamed:@"home_profile"].size.height/1.5)];//WithFrame:CGRectMake(15, 220-60, 14, 13.5f)];
    landphone_img.image=[UIImage imageNamed:@"home_profile"];
    [landphonelb addSubview:landphone_img];
    
    
    
    divider_1 = [[UIView alloc]init];//WithFrame:CGRectMake(14.5f, 248-60, 291, 1)];
    
    //    divider_1.image=[UIImage imageNamed:@"divider-prof.png"];
    
    [personalview addSubview:divider_1];
    
   
    
    mail_lb = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [mail_lb setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 40, 0, 0)];
    
    mail_lb.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    [mail_lb setBackgroundImage:[UIImage imageNamed:@"detbuttonNormal"] forState:UIControlStateNormal];
    
    [mail_lb setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateHighlighted];
    
    [mail_lb setBackgroundImage:[UIImage imageNamed:@"detbuttonSelected"] forState:UIControlStateSelected];
    
    [mail_lb.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
    
    [mail_lb setTitleColor:[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f]forState:UIControlStateNormal];
    
    [mail_lb addTarget:self action:@selector(mailFun:) forControlEvents:UIControlEventTouchUpInside];
    
    [personalview addSubview:mail_lb];
    
    
    mailimg =[[UIImageView alloc]init];//WithFrame:CGRectMake(10, 10, [UIImage imageNamed:@"email_profile"].size.width, [UIImage imageNamed:@"email_profile"].size.height+12) ];//WithFrame:CGRectMake(15, divider_1.frame.origin.y+divider_1.frame.size.height+13, 16, 11)];
    mailimg.image=[UIImage imageNamed:@"email_profile"];
    [mail_lb addSubview:mailimg];
    
    
    //Checking for User Interaction
    
    if ([other isEqualToString:@"yes"]) {
        
        mobileno.userInteractionEnabled = YES;
        
        landphonelb.userInteractionEnabled = YES;
        
        mail_lb.userInteractionEnabled = YES;
        
        website_lb.userInteractionEnabled = YES;
    }else{
        
        mobileno.userInteractionEnabled = NO;
        
        landphonelb.userInteractionEnabled = NO;
        
        mail_lb.userInteractionEnabled = NO;
        
        //        website_lb.userInteractionEnabled = NO;
    }
    
    divider_2 = [[UIView alloc]init];//WithFrame:CGRectMake(14.5f, website_lb.frame.origin.y+website_lb.frame.size.height+11, 291, 1)];
    
    //    divider_2.image=[UIImage imageNamed:@"divider-prof.png"];
    
    [personalview addSubview:divider_2];
    
    
    
    address_lb = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [address_lb setTitleEdgeInsets:UIEdgeInsetsMake(14.0f, 40, 0, 0)];
    
    address_lb.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    
    [address_lb.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
    
    address_lb.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    //    address_lb.backgroundColor=[UIColor redColor];
    
    [address_lb setTitleColor:[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f]forState:UIControlStateNormal];
    
    [address_lb addTarget:self action:@selector(openmap:) forControlEvents:UIControlEventTouchUpInside];
    
    [personalview addSubview:address_lb];
    
    street_number_lbl = [[UILabel alloc] init];//WithFrame:CGRectMake(40, address_lb.frame.origin.y,200,27)];
    street_number_lbl.backgroundColor = [UIColor clearColor];
    street_number_lbl.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
    street_number_lbl.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    [personalview addSubview:street_number_lbl];
    
    zipcode_lbl = [[UILabel alloc] init];//WithFrame:CGRectMake(40, street_number_lbl.frame.origin.y+street_number_lbl.frame.size.height,75,27)];
    zipcode_lbl.backgroundColor = [UIColor clearColor];
    zipcode_lbl.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
    zipcode_lbl.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    [personalview addSubview:zipcode_lbl];
    
    
    country_lbl = [[UILabel alloc] init];//WithFrame:CGRectMake(40, zipcode_lbl.frame.origin.y+zipcode_lbl.frame.size.height,100,27)];
    country_lbl.backgroundColor = [UIColor clearColor];
    country_lbl.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
    country_lbl.font=[UIFont fontWithName:@"ProximaNova-Regular" size:17];
    [personalview addSubview:country_lbl];
    
    address_img=[[UIImageView alloc]init];//WithFrame:CGRectMake(15,divider_2.frame.origin.y+divider_2.frame.size.height+11,12,17)];
    address_img.image=[UIImage imageNamed:@"map_profile"];
    [address_lb addSubview:address_img];
    
    
    divider_3 = [[UIView alloc]init];//WithFrame:CGRectMake(14.5f, address_lb.frame.origin.y+address_lb.frame.size.height+5, 291, 1)];
    
    //    divider_3.image=[UIImage imageNamed:@"divider-prof.png"];
    
    [personalview addSubview:divider_3];
    
    
    
    [self performSelectorInBackground:@selector(yesSuccess)
                           withObject:nil];
    [del showTabValues:YES];
    
    divider_5 = [[UIView alloc]init];//WithFrame:CGRectMake(14.5f, 248-60, 291, 1)];
    
    //    if ([uid intValue] != [[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"] intValue])
    [personalview addSubview:divider_5];
    
    website_lb.userInteractionEnabled = YES;
    UITapGestureRecognizer *websiteTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(websiteFun:)];
    
    [website_lb addGestureRecognizer:websiteTap];
    
    divider_1.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
    divider_2.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
    divider_3.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
    divider_4.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
    divider_5.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1];
    [divider_9 setBackgroundColor:[UIColor colorWithRed:(239.0f / 255.0f) green:(239.0f / 255.0f) blue:(239.0f / 255.0f) alpha:1.0f]];
    
    
    
    //NSString *street,*city,*zipcode,*country, *number;
    NSString *mobile_num1, *landline_num1, *business_num1;
    phonearr=[[NSMutableArray alloc]init];
    
    [boxscroll removeFromSuperview];
    [divider_4 removeFromSuperview];
    [divider_5 removeFromSuperview];
    
    
    [mobileimg setHidden:NO];
    
    [mobileno setHidden:NO];
    
    if (switchprof ==0)
    {
        companyimg.hidden=YES;
        companylb.hidden=YES;
        desigimg.hidden=YES;
        desiglb.hidden=YES;
        
        [busi_desig_divi removeFromSuperview];
        [busi_mob_divi removeFromSuperview];
        
        [activity removeFromSuperview];
        if ([dict_profile objectForKey:@"mobile_num"] != false && ![[dict_profile objectForKey:@"mobile_num"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"mobile_num"] length] > 1 && ![[dict_profile objectForKey:@"mobile_num"] isEqualToString:@"(null)"])
        {
            
            mobile= [NSString stringWithFormat:@"%@%@",[dict_profile objectForKey:@"mobile_pre"],[dict_profile objectForKey:@"mobile_num"]];
            mobileArray = [[NSMutableArray alloc]init];
            [mobileArray addObject:mobile];
            [mobileno setHidden:NO];
            [mobileimg setHidden:NO];
            mobileimg.frame = CGRectMake(13, 20, [UIImage imageNamed:@"mobile_profile"].size.width/1.5,[UIImage imageNamed:@"mobile_profile"].size.height/1.5);
            //            mobileimg.frame = CGRectMake(13, 18, 14, 18);
            mobileno.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIImage imageNamed:@"detbuttonNormal"].size.height + 10);
            [mobileno setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
            mobileno.userInteractionEnabled = YES;
            [mobileno setTitle:[NSString stringWithFormat:@"%@", mobile] forState:UIControlStateNormal];
            [mobileno addTarget:self action:@selector(mobileFun:) forControlEvents:UIControlEventTouchUpInside];
            
            //            mobileback.frame = CGRectMake(0, businesstext.frame.origin.y+businesstext.frame.size.height+7, [UIScreen mainScreen].bounds.size.width, 43);
            divider_9.frame = CGRectMake(0,  mobileno.frame.origin.y+mobileno.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.4f);
            
            //            DebugLog(@"IF PART A ENTERING");
        }else{
            
            mobile=@"";
            
            mobileimg.frame = CGRectMake(13, 15, 14, 0);
            
            mobileno.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0);
            divider_9.frame = CGRectMake(0,  mobileno.frame.origin.y+mobileno.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        }
        
        
        if ([dict_profile objectForKey:@"phone_num"] != false && ![[dict_profile objectForKey:@"phone_num"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"phone_num"] length] > 1 && ![[dict_profile objectForKey:@"phone_num"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"phone_num"] isEqualToString:@""])
        {
            landline= [NSString stringWithFormat:@"%@%@",[dict_profile objectForKey:@"phone_pre"],[dict_profile objectForKey:@"phone_num"]];
            
            [landphonelb setHidden:NO];
            
            [landphone_img setHidden:NO];
            
            landphone_img.frame = CGRectMake(13, 20, [UIImage imageNamed:@"home_profile"].size.width/1.5, [UIImage imageNamed:@"home_profile"].size.height/1.5);//CGRectMake(13, 18, 14, 18);
            
            landphonelb.frame = CGRectMake(0, divider_9.frame.origin.y+divider_9.frame.size.height, [UIImage imageNamed:@"detbuttonNormal"].size.width, [UIImage imageNamed:@"detbuttonNormal"].size.height+10);
            [landphonelb setTitle:[NSString stringWithFormat:@"%@", landline] forState:UIControlStateNormal];
            landphonelb.userInteractionEnabled = YES;
            [landphonelb setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
            [landphonelb addTarget:self action:@selector(landphoneFun:) forControlEvents:UIControlEventTouchUpInside];
            
            
        }else{
            
            landline=@"";
            
            landphone_img.frame = CGRectMake(13, 15, 14, 0);
            
            landphonelb.frame = CGRectMake(0, divider_9.frame.origin.y+divider_9.frame.size.height, [UIImage imageNamed:@"detbuttonNormal"].size.width, 0);
            
        }
        
        
        
        if (![mobile isEqualToString:@""] && ![landline isEqualToString:@""]) {
            
            divider_1.frame = CGRectMake(0,  landphonelb.frame.origin.y+landphonelb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
            
        }
        
        
        else if (![landline isEqualToString:@""]) {
            
            divider_1.frame = CGRectMake(0,  landphonelb.frame.origin.y+landphonelb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        }
        
        else {
            
            divider_1.frame = CGRectMake(0,  landphonelb.frame.origin.y+landphonelb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        }
        
        if (mobile) {
            numberstring=mobile;
            [phonearr addObject:mobile];
        }
        else if (landline)
        {
            numberstring=landline;
            [phonearr addObject:landline];
        }
        
        
        if ([dict_profile objectForKey:@"email"] != false && ![[dict_profile objectForKey:@"email"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"email"] length] > 1 && ![[dict_profile objectForKey:@"email"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"email"] isEqualToString:@""])
        {
            mail= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"email"]];
            
            [mail_lb setHidden:NO];
            
            [mailimg setHidden:NO];
            mailimg.frame= CGRectMake(13, 20, [UIImage imageNamed:@"email_profile"].size.width/1.5, [UIImage imageNamed:@"email_profile"].size.height/1.5);
            //mailimg.frame = CGRectMake(13, 18, 14.5f, 14);
            
            mail_lb.frame = CGRectMake(0, divider_1.frame.origin.y+divider_1.frame.size.height, [UIImage imageNamed:@"detbuttonNormal"].size.width,[UIImage imageNamed:@"detbuttonNormal"].size.height+10);
            
            mail_lb.userInteractionEnabled = YES;
            [mail_lb setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
            [mail_lb addTarget:self action:@selector(mailFun:) forControlEvents:UIControlEventTouchUpInside];
            
            
            [mail_lb setTitle:[NSString stringWithFormat:@"%@", mail] forState:UIControlStateNormal];
            mailback.frame = CGRectMake(0, divider_1.frame.origin.y+divider_1.frame.size.height, [UIScreen mainScreen].bounds.size.width, 51);
            
        }
        else{
            
            mail=@"";
            
            mailimg.frame = CGRectMake(13, 16, 16, 0);
            
            mail_lb.frame = CGRectMake(0, divider_1.frame.origin.y+divider_1.frame.size.height, [UIImage imageNamed:@"detbuttonNormal"].size.width, 0);
        }
        
        
        if (![mail isEqualToString:@""]){  //|| ![website isEqualToString:@""]) {
            
            divider_2.frame = CGRectMake(0,  mail_lb.frame.origin.y+mail_lb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
            DebugLog(@"mail divider: %@",NSStringFromCGRect(divider_2.frame));
        }
        
        //        else if (![website isEqualToString:@""]) {
        //
        //            divider_2.frame = CGRectMake(14.5f,  website_lb.frame.origin.y+website_lb.frame.size.height+16, 291, 0.5f);
        //        }
        
        else {
            
            divider_2.frame = CGRectMake(0,  mail_lb.frame.origin.y+mail_lb.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        }
        
        //======================================PERSONAL ADDRESS =============================//
        
        if ([dict_profile objectForKey:@"street"] != false && ![[dict_profile objectForKey:@"street"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"street"] length]!=0)
        {
            street= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"street"]];
            x=x+27;
        }
        else
            
            street=@"";
        
        if ([dict_profile objectForKey:@"housenumber"] != false && ![[dict_profile objectForKey:@"housenumber"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"housenumber"] length]!=0 && ![[dict_profile objectForKey:@"housenumber"] isEqualToString:@""])
        {
            number= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"housenumber"]];
            if (!street)
                x=x+27;
        }
        else
            number= @"";
        
        if ([dict_profile objectForKey:@"zipcode"] != false && ![[dict_profile objectForKey:@"zipcode"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"zipcode"] length]!=0 && ![[dict_profile objectForKey:@"zipcode"] isEqualToString:@""])
        {
            zipcode= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"zipcode"]];
            x=x+27;
        }
        
        else
            zipcode=@"";
        
        
        if ([dict_profile objectForKey:@"city"] != false && ![[dict_profile objectForKey:@"city"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"city"] length]!=0 && ![[dict_profile objectForKey:@"city"] isEqualToString:@""])
        {
            city= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"city"]];
            if (!zipcode)
                x=x+27;
        }
        else
            
            city=@"";
        
        
        if ([dict_profile objectForKey:@"country"] != false && ![[dict_profile objectForKey:@"country"] isEqualToString:@"(null)"]&& [[dict_profile objectForKey:@"country"] length]!=0 && ![[dict_profile objectForKey:@"country"] isEqualToString:@""])
        {
            country= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"country"]];
            x=x+27;
        }
        else
            country=@"";
        
        
        
        if ( ![street isEqualToString: @""] || ![number isEqualToString: @""] || ![city isEqualToString: @""] || ![zipcode isEqualToString: @""] || ![country isEqualToString: @""])
        {
            [self setAddress];
            
            
            address_img.frame = CGRectMake(13, 10,[UIImage imageNamed:@"map_profile"].size.width/1.5,[UIImage imageNamed:@"map_profile"].size.height/1.5);
            
            address_lb.frame = CGRectMake(0, divider_2.frame.origin.y+divider_2.frame.size.height, [[UIScreen mainScreen] bounds].size.width, y1+8);
            divider_3.frame = CGRectMake(0, address_lb.frame.origin.y+address_lb.frame.size.height, [[UIScreen mainScreen] bounds].size.width, 0.5);
            
            
        }else{
            
            address_img.frame = CGRectMake(13, 10,12,0);
            
            address_lb.frame = CGRectMake(0, divider_2.frame.origin.y+divider_2.frame.size.height+1, [UIImage imageNamed:@"detbuttonNormal"].size.width, 0);
        }
        
             counter = 0;
        
        //////////////////////////////   Whether Business Button will be shown   /////////////////////////////////////////////////
        if ([dict_profile objectForKey:@"b_company"] != false && ![[dict_profile objectForKey:@"b_company"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_company"] length] > 1 && ![[dict_profile objectForKey:@"b_company"] isEqualToString:@"(null)"])
        {
            DebugLog(@"company business");
            nobusiness=1;
        }
        if ([dict_profile objectForKey:@"b_function"] != false && ![[dict_profile objectForKey:@"b_function"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_function"] length] > 1 && ![[dict_profile objectForKey:@"b_function"] isEqualToString:@"(null)"])
        {
            DebugLog(@"function business");
            
            nobusiness = 1;
        }
        if ([dict_profile objectForKey:@"b_phone_num"] != false && ![[dict_profile objectForKey:@"b_phone_num"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_phone_num"] length] > 1 && ![[dict_profile objectForKey:@"b_phone_num"] isEqualToString:@"(null)"])
        {
            DebugLog(@"phone business");
            
            nobusiness=1;
        }
        if ([dict_profile objectForKey:@"b_email"] != false && ![[dict_profile objectForKey:@"b_email"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_email"] length] > 1 && ![[dict_profile objectForKey:@"b_email"] isEqualToString:@"(null)"])
        {
            DebugLog(@"email business");
            
            nobusiness =1;
        }
        if ([dict_profile objectForKey:@"b_website"] != false && ![[dict_profile objectForKey:@"b_website"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_website"] length] > 1 && ![[dict_profile objectForKey:@"b_website"] isEqualToString:@"(null)"])
        {
            DebugLog(@"website business");
            
            nobusiness=1;
        }
        
        if ([dict_profile objectForKey:@"b_city"] != false && ![[dict_profile objectForKey:@"b_city"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_city"] length] >= 1 && ![[dict_profile objectForKey:@"b_city"] isEqualToString:@"0"])
        {
            DebugLog(@"city business");
            
            nobusiness=1;
        }
        if ([dict_profile objectForKey:@"b_street"] != false && ![[dict_profile objectForKey:@"b_street"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_street"] length] >= 1 && ![[dict_profile objectForKey:@"b_street"] isEqualToString:@"0"])
        {
            DebugLog(@"street business");
            
            nobusiness=1;
        }
        if ([dict_profile objectForKey:@"b_zipcode"] != false && ![[dict_profile objectForKey:@"b_zipcode"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_zipcode"] length] >= 1 && ![[dict_profile objectForKey:@"b_zipcode"] isEqualToString:@"0"])
        {
            DebugLog(@"zipcode business");
            
            nobusiness=1;
        }
        if ([dict_profile objectForKey:@"b_country"] != false && ![[dict_profile objectForKey:@"b_country"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_country"] length] >= 1 && ![[dict_profile objectForKey:@"b_country"] isEqualToString:@"0"])
        {
            DebugLog(@"country business");
            
            nobusiness = 1;
        }
        if ([dict_profile objectForKey:@"b_housenumber"] != false && ![[dict_profile objectForKey:@"b_housenumber"] isEqualToString:@"(null)"] && [[dict_profile objectForKey:@"b_housenumber"] length] >= 1 && ![[dict_profile objectForKey:@"b_housenumber"] isEqualToString:@"0"])
        {
            DebugLog(@"housenumber business");
            
            nobusiness = 1;
        }
    }
    
    else            //BUSINESS STARTS HERE======================================>
    {
        
    }
    
    
    
    website_lb.text= website;
    if (switchprof ==0){
        
        [self setAddress];
        
        
    }
    
    else{
        [street_number_lbl removeFromSuperview];
        [zipcode_lbl removeFromSuperview];
        [city_lbl removeFromSuperview];
        [country_lbl removeFromSuperview];
    }
    
     ////////////////////////////////////BOXSCROLL FOR SOCIAL GOES HERE...
    
    boxscroll = [[UIScrollView alloc] init];
    
    boxscroll.delegate=self;
    
    boxscroll.scrollEnabled=YES;
    
    boxscroll.showsHorizontalScrollIndicator = NO;
    
    boxscroll.backgroundColor =[UIColor clearColor];
    
    boxscroll.userInteractionEnabled=YES;
    
    //    boxscroll.tag = 1;
    
    //    boxscroll.pagingEnabled = YES;
    boxscroll.autoresizingMask=UIViewAutoresizingNone;
    
    
    
    twitterbt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [twitterbt setBackgroundImage:[UIImage imageNamed:@"twitter1.png"] forState:UIControlStateNormal];
    
    [twitterbt addTarget:self action:@selector(twitterfunc:) forControlEvents:UIControlEventTouchUpInside];
    
    [boxscroll addSubview:twitterbt];
    
    
    
    pinterestbt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [pinterestbt setBackgroundImage:[UIImage imageNamed:@"pinterest@2x1.png"] forState:UIControlStateNormal];
    
    [pinterestbt addTarget:self action:@selector(pinterestfunc:) forControlEvents:UIControlEventTouchUpInside];
    
    [boxscroll addSubview:pinterestbt];
    
    
    
    facebookbt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [facebookbt setBackgroundImage:[UIImage imageNamed:@"facebook@2x1.png"] forState:UIControlStateNormal];
    
    [facebookbt addTarget:self action:@selector(facebookfunc:) forControlEvents:UIControlEventTouchUpInside];
    
    [boxscroll addSubview:facebookbt];
    
    
    
    gplusbt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [gplusbt setBackgroundImage:[UIImage imageNamed:@"g+1.png"] forState:UIControlStateNormal];
    
    [gplusbt addTarget:self action:@selector(gplusfunc:) forControlEvents:UIControlEventTouchUpInside];
    
    [boxscroll addSubview:gplusbt];
    
    
    
    youtubebt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [youtubebt setBackgroundImage:[UIImage imageNamed:@"you-tube@2x1.png"] forState:UIControlStateNormal];
    
    [youtubebt addTarget:self action:@selector(youtubefunc:) forControlEvents:UIControlEventTouchUpInside];
    
    [boxscroll addSubview:youtubebt];
    
    
    
    instagrambt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [instagrambt setBackgroundImage:[UIImage imageNamed:@"instagram@2x1.png"] forState:UIControlStateNormal];
    
    [instagrambt addTarget:self action:@selector(instagramfunc:) forControlEvents:UIControlEventTouchUpInside];
    
    [boxscroll addSubview:instagrambt];
    
    
    linkedinbt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [linkedinbt setBackgroundImage:[UIImage imageNamed:@"linkedin1@2x1.png"] forState:UIControlStateNormal];
    
    [linkedinbt addTarget:self action:@selector(linkedinfunc:) forControlEvents:UIControlEventTouchUpInside];
    
    [boxscroll addSubview:linkedinbt];
    
    
    
    skypebt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [skypebt setBackgroundImage:[UIImage imageNamed:@"skype@2x1.png"] forState:UIControlStateNormal];
    
    [skypebt addTarget:self action:@selector(skypefunc:) forControlEvents:UIControlEventTouchUpInside];
    
    [boxscroll addSubview:skypebt];
    
    
    
    soundcloudbt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [soundcloudbt setBackgroundImage:[UIImage imageNamed:@"soundcloud@2x1.png"] forState:UIControlStateNormal];
    
    [soundcloudbt addTarget:self action:@selector(soundcloudfunc:) forControlEvents:UIControlEventTouchUpInside];
    
    [boxscroll addSubview:soundcloudbt];
    
    
    
    vimeobt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [vimeobt setBackgroundImage:[UIImage imageNamed:@"vimeo@2x1.png"] forState:UIControlStateNormal];
    
    [vimeobt addTarget:self action:@selector(vimeofunc:) forControlEvents:UIControlEventTouchUpInside];
    
    [boxscroll addSubview:vimeobt];
    
    DebugLog(@"DICT PROFILE++++++=============>%@",dict_profile);
    
    if ([dict_profile objectForKey:@"s_twitter"] != false  && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_twitter"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_twitter"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_twitter"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_twitter"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_twitter"] isEqualToString:@"(null)"])
    {
        counter += 1;
        twitterbt.frame = CGRectMake(5, 4,31.5f,31);
    }else{
        
        twitterbt.frame = CGRectMake(5, 4,0,0);
    }
    
    
    if ([dict_profile objectForKey:@"s_pinterest"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_pinterest"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_pinterest"] isEqualToString:@""]  && ![[dict_profile objectForKey:@"s_pinterest"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_pinterest"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_pinterest"] isEqualToString:@"(null)"])
    {
        counter += 1;
        pinterestbt.frame = CGRectMake(twitterbt.frame.origin.x + twitterbt.frame.size.width + 5.0f, 4,31.5f,31);
    }else{
        
        pinterestbt.frame = CGRectMake(twitterbt.frame.origin.x + twitterbt.frame.size.width, 4,0,0);
    }
    
    
    if ([dict_profile objectForKey:@"s_facebook"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_facebook"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_facebook"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_facebook"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_facebook"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_facebook"] isEqualToString:@"(null)"])
    {
        counter += 1;
        facebookbt.frame = CGRectMake(pinterestbt.frame.origin.x + pinterestbt.frame.size.width + 5.0f, 4,31.5f,31);
    }else{
        
        facebookbt.frame = CGRectMake(pinterestbt.frame.origin.x + pinterestbt.frame.size.width, 4,0,0);
    }
    
    if ([dict_profile objectForKey:@"s_google"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_google"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_google"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_google"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_google"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_google"] isEqualToString:@"(null)"])
    {
        counter += 1;
        gplusbt.frame = CGRectMake(facebookbt.frame.origin.x + facebookbt.frame.size.width + 5.0f, 4,31.5f,31);
    }else{
        
        gplusbt.frame = CGRectMake(facebookbt.frame.origin.x + facebookbt.frame.size.width, 4,0,0);
    }
    
    if ([dict_profile objectForKey:@"s_youtube"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_youtube"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_youtube"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_youtube"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_youtube"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_youtube"] isEqualToString:@"(null)"])
    {
        counter += 1;
        youtubebt.frame = CGRectMake(gplusbt.frame.origin.x + gplusbt.frame.size.width + 5.0f, 4,31.5f,31);
    }else{
        
        youtubebt.frame = CGRectMake(gplusbt.frame.origin.x + gplusbt.frame.size.width, 4,0,0);
    }
    
    
    if ([dict_profile objectForKey:@"s_instagram"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_instagram"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_instagram"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_instagram"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_instagram"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_instagram"] isEqualToString:@"(null)"])
    {
        counter += 1;
        instagrambt.frame = CGRectMake(youtubebt.frame.origin.x + youtubebt.frame.size.width + 5.0f, 4,31.5f,31);
    }else{
        
        instagrambt.frame = CGRectMake(youtubebt.frame.origin.x + youtubebt.frame.size.width , 4,0,0);
    }
    
    if ([dict_profile objectForKey:@"s_linkedin"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_linkedin"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_linkedin"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_linkedin"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_linkedin"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_linkedin"] isEqualToString:@"(null)"] && ![[dict_profile objectForKey:@"s_linkedin"] isEqualToString:@"<null>"])
    {
        counter += 1;
        linkedinbt.frame = CGRectMake(instagrambt.frame.origin.x + instagrambt.frame.size.width + 5.0f, 4,31.5f,31);
    }else{
        
        linkedinbt.frame = CGRectMake(instagrambt.frame.origin.x + instagrambt.frame.size.width, 4,0,0);
    }
    
    
    if ([dict_profile objectForKey:@"s_skype"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_skype"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_skype"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_skype"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_skype"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_skype"] isEqualToString:@"(null)"])
    {
        counter += 1;
        skypebt.frame = CGRectMake(linkedinbt.frame.origin.x + linkedinbt.frame.size.width + 5.0f, 4,31.5f,31);
    }else{
        
        skypebt.frame = CGRectMake(linkedinbt.frame.origin.x + linkedinbt.frame.size.width, 4,0,0);
    }
    
    if ([dict_profile objectForKey:@"s_soundcloud"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_soundcloud"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_soundcloud"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_soundcloud"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_soundcloud"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_soundcloud"] isEqualToString:@"(null)"])
    {
        counter += 1;
        soundcloudbt.frame = CGRectMake(skypebt.frame.origin.x + skypebt.frame.size.width + 5.0f, 4,31.5f,31);
    }else{
        
        soundcloudbt.frame = CGRectMake(skypebt.frame.origin.x + skypebt.frame.size.width, 4,0,0);
    }
    
    if ([dict_profile objectForKey:@"s_vimeo"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_vimeo"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_vimeo"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_vimeo"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_vimeo"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_vimeo"] isEqualToString:@"(null)"])
    {
        counter += 1;
        vimeobt.frame = CGRectMake(soundcloudbt.frame.origin.x + soundcloudbt.frame.size.width + 5.0f, 4,31.5f,31);
    }else{
        
        vimeobt.frame = CGRectMake(soundcloudbt.frame.origin.x + soundcloudbt.frame.size.width, 4,0,0);
    }
    
    
    boxscroll.contentSize = CGSizeMake(counter*36.5f, 40);
    boxscroll.backgroundColor = [UIColor clearColor];
    [boxscroll removeFromSuperview];
    [mainview bringSubviewToFront:boxscroll];
    //    removeconbt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    if (boxscroll.contentSize.width >= 300) {
        
        DebugLog(@"BOXSCROLL CONTENT >=300");
        DebugLog(@"boxscroll framesize===>%f",divider_3.frame.origin.y+divider_3.frame.size.height+10);
        //
        boxscroll.frame =CGRectMake(10,divider_3.frame.origin.y+divider_3.frame.size.height, 300,40.0f);
        [personalview addSubview:boxscroll];
        DebugLog(@"boxscroll frame1: %@",NSStringFromCGRect(boxscroll.frame));
        divider_5.frame = CGRectMake(0, boxscroll.frame.origin.y + boxscroll.frame.size.height+6, [UIScreen mainScreen].bounds.size.width, 0.5f);
        [personalview addSubview:divider_5];
        
        mainscroll.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, divider_5.frame.origin.y + divider_5.frame.size.height+35.0f);
        
    }else{
        
        DebugLog(@"BOXSCROLL CONTENT <=300 ---- ELSE part");
        
        boxscroll.frame = CGRectMake((310 - boxscroll.contentSize.width)/2,divider_3.frame.origin.y+divider_3.frame.size.height, 300,40.0f);
        [personalview addSubview:boxscroll];
        DebugLog(@"boxscroll frame1: %@",NSStringFromCGRect(boxscroll.frame));
        
        divider_5.frame = CGRectMake(0,boxscroll.frame.origin.y + boxscroll.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        [personalview addSubview:divider_5];
               mainscroll.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, divider_5.frame.origin.y + divider_5.frame.size.height+35.0f);
        //        DebugLog(@"mainscroll contentsize: %f",mainscroll.contentSize.height);
    }
    //
    if (counter != 0)
    {
        DebugLog(@"COUNTER========================>%d",counter);
        divider_5.frame = CGRectMake(0,  boxscroll.frame.origin.y+boxscroll.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.5f);
        //        divider_5.backgroundColor=[UIColor redColor];
        [personalview addSubview:divider_5];
    }
    else
    {
        divider_5.frame = CGRectMake(0,  boxscroll.frame.origin.y+boxscroll.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
        [personalview addSubview:divider_5];
    }
    
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    if (switchprof == 0)
    {
        phonearr= [[NSMutableArray alloc] init];
        if ([dict_profile objectForKey:@"mobile_num"] != false && ![[dict_profile objectForKey:@"mobile_num"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"mobile_num"] length] > 1 && ![[dict_profile objectForKey:@"mobile_num"] isEqualToString:@"(null)"])
        {
            mobile_num1= [NSString stringWithFormat:@"%@%@",[dict_profile objectForKey:@"mobile_pre"],[dict_profile objectForKey:@"mobile_num"]];
            [phonearr addObject:mobile_num1];
        }
        
        else if ([dict_profile objectForKey:@"phone_num"] != false && ![[dict_profile objectForKey:@"phone_num"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"phone_num"] length] > 1 && ![[dict_profile objectForKey:@"phone_num"] isEqualToString:@"(null)"])
        {
            landline_num1= [NSString stringWithFormat:@"%@%@",[dict_profile objectForKey:@"phone_pre"],[dict_profile objectForKey:@"phone_num"]];
            [phonearr addObject:landline_num1];
        }
    }
    else
    {
        if ([dict_profile objectForKey:@"b_phone_num"] != false && ![[dict_profile objectForKey:@"b_phone_num"] isEqualToString:@"0"] && [[dict_profile objectForKey:@"b_phone_num"] length] > 1 && ![[dict_profile objectForKey:@"b_phone_num"] isEqualToString:@"(null)"])
        {
            business_num1= [NSString stringWithFormat:@"%@%@",[dict_profile objectForKey:@"b_phone_pre"],[dict_profile objectForKey:@"b_phone_num"]];
            [phonearr addObject:business_num1];
        }
    }
    
    
    DebugLog(@"nobusiness: %d",nobusiness);
    if (nobusiness == 0)
    {
        //        [togglebt removeFromSuperview];
        [divider_5 removeFromSuperview];
    }
}


-(void)twitterfunc: (id)sender
{
    NSURL *urlApp = [NSURL URLWithString: [NSString stringWithFormat:@"twitter://user?screen_name=%@", [dict_profile objectForKey:@"s_twitter"]]];
    
    if ([[UIApplication sharedApplication] canOpenURL:urlApp]) {
        
        [[UIApplication sharedApplication] openURL:urlApp];
    } else {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://twitter.com/%@",[dict_profile objectForKey:@"s_twitter"]]]];
        DebugLog(@"TWIT URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"http://twitter.com/%@",[dict_profile objectForKey:@"s_twitter"]]]);
    }
}


-(void)pinterestfunc: (id)sender
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"pinterest://user/%@",[dict_profile objectForKey:@"s_pinterest"]]]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"pinterest://user/%@",[dict_profile objectForKey:@"s_pinterest"]]]];
    } else {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://pinterest.com"]];
        DebugLog(@"PIN URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"http://pinterest.com/%@",[dict_profile objectForKey:@"s_pinterest"]]]);
    }
}



-(void)facebookfunc: (id)sender
{
    
    NSString *fb_app_str = [[NSString alloc]init];
    
    fb_app_str = [dict_profile objectForKey:@"s_facebook"];
    
//    NSString *string1=[NSString
//                       stringWithFormat:@"http://graph.facebook.com/%@",[dict_profile
//                                                                         objectForKey:@"s_facebook"]];
//    
//    DebugLog(@"facebook graph %@",string1);
//    
//    NSError *localErr;
//    
//    
//    
//    NSData *result =  [NSData dataWithContentsOfURL:[NSURL
//                                                     URLWithString:string1] options:NSDataReadingUncached error:&localErr];
//    
//    
//    
//    if (result != nil)
//        
//    {
//        
//        NSDictionary *graphjson =[NSJSONSerialization
//                                  JSONObjectWithData:result options:0 error:&localErr];
//        
//        if (![graphjson isKindOfClass:[NSNull class]])
//            
//        {
//            
//            if ([[graphjson allKeys]containsObject:@"id"])
//                
//            {
//                
//                fb_app_str = [graphjson objectForKey:@"id"];
//                
//            }
//            
//        }
//        
//    }
//    
//    
    
    NSURL *url = [NSURL URLWithString:[NSString
                                       stringWithFormat:@"fb://profile/%@",fb_app_str]];  //[dict_profile
    //objectForKey:@"s_facebook"]
    
    [[UIApplication sharedApplication] openURL:url];
    
    
    
    if ([[UIApplication sharedApplication] canOpenURL:url]){
        
        [[UIApplication sharedApplication] openURL:url];
        
    }
    
    else {
        
        [[UIApplication sharedApplication] openURL:[NSURL
                                                    URLWithString:[NSString
                                                                   stringWithFormat:@"http://facebook.com/%@",[dict_profile
                                                                                                               objectForKey:@"s_facebook"]]]];  //100001925673906
        
        DebugLog(@"http://facebook.com/%@",[dict_profile
                                         objectForKey:@"s_facebook"]);
        
    }
    
}



-(void)gplusfunc: (id)sender
{
    NSString *string = [dict_profile objectForKey:@"s_google"];
    NSArray *stringArray = [string componentsSeparatedByString: @"com/"];
    NSString *gplusstr = [stringArray objectAtIndex:1];
    NSURL *GooglePlus = [NSURL URLWithString:[NSString stringWithFormat:@"gplus://plus.google.com/%@",gplusstr]];
    DebugLog(@"GOO+ URL=============>%@",GooglePlus);
    
    if ([[UIApplication sharedApplication] canOpenURL:GooglePlus])  //[NSURL URLWithString:@"gplus://plus.google.com/u/0/105819873801211194735/"]
        [[UIApplication sharedApplication] openURL:GooglePlus];
    else
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://plus.google.com/%@",gplusstr]]];
}


-(void)youtubefunc: (id)sender
{
    NSString* social   = [NSString stringWithFormat:@"http://www.youtube.com/user/%@",[dict_profile objectForKey:@"s_youtube"]];
    NSString* socialApp = [NSString stringWithFormat:@"youtube://user/%@",[dict_profile objectForKey:@"s_youtube"]];
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:socialApp]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:socialApp]];
        
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:social]];
    }
}



-(void)instagramfunc: (id)sender
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"instagram://user?username=%@",[dict_profile objectForKey:@"s_instagram"]]]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"instagram://user?username=%@",[dict_profile objectForKey:@"s_instagram"]]]];
        
        DebugLog(@"INSTA URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"instagram://user?username=%@",[dict_profile objectForKey:@"s_instagram"]]]);
    } else {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.instagram.com"]];
        DebugLog(@"INSTA URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"https://www.instagram.com/%@",[dict_profile objectForKey:@"s_instagram"]]]);
    }
}


-(void)linkedinfunc: (id)sender
{
    NSString *linkedinstra = [dict_profile objectForKey:@"s_linkedin"];
    NSArray *linkedinarr= [linkedinstra componentsSeparatedByString:@"="];
    NSString *linkedinstr = [linkedinarr objectAtIndex:1];
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"linkedin://profile/%@",linkedinstr]]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"linkedin://profile/%@",linkedinstr]]];
        
        DebugLog(@"LINKED URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"linkedin://profile/%@",linkedinstr]]);
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.linkedin.com"]];
        DebugLog(@"LINKED URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_linkedin"]]]);
    }
}


-(void)skypefunc: (id)sender
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"skype:%@",[dict_profile objectForKey:@"s_skype"]]]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"skype:%@",[dict_profile objectForKey:@"s_skype"]]]];
        
        DebugLog(@"SKYPE URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"skype:%@",[dict_profile objectForKey:@"s_skype"]]]);
    } else {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.skype.com/%@",[dict_profile objectForKey:@"s_skype"]]]];
        
        DebugLog(@"SKYPE URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"https://www.skype.com/%@",[dict_profile objectForKey:@"s_skype"]]]);
    }
}
-(void)skypeAction{
    
    if ([dict_profile objectForKey:@"s_skype"] != false && ![[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"s_skype"]]isEqualToString:@"0"] && ![[dict_profile objectForKey:@"s_skype"] isEqualToString:@""] && ![[dict_profile objectForKey:@"s_skype"] isKindOfClass:[NSNull class]] && [dict_profile objectForKey:@"s_skype"] != (id)[NSNull null] && ![[dict_profile objectForKey:@"s_skype"] isEqualToString:@"(null)"]) {
        
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"skype:%@",[dict_profile objectForKey:@"s_skype"]]]]) {
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"skype:%@",[dict_profile objectForKey:@"s_skype"]]]];
            
            DebugLog(@"SKYPE URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"skype:%@",[dict_profile objectForKey:@"s_skype"]]]);
        } else {
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.skype.com/%@",[dict_profile objectForKey:@"s_skype"]]]];
            
            DebugLog(@"SKYPE URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"https://www.skype.com/%@",[dict_profile objectForKey:@"s_skype"]]]);
        }
        
    }else{
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.skype.com"]]];
        
    }
    
    
    
}


-(void)soundcloudfunc: (id)sender
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.soundcloud.com/%@",[dict_profile objectForKey:@"s_soundcloud"]]]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.soundcloud.com/%@",[dict_profile objectForKey:@"s_soundcloud"]]]];
        
        DebugLog(@"SOUNDC URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"https://www.soundcloud.com/%@",[dict_profile objectForKey:@"s_soundcloud"]]]);
        
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.soundcloud.com"]];
        
        DebugLog(@"SOUNDC URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"https://www.soundcloud.com/%@",[dict_profile objectForKey:@"s_soundcloud"]]]);
    }
}



-(void)vimeofunc: (id)sender
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.vimeo.com/%@",[dict_profile objectForKey:@"s_vimeo"]]]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.vimeo.com/%@",[dict_profile objectForKey:@"s_vimeo"]]]];
        
        DebugLog(@"VIMEO URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"https://www.vimeo.com/%@",[dict_profile objectForKey:@"s_vimeo"]]]);
        
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.vimeo.com"]];
        
        DebugLog(@"VIMEO URL=============>%@",[NSURL URLWithString:[NSString stringWithFormat:@"https://www.vimeo.com/%@",[dict_profile objectForKey:@"s_vimeo"]]]);
    }
}


-(void)togglefunc: (UIButton *)sender
{
    DebugLog(@"TOGGLE FUNC CaLl HoChChE");
    
    if (switchprof == 0)
    {
        [UIView animateWithDuration:0.3 animations:^{
            
            //coverView.backgroundColor = [UIColor blackColor];
            
            UIColor *darkOp =
            [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
            UIColor *lightOp =
            [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
            
            // Create the gradient
            CAGradientLayer *gradient = [CAGradientLayer layer];
            
            // Set colors
            gradient.colors = [NSArray arrayWithObjects:
                               (id)lightOp.CGColor,
                               (id)darkOp.CGColor,
                               nil];
            
            // Set bounds
            gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
            
            // Add the gradient to the view
            [coverView.layer insertSublayer:gradient atIndex:0];
            
            
            
            [blackView setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0)];
            
            [businessImg setFrame:CGRectMake(15, 0, 61.0f/2.0f, 0)];
            
            [togglebt setFrame:CGRectMake(50, 0, 180, 0)];
            
            [lineDivMenu setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0)];
            
            [removeImg setFrame:CGRectMake(15, 0, 61.0f/2.0f, 0)];
            
            removeconbt.frame = CGRectMake(36, 0, 180, 0);
            
        }
         
         //        }];
         
                         completion:^(BOOL finished){
                             
                             rightmenuOpen = YES;
                             
                         }];
        
        switchprof = 1;
        [self business];
    }
    else
    {
        [UIView animateWithDuration:0.3 animations:^{
            
            //coverView.backgroundColor = [UIColor blackColor];
            
            UIColor *darkOp =
            [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
            UIColor *lightOp =
            [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
            
            // Create the gradient
            CAGradientLayer *gradient = [CAGradientLayer layer];
            
            // Set colors
            gradient.colors = [NSArray arrayWithObjects:
                               (id)lightOp.CGColor,
                               (id)darkOp.CGColor,
                               nil];
            
            // Set bounds
            gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
            
            // Add the gradient to the view
            [coverView.layer insertSublayer:gradient atIndex:0];
            
            
            
            [blackView setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0)];
            
            [businessImg setFrame:CGRectMake(15, 0, 61.0f/2.0f, 0)];
            
            [togglebt setFrame:CGRectMake(50, 0, 180, 0)];
            
            [lineDivMenu setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0)];
            
            [removeImg setFrame:CGRectMake(15, 0, 61.0f/2.0f, 0)];
            
            removeconbt.frame = CGRectMake(36, 0, 180, 0);
            
        }
                         completion:^(BOOL finished){
                             
                             rightmenuOpen = YES;
                             
                         }];
        
        switchprof = 0;
        [self loadViews];
    }
}


-(void)mobileFun:(UITapGestureRecognizer *)sender {
    
    //    mobileback.backgroundColor = [UIColor colorWithRed:(238.0f/255.0f) green:(238.0f/255.0f) blue:(238.0f/255.0f) alpha:1.0f];
    
    UIActionSheet *phoneAction = [[UIActionSheet alloc]initWithTitle:Nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles:@"Call",@"SMS", nil];
    
    phoneAction.tag = 100;
    [phoneAction showInView:mainview];
}

-(void)landphoneFun:(UITapGestureRecognizer *)sender {
    
    landlineback.backgroundColor = [UIColor colorWithRed:(238.0f/255.0f) green:(238.0f/255.0f) blue:(238.0f/255.0f) alpha:1.0f];
    
    UIActionSheet *landphoneAction = [[UIActionSheet alloc]initWithTitle:Nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles:@"Call", nil];
    landphoneAction.tag = 200;
    [landphoneAction showInView:mainview];
}


-(void)mailFun:(UITapGestureRecognizer *)sender {
    
    mailback.backgroundColor = [UIColor colorWithRed:(238.0f/255.0f) green:(238.0f/255.0f) blue:(238.0f/255.0f) alpha:1.0f];
    
    mailComposer = [[MFMailComposeViewController alloc]init];
    mailComposer.mailComposeDelegate = self;
    NSArray *recipentsArray = [NSArray arrayWithObject:mail];
    [mailComposer setToRecipients:recipentsArray];
    [mailComposer setMessageBody:@"" isHTML:NO];
    [self.navigationController presentViewController:mailComposer animated:YES completion:nil];
}

#pragma mark - mail compose delegate

-(void)mailComposeController:(MFMailComposeViewController *)controller

         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (result) {
        DebugLog(@"Result : %d",result);
    }
    if (error) {
        DebugLog(@"Error : %@",error);
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
    mailback.backgroundColor = [UIColor clearColor];
}



- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 100) {
        
        if (buttonIndex == 0) {
            UIDevice *device = [UIDevice currentDevice];
            
            if ([[device model] isEqualToString:@"iPhone"] ) {
                NSString *phoneNumber = [@"telprompt://" stringByAppendingString:[NSString stringWithFormat:@"%@",mobile]];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
                
            } else {
                
                UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                [notPermitted show];
            }
        }
        
        else if(buttonIndex == 1){
            
            UIDevice *device = [UIDevice currentDevice];
            
            if ([[device model] isEqualToString:@"iPhone"] ) {
                
                
                MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
                
                if([MFMessageComposeViewController canSendText])
                {
                    [self presentViewController:controller animated:YES completion:Nil];
                    controller.body = @"";
                    
                    NSString *smsstring= [NSString stringWithFormat:@"%@",[mobileArray objectAtIndex:0]];
                    
                    controller.recipients = [NSArray arrayWithObjects:smsstring, nil];
                    
                    controller.messageComposeDelegate = self;
                    
                    //[self presentModalViewController:controller animated:YES];
                    //                    mobileback.backgroundColor = [UIColor clearColor];
                }
            }
            else
            {
                UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                [notPermitted show];
                //                mobileback.backgroundColor = [UIColor clearColor];
            }
        }else{
            
            //            mobileback.backgroundColor = [UIColor clearColor];
            
        }
    }
    
    else if (actionSheet.tag == 200){
        
        
        
        if (buttonIndex == 0) {
            
            UIDevice *device = [UIDevice currentDevice];
            
            if ([[device model] isEqualToString:@"iPhone"] ) {
                
                NSString *phoneNumber = [@"telprompt://" stringByAppendingString:[NSString stringWithFormat:@"%@",landline]];
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
                
                
                
                //                [[UIApplication sharedApplication] openURL:[NSURL URLWithString: [NSString stringWithFormat:@"tel:%@",[phonesarr objectAtIndex:buttonIndex]]]];
                landlineback.backgroundColor = [UIColor clearColor];
                
            } else {
                
                UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                [notPermitted show];
                landlineback.backgroundColor = [UIColor clearColor];
                
            }
        }else{
            
            landlineback.backgroundColor = [UIColor clearColor];
            
        }
    }else if (actionSheet.tag == 900){
        
        if (buttonIndex == 0) {
            
            DebugLog(@"WhatsApp button clicked");
            [self showPersonViewController];
            
        }else if (buttonIndex == 1){
           
            if ([phonearr count] == 0)
            {
                [bgcallsms1 setBackgroundImage:[UIImage imageNamed:@"smsend_profile"] forState:UIControlStateNormal];
                
                
                smsAlert1 = [[UIAlertView alloc] initWithTitle:@"No Number found for this user!"
                                                       message:nil
                                                      delegate:self
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                smsAlert1.tag = 6;
                [smsAlert1 show];
            }
            else
            {
                
                [bgcallsms1 setBackgroundImage:[UIImage imageNamed:@"smsend_profile"] forState:UIControlStateNormal];
                
                UIDevice *device = [UIDevice currentDevice];
                if ([[device model] isEqualToString:@"iPhone"]) {
                    
                    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
                    if([MFMessageComposeViewController canSendText])
                    {
                        [self presentViewController:controller animated:YES completion:Nil];
                        controller.body = @"";
                        NSString *smsstring= [NSString stringWithFormat:@"%@",[phonearr objectAtIndex:0]];
                        controller.recipients = [NSArray arrayWithObjects:smsstring, nil];
                        controller.messageComposeDelegate = self;
                        //[self presentModalViewController:controller animated:YES];
                    }
                }
                else
                {
                    smsAlert2=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    smsAlert2.tag = 66;
                    [smsAlert2 show];
                }
            }
            
            
        }
        
    }
    
}


-(void)showPersonViewController
{
    DebugLog(@"profile name field text: %@",prof_name.text);
    // Fetch the address book
    CFErrorRef *errorab = nil;
    
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, errorab);
    
    // Search for the person named "Appleseed" in the address book
    //    CFArrayRef people = ABAddressBookCopyPeopleWithName(addressBook, CFSTR("Appleseed"));
    
    CFArrayRef people = ABAddressBookCopyPeopleWithName(addressBook,
                                                        (__bridge CFStringRef)prof_name.text);
    
    // Display "Appleseed" information if found in the address book
    if ((people != nil) && (CFArrayGetCount(people) > 0))
    {
        
        for (int m = 0; m < (CFArrayGetCount(people)); m++){
            DebugLog(@"count e dichhe: %ld == %@", (CFArrayGetCount(people)),people);
            ABRecordRef person = CFArrayGetValueAtIndex(people, 0);
            
            ABRecordID recordID = ABRecordGetRecordID(person);
            
            DebugLog(@"ab id: %d",recordID);
            DebugLog(@"phone array; %@", phonearr);
            
            ABMultiValueRef phoneNumberProperty = ABRecordCopyValue(person, kABPersonPhoneProperty);
            NSArray *_phoneNumbers = (__bridge NSArray*)ABMultiValueCopyArrayOfAllValues(phoneNumberProperty);
            
            NSMutableArray *mutableph= [_phoneNumbers mutableCopy];
            int pp;
            for (pp =0; pp< [mutableph count]; pp++)
            {
                if ([[mutableph objectAtIndex:pp] isKindOfClass:[NSNull class]] || [mutableph objectAtIndex:pp] == (id)[NSNull null] || [[mutableph objectAtIndex:pp] length] < 3)
                {
                    [mutableph removeObjectAtIndex:pp];
                    pp= pp-1;
                }
            }
            NSMutableSet* set1 = [NSMutableSet setWithArray:mutableph];
            NSMutableSet* set2 = [NSMutableSet setWithArray:phonearr];
            [set1 intersectSet:set2]; //this will give you only the obejcts that are in both sets
            
            NSArray* result = [set1 allObjects];
            DebugLog(@"result pelo: %@",result);
            
            if ([result count] > 0)
            {
                NSString * urlWhats = [NSString stringWithFormat:@"whatsapp://send?abid=%d&text=",recordID];
                NSURL * whatsappURL = [NSURL URLWithString:[urlWhats stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
                    [[UIApplication sharedApplication] openURL: whatsappURL];
                } else {
                    UIAlertView * alert1 = [[UIAlertView alloc] initWithTitle:@"WhatsApp not installed." message:@"Your device has no WhatsApp installed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert1 show];
                }
                break;
            }
        }
    }
    else
    {
        // Show an alert if "Appleseed" is not in Contacts
    }
    //CFRelease(addressBook);
    //CFRelease(people);
}



- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    if (result == MessageComposeResultCancelled) {
        DebugLog(@"Message cancelled");
    } else if (result == MessageComposeResultSent) {
        DebugLog(@"Message sent");
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


+(void)chngpostion
{
    DebugLog(@"Change Position profile page");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Data" object:Nil];
}


-(void)requestreceivedaction
{
    del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    ConDetailsViewController *con = [[ConDetailsViewController alloc]init];
    con.other=@"yes";
    con.uid=[NSString stringWithFormat:@"%@",del.pushtoprofileid];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}

-(void)acceptedrequestaction
{
    ConRequestViewController *con = [[ConRequestViewController alloc]init];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}



-(void)navtopage: (NSNotification *)notification
{
    DebugLog(@"navtopage");
    NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
    if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"4"])
    {
        ConAccountSettingsViewController *con = [[ConAccountSettingsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"6"])
    {
        ConInviteViewController *con = [[ConInviteViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"5"])
    {
        ConSyncLoaderViewController *con = [[ConSyncLoaderViewController alloc]init];
        //        [self.navigationController pushViewController:con animated:NO];
        
        [self presentViewController:con animated:NO completion:nil];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"2"])
    {
        ConDetailsViewController *con = [[ConDetailsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"3"])
    {
        ConQROptionsViewController *con = [[ConQROptionsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    
    
    
    //    [leftmenu setFrame:CGRectMake(-280, leftmenu.frame.origin.y, leftmenu.frame.size.width, leftmenu.frame.size.height)];
    
    [mainview setFrame:CGRectMake(0, mainview.frame.origin.y, mainview.frame.size.width, mainview.frame.size.height)];
    
    del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    
    
    move=1;
    
}



-(void)getData:(NSNotification *)notification {
    
    DebugLog(@"mainview.frame.origin.x = %f",mainview.frame.origin.x);
    
    if(move == 0) {
        
        DebugLog(@"if prof move");
        
        [UIView animateWithDuration:0.25
         
                         animations:^{
                             
                             [mainview setFrame:CGRectMake(280, (mainview.frame.origin.y), mainview.frame.size.width, mainview.frame.size.height)];
                             
                         }
         
                         completion:^(BOOL finished){
                             
                             move=1;
                         }];
        
    } else {
        
        DebugLog(@"else prof move");
        
        [UIView animateWithDuration:0.25
         
                         animations:^{
                             
                             [mainview setFrame:CGRectMake(0, (mainview.frame.origin.y), mainview.frame.size.width, mainview.frame.size.height)];
                         }
         
                         completion:^(BOOL finished){
                             move=0;
                         }];
        
    }
    
}



-(void)viewDidDisappear:(BOOL)animated
{
    move=0;
    personal_array = Nil;
    business_array =Nil;
    social_array = Nil;
    dict_profile = nil;
    dict_profile = nil;
    dict_profile = nil;
    profdict = nil;
    dict_profile = nil;
    [timea invalidate];
    [coverview1 removeFromSuperview];
    [SVProgressHUD dismiss];
    [mainview removeFromSuperview];
    
    self.navigationController.navigationBarHidden=YES;
    [super viewDidDisappear:YES];
}


-(void)openmap: (UIGestureRecognizer *)sender
{
    addressback.backgroundColor = [UIColor colorWithRed:(238.0f/255.0f) green:(238.0f/255.0f) blue:(238.0f/255.0f) alpha:1.0f];
    
    if (switchprof == 0)
    {
        latitude=[[dict_profile objectForKey:@"lat"] doubleValue];
        longitude=[[dict_profile objectForKey:@"lng"] doubleValue];
    }
    else
    {
        latitude=[[dict_profile objectForKey:@"b_lat"] doubleValue];
        longitude=[[dict_profile objectForKey:@"b_lng"] doubleValue];
    }
    DebugLog(@"lati=== %f",latitude);
    DebugLog(@"long=== %f",longitude);
    [self generateRoute];
}

- (void)getDirections
{
}

-(void)showRoute:(MKDirectionsResponse *)response1
{
    for (MKRoute *route in response1.routes)
    {
        [map_View addOverlay:route.polyline level:MKOverlayLevelAboveLabels];
        
        for (MKRouteStep *step in route.steps)
        {
            DebugLog(@" here %@", step.instructions);
        }
    }
}


- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id < MKOverlay >)overlay
{
    MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
    renderer.strokeColor = [UIColor blueColor];
    renderer.lineWidth = 4.0;
    return renderer;
}


- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    CLLocation *location = map_View.userLocation.location;
    DebugLog(@"lat current: %f - long current: %f", location.coordinate.latitude, location.coordinate.longitude);
}


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    // Handle any custom annotations.
    
    if ([annotation isKindOfClass:[MKPointAnnotation class]])
    {
        // Try to dequeue an existing pin view first.
        
        MKPinAnnotationView *pinView = (MKPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomPinAnnotationView"];
        //        if (!pinView)
        //        {
        pinView = [[MKPinAnnotationView alloc] init];
        //            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomPinAnnotationView"];
        
        pinView.animatesDrop = YES;
        pinView.canShowCallout = YES;
        pinView.image = [UIImage imageNamed:@"locator1.png"];
        pinView.calloutOffset = CGPointMake(0, 0);
        
        
        UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        
        pinView.rightCalloutAccessoryView = rightButton;
        
        
        UIImageView *iconView = [[UIImageView alloc] init];
        
        iconView.frame = CGRectMake(0.0f, 0.0f,20, 20);
        
        pinView.leftCalloutAccessoryView = iconView;
        
        iconView.backgroundColor=[UIColor whiteColor];
        
        
        if (annotation == mapView.userLocation)
            
        {
            pinView.image= [UIImage imageNamed:@"locatorown.png"];
            
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomPinAnnotationView"];
        }
        
        DebugLog(@"(int)annotation.subtitle===%@",annotation.subtitle);
        
        iconView.image= profilepic;
        
        if (switchprof == 0)
            pinView.image = [UIImage imageNamed:@"locator1.png"];
        else
            pinView.image = [UIImage imageNamed:@"locator2.png"];
        
        //        }
        return pinView;
    }
    return nil;
}





-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    DebugLog(@"annotation selected");
}

- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)aUserLocation {
    
    MKCoordinateRegion region;
    
    MKCoordinateSpan span;
    
    span.latitudeDelta = 0.005;
    
    span.longitudeDelta = 0.005;
    
    CLLocationCoordinate2D location;
    
    location.latitude =  latitude;       //aUserLocation.coordinate.latitude;
    
    location.longitude = longitude;      //aUserLocation.coordinate.longitude;
    
    region.span = span;
    
    region.center = location;
    
    googleMapUrlString = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%f,%f&daddr=%f,%f", aUserLocation.location.coordinate.latitude,aUserLocation.location.coordinate.longitude, location.latitude, location.longitude];
    
    DebugLog(@"url fired map: %@",googleMapUrlString);
    
    //    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapUrlString]];
    appleMapUrlString = [NSString stringWithFormat:@"http://maps.apple.com/maps?saddr=%f,%f&daddr=%f,%f", aUserLocation.location.coordinate.latitude,aUserLocation.location.coordinate.longitude, location.latitude, location.longitude];
    [self generateRoute];
}

-(void)crossmap
{
    [crossbt removeFromSuperview];
    [mapview removeFromSuperview];
    [tapbgview removeFromSuperview];
    [navigatebt removeFromSuperview];
    [navigatebtapple removeFromSuperview];
}


- (void)generateRoute {
    CLLocationCoordinate2D end = {latitude, longitude};
    
    MKMapItem *destination_mapitem = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:end addressDictionary:nil]];
    
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
    
    request.source = [MKMapItem mapItemForCurrentLocation];
    
    request.destination = destination_mapitem;
    
    [request setTransportType:MKDirectionsTransportTypeAny]; // This can be limited to automobile and walking directions.
    
    [request setRequestsAlternateRoutes:YES];
    
    MKDirections *directions = [[MKDirections alloc] initWithRequest:request];
    
    [directions calculateDirectionsWithCompletionHandler:
     
     ^(MKDirectionsResponse *response1, NSError *error) {
         
         if (error) {
             DebugLog(@"error generate route----  %@",error);
             
         } else {
             [self showRoute:response1];
         }
     }];
    [self navigateinapple];
}


-(void)navigatetoloc
{
    ConNavigateViewController *con = [[ConNavigateViewController alloc]init];
    con.fireurl = googleMapUrlString;
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}


-(void)navigateinapple
{
    CLLocationCoordinate2D endingCoord = CLLocationCoordinate2DMake(latitude, longitude);
    MKPlacemark *endLocation = [[MKPlacemark alloc] initWithCoordinate:endingCoord addressDictionary:nil];
    MKMapItem *endingItem = [[MKMapItem alloc] initWithPlacemark:endLocation];
    NSMutableDictionary *launchOptions = [[NSMutableDictionary alloc] init];
    [launchOptions setObject:MKLaunchOptionsDirectionsModeDriving forKey:MKLaunchOptionsDirectionsModeKey];
    [endingItem openInMapsWithLaunchOptions:launchOptions];
}


-(void)detailpic: (UIGestureRecognizer *)sender
{
    ConPictureProfViewController *con = [[ConPictureProfViewController alloc]init];
    con.profilepic = profilepic;
    //    con.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:con animated:NO completion:nil];
}



-(void)closedetailpic: (UIGestureRecognizer *)sender

{
    
    CATransition *animation = [CATransition animation];
    
    
    
    [animation setType:kCATransitionFade];
    
    
    
    [animation setSubtype:kCATransitionFade];
    
    
    
    animation.duration = 0.7f;
    
    
    
    [detailimg.layer addAnimation:animation forKey:nil];
    
    
    
    if(detailimg.hidden==NO)
        
    {
        
        detailimg.hidden=YES;
        
    }
    
    [tapbgview removeFromSuperview];
    
}





- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data

{
    
    [responseData appendData:data];
    
    
    
}



- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error

{
    
    DebugLog(@"connection failed");
    
}



- (void)connectionDidFinishLoading:(NSURLConnection *)connection

{
    
    DebugLog(@"connection finished loading");
    
    
    
    DebugLog(@"response data - %@", [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
    
    
    
    NSError *err;
    
    
    if (responseData != nil)
        
    {
        
        NSDictionary  *json1aa=[NSJSONSerialization JSONObjectWithData:responseData options:0 error:&err];
        
        DebugLog(@"this now has json string url: %@",json1aa);
        
    }
    
    else
        
    {
        
        alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                 
                                           message:nil
                 
                                          delegate:self
                 
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        
        alert.tag=7;
        
        //        [alert show];
    }
}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    responseData = [NSMutableData data];
}

-(void)servererror
{
    alert = [[UIAlertView alloc] initWithTitle:@"Error in Profile Server Connection!"
                                       message:nil delegate:self
                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
    [alert show];
}

-(void) reloadDataFromWeb
{
    DebugLog(@"reload profile from web");
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [mainview addSubview:activity];
    activity.center= self.view.center;
    activity.tintColor=[UIColor grayColor];
    //    [activity startAnimating];
    
    //    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    NSString *errornumber;
    
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=profile&id=%@&access_token=%@&device_id=%@&image=true",user_id,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
    
    DebugLog(@"profile url: %@",urlString1);
    
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL11a =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    
    if (signeddataURL11a == nil)
    {
        DebugLog(@"no connnn profile");
        
        // [self performSelectorOnMainThread:@selector(servererror) withObject:nil waitUntilDone:YES];
        [self reloadFromLocalDB];
    }
    //    DebugLog(@"json returns: %@",json1);
    else
    {
        //            [alert show];
        
        
        NSError *error;
        json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL11a //1
                 
                                                options:kNilOptions
                 
                                                  error:&error];
        errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
        
        DebugLog(@"err  %@",errornumber);
        
        
        if (![errornumber isEqualToString:@"0"])
        {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"error"];
            [activity removeFromSuperview];
            NSString *err_str = [json1 objectForKey:@"error"];
            
            alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                     
                                               message:err_str
                     
                                              delegate:self
                     
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            //            [alert show];
        }
        else
        {
            dict_profile = [NSMutableDictionary new];
            
            profdict = [NSMutableDictionary new];
            //
            //            dict_profile = [NSMutableDictionary new];
            
            
            
            profdict = [json1 objectForKey:@"details"];
            
            dict_profile = [profdict objectForKey:@"profile"];
            [activity removeFromSuperview];
            
            
            if ([user_id intValue] == [[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]intValue])
                switchprof=0;
            else
            {
                if ([[dict_profile objectForKey:@"friend"]intValue] == 1)
                {
                    switchprof=0;
                }
                
                else if ([[dict_profile objectForKey:@"business"] intValue]==1)
                {
                    switchprof=1;
                }
                DebugLog(@"fields are: %d , %d",[[dict_profile objectForKey:@"own"]intValue], switchprof);
            }
        }
  
        
    }
    
    if (signeddataURL11a != nil && [errornumber isEqualToString:@"0"] && [dict_profile count] > 0)
    {
        
        [[DBManager getSharedInstance]insertProfile:profdict :[user_id intValue]];
        
    }
    
}



-(void) reloadFromLocalDB
{
    
    
    NSDictionary *localDBProfile = [[NSDictionary alloc]init];
    
    localDBProfile = [[DBManager getSharedInstance]fetchProfile:[user_id intValue]];
    
    DebugLog(@"this gives: %@",localDBProfile);
    
    if ([localDBProfile isKindOfClass:[NSNull class]] || localDBProfile == (id)[NSNull null])
    {
        //        [self reloadDataFromWeb];
        [self performSelectorOnMainThread:@selector(servererror) withObject:nil waitUntilDone:YES];
        
    }
    else{
        
        dict_profile = [[NSMutableDictionary alloc]init];
        
        dict_profile = [[NSMutableDictionary alloc]init];
        
        dict_profile = [[NSMutableDictionary alloc]init];
        
        dict_profile = [localDBProfile mutableCopy];
        profdict = [dict_profile mutableCopy];
        [activity removeFromSuperview];
        
        
        NSString *fullname1;
        
        if (![[NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]] isKindOfClass:[NSNull class]] && ![[NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]] isEqualToString:@""] && ![[NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]] isEqualToString:@"(null)"] && [[NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]] length]>0)
        {
            
            fullname1= [NSString stringWithFormat:@"%@ %@",[[dict_profile objectForKey:@"name"] capitalizedString],[[dict_profile objectForKey:@"surname"] capitalizedString]];
        }
        
        [prof_name removeFromSuperview];
        
        prof_name = [[UILabel alloc]init]; //WithFrame:CGRectMake(112, 72, 196, 60)];
        prof_name.backgroundColor=[UIColor clearColor];
        prof_name.textColor=[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
        
        
        if ([fullname1 isKindOfClass:[NSNull class]] || [fullname1 isEqualToString:@""] || [fullname1 isEqualToString:@"(null) (null)"] || [fullname1 length]==0) {
            
            prof_name.text = @"";
            DebugLog(@"PROFILE NAME1:%@",prof_name);
            prof_name.frame=CGRectMake(0, 0, 0, 0);
            
            
        }
        else
        {
            prof_name.text=fullname1;
            prof_name.frame=CGRectMake(120, 75-70, 195, 60);
        }
        
        prof_name.font=[UIFont fontWithName:@"ProximaNova-Bold" size:18];
        prof_name.numberOfLines=0;
        
        
        UIImageView *prof_img = [[UIImageView alloc]initWithFrame:CGRectMake(15, 75-64, 90, 90)];
        
        [grayback addSubview:prof_img];
        
        base64String= [localDBProfile objectForKey:@"thumb"];
        
        if ([base64String length] >6)
        {
            NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
            
            //    NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
            
            profilepic = [UIImage imageWithData:decodedData];
            
            prof_img.image=profilepic;
            
            prof_img.contentMode= UIViewContentModeScaleAspectFill;
            
            prof_img.clipsToBounds=YES;
        }
        
        prof_img.userInteractionEnabled=YES;
        
        UITapGestureRecognizer *propictap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(detailpic:)];
        
        [prof_img addGestureRecognizer:propictap];
        
        
    }
    
    //    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"isfriend"] integerValue]==2)
    //        [self loadBusinessView];
    //    else
    //        [self loadPersonalViews];
    
    //    [self loadViews];
}

-(void)startDBInsert
{
    }

-(void)startDBInserta:(NSNotification *)notification
{
    
    if (profile_insertion == 0)
    {
        profile_insertion =1;
        DebugLog(@"startdbinserta called");
        [[DBManager getSharedInstance]insertProfile:profdict :[user_id intValue]];
    }
}

-(void)removeConnection//: (id)sender
{
    
    [UIView animateWithDuration:0.3 animations:^{
        
        //coverView.backgroundColor = [UIColor blackColor];
        
        UIColor *darkOp =
        [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
        UIColor *lightOp =
        [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
        
        // Create the gradient
        CAGradientLayer *gradient = [CAGradientLayer layer];
        
        // Set colors
        gradient.colors = [NSArray arrayWithObjects:
                           (id)lightOp.CGColor,
                           (id)darkOp.CGColor,
                           nil];
        
        // Set bounds
        gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
        
        // Add the gradient to the view
        [coverView.layer insertSublayer:gradient atIndex:0];
        
        
        
        
        [blackView setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0)];
        
        [businessImg setFrame:CGRectMake(15, 0, 61.0f/2.0f, 0)];
        
        [togglebt setFrame:CGRectMake(50, 0, 180, 0)];
        
        [lineDivMenu setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0)];
        
        [removeImg setFrame:CGRectMake(15, 0, 61.0f/2.0f, 0)];
        
        removeconbt.frame = CGRectMake(36, 0, 180, 0);
        
    }
     
     //        }];
     
                     completion:^(BOOL finished){
                         
                         rightmenuOpen = YES;
                         
                     }];

    
    
    alert = [[UIAlertView alloc] initWithTitle:@"Remove this contact?"
                                       message:nil
                                      delegate:self
                             cancelButtonTitle:@"Cancel"  otherButtonTitles:@"Ok", nil];
    alert.tag=123;
    [alert show];
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    DebugLog(@"ALERT . TAG====%ld",(long)alertView.tag);
    if (alertView.tag == 123) {
        
        if(buttonIndex == 0)
        {
            // Do something
        }
        else
        {
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
//            NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=removeconnection&id=%d&access_token=%@&device_id=%@&business=true",[user_id intValue],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
            
            NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action= group-removemember&group_id=%d&user_id=%d&access_token=%@&device_id=%@&business=true",[groupid intValue], [user_id intValue],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
            
            
            DebugLog(@"deny url: %@",urlString1);
            NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
            NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
            
            if (signeddataURL1 != nil)
            {
                NSError *error=nil;
                NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                          options:kNilOptions
                                                                            error:&error];
                DebugLog(@"deny json returns: %@",json_deny);
                if ([[json_deny objectForKey:@"success"]intValue] == 1)
                {
                    alert = [[UIAlertView alloc] initWithTitle:@"Connection Successfully Removed!"
                                                       message:nil
                                                      delegate:self
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                    [alert show];
                    [removeconbt removeFromSuperview];
                    //                togglebt.userInteractionEnabled = NO;
                    
                    ConNewRequestsViewController *mng = [[ConNewRequestsViewController alloc]init];
                    //                mng.imported_arr = [filtered_arr objectAtIndex:indexPath.row];
                    mng.userid= [user_id intValue];
                    DebugLog(@"selected user id : %d",mng.userid);
                    
//                    CATransition* transition = [CATransition animation];
//                    
//                    transition.duration = 0.4;
//                    transition.type = kCATransitionPush;
//                    transition.subtype = kCATransitionFade;
//                    
//                    [[self navigationController].view.layer addAnimation:transition forKey:nil];
                    
                    [self.navigationController pushViewController:mng animated:YES];
                }
                else
                {
                    alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                                       message:[NSString stringWithFormat:@"%@",[json_deny objectForKey:@"error"]]
                                                      delegate:self
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                    [alert show];
                }
            }
            else
            {
                alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                                   message:nil
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                [alert show];
            }
        }
    }else if (alertView.tag == 9 || alertView.tag == 99){
        
        DebugLog(@"BUTTON INDEX 0");
        if (buttonIndex == 0)
        {
            
            [bgcallsms setBackgroundImage:[UIImage imageNamed:@"call_profile"] forState:UIControlStateNormal];
            
        }
        
    }else if (alertView.tag == 6 || alertView.tag == 66){
        
        if(buttonIndex == 0)
        {
            [bgcallsms1 setBackgroundImage:[UIImage imageNamed:@"sms_profile"] forState:UIControlStateNormal];
            
        }
        
    }
}

-(void)websiteFun: (id)sender
{
    DebugLog(@"dcdc");
    SocialWebViewController *social= [[SocialWebViewController alloc]init];
    if (switchprof == 0)
        //        social.type_social=[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"website"]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"website"]]]];
    else
        //        social.type_social=[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_website"]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"b_website"]]]];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:social animated:YES];
}

-(void)yesSuccess
{
    //    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //    [del showTabValues:YES];
}

-(void)editprofile_func: (id)sender
{
    ConFullSettingsViewController *con =[[ConFullSettingsViewController alloc]init];
    con._control_check = TRUE;
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    transition.subtype = kCATransitionFromRight;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:NO];
    //[self presentViewController:con animated:NO completion:nil];
}

-(void)phonefunc
{
    DebugLog(@"call phonesarr: %@",phonearr);
    if ([phonearr count] == 0)
    {
        [bgcallsms setBackgroundImage:[UIImage imageNamed:@"callend_profile"] forState:UIControlStateNormal];
        
        callAlert1 = [[UIAlertView alloc] initWithTitle:@"No Number found for this user!"
                                                message:nil
                                               delegate:self
                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        callAlert1.tag = 9;
        [callAlert1 show];
    }
    else
    {
        [bgcallsms setBackgroundImage:[UIImage imageNamed:@"callend_profile"] forState:UIControlStateNormal];
        
        
        UIDevice *device = [UIDevice currentDevice];
        if ([[device model] isEqualToString:@"iPhone"] ) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString: [NSString stringWithFormat:@"tel:%@",[phonearr objectAtIndex:0]]]];
        } else {
            callAlert2=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            callAlert2.tag = 99;
            [callAlert2 show];
        }
    }
}


-(void)chatfunc:(UIButton *)sender
{
    
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select a option"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"WhatsApp", @"SMS", nil];
    
    //            [actionSheet showInView:self.view];
    actionSheet.tag = 900;
    [actionSheet showInView:mainview];
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

-(void)gobackoption:(UIButton *)button
{
    //    backBground.backgroundColor = [UIColor colorWithRed:(30.0f/255.0f) green:(30.0f/255.0f) blue:(30.0f/255.0f) alpha:1.0f];
    if (button.tag==0)
    {
        
        CATransition *transition = [CATransition animation];
        
        transition.duration = 0.4f;
        
        transition.type = kCATransitionFade;
        
        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    if (button.tag==1)
    {
        
        CATransition *transition = [CATransition animation];
        
        transition.duration = 0.4f;
        
        transition.type = kCATransitionFade;
        
        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    if (button.tag==2)
    {
        GroupDetailsViewController *grp=[[GroupDetailsViewController alloc]init];
        grp.filter=YES;
        grp.filtered_arr=[filter_arr mutableCopy];
        grp.isadmin=isadmin;
        grp.groupname=groupname;
        grp.groupimage.hidden=YES;
        grp.group_namelbl.hidden=YES;
        //grp.searchBar.hidden=NO;
        grp.check_search=@"YES";
        grp.groupid=groupid;
        DebugLog(@"member count 4 %@",membercount);
        grp.member_count=membercount;
        grp.group_array=search;
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:grp animated:YES];
        //        [self.navigationController popViewControllerAnimated:NO];
    }
    
    if (button.tag==3)
    {
        GroupDetailsViewController *grp=[[GroupDetailsViewController alloc]init];
        grp.search_result=[filter_arr mutableCopy];
        DebugLog(@"copied array 2: %@",grp.search_result);
        grp.groupstable.hidden=YES;
        //        grp.searchtableback.hidden=NO;
        grp.isadmin=isadmin;
        [grp.invite setEnabled:YES];
        DebugLog(@"member count 4 %@",membercount);
        grp.member_count=membercount;
        grp.groupname=groupname;
        grp.groupid=groupid;
        grp.check_member=@"YES";
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        transition.subtype = kCATransitionFromLeft;
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:grp animated:NO];
        //[self.navigationController popViewControllerAnimated:YES];
    }
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return array_menu.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MyReuseIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    cell=nil;
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:MyIdentifier];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.backgroundColor=[UIColor clearColor];
    if (switchprof == 0)
    {
        UILabel *menulabel = [[UILabel alloc]initWithFrame:CGRectMake(55.0f,0.0f, righttable.frame.size.width-55, 57.5f)];
        [menulabel setText:[NSString stringWithFormat:@"%@",[array_menu objectAtIndex:indexPath.row]]];
        [menulabel setTextAlignment:NSTextAlignmentLeft];
        [menulabel setTextColor:[UIColor whiteColor]];
        menulabel.userInteractionEnabled=YES;
        [menulabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
        [cell addSubview:menulabel];
    }
    else
    {
        UILabel *menulabel = [[UILabel alloc]initWithFrame:CGRectMake(55.0f,0.0f, righttable.frame.size.width-55, 57.5f)];
        [menulabel setText:[NSString stringWithFormat:@"%@",[arraymenu_busi objectAtIndex:indexPath.row]]];
        [menulabel setTextAlignment:NSTextAlignmentLeft];
        [menulabel setTextColor:[UIColor whiteColor]];
        menulabel.userInteractionEnabled=YES;
        [menulabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
        [cell addSubview:menulabel];
        
    }
    UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(15.0f,18.75f,20.0f,20.0f)];
    image.backgroundColor=[UIColor clearColor];
    image.userInteractionEnabled=YES;
    [image setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_image objectAtIndex:indexPath.row]]]];
    image.contentMode=UIViewContentModeScaleAspectFit;
    [cell addSubview:image];
    
    UILabel *separatorlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 57, [UIScreen mainScreen].bounds.size.width,0.5)];
    separatorlabel.backgroundColor = [UIColor colorWithRed:(102.0f/255.0f) green:(102.0f/255.0f) blue:(102.0f/255.0f) alpha:1.0f];
    [cell addSubview:separatorlabel];
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 57.5f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
        //        [self performSelector:@selector(togglefunc:)];
        if (switchprof == 1) {
            switchprof = 0;
            [businessview removeFromSuperview];
            businessview=nil;
            
            [self loadViews];
            blackView.hidden=YES;
            rightmenuOpen=YES;
        }
        else
        {
            switchprof = 1;
            [personalview removeFromSuperview];
            personalview=nil;
            [self business];
            blackView.hidden=YES;
            rightmenuOpen=YES;
        }
    }
    if (indexPath.row==1) {
//        [self performSelector:@selector(removeConnection:)];
        
        [self removeConnection];
        blackView.hidden=YES;
        rightmenuOpen=YES;
    }
}

-(void)rightMenu:(UIButton *)sender{
    
    DebugLog(@"rightMenu: %x", rightmenuOpen);
    
    [overlayTap setEnabled:NO];
    //    menuBground.backgroundColor = [UIColor clearColor];
    array_menu= [[NSMutableArray alloc] initWithObjects:@"View Business Card",@"Remove Connection", nil];
    arraymenu_busi= [[NSMutableArray alloc] initWithObjects:@"View Personal Card",@"Remove Connection", nil];
    array_image= [[NSMutableArray alloc] initWithObjects:@"business_profile",@"remove_profile", nil];
    
    if(rightmenuOpen == YES) {
        
        //        menuBground.backgroundColor = [UIColor colorWithRed:(30.0f/255.0f) green:(30.0f/255.0f) blue:(30.0f/255.0f) alpha:1.0f];
        
        blackView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 110)];
        //        [blackView setBackgroundColor:[UIColor blackColor]];
        blackView.backgroundColor=[[UIColor blackColor] colorWithAlphaComponent:0.9f];
        //        [blackView setAlpha:0.9f];
        //        blackView.layer.zPosition = 3;
        blackView.opaque=NO;
        [blackView setUserInteractionEnabled:YES];
        [grayback addSubview:blackView];
        
        righttable=[[UITableView alloc] initWithFrame:CGRectMake(0, blackView.frame.origin.y, blackView.frame.size.width, blackView.frame.size.height)];
        righttable.delegate=self;
        righttable.dataSource=self;
        [righttable setBackgroundColor:[UIColor clearColor]];
        righttable.separatorStyle=UITableViewCellSeparatorStyleNone;
        righttable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        [blackView addSubview:righttable];
        [righttable reloadData];
        rightmenuOpen = NO;
        DebugLog(@"blackview color %@", blackView.backgroundColor);
    }
    else if (rightmenuOpen == NO)
    {
        
        blackView.hidden=YES;
        rightmenuOpen = YES;
    }
    
    
}
-(void)changeButtonBackGroundColor:(id)sender{
    
    [rightMenuBack setBackgroundColor:[UIColor colorWithRed:(30.0f/255.0f) green:(30.0f/255.0f) blue:(30.0f/255.0f) alpha:1.0f]];
    
}
-(void)resetButtonBackGroundColor:(id)sender{
    
    [rightMenuBack setBackgroundColor:[UIColor blackColor]];
    
}

-(void)setAddress
{
    
    
    y1=0;
    
    if (![street isEqualToString: @""] && [number isEqualToString: @""])
    {
        street_number_lbl.text=[NSString stringWithFormat:@"%@,",street];
        street_number_lbl.frame = CGRectMake(38, divider_2.frame.origin.y+divider_2.frame.size.height+8, 250, 20);
        y1=street_number_lbl.frame.size.height+8;
    }
    else if ([street isEqualToString: @""] && ![number isEqualToString: @""])
    {
        street_number_lbl.text=[NSString stringWithFormat:@"%@,",number];
        street_number_lbl.frame = CGRectMake(38, divider_2.frame.origin.y+divider_2.frame.size.height+8, 250, 20);
        y1=street_number_lbl.frame.size.height+8;
    }
    else if (![street isEqualToString: @""] && ![number isEqualToString: @""])
    {
        street_number_lbl.text=[NSString stringWithFormat:@"%@, %@,",street,number];
        street_number_lbl.frame = CGRectMake(38, divider_2.frame.origin.y+divider_2.frame.size.height+8, 250, 20);
        y1=street_number_lbl.frame.size.height+8;
    }
    else if ([street isEqualToString: @""] && [number isEqualToString: @""])
    {
        street_number_lbl.frame = CGRectMake(38, divider_2.frame.origin.y+divider_2.frame.size.height+8, 250, 0);
        y1=street_number_lbl.frame.size.height+8;
    }
    if (![zipcode isEqualToString: @""] && [city isEqualToString:@""])
    {
        zipcode_lbl.text=[NSString stringWithFormat:@"%@",zipcode];
        zipcode_lbl.frame = CGRectMake(38, street_number_lbl.frame.origin.y+street_number_lbl.frame.size.height, 250, 20);
        y1=y1+zipcode_lbl.frame.size.height;
    }
    else if ([zipcode isEqualToString: @""] && ![city isEqualToString:@""])
    {
        zipcode_lbl.text=[NSString stringWithFormat:@"%@",city];
        zipcode_lbl.frame = CGRectMake(38, street_number_lbl.frame.origin.y+street_number_lbl.frame.size.height, 250, 20);
        y1=y1+zipcode_lbl.frame.size.height;
    }
    else if (![zipcode isEqualToString: @""] && ![city isEqualToString:@""])
    {
        zipcode_lbl.text=[NSString stringWithFormat:@"%@, %@",zipcode,city];
        zipcode_lbl.frame = CGRectMake(38, street_number_lbl.frame.origin.y+street_number_lbl.frame.size.height, 250, 20);
        y1=y1+zipcode_lbl.frame.size.height;
    }
    else if ([zipcode isEqualToString: @""] && [city isEqualToString:@""])
    {
        zipcode_lbl.frame = CGRectMake(38, street_number_lbl.frame.origin.y+street_number_lbl.frame.size.height, 250, 0);
        y1=y1+zipcode_lbl.frame.size.height;
    }
    if (![country isEqualToString: @""]) {
        country_lbl.text=[NSString stringWithFormat:@"%@",country];
        country_lbl.frame = CGRectMake(38, zipcode_lbl.frame.origin.y+zipcode_lbl.frame.size.height, 250, 20);
        y1=y1+country_lbl.frame.size.height;
    }
    else if ([country isEqualToString: @""])
    {
        country_lbl.frame = CGRectMake(38, zipcode_lbl.frame.origin.y+zipcode_lbl.frame.size.height, 250, 0);
        y1=y1+country_lbl.frame.size.height;
    }
    DebugLog(@"street frame: %@",NSStringFromCGRect(street_number_lbl.frame));
    
      
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
