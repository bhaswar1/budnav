//
//  ConNavigateViewController.h
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 09/06/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface ConNavigateViewController : GAITrackedViewController

@property (nonatomic) float destlat,destlong,currlat,currlong;
@property (nonatomic,retain) NSString *fireurl;
@end
