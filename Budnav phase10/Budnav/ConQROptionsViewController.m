//
//  ConQROptionsViewController.m
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 22/05/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
#import "SVProgressHUD.h"
#import "SCViewController.h"
#import "ConQROptionsViewController.h"
#import "AppDelegate.h"
#import "ConProfileOwnViewController.h"
#import "ConAccountSettingsViewController.h"
#import "ConInviteViewController.h"
#import "ConSyncLoaderViewController.h"
#import "ConRequestViewController.h"
#import "ConNewProfileViewController.h"
#import "ConAddFriendViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"
#import "ConLocateGroupViewController.h"

@interface ConQROptionsViewController ()
{
    //   UIView *mainview;
    int move;
    UIImage *imgqr;
}
@end

@implementation ConQROptionsViewController
@synthesize mainview;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    DebugLog(@"requests");
    
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    self.navigationController.navigationBarHidden=YES;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    
    move=0;
    
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    self.navigationController.navigationBarHidden=YES;
    
    mainview = [[UIView alloc]initWithFrame:CGRectMake(0, 65, [UIScreen mainScreen].bounds.size.width, self.view.frame.size.height-60)];
    [self.view addSubview:mainview];
    mainview.backgroundColor=[UIColor whiteColor];
    
    //    [SVProgressHUD showWithStatus:@"Loading"];
    
    //    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    //    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    //
    //    NSString *urlString_qr =[NSString stringWithFormat:@"https://budnav.com/ext/?action=qr&id=%@&access_token=%@&device_id=%@",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
    //
    //    NSString *newString_qr = [urlString_qr stringByReplacingOccurrencesOfString:@" " withString:@""];
    //
    //    NSData *signeddataURL_qr =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString_qr]];
    //
    //    if (signeddataURL_qr != nil)
    //    //    DebugLog(@"json returns: %@",json1);
    //    imgqr = [[UIImage alloc]initWithData: signeddataURL_qr];
    //   else
    //    {
    //       alert = [[UIAlertView alloc] initWithTitle:@"Error in Retrieving QR Code Image!"
    //                                       message:nil
    //                                      delegate:self
    //                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
    ////       [alert show];
    ////        [SVProgressHUD dismiss];
    //        [act removeFromSuperview];
    //    }
    //
    //        dispatch_async(dispatch_get_main_queue(), ^{
    ////            [SVProgressHUD dismiss];
    //            [act removeFromSuperview];
    //
    //            UIImageView *zipimage=[[UIImageView alloc]initWithFrame:CGRectMake(85, 84, 150, 150)];
    //            zipimage.image= imgqr;  //[UIImage imageNamed:@"exqr.png"];
    //            zipimage.clipsToBounds=YES;
    //            [mainview addSubview:zipimage];
    //            zipimage.userInteractionEnabled=YES;
    //
    //            [del showTabValues:YES];
    //        });
    //    });
    
    UIButton *makepicbt = [UIButton buttonWithType:UIButtonTypeCustom];
    makepicbt.frame = CGRectMake(31, 310, 120, 27.5f);
    makepicbt.layer.cornerRadius=5;
    [makepicbt setBackgroundImage:[UIImage imageNamed:@"personal-scan.png"] forState:UIControlStateNormal];
    makepicbt.backgroundColor=[UIColor clearColor];
    [makepicbt addTarget:self action:@selector(friend_scan:) forControlEvents:UIControlEventTouchUpInside];
    [mainview addSubview:makepicbt];
    
    UIButton *gallerybt = [UIButton buttonWithType:UIButtonTypeCustom];
    gallerybt.frame = CGRectMake(169, 310, 120, 27.5f);
    gallerybt.layer.cornerRadius=5;
    [gallerybt setBackgroundImage:[UIImage imageNamed:@"briffcase.png"] forState:UIControlStateNormal];
    gallerybt.backgroundColor=[UIColor clearColor];
    [gallerybt addTarget:self action:@selector(business_scan:) forControlEvents:UIControlEventTouchUpInside];
    [mainview addSubview:gallerybt];
    
    [del showTabValues:YES];
    
    NSData* myEncodedImageData = [[NSUserDefaults standardUserDefaults]objectForKey:@"qrcode"];
    // DebugLog(@"myencodedimagedata: %@",myEncodedImageData);
    
    //       UIImage *image = [UIImage imageWithData:myEncodedImageData];
    
    if (myEncodedImageData != nil)
    {
        DebugLog(@"data not nil");
        
        imgqr = [[UIImage alloc]initWithData: myEncodedImageData];
        
        UIImageView *zipimage=[[UIImageView alloc]initWithFrame:CGRectMake(85, 84, 150, 150)];
        zipimage.image= imgqr;  //[UIImage imageNamed:@"exqr.png"];
        zipimage.clipsToBounds=YES;
        [mainview addSubview:zipimage];
        zipimage.userInteractionEnabled=YES;
        
    }
    else
    {
        DebugLog(@"data is nil");
        
        //    [SVProgressHUD showWithStatus:@"Loading"];
        UIActivityIndicatorView *act = [[UIActivityIndicatorView alloc] init];
        act.center = self.view.center;
        [act startAnimating];
        act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        [act setColor:[UIColor blackColor]];
        [mainview addSubview:act];
        
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSString *urlString_qr =[NSString stringWithFormat:@"https://budnav.com/ext/?action=qr&id=%@&access_token=%@&device_id=%@",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
            
            NSString *newString_qr = [urlString_qr stringByReplacingOccurrencesOfString:@" " withString:@""];
            DebugLog(@"url: %@",newString_qr);
            NSData *signeddataURL_qr =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString_qr]];
            
            if (signeddataURL_qr != nil)
            {
                // DebugLog(@"json returns: %@",signeddataURL_qr);
                imgqr = [[UIImage alloc]initWithData: signeddataURL_qr];
                
                //  NSData* imageData = UIImagePNGRepresentation(imgqr);
                
                [[NSUserDefaults standardUserDefaults]setObject:signeddataURL_qr forKey:@"qrcode"];
                
                
                
            }
            else
            {
                alert = [[UIAlertView alloc] initWithTitle:@"Error in Retrieving QR Code Image!"
                                                   message:nil
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                //       [alert show];
                //        [SVProgressHUD dismiss];
                [act removeFromSuperview];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //            [SVProgressHUD dismiss];
                [act removeFromSuperview];
                
                
                UIImageView *zipimage=[[UIImageView alloc]initWithFrame:CGRectMake(85, 84, 150, 150)];
                zipimage.image= imgqr;  //[UIImage imageNamed:@"exqr.png"];
                zipimage.clipsToBounds=YES;
                [mainview addSubview:zipimage];
                zipimage.userInteractionEnabled=YES;
                
                [del showTabValues:YES];
            });
        });
        
    }
}

+(void)chngpostion
{
    DebugLog(@"Change pos requests page");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Data" object:Nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getData:) name:@"Data" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(navtopage:) name:@"pagenav" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestreceivedaction) name:@"Requestreceived_push" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(acceptedrequestaction) name:@"Accepted_push" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AcceptedRequest) name:@"Accepted_request" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AddFriend) name:@"NewConnection" object:nil];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AcceptedRequest) name:@"TypeChange" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shareLocation) name:@"ShareLocation" object:nil];
    
}

-(void)shareLocation
{
    ConLocateGroupViewController *conLocate = [[ConLocateGroupViewController alloc]init];
    conLocate.group_id=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"sharedLocGrpId"]];
    [self.navigationController pushViewController:conLocate animated:YES];
}

-(void)AddFriend
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDel.newUser)
    {

    ConAddFriendViewController *con = [[ConAddFriendViewController alloc]init];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
        
        appDel.newUser = NO;
    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    [[self navigationController] pushViewController:con animated:YES];
        
    }
    
}

-(void)AcceptedRequest
{
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (del.profileRedirect)
    {
    
    ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
    // con.other=@"yes";
    con.request = YES;
    [self.tabBarController.tabBar setHidden:YES];
    // con.uid=[NSString stringWithFormat:@"%d",[userid intValue]];
    //                [self.navigationController presentViewController:con animated:NO completion:nil];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
        
        del.profileRedirect = NO;
    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
        
    }
    
}

-(void)requestreceivedaction
{
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
    con.other=@"yes";
    con.uid=[NSString stringWithFormat:@"%@",del.pushtoprofileid];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}

-(void)acceptedrequestaction
{
    ConRequestViewController *con = [[ConRequestViewController alloc]init];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    
    [self.navigationController pushViewController:con animated:YES];
}


-(void)navtopage: (NSNotification *)notification
{
    DebugLog(@"navtopage");
    NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
    if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"4"])
    {
        ConAccountSettingsViewController *con = [[ConAccountSettingsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"6"])
    {
        ConInviteViewController *con = [[ConInviteViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"5"])
    {
        ConSyncLoaderViewController *con = [[ConSyncLoaderViewController alloc]init];
        //        [self.navigationController pushViewController:con animated:NO];
        
        [self presentViewController:con animated:NO completion:nil];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"2"])
    {
        ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"3"])
    {
        ConQROptionsViewController *con = [[ConQROptionsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    
    
    [mainview setFrame:CGRectMake(0, mainview.frame.origin.y, mainview.frame.size.width, mainview.frame.size.height)];
    //    [prefs setObject:@"111" forKey:@"whichpage"];
}

-(void)getData:(NSNotification *)notification {
    
    if(move == 0) {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake(280, mainview.frame.origin.y, mainview.frame.size.width, mainview.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             move=1;
                         }];
    } else {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake(0, (mainview.frame.origin.y),mainview.frame.size.width, mainview.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             move=0;
                         }];
    }
}

-(void)friend_scan: (id)sender
{
    SCViewController *scv = [[SCViewController alloc]init];
    scv.request_type=@"friend";
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:scv animated:YES];
}

-(void)business_scan: (id)sender
{
    SCViewController *scv = [[SCViewController alloc]init];
    scv.request_type=@"business";
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:scv animated:YES];
}

-(void)viewDidDisappear:(BOOL)animated
{
    move=0;
    [mainview removeFromSuperview];
    [super viewDidDisappear:YES];
}
@end