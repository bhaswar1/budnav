//
//  ConRequestViewController.h
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 08/05/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConParentTopBarViewController.h"
#import "GAITrackedViewController.h"

@interface ConRequestViewController : GAITrackedViewController<UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate>
{
    NSDictionary *json1;
    UIAlertView *alert;
    NSMutableArray *requestslist,*groupadminlist,*grouplist;
    UITableView *tab_request;
}
@property (nonatomic, strong) UIView *mainview;
+(void)chngpostion;
@end
