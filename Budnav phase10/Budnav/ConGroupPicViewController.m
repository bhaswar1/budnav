//
//  ConGroupPicViewController.m
//  Contacter
//
//  Created by ios on 19/01/15.
//  Copyright (c) 2015 Esolz. All rights reserved.
//

#import "ConGroupPicViewController.h"
#import "ConGroupSettingsViewController.h"
#import "AppDelegate.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

@interface ConGroupPicViewController ()
{
    UIView *tapbgview,*mainview;
    UIImageView *detailimg,*selectedimg;
    NSTimer *timerpic;
    NSString *version,*groups_url;
    BOOL isAtLeast7,isAtLeast8;
    UIActionSheet *actionsheet;
    NSDictionary *groupdict;
    NSMutableData *responseData;
    NSString *err_str;
}

@end

@implementation ConGroupPicViewController
@synthesize grouppic,groupid,adminname;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tabBarController.tabBar.hidden=YES;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    version = [[UIDevice currentDevice] systemVersion];
    DebugLog(@"version: %@",version);
    isAtLeast7 = [version floatValue] < 8.0;
    isAtLeast8 = [version floatValue] >= 8.0;
    
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    
    mainview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    mainview.backgroundColor=[UIColor clearColor];
    [self.view addSubview:mainview];
    
    
    tapbgview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, mainview.frame.size.height)];//[UIScreen mainScreen].bounds.size.height)];
    
    [mainview addSubview:tapbgview];
    
    tapbgview.backgroundColor=[UIColor blackColor];
    
    tapbgview.alpha=0.95f;
    
    UITapGestureRecognizer *tapback = [[UITapGestureRecognizer alloc]
                                       
                                       initWithTarget:self
                                       
                                       action:@selector(closedetailpic:)];
    
    tapbgview.userInteractionEnabled=YES;
    
    [tapbgview addGestureRecognizer:tapback];
    
    
    
    detailimg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 50, tapbgview.frame.size.width, tapbgview.frame.size.height-100)];
    DebugLog(@"Frame: %@",NSStringFromCGSize(grouppic.size));
    
    
    [self.view addSubview:detailimg];
    
    //detailimg.hidden=YES;
    
    detailimg.image = grouppic;
    detailimg.contentMode=UIViewContentModeScaleAspectFit;
    
    //detailimg.contentMode=UIViewContentModeScaleAspectFit;
    
    detailimg.userInteractionEnabled=YES;
    
    
    UITapGestureRecognizer *tapimg1 = [[UITapGestureRecognizer alloc]
                                       
                                       initWithTarget:self
                                       
                                       action:@selector(closedetailpic:)];
    
    detailimg.userInteractionEnabled=YES;
    
    [detailimg addGestureRecognizer:tapimg1];
    
    
    CATransition *animation = [CATransition animation];
    
    [animation setType:kCATransitionReveal];
    
    [animation setSubtype:kCATransitionReveal];
    
    animation.duration = 1.5f;
    
    [detailimg.layer addAnimation:animation forKey:nil];
    
    
}


-(void)closedetailpic: (UIGestureRecognizer *)sender
{
    
    CATransition *animation = [CATransition animation];
    
    [animation setType:kCATransitionFade];
    
    [animation setSubtype:kCATransitionFade];
    
    animation.duration = 0.25f;
    
    [detailimg.layer addAnimation:animation forKey:nil];
    
    if(detailimg.hidden==NO)
    {
        detailimg.hidden=YES;
        //        [self dismissViewControllerAnimated:NO completion:nil];
        timerpic = [NSTimer scheduledTimerWithTimeInterval:0.3f
                                                    target:self
                                                  selector:@selector(targetMethoda)
                                                  userInfo:nil
                                                   repeats:NO];
    }
    
}

-(void)targetMethoda
{
        [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
