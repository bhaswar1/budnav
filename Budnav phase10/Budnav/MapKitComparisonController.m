#import "MapKitComparisonController.h"

// We need a custom subclass of MKMapView in order to allow touches on UIControls in our custom callout view.
#import "ConInviteViewController.h"
#import "REVClusterAnnotationView.h"
#import "ConMapViewController.h"
#import "AppDelegate.h"
#import "ConProfileOwnViewController.h"
#import "ConAccountSettingsViewController.h"
#import "REVClusterMap.h"
#import "iToast.h"
#import "SVProgressHUD.h"
#import "DBManager.h"
#import "ConSyncLoaderViewController.h"
#import "CalloutView.h"
#import "myAnnotation.h"
#import "ConQROptionsViewController.h"
#import "ConNewProfileViewController.h"
//#import "TouchView.h"
@interface CustomMapView : REVClusterMapView
{
}

@property (nonatomic, strong) SMCalloutView *calloutView;
@end

@interface MapKitComparisonController ()<MKMapViewDelegate,CLLocationManagerDelegate>
{
    MKMapView *map_View;
    int userid,move,map_insertion, map_fetch;
    NSMutableArray *loadagainarr;
    UIActivityIndicatorView *act;
    CalloutView *calloutView1;
    UIView *calloutView11;
    //    TouchView* touchView;
}
@end
@implementation MapKitComparisonController
//@interface ConMapViewController ()<MKMapViewDelegate,CLLocationManagerDelegate>
//@end

//@implementation ConMapViewController
@synthesize getmapvalue,mainview;
//static int count=0;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    self.navigationController.navigationBarHidden=YES;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    //    move=0;
    //    map_insertion=0;
    //    map_fetch=0;
    
    
    loadagainarr = [[NSMutableArray alloc]init];
    
    newarr1 = [[NSMutableArray alloc]init];
    
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    self.navigationController.navigationBarHidden=YES;
    
    mainview = [[UIView alloc]initWithFrame:CGRectMake(0, 65, [UIScreen mainScreen].bounds.size.width, self.view.frame.size.height-60)];
    [self.view addSubview:mainview];
    mainview.backgroundColor=[UIColor whiteColor];
    
    act = [[UIActivityIndicatorView alloc] init];
    act.center = self.view.center;
    [act startAnimating];
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [act setColor:[UIColor blackColor]];
    [mainview addSubview:act];
    
    //    [SVProgressHUD showWithStatus:@"Loading"];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    //    NSError *error =nil;
    CLLocation *location = map_View.userLocation.location;
    DebugLog(@"get dir lat current: %f - long current: %f", location.coordinate.latitude, location.coordinate.longitude);
    _mapView =[[REVClusterMapView alloc] initWithFrame:CGRectMake(0, 0,[[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height-60)];
    //    [map_View setDelegate:self];
    //    map_View.showsUserLocation=YES;
    //    [mainview addSubview:map_View];
    
    _mapView.delegate = self;
    
    [mainview addSubview:_mapView];
    
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = 51.22;
    coordinate.longitude = 4.39625;
    //    _mapView.region = MKCoordinateRegionMakeWithDistance(coordinate, 5000, 5000);
    
    _mapView.showsUserLocation=YES;
    
    CLLocationManager *locManager = [[CLLocationManager alloc] init];
    [locManager setDelegate:self];
    [locManager setDesiredAccuracy:kCLLocationAccuracyBest];
    //    [locManager startUpdatingLocation];
    
    if ([[prefs objectForKey:@"firstMap"] isEqualToString:@"yes"])
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=map&id=%@&access_token=%@&device_id=%@",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
            
            DebugLog(@"profile url: %@",urlString1);
            NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
            NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
            
            if (signeddataURL1 == nil)
            {
                alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                                   message:nil
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                //                [alert show];
                [act removeFromSuperview];
            }
            else
            {
                NSError *error =nil;
                json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                         
                                                        options:kNilOptions
                         
                                                          error:&error];
                DebugLog(@"json returns: %@",json1);
                
                NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
                DebugLog(@"err  %@",errornumber);
                if (![errornumber isEqualToString:@"0"])
                {
                    DebugLog(@"if if");
                    NSString *err_str = [json1 objectForKey:@"error"];
                    alert = [[UIAlertView alloc] initWithTitle:@"Error in Retrieving Data!"
                                                       message:err_str
                                                      delegate:self
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                    [alert show];
                    //        [SVProgressHUD dismiss];
                    [act removeFromSuperview];
                }
                
                if ([[json1 objectForKey:@"details"] isKindOfClass:[NSNull class]] || [json1 objectForKey:@"details"] == (id)[NSNull null] ||[[json1 objectForKey:@"details"] count] ==0)
                {
                    DebugLog(@"if called req");
                    alert = [[UIAlertView alloc] initWithTitle:@"No Locations Found!"
                                                       message:nil
                                                      delegate:self
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                    [alert show];
                    //        [SVProgressHUD dismiss];
                    [act removeFromSuperview];
                }
                else
                {
                    NSDictionary *newdict= [json1 objectForKey:@"details"];
                    for (NSDictionary *reqdict in [newdict objectForKey:@"markers"])
                    {
                        [newarr1 addObject:reqdict];
                    }
                    DebugLog(@"map list array has: %@",newarr1);
                    
                }
                
                
                DebugLog(@"newarr ache: %@",newarr1);
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                int i=0;
                NSMutableArray *pins = [NSMutableArray array];
                while (i<[newarr1 count])
                {
                    double latitude=[[[newarr1 objectAtIndex:i]objectForKey:@"lat"] doubleValue];
                    double longitude=[[[newarr1 objectAtIndex:i]objectForKey:@"lng"] doubleValue];
                    
                    DebugLog(@"lati=== %f",latitude);
                    DebugLog(@"long=== %f",longitude);
                    
                    CLLocationCoordinate2D newCoord = {latitude, longitude};
                    REVClusterPin *pin = [[REVClusterPin alloc] init];
                    pin.title = [[newarr1 objectAtIndex:i]objectForKey:@"name"];
                    pin.subtitle = [[newarr1 objectAtIndex:i]objectForKey:@"surname"];
                    pin.coordinate = newCoord;
                    [pins addObject:pin];
                    
                    i++;
                }
                [_mapView addAnnotations:pins];
                //            [SVProgressHUD dismiss];
                [act removeFromSuperview];
                
                [self performSelectorOnMainThread:@selector(startDBInsert)
                                       withObject:nil
                                    waitUntilDone:YES];
                
                //            [[DBManager getSharedInstance]insertMap:newarr1];
                
            });
        });
        
        
        [prefs setObject:@"no" forKey:@"firstMap"];
    }
    
    else
    {
        DebugLog(@"1st time er por theke");
        NSArray *retval= [[NSArray alloc]init];
        //        Mymodel *obj=[Mymodel getInstance];
        //        if (obj.db_busy == NO)
        //        {
        //            map_fetch=1;
        retval = [[DBManager getSharedInstance]fetchMap];
        //        }
        
        newarr1 = [retval mutableCopy];
        //
        //        [_mapView removeFromSuperview];
        //        _mapView =[[REVClusterMapView alloc] initWithFrame:CGRectMake(0, 0,[[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height-60)];
        //
        //        _mapView.delegate = self;
        //
        //        [mainview addSubview:_mapView];
        
        CLLocationCoordinate2D coordinate;
        coordinate.latitude = 51.22;
        coordinate.longitude = 4.39625;
        _mapView.showsUserLocation=YES;
        
        
        //        self.mapKitWithSMCalloutView = [[CustomMapView alloc] initWithFrame:self.view.bounds];
        //        self.mapKitWithSMCalloutView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        //        self.mapKitWithSMCalloutView.delegate = self;
        [self.mapKitWithSMCalloutView addAnnotation:self.annotationForSMCalloutView];
        //        [self.view addSubview:self.mapKitWithSMCalloutView];
        
        int i=0;
        
        NSMutableArray *pins = [NSMutableArray array];
        while (i<[newarr1 count])
        {
            double latitude=[[[newarr1 objectAtIndex:i]objectForKey:@"lat"] doubleValue];
            double longitude=[[[newarr1 objectAtIndex:i]objectForKey:@"lng"] doubleValue];
            
            //            DebugLog(@"after first time from db lati=== %f",latitude);
            //            DebugLog(@"after first time from db long=== %f",longitude);
            
            CLLocationCoordinate2D newCoord = {latitude, longitude};
            REVClusterPin *pin = [[REVClusterPin alloc] init];
            pin.title = [[newarr1 objectAtIndex:i]objectForKey:@"name"];
            pin.subtitle = [[newarr1 objectAtIndex:i]objectForKey:@"surname"];
            pin.coordinate = newCoord;
            
            [pins addObject:pin];
            
            //            myAnnotation *annotation = [[myAnnotation alloc] initWithCoordinate:newCoord title:[[newarr1 objectAtIndex:i]objectForKey:@"name"]];
            //            [_mapView addAnnotation:annotation];
            
            self.annotationForSMCalloutView = [MKPointAnnotation new];
            self.annotationForSMCalloutView.coordinate = newCoord;
            self.annotationForSMCalloutView.title = [[newarr1 objectAtIndex:i]objectForKey:@"name"];
            self.annotationForSMCalloutView.subtitle =[[newarr1 objectAtIndex:i]objectForKey:@"surname"];
            
            i++;
        }
        
        [_mapView addAnnotations:pins];
        //        [SVProgressHUD dismiss];
        [act removeFromSuperview];
        
        self.calloutView = [SMCalloutView platformCalloutView];
        self.calloutView.delegate = self;
        
        // tell our custom map view about the callout so it can send it touches
        self.mapKitWithSMCalloutView.calloutView = self.calloutView;
        [self.view bringSubviewToFront:self.mapKitWithSMCalloutView];
        
        NSTimer *timerr;
        timerr = [NSTimer scheduledTimerWithTimeInterval:3.0
                                                  target:self
                                                selector:@selector(targetMethod:)
                                                userInfo:nil
                                                 repeats:NO];
    }
    //    [del showTabValues:YES];
}

-(void) targetMethod:(NSTimer *)timer
{
    [self loadmapfromweb];
}

//-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
////    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.location.coordinate, 1500, 1500);
////    [_mapView setRegion:region animated:YES];
//}

- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)aUserLocation {
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 8;
    span.longitudeDelta = 8;
    CLLocationCoordinate2D location;
    location.latitude = aUserLocation.coordinate.latitude;
    location.longitude = aUserLocation.coordinate.longitude;
    region.span = span;
    region.center = location;
    [aMapView setRegion:region animated:YES];
}

+(void)chngpostion
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Data" object:Nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getData:) name:@"Data" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(navtopage:) name:@"pagenav" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startDBInsertAgain) name:@"db_busy" object:nil];
    
}

-(void)navtopage: (NSNotification *)notification
{
    DebugLog(@"navtopage map");
    NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
    if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"4"])
    {
        ConAccountSettingsViewController *con = [[ConAccountSettingsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"6"])
    {
        ConInviteViewController *con = [[ConInviteViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"5"])
    {
        ConSyncLoaderViewController *con = [[ConSyncLoaderViewController alloc]init];
        //        [self.navigationController pushViewController:con animated:NO];
        
        [self presentViewController:con animated:NO completion:nil];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"2"])
    {
        ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"3"])
    {
        ConQROptionsViewController *con = [[ConQROptionsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    
    [mainview setFrame:CGRectMake(0, mainview.frame.origin.y, mainview.frame.size.width, mainview.frame.size.height)];
    //    [prefs setObject:@"111" forKey:@"whichpage"];
}

-(void)getData:(NSNotification *)notification {
    
    if(move == 0) {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake(280, mainview.frame.origin.y, mainview.frame.size.width, mainview.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             move = 1;
                         }];
    } else {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake(0, (mainview.frame.origin.y),mainview.frame.size.width, mainview.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             move = 0;
                         }];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getDirections
{
    CLLocationCoordinate2D start = {37.774929, -122.419416 };
    CLLocationCoordinate2D end = {40.714353, -74.005973};
    
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
    
    MKMapItem *source_mapitem =[[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:start addressDictionary:nil]];
    [source_mapitem setName:@"Source"];
    
    MKMapItem *destination_mapitem = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:end addressDictionary:nil]];
    
    [destination_mapitem setName:@"Destination"];
    request.source = source_mapitem;
    request.destination = destination_mapitem;
    
    request.requestsAlternateRoutes = YES;
    MKDirections *directions =
    [[MKDirections alloc] initWithRequest:request];
    
    [directions calculateDirectionsWithCompletionHandler:
     ^(MKDirectionsResponse *response, NSError *error) {
         if (error) {
             DebugLog(@" error is %@",error.localizedDescription);
         } else {
             DebugLog(@" here we came");
             [self showRoute:response];
         }
     }];
}

-(void)showRoute:(MKDirectionsResponse *)response
{
    for (MKRoute *route in response.routes)
    {
        [map_View addOverlay:route.polyline level:MKOverlayLevelAboveLabels];
        for (MKRouteStep *step in route.steps)
        {
            DebugLog(@" here %@", step.instructions);
        }
    }
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id < MKOverlay >)overlay
{
    MKPolylineRenderer *renderer =
    [[MKPolylineRenderer alloc] initWithOverlay:overlay];
    renderer.strokeColor = [UIColor blueColor];
    renderer.lineWidth = 2.0;
    return renderer;
}


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    MKAnnotationView *annView;
    
    if([annotation class] == MKUserLocation.class) {
        //userLocation = annotation;
        annView.canShowCallout = NO;
        return nil;
    }
    
    REVClusterPin *pin = (REVClusterPin *)annotation;
    
    
    if( [pin nodeCount] > 0 ){
        pin.title = @"___";
        
        annView = (REVClusterAnnotationView*)
        [mapView dequeueReusableAnnotationViewWithIdentifier:@"cluster"];
        
        if( !annView )
            annView = (REVClusterAnnotationView*)[[REVClusterAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"cluster"];
        
        annView.image = [UIImage imageNamed:@"locator.png"];
        
        [(REVClusterAnnotationView*)annView setClusterText:
         [NSString stringWithFormat:@"%lu",(unsigned long)[pin nodeCount]]];
        
        annView.canShowCallout = YES;
    } else {
        
        //        MKPinAnnotationView *view = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@""];
        
        annView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@""];
        //        annView.canShowCallout = YES;
        annView.calloutOffset = CGPointMake(-6.0, 0.0);
        
        self.calloutView.userInteractionEnabled=YES;
        [self.calloutView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewtapped:)]];
        
        // create a disclosure button for map kit
        UIButton *disclosure = [UIButton buttonWithType:UIButtonTypeCustom];
        [disclosure addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(disclosureTapped)]];
        annView.rightCalloutAccessoryView = disclosure;
        
        // if we're using SMCalloutView, we don't want MKMapView to create its own callout!
        //        if (
        annotation = self.annotationForSMCalloutView;
        annView.canShowCallout = NO;
        //        else
        //            annView.canShowCallout = YES;
        
    }
    return annView;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    DebugLog(@"REVMapViewController mapView didSelectAnnotationView: %d",userid);
    if ([view isKindOfClass:[REVClusterAnnotationView class]])
    {
        CLLocationCoordinate2D centerCoordinate = [(REVClusterPin *)view.annotation coordinate];
        MKCoordinateSpan newSpan =
        MKCoordinateSpanMake(mapView.region.span.latitudeDelta/4.0,
                             mapView.region.span.longitudeDelta/4.0);
        
        //mapView.region = MKCoordinateRegionMake(centerCoordinate, newSpan);
        [mapView setRegion:MKCoordinateRegionMake(centerCoordinate, newSpan)
                  animated:YES];
    }
    else
    {
        //        view.userInteractionEnabled=YES;
        //
        //         CGRect calloutViewFrame = calloutView.frame;
        //    calloutViewFrame.origin = CGPointMake(-calloutViewFrame.size.width/2 + 15, -calloutViewFrame.size.height);
        //    calloutView.frame = calloutViewFrame;
        //    [calloutView.calloutLabel setText:[NSString stringWithFormat:@"%@ %@",[[view annotation] title],[[view annotation] subtitle]]];
        //        [calloutView.calloutbutton setBackgroundColor:[UIColor clearColor]];
        //    [view addSubview:calloutView];
        //        [view bringSubviewToFront:calloutView];
        //        calloutView.tag= userid;
        ////        calloutView1.layer.zPosition=4;
        ////        calloutView.calloutbutton.tag=userid;
        ////        [calloutView.calloutbutton addTarget:self action:@selector(maptap:) forControlEvents:UIControlEventTouchUpInside];
        //
        
        // apply the MKAnnotationView's basic properties
        
        DebugLog(@"bvjdnciknl");
        
        self.calloutView.userInteractionEnabled=YES;
        self.calloutView.layer.zPosition= 3;
        self.calloutView.title = view.annotation.title;
        self.calloutView.subtitle = view.annotation.subtitle;
        
        // Apply the MKAnnotationView's desired calloutOffset (from the top-middle of the view)
        self.calloutView.calloutOffset = view.calloutOffset;
        self.calloutView.userInteractionEnabled=YES;
        [self.calloutView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewtapped:)]];
        
        // create a disclosure button for comparison
        UIButton *disclosure = [UIButton buttonWithType:UIButtonTypeInfoDark];
        [disclosure addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(disclosureTapped)]];
        self.calloutView.rightAccessoryView = disclosure;
        
        // iOS 7 only: Apply our view controller's edge insets to the allowable area in which the callout can be displayed.
        if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
            self.calloutView.constrainedInsets = UIEdgeInsetsMake(self.topLayoutGuide.length, 0, self.bottomLayoutGuide.length, 0);
        
        // This does all the magic.
        [self.calloutView presentCalloutFromRect:view.bounds inView:view constrainedToView:self.view animated:YES];
    }
}


//-(void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
//
//    for (UIView *subview in view.subviews ){
//        DebugLog(@"sub: %@",subview);
//        if ([view isKindOfClass:[REVClusterAnnotationView class]])
//        {
//        }
//        else
//        {
////            if ([subview isKindOfClass:[SMCalloutView class]])
////            {
////                //                DebugLog(@"subview: %@",subview);
////                //                ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
////                //                con.other=@"yes";
////                //                con.uid=[NSString stringWithFormat:@"%ld",(long)view.tag];
////                //                [self.navigationController pushViewController:con animated:NO];
////                DebugLog(@"calloutview");
////            }
////            [subview removeFromSuperview];
//
//            [self.calloutView dismissCalloutAnimated:YES];
//        }
//    }
//}


-(void)maptap1: (UIGestureRecognizer *)sender
{
    DebugLog(@"bhaswar yes: %ld",(long)sender.view.tag);
    ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
    con.other=@"yes";
    con.uid=[NSString stringWithFormat:@"%ld",(long)sender.view.tag];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
    
}



//- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
//{
//    DebugLog(@"Tapped on %@ & Tapped user id: %ld", view.annotation.subtitle, (long)view.tag);
//    //        NSString *IdTag =view.annotation.subtitle;
//    ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
//    con.other=@"yes";
//    con.uid=[NSString stringWithFormat:@"%ld",(long)view.tag];
//    [self.navigationController pushViewController:con animated:NO];
//}

-(void)viewDidDisappear:(BOOL)animated
{
    move=0;
    [mainview removeFromSuperview];
    [super viewDidDisappear:YES];
}

-(void)loadmapfromweb
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        DebugLog(@"hmmmmmm again load map");
        NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=map&id=%@&access_token=%@&device_id=%@",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
        
        DebugLog(@"profile url: %@",urlString1);
        
        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
        
        if (signeddataURL1 == nil)
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            //            [alert show];
            //            [SVProgressHUD dismiss];
            [act removeFromSuperview];
        }
        else
        {
            NSError *error =nil;
            json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                     
                                                    options:kNilOptions
                     
                                                      error:&error];
            //            DebugLog(@"json returns: %@",json1);
            
            NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
            DebugLog(@"err  %@",errornumber);
            if (![errornumber isEqualToString:@"0"])
            {
                DebugLog(@"if if");
                //                NSString *err_str = [json1 objectForKey:@"error"];
                //                alert = [[UIAlertView alloc] initWithTitle:@"Error in Retrieving Data!"
                //                                                   message:err_str
                //                                                  delegate:self
                //                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                //                [alert show];
                //                [SVProgressHUD dismiss];
                [act removeFromSuperview];
            }
            
            if ([[json1 objectForKey:@"details"] isKindOfClass:[NSNull class]] || [json1 objectForKey:@"details"] == (id)[NSNull null] ||[[json1 objectForKey:@"details"] count] ==0)
            {
                DebugLog(@"if called req");
                alert = [[UIAlertView alloc] initWithTitle:@"No Locations Found!"
                                                   message:nil
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                [alert show];
                //                [SVProgressHUD dismiss];
                [act removeFromSuperview];
            }
            else
            {
                NSDictionary *newdict= [json1 objectForKey:@"details"];
                for (NSDictionary *reqdict in [newdict objectForKey:@"markers"])
                {
                    [loadagainarr addObject:reqdict];
                }
                //                DebugLog(@"map list array has: %@",loadagainarr);
                
            }
            
            //            [[DBManager getSharedInstance]insertMap:loadagainarr];
            
            
            DebugLog(@"loadagainarr ache: %lu",(unsigned long)[loadagainarr count]);
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self loaddatabase_data];
            [self performSelectorOnMainThread:@selector(startDBInsertAgain)
                                   withObject:nil
                                waitUntilDone:YES];
            
        });
    });
}


-(void) loaddatabase_data
{
    DebugLog(@"1st time er por theke");
    NSArray *retval = [[DBManager getSharedInstance]fetchMap];
    newarr1 = [retval mutableCopy];
    
    //    [_mapView removeFromSuperview];
    //    _mapView =[[REVClusterMapView alloc] initWithFrame:CGRectMake(0, 0,[[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height-60)];
    //
    //    _mapView.delegate = self;
    //
    //    [mainview addSubview:_mapView];
    //
    //    CLLocationCoordinate2D coordinate;
    //    coordinate.latitude = 51.22;
    //    coordinate.longitude = 4.39625;
    //    _mapView.showsUserLocation=YES;
    
    int i=0;
    NSMutableArray *pins = [NSMutableArray array];
    while (i<[newarr1 count])
    {
        double latitude=[[[newarr1 objectAtIndex:i]objectForKey:@"lat"] doubleValue];
        double longitude=[[[newarr1 objectAtIndex:i]objectForKey:@"lng"] doubleValue];
        
        //        DebugLog(@"coming again from db lati=== %f",latitude);
        //        DebugLog(@"coming again from db long=== %f",longitude);
        
        CLLocationCoordinate2D newCoord = {latitude, longitude};
        REVClusterPin *pin = [[REVClusterPin alloc] init];
        pin.title = [[newarr1 objectAtIndex:i]objectForKey:@"name"];
        pin.subtitle = [[newarr1 objectAtIndex:i]objectForKey:@"surname"];
        pin.coordinate = newCoord;
        [pins addObject:pin];
        
        i++;
    }
    [_mapView addAnnotations:pins];
    //    [SVProgressHUD dismiss];
    [act removeFromSuperview];
}


-(void)startDBInsert
{
    if (map_insertion == 0)
    {
        map_insertion=1;
        DebugLog(@"start map insert");
        AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [del loadMapIntoDB:newarr1];
    }
}

-(void)startDBInsertAgain
{
    //    if (![[[NSUserDefaults standardUserDefaults]objectForKey:@"firstMap"] isEqualToString:@"yes"] && map_fetch == 0)
    //    {
    //        map_fetch=1;
    //        [self loaddatabase_data];
    //    }
    
    if (map_insertion == 0)
    {
        map_insertion=1;
        //    DebugLog(@"start db insert : %@",requestslist);
        AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [del loadMapIntoDB:loadagainarr];
    }
}









//- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
//
//    // create a proper annotation view, be lazy and don't use the reuse identifier
//    MKPinAnnotationView *view = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@""];
//
//    self.calloutView.userInteractionEnabled=YES;
//    [self.calloutView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewtapped:)]];
//
//    // create a disclosure button for map kit
//    UIButton *disclosure = [UIButton buttonWithType:UIButtonTypeCustom];
//    [disclosure addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(disclosureTapped)]];
//    view.rightCalloutAccessoryView = disclosure;
//
//    // if we're using SMCalloutView, we don't want MKMapView to create its own callout!
//    if (annotation == self.annotationForSMCalloutView)
//        view.canShowCallout = NO;
//    else
//        view.canShowCallout = YES;
//
//    return view;
//}
//
//- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)annotationView {
//
//    if (mapView == self.mapKitWithSMCalloutView) {
//
//        // apply the MKAnnotationView's basic properties
//        self.calloutView.title = annotationView.annotation.title;
//        self.calloutView.subtitle = annotationView.annotation.subtitle;
//
//        // Apply the MKAnnotationView's desired calloutOffset (from the top-middle of the view)
//        self.calloutView.calloutOffset = annotationView.calloutOffset;
//        self.calloutView.userInteractionEnabled=YES;
//        [self.calloutView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewtapped:)]];
//
//        // create a disclosure button for comparison
//        UIButton *disclosure = [UIButton buttonWithType:UIButtonTypeInfoDark];
//        [disclosure addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(disclosureTapped)]];
//        self.calloutView.rightAccessoryView = disclosure;
//
//        // iOS 7 only: Apply our view controller's edge insets to the allowable area in which the callout can be displayed.
//        if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
//            self.calloutView.constrainedInsets = UIEdgeInsetsMake(self.topLayoutGuide.length, 0, self.bottomLayoutGuide.length, 0);
//
//        // This does all the magic.
//        [self.calloutView presentCalloutFromRect:annotationView.bounds inView:annotationView constrainedToView:self.view animated:YES];
//    }
//}

-(void)viewtapped: (UIGestureRecognizer *)sender
{
    DebugLog(@"hihijiwjxi");
}

//- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
//
//    [self.calloutView dismissCalloutAnimated:YES];
//}

//
// SMCalloutView delegate methods
//
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    DebugLog(@"Tapped on %@ & Tapped user id: %ld", view.annotation.subtitle, (long)view.tag);
}

- (NSTimeInterval)calloutView:(SMCalloutView *)calloutView delayForRepositionWithSize:(CGSize)offset {
    
    // When the callout is being asked to present in a way where it or its target will be partially offscreen, it asks us
    // if we'd like to reposition our surface first so the callout is completely visible. Here we scroll the map into view,
    // but it takes some math because we have to deal in lon/lat instead of the given offset in pixels.
    
    CLLocationCoordinate2D coordinate = self.mapKitWithSMCalloutView.centerCoordinate;
    
    // where's the center coordinate in terms of our view?
    CGPoint center = [self.mapKitWithSMCalloutView convertCoordinate:coordinate toPointToView:self.view];
    
    // move it by the requested offset
    center.x -= offset.width;
    center.y -= offset.height;
    
    // and translate it back into map coordinates
    coordinate = [self.mapKitWithSMCalloutView convertPoint:center toCoordinateFromView:self.view];
    
    // move the map!
    [self.mapKitWithSMCalloutView setCenterCoordinate:coordinate animated:YES];
    
    // tell the callout to wait for a while while we scroll (we assume the scroll delay for MKMapView matches UIScrollView)
    return kSMCalloutViewRepositionDelayForUIScrollView;
}

- (void)disclosureTapped {
    alert = [[UIAlertView alloc] initWithTitle:@"Tap!" message:@"You tapped the disclosure button."
                                      delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
    [alert show];
}

@end

//
// Custom Map View
//
// We need to subclass MKMapView in order to present an SMCalloutView that contains interactive
// elements.
//

@interface CustomMapView (UIGestureRecognizer)

// this tells the compiler that MKMapView actually implements this method
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch;

@end

@implementation CustomMapView

// override UIGestureRecognizer's delegate method so we can prevent MKMapView's recognizer from firing
// when we interact with UIControl subclasses inside our callout view.
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isKindOfClass:[UIControl class]])
        return NO;
    else
        return [super gestureRecognizer:gestureRecognizer shouldReceiveTouch:touch];
}

// Allow touches to be sent to our calloutview.
// See this for some discussion of why we need to override this: https://github.com/nfarina/calloutview/pull/9
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    
    DebugLog(@"RNFKNRIN");
    UIView *calloutMaybe = [self.calloutView hitTest:[self.calloutView convertPoint:point fromView:self] withEvent:event];
    if (calloutMaybe)
    {
        DebugLog(@"jvbrv");
        return calloutMaybe;
    }
    
    return [super hitTest:point withEvent:event];
}


@end
