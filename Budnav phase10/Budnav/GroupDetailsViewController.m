//
//  GroupDetailsViewController.m
//  Contacter
//
//  Created by intel on 31/12/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

#import "GroupDetailsViewController.h"
#import "ConDetailsViewController.h"
#import "ConGroupSettingsViewController.h"
#import "ConGroupPicViewController.h"
#import "ConNewDetailsViewController.h"
#import "iToast.h"
#import "AppDelegate.h"
#import "SVProgressHUD.h"
#import "GroupInfoMemberViewController.h"
#import "GroupInfoAdminViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"
#import "ConLocateGroupViewController.h"



@interface GroupDetailsViewController ()<UIGestureRecognizerDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>
{
    NSIndexPath *scrollToPath;
    UIView *mainview,*blackBack,*searchview,*popupview,*grayView,*change;
    //UISearchBar *searchBar1;
    UITextField *searchfield;
    NSOperationQueue *groupsarray,*navigationqueue;
    NSDictionary *groupdict,*temp,*grpdict,*groupdictionary;
    NSDictionary *groupnames,*searchnames,*array;
    NSMutableArray *groupmember_array,*array_copy,*arraymenu_admin,*arrayadmin_imagename,*array_imagename,*array_menu,*array_id,*hld;
    UIButton *backbutton,*crossbutton,*bigbackbutton;
    //NSArray *group_array;
    UILabel *nouserlb,*nouserlabel,*membercount;
    UIImageView *crossimage,*backimage,*moreImage;
    BOOL expand;
    UITextField *txtSearchField;
    NSCharacterSet *whitespace;
    UILabel *back_label;
    UIActivityIndicatorView *act, *activity;
    BOOL isAtLeast7,isAtLeast8;
    NSString *name,*version,*base64String,*base64String1;
    int h, rem_row, ind,ind1;
    NSInteger row,row1,section;
    NSData *decodedData,*decodedData1,*data1;
    UILabel *lineDiv, *lbdivider;
    NSString *check1;
    UIRefreshControl *refreshcontrolnew;
    NSMutableAttributedString *refreshString;
    AppDelegate *del;
    ConNewDetailsViewController *condetails;
    MFMailComposeViewController *mailComposer;
    UIAlertView *alert;
    NSMutableArray *mailarr,*marr,*messagearr;
    //    ConDetailsViewController *condetails;
}

@end

@implementation GroupDetailsViewController
@synthesize groupid,groupname,menu,st,search_name,isadmin,finalsearch_result,filter,filtered_arr,search_result,groupstable,menutable,searchtableback,searchtablefront,group_namelbl,grouppic,searchBar,check_search,check_member,p,invite,groupimage,member_count,searchresults,group_array,searchBar1,accept_ckeck,group_namebtn;

//+ (UIImage *)imageWithColor:(UIColor *)color
//{
//    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
//    UIGraphicsBeginImageContext(rect.size);
//    CGContextRef context = UIGraphicsGetCurrentContext();
//
//    CGContextSetFillColorWithColor(context, [color CGColor]);
//    CGContextFillRect(context, rect);
//
//    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//
//    return image;
//}



- (BOOL)hidesBottomBarWhenPushed {
    return YES;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    DebugLog(@"GROUP NAME:%@",groupname);
    
    NSLog(@"The Desired Array...%@",self.copydic);
    
    if ([groupid isKindOfClass:[NSString class]])
    {
        if (![groupid isKindOfClass:[NSNull class]] && ![groupid isEqualToString:@"(null)"] && ![groupid isEqualToString:@""] &&groupid!=nil)
        {
            [[NSUserDefaults standardUserDefaults] setObject:groupid forKey:@"group_id"];
        }
    }
    else
    {
        if ([groupid intValue]>0)
        {
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",groupid] forKey:@"group_id"];
        }
    }
    
    
    
    NSLog(@"From GroupDetailsViewController");
    //[self loaddata];
    
    h=1;
    p=0;
    ind = 0;
    ind1 = 0;
    
    del.tabBarController.tabBar.frame = CGRectMake(0, [[UIScreen mainScreen]bounds].size.height, [UIScreen mainScreen].bounds.size.width,44);
    self.tabBarController.tabBar.translucent = YES;
    
    //    self.tabBarController.tabBar.frame = CGRectMake(0, [[UIScreen mainScreen]bounds].size.height-54, 0, 0);
    //    self.tabBarController.tabBar.hidden = YES;
    //self.tabBarController.tabBar.frame=CGRectMake(0, 0, 0, 0);
    //act.layer.zPosition=2;
    if (search_result==nil) {
        search_result =[[NSMutableArray alloc]init];
    }
    array_id=[[NSMutableArray alloc]init];
    if (![check_search isEqualToString:@"YES"]) {
        group_array =[[NSMutableArray alloc]init];
    }
    
    
    del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    //=====================Search bar black view with add groups button=======================//
    
    blackBack = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 380, 64)];
    //[blackBack setBackgroundColor:[UIColor blackColor]];
    [self.view addSubview:blackBack];
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
    
    // Add the gradient to the view
    [blackBack.layer insertSublayer:gradient atIndex:0];
    
    
    if (![check_search isEqualToString:@"YES"]) {
        searchresults=[[NSMutableArray alloc]init];
    }
    
    //    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(25, 20, 265, 51)];
    //    searchBar.delegate=self;
    //    searchBar.tintColor=[UIColor whiteColor];
    //    searchBar.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:1.0f];
    //    searchBar.barTintColor= [UIColor colorWithRed:0 green:0 blue:0 alpha:1.0f];
    //    searchBar.placeholder= @"Search in group";
    //    [blackBack addSubview:searchBar];
    //    searchBar.hidden=NO;
    
    change = [[UIView alloc]initWithFrame:CGRectMake(0,0,70,64)];
    [change setBackgroundColor:[UIColor clearColor]];
    [blackBack addSubview:change];
    
    
    backimage=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"back3"]];
    //    backimage.frame=CGRectMake(8, 15, 10, 20);
    backimage.frame=CGRectMake(15, 32, 12, 20);
    [change addSubview:backimage];
    backimage.userInteractionEnabled=YES;
    
    groupimage=[[UIImageView alloc]initWithFrame:CGRectMake(32, 9, 32, 31)];
    groupimage.image=[UIImage imageNamed:@"groups_normal@2x.png"];
    groupimage.layer.cornerRadius=2;
    groupimage.layer.masksToBounds=YES;
    groupimage.contentMode= UIViewContentModeScaleAspectFit;
    //groupimage.image=grouppic;
    //groupimage.backgroundColor=[UIColor redColor];
    //[change addSubview:groupimage];
    groupimage.userInteractionEnabled=YES;
    
    
    backbutton = [UIButton buttonWithType:UIButtonTypeCustom];
    //backbutton.frame = CGRectMake(0, 0, groupimage.frame.origin.x-1, 64);
    backbutton.frame = CGRectMake(0, 0,70, 64);
    backbutton.backgroundColor=[UIColor clearColor];
    [backbutton setTitle:@"" forState:UIControlStateNormal];
    //[backbutton setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1.0]] forState:UIControlStateHighlighted];
    [backbutton addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDown];
    [backbutton addTarget:self action:@selector(changecoloragain) forControlEvents:UIControlEventTouchDragExit];
    [backbutton addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDragEnter];
    [backbutton addTarget:self action:@selector(goback:) forControlEvents:UIControlEventTouchUpInside];
    backbutton.tag=0;
    [change addSubview:backbutton];
    
    //[backbutton setBackgroundImage:[UIImage imageNamed:@"Back button.png"] forState:UIControlStateNormal];
    //    UIImage *img=ACUTilingImageRGB(235.0/255.0,235.0/255.0,235.0/255.0,1.0);
    //    [backbutton setBackgroundImage:img forState:UIControlStateHighlighted];
    //[backbutton setBackgroundImage:[self im] forState:<#(UIControlState)#>];
    
    
    //UITapGestureRecognizer *propictap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(detailpic:)];
    
    // [groupimage addGestureRecognizer:propictap];
    
    //    NSDictionary *attributes= @{NSFontAttributeName: [UIFont fontWithName:@"ProximaNova-Regular" size:18.0]};
    //    CGSize textSize = [text sizeWithAttributes:@{ NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Light" size:16.0] }];
    //
    // Values are fractional -- you should take the ceilf to get equivalent values
    //    CGSize adjustedSize = CGSizeMake(ceilf(size.width), ceilf(size.height));
    //    CGRect rect = [groupname boundingRectWithSize:CGSizeMake(ceilf(size.width), ceilf(size.height)) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    
    
    group_namelbl=[[UILabel alloc]initWithFrame:CGRectMake(70, 20, self.view.frame.size.width-140, 30)];
    group_namelbl.text=groupname;
    group_namelbl.textAlignment=NSTextAlignmentCenter;
    group_namelbl.contentMode=UIViewContentModeBottom;
    group_namelbl.font=[UIFont fontWithName:@"ProximaNova-Bold" size:18.0];
    group_namelbl.textColor=[UIColor whiteColor];
    group_namelbl.backgroundColor=[UIColor clearColor];
    [group_namelbl setUserInteractionEnabled:YES];
    [blackBack addSubview:group_namelbl];
    
    
    membercount=[[UILabel alloc] initWithFrame:CGRectMake(60, 42, self.view.frame.size.width-120, 20)];
    membercount.contentMode=UIViewContentModeTop;
    membercount.textAlignment=NSTextAlignmentCenter;
    [membercount setUserInteractionEnabled:YES];
    // membercount.backgroundColor=[UIColor grayColor];
    if ([member_count isEqualToString:@"1"]) {
        
        membercount.text=@"1 Member";
    }
    else
    {
        if (!accept_ckeck) {
            membercount.text=[NSString stringWithFormat:@"%@ Members",member_count];
            
        }
    }
    
    membercount.font=[UIFont fontWithName:@"ProximaNova-Regular" size:14.0];
    membercount.textColor=[UIColor colorWithRed:204.0f/255.0f green:204.0f/255.0f blue:204.0f/255.0f alpha:1.0f];
    [blackBack addSubview:membercount];
    
    
    group_namebtn=[[UIButton alloc]initWithFrame:CGRectMake(70, 0, self.view.frame.size.width-140, 64)];
    [group_namebtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    group_namebtn.backgroundColor=[UIColor clearColor];
    [blackBack addSubview:group_namebtn];
    
    
    
    //    UITapGestureRecognizer *oneFingerTwoTaps =
    //    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(oneFingerTwoTaps:)];
    //    oneFingerTwoTaps.delegate=self;
    //
    //    // Set required taps and number of touches
    //    [oneFingerTwoTaps setNumberOfTapsRequired:1];
    //    [oneFingerTwoTaps setNumberOfTouchesRequired:1];
    //
    //    // Add the gesture to the view
    //    [group_namelbl addGestureRecognizer:oneFingerTwoTaps];
    
    [group_namebtn addTarget:self action:@selector(btnchangecolor) forControlEvents:UIControlEventTouchDown];
    [group_namebtn addTarget:self action:@selector(btnchangecoloragain) forControlEvents:UIControlEventTouchDragExit];
    [group_namebtn addTarget:self action:@selector(btnchangecolor) forControlEvents:UIControlEventTouchDragEnter];
    [group_namebtn addTarget:self action:@selector(MoveToNextPage) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    //    moreImage=[[UIImageView alloc]initWithFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-[UIImage imageNamed:@"menu-icon@2x.png"].size.width-10, 25, [UIImage imageNamed:@"menu-icon@2x.png"].size.width,[UIImage imageNamed:@"menu-icon@2x.png"].size.height)];
    
    moreImage=[[UIImageView alloc]initWithFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-37, 32, 22,19)];
    
    
    [moreImage setImage:[UIImage imageNamed:@"new_menu"]];
    [blackBack addSubview:moreImage];
    
    menu=[[UIButton alloc]initWithFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-55,[[UIApplication sharedApplication]statusBarFrame].size.height, 55,blackBack.frame.size.height-[[UIApplication sharedApplication]statusBarFrame].size.height)];//CGRectMake(290, 32, 22, 28)];
    [menu setBackgroundImage:nil forState:UIControlStateDisabled];
    [menu setBackgroundImage:nil forState:UIControlStateSelected];
    [menu setBackgroundImage:nil forState:UIControlStateHighlighted];
    [menu addTarget:self action:@selector(gomenu:) forControlEvents:UIControlEventTouchUpInside];
    menu.tag=9;
    menu.contentMode=UIViewContentModeScaleAspectFill;
    expand=NO;
    menu.enabled=NO;
    [blackBack addSubview:menu];
    
    
    
    //    lineDiv = [[UILabel alloc]initWithFrame:CGRectMake(274.0f, 30, 1.0f, 37)];
    //    lineDiv.backgroundColor = [UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0f];
    //    [blackBack addSubview:lineDiv];
    
    //    menu=[[UIButton alloc]initWithFrame:CGRectMake(280 ,30, [UIImage imageNamed:@"menu-icon@2x.png"].size.width,[UIImage imageNamed:@"menu-icon@2x.png"].size.height)];//CGRectMake(290, 32, 22, 28)];
    //    //    rightMenu.frame = CGRectMake(lineDiv.frame.origin.x+2 , 28, [UIImage imageNamed:@"menu_profile"].size.width,[UIImage imageNamed:@"menu_profile"].size.height);// 12.0f/3.0f, 64.0f/3.0f);
    //    //    [rightMenu setBackgroundImage:[UIImage imageNamed:@"menu_profile"] forState:UIControlStateNormal];
    //
    //    [menu setBackgroundImage:[UIImage imageNamed:@"menu-icon@2x.png"] forState:UIControlStateDisabled];
    //    //[menu setBackgroundImage:[UIImage imageNamed:@"menu_profile"] forState:UIControlStateNormal];
    //    [menu setBackgroundImage:[UIImage imageNamed:@"menu-icon@2x.png"] forState:UIControlStateSelected];
    //    [menu setBackgroundImage:[UIImage imageNamed:@"menu-icon@2x.png"] forState:UIControlStateHighlighted];
    //    [menu addTarget:self action:@selector(gomenu:) forControlEvents:UIControlEventTouchUpInside];
    //    menu.tag=9;
    //    menu.contentMode=UIViewContentModeScaleAspectFill;
    //    expand=NO;
    //    menu.enabled=NO;
    ////    menu.adjustsImageWhenDisabled=YES;
    //    [blackBack addSubview:menu];
    
    
    
    
    // Do any additional setup after loading the view.
    
    NSLog(@"isAdmin isAdmin: %@", isadmin);
}

-(void)MoveToNextPage
{
    NSLog(@"okkk......%@",searchnames);
    
    int type= [[_copydic objectForKey:@"isAdmin"]intValue];
    
    if(type==0)
    {
        
        DebugLog(@"Details :%@",_copydic);
        
        GroupInfoMemberViewController *next=[[GroupInfoMemberViewController alloc]init];
        next.mydic=[self.copydic mutableCopy];
        next.grpdict=[groupdict copy];
        next.mydata2=self.mydata;
        //        CATransition* transition = [CATransition animation];
        //
        //        transition.duration = 0.4;
        //        transition.type = kCATransitionPush;
        //        transition.subtype = kCATransitionFade;
        //
        //        [[self navigationController].view.layer addAnimation:transition forKey:kCATransition];
        
        [self.navigationController pushViewController:next animated:YES];
    }
    
    else
    {
        GroupInfoAdminViewController *next2=[[GroupInfoAdminViewController alloc]init];
        next2.mydic=[self.copydic mutableCopy];
        next2.grpdict=[groupdict copy];
        next2.mydata2=self.mydata;
        //        CATransition* transition = [CATransition animation];
        //
        //        transition.duration = 0.4;
        //        transition.type = kCATransitionPush;
        //        transition.subtype = kCATransitionFade;
        //
        //        [[self navigationController].view.layer addAnimation:transition forKey:kCATransition];
        
        [self.navigationController pushViewController:next2 animated:YES];
    }
}

-(void)oneFingerTwoTaps:(UITapGestureRecognizer *)recognizer
{
    [self changecolor];
    [self changecoloragain];
    [self changecolor];
    
    NSLog(@"okkk......%@",searchnames);
    
    int type= [[_copydic objectForKey:@"isAdmin"]intValue];
    
    if(type==0)
    {
        
        DebugLog(@"Details :%@",_copydic);
        
        GroupInfoMemberViewController *next=[[GroupInfoMemberViewController alloc]init];
        next.mydic=[self.copydic mutableCopy];
        next.grpdict=[groupdict copy];
        next.mydata2=self.mydata;
        //        CATransition* transition = [CATransition animation];
        //
        //        transition.duration = 0.4;
        //        transition.type = kCATransitionPush;
        //        transition.subtype = kCATransitionFade;
        //
        //        [[self navigationController].view.layer addAnimation:transition forKey:kCATransition];
        
        [self.navigationController pushViewController:next animated:YES];
    }
    
    else
    {
        GroupInfoAdminViewController *next2=[[GroupInfoAdminViewController alloc]init];
        next2.mydic=[self.copydic mutableCopy];
        next2.grpdict=[groupdict copy];
        next2.mydata2=self.mydata;
        //        CATransition* transition = [CATransition animation];
        //
        //        transition.duration = 0.4;
        //        transition.type = kCATransitionPush;
        //        transition.subtype = kCATransitionFade;
        //
        //        [[self navigationController].view.layer addAnimation:transition forKey:kCATransition];
        
        [self.navigationController pushViewController:next2 animated:YES];
    }
}

- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

//-(void)changecoloragain
//{
//    change.backgroundColor=[UIColor clearColor];
//}
//-(void)changecolor
//{
//    change.backgroundColor=[UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0];
//}

-(void)gomenu:(UIButton *)button1
{
    // DebugLog(@"123456789");
    array_menu=[[NSMutableArray alloc]init];
    [array_menu addObject:@"Group Info"];
    [array_menu addObject:@"Email Group"];
    [array_menu addObject:@"SMS Group"];
    [array_menu addObject:@"Locate Group"];
    
    
    //    [array_menu addObject:@"View Group Picture"];
    //    [array_menu addObject:@"Leave Group"];
    
    arraymenu_admin=[[NSMutableArray alloc]init];
    [arraymenu_admin addObject:@"Group Info"];
    [arraymenu_admin addObject:@"Email Group"];
    [arraymenu_admin addObject:@"SMS Group"];
    [arraymenu_admin addObject:@"Locate Group"];
    
    //    [arraymenu_admin addObject:@"View Group Picture"];
    //    [arraymenu_admin addObject:@"Add Member"];
    //    [arraymenu_admin addObject:@"Group Settings"];
    
    
    arrayadmin_imagename=[[NSMutableArray alloc]initWithObjects:@"searchwhite.png",/*@"viewgrpimage.png",@"gr_addmembers",@"gr_settings",*/@"emailgroup",@"smsgroup",@"new_map",nil];
    
    array_imagename=[[NSMutableArray alloc] initWithObjects:@"searchwhite.png",/*@"viewgrpimage.png",@"cross.png",*/@"emailgroup",@"smsgroup",@"new_map", nil];
    
    if (!expand)
    {
        if ([isadmin isEqualToString:@"1"])
        {
            popupview=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 200)];
            popupview.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.9];
            popupview.opaque=NO;
            //popupview.layer.cornerRadius=5;
            //            popupview.layer.shadowOpacity=0.8;
            //            popupview.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
            [mainview addSubview:popupview];
        }
        else if ([isadmin isEqualToString:@"0"])
        {
            popupview=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, array_imagename.count*50)];
            popupview.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.9];
            popupview.opaque=NO;
            //popupview.layer.cornerRadius=5;
            //            popupview.layer.shadowOpacity=0.8;
            //            popupview.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
            [mainview addSubview:popupview];
        }
        
        menutable=[[UITableView alloc]initWithFrame:CGRectMake(0, popupview.frame.origin.y, popupview.frame.size.width, popupview.frame.size.height)];
        menutable.delegate=self;
        menutable.tag=2;
        menutable.dataSource=self;
        [menutable setBackgroundColor:[UIColor clearColor]];
        //menutable.separatorInset=UIEdgeInsetsMake(<#CGFloat top#>, <#CGFloat left#>, <#CGFloat bottom#>, <#CGFloat right#>)
        //menutable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        menutable.separatorStyle=UITableViewCellSeparatorStyleNone;
        menutable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        [popupview addSubview:menutable];
        menutable.scrollEnabled=NO;
        [menutable reloadData];
        
        expand=YES;
        backbutton.tag=1;
        
    }
    else if (expand)
    {
        popupview.hidden=YES;
        menu.tag=9;
        backbutton.tag=0;
        expand=NO;
    }
    
    
}


-(void)viewDidDisappear:(BOOL)animated
{
    [group_array removeAllObjects];
    [act removeFromSuperview];
    [mainview removeFromSuperview];
    
    back_label.alpha = 1.0f;
    backimage.alpha = 1.0f;
    group_namelbl.alpha = 1.0f;
    // group_namebtn.alpha = 1.0f;
    membercount.alpha = 1.0f;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    //DebugLog(@"id id id id ----- %@",groupid);
    //del.tabBarController.tabBar.frame = CGRectMake(0, 0, 0, 0);
    //    self.tabBarController.tabBar.frame = CGRectMake(0, 0, 0, 0);
    //    self.tabBarController.tabBar.frame = CGRectMake(0, [[UIScreen mainScreen]bounds].size.height-54, 0, 0);
    //     self.tabBarController.tabBar.hidden = YES;
    
    
    // group_namelbl.text=groupname;
    
    backimage=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"back3"]];
    //    backimage.frame=CGRectMake(8, 15, 10, 20);
    backimage.frame=CGRectMake(15, 32, 12, 20);
    [change addSubview:backimage];
    backimage.userInteractionEnabled=YES;
    backbutton = [UIButton buttonWithType:UIButtonTypeCustom];
    //backbutton.frame = CGRectMake(0, 0, groupimage.frame.origin.x-1, 64);
    backbutton.frame = CGRectMake(0, 0,70, 64);
    backbutton.backgroundColor=[UIColor clearColor];
    [backbutton setTitle:@"" forState:UIControlStateNormal];
    //[backbutton setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1.0]] forState:UIControlStateHighlighted];
    [backbutton addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDown];
    [backbutton addTarget:self action:@selector(changecoloragain) forControlEvents:UIControlEventTouchDragExit];
    [backbutton addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDragEnter];
    [backbutton addTarget:self action:@selector(goback:) forControlEvents:UIControlEventTouchUpInside];
    backbutton.tag=0;
    [change addSubview:backbutton];
    
    if (![del.groupnameUpdate isKindOfClass:[NSNull class]] && ![del.groupnameUpdate isEqualToString:@""] && ![del.groupnameUpdate isEqualToString:@"(null)"] && del.groupnameUpdate!=nil)
    {
        
        
        if([del.groupnameUpdate isEqualToString:groupname])
        {
            group_namelbl.text=groupname;
            //[group_namebtn setTitle:groupname forState:UIControlStateNormal];
            NSLog(@"Existing Group name...%@",groupname);
        }
        else
        {
            group_namelbl.text=del.groupnameUpdate;
            //[group_namebtn setTitle:del.groupnameUpdate forState:UIControlStateNormal];
            NSLog(@"New Group name...%@",del.groupnameUpdate);
        }
        
    }
    else
    {
        group_namelbl.text=groupname;
        //  [group_namebtn setTitle:groupname forState:UIControlStateNormal];
    }
    
    menu.enabled=NO;
    ind= 0;
    ind1 = 0;
    
    back_label.alpha = 1.0f;
    backimage.alpha = 1.0f;
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    version = [[UIDevice currentDevice] systemVersion];
    //DebugLog(@"version: %@",version);
    isAtLeast7 = [version floatValue] < 8.0;
    isAtLeast8 = [version floatValue] >= 8.0;
    
    
    //    moreImage=[[UIImageView alloc]initWithFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-[UIImage imageNamed:@"menu-icon@2x.png"].size.width-10, 25, [UIImage imageNamed:@"menu-icon@2x.png"].size.width,[UIImage imageNamed:@"menu-icon@2x.png"].size.height)];
    //    [moreImage setImage:[UIImage imageNamed:@"menu-icon@2x.png"]];
    //    [blackBack addSubview:moreImage];
    //
    //    menu=[[UIButton alloc]initWithFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-55,[[UIApplication sharedApplication]statusBarFrame].size.height, 55,blackBack.frame.size.height-[[UIApplication sharedApplication]statusBarFrame].size.height)];//CGRectMake(290, 32, 22, 28)];
    //    [menu setBackgroundImage:nil forState:UIControlStateDisabled];
    //    [menu setBackgroundImage:nil forState:UIControlStateSelected];
    //    [menu setBackgroundImage:nil forState:UIControlStateHighlighted];
    //    [menu addTarget:self action:@selector(gomenu:) forControlEvents:UIControlEventTouchUpInside];
    //    menu.tag=9;
    //    menu.contentMode=UIViewContentModeScaleAspectFill;
    //    expand=NO;
    //    menu.enabled=NO;
    //    [blackBack addSubview:menu];
    
    
    
    mainview = [[UIView alloc]initWithFrame:CGRectMake(0,blackBack.frame.size.height, self.view.frame.size.width, self.view.frame.size.height -72)];
    mainview.backgroundColor=[UIColor whiteColor];
    // mainview.layer.zPosition=5;
    //    mainview.layer.zPosition=3;
    [self.view addSubview:mainview];
    
    
    act = [[UIActivityIndicatorView alloc] init];
    act.center = self.view.center;
    [act startAnimating];
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [act setColor:[UIColor blackColor]];
    [self.view addSubview:act];
    
    
    
    nouserlb= [[UILabel alloc]initWithFrame:CGRectMake(0, 70, [UIScreen mainScreen].bounds.size.width, 30)];
    [mainview addSubview:nouserlb];
    nouserlb.text=@"No results";
    nouserlb.backgroundColor=[UIColor clearColor];
    nouserlb.textAlignment=NSTextAlignmentCenter;
    nouserlb.textColor=[UIColor grayColor];//colorWithRed:238.0f/255.0f green:238.0f/255.0f blue:238.0f/255.0f alpha:1];
    nouserlb.hidden=YES;
    
    if ([check_search isEqualToString:@"YES"])
    {
        [self search];
    }
    if ([check_member isEqualToString:@"YES"])
    {
        [self addmember];
        searchtableback.hidden=NO;
        groupstable.hidden=YES;
    }
    
    groupstable =[[UITableView alloc]initWithFrame:CGRectMake(0,0,mainview.frame.size.width,mainview.frame.size.height)];
    groupstable.delegate=self;
    groupstable.tag=1;
    [groupstable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    groupstable.dataSource=self;
    [groupstable setBackgroundColor:[UIColor clearColor]];
    
    //    if (!filter)
    //    {
    //        if ([groupnames count] > 0)
    //        {
    //            if ([[UIDevice currentDevice].systemVersion floatValue] <= 7.1f)
    //            {
    //                scrollToPath = [NSIndexPath indexPathForRow:del.scrollrow inSection:del.scrollsection];
    //                //DebugLog(@"scrollpath= %@",scrollToPath);
    //
    //                if ([[groupnames objectForKey:[NSNumber numberWithInteger:del.scrollsection]] count] > 0)
    //                    [groupstable scrollToRowAtIndexPath:scrollToPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
    //            }
    //        }
    //    }
    //groupstable.bounces=NO;
    groupstable.sectionIndexColor=[UIColor whiteColor];
    groupstable.sectionIndexTrackingBackgroundColor = [UIColor whiteColor];
    //groupstable.layer.borderWidth=1;
    //    groupstable.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    //    groupstable.separatorStyle=UITableViewCellSeparatorStyleNone;
    //groupstable.separatorStyle = UITableViewCellSeparatorStyleSingleLineEtched;
    groupstable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    //    if (h==0)
    //    {
    
    
    //    }
    
    
    
    //    groupstable =[[UITableView alloc]initWithFrame:CGRectMake(0,0,320,480)];
    //    groupstable.delegate=self;
    //    groupstable.tag=1;
    //    groupstable.dataSource=self;
    //    [groupstable setBackgroundColor:[UIColor clearColor]];
    //    //groupstable.layer.borderWidth=1;
    //    //    groupstable.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    //    //    groupstable.separatorStyle=UITableViewCellSeparatorStyleNone;
    //    //groupstable.separatorStyle = UITableViewCellSeparatorStyleSingleLineEtched;
    //    groupstable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    //    if (h==0)
    //    {
    //        [mainview addSubview:groupstable];
    //
    //    }
    
    UITableViewController *tableViewController = [[UITableViewController alloc] init];
    tableViewController.tableView = groupstable;
    
    refreshcontrolnew = [[UIRefreshControl alloc] init];
    [refreshString addAttributes:@{NSForegroundColorAttributeName : [UIColor grayColor]} range:NSMakeRange(0, refreshString.length)];
    refreshcontrolnew.attributedTitle = refreshString;
    [refreshcontrolnew addTarget:self action:@selector(reloadAgain) forControlEvents:UIControlEventValueChanged];
    tableViewController.refreshControl = refreshcontrolnew;
    refreshcontrolnew.tintColor=[UIColor grayColor];
    
    //[self datadisplay];
    
    NSOperationQueue *queue1 = [NSOperationQueue new];
    NSInvocationOperation *operation1 = [[NSInvocationOperation alloc]
                                         initWithTarget:self
                                         selector:@selector(loaddata)
                                         object:nil];
    [queue1 addOperation:operation1];
    
    
    
    
}

-(void)detailpic: (UIGestureRecognizer *)sender
{
    ConGroupPicViewController *congrppic=[[ConGroupPicViewController alloc] init];
    congrppic.grouppic=[UIImage imageWithData:decodedData];
    backbutton.tag=0;
    [self presentViewController:congrppic animated:YES completion:nil];
}

- (void)loaddata
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *groups_url =[NSString stringWithFormat:@"https://budnav.com/ext/?action=group-getmembers&group_id=%@&image=true&thumb=true&access_token=%@&device_id=%@",groupid,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
    DebugLog(@"groups url string ------ %@",groups_url);
    NSError *error1=nil;
    @try {
        NSData *data2=[NSData dataWithContentsOfURL:[NSURL URLWithString:groups_url]options:NSDataReadingUncached error:&error1];
        if (data2 != nil)
        {
            groupdict=[NSJSONSerialization JSONObjectWithData:data2 //1
                                                      options:kNilOptions
                                                        error:&error1];
            //DebugLog(@"-----Groups info ----- %@",groupdict);
            menu.enabled=YES;
            //        [menu setBackgroundImage:[UIImage imageNamed:@"menu-icon@2x.png"] forState:UIControlStateNormal];
            //[menu setBackgroundImage:[UIImage imageNamed:@"Untitled-8.png"] forState:UIControlStateNormal];
            //menu.adjustsImageWhenHighlighted=YES;
            
            
            groupmember_array=[[NSMutableArray alloc]init];
            groupnames = [[groupdict valueForKey:@"details"] valueForKeyPath:@"members"];
            DebugLog(@"GROUP MEMBERS %ld",groupnames.count);
            
            DebugLog(@"Group Details Getting:%@",groupdict);
            
            if ([[groupdict objectForKey:@"success"] intValue]==1)
            {
                mailarr=[[NSMutableArray alloc]init];
                
                mailarr=[[[groupdict objectForKey:@"details"]objectForKey:@"members" ] mutableCopy];
                
                NSLog(@"The Mail Array...%@",mailarr);
                
                NSMutableDictionary *mdic = [[NSMutableDictionary alloc] init];
                marr = [[NSMutableArray alloc]init];
                messagearr=[[NSMutableArray alloc]init];
                
                for (mdic in mailarr)
                {
                    if ([[mdic objectForKey:@"id"] intValue] != [[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"] intValue])
                    {
                        [marr addObject:[mdic valueForKey:@"email"]];
                        [messagearr addObject:[mdic valueForKey:@"phone"]];
                    }
                    
                }
                
                NSLog(@"New mail array:    %@", marr);
                NSLog(@"New message array:    %@", messagearr);
                
                
                
                // create a dictionary to store an array of objects for each section
                NSMutableDictionary *tempSections = [NSMutableDictionary dictionary];
                
                // iterate through each dictionaey in the list, and put them into the correct section
                for (NSDictionary *item in groupnames)
                {
                    // get the index of the section (Assuming the table index is showing A-#)
                    NSInteger indexName = [[UILocalizedIndexedCollation currentCollation] sectionForObject:[item valueForKey:@"name"] collationStringSelector:@selector(description)];
                    
                    NSNumber *keyName = [NSNumber numberWithInteger:indexName];
                    
                    // if an array doesnt exist for the key, create one
                    NSMutableArray *arrayName = [tempSections objectForKey:keyName];
                    if (arrayName == nil)
                    {
                        arrayName = [NSMutableArray array];
                    }
                    
                    // add the dictionary to the array (add the actual value as we need this object to sort the array later)
                    [arrayName addObject: item];   //[item valueForKey:@"fullname"]];
                    
                    // put the array with new object in, back into the dictionary for the correct key
                    [tempSections setObject:arrayName forKey:keyName];
                }
                
                /* now to do the sorting of each index */
                
                NSMutableDictionary *sortedSections = [NSMutableDictionary dictionary];
                
                // sort each index array (A..Z)
                [tempSections enumerateKeysAndObjectsUsingBlock:^(id key, id array1, BOOL *stop)
                 {
                     // sort the array - again, we need to tell it which selctor to sort each object by
                     NSArray *sortedArray = [[UILocalizedIndexedCollation currentCollation] sortedArrayFromArray:array1 collationStringSelector:@selector(description)];
                     
                     NSSortDescriptor *sort =[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)];
                     sortedArray=[[sortedArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]] mutableCopy];
                     [sortedSections setObject:[NSMutableArray arrayWithArray:sortedArray] forKey:key];
                     
                     
                 }];
                //DebugLog(@"Sorted array %@",sortedSections );
                // set the global sectioned dictionary
                groupnames = sortedSections;
                
                //            for (int i = 0; i<26; i++) {
                //                if ([groupnames objectForKey:[NSString stringWithFormat:@"%d",i]]) {
                //
                //                    [group_array addObject:[groupnames objectForKey:[NSString stringWithFormat:@"%d",i]]];
                //                }
                //
                //            }
                //group_array = [groupnames mutableCopy];
                
                //  [[groupnames objectForKey:[NSNumber numberWithInteger:indexPath.section]] objectAtIndex:indexPath.row]
                for (int i =0; i<26; i++) {
                    if (![[groupnames objectForKey:[NSNumber numberWithInteger:i]] isKindOfClass:[NSNull class]] && ![[groupnames objectForKey:[NSNumber numberWithInteger:i]] isEqual:@"(null)"] && ![[groupnames objectForKey:[NSNumber numberWithInteger:i]] isEqual:(id)NULL] && [groupnames objectForKey:[NSNumber numberWithInteger:i]]!=nil) {
                        DebugLog(@"DATA:%@",[groupnames objectForKey:[NSNumber numberWithInteger:i]]);
                        [group_array addObject:[groupnames objectForKey:[NSNumber numberWithInteger:i]]];
                    }
                    
                }
                
                DebugLog(@"Group Array: %@",group_array);
                DebugLog(@"group member details 2nd time %@",groupnames);
                
                
                
                [self performSelectorOnMainThread:@selector(datadisplay) withObject:nil waitUntilDone:NO];
                
                
            }
            else
            {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    
                    [activity removeFromSuperview];
                    [act removeFromSuperview];
                    
                    alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@", [groupdict objectForKey:@"error"] ]
                                                       message:nil
                                                      delegate:self
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                    
                    [alert show];
                }];
                
            }
            
        }
        
        else
        {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                [activity removeFromSuperview];
                [act removeFromSuperview];
                DebugLog(@"no connnnn");
                alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                                   message:nil
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                
                [alert show];
                
            }];
        }
        
    }
    @catch (NSException *exception) {
        // DebugLog(@"exception %@",exception);
    }
}

- (void)datadisplay
{
    //    AppDelegate *app=[[AppDelegate alloc]init];
    //
    //    if([group_namelbl.text isEqualToString:@"groupname"])
    //    {
    //     group_namelbl.text=groupname;
    //    }
    //    else
    //    {
    //        group_namelbl.text=app.groupnameUpdate;
    //    }
    if ([[[groupdict valueForKey:@"details"] valueForKeyPath:@"members" ] count ]>1)
    {
        membercount.text=[NSString stringWithFormat:@"%lu Members",(unsigned long)[[[groupdict valueForKey:@"details"] valueForKeyPath:@"members"] count]];
        // membercount.text=[[groupdict valueForKey:@"details"] valueForKeyPath:@"members"];
    }
    else
    {
        membercount.text=[NSString stringWithFormat:@"%lu Member",(unsigned long)[[[groupdict valueForKey:@"details"] valueForKeyPath:@"members"] count]];
        // membercount.text=[[groupdict valueForKey:@"details"] valueForKeyPath:@"members"];
    }
    
    base64String1= [NSString stringWithFormat:@"%@",[[groupdict valueForKey:@"details"] valueForKeyPath:@"thumb"]];
    
    if ([base64String1 length] >6)
    {
        decodedData1 = [[NSData alloc] initWithBase64EncodedString:base64String1 options:0];
        
    }
    
    
    //DebugLog(@"groups array ---- %@",groupnames);
    
    //    for(temp in groupnames){
    //         //birdtemp = temp[@"name"];
    //        [group_array addObject:groupnames];
    //        DebugLog(@"array %@",group_array);
    //        //[groupnames_array addObject:birdtemp];
    //    }
    
    //for (int b=0; b<[[groupnames valueForKey:@"name"]count]; b++) {
    
    
    //DebugLog(@"bb %@",group_array);
    groupimage.contentMode= UIViewContentModeScaleAspectFill;
    groupimage.image=[UIImage imageWithData:decodedData1];
    //    if (group_array.count==1) {
    //        membercount.text=[NSString stringWithFormat:@"%lu Member",(unsigned long)group_array.count];
    //    }
    //    else
    //        membercount.text=[NSString stringWithFormat:@"%lu Members",(unsigned long)group_array.count];
    //    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)];
    //    group_array=[[group_array sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]] mutableCopy];
    
    //filter=NO;
    
    
    //    for (int i=0; i<[[groupnames valueForKey:@"name"]count]; i++)
    //    {
    //        [groupmember_array addObject:[[groupnames valueForKey:@"name"] objectAtIndex:i]];
    //        DebugLog(@"array %@",groupmember_array);
    //    }
    //
    //
    //    for (int y=0; y<[group_array count]; y++)
    //    {
    //        NSString *ad=[NSString stringWithFormat:@"%@",[[groupnames objectForKey:@"admin"]objectAtIndex:y]];
    //        if ([ad isEqualToString:@"1"]) {
    //            name=[NSString stringWithFormat:@"%@ %@",[[group_array objectAtIndex:y] objectForKey:@"name"],[[group_array objectAtIndex:y] objectForKey:@"surname"]];
    //
    //            //DebugLog(@"name: %@",name);
    //
    //        }
    //
    //
    //    }
    //h=0;
    
    if (h!=2)
        [mainview addSubview:groupstable];
    [groupstable reloadData];
    groupstable.sectionIndexColor=[UIColor colorWithRed:40.0f/255.0f green:170.0f/255.0f blue:185.0f/255.0f alpha:1];
    
    NSOperationQueue *queue = [NSOperationQueue new];
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(imageconvert)
                                        object:nil];
    [queue addOperation:operation];
    
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
}

-(void)imageconvert
{
    base64String= [NSString stringWithFormat:@"%@",[[groupdict valueForKey:@"details"] valueForKeyPath:@"image"]];
    
    if ([base64String length] >6)
    {
        decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
        
    }
    
}


#pragma mark- table delegates and datasource

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section1
{
    BOOL showSection = [[groupnames objectForKey:[NSNumber numberWithInteger:section1]] count]!=0;
    if (showSection)
        //only show the section title if there are rows in the section
        return  [[[UILocalizedIndexedCollation currentCollation] sectionTitles] objectAtIndex:section1];
    else
        return 0;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 0;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView.tag==1)
    {
        if (!filter)
        {
            
            int total;
            total =1;
            //        //DebugLog(@"e4eee %ld",[[[UILocalizedIndexedCollation currentCollation] sectionTitles] count]);
            return [[[UILocalizedIndexedCollation currentCollation] sectionTitles] count]; //[final_con_array count];
        }
        else
            return 1;
        
    }
    else
        return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section1
{
    if (tableView.tag==1)
    {
        if (!filter)
        {
            DebugLog(@"GROUP NAMES:%@",groupnames);
            DebugLog(@"count of groups ---- %lu",(unsigned long)[[groupnames objectForKey:[NSNumber numberWithInteger:section1]] count]);
            //return [group_array count];
            return [[groupnames objectForKey:[NSNumber numberWithInteger:section1]] count];
        }
        else if (filter)
        {
            return filtered_arr.count;
        }
        
    }
    else if (tableView.tag==2)
    {
        if ([isadmin isEqualToString:@"1"]) {
            
            DebugLog(@"Admin Count:%@",arrayadmin_imagename);
            return arrayadmin_imagename.count;
            // return 3;
        }
        else if ([isadmin isEqualToString:@"0"])
        {
            DebugLog(@"Image Count:%@",array_imagename);
            return array_imagename.count;
        }
        
        //return 3;
        
    }
    
    else if (tableView.tag==3)
    {
        DebugLog(@"%lu",(unsigned long)[[array_copy valueForKey:@"name"] count]);
        return [[array_copy valueForKey:@"name"] count];
    }
    
    else if (tableView.tag==4)
    {
        return [search_result count];
    }
    
    
    return 0;
}


- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    if (tableView.tag==1) {
        if (!filter)
        {
            [[[iToast makeText:[NSString stringWithFormat:@" %@",title]]
              setGravity:iToastGravityCenter] show];
            
            UILocalizedIndexedCollation *currentCollation = [UILocalizedIndexedCollation currentCollation];
            return [currentCollation sectionForSectionIndexTitleAtIndex:index];
            //        }
        }
        else
            return 0;
    }
    else
        return 0;
    
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if (tableView.tag==1)
    {
        if (!filter)
            return [[UILocalizedIndexedCollation currentCollation] sectionIndexTitles];
        else
            return Nil;
    }
    else
        return Nil;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // NSString *login_id = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"userid"]];
    
    //[group_array removeAllObjects];
    
    static NSString *MyIdentifier = @"MyReuseIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    cell=nil;
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:MyIdentifier];
    }
    [act stopAnimating];
    menu.enabled=YES;
    
    
    
    if (tableView.tag==1)
    {
        
        // NSLog(@"CELL TAG:%ld",cell.tag);
        
        if (!filter)
        {
            
            //            DebugLog(@"LOGIN ID:%@",login_id);
            
            
            //cell.selectionStyle = UITableViewCellSelectionStyleNone;
            DebugLog(@"GROUP ARRAY:%@",groupnames);
            DebugLog(@"FILTER ARRAY::%ld %@",[filtered_arr count],filtered_arr);
            DebugLog(@"GROUP ARRAY DOUBLE:%ld,%@",[group_array count],group_array);
            id object = [[groupnames objectForKey:[NSNumber numberWithInteger:indexPath.section]] objectAtIndex:indexPath.row];
            
            // [group_array addObject:object];
            
            
            
            
            
            NSString *ad=[NSString stringWithFormat:@"%@",[object objectForKey:@"admin"]];
            //  DebugLog(@"%@",ad);
            if ([ad isEqualToString:@"1"])
                name=[NSString stringWithFormat:@"%@ %@",[object objectForKey:@"name"],[object objectForKey:@"surname"]];
            
            DebugLog(@"name: %@",group_array);
            
            
            cell.backgroundColor = [UIColor clearColor];
            
            UILabel *firstnamelbl = [[UILabel alloc]init] ;//]WithFrame:CGRectMake(15.0f,5.0f, 280.0f, 30.0f)];
            [firstnamelbl setText:[[NSString stringWithFormat:@"%@",[object objectForKey:@"name"]] capitalizedString]];//,[object objectForKey:@"surname"]]];
            [firstnamelbl setTextAlignment:NSTextAlignmentLeft];
            [firstnamelbl setTextColor:[UIColor blackColor]];
            [firstnamelbl setFont:[UIFont fontWithName:@"ProximaNova-Bold" size:17]];
            [cell addSubview:firstnamelbl];
            
            UILabel *lastnamelbl = [[UILabel alloc]init];
            [lastnamelbl setText:[[NSString stringWithFormat:@"%@",[object objectForKey:@"surname"]] capitalizedString]];
            [lastnamelbl setTextAlignment:NSTextAlignmentLeft];
            [lastnamelbl setTextColor:[UIColor blackColor]];
            [lastnamelbl setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
            [cell addSubview:lastnamelbl];
            
            CGSize maximumSize = CGSizeMake(160, 50);
            
            UIFont *myFont = [UIFont fontWithName:@"ProximaNova-Bold" size:17];
            CGSize myStringSize = [firstnamelbl.text sizeWithFont:myFont
                                   
                                                constrainedToSize:maximumSize
                                   
                                                    lineBreakMode:NSLineBreakByWordWrapping];
            
            firstnamelbl.frame = CGRectMake(15, 5, myStringSize.width, 30);
            lastnamelbl.frame = CGRectMake(firstnamelbl.frame.origin.x+firstnamelbl.frame.size.width+4, 5, 210, 30);
            
            
            
            UIImageView *proficon = [[UIImageView alloc]initWithFrame:CGRectMake(15.0f,35,11.0f,10)];
            proficon.backgroundColor=[UIColor clearColor];
            proficon.contentMode=UIViewContentModeScaleAspectFit;
            
            [cell addSubview:proficon];
            
            
            if (![[NSString stringWithFormat:@"%@",[object objectForKey:@"city" ]] isEqualToString:@""] ||[[NSString stringWithFormat:@"%@",[object objectForKey:@"city" ]] length]!=0)
            {
                UILabel *citylbl = [[UILabel alloc]initWithFrame:CGRectMake(28,25.0f,150.0f, 30.0f)];
                [citylbl setText:[NSString stringWithFormat:@"%@, %@",[object objectForKey:@"city" ],[object objectForKey:@"country"]]];//[[[group_array objectAtIndex:0] objectAtIndex:indexPath.row]objectForKey:@"city"]]];
                [citylbl setTextAlignment:NSTextAlignmentLeft];
                [citylbl setTextColor:[UIColor grayColor]];
                [citylbl setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:13]];
                [cell addSubview:citylbl];
                
                
                [proficon setImage:[UIImage imageNamed:@"Map Marker Contacts Screen copy2x.png"]];
            }
            
            UILabel *separatorlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 54.5f, [UIScreen mainScreen].bounds.size.width, 0.5f)];
            separatorlabel.backgroundColor=[UIColor colorWithRed:(204.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f];
            [cell addSubview:separatorlabel];
            
            
            //            UIView *cellView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)];
            //            [cell.contentView addSubview:cellView];
            //            cellView.backgroundColor = [UIColor clearColor];
            //            cellView.tag = [[[group_array objectAtIndex:indexPath.row] objectForKey:@"id"] intValue];
            
            //    UILabel *countrylbl = [[UILabel alloc]initWithFrame:CGRectMake(110.0f,25.0f, 150.0f, 30.0f)];
            //    [countrylbl setText:[NSString stringWithFormat:@",%@",[[[group_array objectAtIndex:0] objectAtIndex:indexPath.row]objectForKey:@"country"]]];
            //    [countrylbl setTextAlignment:NSTextAlignmentLeft];
            //    [countrylbl setTextColor:[UIColor grayColor]];
            //    [countrylbl setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:13]];
            //    [cell addSubview:countrylbl];
            
            //            DebugLog(@"MEMBER ID:%d %d",[[[group_array objectAtIndex:ind ] objectForKey:@"id"] intValue],ind);
            //            cell.tag = [[[group_array objectAtIndex:ind ] objectForKey:@"id"] intValue];
            DebugLog(@"MEMBER ID:%d %d",[[object objectForKey:@"id"] intValue],ind);
            cell.tag = [[object objectForKey:@"id"] intValue];
            DebugLog(@"CEL TAG TAG:%ld",cell.tag);
            ind++;
        }
        else if (filter)
        {
            DebugLog(@"GROUP ARRAY1:%@",groupnames);
            DebugLog(@"FILTER ARRAY1:%ld %@",[filtered_arr count],filtered_arr);
            
            cell.backgroundColor = [UIColor clearColor];
            
            
            
            DebugLog(@"GROUP FROM FILTER:%@",filtered_arr);
            UILabel *firstnamelbl = [[UILabel alloc]initWithFrame:CGRectMake(15.0f,5.0f, 280.0f, 30.0f)];
            [firstnamelbl setText:[NSString stringWithFormat:@"%@ %@",[[filtered_arr valueForKey:@"name"]objectAtIndex:indexPath.row],[[filtered_arr valueForKey:@"surname"]objectAtIndex:indexPath.row]]];
            [firstnamelbl setTextAlignment:NSTextAlignmentLeft];
            [firstnamelbl setTextColor:[UIColor blackColor]];
            [firstnamelbl setFont:[UIFont fontWithName:@"ProximaNova-Bold" size:17]];
            [cell addSubview:firstnamelbl];
            
            UIImageView *proficon = [[UIImageView alloc]initWithFrame:CGRectMake(15.0f,35,11.0f,10)];
            proficon.backgroundColor=[UIColor clearColor];
            proficon.contentMode=UIViewContentModeScaleAspectFit;
            [cell addSubview:proficon];
            
            
            
            if (![[NSString stringWithFormat:@"%@",[[filtered_arr valueForKey:@"city" ]objectAtIndex:indexPath.row]] isEqualToString:@""] && [[NSString stringWithFormat:@"%@",[[filtered_arr valueForKey:@"city" ]objectAtIndex:indexPath.row]] length]!=0 && ![[NSString stringWithFormat:@"%@",[[filtered_arr valueForKey:@"city" ]objectAtIndex:indexPath.row]] isKindOfClass:[NSNull class]] && [NSString stringWithFormat:@"%@",[[filtered_arr valueForKey:@"city" ]objectAtIndex:indexPath.row]]!= (id)[NSNull null]) {
                UILabel *citylbl = [[UILabel alloc]initWithFrame:CGRectMake(28.0f,25.0f,150.0f, 30.0f)];
                [citylbl setText:[NSString stringWithFormat:@"%@, %@",[[filtered_arr valueForKey:@"city" ]objectAtIndex:indexPath.row],[[filtered_arr valueForKey:@"country"] objectAtIndex:indexPath.row]]];//[[[group_array objectAtIndex:0] objectAtIndex:indexPath.row]objectForKey:@"city"]]];
                [citylbl setTextAlignment:NSTextAlignmentLeft];
                [citylbl setTextColor:[UIColor grayColor]];
                [citylbl setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:13]];
                [cell addSubview:citylbl];
                
                [proficon setImage:[UIImage imageNamed:@"Map Marker Contacts Screen copy2x.png"]];
                
            }
            
            
            UILabel *separatorlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 54.5f, [UIScreen mainScreen].bounds.size.width, 0.5f)];
            separatorlabel.backgroundColor=[UIColor colorWithRed:(204.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f];
            [cell addSubview:separatorlabel];
            
            //            DebugLog(@"MEMBER ID:%d %d",[[[filtered_arr objectAtIndex:ind1] objectForKey:@"id"] intValue],ind1);
            cell.tag = [[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"id"] intValue];
            //            NSLog(@"CEL TAG TAG:%ld",cell.tag);
            //            ind1++;
            
            backbutton.tag=2;
        }
        
        if ([isadmin isEqualToString:@"1"]) {
            UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                                  initWithTarget:self action:@selector(handleLongPress:)];
            //                        lpgr.view.tag = indexPath.row;
            lpgr.minimumPressDuration = 1.0; //seconds
            [cell addGestureRecognizer:lpgr];
        }
        
    }
    
    else if (tableView.tag==2)
    {
        cell.backgroundColor=[UIColor clearColor];
        
        if ([isadmin isEqualToString:@"1"])
        {
            //            cell.imageView.image=[UIImage imageNamed:[array_imagename objectAtIndex:indexPath.row]];
            //            //cell.imageView.contentMode=UIViewContentModeCenter;
            //            cell.textLabel.textColor=[UIColor whiteColor];
            //            cell.textLabel.text=[arraymenu_admin objectAtIndex:indexPath.row];
            //            cell.textLabel.textAlignment=NSTextAlignmentCenter;
            //            cell.textLabel.font=[UIFont fontWithName:@"ProximaNova-Regular" size:14];
            
            UILabel *menulabel = [[UILabel alloc]initWithFrame:CGRectMake(55.0f,0.0f, menutable.frame.size.width-55, 49.0f)];
            [menulabel setText:[NSString stringWithFormat:@"%@",[arraymenu_admin objectAtIndex:indexPath.row]]];
            [menulabel setTextAlignment:NSTextAlignmentLeft];
            [menulabel setTextColor:[UIColor whiteColor]];
            [menulabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
            [cell addSubview:menulabel];
            
            UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(15.0f,15.0f,20.0f,20.0f)];
            image.backgroundColor=[UIColor clearColor];
            [image setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[arrayadmin_imagename objectAtIndex:indexPath.row]]]];
            image.contentMode=UIViewContentModeScaleAspectFit;
            [cell addSubview:image];
            
            UILabel *separatorlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 49.5f, [UIScreen mainScreen].bounds.size.width, 0.5f)];
            separatorlabel.backgroundColor=[UIColor grayColor];
            [cell addSubview:separatorlabel];
            
        }
        else if ([isadmin isEqualToString:@"0"])
        {
            //DebugLog(@"not admin");
            UILabel *menulabel = [[UILabel alloc]initWithFrame:CGRectMake(55.0f,0.0f, menutable.frame.size.width-55, 49.0f)];
            [menulabel setText:[NSString stringWithFormat:@"%@",[array_menu objectAtIndex:indexPath.row]]];
            [menulabel setTextAlignment:NSTextAlignmentLeft];
            [menulabel setTextColor:[UIColor whiteColor]];
            [menulabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
            [cell addSubview:menulabel];
            
            UIImageView *cross_image = [[UIImageView alloc]initWithFrame:CGRectMake(15.0f,15.0f,20.0f,20.0f)];
            cross_image.backgroundColor=[UIColor clearColor];
            [cross_image setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_imagename objectAtIndex:indexPath.row]]]];
            cross_image.contentMode=UIViewContentModeScaleAspectFit;
            [cell addSubview:cross_image];
            
            //            UIButton *cross_button=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, menutable.frame.size.width, 55)];
            //            [cross_button setTitle:@"" forState:UIControlStateNormal];
            //            [cross_button addTarget:self action:@selector(leavegroup) forControlEvents:UIControlEventTouchUpInside];
            //            cross_button.userInteractionEnabled=YES;
            //            cross_button.tag=indexPath.row;
            //            [cross_image addSubview:cross_button];
            
            UILabel *separatorlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 49, [UIScreen mainScreen].bounds.size.width, 1)];
            separatorlabel.backgroundColor=[UIColor grayColor];
            [cell addSubview:separatorlabel];
            
            
            //            cell.imageView.image=[UIImage imageNamed:@"cross.png"];
            //            cell.textLabel.text=[array_menu objectAtIndex:indexPath.row];
        }
        
        
    }
    else if (tableView.tag==3)
    {
        cell.backgroundColor=[UIColor clearColor];
        if (array_copy.count<=3) {
            searchview.frame=CGRectMake(10, 0, mainview.frame.size.width-20, 55*array_copy.count);
        }
        
        
        if (![[[array_copy valueForKey:@"surname"]objectAtIndex:indexPath.row] isKindOfClass:[NSNull class]] && [[array_copy valueForKey:@"surname"]objectAtIndex:indexPath.row] != (id)[NSNull null])
        {
            
            search_name=[NSString stringWithFormat:@"%@ %@",[[array_copy valueForKey:@"name"]objectAtIndex:indexPath.row],[[array_copy valueForKey:@"surname"]objectAtIndex:indexPath.row]];
        }
        if ([[[array_copy valueForKey:@"surname"]objectAtIndex:indexPath.row] isKindOfClass:[NSNull class]] || [[array_copy valueForKey:@"surname"]objectAtIndex:indexPath.row] == (id)[NSNull null])
        {
            search_name=[NSString stringWithFormat:@"%@ ",[[array_copy valueForKey:@"name"]objectAtIndex:indexPath.row]];
        }
        
        UILabel *namelbl = [[UILabel alloc]initWithFrame:CGRectMake(15.0f,5.0f, 250.0f, 30.0f)];
        [namelbl setText:search_name];
        [namelbl setTextAlignment:NSTextAlignmentLeft];
        [namelbl setTextColor:[UIColor blackColor]];
        [namelbl setFont:[UIFont fontWithName:@"ProximaNova-Bold" size:17]];
        [cell addSubview:namelbl];
        
        UIImageView *mapPin = [[UIImageView alloc]init];
        mapPin.frame = CGRectMake(15, 34, (18.0f/2.0f),11);// (25.0f/2.0f));
        
        mapPin.contentMode=UIViewContentModeScaleAspectFit;
        mapPin.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:mapPin];
        
        
        NSString *location;
        if ([[[array_copy objectAtIndex:indexPath.row] objectForKey:@"city"] isKindOfClass:[NSNull class]] || [[array_copy objectAtIndex:indexPath.row] objectForKey:@"city"] == (id)[NSNull null] || [[[array_copy objectAtIndex:indexPath.row] objectForKey:@"city"] length] ==0)
        {
            DebugLog(@"no city");
            if ([[[array_copy objectAtIndex:indexPath.row] objectForKey:@"country"] isKindOfClass:[NSNull class]] || [[array_copy objectAtIndex:indexPath.row] objectForKey:@"country"] == (id)[NSNull null] || [[[array_copy objectAtIndex:indexPath.row] objectForKey:@"country"] length] ==0 || [[NSString stringWithFormat:@"%@",[[array_copy objectAtIndex:indexPath.row] objectForKey:@"city"]] isEqualToString:@""])
            {
                DebugLog(@"no country");
            }
            else
                location= [NSString stringWithFormat:@"%@",[[array_copy objectAtIndex:indexPath.row] objectForKey:@"country"]];
        }
        else if (![[[array_copy objectAtIndex:indexPath.row] objectForKey:@"city"] isKindOfClass:[NSNull class]] || [[array_copy objectAtIndex:indexPath.row] objectForKey:@"city"] != (id)[NSNull null] || [[[array_copy objectAtIndex:indexPath.row] objectForKey:@"city"] length] !=0 ||![[NSString stringWithFormat:@"%@",[[array_copy objectAtIndex:indexPath.row] objectForKey:@"city"]] isEqualToString:@""])
        {
            DebugLog(@"city present");
            if ([[[array_copy objectAtIndex:indexPath.row] objectForKey:@"country"] isKindOfClass:[NSNull class]] || [[array_copy objectAtIndex:indexPath.row] objectForKey:@"country"] == (id)[NSNull null] || [[[array_copy objectAtIndex:indexPath.row] objectForKey:@"country"] length] ==0)
            {
                DebugLog(@"no country");
                location= [NSString stringWithFormat:@"%@",[[array_copy objectAtIndex:indexPath.row] objectForKey:@"city"]];
            }
            else
                location= [NSString stringWithFormat:@"%@,%@",[[array_copy objectAtIndex:indexPath.row] objectForKey:@"city"],[[array_copy objectAtIndex:indexPath.row] objectForKey:@"country"]];
        }
        
        else
            location= [NSString stringWithFormat:@"%@,%@",[[array_copy objectAtIndex:indexPath.row] objectForKey:@"city"],[[array_copy objectAtIndex:indexPath.row] objectForKey:@"country"]];
        
        
        UILabel *locationlb = [[UILabel alloc]initWithFrame:CGRectMake(26, 30, 210, 20)];
        [cell.contentView addSubview:locationlb];
        locationlb.backgroundColor=[UIColor clearColor];
        locationlb.text=location;
        locationlb.textColor=[UIColor colorWithRed:153.0f/255.0f green:153.0f/255.0f blue:153.0f/255.0f alpha:1];
        locationlb.font=[UIFont fontWithName:@"ProximaNova-Regular" size:13];
        locationlb.textAlignment=NSTextAlignmentLeft;
        
        if (location) {
            mapPin.image = [UIImage imageNamed:@"Map Marker Contacts Screen copy2x.png"];
        }
        
        
        UILabel *separatorlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 54, [UIScreen mainScreen].bounds.size.width, 1)];
        separatorlabel.backgroundColor=[UIColor lightGrayColor];
        [cell addSubview:separatorlabel];
        //        crossimage=[[UIImageView alloc]initWithFrame:CGRectMake(285, 5, 20, 30)];
        //        crossimage.image=[UIImage imageNamed:@"cross.png"];
        //        [cell addSubview:crossimage];
        //        crossbutton=[[UIButton alloc]init];
        //        [crossbutton setTitle:@"" forState:UIControlStateNormal];
        //        crossbutton.tag=indexPath.row;
        //        [crossbutton addTarget:self action:@selector(cross:) forControlEvents:UIControlEventTouchUpInside];
        //        [crossimage addSubview:crossbutton];
        
        //            cell.textLabel.text=search_name;
        //            DebugLog(@"name is %@",search_name);
        //            cell.textLabel.textColor=[UIColor whiteColor];
        
    }
    
    else if (tableView.tag==4)
    {
        cell.backgroundColor = [UIColor clearColor];
        finalsearch_result=[NSString stringWithFormat:@"%@ %@",[[search_result valueForKey:@"name"]objectAtIndex:indexPath.row],[[search_result valueForKey:@"surname"]objectAtIndex:indexPath.row]];
        UILabel *namelbl = [[UILabel alloc]initWithFrame:CGRectMake(15.0f,5.0f, 280.0f, 30.0f)];
        [namelbl setText:finalsearch_result];
        [namelbl setTextAlignment:NSTextAlignmentLeft];
        [namelbl setTextColor:[UIColor blackColor]];
        [namelbl setFont:[UIFont fontWithName:@"ProximaNova-Bold" size:17]];
        [cell addSubview:namelbl];
        //int i=0;
        
        UIImageView *mapPin = [[UIImageView alloc]init];
        mapPin.frame = CGRectMake(15, 34, (18.0f/2.0f),11);// (25.0f/2.0f));
        
        mapPin.contentMode=UIViewContentModeScaleAspectFit;
        mapPin.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:mapPin];
        
        
        NSString *location;
        if ([[[search_result objectAtIndex:indexPath.row] objectForKey:@"city"] isKindOfClass:[NSNull class]] || [[search_result objectAtIndex:indexPath.row] objectForKey:@"city"] == (id)[NSNull null] || [[[search_result objectAtIndex:indexPath.row] objectForKey:@"city"] length] ==0)
        {
            DebugLog(@"no city");
            if ([[[search_result objectAtIndex:indexPath.row] objectForKey:@"country"] isKindOfClass:[NSNull class]] || [[search_result objectAtIndex:indexPath.row] objectForKey:@"country"] == (id)[NSNull null] || [[[search_result objectAtIndex:indexPath.row] objectForKey:@"country"] length] ==0 || [[NSString stringWithFormat:@"%@",[[search_result objectAtIndex:indexPath.row] objectForKey:@"city"]] isEqualToString:@""])
            {
                DebugLog(@"no country");
            }
            else
                location= [NSString stringWithFormat:@"%@",[[search_result objectAtIndex:indexPath.row] objectForKey:@"country"]];
        }
        else if (![[[search_result objectAtIndex:indexPath.row] objectForKey:@"city"] isKindOfClass:[NSNull class]] || [[search_result objectAtIndex:indexPath.row] objectForKey:@"city"] != (id)[NSNull null] || [[[search_result objectAtIndex:indexPath.row] objectForKey:@"city"] length] !=0 ||![[NSString stringWithFormat:@"%@",[[search_result objectAtIndex:indexPath.row] objectForKey:@"city"]] isEqualToString:@""])
        {
            DebugLog(@"city present");
            if ([[[search_result objectAtIndex:indexPath.row] objectForKey:@"country"] isKindOfClass:[NSNull class]] || [[search_result objectAtIndex:indexPath.row] objectForKey:@"country"] == (id)[NSNull null] || [[[search_result objectAtIndex:indexPath.row] objectForKey:@"country"] length] ==0)
            {
                DebugLog(@"no country");
                location= [NSString stringWithFormat:@"%@",[[search_result objectAtIndex:indexPath.row] objectForKey:@"city"]];
            }
            else
                location= [NSString stringWithFormat:@"%@,%@",[[search_result objectAtIndex:indexPath.row] objectForKey:@"city"],[[search_result objectAtIndex:indexPath.row] objectForKey:@"country"]];
        }
        
        else
            location= [NSString stringWithFormat:@"%@,%@",[[search_result objectAtIndex:indexPath.row] objectForKey:@"city"],[[search_result objectAtIndex:indexPath.row] objectForKey:@"country"]];
        
        
        UILabel *locationlb = [[UILabel alloc]initWithFrame:CGRectMake(26, 30, 210, 20)];
        [cell.contentView addSubview:locationlb];
        locationlb.backgroundColor=[UIColor clearColor];
        locationlb.text=location;
        locationlb.textColor=[UIColor colorWithRed:153.0f/255.0f green:153.0f/255.0f blue:153.0f/255.0f alpha:1];
        locationlb.font=[UIFont fontWithName:@"ProximaNova-Regular" size:13];
        locationlb.textAlignment=NSTextAlignmentLeft;
        
        if (location) {
            mapPin.image = [UIImage imageNamed:@"Map Marker Contacts Screen copy2x.png"];
        }
        
        
        crossimage=[[UIImageView alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-35, 19, 15, 15)];
        crossimage.image=[UIImage imageNamed:@"cross.png"];
        crossimage.userInteractionEnabled=YES;
        [cell addSubview:crossimage];
        
        crossbutton=[[UIButton alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-45, 10, 40, 35)];
        [crossbutton setTitle:nil forState:UIControlStateNormal];
        //        crossbutton.backgroundColor=[UIColor redColor];
        [crossbutton addTarget:self action:@selector(cross:) forControlEvents:UIControlEventTouchUpInside];
        crossbutton.userInteractionEnabled=YES;
        crossbutton.tag=indexPath.row;
        [cell addSubview:crossbutton];
        
        UILabel *separatorlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 54, [UIScreen mainScreen].bounds.size.width, 1)];
        separatorlabel.backgroundColor=[UIColor lightGrayColor];
        [cell addSubview:separatorlabel];
        
        
    }
    DebugLog(@"grop: %@",group_array);
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag==2) {
        return 50;
    }
    else
        return 55.0f;
}

//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (tableView.tag==1)
//    {
//        if (editingStyle == UITableViewCellEditingStyleDelete)
//        {
//            [[[array valueForKey:@"name"]objectAtIndex:indexPath.row] removeObjectAtIndex:indexPath.row];
//            [[[array valueForKey:@"surname"]objectAtIndex:indexPath.row] removeObjectAtIndex:indexPath.row];
//            //[self.numbers removeObjectAtIndex:indexPath.row];
//            // Delete the row from the data source
//            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
//        }
//    }
//
//}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DebugLog(@"Index value:%ld",indexPath.row);
    
    
    
    if (tableView.tag==1)
    {
        //        NSIndexPath *firstVisibleIndexPath = [[groupstable indexPathsForVisibleRows] objectAtIndex:0];
        //        //DebugLog(@"first visible cell's section: %li, row: %li", (long)firstVisibleIndexPath.section, (long)firstVisibleIndexPath.row);
        //
        //        del.scrollsection = firstVisibleIndexPath.section;
        //        del.scrollrow = firstVisibleIndexPath.row;
        //
        //
        //        //    cell.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f];
        //        [[NSUserDefaults standardUserDefaults]setInteger:del.scrollsection forKey:@"savedSection"];
        //        [[NSUserDefaults standardUserDefaults]setInteger:indexPath.row forKey:@"savedrow"];
        
        //    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        //    NSInteger index = indexPath.row;
        //    [userDefaults setInteger:index forKey:@"saverows"];
        //    [userDefaults synchronize];
        
        [[tableView cellForRowAtIndexPath:indexPath] setBackgroundColor:[UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f]];
        
        //        ConDetailsViewController *conprf=[[ConDetailsViewController alloc]init];
        //        conprf.groupid=groupid;
        //        DebugLog(@"GROUP GROUP ID:%@",conprf.groupid);
        
        [searchBar resignFirstResponder];
        
        if (!filter) {
            row=indexPath.row;
            section=indexPath.section;
            
            
        }
        else if (filter)
            row1=indexPath.row;
        navigationqueue = [NSOperationQueue new];
        NSInvocationOperation *operation2=[[NSInvocationOperation alloc]initWithTarget:self selector:@selector(navi) object:nil];
        [navigationqueue addOperation:operation2];
        
        
        
        //        if (!filter)
        //        {
        //            conprf.userid=[[group_array valueForKey:@"id"] objectAtIndex:indexPath.row];
        //            conprf.response=@"yes";
        //        }
        //        else if (filter)
        //        {
        //            conprf.userid=[[filtered_arr valueForKey:@"id"] objectAtIndex:indexPath.row];
        //            conprf.response=@"yes";
        //        }
        //        [self.navigationController pushViewController:conprf animated:YES];
        
    }
    if (tableView.tag==2)
    {
        if (indexPath.row==1)
        {
            //            ConGroupPicViewController *congrppic=[[ConGroupPicViewController alloc] init];
            //            congrppic.grouppic=[UIImage imageWithData:decodedData];
            //            backbutton.tag=0;
            //            expand=NO;
            //            [self presentViewController:congrppic animated:YES completion:nil];
            //            popupview.hidden=YES;
            
            
            
            mailComposer = [[MFMailComposeViewController alloc]init];
            mailComposer.mailComposeDelegate = self;
            NSMutableArray *recipentsArray=[[NSMutableArray alloc]init];
            recipentsArray = [NSMutableArray arrayWithArray:marr];
            [mailComposer setToRecipients:recipentsArray];
            
            [mailComposer setMessageBody:@"" isHTML:NO];
            [self.navigationController presentViewController:mailComposer animated:YES completion:nil];
            
        }
        
        if (indexPath.row==3)
        {
            expand=NO;
            [menutable removeFromSuperview];
            ConLocateGroupViewController *con = [[ConLocateGroupViewController alloc]init];
            con.group_id = groupid;
            [self.tabBarController.tabBar setHidden:YES];
            
            // del.profileRedirect = NO;
            
            [self.navigationController pushViewController:con animated:NO];
        }
        
        
        if ([isadmin isEqualToString:@"1"])
        {
            if (indexPath.row==0)
            {
                //                check_search=@"NO";
                //                [self search];
                moreImage.hidden = YES;
                menu.hidden = YES;
                
                GroupInfoAdminViewController *next2=[[GroupInfoAdminViewController alloc]init];
                next2.mydic=[self.copydic mutableCopy];
                next2.grpdict=[groupdict copy];
                next2.mydata2=self.mydata;
                //        CATransition* transition = [CATransition animation];
                //
                //        transition.duration = 0.4;
                //        transition.type = kCATransitionPush;
                //        transition.subtype = kCATransitionFade;
                //
                //        [[self navigationController].view.layer addAnimation:transition forKey:kCATransition];
                
                [self.navigationController pushViewController:next2 animated:YES];
                
                
                
            }
            if (indexPath.row==2)
            {
                //                groupstable.hidden=YES;
                //                [self addmember];
                UIDevice *device = [UIDevice currentDevice];
                if ([[device model] isEqualToString:@"iPhone"])
                {
                    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
                    if([MFMessageComposeViewController canSendText])
                    {
                        [self presentViewController:controller animated:YES completion:Nil];
                        controller.body = @"";
                        NSMutableArray *messageArray=[[NSMutableArray alloc]init];
                        messageArray = [NSMutableArray arrayWithArray:messagearr];
                        // NSString *smsstring= [NSString stringWithFormat:@"%@",[messagearr objectAtIndex:0]];
                        controller.recipients = [NSArray arrayWithArray:messageArray];
                        controller.messageComposeDelegate = self;
                        //[self presentModalViewController:controller animated:YES];
                    }
                }
                else
                {
                    alert=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                }
            }
            //            if (indexPath.row==3)
            //            {
            //                [self editgroup];
            //            }
            
            
        }
        
        else if ([isadmin isEqualToString:@"0"])
        {
            if (indexPath.row==0)
            {
                // [self search];
                
                GroupInfoMemberViewController *next=[[GroupInfoMemberViewController alloc]init];
                next.mydic=[self.copydic mutableCopy];
                next.grpdict=[groupdict copy];
                next.mydata2=self.mydata;
                //        CATransition* transition = [CATransition animation];
                //
                //        transition.duration = 0.4;
                //        transition.type = kCATransitionPush;
                //        transition.subtype = kCATransitionFade;
                //
                //        [[self navigationController].view.layer addAnimation:transition forKey:kCATransition];
                
                [self.navigationController pushViewController:next animated:YES];
                
            }
            
            if (indexPath.row==1)
            {
                
                mailComposer = [[MFMailComposeViewController alloc]init];
                mailComposer.mailComposeDelegate = self;
                NSMutableArray *recipentsArray=[[NSMutableArray alloc]init];
                recipentsArray = [NSMutableArray arrayWithArray:marr];
                [mailComposer setToRecipients:recipentsArray];
                
                [mailComposer setMessageBody:@"" isHTML:NO];
                [self.navigationController presentViewController:mailComposer animated:YES completion:nil];
            }
            if (indexPath.row==2)
            {
                //                groupstable.hidden=YES;
                //
                //                [self leavegroup];
                UIDevice *device = [UIDevice currentDevice];
                if ([[device model] isEqualToString:@"iPhone"])
                {
                    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
                    if([MFMessageComposeViewController canSendText])
                    {
                        [self presentViewController:controller animated:YES completion:Nil];
                        controller.body = @"";
                        NSMutableArray *messageArray=[[NSMutableArray alloc]init];
                        messageArray = [NSMutableArray arrayWithArray:messagearr];
                        // NSString *smsstring= [NSString stringWithFormat:@"%@",[messagearr objectAtIndex:0]];
                        controller.recipients = [NSArray arrayWithArray:messageArray];
                        controller.messageComposeDelegate = self;
                        //[self presentModalViewController:controller animated:YES];
                    }
                }
                else
                {
                    alert=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                }
                
                DebugLog(@"leave group clicked");
            }
            
        }
        
    }
    if (tableView.tag==3)
    {
        
        [invite setEnabled:YES];
        invite.tag=indexPath.row;
        searchview.hidden=YES;
        [searchtablefront scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
        searchfield.text=@"";
        // searchfield.s
        [searchfield resignFirstResponder];
        
        [search_result addObject:[array_copy objectAtIndex:invite.tag]];
        //DebugLog(@"copied array: %@",search_result);
        searchtableback.hidden=NO;
        [searchtableback reloadData];
        
    }
    if (tableView.tag==4)
    {
        
        groupstable.hidden=YES;
        //        ConDetailsViewController *conprf=[[ConDetailsViewController alloc]init];
        ConNewDetailsViewController *conprf=[[ConNewDetailsViewController alloc]init];
        conprf.userid=[[search_result valueForKey:@"id"] objectAtIndex:indexPath.row];
        conprf.response=@"yes";
        conprf.groupid=groupid;
        conprf.membercount=member_count;
        conprf.groupname=groupname;
        conprf.isadmin=isadmin;
        conprf.filter_arr=[search_result mutableCopy];
        
        conprf.p=3;
        
        //        CATransition* transition = [CATransition animation];
        //
        //        transition.duration = 0.4f;
        //        transition.type = kCATransitionPush;
        //        transition.subtype = kCATransitionFade;
        //
        //        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:conprf animated:YES];
    }
}



#pragma mark - mail compose delegate

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (result) {
        DebugLog(@"Result : %d",result);
    }
    if (error) {
        DebugLog(@"Error : %@",error);
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    expand=NO;
    backbutton.tag=0;
    //  mailback.backgroundColor = [UIColor clearColor];
}

//-(void)smsfunc
//{
//    //sms_btn.alpha=0.0f;
//
//    DebugLog(@"I Want to Send a SMS");
//    UIAlertView *smsAlert1;
//    if ([messagearr count] == 0)
//    {
//        smsAlert1 = [[UIAlertView alloc] initWithTitle:@"No Number found for this user!"
//                                               message:nil
//                                              delegate:self
//                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
//        smsAlert1.tag = 6;
//        [smsAlert1 show];
//    }
//    else
//    {
//        UIDevice *device = [UIDevice currentDevice];
//        if ([[device model] isEqualToString:@"iPhone"]) {
//
//            MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
//            if([MFMessageComposeViewController canSendText])
//            {
//                [self presentViewController:controller animated:YES completion:Nil];
//                controller.body = @"";
//                NSString *smsstring= [NSString stringWithFormat:@"%@",[messagearr objectAtIndex:0]];
//                controller.recipients = [NSArray arrayWithObjects:smsstring, nil];
//                controller.messageComposeDelegate = self;
//                //[self presentModalViewController:controller animated:YES];
//            }
//        }
//        else
//        {
//            smsAlert1=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            smsAlert1.tag = 66;
//            [smsAlert1 show];
//        }
//    }
//
//}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    if (result == MessageComposeResultCancelled) {
        
    } else if (result == MessageComposeResultSent) {
        
    }
    
}


-(void)navi
{
    //condetails=[[ConDetailsViewController alloc]init];
    condetails=[[ConNewDetailsViewController alloc]init];
    if (!filter)
    {
        id object = [[groupnames objectForKey:[NSNumber numberWithInteger:section]] objectAtIndex:row];
        NSString *userid = [object objectForKey:@"id"];
        NSString *isfriend = [object objectForKey:@"isFriend"];
        [[NSUserDefaults standardUserDefaults] setObject:isfriend forKey:@"isfriend"];
        [group_array removeAllObjects];
        DebugLog(@" id: %@",userid);
        condetails.membercount=member_count;
        condetails.userid=userid;
        condetails.response=@"yes";
        condetails.p=1;
    }
    else if (filter)
    {
        condetails.userid=[[filtered_arr valueForKey:@"id"] objectAtIndex:row1];
        condetails.response=@"yes";
        condetails.p=2;
        condetails.groupid=groupid;
        condetails.membercount=member_count;
        condetails.groupname=groupname;
        condetails.isadmin=isadmin;
        condetails.filter_arr=[filtered_arr mutableCopy];
        condetails.search=[group_array mutableCopy];
    }
    [self performSelectorOnMainThread:@selector(finalnavi) withObject:condetails waitUntilDone:NO];
    
    
}
-(void)finalnavi
{
    //    ConDetailsViewController *conprf=[[ConDetailsViewController alloc]init];
    condetails.groupid=groupid;
    DebugLog(@"GROUP GROUP ID:%@",condetails.groupid);
    
    //    CATransition* transition = [CATransition animation];
    //
    //    transition.duration = 0.4f;
    //    transition.type = kCATransitionPush;
    //    transition.subtype = kCATransitionFade;
    //
    //    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    
    [self.navigationController pushViewController:condetails animated:YES];
}

-(void)loaddata1
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *groups_url =[NSString stringWithFormat:@"https://budnav.com/ext/?action=search&q=%@&access_token=%@&device_id=%@",[st stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
    DebugLog(@"groups url string ------ %@",groups_url);
    
    NSError *error1=nil;
    data1=[NSData dataWithContentsOfURL:[NSURL URLWithString:groups_url]options:NSDataReadingUncached error:&error1];
    if (data1==nil)
    {
        DebugLog(@"no connnnn");
        alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        alert.tag=5;
        [alert show];
    }
    if (data1 != nil)
    {
        temp=[[NSDictionary alloc]init];
        temp=[NSJSONSerialization JSONObjectWithData:data1 //1
                                             options:kNilOptions
                                               error:&error1];
        DebugLog(@"-----Groups info ----- %@",temp);
    }
    [self performSelectorOnMainThread:@selector(datadisplay1) withObject:nil waitUntilDone:NO];
    
}

-(void)datadisplay1
{
    searchnames = [temp objectForKey:@"details"];
    NSLog(@"searchnames.count------  %lu",(unsigned long)searchnames.count);
    if (searchnames.count!=0)
    {
        array=[searchnames objectForKey:@"search"];
        hld=[[NSMutableArray alloc] init];
        array_copy=[array mutableCopy];
        DebugLog(@"aaaaa %lu",(unsigned long)array_copy.count);
        
        for (int i=0; i<array_copy.count; i++) {
            if ([[[array_copy objectAtIndex:i] objectForKey:@"isgroup"] intValue]!=1)
            {
                //hld=[[NSMutableArray alloc] init];
                [hld addObject:[array_copy objectAtIndex:i]];
                
            }
            
        }
        [array_copy removeAllObjects];
        array_copy=[hld mutableCopy];
        DebugLog(@"aaaaa %@",array_copy);
        [searchview addSubview:searchtablefront];
        [searchtablefront reloadData];
        searchview.hidden=NO;
        
        if ([array_copy count] ==0)
        {
            [array_copy removeAllObjects];
            [searchview addSubview:nouserlabel];
            searchtablefront.hidden=YES;
            nouserlabel.hidden=NO;
            backbutton.tag=5;
        }
        else
        {
            nouserlabel.hidden=YES;
            searchtablefront.hidden=NO;
            backbutton.tag=5;
        }
        
    }
    else if (searchnames.count==0)
    {
        [searchview addSubview:nouserlabel];
        searchtablefront.hidden=YES;
        nouserlabel.hidden=NO;
        backbutton.tag=5;
    }
    backbutton.tag=5;
    
}


#pragma mark- textfield delegates

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    filter=NO;
    return YES;
}

- (BOOL) textField: (UITextField *)theTextField shouldChangeCharactersInRange: (NSRange)range replacementString: (NSString *)string
{
    NSString *str=  [NSString stringWithFormat:@"%@",[theTextField.text stringByReplacingCharactersInRange:range withString:string]];
    //filter=NO;
    if ([str length]>0)
    {
        whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        st = [str stringByTrimmingCharactersInSet:whitespace];
        if ([st length]<1)
        {
            searchview.hidden=YES;
        }
        
        if ([st length]>2)
        {
            searchview.hidden=NO;
            [self loaddata1];
            //
        }
    }
    return YES;
}


- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    nouserlb.hidden=YES;
    groupstable.hidden=NO;
    filter=NO;
    searchview.hidden=YES;
    [searchfield resignFirstResponder];
    
    [groupstable reloadData];
    return YES;
    
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    searchview.hidden=YES;
    [textField resignFirstResponder];
    return YES;
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

#pragma mark-searchbar delegates
- (BOOL)  ShouldBeginEditing:(UISearchBar *)searchBarlocal
{
    searchBarlocal.showsCancelButton=YES;
    
    //searchBar.frame = CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width, 51);
    
    return YES;
}



- (BOOL)searchBarDidBeginEditing:(UISearchBar *)searchBarlocal
{
    searchBarlocal.showsCancelButton=YES;
    //    searchview.hidden=NO;
    [searchBarlocal becomeFirstResponder];
    [filtered_arr removeAllObjects];
    //[searchresults removeAllObjects];
    //    tab_contact.userInteractionEnabled=NO;
    //    tab_contact.scrollEnabled=NO;
    
    return YES;
}

- (BOOL)searchBarDidEndEditing:(UISearchBar *)searchBarlocal
{
    //[searchview removeFromSuperview];
    //    searchview.hidden=YES;
    searchBarlocal.showsCancelButton=YES;
    filter= NO;
    [searchBarlocal resignFirstResponder];
    groupstable.userInteractionEnabled=YES;
    groupstable.scrollEnabled=YES;
    backbutton.tag=5;
    //searchBarlocal.text=@"";
    return YES;
}

- (BOOL)searchBar:(UISearchBar *)searchBarlocal shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return YES;
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBarlocal
{
    
    searchBarlocal.showsCancelButton = NO;
    [searchBarlocal resignFirstResponder];
    //[searchview removeFromSuperview];
    groupstable.userInteractionEnabled=YES;
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBarlocal
{
    //[searchview removeFromSuperview];
    filter =NO;
    //searchview.hidden=YES;
    // DebugLog(@"abcdefghi");
    
    //searchBar.frame = CGRectMake(0, 20, 265, 51);
    //[searchview removeFromSuperview];
    
    [searchBarlocal resignFirstResponder];
    
    txtSearchField.layer.borderColor = [[UIColor clearColor]CGColor];
    
    searchBarlocal.text=@"";
    searchBarlocal.showsCancelButton=NO;
    searchview.hidden=YES;
    filter= NO;
    [groupstable reloadData];
    groupstable.hidden=NO;
    nouserlb.hidden=YES;
    groupstable.userInteractionEnabled=YES;
    groupstable.scrollEnabled=YES;
    
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    filter = NO;
    [filtered_arr removeAllObjects];
    //[searchresults removeAllObjects];
    DebugLog(@"search results %@",searchresults);
}

//- (BOOL)textFieldShouldClear:(UITextField *)textField {
//    //if we only try and resignFirstResponder on textField or searchBar,
//    //the keyboard will not dissapear (at least not on iPad)!
//    filter =NO;
//    [self performSelector:@selector(searchBarCancelButtonClicked:) withObject:searchBar afterDelay: 0.05];
//    return YES;
//}
- (void)searchBar:(UISearchBar *)searchBar2 textDidChange:(NSString *)searchText {
    
    //    searchview.hidden=NO;
    whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    st = [searchText stringByTrimmingCharactersInSet:whitespace];
    //Remove all objects first.
    //    [filtered_arr removeAllObjects];
    if (searchBar2.tag==1)
    {
        if ([st length] == 0)
        {
            [searchBar2 performSelector:@selector(resignFirstResponder) withObject:nil afterDelay:0];
            
            
            //            nouserlb.hidden=YES;
            //            groupstable.hidden=NO;
            //            NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
            //            filtered_arr=[[group_array sortedArrayUsingDescriptors:[NSMutableArray arrayWithObject:sort]] mutableCopy];
            //            //        [filtered_arr removeAllObjects];
            //            [groupstable reloadData];
        }
        
        
        if([st length] >= 1) {
            [filtered_arr removeAllObjects];
            [searchresults removeAllObjects];
            
            searchresults = [[NSMutableArray alloc]init];
            filter = YES;
            
            for (NSArray *array in group_array) {
                for (int i=0; i<[array count]; i++) {
                    [searchresults addObject:[array objectAtIndex:i]];
                }
                
            }
            
            NSPredicate *resultPredicate =[NSPredicate predicateWithFormat:@"(name CONTAINS[c] %@) OR (surname CONTAINS[c] %@)", searchText, searchText];
            //[NSPredicate  predicateWithFormat:@"name contains[cd] %@",searchText];
            //      [NSPredicate predicateWithFormat:@"(first CONTAINS[c] %@) OR (last CONTAINS[c] %@)", keyword, keyword]
            DebugLog(@"aa %@",group_array);
            filtered_arr=[[NSMutableArray alloc] init];
            
            //[searchresults addObjectsFromArray:[[NSSet setWithArray:group_array] allObjects]];
            DebugLog(@"SEARCH%@",searchresults);
            [filtered_arr addObjectsFromArray:[searchresults filteredArrayUsingPredicate:resultPredicate]];
            
            NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)];
            filtered_arr=[[filtered_arr sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]] mutableCopy];
            DebugLog(@"aaa %@",filtered_arr);
            [groupstable reloadData];
            
            if ([filtered_arr count] ==0)
            {
                [filtered_arr removeAllObjects];
                groupstable.hidden=YES;
                nouserlb.hidden=NO;
                backbutton.tag=2;
            }
            else
            {
                nouserlb.hidden=YES;
                groupstable.hidden=NO;
                backbutton.tag=2;
            }
            
        }
        else {
            filter = NO;
            nouserlb.hidden=YES;
            groupstable.hidden=NO;
            [searchresults removeAllObjects];
            [search_result removeAllObjects];
            [filtered_arr removeAllObjects];
            //  [self search];
            //[group_array removeAllObjects];
            //            NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
            //            filtered_arr=[[group_array sortedArrayUsingDescriptors:[NSMutableArray arrayWithObject:sort]] mutableCopy];
            // DebugLog(@"%@",filtered_arr);
            //        [filtered_arr removeAllObjects];
            [searchtableback reloadData];
            [searchtablefront reloadData];
            [groupstable reloadData];
            DebugLog(@"Search DATA:%@",searchresults);
            DebugLog(@"Search DATA:%@",search_result);
        }
        //    tab_contact.userInteractionEnabled=NO;
        //    tab_contact.scrollEnabled=NO;
    }
    
    
}



-(void)goback:(UIButton *)backbutton1
{
    //DebugLog(@"backbutton.tag==%ld",(long) backbutton1.tag);
    
    if (backbutton1.tag==0)
    {
        //backbutton.backgroundColor=[UIColor lightGrayColor];
        //        groupview=[[ConGroupsViewController alloc] init];
        //        [self.navigationController pushViewController:groupview animated:NO];
        //        self.tabBarController.tabBar.hidden = NO;
        //        [self.navigationController popViewControllerAnimated:NO];
        //        NSInteger index = -1;
        //        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
        //        UINavigationController *navigationController = [appDelegate navigationController];
        //        NSArray* arr = [[NSArray alloc] initWithArray:navigationController.viewControllers];
        //        for(int i=0 ; i<[arr count] ; i++)
        //        {
        //            if([[arr objectAtIndex:i] isKindOfClass:NSClassFromString(@"ConGroupsViewController")])
        //            {
        //                index = i;
        //            }
        //        }
        //        [self.navigationController popToViewController:[arr objectAtIndex:index] animated:YES];
        for (id controller in [self.navigationController viewControllers])
        {
            
            if ([controller isKindOfClass:[ConGroupsViewController class]])
            {
                
                //                CATransition *transition = [CATransition animation];
                //
                //                transition.duration = 0.5f;
                //
                //                transition.type = kCATransitionFade;
                //
                //                [[self navigationController].view.layer addAnimation:transition forKey:nil];
                
                
                @try {
                    [self.navigationController popToViewController:controller animated:YES];
                    break;
                }
                @catch (NSException * e)
                {
                    DebugLog(@"Exception: %@", e);
                    break;
                    
                }
                @finally {
                    
                }
            }
            else
            {
                //                CATransition *transition = [CATransition animation];
                //
                //                transition.duration = 0.5f;
                //
                //                transition.type = kCATransitionFade;
                //
                //                [[self navigationController].view.layer addAnimation:transition forKey:nil];
                [self.navigationController popToViewController:controller animated:YES];
                break;
            }
        }
        
        DebugLog(@"Break For");
        //[self gotoGroup];
    }
    
    else if (backbutton1.tag==1)
    {
        [popupview removeFromSuperview];
        change.backgroundColor=[UIColor clearColor];
        backbutton.tag=0;
        if (expand) {
            expand=NO;
        }
        else if (!expand)
        {
            expand=YES;
        }
    }
    else if (backbutton1.tag==2)
    {
        ind = 0;
        [searchBar resignFirstResponder];
        [group_array removeAllObjects];
        searchBar.hidden=YES;
        back_label.alpha = 1.0f;
        backimage.alpha = 1.0f;
        membercount.hidden=NO;
        group_namelbl.hidden=NO;
        groupimage.hidden=NO;
        groupstable.tag=1;
        filter=NO;
        expand=NO;
        moreImage.hidden = NO;
        menu.hidden=NO;
        popupview.hidden=NO;
        check_search=@"NO";
        nouserlb.hidden=YES;
        groupstable.hidden=NO;
        [filtered_arr removeAllObjects];
        [search_result removeAllObjects];
        [searchresults removeAllObjects];
        [groupstable reloadData];
        backbutton.tag=0;
        change.frame=CGRectMake(0,18,70,52);
        change.backgroundColor=[UIColor clearColor];
        backbutton.frame = CGRectMake(0,0,groupimage.frame.size.width+groupimage.frame.origin.x+10,52);
    }
    else if(backbutton1.tag==5)
    {
        h=0;
        //DebugLog(@"backbutton.tag==5");
        //[searchview removeFromSuperview];
        DebugLog(@"member count2%@",member_count);
        //searchtablefront.hidden=YES;
        
        backbutton.hidden=NO;
        backbutton.tag=0;
        back_label.alpha = 1.0f;
        backimage.alpha = 1.0f;
        
        mainview = [[UIView alloc]initWithFrame:CGRectMake(0,65, self.view.frame.size.width, self.view.frame.size.height -72)];
        groupstable.frame=CGRectMake(0,0,mainview.frame.size.width,mainview.frame.size.height);
        //mainview.backgroundColor=[UIColor redColor];
        [self.view addSubview:mainview];
        [mainview addSubview:groupstable];
        
        [invite setEnabled:YES];
        
        [search_result removeAllObjects];
        
        searchfield.hidden=YES;
        grayView.hidden=YES;
        invite.hidden=YES;
        back_label.hidden=YES;
        bigbackbutton.hidden=YES;
        searchtableback.hidden=YES;
        searchview.hidden=YES;
        lbdivider.hidden = YES;
        expand=NO;
        
        moreImage.hidden=NO;
        menu.hidden=NO;
        //[menu setBackgroundImage:[UIImage imageNamed:@"menu_profile"] forState:UIControlStateDisabled];
        //        [menu setBackgroundImage:[UIImage imageNamed:@"menu-icon@2x.png"] forState:UIControlStateNormal];
        menu.enabled=NO;
        membercount.hidden=NO;
        lineDiv.hidden=NO;
        groupstable.hidden=NO;
        groupimage.hidden=NO;
        menu.hidden=NO;
        group_namelbl.hidden=NO;
        
        //popupview.hidden=NO;
        [searchfield resignFirstResponder];
        
        check_member=@"NO";
        [groupstable reloadData];
    }
}

-(void)gotoGroup
{
    ConGroupsViewController *CGVC = [[ConGroupsViewController alloc] init];
    [self.navigationController pushViewController:CGVC animated:YES];
}

-(void)addmember
{
    h=2;
    moreImage.hidden=YES;
    popupview.hidden=YES;
    backbutton.tag=5;
    backbutton.hidden=YES;
    lineDiv.hidden=YES;
    membercount.hidden=YES;
    DebugLog(@"member count3%@",member_count);
    //expand=YES;
    bigbackbutton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 150, 64)];
    bigbackbutton.tag=backbutton.tag;
    bigbackbutton.backgroundColor=[UIColor clearColor];
    [bigbackbutton setTitle:@"" forState:UIControlStateNormal];
    
    [bigbackbutton addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDown];
    [bigbackbutton addTarget:self action:@selector(changecoloragain) forControlEvents:UIControlEventTouchDragExit];
    [bigbackbutton addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDragEnter];
    
    [bigbackbutton addTarget:self action:@selector(goback:) forControlEvents:UIControlEventTouchUpInside];
    [blackBack addSubview:bigbackbutton];
    
    back_label=[[UILabel alloc]initWithFrame:CGRectMake(31, 28, 58, 30)];
    back_label.text=@"Back";
    back_label.font=[UIFont fontWithName:@"ProximaNova-Regular" size:20.0];
    back_label.textColor=[UIColor whiteColor];
    [blackBack addSubview:back_label];
    
    groupimage.hidden=YES;
    group_namelbl.hidden=YES;
    menu.hidden=YES;
    groupstable.hidden=YES;
    
    mainview = [[UIView alloc]initWithFrame:CGRectMake(0,123, self.view.frame.size.width, self.view.frame.size.height-123)];
    mainview.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:mainview];
    
    grayView=[[UIView alloc]initWithFrame:CGRectMake(0, 65, self.view.frame.size.width, 51)];
    //    grayView.backgroundColor=[UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1.0];
    grayView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:grayView];
    
    UIView *addView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 11, 31)];;
    
    //    searchBar1 = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
    //    searchBar1.delegate=self;
    //
    //    searchBar1.tintColor=[UIColor darkGrayColor];
    //    searchBar1.backgroundColor=[UIColor whiteColor];
    //    [searchBar1 setSearchBarStyle:UISearchBarStyleMinimal];
    //    //    searchBar.barTintColor= [UIColor colorWithRed:238.0f/255.0f green:238.0f/255.0f blue:238.0f/255.0f alpha:1];
    //    searchBar.barTintColor = [UIColor whiteColor];
    //
    //    UILabel *lbdivider = [[UILabel alloc]initWithFrame:CGRectMake(0, searchBar1.frame.origin.y + searchBar1.frame.size.height, self.view.frame.size.width, 0.6f)];
    //    [grayView addSubview:lbdivider];
    //    lbdivider.backgroundColor = [UIColor colorWithRed:238.0f/255.0f green:238.0f/255.0f blue:238.0f/255.0f alpha:1];
    //
    //
    //    //searchBar.placeholder= @"Search new contacts";
    //    UITextField *searchField = [searchBar valueForKey:@"_searchField"];
    //    //    searchField.frame = CGRectMake(10, 10, [UIScreen mainScreen].bounds.size.width, 50);
    //    searchField.textColor = [UIColor colorWithRed:(0.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0f];
    //    searchField.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f];
    //
    //    NSAttributedString *str = [[NSAttributedString alloc] initWithString:@"Search contacts" attributes:@{ NSForegroundColorAttributeName : [UIColor blueColor]}];//colorWithRed:(0.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:0.7f] }];
    //    searchField.attributedPlaceholder = str;
    //    searchBar1.keyboardAppearance=UIKeyboardAppearanceDark;
    //    [grayView addSubview:searchBar1];
    //    searchBar1.userInteractionEnabled = YES;
    //    searchBar1.searchTextPositionAdjustment = UIOffsetMake(10, 0);
    
    searchfield = [[UITextField alloc] initWithFrame:CGRectMake(10, 10, [UIScreen mainScreen].bounds.size.width-20, 31)];
    searchfield.delegate=self;
    searchfield.autocorrectionType=UITextAutocorrectionTypeNo;
    searchfield.tintColor=[UIColor darkGrayColor];
    //    searchfield.backgroundColor=[UIColor colorWithRed:41.0f/255.0f green:42.0f/255.0f blue:46.0f/255.0f alpha:1.0f];
    //    searchfield.placeholder= @"Search Contacts";
    //searchField.textColor = [UIColor colorWithRed:(0.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0f];
    searchfield.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f];
    // searchfield.backgroundColor = [UIColor whiteColor];
    searchfield.keyboardAppearance=UIKeyboardAppearanceDark;
    searchfield.layer.cornerRadius=5;
    searchfield.clearButtonMode=UITextFieldViewModeWhileEditing;
    searchfield.leftView=addView;
    searchfield.leftViewMode=UITextFieldViewModeAlways;
    searchfield.font=[UIFont fontWithName:@"ProximaNova-Regular" size:14];
    searchfield.textColor = [UIColor colorWithRed:(0.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0f];
    //searchfield.backgroundColor = [UIColor colorWithRed:(43.0f/255.0f) green:(44.0f/255.0f) blue:(48.0f/255.0f) alpha:1.0f];
    
    lbdivider = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 0.6f)];
    [mainview addSubview:lbdivider];
    lbdivider.backgroundColor = [UIColor colorWithRed:238.0f/255.0f green:238.0f/255.0f blue:238.0f/255.0f alpha:1];
    
    NSAttributedString *str = [[NSAttributedString alloc] initWithString:@"Search Contacts" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:(0.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:0.8f] }];
    searchfield.attributedPlaceholder = str;
    //    searchfield.keyboardAppearance=UIKeyboardAppearanceDark;
    
    [grayView addSubview:searchfield];
    searchfield.tag=2;
    
    
    //    mainview.backgroundColor=[UIColor whiteColor];
    //    //    mainview.layer.zPosition=3;
    //    [self.view addSubview:mainview];
    
    
    invite=[[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width-70, 29, 65, 30)];
    [invite setTitle:@"Invite" forState:UIControlStateNormal];
    invite.titleLabel.font=[UIFont fontWithName:@"ProximaNova-Regular" size:20.0];
    if (search_result.count==0) {
        [invite setEnabled:NO];
    }
    
    
    [invite addTarget:self action:@selector(invite:) forControlEvents:UIControlEventTouchUpInside];
    [blackBack addSubview:invite];
    
    //    searchtableback=[[UITableView alloc]init];
    //    searchtableback.tag=4;
    //    searchtableback.frame=CGRectMake(0, 51, 360, 480);
    //    [mainview addSubview:searchtableback];
    //groupstable.hidden=YES;
    
    //[groupstable reloadData];
    searchtableback=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, mainview.frame.size.height)];
    searchtableback.delegate=self;
    searchtableback.dataSource=self;
    searchtableback.tag=4;
    searchtableback.separatorStyle=UITableViewCellSeparatorStyleNone;
    searchtableback.backgroundColor=[UIColor clearColor];
    searchtableback.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    //searchtableback.hidden=YES;
    [mainview addSubview:searchtableback];
    
    searchview=[[UIView alloc]initWithFrame:CGRectMake(10, 0, mainview.frame.size.width-20, 162)];
    searchview.backgroundColor=[[UIColor whiteColor]colorWithAlphaComponent:1.0];
    searchview.opaque=NO;
    searchview.layer.cornerRadius=0;
    searchview.layer.shadowOpacity=0.8;
    searchview.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    searchview.hidden=YES;
    
    
    searchtablefront=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, searchview.frame.size.width, searchview.frame.size.height)];
    searchtablefront.delegate=self;
    searchtablefront.dataSource=self;
    searchtablefront.tag=3;
    searchtablefront.backgroundColor=[UIColor clearColor];
    searchtablefront.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    searchtablefront.separatorStyle = UITableViewCellSeparatorStyleNone;
    searchtablefront.hidden=YES;
    
    nouserlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 30, [UIScreen mainScreen].bounds.size.width, 30)];
    nouserlabel.text=@"No results";
    nouserlabel.backgroundColor=[UIColor clearColor];
    nouserlabel.textAlignment=NSTextAlignmentCenter;
    nouserlabel.textColor=[UIColor blackColor];//colorWithRed:238.0f/255.0f green:238.0f/255.0f blue:238.0f/255.0f alpha:1];
    nouserlabel.hidden=YES;
    
    [mainview addSubview:searchview];
}


-(void)cross:(UIButton *)sender
{
    //DebugLog(@"index path %ld %@",sender.tag,search_result);
    [search_result removeObjectAtIndex:sender.tag];
    //[[groupnames valueForKey:@"surname"] removeObjectAtIndex:sender.tag];
    //[self.numbers removeObjectAtIndex:indexPath.row];
    // Delete the row from the data source
    NSIndexPath *indexpath=[NSIndexPath indexPathForRow:sender.tag inSection:0];
    [searchtableback beginUpdates];
    [searchtableback deleteRowsAtIndexPaths:[[NSArray alloc] initWithObjects:indexpath, nil] withRowAnimation:
     UITableViewRowAnimationFade];
    // DebugLog(@"index path %ld %@",sender.tag,search_result);
    [searchtableback endUpdates];
    [searchtableback reloadData];
    if (search_result.count==0) {
        [invite setEnabled:NO];
    }    //    [groupstable endUpdates];
    //[groupstable deleteRowsAtIndexPaths:@[indexpath] withRowAnimation:UITableViewRowAnimationAutomatic];
    //[self performSelector:@selector(tableView:commitEditingStyle:forRowAtIndexPath:) withObject:indexpath];
    //[groupstable reloadData];
}

-(void)invite:(id)sender
{
    [lbdivider removeFromSuperview];
    //    [group_array addObject:[array_copy objectAtIndex:invite.tag]];
    //    searchview.hidden=YES;
    //    groupstable.hidden=NO;
    //    [groupstable reloadData];
    
    // [[NSOperationQueue mainQueue] addOperationWithBlock:^{
    [UIView animateWithDuration:0.0 animations:^{
        
        activity=[[UIActivityIndicatorView alloc] init];
        activity.center = self.view.center;
        [activity startAnimating];
        activity.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        [activity setColor:[UIColor blackColor]];
        [mainview addSubview:activity];
        
        
    }
                     completion:^(BOOL finished)
     {
         
         bigbackbutton.tag=backbutton.tag;
         NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
         array_id =[[search_result valueForKey:@"id"] mutableCopy];
         DebugLog(@"123654789");
         DebugLog(@"member count1%@",member_count);
         
         NSString *str=[array_id componentsJoinedByString:@","];
         DebugLog(@"string append %@",str);
         NSString *groups_url =[NSString stringWithFormat:@"https://budnav.com/ext/?action=group-request&group_id=%@&user_id=%@&access_token=%@&device_id=%@",groupid,str,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
         DebugLog(@"groups url string ------ %@",groups_url);
         
         NSError *error1=nil;
         @try {
             
             NSData *data2=[NSData dataWithContentsOfURL:[NSURL URLWithString:groups_url]options:NSDataReadingUncached error:&error1];
             if (data2==nil)
             {
                 DebugLog(@"no connnnn");
                 alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                                    message:nil
                                                   delegate:self
                                          cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                 alert.tag=5;
                 [alert show];
                 
                 
                 
             }
             
             if (data2 != nil)
             {
                 grpdict=[[NSDictionary alloc] init];
                 grpdict=[NSJSONSerialization JSONObjectWithData:data2 //1
                                                         options:kNilOptions
                                                           error:&error1];
                 DebugLog(@"-----Groups info123456  ----- %@",grpdict);
                 
                 NSString *check=[NSString stringWithFormat:@"%@",[grpdict valueForKey:@"success"]];
                 if ([check isEqualToString:@"1"])
                 {
                     [activity removeFromSuperview];
                     if ([[grpdict valueForKey:@"details"] valueForKeyPath:@"type"])
                     {
                         NSString *type=[NSString stringWithFormat:@"%@",[[grpdict valueForKey:@"details"] valueForKeyPath:@"type"]];
                         if ([type isEqualToString:@"add"]||[type isEqualToString:@"request"])
                         {
                             [activity removeFromSuperview];
                             if (isAtLeast8)
                             {
                                 UIAlertController * alert4=   [UIAlertController
                                                                alertControllerWithTitle:@"Action"
                                                                message:@"Request sent"
                                                                preferredStyle:UIAlertControllerStyleAlert];
                                 
                                 
                                 //UIAlertAction* cancel = [UIAlertAction
                                 //                                                 actionWithTitle:@"Ok"
                                 //                                                 style:UIAlertActionStyleCancel
                                 //                                                 handler:^(UIAlertAction * action)
                                 //                                                 {
                                 //                                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                 //
                                 //                                                 }];
                                 //
                                 //[alert addAction:cancel];
                                 
                                 [self presentViewController:alert4 animated:YES completion:nil];
                                 [self performSelector:@selector(newfunction) withObject:nil afterDelay:1.0];
                                 //[self newfunction];
                             }
                             //
                             if (isAtLeast7)
                             {
                                 //UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Action" message:@"Request sent" delegate:self cancelButtonTitle:nil otherButtonTitles:nil, nil];
                                 alert=[[UIAlertView alloc]initWithTitle:@"Action" message:@"Request sent" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                 
                                 [alert show];
                             }
                             
                         }
                     }
                     else
                     {
                         [activity stopAnimating];
                         if (isAtLeast8)
                         {
                             UIAlertController * alert5=   [UIAlertController
                                                            alertControllerWithTitle:@"Action"
                                                            message:@"Request sent"
                                                            preferredStyle:UIAlertControllerStyleAlert];
                             
                             //                        UIAlertAction* cancel = [UIAlertAction
                             //                                                                         actionWithTitle:@"Ok"
                             //                                                                         style:UIAlertActionStyleCancel
                             //                                                                         handler:^(UIAlertAction * action)
                             //                                                                         {
                             //                                                                             [alert dismissViewControllerAnimated:YES completion:nil];
                             //
                             //                                                                         }];
                             //
                             //                        [alert addAction:cancel];
                             
                             [self presentViewController:alert5 animated:YES completion:nil];
                             [self performSelector:@selector(newfunction) withObject:nil afterDelay:1.0];
                             //[self newfunction];
                         }
                         //
                         if (isAtLeast7)
                         {
                             //UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Action" message:@"Request sent" delegate:self cancelButtonTitle:nil otherButtonTitles:nil, nil];
                             alert=[[UIAlertView alloc]initWithTitle:@"Action" message:@"Request sent" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                             
                             [alert show];
                         }
                         
                     }
                     
                     
                     
                     
                     //                [self newfunction];//goback:(UIButton *)backbutton];
                 }
                 else
                 {
                     [activity removeFromSuperview];
                     if (isAtLeast8)
                     {
                         UIAlertController * alert6=   [UIAlertController
                                                        alertControllerWithTitle:@"Action"
                                                        message:[NSString stringWithFormat:@"%@",[grpdict valueForKey:@"error"]]
                                                        preferredStyle:UIAlertControllerStyleAlert];
                         
                         //                        UIAlertAction* cancel = [UIAlertAction
                         //                                                                         actionWithTitle:@"Ok"
                         //                                                                         style:UIAlertActionStyleCancel
                         //                                                                         handler:^(UIAlertAction * action)
                         //                                                                         {
                         //                                                                             [alert dismissViewControllerAnimated:YES completion:nil];
                         //
                         //                                                                         }];
                         //
                         //                        [alert addAction:cancel];
                         
                         [self presentViewController:alert6 animated:YES completion:nil];
                         [self performSelector:@selector(newfunction) withObject:nil afterDelay:3.0];
                         //[self newfunction];
                     }
                     //
                     if (isAtLeast7)
                     {
                         //UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Action" message:@"Request sent" delegate:self cancelButtonTitle:nil otherButtonTitles:nil, nil];
                         alert=[[UIAlertView alloc]initWithTitle:@"Action" message:[NSString stringWithFormat:@"%@",[grpdict valueForKey:@"error"]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                         
                         [alert show];
                     }
                 }
                 
                 
             }
         }
         @catch (NSException *exception) {
             //DebugLog(@"exception %@",exception);
         }
         
     }
     ];
    
    
    
    // }];
    
    
    
}
#pragma mark- alerview delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"Ok"])
    {
        [self newfunction];
    }
}

-(void)newfunction
{
    h=0;
    [activity removeFromSuperview];
    //DebugLog(@"backbutton.tag==5");
    //[searchview removeFromSuperview];
    [self dismissViewControllerAnimated:NO completion:nil];
    //searchtablefront.hidden=YES;
    backbutton.tag=0;
    moreImage.hidden=NO;
    mainview = [[UIView alloc]initWithFrame:CGRectMake(0,65, self.view.frame.size.width, [UIScreen mainScreen].bounds.size.height)];
    groupstable.frame=CGRectMake(0,0,mainview.frame.size.width,mainview.frame.size.height);
    //mainview.backgroundColor=[UIColor redColor];
    [self.view addSubview:mainview];
    [mainview addSubview:groupstable];
    
    //[invite setEnabled:YES];
    
    [search_result removeAllObjects];
    
    searchfield.hidden=YES;
    grayView.hidden=YES;
    invite.hidden=YES;
    back_label.hidden=YES;
    bigbackbutton.hidden=YES;
    searchtableback.hidden=YES;
    searchview.hidden=YES;
    expand=NO;
    
    groupimage.contentMode= UIViewContentModeScaleAspectFill;
    groupimage.image=[UIImage imageWithData:decodedData1];
    groupimage.hidden=NO;
    membercount.hidden=NO;
    lineDiv.hidden=NO;
    groupstable.hidden=NO;
    groupimage.hidden=NO;
    menu.hidden=NO;
    group_namelbl.hidden=NO;
    backbutton.hidden=NO;
    
    //popupview.hidden=NO;
    [searchfield resignFirstResponder];
    
    check_member=@"NO";
    [groupstable reloadData];
    
}

-(void)leavegroup
{
    activity=[[UIActivityIndicatorView alloc] init];
    activity.center = self.view.center;
    [activity startAnimating];
    activity.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [activity setColor:[UIColor blackColor]];
    [mainview addSubview:activity];
    
    [UIView animateWithDuration:0 animations:^{
        
        expand = NO;
        popupview.hidden=YES;
        [SVProgressHUD showWithStatus:@"Procesing......"];
        
    }completion:^(BOOL finished) {
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *groups_url =[NSString stringWithFormat:@"https://budnav.com/ext/?action=group-leave&group_id=%@&access_token=%@&device_id=%@",groupid,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
        DebugLog(@"groups url string ------ %@",groups_url);
        
        NSError *error1=nil;
        
        NSData *data2=[NSData dataWithContentsOfURL:[NSURL URLWithString:groups_url]];
        
        //        NSData *data2=[NSData dataWithContentsOfURL:[NSURL URLWithString:groups_url]options:NSDataReadingUncached error:&error1];
        if (data2==nil)
        {
            DebugLog(@"no connnnn");
            alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            alert.tag=5;
            [alert show];
            
            
            
        }
        else
        {
            groupdictionary=[NSJSONSerialization JSONObjectWithData:data2 //1
                                                            options:kNilOptions
                                                              error:&error1];
            
            DebugLog(@"Leave Group:%@",groupdictionary);
            
            
            NSString *check=[NSString stringWithFormat:@"%@",[groupdictionary valueForKey:@"success"]];
            if ([check isEqualToString:@"1"])
            {
                alert = [[UIAlertView alloc] initWithTitle:@"You have left the group successfully!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
                [SVProgressHUD dismiss];
                
                //                CATransition *transition = [CATransition animation];
                //
                //                transition.duration = 0.5f;
                // transition.type = kCATransitionPush;
                //                transition.type = kCATransitionFade;
                // transition.subtype = kCATransitionFromLeft;
                
                //                [[self navigationController].view.layer addAnimation:transition forKey:nil];
                [self.navigationController popViewControllerAnimated:YES];
            }
            
            else if ([check isEqualToString:@"0"])
            {
                if (isAtLeast8)
                {
                    UIAlertController * alert1=   [UIAlertController
                                                   alertControllerWithTitle:@"Warning"
                                                   message:[NSString stringWithFormat:@"%@",[groupdictionary valueForKey:@"error"]]
                                                   preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* cancel = [UIAlertAction
                                             actionWithTitle:@"Ok"
                                             style:UIAlertActionStyleDefault
                                             handler:^(UIAlertAction * action)
                                             {
                                                 [alert1 dismissViewControllerAnimated:YES completion:nil];
                                                 
                                             }];
                    
                    [alert1 addAction:cancel];
                    
                    [self presentViewController:alert1 animated:YES completion:nil];
                }
                
                if (isAtLeast7)
                {
                    alert=[[UIAlertView alloc]initWithTitle:@"Warning" message:[NSString stringWithFormat:@"%@",[groupdictionary valueForKey:@"error"]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    
                    [alert show];
                }
                
            }
            
        }
        
        
    }];
    
    
    // @try {
    //    }
    //    @catch (NSException *exception) {
    //        //DebugLog(@"exception %@",exception);
    //    }
    //    NSString *check=[NSString stringWithFormat:@"%@",[groupdictionary valueForKey:@"success"]];
    //    if ([check isEqualToString:@"1"])
    //    {
    //        [self.navigationController popViewControllerAnimated:NO];
    //    }
    //
    //    else if ([check isEqualToString:@"0"])
    //    {
    //        if (isAtLeast8)
    //        {
    //            UIAlertController * alert=   [UIAlertController
    //                                          alertControllerWithTitle:@"Warning"
    //                                          message:[NSString stringWithFormat:@"%@",[groupdictionary valueForKey:@"error"]]
    //                                          preferredStyle:UIAlertControllerStyleAlert];
    //
    //            UIAlertAction* cancel = [UIAlertAction
    //                                     actionWithTitle:@"Ok"
    //                                     style:UIAlertActionStyleDefault
    //                                     handler:^(UIAlertAction * action)
    //                                     {
    //                                         [alert dismissViewControllerAnimated:YES completion:nil];
    //
    //                                     }];
    //
    //            [alert addAction:cancel];
    //
    //            [self presentViewController:alert animated:YES completion:nil];
    //        }
    //
    //        if (isAtLeast7)
    //        {
    //            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Warning" message:[NSString stringWithFormat:@"%@",[groupdictionary valueForKey:@"error"]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    //
    //            [alert show];
    //        }
    //
    //    }
    
    
}

-(void)search
{
    [popupview removeFromSuperview];
    
    change.frame=CGRectMake(0,18,24,52);
    
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(30, 18, [UIScreen mainScreen].bounds.size.width-54, 49)];
    searchBar.delegate=self;
    searchBar.tintColor=[UIColor whiteColor];
    //    searchBar.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:1.0f];
    searchBar.barTintColor= [UIColor clearColor];
    searchBar.backgroundImage = [UIImage new];
    searchBar.keyboardAppearance=UIKeyboardAppearanceDark;
    //searchBar.placeholder= @"Search in group";
    searchBar.tag=1;
    //    [[UILabel appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor colorWithRed:(213.0f/255.0f) green:(213.0f/255.0f) blue:(214.0f/255.0f) alpha:1.0f]];
    
    //    UITextField *searchField;
    //    NSUInteger numViews = [searchBar.subviews count];
    //    for(int i = 0; i < numViews; i++) {
    //        if([[searchBar.subviews objectAtIndex:i] isKindOfClass:[UITextField class]]) { //conform?
    //            searchField = [searchBar.subviews objectAtIndex:i];
    //        }
    //    }
    //    if(!(searchField == nil)) {
    //        searchField.textColor = [UIColor whiteColor];
    //        [searchField setBackground: [UIImage imageNamed:@"yourImage"]];//just add here gray image which you display in quetion
    //        [searchField setBorderStyle:UITextBorderStyleNone];
    //    }
    [searchBar setImage:[UIImage imageNamed:@"searchIcon"]
       forSearchBarIcon:UISearchBarIconSearch
                  state:UIControlStateNormal];
    
    UITextField *searchField = [searchBar valueForKey:@"_searchField"];
    searchField.textColor = [UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:1.0f];
    searchField.backgroundColor = [UIColor colorWithRed:(20.0f/255.0f) green:(102.0f/255.0f) blue:(110.0f/255.0f) alpha:1.0f];
    
    NSAttributedString *str = [[NSAttributedString alloc] initWithString:@"Search in group" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:(153.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f] }];
    searchField.attributedPlaceholder = str;
    //    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:1.0f]];
    
    //    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setBackgroundColor:[UIColor grayColor]];
    //    if (![check_search isEqualToString:@"YES"])
    //    {
    moreImage.hidden = YES;
    menu.hidden=YES;
    [blackBack addSubview:searchBar];
    //    }
    
    searchBar.hidden=NO;
    group_namelbl.hidden=YES;
    groupimage.hidden=YES;
    membercount.hidden=YES;
    backbutton.tag=2;
    backbutton.backgroundColor = [UIColor clearColor];
    backbutton.frame=CGRectMake(0, backimage.frame.origin.y-5, searchBar.frame.origin.x, 64-backimage.frame.origin.y+5);
}


-(void)editgroup
{
    ConGroupSettingsViewController *congrpset=[[ConGroupSettingsViewController alloc]init];
    congrpset.group_id=groupid;
    congrpset.groupname=groupname;
    congrpset.membercount=member_count;
    backbutton.tag=0;
    popupview.hidden=YES;
    expand=NO;
    congrpset.adminname=name;
    DebugLog(@"admin name: %@",congrpset.adminname);
    //DebugLog(@"12345: %@",[group_array valueForKey:@"name"]);
    
    
    //    CATransition* transition = [CATransition animation];
    //
    //    transition.duration = 0.3;
    //    transition.type = kCATransitionPush;
    //    transition.subtype = kCATransitionFade;
    //    transition.subtype = kCATransitionFromRight;
    //    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
    [self.navigationController pushViewController:congrpset animated:NO];
    //    NSString *groups_url =[NSString stringWithFormat:@"https://budnav.com/ext/?action=group-edit&group_id=%@&access_token=%@&device_id=%@",groupid,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
    //    DebugLog(@"groups url string ------ %@",groups_url);
    //
    //    NSError *error1=nil;
    //    @try {
    //        NSData *data1=[NSData dataWithContentsOfURL:[NSURL URLWithString:groups_url]options:NSDataReadingUncached error:&error1];
    //        if (data1 != nil)
    //            groupdict=[NSJSONSerialization JSONObjectWithData:data1 //1
    //                                                      options:kNilOptions
    //                                                        error:&error1];
    //        DebugLog(@"-----Groups info ----- %@",groupdict);
    //    }
    //    @catch (NSException *exception) {
    //        DebugLog(@"exception %@",exception);
}

-(void)changecoloragain
{
    DebugLog(@"CHANGE COLOR");
    //    change.backgroundColor=[UIColor clearColor];
    back_label.alpha = 1.0f;
    backimage.alpha = 1.0f;
    //  group_namelbl.alpha=1.0f;
}

-(void)changecolor
{
    DebugLog(@"CHANGE COLOR1");
    //    change.backgroundColor=[UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0];
    back_label.alpha = 0.5f;
    backimage.alpha = 0.5f;
    // group_namelbl.alpha=0.5f;
    
}

-(void)btnchangecolor
{
    // group_namebtn.alpha=0.5f;
    group_namelbl.alpha = 0.5f;
    membercount.alpha = 0.5f;
}
-(void)btnchangecoloragain
{
    // group_namebtn.alpha=1.0f;
    group_namelbl.alpha = 1.0f;
    membercount.alpha = 1.0f;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)handleLongPress:(UILongPressGestureRecognizer *)longPress
{
    if (longPress.state == UIGestureRecognizerStateBegan) {
        rem_row = (int)longPress.view.tag;
        
        DebugLog(@"cell tag value:%d",(int)rem_row);
        
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select an option" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Remove member" otherButtonTitles:@"Make group admin", nil];
        
        
        //        UIButton *btn = [[actionSheet subviews] objectAtIndex:0];
        //        btn.titleLabel.textColor = [UIColor redColor];
        // UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select a option"
        //                                                          delegate:self
        //                                                cancelButtonTitle:@"Cancel"
        //                                                otherButtonTitles:@"make group admin", @"remove member", nil];
        
        
        actionSheet.tag = 101;
        //[self changeTextColorForUIActionSheet:actionSheet];
        
        [actionSheet showInView:mainview];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //    [actionSheet removeFromSuperview];
    if (actionSheet.tag == 101){
        
        if (buttonIndex == 0) {
            [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
            DebugLog(@"Make Admin");
            [self remove_member:(rem_row)];
        }
        else if (buttonIndex == 1)
        {
            [actionSheet dismissWithClickedButtonIndex:1 animated:YES];
            DebugLog(@"Remove From Group");
            [self makeAdmin:(rem_row)];
        }
        else{
            DebugLog(@"dismissWithClickedButtonIndex");
            [actionSheet dismissWithClickedButtonIndex:2 animated:YES];
        }
        
    }
    
}

-(void)makeAdmin:(int)userId
{
    NSString *userid = [NSString stringWithFormat:@"%d",userId];
    groupdict=[[NSDictionary alloc]init];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *groups_url =[NSString stringWithFormat:@"https://budnav.com/ext/?action=group-changeadmin&group_id=%@&user_id=%@&access_token=%@&device_id=%@",groupid,userid,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
    DebugLog(@"groups url string ------ %@",groups_url);
    
    NSError *error1=nil;
    @try
    {
        NSData *data12=[NSData dataWithContentsOfURL:[NSURL URLWithString:groups_url]options:NSDataReadingUncached error:&error1];
        if (data12 != nil)
            groupdict=[NSJSONSerialization JSONObjectWithData:data12 //1
                                                      options:kNilOptions
                                                        error:&error1];
        DebugLog(@"-----Groups info ----- %@",groupdict);
        check1=[NSString stringWithFormat:@"%@",[groupdict valueForKey:@"success"]];
        if ([check1 isEqualToString:@"1"])
        {
            if (isAtLeast8)
            {
                UIAlertController * alert2=   [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:@"You are no longer admin. Admin change sucessful"
                                               preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert2 animated:YES completion:nil];
                [self performSelector:@selector(test:) withObject:alert2 afterDelay:2];
            }
            if (isAtLeast7) {
                alert=[[UIAlertView alloc] initWithTitle:nil message:@"You are no longer admin. Admin change sucessful" delegate:self cancelButtonTitle:nil otherButtonTitles:nil, nil];
                [alert show];
                
                [self performSelector:@selector(test:) withObject:alert afterDelay:2];
            }
            
            //            GroupDetailsViewController *grpdtls=[[GroupDetailsViewController alloc]init];
            //            grpdtls.groupid=groupid;
            //            grpdtls.groupname=groupname;
            //            grpdtls.isadmin=@"0";
            //            [self.navigationController pushViewController:grpdtls animated:NO];
            
            // [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
            //            GroupDetailsViewController *grpdtls=[[GroupDetailsViewController alloc]init];
            //            [self.navigationController pushViewController:grpdtls animated:NO];
            
        }
        else if ([check1 isEqualToString:@"0"])
        {
            if (isAtLeast8)
            {
                UIAlertController * alert3=   [UIAlertController
                                               alertControllerWithTitle:@"Warning"
                                               message:[NSString stringWithFormat:@"%@",[groupdict valueForKey:@"error"]]
                                               preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* cancel = [UIAlertAction
                                         actionWithTitle:@"Ok"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             [alert3 dismissViewControllerAnimated:YES completion:nil];
                                             
                                         }];
                
                [alert3 addAction:cancel];
                
                [self presentViewController:alert3 animated:YES completion:nil];
            }
            if (isAtLeast7)
            {
                alert=[[UIAlertView alloc] initWithTitle:@"Warning" message:[NSString stringWithFormat:@"%@",[groupdict valueForKey:@"error"]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                
                [alert show];
            }
        }
        
    }
    @catch (NSException *exception)
    {
        DebugLog(@"exception %@",exception);
    }
    
    
    
}

-(void)remove_member:(int)index
{
    //int userid = [[[group_array objectAtIndex:index] objectForKey:@"id"] intValue];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action= group-removemember&group_id=%d&user_id=%d&access_token=%@&device_id=%@&business=true",[groupid intValue], index,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
    
    
    DebugLog(@"deny url: %@",urlString1);
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    
    if (signeddataURL1 != nil)
    {
        NSError *error=nil;
        NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                  options:kNilOptions
                                                                    error:&error];
        DebugLog(@"deny json returns: %@",json_deny);
        if ([[json_deny objectForKey:@"success"]intValue] == 1)
        {
            ind = 0;
            ind1 = 0;
            
            alert = [[UIAlertView alloc] initWithTitle:@"Member removed!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
            //                togglebt.userInteractionEnabled = NO;
            [group_array removeAllObjects];
            [filtered_arr removeAllObjects];
            [self loaddata];
            
            [groupstable reloadData];
            
        }
        else
        {
            alert = [[UIAlertView alloc] initWithTitle:nil
                                               message:@"You can't remove admin"
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
            
        }
        
    }
    
}

-(void)test:(UIAlertController*)x
{
    //[x dismissWithClickedButtonIndex:-1 animated:YES];
    //[x dismissViewControllerAnimated:NO completion:nil];
    [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
    GroupDetailsViewController *grpdtls=[[GroupDetailsViewController alloc]init];
    grpdtls.groupid=groupid;
    grpdtls.groupname=groupname;
    grpdtls.member_count=member_count;
    grpdtls.isadmin=@"0";
    
    //    CATransition* transition = [CATransition animation];
    //
    //    transition.duration = 0.4;
    //    transition.type = kCATransitionPush;
    //    transition.subtype = kCATransitionFade;
    //    transition.subtype = kCATransitionFromRight;
    //
    //    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:grpdtls animated:NO];
}


-(void)handleLongPress1:(UILongPressGestureRecognizer *)longPress1
{
    // Your code here
    
    //    CGPoint p = [longPress locationInView:groupstable];
    //
    //    NSIndexPath *indexPath = [groupstable indexPathForRowAtPoint:p];
    //    if (indexPath == nil) {
    //        NSLog(@"long press on table view but not on a row");
    //    } else if (longPress.state == UIGestureRecognizerStateBegan) {
    //        NSLog(@"long press on table view at row %ld", indexPath.section);
    //    } else {
    //        NSLog(@"gestureRecognizer.state = %ld", longPress.state);
    //    }
    rem_row = (int)longPress1.view.tag;
    
    DebugLog(@"cell tag value:%d",(int)rem_row);
    
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select an option" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Remove member" otherButtonTitles:@"Make group admin", nil];
    // UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select a option"
    //                                                          delegate:self
    //                                                cancelButtonTitle:@"Cancel"
    //                                                otherButtonTitles:@"make group admin", @"remove member", nil];
    
    actionSheet.tag = 101;
    [actionSheet showInView:mainview];
    
}

- (void)reloadAgain
{
    [self loaddata];
    [refreshcontrolnew endRefreshing];
    
    [groupstable reloadData];
    
}

- (void)willPresentActionSheet:(UIActionSheet *)actionSheet
{
    for (UIButton *subview in actionSheet.subviews) {
        if ([subview isKindOfClass:[UIButton class]]) {
            [subview setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        }
    }
}

-(void)changeTextColorForUIActionSheet:(UIActionSheet*)actionSheet {
    UIColor *tintColor = [UIColor redColor];
    
    NSArray *actionSheetButtons = actionSheet.subviews;
    for (int i = 0; [actionSheetButtons count] > i; i++) {
        
        UIView *view = (UIView*)[actionSheetButtons objectAtIndex:i];
        if([view isKindOfClass:[UIButton class]]){
            UIButton *btn = (UIButton*)view;
            [btn setTitleColor:tintColor forState:UIControlStateNormal];
            
        }
        
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
