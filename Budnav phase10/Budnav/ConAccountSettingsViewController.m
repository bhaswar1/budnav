//
//  ConAccountSettingsViewController.m
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 28/05/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
#import "ConAccountSettingsViewController.h"
#import "AppDelegate.h"
#import "ConEditProfileViewController.h"
#import "ConPasswordChangeViewController.h"
#import "ConSocialEditViewController.h"
#import "ConInviteViewController.h"
#import "ConLoginViewController.h"
#import "iToast.h"
#import "ConAddBackUpViewController.h"
#import <AddressBookUI/AddressBookUI.h>
#import "SVProgressHUD.h"
#import "ConAddFriendViewController.h"
#import "ConSyncLoaderViewController.h"
#import "ConProfileOwnViewController.h"
#import "ConRequestViewController.h"
#import "ConQROptionsViewController.h"
#import "ConFullSettingsViewController.h"
#import "ConNameViewController.h"
#import "ConHomeViewController.h"
#import "ConNewProfileViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

@interface ConAccountSettingsViewController ()<UIGestureRecognizerDelegate>
{
    int move, moveparent;
    UIView *mainview,*topbar;
    UITextField *myTextField;
    NSString *pass;
    NSMutableArray *con_array,*final_con_array,*filtered_arr;
    NSMutableDictionary *add_contacts_dict;
    NSMutableArray *app_contacts, *check_app_cont, *phonesarr;
    NSMutableArray *uniquearray;
    NSMutableData *responseData;
    NSDictionary *jsonparsed;
    UIImageView *logo_img, *bck_img;
    AppDelegate *del;
    UIActivityIndicatorView *actincir;
}
@end

@implementation ConAccountSettingsViewController
@synthesize length, push_check;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"From ConAccountSettingsViewController");
    // Do any additional setup after loading the view.
    del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, [[UIScreen mainScreen]bounds].size.height-44, [UIScreen mainScreen].bounds.size.width, 44)];
    del.tabBarController.tabBar.hidden = YES;
    self.navigationController.navigationBarHidden=YES;
    self.view.backgroundColor = [UIColor colorWithRed:(238.0f/255.0f) green:(238.0f/255.0f) blue:(238.0f/255.0f) alpha:1.0f];

    if (push_check) {
        
        [del selectedIcon];
    }
    
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64);
    
    // Add the gradient to the view
    [self.view.layer insertSublayer:gradient atIndex:0];
    
    
    logo_img = [[UIImageView alloc]init];
    logo_img.frame = CGRectMake(27+([UIScreen mainScreen].bounds.size.width-160)/2, 32, 101, 20);
    logo_img.backgroundColor = [UIColor clearColor];
    
    logo_img.image = [UIImage imageNamed:@"new_logo"];
    [self.view addSubview:logo_img];
    
    UILabel *topbartitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 32, [UIScreen mainScreen].bounds.size.width, 20)];
    topbartitle.backgroundColor=[UIColor clearColor];
    topbartitle.text=@"ACCOUNT SETTINGS";
    topbartitle.textAlignment=NSTextAlignmentCenter;
    topbartitle.font=[UIFont fontWithName:@"ProximaNova-Regular" size:15.5f];
    topbartitle.textColor=[UIColor whiteColor];
    
    
    bck_img = [[UIImageView alloc]initWithFrame:CGRectMake(15,32,12,20)];//(13, 39, 10, 22)];
    bck_img.image=[UIImage imageNamed:@"back3"];
    [self.view addSubview:bck_img];
    
    UIButton *helpbt = [UIButton buttonWithType:UIButtonTypeCustom];
    helpbt.frame = CGRectMake(0, 0, logo_img.frame.origin.x-1, 64);
    helpbt.backgroundColor=[UIColor clearColor];
    [helpbt setTitle:nil forState:UIControlStateNormal];
    
    [helpbt addTarget:self action:@selector(backColor) forControlEvents:UIControlEventTouchDown];
    [helpbt addTarget:self action:@selector(backColorAgain) forControlEvents:UIControlEventTouchDragExit];
    [helpbt addTarget:self action:@selector(backColor) forControlEvents:UIControlEventTouchDragEnter];
    [helpbt addTarget:self action:@selector(goback) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:helpbt];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    moveparent=1;
  
    
    move=0;
    cellobjects = [[NSArray alloc] initWithObjects:@"Name",@"Password",@"Delete Account",@"",@"Sign out", nil]; //,@"Backup My Contact Book",@"Restore Backup"
    
    mainview = [[UIView alloc]initWithFrame:CGRectMake(0, 65, [UIScreen mainScreen].bounds.size.width, self.view.frame.size.height-65)];
    [self.view addSubview:mainview];
    mainview.backgroundColor=[UIColor clearColor];
    
    del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, [[UIScreen mainScreen]bounds].size.height-44, [UIScreen mainScreen].bounds.size.width, 44)];
    del.tabBarController.tabBar.hidden = YES;
    
    itemsTable = [[UITableView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, [UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)];
    [itemsTable setBackgroundColor:[UIColor clearColor]];
    [itemsTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    itemsTable.delegate= self;
    [itemsTable setDataSource:self];
    itemsTable.userInteractionEnabled=YES;
    itemsTable.scrollEnabled=NO;
    [mainview addSubview:itemsTable];
}

+(void)chngpostion
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Data" object:Nil];
}


-(void)requestreceivedaction
{
    del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
    con.other=@"yes";
    con.uid=[NSString stringWithFormat:@"%@",del.pushtoprofileid];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.3;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:kCATransition];
    
    [self.navigationController pushViewController:con animated:YES];
}

-(void)acceptedrequestaction
{
    ConRequestViewController *con = [[ConRequestViewController alloc]init];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.3;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:kCATransition];
    
    [self.navigationController pushViewController:con animated:YES];
}

-(void)navtopage: (NSNotification *)notification
{
    NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
    if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"4"])
    {
        ConAccountSettingsViewController *con = [[ConAccountSettingsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"6"])
    {
        ConInviteViewController *con = [[ConInviteViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"5"])
    {
        ConSyncLoaderViewController *con = [[ConSyncLoaderViewController alloc]init];
        
        [self presentViewController:con animated:NO completion:nil];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"2"])
    {
        ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"3"])
    {
        ConQROptionsViewController *con = [[ConQROptionsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    
    [mainview setFrame:CGRectMake(0, mainview.frame.origin.y, mainview.frame.size.width, mainview.frame.size.height)];
    del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    
    ConParentTopBarViewController *con = [[ConParentTopBarViewController alloc]init];
    con.topbarparent.backgroundColor=[UIColor blackColor];
    move=1;
    
}


-(void)getData:(NSNotification *)notification {
    
    if(move == 0) {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-40, mainview.frame.origin.y, mainview.frame.size.width, mainview.frame.size.height)];
                             [topbar setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-40)+topbar.frame.origin.x, topbar.frame.origin.y, topbar.frame.size.width, topbar.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             move=1;
                         }];
    } else {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake(0, (mainview.frame.origin.y),mainview.frame.size.width, mainview.frame.size.height)];
                             [topbar setFrame:CGRectMake(topbar.frame.origin.x-([UIScreen mainScreen].bounds.size.width-40), topbar.frame.origin.y, topbar.frame.size.width, topbar.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             move=0;
                         }];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat row_height;
    
    if (indexPath.row==3)
        row_height = 35;
    else
    row_height=50;
    
    return row_height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell==nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
    UIImage *image;
    switch (indexPath.row) {
        case 0:
            image=[UIImage imageNamed:@"4.png"];
            break;
        case 1:
            image=[UIImage imageNamed:@"3.png"];
            break;
        case 2:
            image = [UIImage imageNamed:@"2.png"];
            break;
   
        case 3:
            image = [UIImage imageNamed:@"1.png"];
            break;
    }
    
    
    UILabel *title_label = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, cell.frame.size.width-50, 50)];
    title_label.backgroundColor = [UIColor clearColor];
    title_label.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17.0f];
    title_label.textColor = [UIColor blackColor];
    title_label.text = [cellobjects objectAtIndex:indexPath.row];
    title_label.textAlignment = NSTextAlignmentLeft;
    [cell addSubview:title_label];
    
    
    UIImageView *arrow = [[UIImageView alloc] init];
    arrow.backgroundColor = [UIColor clearColor];
    arrow.image = [UIImage imageNamed:@"grey_arrow"];
    [cell addSubview:arrow];
    
    UIView *sep = [[UIView alloc] init];
    sep.backgroundColor = [UIColor colorWithRed:(153.0f/255.f) green:(153.0f/255.f) blue:(153.0f/255.f) alpha:1.0f];
    [cell addSubview:sep];
    
    if (indexPath.row==3) {
        
        arrow.frame = CGRectMake(0, 0, 0, 0);
        
        [cell setBackgroundColor:[UIColor clearColor]];
        sep.frame = CGRectMake(0, 34, self.view.frame.size.width, 0.6f);
       
    }
    else
    {
        arrow.frame = CGRectMake(self.view.frame.size.width-28, 18, 8, 13);
        [cell setBackgroundColor:[UIColor whiteColor]];
        sep.frame = CGRectMake(0, 49, self.view.frame.size.width, 0.6f);
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==0)
    {
        ConNameViewController *con = [[ConNameViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    
    
    if(indexPath.row==1)
    {
        ConPasswordChangeViewController *con = [[ConPasswordChangeViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        
        [self.navigationController pushViewController:con animated:YES];
    }
    
    
    if(indexPath.row==2)
    {
        
        alert = [[UIAlertView alloc] initWithTitle:@"Do You Want to Delete Your Account?"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"Cancel"  otherButtonTitles:@"Ok", nil];
        alert.delegate=self;
        alert.tag=1;
        [alert show];
    }
    
    if(indexPath.row==4)
    {
        
               actincir = [[UIActivityIndicatorView alloc] init];
        
                actincir.center = self.view.center;
        
                [actincir startAnimating];
        
                actincir.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        
                [actincir setColor:[UIColor whiteColor]];
        
                [self.view addSubview:actincir];
        
        
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
                    NSString *urlString_qr =[NSString stringWithFormat:@"https://budnav.com/ext/?action=logout&id=%@&access_token=%@&device_id=%@",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
        
                    NSString *newString_qr = [urlString_qr stringByReplacingOccurrencesOfString:@" " withString:@""];
        
                    DebugLog(@"STRING=> %@",newString_qr);
        
                    //***********
        
                    NSError *localErr;
        
        
        
                    NSData *dataURL =  [NSData dataWithContentsOfURL:[NSURL URLWithString:newString_qr] options:NSDataReadingUncached error:&localErr];
        
                    if (dataURL != nil)
                    {
                        if(localErr!=nil)
                        {
                            DebugLog(@" nil");
                        }
        
                        jsonparsed = [NSJSONSerialization JSONObjectWithData:dataURL //1
        
                                                                     options:kNilOptions
        
                                                                       error:&localErr];
        
                        DebugLog(@"JSON PARSED%@",jsonparsed);
        
                        NSString *errornumber= [NSString stringWithFormat:@"%@",[jsonparsed objectForKey:@"errorno"]];
        
                        DebugLog(@"error number  %@",errornumber);
        
                        if (![errornumber isEqualToString:@"0"])
                        {
                            [self performSelectorOnMainThread:@selector(failure) withObject:nil waitUntilDone:YES]    ;
                        }
                        else
                        {
                            [self performSelectorOnMainThread:@selector(yesSuccess)
        
                                                   withObject:nil
        
                                                waitUntilDone:YES];
                        }
                    }
                    else
                    {
                        DebugLog(@"server not responding");
        
                        [SVProgressHUD showErrorWithStatus:@"Error in Server Connection"];
        
                        [actincir stopAnimating];
                    }
        
                });
        
        
                //*************************************
        
        NSString *tempValue = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
        
        DebugLog(@"DEVICE TOKEN AFTER SIGN OUT:%@",tempValue);
                NSDictionary * dict = [prefs dictionaryRepresentation];
                for (id key in dict) {
                    [prefs removeObjectForKey:key];
                }
                [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"isloggedin"];
                [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"autologin"];

        [[NSUserDefaults standardUserDefaults] setObject:tempValue forKey:@"deviceToken"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
                ConHomeViewController *sign_out = [[ConHomeViewController alloc] init];
                AppDelegate *mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
                mainDelegate.navigationController = [[UINavigationController alloc] initWithRootViewController:sign_out];
                mainDelegate.navigationController.navigationBar.hidden= YES;
                [mainDelegate showTabStatus:NO];
                mainDelegate.window.rootViewController = mainDelegate.navigationController;
                [mainDelegate.window makeKeyAndVisible];
        
    }
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    move=0;
    [itemsTable reloadData];
    [super viewDidDisappear:YES];
    
    [mainview setFrame:CGRectMake(0, mainview.frame.origin.y, mainview.frame.size.width, mainview.frame.size.height)];
    del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    
    ConParentTopBarViewController *con = [[ConParentTopBarViewController alloc]init];
    con.topbarparent.frame= CGRectMake(0, 0, con.topbarparent.bounds.size.width, con.topbarparent.bounds.size.height);
    con.topbarparent.backgroundColor=[UIColor blackColor];
    move=1;
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch ([alertView tag]) {
        case 1:
        {
            switch (buttonIndex) {
                case 0:
                {
                }
                    break;
                case 1:
                {
                    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Enter Password"
                                                                          message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
                    myAlertView.delegate=self;
                    myAlertView.alertViewStyle = UIAlertViewStyleSecureTextInput;
                    myAlertView.tag=2;
                    [myAlertView show];
                }
                    break;
            }
        }
            break;
        case 2:
        {
            switch (buttonIndex) {
                case 0:
                {
                    DebugLog(@"2 case1");
                }
                    break;
                case 1:
                {
                    pass = [alertView textFieldAtIndex:0].text;
                    DebugLog(@"pass : %@",pass);
                    [self deleteaction];
                }
                    break;
            }
        }
            break;
        case 3:
        {
        case 0:
            {
            }
            break;
        }
            break;
        case 4:
        {
        }
            break;
        case 5:
        {
        }
            break;
    }
}

-(void)deleteaction
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    @try {
        NSString *urlString_del =[NSString stringWithFormat:@"https://budnav.com/ext/?action=deleteaccount&pass=%@&access_token=%@&device_id=%@",[pass stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
        
        DebugLog(@"delete profile url: %@",urlString_del);
        
        NSString *newString_del = [urlString_del stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSData *signeddataURL_del =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString_del]];
        if(signeddataURL_del!=nil)
        {
            NSError *errordel=nil;
            
            NSDictionary *jsondel = [NSJSONSerialization JSONObjectWithData:signeddataURL_del //1
                                     
                                                                    options:kNilOptions
                                     
                                                                      error:&errordel];
            DebugLog(@"json returns: %@",jsondel);
            
            NSString *errornumber_del= [NSString stringWithFormat:@"%@",[jsondel objectForKey:@"errorno"]];
            DebugLog(@"err  %@",errornumber_del);
            if (![errornumber_del isEqualToString:@"0"])
            {
                DebugLog(@"if if");
                NSString *err_str_del = [jsondel objectForKey:@"error"];
                alert = [[UIAlertView alloc] initWithTitle:@"Error in Retrieving Data!"
                                                   message:err_str_del
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                [alert show];
            }
            else
            {
                
                if ([[jsondel objectForKey:@"success"]intValue] == 1)
                {
                    
                    [[[[iToast makeText:[NSString stringWithFormat:@"Account Successfully deleted"]]
                       setGravity:iToastGravityTop] setDuration:iToastDurationLong] show];
                    
                    [prefs setObject:@"no" forKey:@"isloggedin"];
//                    ConLoginViewController *con = [[ConLoginViewController alloc]init];
                    ConHomeViewController *con = [[ConHomeViewController alloc]init];
                    
                    AppDelegate *mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                    
                    mainDelegate.navigationController = [[UINavigationController alloc] initWithRootViewController:con];
                    mainDelegate.navigationController.navigationBar.hidden= YES;
                    [mainDelegate showTabStatus:NO];
                    mainDelegate.window.rootViewController = mainDelegate.navigationController;
                    [mainDelegate.window makeKeyAndVisible];
                    
                }
                else
                {
                    alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                                       message:@"Failed to Delete Account"
                                                      delegate:self
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                    [alert show];
                }
                
            }
        }
        else
        {
            DebugLog(@"no connnnn");
            alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            alert.tag=5;
            [alert show];
            
        }
    }
    @catch (NSException *exception) {
        DebugLog(@"exception profile contact count: %@",exception);
    }
    @finally {
        
    }
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data
{
    [responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    DebugLog(@"connection failed");
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    DebugLog(@"connection finished loading");
    
    DebugLog(@"response data - %@", [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
    
    NSError *err;
    
    
    if (responseData != nil)
    {
        
        NSDictionary  *json1aa=[NSJSONSerialization JSONObjectWithData:responseData options:0 error:&err];
        
        DebugLog(@"this now has json string url: %@",json1aa);
        
        if ([[json1aa objectForKey:@"success"] intValue] ==1)
            [SVProgressHUD showSuccessWithStatus:@"Success!"];
        
    }
    else
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        alert.tag=7;
        [alert show];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    responseData = [NSMutableData data];
}

-(void) goback
{
    del.tabBarController.tabBar.hidden = NO;
    
    if (push_check) {
        
        push_check = FALSE;
        ConFullSettingsViewController *con_full = [[ConFullSettingsViewController alloc] init];
        
        
        CATransition *transition = [CATransition animation];
        
        transition.duration = 0.4f;
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFade;
        transition.subtype = kCATransitionFromLeft;
        
        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        [self.navigationController pushViewController:con_full animated:NO];
        
        
    }
    else
    {
        
        int index;
        NSArray* navarr = [[NSArray alloc] initWithArray:self.navigationController.viewControllers];
        for(int i=0 ; i<[navarr count] ; i++)
        {
            if(![[navarr objectAtIndex:i] isKindOfClass:NSClassFromString(@"ConAccountSettingsViewController")])
            {
                index = i;
            }
        }
        
        CATransition *transition = [CATransition animation];
        
        transition.duration = 0.4f;
    
        transition.type = kCATransitionFade;
        
        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        [self.navigationController popToViewController:[navarr objectAtIndex:index] animated:YES];
        
    }
    
}

-(void)yesSuccess{
    
    NSString *dict_details = [jsonparsed objectForKey:@"success"];
    
    DebugLog(@" dict detaisls %@",dict_details);
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    if([dict_details intValue]== 1)
    {
        NSString *tempValue = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
        
        NSDictionary * dict = [prefs dictionaryRepresentation];
        for (id key in dict) {
            
            [prefs removeObjectForKey:key];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"isloggedin"];
        [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"autologin"];
        
        [[NSUserDefaults standardUserDefaults] setObject:tempValue forKey:@"deviceToken"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    
    [actincir stopAnimating];
    [actincir removeFromSuperview];
   
}


-(void)failure{
    
    [actincir stopAnimating];
    [actincir removeFromSuperview];
}

-(void)backColorAgain
{
    bck_img.alpha = 1.0f;
}
-(void)backColor
{
    bck_img.alpha = 0.5f;
}


@end