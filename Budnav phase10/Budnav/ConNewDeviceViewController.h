//
//  ConNewDeviceViewController.h
//  Budnav
//
//  Created by intel on 03/06/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConSettingsView.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "Mymodel.h"
#import "GAITrackedViewController.h"

@interface ConNewDeviceViewController : GAITrackedViewController<UIGestureRecognizerDelegate,MKMapViewDelegate, NSURLConnectionDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,UIScrollViewDelegate,UIActionSheetDelegate,UIAlertViewDelegate>
{
    //    UIView *topbar;
    float y;
    MKMapView *map_View;
    UIImage *profilepic;
    UIView *divider_1,*divider_2,*divider_9;
    UIImageView *mobileimg,*homeimg,*workimg,*mailimg,*address_img;
    NSString *address_str;
    MFMailComposeViewController *mailComposer;
    NSMutableArray *mobilearray, *workarray, *phonesarr1;
}
@property(nonatomic,retain)NSDictionary *ContactDetails_Dict;
+(void)chngpostion;
@end
