//
//  ConQROptionsViewController.h
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 22/05/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConParentTopBarViewController.h"
@interface ConQROptionsViewController : ConParentTopBarViewController<UIAlertViewDelegate>
{
    NSDictionary *json1;
    UIAlertView *alert;
    NSMutableArray *requestslist;
    UITableView *tab_request;
}
@property (nonatomic, strong) UIView *mainview;
+(void)chngpostion;
@end
