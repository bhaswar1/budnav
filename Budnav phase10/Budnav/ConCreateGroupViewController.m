//
//  ConCreateGroupViewController.m
//  Contacter
//
//  Created by ios on 13/01/15.
//  Copyright (c) 2015 Esolz. All rights reserved.
//
#import "AppDelegate.h"
#import "ConCreateGroupViewController.h"
#import "ConGroupsViewController.h"
#import "ImageCropperViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

#define ORIGINAL_MAX_HEIGHT 450.0f

@interface ConCreateGroupViewController ()<ImageCropperDelegate>
{
    UIActivityIndicatorView *act, *loader;
    UIView *mainview,*blackBack,*grayView,*additionalview;
    UIButton *back,*create,*personal,*business,*edit;
    UIImageView *prof,*bck_img;
    UITextField *group_name;
    UITextView *descp;
    UILabel *backlbl,*personal_lbl1,*personal_lbl2,*business_lbl1,*business_lbl2,*personal_label,*business_label,*sidebar;
    NSDictionary *groupdict,*groupdetail;
    NSMutableArray *detail;
    BOOL isAtLeast7,isAtLeast8;
    NSString *groups_url,*version,*pathss,*encodedString,*params;
    int type;
    NSMutableDictionary *groupdict1;
    UIImage *portraitImg1;
    NSMutableData *responseData;
    AppDelegate *del;
    NSCharacterSet *whitespace;
}
@end

@implementation ConCreateGroupViewController
@synthesize groupname,image_shown;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    del.tabBarController.tabBar.frame = CGRectMake(0, [[UIScreen mainScreen]bounds].size.height, [UIScreen mainScreen].bounds.size.width,44);
    self.tabBarController.tabBar.translucent = YES;
    
    blackBack = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    //[blackBack setBackgroundColor:[UIColor blackColor]];
    [self.view addSubview:blackBack];
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
    
    // Add the gradient to the view
    [blackBack.layer insertSublayer:gradient atIndex:0];
    
    
    
    
    bck_img=[[UIImageView alloc]initWithFrame:CGRectMake(15, 32, 12, 20)];
    bck_img.image=[UIImage imageNamed:@"back3"];
    [blackBack addSubview:bck_img];
    
    backlbl=[[UILabel alloc]initWithFrame:CGRectMake(bck_img.frame.origin.x+bck_img.frame.size.width+5, 29, 54, 30)];
    backlbl.text=@"Back";
    backlbl.textColor=[UIColor whiteColor];
    backlbl.font=[UIFont fontWithName:@"ProximaNova-Regular" size:20];
    backlbl.backgroundColor=[UIColor clearColor];
    [blackBack addSubview:backlbl];
    
    back=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 150, 64)];
    [back setTitle:@"" forState:UIControlStateNormal];
    back.backgroundColor=[UIColor clearColor];
    [back addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDown];
    [back addTarget:self action:@selector(changecoloragain) forControlEvents:UIControlEventTouchDragExit];
    [back addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDragEnter];

    [back addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [blackBack addSubview:back];
    
    create=[[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width-85, 29, 77, 30)];
    [create setTitle:@"Create" forState:UIControlStateNormal];
    create.titleLabel.font=[UIFont fontWithName:@"ProximaNova-Regular" size:20];
    [create addTarget:self action:@selector(create) forControlEvents:UIControlEventTouchUpInside];
    create.backgroundColor=[UIColor clearColor];
    [blackBack addSubview:create];
    
    
    
    version = [[UIDevice currentDevice] systemVersion];
    //DebugLog(@"version: %@",version);
    isAtLeast7 = [version floatValue] < 8.0;
    isAtLeast8 = [version floatValue] >= 8.0;
    
    
    //=====================Search bar black view with add groups button=======================//
    
    grayView=[[UIView alloc]initWithFrame:CGRectMake(0, blackBack.frame.origin.y+blackBack.frame.size.height, self.view.frame.size.width, 106.2)];
    grayView.backgroundColor=[UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1.0];
    [self.view addSubview:grayView];
    
    prof=[[UIImageView alloc]initWithFrame:CGRectMake(grayView.frame.origin.x+15, 11, 88, grayView.frame.size.height-22)];
    if (image_shown!=1) {
        prof.image=[UIImage imageNamed:@"Change Group Picture.png"];
    }
    else if (image_shown==1)
        prof.image=portraitImg1;
    prof.contentMode=UIViewContentModeScaleAspectFit;
    prof.userInteractionEnabled=YES;
    [grayView addSubview:prof];
    
    UITapGestureRecognizer *tapimg1 = [[UITapGestureRecognizer alloc]
                                       
                                       initWithTarget:self
                                       
                                       action:@selector(selectphoto)];
    
    
    [prof addGestureRecognizer:tapimg1];
    
    
    UILabel *label1=[[UILabel alloc]initWithFrame:CGRectMake(120, 20, [UIScreen mainScreen].bounds.size.width-90, 20)];
    label1.text=@"Create a group to \nconnect with multiple \npeople at once.";
    //    [label1 sizeToFit];
    label1.numberOfLines=3;
    label1.lineBreakMode=NSLineBreakByWordWrapping;
    [label1 sizeToFit];
    label1.textColor=[UIColor colorWithRed:101.0/255.0 green:101.0/255.0 blue:101.0/255.0 alpha:1.0];
    label1.font=[UIFont fontWithName:@"ProximaNova-Regular" size:18];
    
    NSMutableAttributedString* attrString = [[NSMutableAttributedString alloc] initWithString:@"Create a group to \nconnect with multiple \npeople at once."];
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setLineSpacing:3];
    [attrString addAttribute:NSParagraphStyleAttributeName
                       value:style
                       range:NSMakeRange(0, [label1.text length])];
    label1.attributedText = attrString;
    
    [grayView addSubview:label1];
    
    
    mainview = [[UIView alloc]initWithFrame:CGRectMake(0,grayView.frame.origin.y+grayView.frame.size.height, self.view.frame.size.width, [UIScreen mainScreen].bounds.size.height)];
    mainview.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:mainview];
    additionalview=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 11, 50)];
    
    
    
    personal=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 160, 85)];
    [personal setTitle:@"" forState:UIControlStateNormal];
    personal.backgroundColor=[UIColor clearColor];
    [personal addTarget:self action:@selector(per) forControlEvents:UIControlEventTouchUpInside];
    [mainview addSubview:personal];
    
    personal_lbl1=[[UILabel alloc]initWithFrame:CGRectMake(0, 18, [UIScreen mainScreen].bounds.size.width/2, 20)];
    personal_lbl1.text=@"Personal";
    personal_lbl1.textAlignment=NSTextAlignmentCenter;
    personal_lbl1.font=[UIFont fontWithName:@"ProximaNova-Regular" size:20];
    [personal addSubview:personal_lbl1];
    
    personal_lbl2=[[UILabel alloc]initWithFrame:CGRectMake(0, personal_lbl1.frame.origin.y+personal_lbl1.frame.size.height+5, [UIScreen mainScreen].bounds.size.width/2, 20)];
    personal_lbl2.text=@"Group";
    personal_lbl2.textAlignment=NSTextAlignmentCenter;
    personal_lbl2.font=[UIFont fontWithName:@"ProximaNova-Regular" size:20];
    [personal addSubview:personal_lbl2];
    
    
    personal_label=[[UILabel alloc]initWithFrame:CGRectMake(0, personal_lbl2.frame.origin.y+personal_lbl2.frame.size.height+16, [UIScreen mainScreen].bounds.size.width/2, 11)];
    personal_label.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"Personal Group Bar.png"]];
    [mainview addSubview:personal_label];
    type=1;
    personal_label.hidden=NO;
    
    sidebar=[[UILabel alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2, 18, 0.5, 45)];
    sidebar.backgroundColor=[UIColor lightGrayColor];
    [mainview addSubview:sidebar];
    
    business=[[UIButton alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2+0.5f, 0, 180, 85)];
    [business setTitle:@"" forState:UIControlStateNormal];
    business.backgroundColor=[UIColor clearColor];
    // business.selected=NO;
    [business addTarget:self action:@selector(busi) forControlEvents:UIControlEventTouchUpInside];
    [mainview addSubview:business];
    
    
    
    business_lbl1=[[UILabel alloc]initWithFrame:CGRectMake(0, 18, [UIScreen mainScreen].bounds.size.width/2, 20)];
    business_lbl1.text=@"Business";
    business_lbl1.textColor=[UIColor colorWithRed:101.0/255.0 green:101.0/255.0 blue:101.0/255.0 alpha:1.0];
    business_lbl1.textAlignment=NSTextAlignmentCenter;
    business_lbl1.font=[UIFont fontWithName:@"ProximaNova-Regular" size:20];
    [business addSubview:business_lbl1];
    
    business_lbl2=[[UILabel alloc]initWithFrame:CGRectMake(0, business_lbl1.frame.origin.y+business_lbl1.frame.size.height+5, [UIScreen mainScreen].bounds.size.width/2, 20)];
    business_lbl2.text=@"Group";
    business_lbl2.textColor=[UIColor colorWithRed:101.0/255.0 green:101.0/255.0 blue:101.0/255.0 alpha:1.0];
    business_lbl2.textAlignment=NSTextAlignmentCenter;
    business_lbl2.font=[UIFont fontWithName:@"ProximaNova-Regular" size:20];
    [business addSubview:business_lbl2];
    
    business_label=[[UILabel alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2, business_lbl2.frame.origin.y+business_lbl2.frame.size.height+16, [UIScreen mainScreen].bounds.size.width/2, 11)];
    business_label.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"Business Group Bar.png"]];
    [mainview addSubview:business_label];
    business_label.hidden=YES;
    
    
    UILabel *separatorlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 90, [UIScreen mainScreen].bounds.size.width, 0.4)];
    separatorlabel.backgroundColor=[UIColor lightGrayColor];
    [mainview addSubview:separatorlabel];
    
    
    group_name=[[UITextField alloc]initWithFrame:CGRectMake(0, 90.4, mainview.frame.size.width, 58)];
    UIColor *color = [UIColor colorWithRed:101.0/255.0 green:101.0/255.0 blue:101.0/255.0 alpha:1.0];
    group_name.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Group Name" attributes:@{NSForegroundColorAttributeName: color}];
    group_name.delegate=self;
    group_name.text=groupname;
    group_name.userInteractionEnabled=YES;
    group_name.leftView=additionalview;
    group_name.autocorrectionType = UITextAutocorrectionTypeNo;
    group_name.leftViewMode=UITextFieldViewModeAlways;
    group_name.keyboardAppearance=UIKeyboardAppearanceDark;
    group_name.font=[UIFont fontWithName:@"ProximaNova-Regular" size:18];
    [mainview addSubview:group_name];
    
    UILabel *separatorlabel1=[[UILabel alloc]initWithFrame:CGRectMake(0, group_name.frame.origin.y+group_name.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.4)];
    separatorlabel1.backgroundColor=[UIColor lightGrayColor];
    [mainview addSubview:separatorlabel1];
    
    descp=[[UITextView alloc]initWithFrame:CGRectMake(0, separatorlabel1.frame.origin.y+separatorlabel1.frame.size.height, mainview.frame.size.width, mainview.frame.size.height)];//140)];
    descp.delegate=self;
    descp.font = [UIFont fontWithName:@"ProximaNova-Regular" size:18];
    descp.textColor=[UIColor colorWithRed:101.0/255.0 green:101.0/255.0 blue:101.0/255.0 alpha:1.0];
    descp.text=@"Description";
    descp.textContainerInset = UIEdgeInsetsMake(19, 8, 0, 0);//UIEdgeInsetsMake(19, 15, 0, 0);
    descp.backgroundColor=[UIColor clearColor];
    descp.scrollEnabled=YES;
    descp.pagingEnabled = YES;
    descp.editable = YES;
    descp.autocorrectionType = UITextAutocorrectionTypeNo;
    descp.keyboardAppearance=UIKeyboardAppearanceDark;
    
    [mainview addSubview:descp];
    
    
}


- (BOOL)hidesBottomBarWhenPushed {
    return YES;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    self.screenName = @"Groups";
    
    [[GAI sharedInstance].defaultTracker send:
     
     [[GAIDictionaryBuilder createEventWithCategory:@"Groups"
       
                                             action:@"Create group"
       
                                              label:nil
       
                                              value:nil] build]];
    
//    version = [[UIDevice currentDevice] systemVersion];
//    //DebugLog(@"version: %@",version);
//    isAtLeast7 = [version floatValue] < 8.0;
//    isAtLeast8 = [version floatValue] >= 8.0;
//    
//    
//    //=====================Search bar black view with add groups button=======================//
//    
//    grayView=[[UIView alloc]initWithFrame:CGRectMake(0, blackBack.frame.origin.y+blackBack.frame.size.height, self.view.frame.size.width, 106.2)];
//    grayView.backgroundColor=[UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1.0];
//    [self.view addSubview:grayView];
//    
//    prof=[[UIImageView alloc]initWithFrame:CGRectMake(grayView.frame.origin.x+15, 11, 88, grayView.frame.size.height-22)];
//    if (image_shown!=1) {
//        prof.image=[UIImage imageNamed:@"Change Group Picture.png"];
//    }
//    else if (image_shown==1)
//        prof.image=portraitImg1;
//    prof.contentMode=UIViewContentModeScaleAspectFit;
//    prof.userInteractionEnabled=YES;
//    [grayView addSubview:prof];
//    
//    UITapGestureRecognizer *tapimg1 = [[UITapGestureRecognizer alloc]
//                                       
//                                       initWithTarget:self
//                                       
//                                       action:@selector(selectphoto)];
//    
//    
//    [prof addGestureRecognizer:tapimg1];
//    
//    
//    UILabel *label1=[[UILabel alloc]initWithFrame:CGRectMake(120, 20, [UIScreen mainScreen].bounds.size.width-90, 20)];
//    label1.text=@"Create a group to \nconnect with multiple \npeople at once.";
//    //    [label1 sizeToFit];
//    label1.numberOfLines=3;
//    label1.lineBreakMode=NSLineBreakByWordWrapping;
//    [label1 sizeToFit];
//    label1.textColor=[UIColor colorWithRed:101.0/255.0 green:101.0/255.0 blue:101.0/255.0 alpha:1.0];
//    label1.font=[UIFont fontWithName:@"ProximaNova-Regular" size:18];
//    
//    NSMutableAttributedString* attrString = [[NSMutableAttributedString alloc] initWithString:@"Create a group to \nconnect with multiple \npeople at once."];
//    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
//    [style setLineSpacing:3];
//    [attrString addAttribute:NSParagraphStyleAttributeName
//                       value:style
//                       range:NSMakeRange(0, [label1.text length])];
//    label1.attributedText = attrString;
//    
//    [grayView addSubview:label1];
//    
//    
//    mainview = [[UIView alloc]initWithFrame:CGRectMake(0,grayView.frame.origin.y+grayView.frame.size.height, self.view.frame.size.width, [UIScreen mainScreen].bounds.size.height)];
//    mainview.backgroundColor=[UIColor whiteColor];
//    [self.view addSubview:mainview];
//    additionalview=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 11, 50)];
//    
//    
//    
//    personal=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 160, 85)];
//    [personal setTitle:@"" forState:UIControlStateNormal];
//    personal.backgroundColor=[UIColor clearColor];
//    [personal addTarget:self action:@selector(per) forControlEvents:UIControlEventTouchUpInside];
//    [mainview addSubview:personal];
//    
//    personal_lbl1=[[UILabel alloc]initWithFrame:CGRectMake(0, 18, [UIScreen mainScreen].bounds.size.width/2, 20)];
//    personal_lbl1.text=@"Personal";
//    personal_lbl1.textAlignment=NSTextAlignmentCenter;
//    personal_lbl1.font=[UIFont fontWithName:@"ProximaNova-Regular" size:20];
//    [personal addSubview:personal_lbl1];
//    
//    personal_lbl2=[[UILabel alloc]initWithFrame:CGRectMake(0, personal_lbl1.frame.origin.y+personal_lbl1.frame.size.height+5, [UIScreen mainScreen].bounds.size.width/2, 20)];
//    personal_lbl2.text=@"Group";
//    personal_lbl2.textAlignment=NSTextAlignmentCenter;
//    personal_lbl2.font=[UIFont fontWithName:@"ProximaNova-Regular" size:20];
//    [personal addSubview:personal_lbl2];
//    
//    
//    personal_label=[[UILabel alloc]initWithFrame:CGRectMake(0, personal_lbl2.frame.origin.y+personal_lbl2.frame.size.height+16, [UIScreen mainScreen].bounds.size.width/2, 11)];
//    personal_label.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"Personal Group Bar.png"]];
//    [mainview addSubview:personal_label];
//    type=1;
//    personal_label.hidden=NO;
//    
//    sidebar=[[UILabel alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2, 18, 0.5, 45)];
//    sidebar.backgroundColor=[UIColor lightGrayColor];
//    [mainview addSubview:sidebar];
//    
//    business=[[UIButton alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2+0.5f, 0, 180, 85)];
//    [business setTitle:@"" forState:UIControlStateNormal];
//    business.backgroundColor=[UIColor clearColor];
//    // business.selected=NO;
//    [business addTarget:self action:@selector(busi) forControlEvents:UIControlEventTouchUpInside];
//    [mainview addSubview:business];
//    
//    
//    
//    business_lbl1=[[UILabel alloc]initWithFrame:CGRectMake(0, 18, [UIScreen mainScreen].bounds.size.width/2, 20)];
//    business_lbl1.text=@"Business";
//    business_lbl1.textColor=[UIColor colorWithRed:101.0/255.0 green:101.0/255.0 blue:101.0/255.0 alpha:1.0];
//    business_lbl1.textAlignment=NSTextAlignmentCenter;
//    business_lbl1.font=[UIFont fontWithName:@"ProximaNova-Regular" size:20];
//    [business addSubview:business_lbl1];
//    
//    business_lbl2=[[UILabel alloc]initWithFrame:CGRectMake(0, business_lbl1.frame.origin.y+business_lbl1.frame.size.height+5, [UIScreen mainScreen].bounds.size.width/2, 20)];
//    business_lbl2.text=@"Group";
//    business_lbl2.textColor=[UIColor colorWithRed:101.0/255.0 green:101.0/255.0 blue:101.0/255.0 alpha:1.0];
//    business_lbl2.textAlignment=NSTextAlignmentCenter;
//    business_lbl2.font=[UIFont fontWithName:@"ProximaNova-Regular" size:20];
//    [business addSubview:business_lbl2];
//    
//    business_label=[[UILabel alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2, business_lbl2.frame.origin.y+business_lbl2.frame.size.height+16, [UIScreen mainScreen].bounds.size.width/2, 11)];
//    business_label.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"Business Group Bar.png"]];
//    [mainview addSubview:business_label];
//    business_label.hidden=YES;
//    
//    
//    UILabel *separatorlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 90, [UIScreen mainScreen].bounds.size.width, 0.4)];
//    separatorlabel.backgroundColor=[UIColor lightGrayColor];
//    [mainview addSubview:separatorlabel];
//    
//    
//    group_name=[[UITextField alloc]initWithFrame:CGRectMake(0, 90.4, mainview.frame.size.width, 58)];
//    UIColor *color = [UIColor colorWithRed:101.0/255.0 green:101.0/255.0 blue:101.0/255.0 alpha:1.0];
//    group_name.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Group Name" attributes:@{NSForegroundColorAttributeName: color}];
//    group_name.delegate=self;
//    group_name.text=groupname;
//    group_name.userInteractionEnabled=YES;
//    group_name.leftView=additionalview;
//    group_name.autocorrectionType = UITextAutocorrectionTypeNo;
//    group_name.leftViewMode=UITextFieldViewModeAlways;
//    group_name.keyboardAppearance=UIKeyboardAppearanceDark;
//    group_name.font=[UIFont fontWithName:@"ProximaNova-Regular" size:18];
//    [mainview addSubview:group_name];
//    
//    UILabel *separatorlabel1=[[UILabel alloc]initWithFrame:CGRectMake(0, group_name.frame.origin.y+group_name.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0.4)];
//    separatorlabel1.backgroundColor=[UIColor lightGrayColor];
//    [mainview addSubview:separatorlabel1];
//    
//    descp=[[UITextView alloc]initWithFrame:CGRectMake(0, separatorlabel1.frame.origin.y+separatorlabel1.frame.size.height, mainview.frame.size.width, mainview.frame.size.height)];//140)];
//    descp.delegate=self;
//    descp.font = [UIFont fontWithName:@"ProximaNova-Regular" size:18];
//    descp.textColor=[UIColor colorWithRed:101.0/255.0 green:101.0/255.0 blue:101.0/255.0 alpha:1.0];
//    descp.text=@"Description";
//    descp.textContainerInset = UIEdgeInsetsMake(19, 8, 0, 0);//UIEdgeInsetsMake(19, 15, 0, 0);
//    descp.backgroundColor=[UIColor clearColor];
//    descp.scrollEnabled=YES;
//    descp.pagingEnabled = YES;
//    descp.editable = YES;
//    descp.autocorrectionType = UITextAutocorrectionTypeNo;
//    descp.keyboardAppearance=UIKeyboardAppearanceDark;
//    
//    [mainview addSubview:descp];
    
    
}

-(void)back
{
    
    CATransition *transition = [CATransition animation];
    
    transition.duration = 0.4f;
    
    transition.type = kCATransitionFade;
    
    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)per
{
    personal_label.hidden=NO;
    personal_lbl1.textColor=[UIColor blackColor];
    personal_lbl2.textColor=[UIColor blackColor];
    business_label.hidden=YES;
    business_lbl1.textColor=[UIColor colorWithRed:101.0/255.0 green:101.0/255.0 blue:101.0/255.0 alpha:1.0];
    business_lbl2.textColor=[UIColor colorWithRed:101.0/255.0 green:101.0/255.0 blue:101.0/255.0 alpha:1.0];
    type=1;
}


-(void)busi
{
    personal_label.hidden=YES;
    personal_lbl1.textColor=[UIColor colorWithRed:101.0/255.0 green:101.0/255.0 blue:101.0/255.0 alpha:1.0];
    personal_lbl2.textColor=[UIColor colorWithRed:101.0/255.0 green:101.0/255.0 blue:101.0/255.0 alpha:1.0];
    business_label.hidden=NO;
    business_lbl1.textColor=[UIColor blackColor];
    business_lbl2.textColor=[UIColor blackColor];
    type=2;
    
}

-(void)create
{
    
     whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    
    [UIView animateWithDuration:0.0 animations:^{
        
        create.userInteractionEnabled = NO;
        self.view.userInteractionEnabled = NO;
        
        loader = [[UIActivityIndicatorView alloc] init];
        loader.center = CGPointMake(self.view.center.x, self.view.center.y-65);
        loader.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        [loader setColor:[UIColor blackColor]];
        [mainview addSubview:loader];
        [loader startAnimating];
        
    }completion:^(BOOL finished){
        
        groupdict=[[NSDictionary alloc]init];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        if ([[group_name.text stringByTrimmingCharactersInSet:whitespace] length]>0) {
            if ([descp.text isEqualToString:@"Description"]||[descp.text isEqualToString:@""] )
            {
                groups_url =[NSString stringWithFormat:@"https://budnav.com/ext/?action=group-add&group_name=%@&group_type=%d&access_token=%@&device_id=%@",[group_name.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],type,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
                //DebugLog(@"groups url string ------ %@",groups_url);
                
            }
            else
            {
                groups_url =[NSString stringWithFormat:@"https://budnav.com/ext/?action=group-add&group_name=%@&group_type=%d&group_description=%@&access_token=%@&device_id=%@",[group_name.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],type,[descp.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
                //DebugLog(@"groups url string ------ %@",groups_url);
                
            }
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:groups_url]];
            if (![encodedString isKindOfClass:[NSNull class]]&&encodedString.length>0&&![encodedString isEqualToString:@"nil"]) {
                params = [[NSString alloc] initWithFormat:@"image=%@",[encodedString stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"]];
            }
            
            [request setHTTPMethod:@"POST"];
            [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            
            NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
            
            
            if(theConnection)
            {
                responseData = [NSMutableData data];
            }
            else
            {
            }
            
        }
        else
        {
            [loader removeFromSuperview];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please enter a group name!" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            create.userInteractionEnabled = YES;
            self.view.userInteractionEnabled = YES;
            
        }
        
    }];
    
}

- (void) Start
{
    act.hidden = NO;
    [act startAnimating];
}


-(void)selectphoto
{
    [act startAnimating];
    
    [self performSelector:@selector(Start) withObject:nil afterDelay:0];
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate=self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [act stopAnimating];
    
    [self presentViewController:picker animated:YES completion:nil];
}

#pragma mark - UINavigationControllerDelegate
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}


- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
}


#pragma mark-nsurlconnection delegates
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data
{
    [responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    DebugLog(@"connection failed");
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *error;
    if (responseData != nil)
    {
        groupdict = [NSJSONSerialization JSONObjectWithData:responseData //1
                                                    options:kNilOptions
                                                      error:&error];
        NSString *check=[NSString stringWithFormat:@"%@",[groupdict valueForKey:@"success"]];
        if ([check isEqualToString:@"1"])
        {
                        
            ConGroupsViewController *congrp=[[ConGroupsViewController alloc]init];
            
//            CATransition* transition = [CATransition animation];
//            
//            transition.duration = 0.4;
//            transition.type = kCATransitionPush;
//            transition.subtype = kCATransitionFade;
//            transition.subtype = kCATransitionFromRight;
//            
//            [[self navigationController].view.layer addAnimation:transition forKey:nil];
            
            [self.navigationController pushViewController:congrp animated:NO];
            self.tabBarController.tabBar.hidden = NO;
        }
        else if ([check isEqualToString:@"0"])
        {
            if (isAtLeast8)
            {
                UIAlertController * alert=   [UIAlertController
                                              alertControllerWithTitle:@"Warning"
                                              message:[NSString stringWithFormat:@"%@",[groupdict valueForKey:@"error"]]
                                              preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* cancel = [UIAlertAction
                                         actionWithTitle:@"Ok"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                             
                                         }];
                
                [alert addAction:cancel];
                
                [self presentViewController:alert animated:YES completion:nil];
            }
            if (isAtLeast7)
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Warning" message:[NSString stringWithFormat:@"%@",[groupdict valueForKey:@"error"]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }
            
        }
        
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    responseData = [NSMutableData data];
}


#pragma mark - Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^{
        
        UIImage *portraitImg = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        portraitImg = [self imageByScalingToMaxSize:portraitImg];
        
        DebugLog(@"%@",NSStringFromCGSize(portraitImg.size));
        
        pathss = [NSTemporaryDirectory() stringByAppendingPathComponent:@"image.png"];
        NSData *imageData = UIImagePNGRepresentation(portraitImg);
        //you can also use UIImageJPEGRepresentation(img,1); for jpegs
        [imageData writeToFile:pathss atomically:YES];
        
        // present the cropper view controller
        ImageCropperViewController *imgCropperVC = [[ImageCropperViewController alloc] initWithImage:portraitImg cropFrame:CGRectMake(35, (self.view.frame.size.height- (self.view.frame.size.width-70))/2, self.view.frame.size.width-70, self.view.frame.size.width-70)];
        imgCropperVC.delegate = self;
        [self presentViewController:imgCropperVC animated:YES completion:nil];
        
        
    }];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

#pragma mark- method to resize photo
- (UIImage *)imageByScalingToMaxSize:(UIImage *)sourceImage {
    
    CGFloat btWidth = 0.0f;
    CGFloat btHeight = 0.0f;
    DebugLog(@"%f",sourceImage.size.height);
    if (sourceImage.size.height < ORIGINAL_MAX_HEIGHT)
        return sourceImage;
    
    if (sourceImage.size.height > sourceImage.size.width) {
        btHeight = ORIGINAL_MAX_HEIGHT;
        btWidth = sourceImage.size.width* (ORIGINAL_MAX_HEIGHT / sourceImage.size.height);
    }
    else
    {
        btHeight = ORIGINAL_MAX_HEIGHT;
        btWidth = sourceImage.size.width * (ORIGINAL_MAX_HEIGHT / sourceImage.size.height);
    }
    CGSize targetSize = CGSizeMake(btWidth, btHeight);
    return [self imageByScalingAndCroppingForSourceImage:sourceImage targetSize:targetSize];
    
}


- (UIImage *)imageByScalingAndCroppingForSourceImage:(UIImage *)sourceImage targetSize:(CGSize)targetSize1 {
    DebugLog(@"called scaling&croppingforsize");
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize1.width;
    CGFloat targetHeight = targetSize1.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    if (CGSizeEqualToSize(imageSize, targetSize1) == NO)
    {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor > heightFactor)
            scaleFactor = widthFactor; // scale to fit height
        else
            scaleFactor = heightFactor; // scale to fit width
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else
            if (widthFactor < heightFactor)
            {
                thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
            }
    }
    UIGraphicsBeginImageContext(targetSize1); // this will crop
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil) DebugLog(@"could not scale image");
    
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    return newImage;
}



#pragma mark- method to change to base64 string
- (NSString*)base64forData:(NSData*)theData {
    
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}


#pragma mark- VPImageCropperViewController delegates
- (void)imageCropper:(VPImageCropperViewController *)cropperViewController didFinished:(UIImage *)editedImage
{
    act = [[UIActivityIndicatorView alloc] init];
    act.center = cropperViewController.view.center;
    act.hidesWhenStopped=YES;
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [act setColor:[UIColor blackColor]];
    act.backgroundColor=[UIColor blueColor];
    [cropperViewController.view addSubview:act];
    act.layer.zPosition=2;
    //[NSThread detachNewThreadSelector: @selector(Start) toTarget:self withObject:nil];
    
    portraitImg1 =editedImage;
    //prof.contentMode=UIViewContentModeScaleAspectFill;
    NSData *imageData = UIImageJPEGRepresentation(editedImage,1.0);
    encodedString = [self base64forData:imageData];
    [self dismissViewControllerAnimated:YES completion:nil];
    prof.image=editedImage;
    image_shown=1;
    
}

- (void)imageCropperDidCancel:(VPImageCropperViewController *)cropperViewController {
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
    [self animateTextView:textView up: YES];
    if ([textView.text isEqualToString:@"Description"])
    {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
        [textView becomeFirstResponder];
    }
}


- (void)textViewDidEndEditing:(UITextView *)textView
{
    
    if ([textView.text isEqualToString:@""])
    {
        textView.text = @"Description";
        textView.textColor = [UIColor colorWithRed:101.0/255.0 green:101.0/255.0 blue:101.0/255.0 alpha:1.0];
    }
    [self animateTextView: textView up: NO];
    //self.navigationController.navigationBarHidden=NO;
}

- (void) animateTextView:(UITextView *)textView up:(BOOL) up
{
    const int movementDistance =-150; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    int movement= (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextView" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


#pragma mark-dismiss textview keyboard
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

#pragma mark- dismiss keyboard
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)changecoloragain
{
    DebugLog(@"CHANGE COLOR");
    bck_img.alpha = 1.0f;
    backlbl.alpha = 1.0f;
    back.alpha = 1.0f;
}

-(void)changecolor
{
    DebugLog(@"CHANGE COLOR1");
    bck_img.alpha = 0.5f;
    backlbl.alpha = 0.5f;
    back.alpha = 0.5f;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    groupname = textField.text;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
