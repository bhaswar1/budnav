#import "ConLocateGroupViewController.h"
#import "ConInviteViewController.h"
#import "ConAddFriendViewController.h"
#import "REVClusterAnnotationView.h"
#import "ConMapViewController.h"
#import "AppDelegate.h"
#import "ConProfileOwnViewController.h"
#import "ConAccountSettingsViewController.h"
#import "REVClusterMap.h"
#import "iToast.h"
#import "SVProgressHUD.h"
#import "DBManager.h"
#import "ConSyncLoaderViewController.h"
#import "CalloutView.h"
#import "myAnnotation.h"
#import "WildcardGestureRecognizer.h"
#import "ConRequestViewController.h"
#import "ConQROptionsViewController.h"
#import "ConNewProfileViewController.h"
#import "ConNewRequestsViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "ConFullSettingsViewController.h"
#import "ConPersonalProfileViewController.h"


@interface ConLocateGroupViewController ()<MKMapViewDelegate,CLLocationManagerDelegate,UIActionSheetDelegate, UIAlertViewDelegate>
{
    MKMapView *map_View;
    int userid,move,map_insertion, map_fetch, seconds, minutes, hours;
    NSMutableArray *loadagainarr;
    UIActivityIndicatorView *act;
    CalloutView *calloutView;
    UIView *calloutView1;
    UIImageView *logo_img, *bck_img;
    CLLocationCoordinate2D coord;
    NSString *mapname;
    //    TouchView* touchView;
    NSArray *retval;
    CLLocationManager *locationManager;
    BOOL locate_group, get_location;
    NSUserDefaults *prefs;
    CLLocationCoordinate2D coordinate;
    NSString *longitude, *latitude;
    UIButton *locator_button;
    UIImageView *locatorImgv;
    int count;
}


@end

@implementation ConLocateGroupViewController
@synthesize getmapvalue,mainview;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    count = 0;
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:NO];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    self.navigationController.navigationBarHidden=YES;
    
    retval = [[NSArray alloc]init];
    
    
    
    UIView *topview=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64)];
    [self.view addSubview:topview];
    //topview.backgroundColor=[UIColor blackColor];
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
    
    // Add the gradient to the view
    [topview.layer insertSublayer:gradient atIndex:0];
    
    
    logo_img = [[UIImageView alloc]init];
    logo_img.frame = CGRectMake(27+([UIScreen mainScreen].bounds.size.width-160)/2, 32, 101, 20);
    logo_img.backgroundColor = [UIColor clearColor];
    
    logo_img.image = [UIImage imageNamed:@"new_logo"];
    [topview addSubview:logo_img];
    
    locatorImgv = [[UIImageView alloc] init];
    locatorImgv.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-45, 29, 29, 22);
    locatorImgv.backgroundColor = [UIColor clearColor];
    locatorImgv.image = [UIImage imageNamed:@"Locate-group"];
    // [topview addSubview:locatorImgv];
    
    locator_button = [[UIButton alloc] init];
    locator_button.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-45, 29, 29, 22);
    [locator_button setBackgroundImage:[UIImage imageNamed:@"Locate-group"] forState:UIControlStateNormal];
    
    // [locator_button setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:0.3f]] forState:UIControlStateHighlighted];
    
    
    [locator_button addTarget:self action:@selector(locationUpdate) forControlEvents:UIControlEventTouchUpInside];
    [locator_button setBackgroundColor:[UIColor clearColor]];
    [topview addSubview:locator_button];
    
    bck_img = [[UIImageView alloc]initWithFrame:CGRectMake(15,32,12,20)];//(13, 39, 10, 22)];
    bck_img.image=[UIImage imageNamed:@"back3"];
    [self.view addSubview:bck_img];
    UIButton *helpbt = [UIButton buttonWithType:UIButtonTypeCustom];
    helpbt.frame = CGRectMake(0, 0, logo_img.frame.origin.x
                              -1, 64);//(10, 35, 8, 17);
    helpbt.backgroundColor=[UIColor clearColor];
    [helpbt setTitle:nil forState:UIControlStateNormal];  //Back button.png
    
    [helpbt addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDown];
    [helpbt addTarget:self action:@selector(changecoloragain) forControlEvents:UIControlEventTouchDragExit];
    [helpbt addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDragEnter];
    
    [helpbt addTarget:self action:@selector(goback) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:helpbt];
    
    
//  [self CurrentLocationIdentifier];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    move=0;
    map_insertion=0;
    map_fetch=0;
    calloutView = (CalloutView *)[[[NSBundle mainBundle] loadNibNamed:@"calloutView" owner:self options:nil] objectAtIndex:0];
    calloutView.userInteractionEnabled=YES;
    for (UIView *subview in calloutView.subviews ){
        subview.userInteractionEnabled=YES;
    }
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(maptap1:)];
    tap.delegate=self;
    [calloutView addGestureRecognizer:tap];
    
    
    
    loadagainarr = [[NSMutableArray alloc]init];
    
    //    newarr1 = [[NSMutableArray alloc]init];
    
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:NO];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    self.navigationController.navigationBarHidden=YES;
    
//    UIView *topview=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64)];
//    [self.view addSubview:topview];
//    //topview.backgroundColor=[UIColor blackColor];
//    
//    UIColor *darkOp =
//    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
//    UIColor *lightOp =
//    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
//    
//    // Create the gradient
//    CAGradientLayer *gradient = [CAGradientLayer layer];
//    
//    // Set colors
//    gradient.colors = [NSArray arrayWithObjects:
//                       (id)lightOp.CGColor,
//                       (id)darkOp.CGColor,
//                       nil];
//    
//    // Set bounds
//    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
//    
//    // Add the gradient to the view
//    [topview.layer insertSublayer:gradient atIndex:0];
//    
//    
//    logo_img = [[UIImageView alloc]init];
//    logo_img.frame = CGRectMake(27+([UIScreen mainScreen].bounds.size.width-160)/2, 32, 101, 20);
//    logo_img.backgroundColor = [UIColor clearColor];
//    
//    logo_img.image = [UIImage imageNamed:@"new_logo"];
//    [topview addSubview:logo_img];
//    
//    locatorImgv = [[UIImageView alloc] init];
//    locatorImgv.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-45, 29, 29, 22);
//    locatorImgv.backgroundColor = [UIColor clearColor];
//    locatorImgv.image = [UIImage imageNamed:@"Locate-group"];
//   // [topview addSubview:locatorImgv];
//    
//    locator_button = [[UIButton alloc] init];
//    locator_button.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-45, 29, 29, 22);
//    [locator_button setBackgroundImage:[UIImage imageNamed:@"Locate-group"] forState:UIControlStateNormal];
//    
//   // [locator_button setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:0.3f]] forState:UIControlStateHighlighted];
//
//    
//    [locator_button addTarget:self action:@selector(locationUpdate) forControlEvents:UIControlEventTouchUpInside];
//    [locator_button setBackgroundColor:[UIColor clearColor]];
//    [topview addSubview:locator_button];
//    
//    bck_img = [[UIImageView alloc]initWithFrame:CGRectMake(15,32,12,20)];//(13, 39, 10, 22)];
//    bck_img.image=[UIImage imageNamed:@"back3"];
//    [self.view addSubview:bck_img];
//    UIButton *helpbt = [UIButton buttonWithType:UIButtonTypeCustom];
//    helpbt.frame = CGRectMake(0, 0, logo_img.frame.origin.x
//                              -1, 64);//(10, 35, 8, 17);
//    helpbt.backgroundColor=[UIColor clearColor];
//    [helpbt setTitle:nil forState:UIControlStateNormal];  //Back button.png
//    
//    [helpbt addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDown];
//    [helpbt addTarget:self action:@selector(changecoloragain) forControlEvents:UIControlEventTouchDragExit];
//    [helpbt addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDragEnter];
//    
//    [helpbt addTarget:self action:@selector(goback) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:helpbt];
    
    
    //[[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        if (![[NSUserDefaults standardUserDefaults] boolForKey:@"profiletaverse"])
        {
            mainview = [[UIView alloc]initWithFrame:CGRectMake(0, 64, [UIScreen mainScreen].bounds.size.width, self.view.frame.size.height)];
            [self.view addSubview:mainview];
            mainview.backgroundColor=[UIColor whiteColor];
            
            act = [[UIActivityIndicatorView alloc] init];
            act.center = self.view.center;
            [act startAnimating];
            act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
            [act setColor:[UIColor blackColor]];
            [mainview addSubview:act];
            
   
            prefs = [NSUserDefaults standardUserDefaults];
            
            CLLocation *location = map_View.userLocation.location;
            DebugLog(@"get dir lat current: %f - long current: %f", location.coordinate.latitude, location.coordinate.longitude);
            _mapView =[[REVClusterMapView alloc] initWithFrame:CGRectMake(0, 0,[[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height)];
            _mapView.delegate = self;
            
            [mainview addSubview:_mapView];
            
//            coordinate.latitude = 51.22;
//            coordinate.longitude = 4.39625;
            
            [_mapView setShowsUserLocation:YES];
            
            locationManager = [[CLLocationManager alloc] init];
            
            [locationManager setDelegate:self];
            
            // we have to setup the location maanager with permission in later iOS versions
            if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
                [locationManager requestWhenInUseAuthorization];
            }
            
            [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
            [locationManager startUpdatingLocation];
            
            
//            CLLocationManager *locManager = [[CLLocationManager alloc] init];
//            [locManager setDelegate:self];
//            [locManager setDesiredAccuracy:kCLLocationAccuracyBest];
//            
//             NSMutableArray *pins = [NSMutableArray array];
//            
//             MKCoordinateRegion region;
//            MKCoordinateSpan span;
//            span.latitudeDelta = 8;
//            span.longitudeDelta = 8;
//            CLLocationCoordinate2D location1;
//            location1.latitude = [latitude doubleValue];
//            location1.longitude = [longitude doubleValue];
//            region.span = span;
//            region.center = location1;
//            [_mapView setRegion:region animated:YES];
            
            if (!locate_group)
            {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    
                    [act startAnimating];
                }];
                
                [[NSOperationQueue new] addOperationWithBlock:^{
                    
                    DebugLog(@"hmmmmmm 1st map");
                    newarr1 = [[NSMutableArray alloc]init];
                    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=group-location_get&group_id=%@&access_token=%@&device_id=%@",_group_id,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
                    
                    DebugLog(@"profile url: %@",urlString1);
                    
                    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
                    
                    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
                    
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        
                        [act stopAnimating];
                        
                        if (signeddataURL1 == nil)
                        {
                            alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                                               message:nil
                                                              delegate:self
                                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                            [act removeFromSuperview];
                        }
                        else
                        {
                            NSError *error =nil;
                            json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                     
                                                                    options:kNilOptions
                                     
                                                                      error:&error];
                            DebugLog(@"json returns: %@",json1);
                            
                            NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
                            DebugLog(@"err  %@",errornumber);
                            if (![errornumber isEqualToString:@"0"])
                            {
                                DebugLog(@"if if");
                                NSString *err_str = [json1 objectForKey:@"error"];
                                alert = [[UIAlertView alloc] initWithTitle:@"Please wait!"
                                                                   message:err_str
                                                                  delegate:self
                                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                                [act removeFromSuperview];
                            }
                            
                            if ([[json1 objectForKey:@"details"] isKindOfClass:[NSNull class]] || [json1 objectForKey:@"details"] == (id)[NSNull null] ||[[json1 objectForKey:@"details"] count] ==0)
                            {
                                alert = [[UIAlertView alloc] initWithTitle:@"Click on the refresh button in the top right corner to update current locations!"
                                                                   message:nil
                                                                  delegate:self
                                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                                [alert show];
                            }
                            else
                            {
                                //                        NSDictionary *newdict= [json1 objectForKey:@"details"];
                                for (NSDictionary *reqdict in [json1 objectForKey:@"details"])
                                {
                                    [newarr1 addObject:reqdict];
                                }
                                DebugLog(@"map list array has: %@",newarr1);
                                
                                
                                int i=0;
                                NSMutableArray *pins = [NSMutableArray array];
                                while (i<[newarr1 count])
                                {
                                    double lat=[[[newarr1 objectAtIndex:i]objectForKey:@"latitude"] doubleValue];
                                    double longi=[[[newarr1 objectAtIndex:i]objectForKey:@"longitude"] doubleValue];
                                    
                                    DebugLog(@"lati=== %f",lat);
                                    DebugLog(@"long=== %f",longi);
                                    
                                    NSString *time;
                                    [self timeFormatted:[[[newarr1 objectAtIndex:i] objectForKey:@"timestamp_difference"] intValue]];
                                    if (hours>0)
                                    {
                                        if (hours>24)
                                        {
                                            int rem = hours%24;
                                            time = [NSString stringWithFormat:@"%dd",rem];
                                            
                                        }
                                        else
                                        {
                                            time = [NSString stringWithFormat:@"%dh",hours];
                                        }
                                    }
                                    else if (minutes>0)
                                    {
                                        time = [NSString stringWithFormat:@"%dm",minutes];
                                    }
                                    else
                                    {
                                        time = [NSString stringWithFormat:@"%ds",seconds];
                                    }
                                    
                                    DebugLog(@"Time:%@",time);
                                    
                                    
                                    CLLocationCoordinate2D newCoord = {lat, longi};
                                    REVClusterPin *pin = [[REVClusterPin alloc] init];
                                    pin.title = [NSString stringWithFormat:@"%@ %@",[[newarr1 objectAtIndex:i]objectForKey:@"name"],[[newarr1 objectAtIndex:i]objectForKey:@"surname"]];
                                    pin.subtitle = [NSString stringWithFormat:@"was here %@ ago",time];
                                    mapname=pin.title;
                                    pin.coordinate = newCoord;
                                    [pins addObject:pin];
                                    
                                    i++;
                                }
                                [_mapView addAnnotations:pins];
                                //            [SVProgressHUD dismiss];
                                [act removeFromSuperview];
                                
                                [self performSelectorOnMainThread:@selector(startDBInsert)
                                                       withObject:nil
                                                    waitUntilDone:YES];
                                
                                [prefs setObject:@"no" forKey:@"firstMap"];
                            }
                        }
                    }];
                }];
            }
            
            else
            {
                DebugLog(@"1st time er por theke");
                retval= [[NSArray alloc]init];
                
                retval = [[NSUserDefaults standardUserDefaults]objectForKey:@"locdata"];
                DebugLog(@"ciiijj : %@", retval);
                
                
                newarr1 = [[NSMutableArray alloc]init];
                newarr1 = [retval mutableCopy];
                _mapView.showsUserLocation=YES;
                
                int i=0;
                
                NSMutableArray *pins = [NSMutableArray array];
                while (i<[newarr1 count])
                {
                    double lat=[[[newarr1 objectAtIndex:i]objectForKey:@"latitude"] doubleValue];
                    double longi=[[[newarr1 objectAtIndex:i]objectForKey:@"longitude"] doubleValue];
                    
                    NSString *time;
                    [self timeFormatted:[[[newarr1 objectAtIndex:i] objectForKey:@"timestamp_difference"] intValue]];
                    if (hours>0)
                    {
                        if (hours>24)
                        {
                            int rem = hours%24;
                            time = [NSString stringWithFormat:@"%dd",rem];
                            
                        }
                        else
                        {
                            time = [NSString stringWithFormat:@"%dh",hours];
                        }
                    }
                    else if (minutes>0)
                    {
                        time = [NSString stringWithFormat:@"%dm",minutes];
                    }
                    else
                    {
                        time = [NSString stringWithFormat:@"%ds",seconds];
                    }
                    
                    DebugLog(@"Time:%@",time);
                    
                    CLLocationCoordinate2D newCoord = {lat, longi};
                    REVClusterPin *pin = [[REVClusterPin alloc] init];
                    pin.title = [NSString stringWithFormat:@"%@ %@",[[newarr1 objectAtIndex:i]objectForKey:@"name"],[[newarr1 objectAtIndex:i]objectForKey:@"surname"]];
                    pin.subtitle = [NSString stringWithFormat:@"was here %@ ago",time];
                    pin.coordinate = newCoord;
                    
                    [pins addObject:pin];
                    
                    i++;
                }
                
                [_mapView addAnnotations:pins];
                //        [SVProgressHUD dismiss];
                [act removeFromSuperview];
                
                NSTimer *timerr;
                timerr = [NSTimer scheduledTimerWithTimeInterval:3.0
                                                          target:self
                                                        selector:@selector(targetMethod:)
                                                        userInfo:nil
                                                         repeats:NO];
            }
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"profiletaverse"];
            _mapView.delegate = self;
        }
   // }];
}

-(void)locationUpdate
{
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose Any Option" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Update my location",@"Request group locations", nil];
    
    actionSheet.tag = 201;
    [actionSheet showInView:mainview];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //    [actionSheet removeFromSuperview];
    if (actionSheet.tag == 201){
        
        if (buttonIndex == 0) {
            [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
            DebugLog(@"Update Own Location");
            [self CurrentLocationIdentifier];
            if (get_location)
            {
                [self ownLocationUpdate];
            }
            
        }
        else if (buttonIndex == 1)
        {
            [actionSheet dismissWithClickedButtonIndex:1 animated:YES];
            DebugLog(@"Request Others Location");
            
            [self CurrentLocationIdentifier];
            if (get_location)
            {
                [self requestOthersLocation];
            }
            
        }
        else{
            DebugLog(@"dismissWithClickedButtonIndex");
            [actionSheet dismissWithClickedButtonIndex:2 animated:YES];
        }
    }
}

-(void)ownLocationUpdate
{
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self startLoader];
        locator_button.userInteractionEnabled = NO;
    }];
    
    [[NSOperationQueue new] addOperationWithBlock:^{
        
        NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=group-location_update&group_id=%@&latitude=%@&longitude=%@&access_token=%@&device_id=%@",_group_id,latitude,longitude,[[NSUserDefaults standardUserDefaults] objectForKey:@"access_token"],[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"]]; //&thumb=true
        
        DebugLog(@"Location Update url: %@",urlString1);
        
        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
//            [act stopAnimating];
            [self stopLoader];
            locator_button.userInteractionEnabled = YES;
            
            if (signeddataURL1 == nil)
            {
                alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                                   message:nil
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                [alert show];
                //        [SVProgressHUD dismiss];
                [act removeFromSuperview];
            }
            else
            {
                NSError *error =nil;
                json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                         
                                                        options:kNilOptions
                         
                                                          error:&error];
                DebugLog(@"Locatiobn Request Update json returns: %@",json1);
                
                DebugLog(@"json returns: %@",json1);
                
                NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
                DebugLog(@"err  %@",errornumber);
                if (![errornumber isEqualToString:@"0"])
                {
                    NSString *err_str1 = [json1 objectForKey:@"error"];
                    alert = [[UIAlertView alloc] initWithTitle:@"Please wait!"
                                                       message:err_str1
                                                      delegate:self
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                    [alert show];
                    [act removeFromSuperview];
                    
                }
            else
                {
                    alert = [[UIAlertView alloc] initWithTitle:@"Location successfully updated!"
                                                       message:@""
                                                      delegate:self
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                    [alert show];
                    [act removeFromSuperview];
                    [self viewDidAppear:YES];
                }
                
            }
            
        }];
    }];
}

-(void)requestOthersLocation
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//        [act startAnimating];
        [self startLoader];
        locator_button.userInteractionEnabled = NO;
    }];
    
    [[NSOperationQueue new] addOperationWithBlock:^{
        
        NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=group-location_request&group_id=%@&latitude=%@&longitude=%@&access_token=%@&device_id=%@",_group_id,latitude,longitude,[[NSUserDefaults standardUserDefaults] objectForKey:@"access_token"],[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"]]; //&thumb=true
        
        DebugLog(@"Location Update url: %@",urlString1);
        
        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
//            [act stopAnimating];
            [self stopLoader];
            locator_button.userInteractionEnabled = YES;
            
            if (signeddataURL1 == nil)
            {
                alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                                   message:nil
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                [alert show];
                //        [SVProgressHUD dismiss];
//                [act removeFromSuperview];
            }
            else
            {
                NSError *error =nil;
                json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                         
                                                        options:kNilOptions
                         
                                                          error:&error];
                DebugLog(@"Locatiobn Request Update json returns: %@",json1);
                
                DebugLog(@"json returns: %@",json1);
                
                NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
                DebugLog(@"err  %@",errornumber);
                if (![errornumber isEqualToString:@"0"])
                {
                    NSString *err_str1 = [json1 objectForKey:@"error"];
                    alert = [[UIAlertView alloc] initWithTitle:@"Please wait!"
                                                       message:err_str1
                                                      delegate:self
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                    [alert show];
                    [act removeFromSuperview];
                    
                }
                else
                {
                    alert = [[UIAlertView alloc] initWithTitle:@"Location requests were sent successfully!"
                                                       message:@""
                                                      delegate:self
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                    [alert show];
                    [act removeFromSuperview];
                }
                
            }
            
        }];
    }];
}


- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)aUserLocation {
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 8;
    span.longitudeDelta = 8;
    CLLocationCoordinate2D location;
    location.latitude = aUserLocation.coordinate.latitude;
    location.longitude = aUserLocation.coordinate.longitude;
    region.span = span;
    region.center = location;
   // [aMapView setRegion:region animated:YES];
    
    MKAnnotationView* annotationView = [aMapView viewForAnnotation:aUserLocation];
    annotationView.canShowCallout = NO;
}

+(void)chngpostion
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Data" object:Nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getData:) name:@"Data" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(navtopage:) name:@"pagenav" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startDBInsertAgain) name:@"db_busy" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestreceivedaction) name:@"Requestreceived_push" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(acceptedrequestaction) name:@"Accepted_push" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AcceptedRequest) name:@"Accepted_request" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AddFriend) name:@"NewConnection" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ConnectionType) name:@"NewConnectionType" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AcceptedRequest) name:@"TypeChange" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shareLocation) name:@"ShareLocation" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdateInformation) name:@"UpdateInfo" object:nil];
    
}

-(void)UpdateInformation
{
    ConPersonalProfileViewController *PVC = [[ConPersonalProfileViewController alloc]init];
    PVC.toUpdateInfo = @"YES";
    [self.navigationController pushViewController:PVC animated:YES];
  //  [[NSNotificationCenter defaultCenter] postNotificationName:@"Update Info" object:Nil];
    
}


-(void)shareLocation
{
    ConLocateGroupViewController *conLocate = [[ConLocateGroupViewController alloc]init];
    conLocate.group_id=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"sharedLocGrpId"]];
    [self.navigationController pushViewController:conLocate animated:YES];
}

-(void)ConnectionType
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDel.newrequestRedirect)
    {
        ConNewRequestsViewController *mng = [[ConNewRequestsViewController alloc]init];
        mng.userid = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userforrequest"] intValue];
        
        appDel.newrequestRedirect = NO;
        [self.navigationController pushViewController:mng animated:YES];
    }
    
}


-(void)AddFriend
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDel.newUser)
    {
        ConAddFriendViewController *con = [[ConAddFriendViewController alloc]init];
        
        appDel.newUser = NO;
        
        //   [[self navigationController].view.layer addAnimation:transition forKey:nil];
        [[self navigationController] pushViewController:con animated:YES];
    }
    
}

-(void)AcceptedRequest
{
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (del.profileRedirect)
    {
        ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
        // con.other=@"yes";
        con.request = YES;
        
        [self.tabBarController.tabBar setHidden:YES];
        del.profileRedirect = NO;
        [self.navigationController pushViewController:con animated:YES];
    }
}

-(void)requestreceivedaction
{
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
    con.other=@"yes";
    con.uid=[NSString stringWithFormat:@"%@",del.pushtoprofileid];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"profiletaverse"];
    
    [self.navigationController pushViewController:con animated:YES];
}

-(void)acceptedrequestaction
{
    ConRequestViewController *con = [[ConRequestViewController alloc]init];
    
    [self.navigationController pushViewController:con animated:YES];
}


-(void)navtopage: (NSNotification *)notification
{
    NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
    if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"4"])
    {
        ConAccountSettingsViewController *con = [[ConAccountSettingsViewController alloc]init];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"6"])
    {
        ConInviteViewController *con = [[ConInviteViewController alloc]init];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"5"])
    {
        ConSyncLoaderViewController *con = [[ConSyncLoaderViewController alloc]init];
        //        [self.navigationController pushViewController:con animated:NO];
        
        [self presentViewController:con animated:NO completion:nil];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"2"])
    {
        //ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
        ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"profiletaverse"];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"3"])
    {
        ConQROptionsViewController *con = [[ConQROptionsViewController alloc]init];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    [mainview setFrame:CGRectMake(0, mainview.frame.origin.y, mainview.frame.size.width, mainview.frame.size.height)];
    //    [prefs setObject:@"111" forKey:@"whichpage"];
}

-(void)getData:(NSNotification *)notification {
    
    if(move == 0) {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake(280, mainview.frame.origin.y, mainview.frame.size.width, mainview.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             move = 1;
                         }];
    } else {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake(0, (mainview.frame.origin.y),mainview.frame.size.width, mainview.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             move = 0;
                         }];
    }
}

- (void)getDirections
{
    CLLocationCoordinate2D start = {37.774929, -122.419416 };
    CLLocationCoordinate2D end = {40.714353, -74.005973};
    
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
    
    MKMapItem *source_mapitem =[[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:start addressDictionary:nil]];
    [source_mapitem setName:@"Source"];
    
    MKMapItem *destination_mapitem = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:end addressDictionary:nil]];
    
    [destination_mapitem setName:@"Destination"];
    request.source = source_mapitem;
    request.destination = destination_mapitem;
    
    request.requestsAlternateRoutes = YES;
    MKDirections *directions =
    [[MKDirections alloc] initWithRequest:request];
    
    [directions calculateDirectionsWithCompletionHandler:
     ^(MKDirectionsResponse *response, NSError *error) {
         if (error) {
             DebugLog(@" error is %@",error.localizedDescription);
         } else {
             DebugLog(@" here we came");
             [self showRoute:response];
         }
     }];
}

-(void)showRoute:(MKDirectionsResponse *)response
{
    for (MKRoute *route in response.routes)
    {
        [map_View addOverlay:route.polyline level:MKOverlayLevelAboveLabels];
        for (MKRouteStep *step in route.steps)
        {
            DebugLog(@" here %@", step.instructions);
        }
    }
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id < MKOverlay >)overlay
{
    MKPolylineRenderer *renderer =
    [[MKPolylineRenderer alloc] initWithOverlay:overlay];
    renderer.strokeColor = [UIColor blueColor];
    renderer.lineWidth = 2.0;
    return renderer;
}


- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views {
    MKAnnotationView *aV;
    for (aV in views) {
        if ([aV.annotation isKindOfClass:[MKUserLocation class]]) {
            MKAnnotationView* annotationView = aV;
            annotationView.canShowCallout = NO;
        }
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    MKAnnotationView *annView;
    
    if([annotation class] == MKUserLocation.class) {
        //userLocation = annotation;
        annView.canShowCallout = NO;
        return nil;
    }
    
    REVClusterPin *pin = (REVClusterPin *)annotation;
    
    if( [pin nodeCount] > 0 ){
        pin.title = @"___";
        
        
        annView = (REVClusterAnnotationView*)
        [mapView dequeueReusableAnnotationViewWithIdentifier:@"cluster"];
        
        if( !annView )
            annView = (REVClusterAnnotationView*)[[REVClusterAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"cluster"];
        
        annView.image = [UIImage imageNamed:@"locator.png"];
        
        [(REVClusterAnnotationView*)annView setClusterText:
         [NSString stringWithFormat:@"%lu",(unsigned long)[pin nodeCount]]];
        
        annView.canShowCallout = NO;
    } else {
        annView = [mapView dequeueReusableAnnotationViewWithIdentifier:@"pin"];
        
        annView = [[MKAnnotationView alloc] init]; //MKAnnotationView 18.11
        annView.canShowCallout = NO;
        annView.image = [UIImage imageNamed:@"locator1.png"];
        annView.calloutOffset = CGPointMake(0, 0);
        
        
        UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        annView.rightCalloutAccessoryView = rightButton;
        annView.rightCalloutAccessoryView.frame=CGRectMake(0, 0, 0, 0);
        
        UIImageView *iconView = [[UIImageView alloc] init];
        iconView.frame = CGRectMake(0.0f, 0.0f,0, 0);
        annView.leftCalloutAccessoryView = iconView;
        iconView.backgroundColor=[UIColor whiteColor];
        annView.userInteractionEnabled=YES;
        if (annotation == mapView.userLocation)
        {
            annView.image= [UIImage imageNamed:@"locatorown.png"];
            annView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomPinAnnotationView"]; ////MKAnnotationView 18.11
        }
        //            if(typec==1)
        //            {
        DebugLog(@"(int)annotation.subtitle===%@",annotation.subtitle);
        for(int newi=0;newi<[newarr1 count];newi++)
        {
            DebugLog(@"pin.title = %@", pin.title);
            
            NSString *time;
            [self timeFormatted:[[[newarr1 objectAtIndex:newi] objectForKey:@"timestamp_difference"] intValue]];
            if (hours>0)
            {
                if (hours>24)
                {
                    int rem = hours%24;
                    time = [NSString stringWithFormat:@"%dd",rem];
                    
                }
                else
                {
                    time = [NSString stringWithFormat:@"%dh",hours];
                }
            }
            else if (minutes>0)
            {
                time = [NSString stringWithFormat:@"%dm",minutes];
            }
            else
            {
                time = [NSString stringWithFormat:@"%ds",seconds];
            }
            
            DebugLog(@"Time:%@",time);
            
            
            NSString *webstr = [NSString stringWithFormat:@"%@ %@",[[newarr1 objectAtIndex:newi]objectForKey:@"name"],[[newarr1 objectAtIndex:newi]objectForKey:@"surname"]];
            DebugLog(@"webstr = %@",webstr);
            //            if([[[newarr1 objectAtIndex:newi]objectForKey:@"name"]isEqualToString:namefirst])
            if ([webstr isEqualToString:pin.title])
            {
                if([[[newarr1 objectAtIndex:newi]objectForKey:@"business"]intValue] ==1)
                {
                    iconView.image=[UIImage imageNamed:@"icon_busi.png"];
                    annView.image = [UIImage imageNamed:@"locator2.png"];
                }
                else
                {
                    iconView.image=[UIImage imageNamed:@"icon_friend.png"];
                    annView.image = [UIImage imageNamed:@"locator1.png"];
                    DebugLog(@"friend");
                }
                userid =[[[newarr1 objectAtIndex:newi] objectForKey:@"id"]intValue];
                annView.tag = [[[newarr1 objectAtIndex:newi] objectForKey:@"id"]intValue];
                break;
            }
        }
        annView.canShowCallout = YES;
        annView.calloutOffset = CGPointMake(-6.0, 0.0);
    }
    if (annView != nil)
    {
        
        CALayer* layer = annView.layer;
        [layer removeAllAnimations];
    }
    return annView;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    DebugLog(@"REVMapViewController mapView didSelectAnnotationView: %d",userid);
    
    if ([view isKindOfClass:[REVClusterAnnotationView class]])
    {
        CLLocationCoordinate2D centerCoordinate = [(REVClusterPin *)view.annotation coordinate];
        MKCoordinateSpan newSpan =
        MKCoordinateSpanMake(mapView.region.span.latitudeDelta/4.0,
                             mapView.region.span.longitudeDelta/4.0);
        
        [mapView setRegion:MKCoordinateRegionMake(centerCoordinate, newSpan)
                  animated:YES];
    }
}


-(void)maptap1: (UIGestureRecognizer *)sender
{
    DebugLog(@"bhaswar yes: %ld",(long)sender.view.tag);
    ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
    con.other=@"yes";
    con.mapReturn = @"mapBack";
    con.uid=[NSString stringWithFormat:@"%ld",(long)sender.view.tag];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"profiletaverse"];
    
    [self.navigationController pushViewController:con animated:YES];
}


- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    DebugLog(@"Tapped on %@ & Tapped user id: %ld", view.annotation.title, (long)view.tag);
    DebugLog(@"Tapped on %@ & Tapped user id: %ld", view.annotation.title, (long)userid);
    ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
    con.other=@"yes";
    con.mapReturn = @"mapBack";
    con.uid=[NSString stringWithFormat:@"%ld",(long)view.tag];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"profiletaverse"];
    
    if ([retval count]>0)
    {
        for (NSDictionary *tempDict in retval)
        {
            if ([[tempDict objectForKey:@"id"] intValue]==userid)
            {
                if ([[tempDict objectForKey:@"business"] intValue]==1)
                {
                    con.business_friend = @"yes";
                }
            }
        }
    }
    if (json1!=nil)
    {
        for (NSDictionary *reqdict in [json1 objectForKey:@"details"])
        {
            if ([[reqdict objectForKey:@"id"] intValue]==userid)
            {
                if ([[reqdict objectForKey:@"business"] intValue]==1)
                {
                    con.business_friend = @"yes";
                }
            }
        }
    }
    [self.navigationController pushViewController:con animated:YES];
}

-(void)viewDidDisappear:(BOOL)animated
{
    move=0;
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"profiletaverse"])
        [mainview removeFromSuperview];
    [super viewDidDisappear:YES];
}

-(void)loadmapfromweb
{
    loadagainarr = [[NSMutableArray alloc]init];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        prefs = [NSUserDefaults standardUserDefaults];
        DebugLog(@"hmmmmmm again load map");
        NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=group-location_get&group_id=%@&access_token=%@&device_id=%@",_group_id,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
        
        DebugLog(@"profile url: %@",urlString1);
        
        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
        
        if (signeddataURL1 == nil)
        {
            DebugLog(@"ekhanei to elona,mane data asheni");
            alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [act removeFromSuperview];
        }
        else
        {
            NSError *error =nil;
            json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                     
                                                    options:kNilOptions
                     
                                                      error:&error];
            DebugLog(@"json returns: %@",json1);
            
            NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
            DebugLog(@"err  %@",errornumber);
            if (![errornumber isEqualToString:@"0"])
            {
                DebugLog(@"if if");
                
                [act removeFromSuperview];
            }
            
            if ([[json1 objectForKey:@"details"] isKindOfClass:[NSNull class]] || [json1 objectForKey:@"details"] == (id)[NSNull null] ||[[json1 objectForKey:@"details"] count] ==0)
            {
                DebugLog(@"if called req");
                alert = [[UIAlertView alloc] initWithTitle:@"Click on the refresh button in the top right corner to update current locations!"
                                                   message:nil
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                [act removeFromSuperview];
            }
            else
            {
                //  NSDictionary *newdict= [json1 objectForKey:@"details"];
                for (NSDictionary *reqdict in [json1 objectForKey:@"details"])
                {
                    [loadagainarr addObject:reqdict];
                }
                //                DebugLog(@"map list array has: %@",loadagainarr);
            }
            DebugLog(@"loadagainarr ache: %lu",(unsigned long)[loadagainarr count]);
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            DebugLog(@"loadagainarr ekhon.. %lu",(unsigned long)[loadagainarr count]);
            if([loadagainarr count]>0)
            {
                [self performSelectorOnMainThread:@selector(startDBInsertAgain)
                                       withObject:nil
                                    waitUntilDone:YES];
                [self loaddatabase_data];
            }
        });
    });
}


-(void) loaddatabase_data
{
    DebugLog(@"1st time er por theke");
    retval = [[NSUserDefaults standardUserDefaults]objectForKey:@"locdata"];
    newarr1= [NSMutableArray new];
    newarr1 = [retval mutableCopy];
}

-(void)startDBInsert
{
    [[NSUserDefaults standardUserDefaults]setObject:newarr1 forKey:@"locdata"];
}

-(void)startDBInsertAgain
{
    DebugLog(@"start db insert");
    
    [[NSUserDefaults standardUserDefaults]setObject:loadagainarr forKey:@"locdata"];
}


-(void)goback
{
    int index;
    NSArray* navarr = [[NSArray alloc] initWithArray:self.navigationController.viewControllers];
    for(int i=0 ; i<[navarr count] ; i++)
    {
        if(![[navarr objectAtIndex:i] isKindOfClass:NSClassFromString(@"ConLocateGroupViewController")])
        {
            index = i;
        }
    }
    
    CATransition *transition = [CATransition animation];
    
    transition.duration = 0.4f;
    
    transition.type = kCATransitionFade;
    
    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    [self.navigationController popToViewController:[navarr objectAtIndex:index] animated:YES];
    
    
  //  [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)changecoloragain
{
    bck_img.alpha = 1.0f;
}

-(void)changecolor
{
    bck_img.alpha = 0.5f;
    
}

-(void)changelocatecoloragain
{
    locatorImgv.alpha = 1.0f;
    locator_button.alpha = 1.0f;
}
-(void)changelocatecolor
{
    locatorImgv.alpha = 0.5f;
    locator_button.alpha = 0.5f;
}

#pragma mark - Fetching Current Location
-(void)CurrentLocationIdentifier
{
    
    if (![CLLocationManager locationServicesEnabled]) {
        
        if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            
            get_location = false;
            
            alert = [[UIAlertView alloc] initWithTitle:@"App Permission Denied"
                     
                                               message:@"To re-enable, please go to Settings and turn on Location Service for this app."
                     
                                              delegate:nil
                     
                                     cancelButtonTitle:@"OK"
                     
                                     otherButtonTitles:nil];
            
            alert.delegate = self;
            
            alert.tag = 122;
            
            [alert show];
            
        }
        
        else
            
        {
            
            //---- For getting current gps location
            
            locationManager = [CLLocationManager new];
            
            locationManager.delegate = self;
            
            locationManager.distanceFilter = kCLDistanceFilterNone;
            
            locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            
            [locationManager startUpdatingLocation];
            
            //------
            
            if (![latitude isKindOfClass:[NSNull class]] && ![latitude isEqualToString:@"(null)"] && latitude!=nil && ![longitude isKindOfClass:[NSNull class]] && ![longitude isEqualToString:@"(null)"] && longitude!=nil)
            {
                count = 0;
                get_location = true;
            }
            else
            {
                if (count==3)
                {
                    count = 0;
                    get_location = false;
                    
                    alert = [[UIAlertView alloc] initWithTitle:@"Unable to Get Your Location. Please Try Again Later!"
                             
                                                       message:nil
                             
                                                      delegate:self
                             
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                    
                    [alert show];
                }
                else
                {
                    count++;
                    [self CurrentLocationIdentifier];
                }
                
                
            }
        }
        
    }
    
    else
        
    {
        
        NSLog(@"Location Services Enabled");
        
        if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            
            get_location = false;
            
            alert = [[UIAlertView alloc] initWithTitle:@"App Permission Denied"
                     
                                               message:@"To re-enable, please go to Settings and turn on Location Service for this app."
                     
                                              delegate:nil
                     
                                     cancelButtonTitle:@"OK"
                     
                                     otherButtonTitles:nil];
            
            alert.delegate = self;
            
            alert.tag = 122;
            
            [alert show];
            
        }
        
        else
            
        {
            
            //---- For getting current gps location
            
            locationManager = [CLLocationManager new];
            
            locationManager.delegate = self;
            
            locationManager.distanceFilter = kCLDistanceFilterNone;
            
            locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            
            [locationManager startUpdatingLocation];
            
            //------      
            
            if (![latitude isKindOfClass:[NSNull class]] && ![latitude isEqualToString:@"(null)"] && latitude!=nil && ![longitude isKindOfClass:[NSNull class]] && ![longitude isEqualToString:@"(null)"] && longitude!=nil)
            {
                count = 0;
                get_location = true;
            }
            else
            {
                if (count==3)
                {
                    DebugLog(@"Count:%d",count);
                    count = 0;
                    get_location = false;
                    
                    alert = [[UIAlertView alloc] initWithTitle:@"Unable to Get Your Location. Please Try Again Later!"
                             
                                                       message:nil
                             
                                                      delegate:self
                             
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                    
                    [alert show];

                }
                else
                {
                    count++;
                    [self CurrentLocationIdentifier];
                }
                
                
            }
        }
        
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex

{
    
    NSLog(@"buttonIndex:%ld",(long)buttonIndex);   
    
    if (alertView.tag == 122 && buttonIndex == 0)
    {
        //code for opening settings app in iOS 8
        
        [[UIApplication sharedApplication] openURL:[NSURL  URLWithString:UIApplicationOpenSettingsURLString]];
        
    }
    
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    DebugLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    DebugLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        
        DebugLog(@"Lat:%ld Lon:%ld",(long)currentLocation.coordinate.latitude,(long)currentLocation.coordinate.longitude);
        longitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        latitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
    }
}

- (void)timeFormatted:(int)totalSeconds
{
    // totalSeconds = totalSeconds/1000;
    seconds = totalSeconds % 60;
    minutes = (totalSeconds / 60) % 60;
    hours = totalSeconds / 3600;
}

-(UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

-(void)startLoader
{
    act = [[UIActivityIndicatorView alloc] init];
    act.center = self.view.center;
    [act startAnimating];
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [act setColor:[UIColor blackColor]];
    [self.view addSubview:act];
}

-(void)stopLoader
{
    [act removeFromSuperview];
}

@end