#import "ConNewRequestsViewController.h"
#import "AppDelegate.h"
#import "ConProfileOwnViewController.h"
#import "ConAccountSettingsViewController.h"
#import "ConInviteViewController.h"
#import <AddressBook/AddressBook.h>
#import "ConSyncLoaderViewController.h"
#import "ConRequestViewController.h"
#import "ConQROptionsViewController.h"
#import "UIImage+animatedGIF.h"
#import "ConNewProfileViewController.h"
#import "ConAddFriendViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"
#import "ConLocateGroupViewController.h"
#import "ConFullSettingsViewController.h"
#import "ConPersonalProfileViewController.h"


@interface ConNewRequestsViewController ()
{
    int move,status,userid;
    UIView *mainview;
    NSString *currentstatus_str, *table_type;
    NSDictionary *import_dict;
    UITableView *tab_request;
    NSMutableArray *connection_type_arr;
    UIButton *acceptchange, *denybt, *currentstatus;
    UIView *grayback;
    UIImageView *address_img;
    UIActivityIndicatorView *loader;
    UILabel *backLabel;
    UIImageView *bck_img, *logo_img;
}
@end
@implementation ConNewRequestsViewController
@synthesize imported_arr,userid;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"From ConNewRequestsViewController");
    self.view.backgroundColor = [UIColor whiteColor];
    
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    del.tabBarController.tabBar.hidden = YES;
    self.navigationController.navigationBarHidden=YES;
    
    //    loader = [[UIActivityIndicatorView alloc] init];
    //    loader.center = mainview.center;
    //    loader.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    //    [loader setColor:[UIColor blackColor]];
    //    [mainview addSubview:loader];
    //    [loader startAnimating];
    
    
    UIView *coverview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64)];
    //coverview.backgroundColor=[UIColor blackColor];
    [self.view addSubview:coverview];
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
    
    // Add the gradient to the view
    [coverview.layer insertSublayer:gradient atIndex:0];
    
    
    
    
    UIView *backbuttonextended = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 85, 64)];
    backbuttonextended.backgroundColor=[UIColor clearColor];
    [self.view addSubview:backbuttonextended];
    backbuttonextended.userInteractionEnabled=YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goback)];
    [backbuttonextended addGestureRecognizer:tap];
    
    bck_img=[[UIImageView alloc]initWithFrame:CGRectMake(15,32,12,20)];//(13, 39, 10, 22)];
    bck_img.image=[UIImage imageNamed:@"back3"];
    [self.view addSubview:bck_img];
    
    
    backLabel=[[UILabel alloc]initWithFrame:CGRectMake(bck_img.frame.origin.x+bck_img.frame.size.width+5, 30, 50, 20)];
    backLabel.text=@"Back";
    backLabel.textColor=[UIColor whiteColor];
    backLabel.contentMode=UIViewContentModeScaleAspectFit;
    //    backlbl.font=[UIFont systemFontOfSize:17];//fontWithName:@"ProximaNova-Regular" size:17];
    backLabel.backgroundColor=[UIColor clearColor];
   // [self.view addSubview:backLabel];
    
    logo_img = [[UIImageView alloc]init];
    logo_img.frame = CGRectMake(27+([UIScreen mainScreen].bounds.size.width-160)/2, 32, 101, 20);
    logo_img.backgroundColor = [UIColor clearColor];
    
    logo_img.image = [UIImage imageNamed:@"new_logo"];
    [self.view addSubview:logo_img];
    
    
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    
    self.screenName = @"Public contact";
    [[GAI sharedInstance].defaultTracker send:
     
     [[GAIDictionaryBuilder createEventWithCategory:@"Add contact"
                                             action:@"Public contact"
                                              label:nil
                                              value:nil] build]];
    move=0;
    status=0;
    connection_type_arr= [[NSMutableArray alloc]init];
    connection_type_arr= [ @[@"Friend", @"Business"] mutableCopy ];
    
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    [del.tabBarController.tabBar setFrame:CGRectMake(0, del.tabBarController.tabBar.frame.origin.y, del.tabBarController.tabBar.frame.size.width, del.tabBarController.tabBar.frame.size.height)];
    del.tabBarController.tabBar.hidden = YES;
    self.navigationController.navigationBarHidden=YES;
    
    
    UIButton *helpbt = [UIButton buttonWithType:UIButtonTypeCustom];
    helpbt.frame = CGRectMake(0, 0, logo_img.frame.origin.x-1, 64);//(10, 35, 8, 17);
    helpbt.backgroundColor=[UIColor clearColor];
    [helpbt setTitle:nil forState:UIControlStateNormal];  //Back button.png
    
    [helpbt addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDown];
    [helpbt addTarget:self action:@selector(changecoloragain) forControlEvents:UIControlEventTouchDragExit];
    [helpbt addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDragEnter];
    
    
    [helpbt addTarget:self action:@selector(goback) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:helpbt];
    
    mainview = [[UIView alloc]initWithFrame:CGRectMake(0, 64, [UIScreen mainScreen].bounds.size.width, self.view.frame.size.height-59)];
    [self.view addSubview:mainview];
    mainview.backgroundColor=[UIColor whiteColor];
    
    
    loader = [[UIActivityIndicatorView alloc] init];
    loader.center = CGPointMake(self.view.center.x, self.view.center.y-65);
    loader.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [loader setColor:[UIColor blackColor]];
    [mainview addSubview:loader];
    [loader startAnimating];
    
    //===============Gray Background==============//
    
    //    grayback = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 110.0f)];
    //    [grayback setBackgroundColor:[UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f]];
    //    [mainview addSubview:grayback];
    
    
    NSOperationQueue *operationQueue = [NSOperationQueue new];
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        
        NSError *error = nil;
        NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=profile&id=%d&access_token=%@&device_id=%@&image=true",userid,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
        
        DebugLog(@"profile url: %@",urlString1);
        
        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
        
        if (signeddataURL1!=nil) {
            
            NSDictionary *json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                   
                                                                  options:kNilOptions
                                                                    error:&error];
            DebugLog(@"1st json returns: %@",json1);
            
            NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
            DebugLog(@"err  %@",errornumber);
            
            if (![errornumber isEqualToString:@"0"])
            {
                DebugLog(@"if if");
                NSString *err_str = [json1 objectForKey:@"error"];
                alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                                   message:err_str
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                [alert show];
            }
            else
                import_dict = [json1 objectForKey:@"details"];
            
            
            grayback = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 110.0f)];
            [grayback setBackgroundColor:[UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f]];
            [mainview addSubview:grayback];
            
            
            
            UIImageView *prof_img = [[UIImageView alloc]initWithFrame:CGRectMake(15, 75-64, 90, 90)];
            [grayback addSubview:prof_img];
            
            base64String= [import_dict objectForKey:@"image"];
            if ([base64String length] >6)
            {
                NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
                //    NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
                UIImage *profilepic = [UIImage imageWithData:decodedData];
                prof_img.image=profilepic;
                prof_img.contentMode= UIViewContentModeScaleAspectFit;
                prof_img.clipsToBounds=YES;
            }
            
            NSDictionary *profdict= [json1 objectForKey:@"details"];
            NSDictionary *dict_profile = [profdict objectForKey:@"profile"];
            
            if (dict_profile != 0) {
                
                
                fullname= [NSString stringWithFormat:@"%@ %@",[dict_profile objectForKey:@"name"],[dict_profile objectForKey:@"surname"]];
                UILabel *prof_name = [[UILabel alloc]init]; //WithFrame:CGRectMake(112, 72, 196, 60)];
                [grayback addSubview:prof_name];
                prof_name.backgroundColor=[UIColor clearColor];
                prof_name.textColor=[UIColor blackColor];
                prof_name.text=fullname;
                //    prof_name.font=[UIFont fontWithName:@"Mark Simonson - Proxima Nova Alt Condensed Bold" size:19];
                prof_name.font=[UIFont fontWithName:@"ProximaNova-Bold" size:20];
                prof_name.numberOfLines=0;
                
                prof_name.frame=CGRectMake(115, 75-70, 196, 60);
                
                //================Location icon===========//
                
                address_img=[[UIImageView alloc]init];
                address_img.frame = CGRectMake(prof_img.frame.origin.x+prof_img.frame.size.width+10,prof_name.frame.origin.y + prof_name.frame.size.height-3,[UIImage imageNamed:@"addMapPin"].size.width,[UIImage imageNamed:@"addMapPin"].size.height);
                address_img.image=[UIImage imageNamed:@"addMapPin"];
                [grayback addSubview:address_img];
                
                NSString *location;
                if ([[dict_profile objectForKey:@"city"] isKindOfClass:[NSNull class]] || [dict_profile objectForKey:@"city"] == (id)[NSNull null] || [[dict_profile objectForKey:@"city"] length] == 0)
                {
                    location= [NSString stringWithFormat:@"%@",[dict_profile objectForKey:@"country"]];
                }
                else
                    location= [NSString stringWithFormat:@"%@, %@",[dict_profile objectForKey:@"city"],[dict_profile objectForKey:@"country"]];
                
                UILabel *locationlb = [[UILabel alloc]initWithFrame:CGRectMake(address_img.frame.origin.x+address_img.frame.size.width+7, prof_name.frame.origin.y + prof_name.frame.size.height+2, 210, 11)];
                [grayback addSubview:locationlb];
                locationlb.backgroundColor=[UIColor clearColor];
                locationlb.text=location;
                locationlb.font=[UIFont fontWithName:@"ProximaNova-Regular" size:14];
                locationlb.textAlignment=NSTextAlignmentLeft;
                locationlb.textColor=[UIColor colorWithRed:153.0f/255.0f green:153.0f/255.0f blue:153.0f/255.0f alpha:1];
                
                //[self performSelector:@selector(loadViews) withObject:nil afterDelay:1];
                [self loadViews];
                
            }else{
                
                
                
            }
            
            
        }
        else
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Server not responding!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
            
        }
        
        
    }];
    //    NSError *error=nil;
    
    
    //    [del showTabValues:YES];
}


-(void)loadViews
{
    [tab_request removeFromSuperview];
    [currentstatus removeFromSuperview];
    [acceptchange removeFromSuperview];
    [denybt removeFromSuperview];
    
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        
        
        currentstatus = [UIButton buttonWithType:UIButtonTypeCustom];
        currentstatus.frame = CGRectMake(15, 120, 75, 25);
        [mainview addSubview:currentstatus];
        currentstatus.layer.cornerRadius=5;
        currentstatus.backgroundColor=[UIColor clearColor];
        currentstatus.layer.borderWidth=1;
        currentstatus.layer.borderColor=[[UIColor grayColor]CGColor];
        [currentstatus setTitleColor:[UIColor colorWithRed:97.0f/255.0f green:132.0f/255.0f blue:83.0f/255.0f alpha:1] forState:UIControlStateNormal];
        
        UIButton *addfriend = [UIButton buttonWithType:UIButtonTypeCustom];
        addfriend.frame = CGRectMake(self.view.frame.size.width/2 - [UIImage imageNamed:@"addPersonalOff"].size.width/2, 130, [UIImage imageNamed:@"addPersonalOff"].size.width, [UIImage imageNamed:@"addPersonalOff"].size.height);
        addfriend.layer.cornerRadius = 5;
        addfriend.clipsToBounds = YES;
        [addfriend setBackgroundColor:[UIColor colorWithRed:(40.0f/255.0f) green:(170.0f/255.0f) blue:(185.0f/255.0f) alpha:1.0f]];
        [addfriend setTitle:@"Request personal connection" forState:UIControlStateNormal];
        [addfriend.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
        [addfriend setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        //[addfriend setBackgroundImage:[UIImage imageNamed:@"addPersonalOff"] forState:UIControlStateNormal];
       [addfriend setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(14.0f/255.0f) green:(112.0f/255.0f) blue:(142.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
        
        [addfriend addTarget:self action:@selector(addFriendOption:) forControlEvents:UIControlEventTouchUpInside];
        addfriend.tag= [[import_dict objectForKey:@"id"]intValue];
        [mainview addSubview:addfriend];
        addfriend.hidden=YES;
        
        UIButton *acceptfriend = [UIButton buttonWithType:UIButtonTypeCustom];
        acceptfriend.frame = CGRectMake(15, 200, 138, 25);
        acceptfriend.backgroundColor = [UIColor clearColor];
        [acceptfriend setTitle:@"Accept Friend" forState:UIControlStateNormal];
        [acceptfriend setTitleColor:[UIColor colorWithRed:92.0f/255.0f green:132.0f/255.0f blue:92.0f/255.0f alpha:1] forState:UIControlStateNormal];
        [acceptfriend addTarget:self action:@selector(accept:) forControlEvents:UIControlEventTouchUpInside];
        acceptfriend.tag= [[import_dict objectForKey:@"id"]intValue];
        acceptfriend.layer.borderWidth= 1;
        acceptfriend.layer.cornerRadius=4;
        [mainview addSubview:acceptfriend];
        acceptfriend.hidden=YES;
        
        UIButton *addbusiness = [UIButton buttonWithType:UIButtonTypeCustom];
        addbusiness.frame = CGRectMake(self.view.frame.size.width/2 - [UIImage imageNamed:@"addPersonalOff"].size.width/2, addfriend.frame.origin.y+addfriend.frame.size.height+20, [UIImage imageNamed:@"addBusinessOff"].size.width, [UIImage imageNamed:@"addBusinessOff"].size.height);
        addbusiness.layer.cornerRadius = 5;
        addbusiness.clipsToBounds = YES;
        [addbusiness setBackgroundColor:[UIColor colorWithRed:(40.0f/255.0f) green:(170.0f/255.0f) blue:(185.0f/255.0f) alpha:1.0f]];
        [addbusiness setTitle:@"Request business connection" forState:UIControlStateNormal];
        [addbusiness.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
        [addbusiness setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        //[addfriend setBackgroundImage:[UIImage imageNamed:@"addPersonalOff"] forState:UIControlStateNormal];
        [addbusiness setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(14.0f/255.0f) green:(112.0f/255.0f) blue:(142.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
        [addbusiness addTarget:self action:@selector(addBusinessOption:) forControlEvents:UIControlEventTouchUpInside];
        addbusiness.tag= [[import_dict objectForKey:@"id"]intValue];
        [mainview addSubview:addbusiness];
        addbusiness.hidden=YES;
        
        //=======================info label================//
        
        UIImageView *infoView = [[UIImageView alloc]init];
        infoView.backgroundColor = [UIColor clearColor];
        infoView.frame = CGRectMake(self.view.frame.size.width/2 - [UIImage imageNamed:@"infoImage"].size.width/2, addbusiness.frame.origin.y+addbusiness.frame.size.height+50, [UIImage imageNamed:@"infoImage"].size.width, [UIImage imageNamed:@"infoImage"].size.height);
        [infoView setImage:[UIImage imageNamed:@"infoImage"]];
        [mainview addSubview:infoView];
        
        UITextView *infoLabel = [[UITextView alloc]init];
        infoLabel.backgroundColor = [UIColor clearColor];
        infoLabel.frame = CGRectMake(10, infoView.frame.origin.y+infoView.frame.size.height+6, [UIScreen mainScreen].bounds.size.width-20, 100);
        infoLabel.text = @"Request business connections with your colleagues and business partners to share each others business contact information!";
        infoLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:14.f];
        infoLabel.textColor = [UIColor grayColor];
        infoLabel.textAlignment = NSTextAlignmentCenter;
        //    [infoLabel setCenter:CGPointMake(self.view.center.x,infoLabel.center.y)];
        [mainview addSubview:infoLabel];
        
        
        UIButton *acceptbusiness = [UIButton buttonWithType:UIButtonTypeCustom];
        acceptbusiness.frame = CGRectMake(15, 130, 138, 25);
        [acceptbusiness setTitle:@"Accept Business" forState:UIControlStateNormal];
        [acceptbusiness setTitleColor:[UIColor colorWithRed:17.0f/255.0f green:61.0f/255.0f blue:125.0f/255.0f alpha:1] forState:UIControlStateNormal];
        [acceptbusiness addTarget:self action:@selector(accept:) forControlEvents:UIControlEventTouchUpInside];
        acceptbusiness.tag= [[import_dict objectForKey:@"id"]intValue];
        acceptbusiness.layer.borderWidth= 1;
        acceptbusiness.layer.cornerRadius=4;
        [mainview addSubview:acceptbusiness];
        acceptbusiness.hidden=YES;
        
        UIButton *cancelrequest = [UIButton buttonWithType:UIButtonTypeCustom];
        cancelrequest.frame = CGRectMake(91, 170, [UIScreen mainScreen].bounds.size.width-182, 25);
        
        cancelrequest.layer.cornerRadius = 5;
        cancelrequest.clipsToBounds = YES;
        [cancelrequest setBackgroundColor:[UIColor colorWithRed:(204.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f]];
        [cancelrequest setTitle:@"Cancel Request" forState:UIControlStateNormal];
        [cancelrequest.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
        [cancelrequest setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        //[addfriend setBackgroundImage:[UIImage imageNamed:@"addPersonalOff"] forState:UIControlStateNormal];
        [cancelrequest setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(115.0f/255.0f) green:(115.0f/255.0f) blue:(115.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
        
        //[cancelrequest setBackgroundImage:[UIImage imageNamed:@"cancel-request.png"] forState:UIControlStateNormal];
        [cancelrequest addTarget:self action:@selector(cancelRequest:) forControlEvents:UIControlEventTouchUpInside];
        cancelrequest.tag= [[import_dict objectForKey:@"id"]intValue];
        [mainview addSubview:cancelrequest];
        cancelrequest.hidden=YES;
        
        UIButton *friendrequestsent = [UIButton buttonWithType:UIButtonTypeCustom];
        friendrequestsent.frame = CGRectMake(15, 210, [UIScreen mainScreen].bounds.size.width-30, 50);
        friendrequestsent.layer.cornerRadius = 5;
        friendrequestsent.clipsToBounds = YES;
        [friendrequestsent setBackgroundColor:[UIColor colorWithRed:(40.0f/255.0f) green:(170.0f/255.0f) blue:(185.0f/255.0f) alpha:1.0f]];
        [friendrequestsent setTitle:@"Friend Request Sent" forState:UIControlStateNormal];
        [friendrequestsent.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
        [friendrequestsent setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        //[addfriend setBackgroundImage:[UIImage imageNamed:@"addPersonalOff"] forState:UIControlStateNormal];
        [friendrequestsent setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(14.0f/255.0f) green:(112.0f/255.0f) blue:(142.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
        //[friendrequestsent setBackgroundImage:[UIImage imageNamed:@"friend-request-send.png"] forState:UIControlStateNormal];
        //    [friendrequestsent addTarget:self action:@selector(cancelRequest:) forControlEvents:UIControlEventTouchUpInside];
        friendrequestsent.tag= [[import_dict objectForKey:@"id"]intValue];
        [mainview addSubview:friendrequestsent];
        friendrequestsent.hidden=YES;
        
        UIButton *businessrequestsent = [UIButton buttonWithType:UIButtonTypeCustom];
        businessrequestsent.frame = CGRectMake(15, 210, [UIScreen mainScreen].bounds.size.width-30, 50);
        businessrequestsent.layer.cornerRadius = 5;
        businessrequestsent.clipsToBounds = YES;
        [businessrequestsent setBackgroundColor:[UIColor colorWithRed:(40.0f/255.0f) green:(170.0f/255.0f) blue:(185.0f/255.0f) alpha:1.0f]];
        [businessrequestsent setTitle:@"Business Request Sent" forState:UIControlStateNormal];
        [businessrequestsent.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
        [businessrequestsent setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        //[addfriend setBackgroundImage:[UIImage imageNamed:@"addPersonalOff"] forState:UIControlStateNormal];
        [businessrequestsent setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(14.0f/255.0f) green:(112.0f/255.0f) blue:(142.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
        
        //[businessrequestsent setBackgroundImage:[UIImage imageNamed:@"business-request-send.png"] forState:UIControlStateNormal];
        //    [businessrequestsent addTarget:self action:@selector(cancelRequest:) forControlEvents:UIControlEventTouchUpInside];
        businessrequestsent.tag= [[import_dict objectForKey:@"id"]intValue];
        [mainview addSubview:businessrequestsent];
        businessrequestsent.hidden=YES;
        
        UIButton *removeconnection = [UIButton buttonWithType:UIButtonTypeCustom];
        removeconnection.frame = CGRectMake(15, 275, [UIScreen mainScreen].bounds.size.width-30, 50);
        
        removeconnection.layer.cornerRadius = 5;
        removeconnection.clipsToBounds = YES;
        [removeconnection setBackgroundColor:[UIColor colorWithRed:(204.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f]];
        [removeconnection setTitle:@"Remove Connection" forState:UIControlStateNormal];
        [removeconnection.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:17]];
        [removeconnection setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        //[addfriend setBackgroundImage:[UIImage imageNamed:@"addPersonalOff"] forState:UIControlStateNormal];
        [removeconnection setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(115.0f/255.0f) green:(115.0f/255.0f) blue:(115.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
        //[removeconnection setBackgroundImage:[UIImage imageNamed:@"remove-Connection.png"] forState:UIControlStateNormal];
        [removeconnection addTarget:self action:@selector(removeConnection:) forControlEvents:UIControlEventTouchUpInside];
        removeconnection.tag= [[import_dict objectForKey:@"id"]intValue];
        [mainview addSubview:removeconnection];
        removeconnection.hidden=YES;
        
        
        
        
        if ([[import_dict objectForKey:@"business"]intValue] == 0 && [[import_dict objectForKey:@"friend"]intValue] == 0)
        {
            currentstatus.hidden=YES;
            
            if ([[import_dict objectForKey:@"requestReceived"]intValue] == 1)
            {
                if ([[import_dict objectForKey:@"requestBusiness"]intValue] == 1)
                {
                    [addbusiness setHidden:YES];
                    [acceptbusiness setHidden:NO];
                    [addfriend setHidden:YES];
                    [acceptfriend setHidden:YES];
                    [cancelrequest setHidden:YES];
                    [friendrequestsent setHidden:YES];
                    [businessrequestsent setHidden:YES];
                    currentstatus.hidden=YES;
                    removeconnection.hidden=YES;
                }
                else
                {
                    [addbusiness setHidden:YES];
                    [acceptbusiness setHidden:YES];
                    [addfriend setHidden:YES];
                    [acceptfriend setHidden:NO];
                    [cancelrequest setHidden:YES];
                    [friendrequestsent setHidden:YES];
                    [businessrequestsent setHidden:YES];
                    currentstatus.hidden=YES;
                    removeconnection.hidden=YES;
                }
                
                denybt= [UIButton buttonWithType:UIButtonTypeCustom];
                denybt.frame = CGRectMake(acceptfriend.frame.origin.x+acceptfriend.frame.size.width+10, 130, 130, 25);
                [mainview addSubview:denybt];
                denybt.layer.cornerRadius=5;
                denybt.backgroundColor=[UIColor clearColor];
                denybt.titleLabel.text=@"Deny";
                [denybt setTitle:@"Deny" forState:UIControlStateNormal];
                [denybt setTitleColor:[UIColor colorWithRed:126.0f/255.0f green:85.0f/255.0f blue:85.0f/255.0f alpha:1] forState: UIControlStateNormal];
                denybt.layer.borderColor=[[UIColor grayColor]CGColor];
                denybt.layer.borderWidth=1;
                [denybt addTarget:self action:@selector(deny:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            
            if ([[import_dict objectForKey:@"requestReceived"]intValue] == 0)
            {
                if ([[import_dict objectForKey:@"requestSent"]intValue] == 1)
                {
                    [cancelrequest setHidden:NO];
                    [addfriend setHidden:YES];
                    [addbusiness setHidden:YES];
                    [acceptfriend setHidden:YES];
                    [acceptbusiness setHidden:YES];
                    
                    if ([[import_dict objectForKey:@"requestBusiness"]intValue] == 1)
                    {
                        [friendrequestsent setHidden:YES];
                        [businessrequestsent setHidden:YES]; //[businessrequestsent setHidden:NO];
                    }
                    else
                    {
                        [friendrequestsent setHidden:YES]; //[friendrequestsent setHidden:NO];
                        [businessrequestsent setHidden:YES];
                    }
                }
                
                else
                {
                    //add button
                    [currentstatus setFrame:CGRectMake(0, 0, 0, 0)];
                    
                    [cancelrequest removeFromSuperview];
                    [addbusiness setHidden:NO];
                    [addfriend setHidden:NO];
                    [acceptfriend setHidden:YES];
                    [acceptbusiness setHidden:YES];
                    [friendrequestsent removeFromSuperview];
                    [businessrequestsent removeFromSuperview];
                }
            }
        }
        
        else if ([[import_dict objectForKey:@"business"]intValue] == 1)
        {
            [currentstatus setTitle:@"Business" forState:UIControlStateNormal];
            currentstatus_str = @"Business";
            currentstatus.frame=addbusiness.frame;
            [currentstatus setTitleColor:[UIColor colorWithRed:17.0f/255.0f green:61.0f/255.0f blue:125.0f/255.0f alpha:1] forState:UIControlStateNormal];
            //      [currentstatus addTarget:self action:@selector(addOptions_Business:) forControlEvents:UIControlEventTouchUpInside];
            currentstatus.hidden=NO;
            addbusiness.hidden=YES;
            removeconnection.hidden=NO;
            
            if ([[import_dict objectForKey:@"requestReceived"]intValue] == 1)
            {
                if ([[import_dict objectForKey:@"requestBusiness"]intValue] == 0)
                {
                    acceptfriend.hidden=NO;
                    acceptfriend.frame= addfriend.frame;
                    acceptbusiness.hidden=YES;
                    addfriend.hidden=YES;
                    addbusiness.hidden=YES;
                    cancelrequest.hidden=YES;
                    friendrequestsent.hidden=YES;
                    businessrequestsent.hidden=YES;
                    
                    denybt= [UIButton buttonWithType:UIButtonTypeCustom];
                    denybt.frame = CGRectMake(acceptfriend.frame.origin.x, 165, 130, 25);
                    [mainview addSubview:denybt];
                    denybt.layer.cornerRadius=5;
                    denybt.backgroundColor=[UIColor clearColor];
                    denybt.titleLabel.text=@"Deny";
                    [denybt setTitle:@"Deny" forState:UIControlStateNormal];
                    [denybt setTitleColor:[UIColor colorWithRed:126.0f/255.0f green:85.0f/255.0f blue:85.0f/255.0f alpha:1] forState: UIControlStateNormal];
                    denybt.layer.borderColor=[[UIColor grayColor]CGColor];
                    denybt.layer.borderWidth=1;
                    [denybt addTarget:self action:@selector(deny:) forControlEvents:UIControlEventTouchUpInside];
                }
            }
            
            else if ([[import_dict objectForKey:@"requestSent"]intValue] == 1)
            {
                cancelrequest.hidden=NO;
                if ([[import_dict objectForKey:@"requestBusiness"]intValue] == 1)
                {
                    friendrequestsent.hidden=YES;
                    businessrequestsent.hidden=YES; //businessrequestsent.hidden=NO;
                }
                else
                {
                    friendrequestsent.hidden=YES; //friendrequestsent.hidden=NO;
                    businessrequestsent.hidden=YES;
                }
            }
            
            else //if ([[import_dict objectForKey:@"requestSent"]intValue] == 0)
            {
                addfriend.hidden=NO;
                cancelrequest.hidden=YES;
                friendrequestsent.hidden=YES;
                businessrequestsent.hidden=YES;
                removeconnection.hidden=NO;
                removeconnection.frame = CGRectMake(15, infoLabel.frame.origin.y+infoLabel.frame.size.height+2, [UIScreen mainScreen].bounds.size.width-30, 50);//friendrequestsent.frame;
            }
        }
        
        else if ([[import_dict objectForKey:@"friend"]intValue] == 1)
        {
            [currentstatus setTitle:@"Friends" forState:UIControlStateNormal];
            currentstatus_str = @"Friends";
            [currentstatus setTitleColor:[UIColor colorWithRed:92.0f/255.0f green:132.0f/255.0f blue:92.0f/255.0f alpha:1] forState:UIControlStateNormal];
            currentstatus.frame = addfriend.frame;
            currentstatus.hidden=NO;
            
            removeconnection.hidden=NO;
            
            if ([[import_dict objectForKey:@"requestReceived"]intValue] == 1)
            {
                if ([[import_dict objectForKey:@"requestBusiness"]intValue] == 1)
                {
                    acceptfriend.hidden=YES;
                    acceptbusiness.hidden=NO;
                    acceptbusiness.frame= addbusiness.frame;
                    addfriend.hidden=YES;
                    addbusiness.hidden=YES;
                    cancelrequest.hidden=YES;
                    friendrequestsent.hidden=YES;
                    businessrequestsent.hidden=YES;
                    
                    
                    denybt= [UIButton buttonWithType:UIButtonTypeCustom];
                    denybt.frame = CGRectMake(acceptbusiness.frame.origin.x, 165, 130, 25);
                    [mainview addSubview:denybt];
                    denybt.layer.cornerRadius=5;
                    denybt.backgroundColor=[UIColor clearColor];
                    denybt.titleLabel.text=@"Deny";
                    [denybt setTitle:@"Deny" forState:UIControlStateNormal];
                    [denybt setTitleColor:[UIColor colorWithRed:126.0f/255.0f green:85.0f/255.0f blue:85.0f/255.0f alpha:1] forState: UIControlStateNormal];
                    denybt.layer.borderColor=[[UIColor grayColor]CGColor];
                    denybt.layer.borderWidth=1;
                    [denybt addTarget:self action:@selector(deny:) forControlEvents:UIControlEventTouchUpInside];
                }
            }
            
            else if ([[import_dict objectForKey:@"requestSent"]intValue] == 1)
            {
                cancelrequest.hidden=NO;
                if ([[import_dict objectForKey:@"requestBusiness"]intValue] == 1)
                {
                    friendrequestsent.hidden=YES;
                    businessrequestsent.hidden=YES; //businessrequestsent.hidden=NO;
                }
                else
                {
                    friendrequestsent.hidden=YES; //friendrequestsent.hidden=NO;
                    businessrequestsent.hidden=YES;
                }
            }
            else //if ([[import_dict objectForKey:@"requestSent"]intValue] == 0)
            {
                addbusiness.hidden=NO;
                cancelrequest.hidden=YES;
                friendrequestsent.hidden=YES;
                businessrequestsent.hidden=YES;
                removeconnection.frame = CGRectMake(15, infoLabel.frame.origin.y+infoLabel.frame.size.height+2, [UIScreen mainScreen].bounds.size.width-30, 50);//friendrequestsent.frame;
            }
        }
        
        [loader removeFromSuperview];
    }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

+(void)chngpostion
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Data" object:Nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getData:) name:@"Data" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(navtopage:) name:@"pagenav" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestreceivedaction) name:@"Requestreceived_push" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(acceptedrequestaction) name:@"Accepted_push" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AcceptedRequest) name:@"Accepted_request" object:nil];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AddFriend) name:@"NewConnection" object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AcceptedRequest) name:@"TypeChange" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shareLocation) name:@"ShareLocation" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdateInformation) name:@"UpdateInfo" object:nil];
    
}

-(void)UpdateInformation
{
    ConPersonalProfileViewController *PVC = [[ConPersonalProfileViewController alloc]init];
    PVC.toUpdateInfo = @"YES";
    [self.navigationController pushViewController:PVC animated:YES];
   // [[NSNotificationCenter defaultCenter] postNotificationName:@"Update Info" object:Nil];
    
}


-(void)shareLocation
{
    ConLocateGroupViewController *conLocate = [[ConLocateGroupViewController alloc]init];
    conLocate.group_id=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"sharedLocGrpId"]];
    [self.navigationController pushViewController:conLocate animated:YES];
}

-(void)AddFriend
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDel.newUser)
    {

    ConAddFriendViewController *con = [[ConAddFriendViewController alloc]init];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
    
        appDel.newUser = NO;
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    [[self navigationController] pushViewController:con animated:YES];
        
    }
    
}



-(void)AcceptedRequest
{
    
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (del.profileRedirect)
    {
    ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
    // con.other=@"yes";
    con.request = YES;
    [self.tabBarController.tabBar setHidden:YES];
    // con.uid=[NSString stringWithFormat:@"%d",[userid intValue]];
    //                [self.navigationController presentViewController:con animated:NO completion:nil];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
        
        del.profileRedirect = NO;
    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
        
    }
    
}

-(void)requestreceivedaction
{
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
    ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
    con.other=@"yes";
    con.uid=[NSString stringWithFormat:@"%@",del.pushtoprofileid];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}

-(void)acceptedrequestaction
{
    ConRequestViewController *con = [[ConRequestViewController alloc]init];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}


-(void)navtopage: (NSNotification *)notification
{
    NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
    //  NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
    if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"4"])
    {
        ConAccountSettingsViewController *con = [[ConAccountSettingsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"6"])
    {
        ConInviteViewController *con = [[ConInviteViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"5"])
    {
        ConSyncLoaderViewController *con = [[ConSyncLoaderViewController alloc]init];
        //        [self.navigationController pushViewController:con animated:NO];
        
        [self presentViewController:con animated:NO completion:nil];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"2"])
    {
        ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"3"])
    {
        ConQROptionsViewController *con = [[ConQROptionsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    
    
    [mainview setFrame:CGRectMake(0, mainview.frame.origin.y, mainview.frame.size.width, mainview.frame.size.height)];
    //    [prefs setObject:@"111" forKey:@"whichpage"];
}


-(void)getData:(NSNotification *)notification {
    
    if(move == 0) {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-40, mainview.frame.origin.y, mainview.frame.size.width, mainview.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             move=1;
                         }];
    } else {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake(0, (mainview.frame.origin.y),mainview.frame.size.width, mainview.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             move=0;
                         }];
    }
}

-(void)viewDidDisappear:(BOOL)animated
{
    move=0;
    [mainview removeFromSuperview];
    [super viewDidDisappear:YES];
}

-(void)accept: (id)sender
{
    UIActivityIndicatorView *act = [[UIActivityIndicatorView alloc] init];
    act.center=self.view.center;
    [act startAnimating];
    act.hidden=NO;
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [act setColor:[UIColor blackColor]];
    [mainview addSubview:act];
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=acceptrequest&id=%d&access_token=%@&device_id=%@",userid,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
    
    DebugLog(@"accept url: %@",urlString1);
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    DebugLog(@"ACCEPT FRIEND URL:%@",newString1);
    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    
    if(signeddataURL1!=nil)
    {
        
        NSError *error=nil;
        NSDictionary *json_accept = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                    options:kNilOptions
                                                                      error:&error];
        DebugLog(@"accept json returns: %@",json_accept);
        if ([[json_accept objectForKey:@"success"]intValue] == 1)
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Request Successfully Accepted!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
            [self reloadTheDictionary];
        }
        
        ////////////////////////////////    Copy Contact    //////////////////////////////////////
        
        
        NSMutableDictionary *finalwritedict = [[import_dict valueForKey:@"profile"] mutableCopy];
        //DebugLog(@"profile dict: %@",finalwritedict);
        NSString *petFirstName = [finalwritedict objectForKey:@"name"];
        NSString *petLastName = [finalwritedict objectForKey:@"surname"];
        NSString *petfullname = [NSString stringWithFormat:@"%@ %@",petFirstName,petLastName];//[finalwritedict objectForKey:@"fullname"];
        NSString *workphone = @"";
        NSString *mobilephone = @"";
        NSString *homephone = @"";
        NSString *cityp= @"";
        NSString *countryp = @"";
        
        NSString *email_address = @"";
        NSString *zipCode = @"";
        
        //        NSString *apphme, *appmobl, *appwrk, *devhme, *devmob, *devwrk;
        
        if ([[finalwritedict objectForKey:@"b_phone_num"] length] > 2)
            workphone = [NSString stringWithFormat:@"%@%@",[finalwritedict objectForKey:@"b_phone_pre"],[finalwritedict objectForKey:@"b_phone_num"]];
        if ([[finalwritedict objectForKey:@"mobile_num"] length] > 2)
            mobilephone = [NSString stringWithFormat:@"%@%@",[finalwritedict objectForKey:@"mobile_pre"],[finalwritedict objectForKey:@"mobile_num"]];
        if ([[finalwritedict objectForKey:@"phone_num"] length] > 2)
            homephone = [NSString stringWithFormat:@"%@%@",[finalwritedict objectForKey:@"phone_pre"],[finalwritedict objectForKey:@"phone_num"]];
        if ([[finalwritedict objectForKey:@"city"] length] > 2)
            cityp = [NSString stringWithFormat:@"%@",[finalwritedict objectForKey:@"city"]];
        if ([[finalwritedict objectForKey:@"country"] length] > 2)
            countryp = [NSString stringWithFormat:@"%@",[finalwritedict objectForKey:@"country"]];
        
        if ([[finalwritedict objectForKey:@"email"] length]>2) {
            email_address = [NSString stringWithFormat:@"%@",[finalwritedict objectForKey:@"email"]];
        }/////////// New Added /////////
        
        if ([[finalwritedict objectForKey:@"zipcode"] length]>2) {
            email_address = [NSString stringWithFormat:@"%@",[finalwritedict objectForKey:@"email"]];
        }/////////// New Added /////////
        
        ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, nil);
        ABRecordRef pet = ABPersonCreate();
        //                NSString *firstname=
        ABRecordSetValue(pet, kABPersonFirstNameProperty, (__bridge CFStringRef)petFirstName, nil);
        ABRecordSetValue(pet, kABPersonLastNameProperty, (__bridge CFStringRef)petLastName, nil);
        
        ABRecordSetValue(pet, kABPersonEmailProperty, (__bridge CFStringRef)email_address, nil);
        
        ABMutableMultiValueRef phoneNumbers = ABMultiValueCreateMutable(kABMultiStringPropertyType);
        ABMultiValueAddValueAndLabel(phoneNumbers, (__bridge CFStringRef)workphone, kABWorkLabel, NULL);
        ABMultiValueAddValueAndLabel(phoneNumbers, (__bridge CFStringRef)homephone, kABHomeLabel, NULL);
        ABMultiValueAddValueAndLabel(phoneNumbers, (__bridge CFStringRef)mobilephone, kABPersonPhoneMobileLabel, NULL);
        ABRecordSetValue(pet, kABPersonPhoneProperty, phoneNumbers, nil);
        
        ABMutableMultiValueRef address = ABMultiValueCreateMutable(kABStringPropertyType);
        ABMultiValueAddValueAndLabel(address, (__bridge CFStringRef)cityp, kABPersonAddressCityKey, nil);
        ABMultiValueAddValueAndLabel(address, (__bridge CFStringRef)countryp, kABPersonAddressCountryKey, nil);
        ABMultiValueAddValueAndLabel(address, (__bridge CFStringRef)zipCode, kABPersonAddressZIPKey, nil);
        
        
        int abcd;
        abcd=0;
        
        NSArray *allContacts = (__bridge NSArray *)ABAddressBookCopyArrayOfAllPeople(addressBookRef);
        for (id record in allContacts){
            ABRecordRef thisContact = (__bridge ABRecordRef)record;
            if (ABRecordCopyCompositeName(thisContact) != NULL)
            {
                if (CFStringCompare(ABRecordCopyCompositeName(thisContact),(__bridge CFStringRef)(petfullname), 0) == kCFCompareEqualTo){
                    //The contact already exists!
                    //     return;
                    abcd=1;
                }
                else
                {
                }
            }
        }
        if (abcd == 0)
        {
            ABAddressBookAddRecord(addressBookRef, pet, nil);
            ABAddressBookSave(addressBookRef, nil);
        }
        
    }
    else{
        alert = [[UIAlertView alloc] initWithTitle:@"Server not responding!"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        
    }
    
}

-(void)deny: (id)sender
{
    NSLog(@"3333");
    UIActivityIndicatorView *act = [[UIActivityIndicatorView alloc] init];
    act.center=self.view.center;
    [act startAnimating];
    act.hidden=NO;
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [act setColor:[UIColor blackColor]];
    [mainview addSubview:act];
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=denyrequest&id=%d&access_token=%@&device_id=%@",userid,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
    
    DebugLog(@"deny url: %@",urlString1);
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    
    if (signeddataURL1!=nil) {
        
        NSError *error=nil;
        NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                  options:kNilOptions
                                                                    error:&error];
        DebugLog(@"deny json returns: %@",json_deny);
        
        if ([[json_deny objectForKey:@"success"]intValue] == 1)
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Request Successfully Denied!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
            [self reloadTheDictionary];
        }
    }
    else
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Server not responding!"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        
    }
}


-(void)cancelRequest: (id)sender
{
    NSLog(@"4444");
    UIActivityIndicatorView *act = [[UIActivityIndicatorView alloc] init];
    act.center=self.view.center;
    [act startAnimating];
    act.hidden=NO;
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [act setColor:[UIColor blackColor]];
    [mainview addSubview:act];
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=withdrawrequest&id=%d&access_token=%@&device_id=%@",userid,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
    
    DebugLog(@"deny url: %@",urlString1);
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    
    if (signeddataURL1!=nil) {
        
        
        NSError *error=nil;
        NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                  options:kNilOptions
                                                                    error:&error];
        DebugLog(@"deny json returns: %@",json_deny);
        
        if ([[json_deny objectForKey:@"success"]intValue] == 1)
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Request Successfully Cancelled!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            alert.delegate=self;
            [alert show];
            [self reloadTheDictionary];
        }
    }
    else
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Server not responding!"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        
    }
}

-(void) reloadTheDictionary
{
    ConNewRequestsViewController *mng = [[ConNewRequestsViewController alloc]init];
    //    mng.imported_arr = [filtered_arr objectAtIndex:indexPath.row];
    mng.userid= userid;
    DebugLog(@"selected user id : %d",mng.userid);
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:mng animated:YES];
}


-(void)removeConnection:(id)sender
{
    NSLog(@"5555");
    UIActivityIndicatorView *act = [[UIActivityIndicatorView alloc] init];
    act.center=self.view.center;
    [act startAnimating];
    act.hidden=NO;
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [act setColor:[UIColor blackColor]];
    [mainview addSubview:act];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=removeconnection&id=%d&access_token=%@&device_id=%@&business=true",userid,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
    
    DebugLog(@"deny url: %@",urlString1);
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    
    if (signeddataURL1!=nil) {
        
        NSError *error=nil;
        NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                  options:kNilOptions
                                                                    error:&error];
        DebugLog(@"deny json returns: %@",json_deny);
        if ([[json_deny objectForKey:@"success"]intValue] == 1)
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Connection Successfully Removed!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
            [self reloadTheDictionary];
        }
        else
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                               message:[NSString stringWithFormat:@"%@",[json_deny objectForKey:@"error"]]
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
            [self reloadTheDictionary];
        }
    }
    
    else
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Server not responding!"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        
    }
    
    
}


-(void)addFriendOption:(id)sender
{
    NSLog(@"111111");
    UIActivityIndicatorView *act = [[UIActivityIndicatorView alloc] init];
    act.center=self.view.center;
    [act startAnimating];
    act.hidden=NO;
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [act setColor:[UIColor blackColor]];
    [mainview addSubview:act];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=addrequest&id=%d&access_token=%@&device_id=%@",userid,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
    
    DebugLog(@"deny url: %@",urlString1);
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    
    if (signeddataURL1!=nil) {
        
        NSError *error=nil;
        NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                  options:kNilOptions
                                                                    error:&error];
        DebugLog(@"deny json returns: %@",json_deny);
        
        if ([[json_deny objectForKey:@"success"]intValue] == 1)
        {
            
            alert = [[UIAlertView alloc] initWithTitle:@"Request Successfully Sent!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
            [self reloadTheDictionary];
        }
        
    }
    
    else
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Server not responding!"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        
    }
}

-(void)addBusinessOption:(id)sender
{
    NSLog(@"2222222");
    UIActivityIndicatorView *act = [[UIActivityIndicatorView alloc] init];
    [mainview addSubview:act];
    act.center=self.view.center;
    [act startAnimating];
    act.hidden=NO;
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [act setColor:[UIColor blackColor]];
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=addrequest&id=%d&access_token=%@&device_id=%@&business=true",userid,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
    
    DebugLog(@"deny url: %@",urlString1);
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    
    if (signeddataURL1!=nil) {
        
        
        NSError *error=nil;
        NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                  options:kNilOptions
                                                                    error:&error];
        DebugLog(@"deny json returns: %@",json_deny);
        
        if ([[json_deny objectForKey:@"success"]intValue] == 1)
        {
            
            alert = [[UIAlertView alloc] initWithTitle:@"Request Successfully Sent!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
            [self reloadTheDictionary];
        }
        
    }
    
    else
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Server not responding!"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
    }
}

-(void)goback
{
    int index;
    NSArray* navarr = [[NSArray alloc] initWithArray:self.navigationController.viewControllers];
    for(int i=0 ; i<[navarr count] ; i++)
    {
        if(![[navarr objectAtIndex:i] isKindOfClass:NSClassFromString(@"ConNewRequestsViewController")])
        {
            index = i;
        }
    }
    
    CATransition *transition = [CATransition animation];
    
    transition.duration = 0.4f;
    
    transition.type = kCATransitionFade;
    
    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    [self.navigationController popToViewController:[navarr objectAtIndex:index] animated:YES];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 0) {
        
        
        CATransition *transition = [CATransition animation];
        
        transition.duration = 0.4f;
        
        transition.type = kCATransitionFade;
        
        [[self navigationController].view.layer addAnimation:transition forKey:nil];
       // [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)changecoloragain
{
    //    change.backgroundColor=[UIColor clearColor];
    backLabel.alpha = 1.0f;
    bck_img.alpha = 1.0f;
}

-(void)changecolor
{
    //    change.backgroundColor=[UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0];
    backLabel.alpha = 0.5f;
    bck_img.alpha = 0.5f;
    
}

-(UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end