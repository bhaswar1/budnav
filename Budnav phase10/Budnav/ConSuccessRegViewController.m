//  ConSuccessRegViewController.m
//  Contacter
//  Created by Bhaswar's MacBook Air on 07/05/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
#import "ConSuccessRegViewController.h"
#import "ConNewContactsViewController.h"
#import "ConProfileOwnViewController.h"
#import "ConLoginViewController.h"
#import "ConAddFriendViewController.h"
#import "MDRadialProgressView.h"
#import "MDRadialProgressTheme.h"
#import "MDRadialProgressLabel.h"
#import <AddressBook/AddressBook.h>
#import "AppDelegate.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

@interface ConSuccessRegViewController ()
{
    NSMutableArray *con_array, *app_contacts, *check_app_cont, *part_array, *testarr, *uniquearraydouble, *part_array_double;
    NSArray *uniquearray, *con_double_array;
    NSMutableData *responseData;
    UIView *viewnew;
    MDRadialProgressView *radialView3;
    UILabel *syncstatus;
    NSTimer *timerr;
    int number_of_doubles, connectiondoublefiler;
    NSMutableDictionary *add_contacts_dict, *ns;
    NSDictionary *json1;
    float time_interval;
}
@end
@implementation ConSuccessRegViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
    
    UILabel *picoptionslb = [[UILabel alloc]initWithFrame:CGRectMake(0, 80, self.view.bounds.size.width, 55)];
    [self.view addSubview:picoptionslb];
    picoptionslb.backgroundColor=[UIColor clearColor];
    picoptionslb.text=@"Successfully made!";
    picoptionslb.textColor=[UIColor colorWithRed:40.0f/255.0f green:82.0f/255.0f blue:135.0f/255.0f alpha:1];
    picoptionslb.font=[UIFont fontWithName:@"ProximaNova-Regular" size:22];
    picoptionslb.textAlignment=NSTextAlignmentCenter;
    
    UIImageView *zipimage=[[UIImageView alloc]initWithFrame:CGRectMake(102.5f, 145, 115, 105.5f)];
    zipimage.image=[UIImage imageNamed:@"icon-success-reg.png"];
    [self.view addSubview:zipimage];
    zipimage.userInteractionEnabled=YES;
    
    UILabel *successmsg=[[UILabel alloc]initWithFrame:CGRectMake(30,zipimage.frame.origin.y+zipimage.frame.size.height+15, self.view.bounds.size.width-60, 55)];
    [self.view addSubview:successmsg];
    successmsg.backgroundColor=[UIColor clearColor];
    successmsg.text=@"Search contacts on your phone who already use Contacter";
    successmsg.numberOfLines=2;
    successmsg.textColor=[UIColor grayColor];
    successmsg.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
    successmsg.textAlignment=NSTextAlignmentCenter;
    
    time_interval = 1.5f;
}

- (MDRadialProgressView *)progressViewWithFrame:(CGRect)frame
{
	MDRadialProgressView *view = [[MDRadialProgressView alloc] initWithFrame:frame];
	// Only required in this demo to align vertically the progress views.
	view.center = CGPointMake(self.view.center.x, view.center.y+40);
	return view;
}

-(void)targetMethodFirstTime: (NSTimer *)timer
{
    radialView3.progressCounter=radialView3.progressCounter+1;
    
    if (radialView3.progressCounter == 15)
    {
        [self performSelectorInBackground:@selector(testfun) withObject:nil];
    }
    if (radialView3.progressCounter == 100)
    {
        [timer invalidate];
        [viewnew removeFromSuperview];
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
        
        ConAddFriendViewController *con = [[ConAddFriendViewController alloc]init];
        con.searchfriends= @"yes";
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
        [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"searchafterreg"];
        [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"searchfriends"];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    connectiondoublefiler =0;
    number_of_doubles = 0;
    con_double_array= [[NSArray alloc]init];
    
    UIButton *makepicbt = [UIButton buttonWithType:UIButtonTypeCustom];
    makepicbt.frame = CGRectMake(15, 340, 290, 45);
    makepicbt.layer.cornerRadius=5;
    [makepicbt setBackgroundImage:[UIImage imageNamed:@"search-friend.png"] forState:UIControlStateNormal];
    makepicbt.backgroundColor=[UIColor clearColor];
    [makepicbt addTarget:self action:@selector(search_friends:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:makepicbt];
    
    UIButton *helpbt = [UIButton buttonWithType:UIButtonTypeCustom];
    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[ver objectAtIndex:0] intValue] >= 7)
        helpbt.frame = CGRectMake(15, self.view.bounds.size.height-65, 290, 30);
    else
        helpbt.frame = CGRectMake(15, self.view.bounds.size.height-35, 290, 30);
    helpbt.layer.cornerRadius=5;
    helpbt.titleLabel.font=[UIFont fontWithName:@"ProximaNova-Regular" size:18];
    [helpbt setTitle:@"Skip" forState:UIControlStateNormal];
    [helpbt setTitleColor:[UIColor colorWithRed:17.0f/255.0f green:61.0f/255.0f blue:125.0f/255.0f alpha:1] forState:UIControlStateNormal];
    helpbt.backgroundColor=[UIColor clearColor];
    [helpbt addTarget:self action:@selector(skip_func:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:helpbt];
    
    
    //        ConAddBackUpViewController *con = [[ConAddBackUpViewController alloc]init];
    //        [self.navigationController pushViewController:con animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)search_friends:(id)sender
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    if ([[prefs objectForKey:@"addbackup"] isEqualToString:@"yes"])
    {
        viewnew = [[UIView alloc]initWithFrame:self.view.bounds];
        viewnew.backgroundColor=[UIColor blackColor];
        [self.view addSubview:viewnew];
        viewnew.layer.zPosition=3;
        
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
        
        UIView *whiteview = [[UIView alloc]initWithFrame:CGRectMake(55, 150, 210, 210)];
        [self.view addSubview:whiteview];
        whiteview.backgroundColor=[UIColor clearColor];
        //
        UIImageView *logo_img = [[UIImageView alloc]initWithFrame:CGRectMake(114, 31, 92.5f, 16)];
        logo_img.image=[UIImage imageNamed:@"logomod.png"];
        [viewnew addSubview:logo_img];
        
        
        radialView3 = [self progressViewWithFrame:CGRectMake(0, 140, 200, 200)];
        radialView3.progressTotal = 100;
        radialView3.progressCounter = 0;
        radialView3.theme.completedColor = [UIColor whiteColor];//colorWithRed:132/255.0 green:23/255.0 blue:23/255.0 alpha:1.0
        radialView3.theme.incompletedColor = [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]; //lightGrayColor
        radialView3.theme.thickness = 9.5f;
        radialView3.theme.sliceDividerHidden = YES;
        radialView3.theme.centerColor = [UIColor clearColor];
        //        radialView3.label.text
        radialView3.center= self.view.center;
        [viewnew addSubview:radialView3];
        
        syncstatus = [[UILabel alloc]initWithFrame:CGRectMake(0, self.view.bounds.size.height-90, 320, 40)];
        syncstatus.text=@"Welcome!";
        syncstatus.textColor=[UIColor whiteColor];
        syncstatus.textAlignment=NSTextAlignmentCenter;
        syncstatus.font= [UIFont fontWithName:@"ProximaNova-Regular" size:18];
        [viewnew addSubview:syncstatus];
        
        timerr = [NSTimer scheduledTimerWithTimeInterval:time_interval
                                                  target:self
                                                selector:@selector(targetMethodFirstTime:)
                                                userInfo:nil
                                                 repeats:YES];
        
        [[NSRunLoop mainRunLoop] addTimer:timerr forMode:NSRunLoopCommonModes];
//        dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
//        dispatch_async(q, ^{
        
     //        });

    }
}



-(void) testfun
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    CFErrorRef *errorab = nil;
    
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, errorab);
    
    __block BOOL accessGranted = NO;
    if (&ABAddressBookRequestAccessWithCompletion != NULL) { // we're on iOS 6
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(sema);
        });
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        //        dispatch_release(sema);
    }
    else { // we're on iOS 5 or older
        accessGranted = YES;
    }
    if (accessGranted) {
        
        NSMutableArray *contactpickarr = [[NSMutableArray alloc]init];
        
        ABAddressBookRef UsersAddressBook = ABAddressBookCreateWithOptions(NULL, NULL);
        
        //contains details for all the contacts
        CFArrayRef ContactInfoArray = ABAddressBookCopyArrayOfAllPeople(UsersAddressBook);
        
        //get the total number of count of the users contact
        CFIndex numberofPeople = CFArrayGetCount(ContactInfoArray);
        DebugLog(@"%ld",numberofPeople);
        con_array = [[NSMutableArray alloc]init];
        
        //iterate through each record and add the value in the array
        for (int i =0; i<numberofPeople; i++) {
            ABRecordRef ref = CFArrayGetValueAtIndex(ContactInfoArray, i);
            ABMultiValueRef names, lastnames;
            
            if ((ABRecordCopyValue(ref, kABPersonFirstNameProperty) == NULL && ABRecordCopyValue(ref, kABPersonLastNameProperty) == NULL) || ABRecordCopyValue(ref, kABPersonPhoneProperty) == NULL)
            {
            }
            else
            {
            if(ABRecordCopyValue(ref, kABPersonFirstNameProperty) != NULL)
                names = (__bridge ABMultiValueRef)((__bridge NSString*)ABRecordCopyValue(ref,kABPersonFirstNameProperty));
            else
                names = @"";
            
            if(ABRecordCopyValue(ref, kABPersonLastNameProperty) != NULL)
                lastnames = (__bridge ABMultiValueRef)((__bridge NSString*)ABRecordCopyValue(ref, kABPersonLastNameProperty));
            else
                lastnames = @"";
            
            //        DebugLog(@"name from address book = %@ %@",names,lastnames);  // works fine.
            NSString *fullname = [NSString stringWithFormat:@"%@ %@",names,lastnames];
            
            //        NSString *contactName = (__bridge NSString *)(names);
            
            [contactpickarr addObject:fullname];
            
            //            NSData  *imgData = (__bridge NSData *)ABPersonCopyImageData(ref);
            //            UIImage  *img = [UIImage imageWithData:imgData];
            //            DebugLog(@"Image : %@",img);
            
            ABMultiValueRef emailProperty = ABRecordCopyValue(ref, kABPersonEmailProperty);
            
            NSArray *emailArray = (__bridge NSArray *)ABMultiValueCopyArrayOfAllValues(emailProperty);
            NSString *newstringemail= [emailArray componentsJoinedByString:@","];
            
            ns = [[NSMutableDictionary alloc]init];
            
            [ns setObject:fullname forKey:@"name"];
            //            [ns setObject:fullname forKey:@"fullname"];
            [ns setValue:newstringemail forKey:@"email"];
            if ([newstringemail isKindOfClass:[NSNull class]] || newstringemail == (id)[NSNull null] || [newstringemail length] < 4)
                [ns setValue:@"" forKey:@"email"];
                
            ABRecordID recordID = ABRecordGetRecordID(ref);
            int useridphn = (int)recordID;
            NSString *idphn = [NSString stringWithFormat:@"p%d",useridphn];
            [ns setValue:idphn forKey:@"id"];

            
            ABMultiValueRef phoneNumberProperty = ABRecordCopyValue(ref, kABPersonPhoneProperty);
            NSArray *_phoneNumbers = (__bridge NSArray*)ABMultiValueCopyArrayOfAllValues(phoneNumberProperty);
            
            NSString *newstringph= [_phoneNumbers componentsJoinedByString:@","];
            
            [ns setValue:newstringph forKey:@"phone"];
            
            //                ABMultiValueRef phones = ABRecordCopyValue(ref, kABPersonPhoneProperty);
            //                for(CFIndex j = 0; j < ABMultiValueGetCount(phones); j++)
            //                {
            //                    CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(phones, j);
            //                    CFStringRef locLabel = ABMultiValueCopyLabelAtIndex(phones, j);
            //                    NSString *phoneLabel =(__bridge NSString*) ABAddressBookCopyLocalizedLabel(locLabel);
            //                    //CFRelease(phones);
            //                    NSString *phoneNumber = (__bridge NSString *)phoneNumberRef;
            //                    //CFRelease(phoneNumberRef);
            //                    //CFRelease(locLabel);
            //                    [ns setValue:phoneNumber forKey:phoneLabel];
            //                }
            
            //            NSMutableArray *addressarr = [[NSMutableArray alloc]init];
            
            //            ABMultiValueRef addrss = ABRecordCopyValue(ref, kABPersonAddressProperty);
            //            for(CFIndex k = 0; k < ABMultiValueGetCount(addrss); k++)
            //            {
            ABMultiValueRef stt = ABRecordCopyValue(ref, kABPersonAddressProperty);
            if (ABMultiValueGetCount(stt) > 0) {
                CFDictionaryRef dict = ABMultiValueCopyValueAtIndex(stt, 0);
                
                NSString *streetp = CFDictionaryGetValue(dict, kABPersonAddressStreetKey);
                NSString *cityp = CFDictionaryGetValue(dict, kABPersonAddressCityKey);
                NSString *zipcdep = CFDictionaryGetValue(dict, kABPersonAddressZIPKey);
                //                    NSString *statep = CFDictionaryGetValue(dict, kABPersonAddressStateKey);
                NSString *countryp = CFDictionaryGetValue(dict, kABPersonAddressCountryKey);
                //                    NSString *address = [NSString stringWithFormat:@"%@,%@,%@,%@,%@",streetp,cityp,zipcdep,statep,countryp];
                //                    [addressarr addObject:address];
                
                if ([streetp isKindOfClass:[NSNull class]] || streetp == (id)[NSNull null])
                    [ns setValue:@"" forKey:@"street"];
                else
                    [ns setValue:streetp forKey:@"street"];
                
                if ([cityp isKindOfClass:[NSNull class]] || cityp == (id)[NSNull null])
                    [ns setValue:@"" forKey:@"city"];
                else
                    [ns setValue:cityp forKey:@"city"];
                
                if ([zipcdep isKindOfClass:[NSNull class]] || zipcdep == (id)[NSNull null])
                    [ns setValue:@"" forKey:@"zip"];
                else
                    [ns setValue:zipcdep forKey:@"street"];
                
                if ([countryp isKindOfClass:[NSNull class]] || countryp == (id)[NSNull null])
                    [ns setValue:@"" forKey:@"country"];
                else
                    [ns setValue:countryp forKey:@"country"];
                
                [ns setValue:@"" forKey:@"number"];
            }
            else
            {
                [ns setValue:@"" forKey:@"number"];
                [ns setValue:@"" forKey:@"city"];
                [ns setValue:@"" forKey:@"street"];
                [ns setValue:@"" forKey:@"zip"];
                [ns setValue:@"" forKey:@"country"];
            }
            //            }
            //            [ns setObject:addressarr forKey:@"address"];
            [con_array addObject:ns];
            }
        }
    }
    con_double_array = con_array;
    //    DebugLog(@"array content = %@", contactpickarr);
    DebugLog(@"array content part 2 = %@", con_array);
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:con_array] forKey:@"devicefilter"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    for (NSMutableDictionary *remove_id_dict in con_array)
    {
        [remove_id_dict removeObjectForKey:@"id"];
    }

//    NSMutableArray *removeidarray = [con_array mutableCopy];
//    for (NSMutableDictionary *changekeydict in removeidarray)
//    {
//        for( NSString *aKey in [changekeydict allKeys])
//        {
//            if ([aKey isEqualToString:@"id"]) {
//                [changekeydict removeObjectForKey:aKey];
//            }
//        }
//    }
    
    
    uniquearray = [con_array mutableCopy];
    
    
    //        @try {
    //            NSError *err1 = nil;
    //            NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:(NSArray *)uniquearray options:NSJSONWritingPrettyPrinted error:&err1];
    //            NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
    //            NSString *encodedText = [self encodeToPercentEscapeString:jsonString];
    //
    //            DebugLog(@"jsonData as string:\n%@", jsonString);
    //
    //            NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=addbackup&access_token=%@&device_id=%@",[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];   //&contacts=%@  ,[jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
    //
    //            DebugLog(@"profile url: %@",urlString1);
    //            //            NSError *errorjs= nil;
    //
    //            //        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    //
    //            //            NSString *firid = [NSString stringWithFormat:@"800"];
    //
    //            responseData = [NSMutableData data];
    //
    //            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString1]];
    //
    //            NSString *params = [[NSString alloc] initWithFormat:@"contacts=%@",encodedText]; //[jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
    //            [request setHTTPMethod:@"POST"];
    //            [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    //            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    //            //            [[NSURLConnection alloc] initWithRequest:request delegate:self];
    //            NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    //
    //            if(theConnection)
    //            {
    //                responseData = [NSMutableData data];
    //
    //            }
    //            else
    //            {
    //                DebugLog(@"theConnection is null");
    //            }
    //
    //        }
    
    @try {
        NSError *err1 = nil;
        
        DebugLog(@"unique array count: %lu",(unsigned long)[uniquearray count]);
        //                        NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"File" ofType:@"json"]];
        //                        NSError *localError = nil;
        //                        NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
        //                       NSArray *countriesList = (NSArray *)parsedObject;
        int number_of_iterations;
        
        part_array = [[NSMutableArray alloc]init];
        if ([uniquearray count] <= 300)
        {
            //                            part_array = [uniquearray mutableCopy];
            number_of_iterations=1;
        }
        else
        {
            if ([uniquearray count] % 300 == 0)
            {
                number_of_iterations = (int)[uniquearray count]/300;
            }
            else
            {
                number_of_iterations = (int)[uniquearray count]/300 +1 ;
            }
        }
        DebugLog(@"number of iterations are: %d", number_of_iterations);
        
        for (int numb =0; numb < number_of_iterations; numb++)
        {
            part_array = [[NSMutableArray alloc]init];
            
            if (numb == number_of_iterations -1)
            {
                NSArray *tempArray = [uniquearray subarrayWithRange:NSMakeRange(numb*300, [uniquearray count]-(300*numb))];
                [part_array addObjectsFromArray:tempArray];
            }
            else
            {
                NSArray *tempArray = [uniquearray subarrayWithRange:NSMakeRange(numb*300, 299)];
                [part_array addObjectsFromArray:tempArray];
            }
            
            DebugLog(@"number of elements in part array are: %lu", (unsigned long)[part_array count]);
            
            NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:(NSArray *)part_array options:NSJSONWritingPrettyPrinted error:&err1];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
            NSString *encodedText = [self encodeToPercentEscapeString:jsonString];
            
            //                            DebugLog(@"jsonData as string:\n%@", jsonString);
            
            NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=addbackuppart&access_token=%@&device_id=%@",[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];   //&contacts=%@  ,[jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
            
            DebugLog(@"addbackuppart url: %@",urlString1);
            //            NSError *errorjs= nil;
            
            //        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
            //            NSString *firid = [NSString stringWithFormat:@"800"];
            
            responseData = [NSMutableData data];
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString1]];
            
            NSString *params = [[NSString alloc] initWithFormat:@"contacts=%@",encodedText]; //[jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
            [request setHTTPMethod:@"POST"];
            [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            //            [[NSURLConnection alloc] initWithRequest:request delegate:self];
            NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
            
            if(theConnection)
            {
                responseData = [NSMutableData data];
            }
            else
            {
                DebugLog(@"theConnection is null");
            }
        }
    }
    @catch (NSException *exception) {
        DebugLog(@"Exception: %@", exception.description);
    }
    @finally {
    }
    
    [prefs setObject:@"no" forKey:@"addbackup"];
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    NSMutableArray *registeredarray = [con_array mutableCopy];
    
    for (NSMutableDictionary *changekeydict in registeredarray)
    {
        for( NSString *aKey in [changekeydict allKeys])
        {
            if ([aKey isEqualToString:@"name"]) {
                id objectToPreserve = [changekeydict objectForKey:aKey];
                [changekeydict setObject:objectToPreserve forKey:@"fullname"];
                [changekeydict removeObjectForKey:aKey];
            }
        }
    }
    
    NSMutableArray *regarray = [registeredarray mutableCopy];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //1
    NSString *documentsDirectory = [paths objectAtIndex:0]; //2
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"data.plist"]; //3
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath: path]) //4
    {
        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"data" ofType:@"plist"]; //5
        [fileManager copyItemAtPath:bundle toPath: path error:&error]; //6
    }
    
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    del.arr_contacts_ad= [regarray mutableCopy];
    del.newarr_contacts_ad = [regarray mutableCopy];
    del.device_phonebook_array = [regarray mutableCopy];
    //                        [del loadContactsIntoDB:con_array];
    
    NSMutableArray *data = [[NSMutableArray alloc]init] ;
    data= [con_array mutableCopy];
    //DebugLog(@"bbbbb data: %lu",(unsigned long)[data count]);
    [data writeToFile: path atomically:YES];
    DebugLog(@"conarray mutablecopy count: %ld", (unsigned long)[del.arr_contacts_ad count]);
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        {
            {
                // The user has previously given access, add the contact
                ns = [[NSMutableDictionary alloc]init];
                con_array = [[NSMutableArray alloc]init];
                //contains details for all the contacts
                 ABAddressBookRef UsersAddressBook = ABAddressBookCreateWithOptions(NULL, NULL);
                CFArrayRef ContactInfoArray = ABAddressBookCopyArrayOfAllPeople(UsersAddressBook);
                
                //get the total number of count of the users contact
                CFIndex numberofPeople = CFArrayGetCount(ContactInfoArray);
                //DebugLog(@"%ld",numberofPeople);
                con_array = [[NSMutableArray alloc]init];
                
                //iterate through each record and add the value in the array
                for (int ik =0; ik< numberofPeople; ik++) {
                    ABRecordRef ref1 = CFArrayGetValueAtIndex(ContactInfoArray, ik);
                    ABMultiValueRef names, lastnames;
                    
                    if ((ABRecordCopyValue(ref1, kABPersonFirstNameProperty) == NULL && ABRecordCopyValue(ref1, kABPersonLastNameProperty) == NULL) || ABRecordCopyValue(ref1, kABPersonPhoneProperty) == NULL)
                    {
                    }
                    else
                    {
                    if(ABRecordCopyValue(ref1, kABPersonFirstNameProperty) != NULL)
                        names = (__bridge ABMultiValueRef)((__bridge NSString*)ABRecordCopyValue(ref1, kABPersonFirstNameProperty));
                    else
                        names =@"";
                    if(ABRecordCopyValue(ref1, kABPersonLastNameProperty) != NULL)
                        lastnames = (__bridge ABMultiValueRef)((__bridge NSString*)ABRecordCopyValue(ref1, kABPersonLastNameProperty));
                    else
                        lastnames = @"";
                    
                  //DebugLog(@"name from address book = %@ %@",names,lastnames);  // works fine.
                    NSString *fullname;
                    NSString *fnm = [NSString stringWithFormat:@"%@",names];
                    NSString *lnm = [NSString stringWithFormat:@"%@",lastnames];
                    //        ////DebugLog(@"fnm : %@",fnm);
                    if ([fnm length] ==0 || [fnm isKindOfClass:[NSNull class]] || fnm == (id)[NSNull null] || [fnm isEqualToString:@"(null)"])
                        fullname = [NSString stringWithFormat:@"%@",lastnames];
                    
                    else if ([lnm length] ==0 || [lnm isKindOfClass:[NSNull class]] || lnm == (id)[NSNull null] || [lnm isEqualToString:@"(null)"])
                        fullname = [NSString stringWithFormat:@"%@",names];
                    else
                        fullname = [NSString stringWithFormat:@"%@ %@",names,lastnames];
                    
                    //        NSString *contactName = (__bridge NSString *)(names);
                    
                    ns = [[NSMutableDictionary alloc]init];

                    ABRecordID recordID = ABRecordGetRecordID(ref1);
                    int useridphn = recordID;
                    NSString *idphn = [NSString stringWithFormat:@"p%d",useridphn];
                    
                    [ns setObject:idphn forKey:@"id"];
                    
                    ABMultiValueRef emailProperty = ABRecordCopyValue(ref1, kABPersonEmailProperty);
                    
                    NSArray *emailArray = (__bridge NSArray *)ABMultiValueCopyArrayOfAllValues(emailProperty);
                    NSString *newstringemail= [emailArray componentsJoinedByString:@","];
                    
                    
                    [ns setObject:fullname forKey:@"fullname"];
                    //            [ns setObject:fullname forKey:@"fullname"];
                    [ns setValue:newstringemail forKey:@"email"];
                    
                    if ([newstringemail isKindOfClass:[NSNull class]] || newstringemail == (id)[NSNull null] || [newstringemail length] < 4)
                        [ns setValue:@"" forKey:@"email"];
                    
                    
                    ABMultiValueRef phoneNumberProperty = ABRecordCopyValue(ref1, kABPersonPhoneProperty);
                    NSArray *_phoneNumbers = (__bridge NSArray*)ABMultiValueCopyArrayOfAllValues(phoneNumberProperty);
                    
                    NSMutableArray *mutableph= [_phoneNumbers mutableCopy];
                    int pp;
                    //                            //DebugLog(@"mutable first: %@",mutableph);
                    for (pp =0; pp< [mutableph count]; pp++)
                    {
                        if ([[mutableph objectAtIndex:pp] isKindOfClass:[NSNull class]] || [mutableph objectAtIndex:pp] == (id)[NSNull null] || [[mutableph objectAtIndex:pp] length] < 3)
                        {
                            [mutableph removeObjectAtIndex:pp];
                            pp= pp-1;
                        }
                    }
                    //                            //DebugLog(@"mutable second: %@",mutableph);
                    NSString *newstringph= [mutableph componentsJoinedByString:@","];
                    
                    [ns setValue:newstringph forKey:@"phone"];
                    
                    ABMultiValueRef stt = ABRecordCopyValue(ref1, kABPersonAddressProperty);
                    if (ABMultiValueGetCount(stt) > 0) {
                        CFDictionaryRef dict = ABMultiValueCopyValueAtIndex(stt, 0);
                        //                    //DebugLog(@"dict address contacts: %@",dict);
                        NSString *streetp = CFDictionaryGetValue(dict, kABPersonAddressStreetKey);
                        NSString *cityp = CFDictionaryGetValue(dict, kABPersonAddressCityKey);
                        NSString *zipcdep = CFDictionaryGetValue(dict, kABPersonAddressZIPKey);
                        //                    NSString *statep = CFDictionaryGetValue(dict, kABPersonAddressStateKey);
                        NSString *countryp = CFDictionaryGetValue(dict, kABPersonAddressCountryKey);
                        
                        if ([streetp isKindOfClass:[NSNull class]] || streetp == (id)[NSNull null])
                            [ns setValue:@"" forKey:@"street"];
                        else
                            [ns setValue:streetp forKey:@"street"];
                        
                        if ([cityp isKindOfClass:[NSNull class]] || cityp == (id)[NSNull null])
                            [ns setValue:@"" forKey:@"city"];
                        else
                            [ns setValue:cityp forKey:@"city"];
                        
                        if ([zipcdep isKindOfClass:[NSNull class]] || zipcdep == (id)[NSNull null])
                            [ns setValue:@"" forKey:@"zip"];
                        else
                            [ns setValue:zipcdep forKey:@"street"];
                        
                        if ([countryp isKindOfClass:[NSNull class]] || countryp == (id)[NSNull null])
                            [ns setValue:@"" forKey:@"country"];
                        else
                            [ns setValue:countryp forKey:@"country"];
                        
                        [ns setValue:@"" forKey:@"number"];
                    }
                    else
                    {
                        [ns setValue:@"" forKey:@"number"];
                        [ns setValue:@"" forKey:@"city"];
                        [ns setValue:@"" forKey:@"street"];
                        [ns setValue:@"" forKey:@"zip"];
                        [ns setValue:@"" forKey:@"country"];
                    }
                    //            }
                    //            [ns setObject:addressarr forKey:@"address"];
                    [con_array addObject:ns];
                }
            }
        }
            uniquearraydouble = [[NSMutableArray alloc]init];
            uniquearraydouble = [con_array mutableCopy];
            
            DebugLog(@"uniquearraydouble: %@", con_array);
            NSError *err1 = nil;
            number_of_doubles=0;
            
            part_array_double = [[NSMutableArray alloc]init];
            if ([uniquearraydouble count] <= 300)
            {
                //                            part_array = [uniquearray mutableCopy];
                number_of_doubles=1;
            }
            else
            {
                if ([uniquearraydouble count] % 300 == 0)
                {
                    number_of_doubles = (int)[uniquearraydouble count]/300;
                }
                else
                {
                    number_of_doubles = (int)[uniquearraydouble count]/300 +1 ;
                }
            }
            DebugLog(@"number of items are: %ld", (unsigned long)[uniquearraydouble count]);
            
            for (int numb =0; numb < number_of_doubles; numb++)
            {
                part_array_double = [[NSMutableArray alloc]init];
                if (numb == number_of_doubles -1)
                {
                    NSArray *tempArray1 = [uniquearraydouble subarrayWithRange:NSMakeRange(numb*300, [uniquearraydouble count]-(300*numb))];
                    [part_array_double addObjectsFromArray:tempArray1];
                }
                else
                {
                    NSArray *tempArray1 = [uniquearraydouble subarrayWithRange:NSMakeRange(numb*300, 299)];
                    [part_array_double addObjectsFromArray:tempArray1];
                }
                
                //DebugLog(@"number of elements in part array are: %lu", (unsigned long)[part_array count]);
                
                //=========================================================ADD BACKUP===========================================================//
                
                NSData *jsonData2a = [NSJSONSerialization dataWithJSONObject:(NSArray *)part_array_double options:NSJSONWritingPrettyPrinted error:&err1];
                NSString *jsonStringa = [[NSString alloc] initWithData:jsonData2a encoding:NSUTF8StringEncoding];
                NSString *encodedTexta = [self encodeToPercentEscapeString:jsonStringa];
                
                //                            //DebugLog(@"jsonData as string:\n%@", jsonString);
                
                NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=adddoublefilter&access_token=%@&device_id=%@",[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];   //&contacts=%@  ,[jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                
                responseData = [NSMutableData data];
                
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString1]];
                
                NSString *params = [[NSString alloc] initWithFormat:@"contacts=%@",encodedTexta]; //[jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                [request setHTTPMethod:@"POST"];
                [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
                [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                
                NSError *error;
                NSURLResponse *response;
                NSData* result = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                
                if (result != nil)
                {
                    NSDictionary  *json1aa=[NSJSONSerialization JSONObjectWithData:result options:0 error:&error];
                    //DebugLog(@"json1aa reloadfromweb-e: %@",json1aa);
                    if ([[json1aa objectForKey:@"success"] intValue] ==1)
                    {
                        connectiondoublefiler = connectiondoublefiler +1;
                    }
                }
            }
            
            
            //DebugLog(@"connectiondoublefilter & number of iterations: %d .... %d",connectiondoublefiler,number_of_iterations);
            //                    if (connectiondoublefiler == number_of_iterations)
            {
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                NSError *error;
                
                NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=getdoublefilter&amount=-1&id=%@&access_token=%@&device_id=%@&thumb=true",[prefs objectForKey:@"userid"],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
                
                //DebugLog(@"profile url: %@",urlString1);
                
                NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
                NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
                
                if (signeddataURL1 == nil)
                {
                    alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                                       message:nil
                                                      delegate:self
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                    //                [alert show];
                    //            [SVProgressHUD dismiss];
                }
                else
                {
                    json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                            options:kNilOptions
                                                              error:&error];
                    
                    //DebugLog(@"get Double Filter : %@",json1);
                    con_array = [[NSMutableArray alloc]init];
                    NSDictionary *getdoub_dict = [json1 objectForKey:@"details"];
                    for (NSDictionary *contactdict in [getdoub_dict objectForKey:@"contacts"])
                    {
                        //////////////////////////////////////////   Compare App & Device Contacts   ////////////////////////////////////////////////////////////
                        
                        [con_array addObject:contactdict];
                    }
                    
                    //// Byas ////
                    AppDelegate *del =  (AppDelegate *)[[UIApplication sharedApplication] delegate];
                    del.device_phonebook_array = [con_array mutableCopy];
                    
                    NSArray *prefsarray = (NSArray *)con_array;
                    
                    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                    [def setObject:[NSKeyedArchiver archivedDataWithRootObject:prefsarray] forKey:@"devicefilter"];
                    [def synchronize];
                    
                    //                            [[NSUserDefaults standardUserDefaults]setObject:prefsarray forKey:@"devicefilter"];
                    
                    
                    del.arr_contacts_ad= [con_array mutableCopy];
                    del.newarr_contacts_ad = [con_array mutableCopy];

                    [def setObject:[NSKeyedArchiver archivedDataWithRootObject:prefsarray] forKey:@"Contactslocaldb"];
                    [def synchronize];

                    
                    //DebugLog(@"conarray mutablecopy count: %ld", (unsigned long)[del.arr_contacts_ad count]);
                    //                        [del loadContactsIntoDB:con_array];
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //1
                    NSString *documentsDirectory = [paths objectAtIndex:0];
                    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"data.plist"];
                    
                    NSMutableArray *data = [[NSMutableArray alloc]init] ;
                    data= [con_array mutableCopy];
                    //DebugLog(@"bbbbb data: %lu",(unsigned long)[data count]);
                    [data writeToFile: path atomically:YES];
                }
            }
        }
        
    });

}

-(void)skip_func:(id)sender
{
    ConProfileOwnViewController *con = [[ConProfileOwnViewController alloc]init];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"searchafterreg"];
    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"searchfriends"];

}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data
{
    [responseData appendData:data];
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    DebugLog(@"connection failed");
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    DebugLog(@"connection finished loading");
    
    DebugLog(@"response data - %@", [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
    
    NSError *err;
    
    //    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString1]];
    
    if (responseData != nil)
    {
        NSDictionary  *json1aa=[NSJSONSerialization JSONObjectWithData:responseData options:0 error:&err];
        
        DebugLog(@"this now has json string url: %@",json1aa);
    }
    else
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Error in Server Connection!"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        alert.tag=7;
        [alert show];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    responseData = [NSMutableData data];
}

-(NSString*) encodeToPercentEscapeString:(NSString *)string {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                              (CFStringRef) string,
                                                              NULL,
                                                              (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
}

// Decode a percent escape encoded string.
-(NSString*) decodeFromPercentEscapeString:(NSString *)string {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL,
                                                                              (CFStringRef) string,
                                                                              CFSTR(""),
                                                                              kCFStringEncodingUTF8));
}

@end