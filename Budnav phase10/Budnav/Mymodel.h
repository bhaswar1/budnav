//
//  Mymodel.h
//  Randomtest
//
//  Created by Soumalya Banerjee on 10/8/13.
//  Copyright (c) 2013 Esolz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Mymodel : NSObject {
    NSString *str;
    BOOL db_busy;
}

@property(nonatomic,retain) NSString *str;
@property(nonatomic) BOOL db_busy;

+(Mymodel *)getInstance;

@end
