//
//  ConNewRequestsViewController.h
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 02/06/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConParentTopBarViewController.h"
#import "GAITrackedViewController.h"

@interface ConNewRequestsViewController : GAITrackedViewController<UIAlertViewDelegate>
{
    NSString *base64String,*fullname;
    UIAlertView *alert;
}
@property (nonatomic,retain) NSMutableArray *imported_arr;
@property (nonatomic) int userid;
@end
