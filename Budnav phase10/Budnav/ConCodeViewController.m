//
//  ConCodeViewController.m
//  Contacter
//
//  Created by Bhaswar's MacBook Air on 06/05/14.
//  Copyright (c) 2014 Esolz. All rights reserved.
//

#import "ConCodeViewController.h"
#import "ConAddressViewController.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

@interface ConCodeViewController ()
{
    UITextField *password;
    UIAlertView *alert;
}
@end

@implementation ConCodeViewController
@synthesize mob_num;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor=[UIColor colorWithRed:228.0f/255.0f green:229.0f/255.0f blue:232.0f/255.0f alpha:1];
    
    UILabel *startlb = [[UILabel alloc]initWithFrame:CGRectMake(0, 110, self.view.bounds.size.width, 35)];
    [self.view addSubview:startlb];
    startlb.backgroundColor=[UIColor clearColor];
    startlb.text=@"Your Code";
    startlb.textColor=[UIColor colorWithRed:40.0f/255.0f green:82.0f/255.0f blue:135.0f/255.0f alpha:1];
    startlb.font=[UIFont fontWithName:@"ProximaNova-Bold" size:26];
    startlb.textAlignment=NSTextAlignmentCenter;
    
    UIImageView *longtexts=[[UIImageView alloc]initWithFrame:CGRectMake(15, 170, [UIScreen mainScreen].bounds.size.width-30, 45)];
    
    longtexts.image=[UIImage imageNamed:@"input filed2.png"];
    longtexts.userInteractionEnabled=YES;
    [self.view addSubview:longtexts];
    
    password= [[UITextField alloc]initWithFrame:CGRectMake(0, 5, [UIScreen mainScreen].bounds.size.width-35, 37)];
    [longtexts addSubview:password];
    password.delegate=self;
    password.backgroundColor=[UIColor clearColor];
    password.placeholder=@"Code";
    password.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    password.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    password.textAlignment=NSTextAlignmentCenter;
    password.text=@"123456";
    password.textColor=[UIColor grayColor];
    password.font=[UIFont fontWithName:@"ProximaNova-Regular" size:16];
    password.tintColor=[UIColor grayColor];
    password.keyboardType=UIKeyboardTypeNumberPad;
    password.keyboardAppearance=UIKeyboardAppearanceDark;
    
    //    UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonDidPressed:)];
    //    UIToolbar *toolbar =[[UIToolbar alloc] initWithFrame:CGRectMake(0, 150, self.view.bounds.size.width, 44)];
    //    [toolbar setItems:[NSArray arrayWithObjects:doneItem, nil]];
    //    password.inputAccessoryView = toolbar;
    
    UILabel *msglabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 225, self.view.bounds.size.width-40, 55)];
    [self.view addSubview:msglabel];
    msglabel.backgroundColor=[UIColor clearColor];
    msglabel.text= [NSString stringWithFormat:@"We have sent you an SMS with a code to"];
    msglabel.textColor=[UIColor grayColor];
    msglabel.numberOfLines=1;
    msglabel.font=[UIFont fontWithName:@"ProximaNova-Bold" size:14];
    
    UILabel *msglabel2 = [[UILabel alloc]initWithFrame:CGRectMake(60, 250, 80, 55)];
    [self.view addSubview:msglabel2];
    msglabel2.backgroundColor=[UIColor clearColor];
    msglabel2.text= [NSString stringWithFormat:@"the number"];
    msglabel2.textColor=[UIColor grayColor];
    msglabel2.numberOfLines=1;
    msglabel2.font=[UIFont fontWithName:@"ProximaNova-Bold" size:14];
    
    UILabel *msglabel3 = [[UILabel alloc]initWithFrame:CGRectMake(140, 250, 180, 55)];
    [self.view addSubview:msglabel3];
    msglabel3.backgroundColor=[UIColor clearColor];
    msglabel3.text= [NSString stringWithFormat:@"%@",mob_num];
    msglabel3.textColor=[UIColor blackColor];
    msglabel3.numberOfLines=1;
    msglabel3.font=[UIFont fontWithName:@"ProximaNova-Regular" size:15.5f];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    UIButton *nextbt = [UIButton buttonWithType:UIButtonTypeCustom];
    nextbt.frame = CGRectMake(15, msglabel3.frame.origin.y+msglabel3.frame.size.height+15, [UIScreen mainScreen].bounds.size.width-30, 45);
    nextbt.layer.cornerRadius=5;
    [nextbt setBackgroundImage:[UIImage imageNamed:@"next.png"] forState:UIControlStateNormal];
    nextbt.backgroundColor=[UIColor clearColor];
    [nextbt addTarget:self action:@selector(gotonext:) forControlEvents:UIControlEventTouchUpInside];
    //    [nextbt setShowsTouchWhenHighlighted:YES];
    [self.view addSubview:nextbt];
    
    UIButton *helpbt = [UIButton buttonWithType:UIButtonTypeCustom];
    helpbt.frame = CGRectMake(15, self.view.bounds.size.height-40, [UIScreen mainScreen].bounds.size.width-30, 30);
    helpbt.layer.cornerRadius=5;
    helpbt.titleLabel.font=[UIFont fontWithName:@"ProximaNova-Regular" size:18];
    [helpbt setTitle:@"Back" forState:UIControlStateNormal];
    [helpbt setTitleColor:[UIColor colorWithRed:17.0f/255.0f green:61.0f/255.0f blue:125.0f/255.0f alpha:1] forState:UIControlStateNormal];
    helpbt.backgroundColor=[UIColor clearColor];
    [helpbt addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:helpbt];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)doneButtonDidPressed:(id)sender {
//    [password resignFirstResponder];
//}
//
//+ (CGFloat)toolbarHeight {
//    // This method will handle the case that the height of toolbar may change in future iOS.
//    return 44.f;
//}

-(void)dismissKeyboard {
    [password resignFirstResponder];
}

-(void)goBack: (id)sender
{
    
    CATransition *transition = [CATransition animation];
    
    transition.duration = 0.4f;
    
    transition.type = kCATransitionFade;
    
    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)gotonext:(id)sender
{
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    
    if ([[password.text stringByTrimmingCharactersInSet:whitespace] length] == 0 || !([password.text rangeOfCharacterFromSet:notDigits].location == NSNotFound))
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter the Correct Code sent to You"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
    }
    else
    {
        ConAddressViewController *pre= [[ConAddressViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:pre animated:YES];
    }
}

@end