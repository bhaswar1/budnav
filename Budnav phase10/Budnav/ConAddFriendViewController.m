#import "ConAddFriendViewController.h"
#import <AddressBookUI/AddressBookUI.h>
#import "AppDelegate.h"
#import "ConProfileOwnViewController.h"
#import "ConManageRequestsViewController.h"
#import "ConNewRequestsViewController.h"
#import "iToast.h"
#import "ConAccountSettingsViewController.h"
#import "ConInviteViewController.h"
#import "ConNewRequestsViewController.h"
#import "SVProgressHUD.h"
#import "ConSyncLoaderViewController.h"
#import "ConProfileOwnViewController.h"
#import "ConRequestViewController.h"
#import "ConQROptionsViewController.h"
#import "ConNewProfileViewController.h"
#import "Reachability.h"
#import "RTSpinKitView.h"
#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"
#import "ConLocateGroupViewController.h"
#import "ConFullSettingsViewController.h"
#import "ConPersonalProfileViewController.h"

@interface ConAddFriendViewController ()<UIGestureRecognizerDelegate>
{
    NSDictionary *animals,*json1;
    UIImageView *profile_imgv;
    NSArray *animalSectionTitles,*dictkeysarr;
    NSArray *animalIndexTitles, *anintit;
    UITableView *tab_contact;
    NSMutableArray *con_array,*final_con_array,*filtered_arr,*addallarray, *selected_array, *deletedIndex;
    NSMutableDictionary *add_contacts_dict;
    NSMutableArray *app_contacts, *check_app_cont, *phonesarr, *inviteallarr;
    UIAlertView *alert;
    int currentRow,currentSection,height,move;
    NSArray *searchresults;
    NSCharacterSet *whitespace;
    NSString *st, *fullName;
    UILabel *nouserlb,*name;
    UIView *invitecountview,*TableFooter, *loader_backView;
    UIView *line_view;
    NSOperationQueue *names,*fetch;
    BOOL filter,isLoadingMore, backTapped;
    NSString *firstname;
    CGSize maximumSize,myStringSize;
    UIFont *myFont;
    int i,start, indexReload, urlindexnow;
    UIActivityIndicatorView *act;
    UIView *pulseLoaderView;
    RTSpinKitView *PulseSpinner_forLoadMore;
    long total_count,getTotalArrayCount;
    UIButton *request_imgv;
    UIActivityIndicatorView *actfooter, *act1;
    UILabel *backLabel,*searching;
    UIImageView *bck_img, *logo_img, *typeimg;
    
    NSTimer *timer;
    
    AppDelegate *appDel;
    
}
@end
@implementation ConAddFriendViewController
@synthesize mainview,sendarray,invite_send;
@synthesize searchfriends;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"RecommendedClicked"];
    
    self.tabBarController.tabBar.hidden = YES;
    DebugLog(@"SEARCHFRIENDZ=========>%@ &&  %@",searchfriends,[[NSUserDefaults standardUserDefaults]objectForKey:@"searchfriends"]);
    
    [self setSearchIconToFavicon];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    self.tabBarController.tabBar.translucent = YES;
    
    appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDel sendingRequest];
    
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    del.recom_search_string = @"null";i=1;filter=NO;
    isLoadingMore=NO;
    start=0;
    total_count=30;
    getTotalArrayCount=0;
    app_contacts = [[NSMutableArray alloc]init];
    addallarray = [[NSMutableArray alloc] init];
    indexReload = -1;
    urlindexnow = 0;
    
    UIView *coverview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64)];
    [self.view addSubview:coverview];
    
    UIColor *darkOp =
    [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
    UIColor *lightOp =
    [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, coverview.frame.size.height);
    
    // Add the gradient to the view
    [coverview.layer insertSublayer:gradient atIndex:0];
    
    bck_img = [[UIImageView alloc]initWithFrame:CGRectMake(15,32,12,20)];//(13, 39, 10, 22)];
    bck_img.image=[UIImage imageNamed:@"back3"];
    [self.view addSubview:bck_img];
    
    
    logo_img = [[UIImageView alloc]init];
    logo_img.frame = CGRectMake(27+([UIScreen mainScreen].bounds.size.width-160)/2, 32, 101, 20);
    logo_img.backgroundColor = [UIColor clearColor];
    
    logo_img.image = [UIImage imageNamed:@"new_logo"];
    [coverview addSubview:logo_img];
    
    
    backLabel=[[UILabel alloc]initWithFrame:CGRectMake(bck_img.frame.origin.x+bck_img.frame.size.width+5, 30, 50, 20)];
    backLabel.text=@"Back";
    backLabel.textColor=[UIColor whiteColor];
    backLabel.contentMode=UIViewContentModeScaleAspectFit;
    backLabel.backgroundColor=[UIColor clearColor];
    
    //appDel.selected_array = [[NSMutableArray alloc] init];
   // appDel.deletedIndex = [[NSMutableArray alloc] init];
    
    
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    self.screenName = @"Add contact";
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createEventWithCategory:@"Contacts"
                                             action:@"Add contact"
                                              label:nil
                                              value:nil] build]];

    
    
    DebugLog(@"filter status: %x", filter);
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    }
    self.view.frame=CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
    
    sendarray =[[NSMutableArray alloc]init];
    move=0;
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del showTabStatus:YES];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"firstContacts"];
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"searchfriends"] isEqualToString:@"yes"]) {
        
        self.navigationController.navigationBarHidden=YES;
        
        
        UIButton *nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        nextBtn.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-75, 5, 90, 60);
        nextBtn.backgroundColor=[UIColor blackColor];
        [nextBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [nextBtn setTitle:[NSString stringWithFormat:@"Next"] forState:UIControlStateNormal];
        [nextBtn addTarget:self action:@selector(nextBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:nextBtn];
        
        UIButton *falseBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        falseBtn.frame = CGRectMake(0, 5, 90, 60);
        falseBtn.backgroundColor=[UIColor clearColor];
        [self.view addSubview:falseBtn];
        
    }
    else{
        
        del.tabBarController.tabBar.hidden = YES;
        self.navigationController.navigationBarHidden=YES;
        
        UIView *coverview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64)];
        [self.view addSubview:coverview];
        
        UIColor *darkOp =
        [UIColor colorWithRed:(0.0f/255.0f) green:(169.0f/255.0f) blue:(157.0f/255.0f) alpha:1.0];
        UIColor *lightOp =
        [UIColor colorWithRed:(41.0f/255.0) green:(171.0f/255.0f) blue:(210.0f/255.0f) alpha:1.0];
        
        // Create the gradient
        CAGradientLayer *gradient = [CAGradientLayer layer];
        
        // Set colors
        gradient.colors = [NSArray arrayWithObjects:
                           (id)lightOp.CGColor,
                           (id)darkOp.CGColor,
                           nil];
        
        // Set bounds
        gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, coverview.frame.size.height);
        
        // Add the gradient to the view
        [coverview.layer insertSublayer:gradient atIndex:0];
        
        
        UIView *backbuttonextended = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 85, 64)];
        backbuttonextended.backgroundColor=[UIColor clearColor];
        [self.view addSubview:backbuttonextended];
        backbuttonextended.userInteractionEnabled=YES;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goback)];
        [backbuttonextended addGestureRecognizer:tap];
        
        bck_img = [[UIImageView alloc]initWithFrame:CGRectMake(15,32,12,20)];//(13, 39, 10, 22)];
        bck_img.image=[UIImage imageNamed:@"back3"];
        [self.view addSubview:bck_img];
        
        
        logo_img = [[UIImageView alloc]init];
        logo_img.frame = CGRectMake(27+([UIScreen mainScreen].bounds.size.width-160)/2, 32, 101, 20);
        logo_img.backgroundColor = [UIColor clearColor];
        
        logo_img.image = [UIImage imageNamed:@"new_logo"];
        [coverview addSubview:logo_img];
        
        
        backLabel=[[UILabel alloc]initWithFrame:CGRectMake(bck_img.frame.origin.x+bck_img.frame.size.width+5, 30, 50, 20)];
        backLabel.text=@"Back";
        backLabel.textColor=[UIColor whiteColor];
        backLabel.contentMode=UIViewContentModeScaleAspectFit;
        backLabel.backgroundColor=[UIColor clearColor];
        
        
        UIButton *helpbt = [UIButton buttonWithType:UIButtonTypeCustom];
        helpbt.frame = CGRectMake(0, 0, logo_img.frame.origin.x
                                  -1, 64);//(10, 35, 8, 17);
        helpbt.backgroundColor=[UIColor clearColor];
        [helpbt setTitle:nil forState:UIControlStateNormal];  //Back button.png
        
        [helpbt addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDown];
        [helpbt addTarget:self action:@selector(changecoloragain) forControlEvents:UIControlEventTouchDragExit];
        [helpbt addTarget:self action:@selector(changecolor) forControlEvents:UIControlEventTouchDragEnter];
        
        [helpbt addTarget:self action:@selector(goback) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:helpbt];
    }
    
    
    currentRow=-1;
    height=0;
    currentSection=-1;
    
    mainview = [[UIView alloc]initWithFrame:CGRectMake(0, 64, [UIScreen mainScreen].bounds.size.width, self.view.frame.size.height-64)];
    [self.view addSubview:mainview];
    mainview.backgroundColor=[UIColor whiteColor];
    
    CGRect frame = CGRectMake(110,225,[UIScreen mainScreen].bounds.size.width-220,100);
    
    loader_backView = [[UIView alloc] init];
    
    loader_backView.frame = CGRectMake(frame.origin.x-25, frame.origin.y-25, frame.size.width+50,frame.size.height+50);
    
    loader_backView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:loader_backView];
    
    
    
    act = [[UIActivityIndicatorView alloc] initWithFrame:frame];
    
    [act startAnimating];
    
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    
    [act setColor:[UIColor grayColor]];
    
    [self.view addSubview:act];
    
    
    searching = [[UILabel alloc] init];

    searching.frame = CGRectMake(0, 115, loader_backView.frame.size.width, 25);
    
    searching.text = @"Searching friends";
    searching.backgroundColor=[UIColor clearColor];
    
    searching.textColor = [UIColor grayColor];
    
     searching.textAlignment = NSTextAlignmentCenter;
    
    [loader_backView addSubview:searching];
    
    
//    ------------- Remove the View & Label in CellforRow --------------
    
    
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
    searchBar.delegate=self;
    
    searchBar.tintColor=[UIColor darkGrayColor];
    searchBar.backgroundColor=[UIColor whiteColor];
    [searchBar setSearchBarStyle:UISearchBarStyleMinimal];
    searchBar.barTintColor = [UIColor clearColor];
    searchBar.backgroundImage=[UIImage new];
    
    UILabel *lbdivider = [[UILabel alloc]initWithFrame:CGRectMake(0, searchBar.frame.origin.y + searchBar.frame.size.height, self.view.frame.size.width, 0.6f)];
    [mainview addSubview:lbdivider];
    lbdivider.backgroundColor = [UIColor colorWithRed:238.0f/255.0f green:238.0f/255.0f blue:238.0f/255.0f alpha:1];
    
    UITextField *searchField = [searchBar valueForKey:@"_searchField"];
    searchField.textColor = [UIColor colorWithRed:(0.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0f];
    searchField.backgroundColor = [UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f];
    
    NSAttributedString *str = [[NSAttributedString alloc] initWithString:@"Search new contacts" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:(0.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:0.7f] }];
    searchField.attributedPlaceholder = str;
    searchBar.keyboardAppearance=UIKeyboardAppearanceDark;
    [mainview addSubview:searchBar];
    searchBar.userInteractionEnabled = YES;
    
    // Give some left padding between the edge of the search bar and the text the user enters
    searchBar.searchTextPositionAdjustment = UIOffsetMake(10, 0);
    
    if (!filter) {
     
        
    }
    
    
    if (![del.recom_search_string isEqualToString:@"null"])
    {
        searchBar.text=st;
        filtered_arr =[del.recom_search_array mutableCopy];
        filter = YES;
        
        nouserlb= [[UILabel alloc]initWithFrame:CGRectMake(0, 50, [UIScreen mainScreen].bounds.size.width, 30)];//CGRectMake(0, 100, 320, 30)];
        [mainview addSubview:nouserlb];
        nouserlb.text=@"No results";
        nouserlb.backgroundColor=[UIColor clearColor];
        nouserlb.textAlignment=NSTextAlignmentCenter;
        nouserlb.textColor=[UIColor colorWithRed:238.0f/255.0f green:238.0f/255.0f blue:238.0f/255.0f alpha:1];
        nouserlb.hidden=YES;
        
        
        tab_contact=[[UITableView alloc]initWithFrame:CGRectMake(0, 50, [UIScreen mainScreen].bounds.size.width, mainview.frame.size.height-50)];//CGRectMake(0, 100, 320, mainview.frame.size.height-100)];
        tab_contact.backgroundColor=[UIColor clearColor];
        tab_contact.dataSource=self;
        tab_contact.delegate=self;
        tab_contact.separatorStyle=UITableViewCellSeparatorStyleNone;
        tab_contact.showsVerticalScrollIndicator=NO;
        if ([tab_contact respondsToSelector:@selector(setSectionIndexColor:)]) {
            tab_contact.sectionIndexColor = [UIColor colorWithRed:151.0f/255.0f green:26.0f/255.0f blue:26.0f/255.0f alpha:1];
            tab_contact.sectionIndexTrackingBackgroundColor = [UIColor whiteColor];
        }
        
        fetch =[NSOperationQueue new];
        [fetch addOperationWithBlock:^{
            [self FetchURLRequest:start];
        }];
        DebugLog(@"app contacts in recommendations page: %@",app_contacts);
        [mainview addSubview:tab_contact];
        [tab_contact reloadData];
        
        del.recom_search_string = @"null";
    }
    else
    {
        
        filter = 0;
        DebugLog(@"eschhe to ekhanei");
        
        DebugLog(@"start ta ekhon hochhe: %d", start);
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"RecommendedClicked"] isEqualToString:@"no"])
        {
            fetch =[NSOperationQueue new];
            [self FetchURLRequest:start];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"RecommendedClicked"];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            nouserlb= [[UILabel alloc]initWithFrame:CGRectMake(0, 100, [UIScreen mainScreen].bounds.size.width, 30)];
            [mainview addSubview:nouserlb];
            nouserlb.text=@"No results";
            nouserlb.backgroundColor=[UIColor clearColor];
            nouserlb.textAlignment=NSTextAlignmentCenter;
            nouserlb.textColor=[UIColor colorWithRed:238.0f/255.0f green:238.0f/255.0f blue:238.0f/255.0f alpha:1];
            nouserlb.hidden = YES;
            
            
            tab_contact=[[UITableView alloc]initWithFrame:CGRectMake(0, 50, [UIScreen mainScreen].bounds.size.width, mainview.frame.size.height-50)];
            [mainview addSubview:tab_contact];
            tab_contact.backgroundColor=[UIColor clearColor];
            tab_contact.dataSource=self;
            tab_contact.delegate=self;
            tab_contact.separatorStyle=UITableViewCellSeparatorStyleNone;
            tab_contact.showsVerticalScrollIndicator=NO;
            
            if ([tab_contact respondsToSelector:@selector(setSectionIndexColor:)]) {
                tab_contact.sectionIndexColor = [UIColor colorWithRed:151.0f/255.0f green:26.0f/255.0f blue:26.0f/255.0f alpha:1];
                tab_contact.sectionIndexTrackingBackgroundColor = [UIColor whiteColor];
            }
            searchBar.userInteractionEnabled=YES;
            [del showTabValues:YES];
        });
    }
    //--------------------------------------------------------------------------------------------------------------
    
//    timer = [NSTimer scheduledTimerWithTimeInterval:30.0f target:self
//                                                    selector:@selector(sendingRequest)
//                                                    userInfo:nil
//                                                     repeats:NO];

    
}

-(void)FetchURLRequest:(int)start1
{
    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(q, ^{
        
        @try {
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            
            NSError *error = nil;
            con_array = [[NSMutableArray alloc]init];
            NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=recommended&start=%d&access_token=%@&device_id=%@&thumb=true",start1,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
            
            DebugLog(@"recommendation url: %@",urlString1);
            
            NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
            NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
            
            if (signeddataURL1 == nil)
            {
                [self performSelectorOnMainThread:@selector(serverError) withObject:nil waitUntilDone:YES];
            }
            else
            {
                json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                         
                                                        options:kNilOptions
                         
                                                          error:&error];
                if (!json1) {
                    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Error in Server. Please login again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alert1 show];
                }
                
                NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
                DebugLog(@"err  %@",errornumber);
                
                if (![errornumber isEqualToString:@"0"])
                {
                    DebugLog(@"error");
                    
                }
                else
                {
                    
                    DebugLog(@"details req= %@",[json1 objectForKey:@"details"]);
                    if ([[json1 objectForKey:@"details"] isKindOfClass:[NSNull class]] || [json1 objectForKey:@"details"] == (id)[NSNull null] ||[[json1 objectForKey:@"details"] count] ==0)
                    {
                        DebugLog(@"App Contacts = 0");
                    }
                    else
                    {
                        NSDictionary *contacts_temp_dict = [json1 objectForKey:@"details"];
                        if ([contacts_temp_dict objectForKey:@"hasbackup"])
                        {
                            urlindexnow = 0;
                            for (NSDictionary *dict in [contacts_temp_dict objectForKey:@"recommended"])
                            {
                                ////////////////////////// Newly Added /////////////////////////////
                                
                                DebugLog(@"AppDel Selected Array:%@",appDel.selected_array);
                                
                                if ([appDel.selected_array count]>0)
                                {
                                    int found = 0;
                                    for (int selectIndex = 0; selectIndex<[appDel.selected_array count]; selectIndex++)
                                    {
                                        if ([[dict objectForKey:@"id"] intValue]==[[appDel.selected_array objectAtIndex:selectIndex] intValue])
                                        {
                                            found++;
                                            break;
                                        }
                                    }
                                    
                                    if (found==0)
                                    {
                                        urlindexnow++ ;
                                        [app_contacts addObject:dict];
                                    }
                                }
                                else
                                {
                                
                                    urlindexnow++ ;
                                    [app_contacts addObject:dict];
                                }
                                
                                
                            }
                            
                        }
                        
                    }
                }
                int rec;
                for (rec = 0; rec < [app_contacts count]; rec++)
                {
                    if ([[[app_contacts objectAtIndex:rec]objectForKey:@"id"]intValue] == [[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"] intValue])
                    {
                        [app_contacts removeObjectAtIndex:rec];
                    }
                }
                
                for (NSDictionary *nsd in app_contacts)
                {
                    [addallarray addObject:[nsd objectForKey:@"id"]];
                }
                getTotalArrayCount=app_contacts.count;
            }
            
            DebugLog(@"app contacts now: %@", app_contacts);
            if ([app_contacts count]==0) {
                
                [self performSelectorOnMainThread:@selector(noFriend) withObject:nil waitUntilDone:YES];
            }
            
            
        }
        @catch (NSException *exception) {
            DebugLog(@"exception is : %@",exception.description);
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [tab_contact reloadData];
        });
    });
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!filter)
    {
        return [app_contacts count];
    }
    else
        return [filtered_arr count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tab_contact dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
   // cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    [act stopAnimating];
    [loader_backView removeFromSuperview];
    [searching removeFromSuperview];
    
    if (!filter)
    {
        maximumSize = CGSizeMake(200, 50);
        firstname= [[app_contacts objectAtIndex:indexPath.row] objectForKey:@"name"];
        myFont = [UIFont fontWithName:@"ProximaNova-Bold" size:17];
        
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
        myStringSize = CGSizeMake(200,0);
        NSDictionary *attributes = @{NSFontAttributeName: myFont};
        CGRect rect = [firstname boundingRectWithSize:myStringSize options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:attributes context:nil];
        
        profile_imgv = [[UIImageView alloc] init];
        profile_imgv.frame = CGRectMake(15, 12, 50, 50);
        profile_imgv.backgroundColor = [UIColor clearColor];
        profile_imgv.image = [UIImage imageNamed:@"Profile picture"];
        [cell addSubview:profile_imgv];
        
        name = [[UILabel alloc]init];
        name.frame = CGRectMake(profile_imgv.frame.origin.x+profile_imgv.frame.size.width+15, profile_imgv.frame.origin.y-5, 160, 40);//rect.size.height);//rect;
        
#else
        myStringSize = [firstname sizeWithFont:myFont constrainedToSize:maximumSize lineBreakMode:NSLineBreakByWordWrapping];
        name = [[UILabel alloc]init];
        name.frame = CGRectMake(20, 0, myStringSize.width, 30);
        
#endif
        [cell addSubview:name];
        name.backgroundColor=[UIColor clearColor];
        name.lineBreakMode = YES;
        name.lineBreakMode = NSLineBreakByWordWrapping;
        name.numberOfLines = 2;
        
        
        if ([[[app_contacts objectAtIndex:indexPath.row]objectForKey:@"name"] isKindOfClass:[NSNull class]] || [[[app_contacts objectAtIndex:indexPath.row]objectForKey:@"name"] isEqualToString:@""] || [[[app_contacts objectAtIndex:indexPath.row]objectForKey:@"name"] isEqualToString:@"(null)"] || [[[app_contacts objectAtIndex:indexPath.row]objectForKey:@"name"] length]==0) {
            
            if ([[[app_contacts objectAtIndex:indexPath.row]objectForKey:@"surname"] isKindOfClass:[NSNull class]] || [[[app_contacts objectAtIndex:indexPath.row]objectForKey:@"surname"] isEqualToString:@""] || [[[app_contacts objectAtIndex:indexPath.row]objectForKey:@"surname"] isEqualToString:@"(null)"] || [[[app_contacts objectAtIndex:indexPath.row]objectForKey:@"surname"] length]==0) {
                
                fullName = @"";
            }
            
            else
            {
                fullName = [NSString stringWithFormat:@"%@",[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"surname"]];
            }
            
        }
        
        else
        {
            if ([[[app_contacts objectAtIndex:indexPath.row]objectForKey:@"surname"] isKindOfClass:[NSNull class]] || [[[app_contacts objectAtIndex:indexPath.row]objectForKey:@"surname"] isEqualToString:@""] || [[[app_contacts objectAtIndex:indexPath.row]objectForKey:@"surname"] isEqualToString:@"(null)"] || [[[app_contacts objectAtIndex:indexPath.row]objectForKey:@"surname"] length]==0) {
                
                fullName = [NSString stringWithFormat:@"%@",[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"name"]];
            }
            
            else
            {
                fullName = [NSString stringWithFormat:@"%@ %@",[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"name"],[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"surname"]];
            }
            
        }
        
        
        name.text = fullName;
        name.font=[UIFont fontWithName:@"ProximaNova-Bold" size:15];
        name.textAlignment=NSTextAlignmentLeft;
        
        request_imgv = [UIButton buttonWithType:UIButtonTypeCustom];
        request_imgv.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-65, 18.5, 50, 37);
        request_imgv.backgroundColor = [UIColor clearColor];
        [request_imgv setBackgroundImage:[UIImage imageNamed:@"Request"] forState:UIControlStateNormal];
        
        
        [cell addSubview:request_imgv];
        
        cell.tag = indexPath.row;
        request_imgv.tag = indexPath.row;
        
        UIButton *transparentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        transparentBtn.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-65, 0, 65, 74);
        [transparentBtn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        transparentBtn.tag = indexPath.row;
        [cell addSubview:transparentBtn];
        
         [transparentBtn addTarget:self action:@selector(addfriendwithbt:) forControlEvents:UIControlEventTouchUpInside];
        //[request_imgv addTarget:self action:@selector(addfriendwithbt:) forControlEvents:UIControlEventTouchUpInside];
        
        DebugLog(@"%d",i);
        i++;
        NSString *location;
        
        if ([[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"city"] isKindOfClass:[NSNull class]] || [[app_contacts objectAtIndex:indexPath.row] objectForKey:@"city"] == (id)[NSNull null]|| [[NSString stringWithFormat:@"%@",[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"city"]] isEqualToString:@""])// || [[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"city"] length] ==0
        {
            
            DebugLog(@"city null");
            if ([[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"country"] isKindOfClass:[NSNull class]] || [[app_contacts objectAtIndex:indexPath.row] objectForKey:@"country"] == (id)[NSNull null]|| [[NSString stringWithFormat:@"%@",[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"country"]] isEqualToString:@""] || [[NSString stringWithFormat:@"%@",[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"country"]] isEqualToString:@""])// || [[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"city"] length] ==0
            {
                DebugLog(@"country null");
                
            }
            else
            {
                DebugLog(@"country present");
                location= [NSString stringWithFormat:@"%@",[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"country"]];
            }
            
        }else if (([[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"city"] isKindOfClass:[NSNull class]] || [[app_contacts objectAtIndex:indexPath.row] objectForKey:@"city"] == (id)[NSNull null] || [[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"city"] isEqualToString:@""]) && ([[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"country"] isKindOfClass:[NSNull class]] || [[app_contacts objectAtIndex:indexPath.row] objectForKey:@"country"] == (id)[NSNull null] || [[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"country"] isEqualToString:@""]))// || [[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"city"] length] ==0
        {
            DebugLog(@"city & country null");
            location= [NSString stringWithFormat:@"%@ %@",[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"city"],[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"country"]];
            
        }
        else if (![[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"city"] isKindOfClass:[NSNull class]] || [[app_contacts objectAtIndex:indexPath.row] objectForKey:@"city"] != (id)[NSNull null]|| ![[NSString stringWithFormat:@"%@",[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"city"]] isEqualToString:@""])// || [[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"city"] length] ==0
        {
            
            // DebugLog(@"city null");
            if ([[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"country"] isKindOfClass:[NSNull class]] || [[app_contacts objectAtIndex:indexPath.row] objectForKey:@"country"] == (id)[NSNull null]|| [[NSString stringWithFormat:@"%@",[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"country"]] isEqualToString:@""] || [[NSString stringWithFormat:@"%@",[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"country"]] isEqualToString:@""])// || [[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"city"] length] ==0
            {
                DebugLog(@"country null");
                location= [NSString stringWithFormat:@"%@",[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"city"]];
            }
            else
            {
                location= [NSString stringWithFormat:@"%@, %@",[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"city"],[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"country"]];
            }
            
        }
        else {
            
            DebugLog(@"city & country has 1st");
            location= [NSString stringWithFormat:@"%@, %@",[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"city"],[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"country"]];
        }
        
        
        UILabel *locationlb = [[UILabel alloc]initWithFrame:CGRectMake(name.frame.origin.x, name.frame.origin.y+name.frame.size.height, 210, 20)];
        [cell.contentView addSubview:locationlb];
        locationlb.backgroundColor=[UIColor clearColor];
        locationlb.textColor=[UIColor lightGrayColor];
        locationlb.text=location;
        locationlb.font=[UIFont fontWithName:@"ProximaNova-Regular" size:12];
        locationlb.textAlignment=NSTextAlignmentLeft;
        
        
        NSString *base64String1= [[app_contacts objectAtIndex:indexPath.row] objectForKey:@"thumb"];
        if ([base64String1 length] > 6)
        {
            NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String1 options:0];
            UIImage *profilepic = [UIImage imageWithData:decodedData];
            profile_imgv.image=profilepic;
            profile_imgv.contentMode= UIViewContentModeScaleAspectFill;
            profile_imgv.clipsToBounds=YES;
        }
        
       /////////////////////////////////////////// Added on 29.06.16 ///////////////////////////////////////////////
        typeimg = [[UIImageView alloc]init];
      //  [cell addSubview:typeimg];
        typeimg.frame= CGRectMake([UIScreen mainScreen].bounds.size.width-42, 26, 22, 22);
        typeimg.image=[UIImage imageNamed:@"circle"];
        
        typeimg.userInteractionEnabled = YES;
        
        
        
        UIImageView *divider = [[UIImageView alloc]initWithFrame:CGRectMake(0, 74, [UIScreen mainScreen].bounds.size.width, 0.6f)];
        [cell.contentView addSubview:divider];
        divider.image= [UIImage imageNamed:@"divider_contact.png"];
        
        cell.tag = indexPath.row;
        
        BOOL isSelected = [self.selectedIndexPaths containsObject:indexPath];
        if (isSelected)
        {
            cell.backgroundColor = [self colorWithHexString:@"DCDCDC"];
        }
        else
        {
            cell.backgroundColor = [UIColor clearColor];
        }
        
        if (indexPath.row == indexReload)
        {
            DebugLog(@"Request Sending!!!");
            
//            if ([locationlb.text isEqualToString:@"Request Sent"])
//            {
//                locationlb.text = @"";
//                 [request_imgv setBackgroundImage:[UIImage imageNamed:@"Request"] forState:UIControlStateNormal];
//            }
//            else
//            {
            [request_imgv setBackgroundImage:[UIImage imageNamed:@"Request sent"] forState:UIControlStateNormal];
            
            locationlb.text = @"Request Sent";
            locationlb.textColor = [UIColor colorWithRed:102.0f/255.0f green:153.0f/255.0f blue:102.0f/255.0f alpha:1];
            locationlb.font=[UIFont fontWithName:@"ProximaNova-Bold" size:12];
            //}
        }
        
    }
    //        ////////////////////////////////         Filter = 1          //////////////////////////////////
    else
    {
        DebugLog(@"filtered array count: %lu", (unsigned long)[filtered_arr count]);
        maximumSize = CGSizeMake(160, 50);
        firstname= [[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"name"];
        myFont = [UIFont fontWithName:@"ProximaNova-Bold" size:21];
        
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
        myStringSize = CGSizeMake(200,0);
        NSDictionary *attributes = @{NSFontAttributeName: myFont};
        CGRect rect = [firstname boundingRectWithSize:myStringSize options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:attributes context:nil];
        
        profile_imgv = [[UIImageView alloc] init];
        profile_imgv.frame = CGRectMake(15, 12, 50, 50);
        profile_imgv.backgroundColor = [UIColor clearColor];
        profile_imgv.image = [UIImage imageNamed:@"Profile picture"];
        [cell addSubview:profile_imgv];
        
        name = [[UILabel alloc]init];
        name.frame = CGRectMake(profile_imgv.frame.origin.x+profile_imgv.frame.size.width+15, profile_imgv.frame.origin.y-5, 160, 40);//rect;
        
#else
        myStringSize = [firstname sizeWithFont:myFont constrainedToSize:maximumSize lineBreakMode:NSLineBreakByWordWrapping];
        name = [[UILabel alloc]init];
        name.frame = CGRectMake(20, 0, myStringSize.width, 30);
        
#endif
        [cell addSubview:name];
        name.backgroundColor=[UIColor clearColor];
        name.lineBreakMode = YES;
        name.lineBreakMode = NSLineBreakByWordWrapping;
        name.numberOfLines = 2;
        
        
        
        if ([[[filtered_arr objectAtIndex:indexPath.row]objectForKey:@"name"] isKindOfClass:[NSNull class]] || [[[filtered_arr objectAtIndex:indexPath.row]objectForKey:@"name"] isEqualToString:@""] || [[[filtered_arr objectAtIndex:indexPath.row]objectForKey:@"name"] isEqualToString:@"(null)"] || [[[filtered_arr objectAtIndex:indexPath.row]objectForKey:@"name"] length]==0) {
            
            if ([[[filtered_arr objectAtIndex:indexPath.row]objectForKey:@"surname"] isKindOfClass:[NSNull class]] || [[[filtered_arr objectAtIndex:indexPath.row]objectForKey:@"surname"] isEqualToString:@""] || [[[filtered_arr objectAtIndex:indexPath.row]objectForKey:@"surname"] isEqualToString:@"(null)"] || [[[filtered_arr objectAtIndex:indexPath.row]objectForKey:@"surname"] length]==0) {
                
                fullName = @"";
            }
            
            else
            {
                fullName = [NSString stringWithFormat:@"%@",[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"surname"]];
            }
            
        }
        
        else
        {
            if ([[[filtered_arr objectAtIndex:indexPath.row]objectForKey:@"surname"] isKindOfClass:[NSNull class]] || [[[filtered_arr objectAtIndex:indexPath.row]objectForKey:@"surname"] isEqualToString:@""] || [[[filtered_arr objectAtIndex:indexPath.row]objectForKey:@"surname"] isEqualToString:@"(null)"] || [[[filtered_arr objectAtIndex:indexPath.row]objectForKey:@"surname"] length]==0) {
                
                fullName = [NSString stringWithFormat:@"%@",[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"name"]];
            }
            
            else
            {
                fullName = [NSString stringWithFormat:@"%@ %@",[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"name"],[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"surname"]];
            }
            
        }
        
        
        name.text = fullName;
        name.font=[UIFont fontWithName:@"ProximaNova-Bold" size:15];
        name.textAlignment=NSTextAlignmentLeft;
        
        
        cell.tag = indexPath.row;
        request_imgv.tag = indexPath.row;
        
        
        UIButton *transparentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        transparentBtn.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-65, 0, 65, 74);
        [transparentBtn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        transparentBtn.tag = indexPath.row;
        [cell addSubview:transparentBtn];
        
        [transparentBtn addTarget:self action:@selector(addfriendwithbt:) forControlEvents:UIControlEventTouchUpInside];
        
       // [request_imgv addTarget:self action:@selector(addfriendwithbt:) forControlEvents:UIControlEventTouchUpInside];
        
        
        DebugLog(@"%d",i);
        i++;
        
        NSString *location;
        
        if ([[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"city"] isKindOfClass:[NSNull class]] || [[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"city"] == (id)[NSNull null]|| [[NSString stringWithFormat:@"%@",[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"city"]] isEqualToString:@""]|| [[NSString stringWithFormat:@"%@",[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"city"]] isEqualToString:@"0"] ) // || [[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"city"] length] ==0
        {
            DebugLog(@"city null");
            if ([[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"country"] isKindOfClass:[NSNull class]] || [[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"country"] == (id)[NSNull null]|| [[NSString stringWithFormat:@"%@",[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"country"]] isEqualToString:@""]|| [[NSString stringWithFormat:@"%@",[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"country"]] isEqualToString:@"0"]) // || [[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"city"] length] ==0
            {
                DebugLog(@"country null");
            }
            else
                location= [NSString stringWithFormat:@"%@",[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"country"]];
            
        }        else if (([[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"city"] isKindOfClass:[NSNull class]] || [[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"city"] == (id)[NSNull null] || [[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"city"] isEqualToString:@""]) && ([[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"country"] isKindOfClass:[NSNull class]] || [[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"country"] == (id)[NSNull null] || [[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"country"] isEqualToString:@""])) // || [[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"city"] length] ==0
        {
            DebugLog(@"city & country null");
        }
        else if (![[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"city"] isKindOfClass:[NSNull class]] || [[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"city"] != (id)[NSNull null]|| ![[NSString stringWithFormat:@"%@",[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"city"]] isEqualToString:@""]|| ![[NSString stringWithFormat:@"%@",[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"city"]] isEqualToString:@"0"] ) // || [[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"city"] length] ==0
        {
            DebugLog(@"city present");
            if ([[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"country"] isKindOfClass:[NSNull class]] || [[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"country"] == (id)[NSNull null]|| [[NSString stringWithFormat:@"%@",[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"country"]] isEqualToString:@""]|| [[NSString stringWithFormat:@"%@",[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"country"]] isEqualToString:@"0"]) // || [[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"city"] length] ==0
            {
                DebugLog(@"country null");
                location= [NSString stringWithFormat:@"%@",[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"city"]];
            }
            else{
                location= [NSString stringWithFormat:@"%@, %@",[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"city"],[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"country"]];
                DebugLog(@"country present");
            }
            
        }
        else{
            DebugLog(@"city & country has 2nd");
            location= [NSString stringWithFormat:@"%@, %@",[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"city"],[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"country"]];
        }
        
        
        UILabel *locationlb = [[UILabel alloc]initWithFrame:CGRectMake(name.frame.origin.x, name.frame.origin.y+name.frame.size.height, 210, 20)];
        [cell addSubview:locationlb];
        locationlb.backgroundColor=[UIColor clearColor];
        locationlb.textColor=[UIColor lightGrayColor];
        locationlb.text=location;
        locationlb.font=[UIFont fontWithName:@"ProximaNova-Regular" size:12];
        locationlb.textAlignment=NSTextAlignmentLeft;
        
        
        NSString *base64String= [[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"thumb"];
        if ([base64String length] >6)
        {
            NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
            UIImage *profilepic = [UIImage imageWithData:decodedData];
            profile_imgv.image=profilepic;
            profile_imgv.contentMode= UIViewContentModeScaleAspectFill;
            profile_imgv.clipsToBounds=YES;
        }
        
        
        ///////////////////////////////////////// Added on 29.06.16 ////////////////////////////////////////////
        
        typeimg = [[UIImageView alloc]init];
      //  [cell addSubview:typeimg];
        typeimg.frame= CGRectMake([UIScreen mainScreen].bounds.size.width-42, 26, 22, 22);
        typeimg.image=[UIImage imageNamed:@"circle"];
        typeimg.tag = 900+indexPath.row;
        typeimg.userInteractionEnabled = YES;
        
        UITapGestureRecognizer  *typeImgTapped   = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectFriend:)];
        [typeimg addGestureRecognizer:typeImgTapped];
        
        
        UIImageView *divider = [[UIImageView alloc]initWithFrame:CGRectMake(0, 74, [UIScreen mainScreen].bounds.size.width, 0.6f)];
        [cell addSubview:divider];
        divider.image= [UIImage imageNamed:@"divider_contact.png"];
    }
    
    
    ///////////////////////////////////////////////// Added on 30.06.16 ////////////////////////////////////////////
    
//    UIButton *contentBtn  = [[UIButton alloc] init];
//    contentBtn.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-60, cell.frame.size.height);
//    [contentBtn setBackgroundColor:[UIColor clearColor]];
//    [contentBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
//    contentBtn.tag = indexPath.row;
//    [contentBtn addTarget:self action:@selector(redirection:) forControlEvents:UIControlEventTouchUpInside];
//    [cell.contentView addSubview:contentBtn];
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    return cell;
}

-(void)redirection:(UIButton *)sender
{
        if (!filter)
        {
    
            if ([[[app_contacts objectAtIndex:sender.tag] objectForKey:@"business"] intValue] == 1 || [[[app_contacts objectAtIndex:sender.tag] objectForKey:@"friends"] intValue] == 1)
            {
                ConNewProfileViewController *mng = [[ConNewProfileViewController alloc]init];
                mng.uid= [[app_contacts objectAtIndex:sender.tag] objectForKey:@"id"];
                mng.other=@"yes";
                DebugLog(@"selected user id : %@",mng.uid);
    
    //            CATransition* transition = [CATransition animation];
    //
    //            transition.duration = 0.4;
    //            transition.type = kCATransitionPush;
    //            transition.subtype = kCATransitionFade;
    //
    //            [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
                [self.navigationController pushViewController:mng animated:YES];
            }
            else
            {
                DebugLog(@"else part e going");
                ConNewRequestsViewController *mng = [[ConNewRequestsViewController alloc]init];
                mng.imported_arr = [app_contacts objectAtIndex:sender.tag];
                mng.userid= [[[app_contacts objectAtIndex:sender.tag] objectForKey:@"id"] intValue];
                DebugLog(@"selected user id : %d",mng.userid);
                filter=YES;//searchBar.text=@"";
    
    //            CATransition* transition = [CATransition animation];
    //
    //            transition.duration = 0.4;
    //            transition.type = kCATransitionPush;
    //            transition.subtype = kCATransitionFade;
    //
    //            [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
                [self.navigationController pushViewController:mng animated:YES];
            }
    
            [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"RecommendedClicked"];
    
        }
        else
        {
            AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            del.recom_search_string= st;
            del.recom_search_array = filtered_arr;
    
            if ([[[filtered_arr objectAtIndex:sender.tag] objectForKey:@"business"] intValue] == 1 || [[[filtered_arr objectAtIndex:sender.tag] objectForKey:@"friends"] intValue] == 1)
            {
                ConNewProfileViewController *mng = [[ConNewProfileViewController alloc]init];
                mng.uid= [[filtered_arr objectAtIndex:sender.tag] objectForKey:@"id"];
                mng.other=@"yes";
                DebugLog(@"selected user id : %@",mng.uid);
    
    //            CATransition* transition = [CATransition animation];
    //
    //            transition.duration = 0.4;
    //            transition.type = kCATransitionPush;
    //            transition.subtype = kCATransitionFade;
    //
    //            [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
                [self.navigationController pushViewController:mng animated:YES];
            }
            else
            {
                ConNewRequestsViewController *mng = [[ConNewRequestsViewController alloc]init];
                mng.imported_arr = [filtered_arr objectAtIndex:sender.tag];
                mng.userid= [[[filtered_arr objectAtIndex:sender.tag] objectForKey:@"id"] intValue];
                DebugLog(@"selected user id : %d",mng.userid);
                filter=YES;//searchBar.text=@"";
    
    //            CATransition* transition = [CATransition animation];
    //
    //            transition.duration = 0.4;
    //            transition.type = kCATransitionPush;
    //            transition.subtype = kCATransitionFade;
    //            
    //            [[self navigationController].view.layer addAnimation:transition forKey:nil];
                
                [self.navigationController pushViewController:mng animated:YES];
            }
        }
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if (!filter)
        return animalIndexTitles;
    else
        return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 75;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!filter)
    {
        
        if ([[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"business"] intValue] == 1 || [[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"friends"] intValue] == 1)
        {
            ConNewProfileViewController *mng = [[ConNewProfileViewController alloc]init];
            mng.uid= [[app_contacts objectAtIndex:indexPath.row] objectForKey:@"id"];
            mng.other=@"yes";
            DebugLog(@"selected user id : %@",mng.uid);
            
//            CATransition* transition = [CATransition animation];
//            
//            transition.duration = 0.4;
//            transition.type = kCATransitionPush;
//            transition.subtype = kCATransitionFade;
//            
//            [[self navigationController].view.layer addAnimation:transition forKey:nil];
            
            [self.navigationController pushViewController:mng animated:YES];
        }
        else
        {
            DebugLog(@"else part e going");
            ConNewRequestsViewController *mng = [[ConNewRequestsViewController alloc]init];
            mng.imported_arr = [app_contacts objectAtIndex:indexPath.row];
            mng.userid= [[[app_contacts objectAtIndex:indexPath.row] objectForKey:@"id"] intValue];
            DebugLog(@"selected user id : %d",mng.userid);
            filter=YES;//searchBar.text=@"";
            
//            CATransition* transition = [CATransition animation];
//            
//            transition.duration = 0.4;
//            transition.type = kCATransitionPush;
//            transition.subtype = kCATransitionFade;
//            
//            [[self navigationController].view.layer addAnimation:transition forKey:nil];
            
            [self.navigationController pushViewController:mng animated:YES];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"RecommendedClicked"];
        
    }
    else
    {
        AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        del.recom_search_string= st;
        del.recom_search_array = filtered_arr;
        
        if ([[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"business"] intValue] == 1 || [[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"friends"] intValue] == 1)
        {
            ConNewProfileViewController *mng = [[ConNewProfileViewController alloc]init];
            mng.uid= [[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"id"];
            mng.other=@"yes";
            DebugLog(@"selected user id : %@",mng.uid);
            
//            CATransition* transition = [CATransition animation];
//            
//            transition.duration = 0.4;
//            transition.type = kCATransitionPush;
//            transition.subtype = kCATransitionFade;
//            
//            [[self navigationController].view.layer addAnimation:transition forKey:nil];
            
            [self.navigationController pushViewController:mng animated:YES];
        }
        else
        {
            ConNewRequestsViewController *mng = [[ConNewRequestsViewController alloc]init];
            mng.imported_arr = [filtered_arr objectAtIndex:indexPath.row];
            mng.userid= [[[filtered_arr objectAtIndex:indexPath.row] objectForKey:@"id"] intValue];
            DebugLog(@"selected user id : %d",mng.userid);
            filter=YES;//searchBar.text=@"";
            
//            CATransition* transition = [CATransition animation];
//            
//            transition.duration = 0.4;
//            transition.type = kCATransitionPush;
//            transition.subtype = kCATransitionFade;
//            
//            [[self navigationController].view.layer addAnimation:transition forKey:nil];
            
            [self.navigationController pushViewController:mng animated:YES];
        }
    }
}


-(void)callPulseLoaderView:(CGRect)frame view:(UIView *)parentView {
    
    pulseLoaderView = [[UIView alloc] init];
    pulseLoaderView.frame = frame;
    pulseLoaderView.backgroundColor = [UIColor clearColor];
    pulseLoaderView.userInteractionEnabled = NO;
    [parentView addSubview:pulseLoaderView];
}

-(void)insertSpinnerOfStylePulseForLoadMore:(RTSpinKitViewStyle)style
                            backgroundColor:(UIColor*)backgroundColor
                                      label:(NSString *)labelString
{
    DebugLog(@"entering there");
    CGRect screenBounds = [pulseLoaderView bounds];
    CGFloat screenWidth = CGRectGetWidth(screenBounds);
    
    PulseSpinner_forLoadMore = [[RTSpinKitView alloc] initWithStyle:style color:[UIColor blackColor]];
    
    
    PulseSpinner_forLoadMore.center = CGPointMake(CGRectGetMidX(screenBounds), CGRectGetMidY(screenBounds));
    [PulseSpinner_forLoadMore startAnimating];
    
    UIView *panel = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenBounds.size.width, screenBounds.size.height)];
    panel.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@""]]; //Profilebg
    [panel addSubview:PulseSpinner_forLoadMore];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 50.0, screenWidth, 30.0)];
    //    label.text = [NSString stringWithFormat:@"Loading, Please Wait..."];
    label.font = [UIFont fontWithName:@"ProximaNova-Regular" size:20.0];
    label.numberOfLines = 3;
    label.textColor = [UIColor whiteColor];
    
    if ([label respondsToSelector:@selector(tintColor)]) {
        label.textAlignment = NSTextAlignmentCenter;
    } else {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
        label.textAlignment = UITextAlignmentCenter;
#pragma GCC diagnostic pop
        label.backgroundColor = [UIColor clearColor];
    }
    
    [panel addSubview:label];
    
    [pulseLoaderView addSubview:panel];
    
}

-(void)removePulseLoader {
    
    
    pulseLoaderView = nil;
    pulseLoaderView.hidden = YES;
    [pulseLoaderView removeFromSuperview];
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
    CGPoint offset = scrollView.contentOffset;
    CGRect bounds = scrollView.bounds;
    CGSize size = scrollView.contentSize;
    UIEdgeInsets inset = scrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_distance = -10.0f;
    
    if(y > h + reload_distance)
    {
        TableFooter.hidden = NO;
        [self loadMoreSearchResults];
    }
}

-(void)loadMoreSearchResults {
    
    
    if (urlindexnow == 10){
        TableFooter.hidden = NO;
        start+=10;
        urlindexnow = 0;
        [self FetchURLRequest:start];
    }
    else
    {
        [self removePulseLoader];
        TableFooter.hidden = YES;
    }
}


- (void)addOrRemoveSelectedIndexPath:(NSIndexPath *)indexPath
{
    if (!self.selectedIndexPaths) {
        self.selectedIndexPaths = [NSMutableArray new];
    }
    BOOL containsIndexPath = [self.selectedIndexPaths containsObject:indexPath];
    
    if (containsIndexPath) {
        [self.selectedIndexPaths removeObject:indexPath];
        
        [tab_contact reloadRowsAtIndexPaths:@[indexPath]
                           withRowAnimation:UITableViewRowAnimationNone];
        
        [sendarray removeObject:[[app_contacts objectAtIndex:indexPath.row]objectForKey:@"id"]];
        
    }else{
        [self.selectedIndexPaths addObject:indexPath];
        
        [tab_contact reloadRowsAtIndexPaths:@[indexPath]
                           withRowAnimation:UITableViewRowAnimationNone];
        
        
        [sendarray addObject:[[app_contacts objectAtIndex:indexPath.row]objectForKey:@"id"]];
        
    }
    
    if ([sendarray count] == 0)
    {
        [invite_send setBackgroundImage:[UIImage imageNamed:@"add-allrec.png"] forState:UIControlStateNormal];
        invite_send.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-95, 10, 87.5f, 30);
        invite_send.backgroundColor = [UIColor clearColor];
        [invite_send setTitle:@"" forState:UIControlStateNormal];
    }
    else
    {
        [invite_send setBackgroundImage:nil forState:UIControlStateNormal];
        invite_send.backgroundColor = [UIColor colorWithRed:(153/255.0f) green:(189/255.0f) blue:(128/255.0f) alpha:1.0f];
        [invite_send setTitle:@"Send" forState:UIControlStateNormal];
        invite_send.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:18];
        invite_send.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-95, 10, 87.5f, 30);
    }
    
}


+(void)chngpostion
{
    DebugLog(@"Change Position profile page");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Data" object:Nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getData:) name:@"Data" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(navtopage:) name:@"pagenav" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestreceivedaction) name:@"Requestreceived_push" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(acceptedrequestaction) name:@"Accepted_push" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AcceptedRequest) name:@"Accepted_request" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ConnectionType) name:@"NewConnectionType" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AcceptedRequest) name:@"TypeChange" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AddFriend) name:@"NewConnection" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shareLocation) name:@"ShareLocation" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdateInformation) name:@"UpdateInfo" object:nil];
    
}

-(void)UpdateInformation
{
    ConPersonalProfileViewController *PVC = [[ConPersonalProfileViewController alloc]init];
    PVC.toUpdateInfo = @"YES";
    [self.navigationController pushViewController:PVC animated:YES];
  //  [[NSNotificationCenter defaultCenter] postNotificationName:@"Update Info" object:Nil];

}

-(void)shareLocation
{
    ConLocateGroupViewController *conLocate = [[ConLocateGroupViewController alloc]init];
    conLocate.group_id=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"sharedLocGrpId"]];
    [self.navigationController pushViewController:conLocate animated:YES];
}

-(void)ConnectionType
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDel.newrequestRedirect)
    {
     ConNewRequestsViewController *mng = [[ConNewRequestsViewController alloc]init];
       mng.userid = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userforrequest"] intValue];
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//        
//        appDel.newrequestRedirect = NO;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:mng animated:YES];
        
    }
}


-(void)AddFriend
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
//    alert = [[UIAlertView alloc] initWithTitle:@"" message:@"IN RECOMMENDED" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//     [alert show];
    
//    if (appDel.newUser)
//    {
    
    ConAddFriendViewController *con = [[ConAddFriendViewController alloc]init];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//        appDel.newUser = NO;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    [[self navigationController] pushViewController:con animated:YES];
        
  //  }
    
}

-(void)AcceptedRequest
{
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (del.profileRedirect)
    {
    
    ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
    // con.other=@"yes";
    con.request = YES;
    [self.tabBarController.tabBar setHidden:YES];
    // con.uid=[NSString stringWithFormat:@"%d",[userid intValue]];
    //                [self.navigationController presentViewController:con animated:NO completion:nil];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    del.profileRedirect = NO;
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
        
    }
    
}

-(void)requestreceivedaction
{
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
    con.other=@"yes";
    con.uid=[NSString stringWithFormat:@"%@",del.pushtoprofileid];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}

-(void)acceptedrequestaction
{
    ConRequestViewController *con = [[ConRequestViewController alloc]init];
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
}


-(void)navtopage: (NSNotification *)notification
{
    DebugLog(@"navtopage");
    NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
    if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"4"])
    {
        ConAccountSettingsViewController *con = [[ConAccountSettingsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"6"])
    {
        ConInviteViewController *con = [[ConInviteViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"5"])
    {
        ConSyncLoaderViewController *con = [[ConSyncLoaderViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        
        [self.navigationController pushViewController:con animated:YES];
        
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"2"])
    {
        ConNewProfileViewController *con = [[ConNewProfileViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([[prefs objectForKey:@"whichpage"] isEqualToString:@"3"])
    {
        ConQROptionsViewController *con = [[ConQROptionsViewController alloc]init];
        
//        CATransition* transition = [CATransition animation];
//        
//        transition.duration = 0.4;
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFade;
//        
//        [[self navigationController].view.layer addAnimation:transition forKey:nil];
        
        [self.navigationController pushViewController:con animated:YES];
    }
    
    [mainview setFrame:CGRectMake(0, mainview.frame.origin.y, mainview.frame.size.width, mainview.frame.size.height)];
}


-(void)getData:(NSNotification *)notification {
    
    if(move == 0) {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-40, (mainview.frame.origin.y), mainview.frame.size.width, mainview.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             move=1;
                         }];
        
    } else {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainview setFrame:CGRectMake(0, (mainview.frame.origin.y), mainview.frame.size.width, mainview.frame.size.height)];
                             
                         }
                         completion:^(BOOL finished){
                             move=0;
                         }];
    }
}

-(void)viewDidDisappear:(BOOL)animated
{
    move=0;
    [names cancelAllOperations];
    [tab_contact removeFromSuperview];
    tab_contact = Nil;
    [mainview removeFromSuperview];
    [super viewDidDisappear:YES];
    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"searchfriends"];
}



- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBarlocal
{
    searchBarlocal.showsCancelButton=YES;
    
    invitecountview.hidden = YES;
    invitecountview.frame=CGRectZero;
    line_view.hidden=YES;
    tab_contact.frame = CGRectMake(0, searchBar.frame.origin.y+searchBar.frame.size.height, [UIScreen mainScreen].bounds.size.width, mainview.frame.size.height-50);
    
    tab_contact.scrollEnabled=YES;
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    DebugLog(@"begin editing: %@",del.recom_search_string);
    if (![del.recom_search_string isEqualToString:@"null"])
    {
        filter =YES;
    }
    
    return YES;
}


-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBarlocal
{
    [searchBarlocal resignFirstResponder];
    searchBarlocal.text=@"";
    searchBarlocal.showsCancelButton=NO;
    filter=NO;
    [tab_contact reloadData];
    tab_contact.hidden=NO;
    invitecountview.frame=CGRectMake(0,49, [UIScreen mainScreen].bounds.size.width, 50);
    invitecountview.hidden = NO;
    line_view.hidden=NO;
    tab_contact.frame = CGRectMake(0, 50, [UIScreen mainScreen].bounds.size.width, mainview.frame.size.height-50);
    tab_contact.scrollEnabled=YES;
}


- (BOOL)searchBarDidBeginEditing:(UISearchBar *)searchBarlocal
{
    searchBarlocal.showsCancelButton=YES;
    [searchBarlocal becomeFirstResponder];
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (![del.recom_search_string isEqualToString:@"null"])
    {
        filter =YES;
    }
    
    return YES;
}

- (BOOL)searchBarDidEndEditing:(UISearchBar *)searchBarlocal
{
    searchBarlocal.showsCancelButton=NO;
    filter=NO;
    [searchBarlocal resignFirstResponder];
    searchBarlocal.text=@"";
    return YES;
}

- (BOOL)searchBar:(UISearchBar *)searchBarlocal shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (![del.recom_search_string isEqualToString:@"null"])
    {
        filter =YES;
    }
    
    return YES;
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBarlocal
{
    searchBarlocal.showsCancelButton = NO;
    [searchBarlocal resignFirstResponder];
    if([st length] >= 3)
        filter = YES;
    else
        filter=NO;
    tab_contact.scrollEnabled=YES;
}


- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (![del.recom_search_string isEqualToString:@"null"])
    {
        filter =YES;
    }
    else
        filter = NO;
}

- (void)searchBar:(UISearchBar *)searchBarl textDidChange:(NSString *)searchText {
    DebugLog(@"Text change - %@",searchText);
    filter =YES;
    whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    st = [searchText stringByTrimmingCharactersInSet:whitespace];
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (![del.recom_search_string isEqualToString:@"null"])
    {
        filter =YES;
    }
    
    DebugLog(@"length of text: %lu",(unsigned long)st.length);
    if ([st length] == 0)
    {
        [searchBarl performSelector:@selector(resignFirstResponder) withObject:nil afterDelay:0];
    }
    
    if([st length] >= 3) {
        
        Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
        if (networkStatus == NotReachable) {
            
            UIAlertView *alertMe = [[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please check your internet connectivity" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertMe show];
            
        } else {
            
            DebugLog(@"There IS internet connection");
            filter = YES;
            [act startAnimating];
            names = [NSOperationQueue new];
            NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                                initWithTarget:self
                                                selector:@selector(loaddata)
                                                object:nil];
            [names addOperation:operation];
        }
        
    }
    else {
        filter = NO;
        [filtered_arr removeAllObjects];
        nouserlb.hidden=YES;
        tab_contact.hidden=NO;
        [tab_contact reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    }
}

-(void)loaddata
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=SearchWithoutConnections&q=%@&access_token=%@&device_id=%@&thumb=true",[st stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]];
    
    DebugLog(@"profile url: %@",urlString1);
    
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    
    NSError *error=nil;
    
    json1 = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
             
                                            options:kNilOptions
                                              error:&error];
    DebugLog(@"json returns: %@",json1);
    [self performSelectorOnMainThread:@selector(datadisplay) withObject:nil waitUntilDone:NO];
    
    
}

-(void)datadisplay
{
    NSString *errornumber= [NSString stringWithFormat:@"%@",[json1 objectForKey:@"errorno"]];
    DebugLog(@"err  %@",errornumber);
    
    if (![errornumber isEqualToString:@"0"])
    {
        DebugLog(@"if if");
        NSString *err_str = [json1 objectForKey:@"error"];
        alert = [[UIAlertView alloc] initWithTitle:@"Error in Retrieving Data!"
                                           message:err_str
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
    }
    else
    {
        filtered_arr = [[NSMutableArray alloc]init];
        if ([[json1 objectForKey:@"details"] isKindOfClass:[NSNull class]] || [json1 objectForKey:@"details"] == (id)[NSNull null] ||[[json1 objectForKey:@"details"] count] ==0)
        {
            DebugLog(@"Filtered Contacts = 0");
        }
        else
        {
            NSDictionary *contacts_temp_dict = [json1 objectForKey:@"details"];
            for (NSDictionary *dict in [contacts_temp_dict objectForKey:@"search"])
            {
                [filtered_arr addObject:dict];
            }
            NSMutableArray *hld = [[NSMutableArray alloc] init];
            for (int ii=0; ii<filtered_arr.count; ii++) {
                if ([[[filtered_arr objectAtIndex:ii] objectForKey:@"type"] intValue]!=4 && [[[filtered_arr objectAtIndex:ii] objectForKey:@"isgroup"] intValue]!=1)
                {
                    [hld addObject:[filtered_arr objectAtIndex:ii]];
                }
            }
            
            
            [filtered_arr removeAllObjects];
            filtered_arr=[hld mutableCopy];
            
            DebugLog(@"filtered array : %@ && count:: %ld",filtered_arr, (unsigned long)[filtered_arr count]);
            NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)];
            filtered_arr=[[filtered_arr sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]] mutableCopy];
            DebugLog(@"filtered array after sorting : %@",filtered_arr);
            DebugLog(@"Array count: %lu",(unsigned long)[app_contacts count]);
            DebugLog(@"Array after filter: %lu",(unsigned long)[filtered_arr count]);
            DebugLog(filter? @"yes" : @"no");
        }
    }
    
    if ([filtered_arr count] ==0)
    {
        [act stopAnimating];
        [filtered_arr removeAllObjects];
        tab_contact.hidden=YES;
        nouserlb.hidden=NO;
    }
    else
    {
        nouserlb.hidden=YES;
        tab_contact.hidden=NO;
        DebugLog(@"Array count: %lu",(unsigned long)[app_contacts count]);
        DebugLog(@"Array after filter: %lu",(unsigned long)[filtered_arr count]);
    }
    [tab_contact reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    
}


-(void)add_friend: (UIGestureRecognizer *)sender
{
    int userid = (int)sender.view.tag;
    act = [[UIActivityIndicatorView alloc] init];
    act.center=self.view.center;
    [act startAnimating];
    act.hidden=NO;
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [act setColor:[UIColor blackColor]];
    [mainview addSubview:act];
    

    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=addrequest&id=%d&access_token=%@&device_id=%@",userid,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
    
    DebugLog(@"deny url: %@",urlString1);
    NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
    
    NSError *error=nil;
    NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                              options:kNilOptions
                                                                error:&error];
    DebugLog(@"deny json returns: %@",json_deny);
    
    if ([[json_deny objectForKey:@"success"]intValue] == 1)
    {
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"searchfriends"] isEqualToString:@"yes"]) {
            ConInviteViewController *con = [[ConInviteViewController alloc]init];
            con.searchfriends= @"yes";
            
//            CATransition* transition = [CATransition animation];
//            
//            transition.duration = 0.4;
//            transition.type = kCATransitionPush;
//            transition.subtype = kCATransitionFade;
//            
//            [[self navigationController].view.layer addAnimation:transition forKey:nil];
            
            [self.navigationController pushViewController:con animated:YES];
            [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"searchafterreg"];
        }
        else
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Successfully sent Requests!"
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
            [act stopAnimating];
        }
    }
    else
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Sorry!, Failed to Process Request!"
                                           message:[json_deny objectForKey:@"error"]
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        [act stopAnimating];
    }
}
-(void)nextBtn:(UIButton *)sender{
    
    DebugLog(@"hhihwihih");
    ConInviteViewController *con = [[ConInviteViewController alloc]init];
    con.searchfriends= @"yes";
    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.4;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:con animated:YES];
    [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"searchafterreg"];
    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"searchfriends"];
}

-(void)invite_all: (id)sender
{
    if ([sendarray count] == 0)
    {
        NSString *addallstr = [addallarray componentsJoinedByString:@","];
        //    int userid = 5;
        act = [[UIActivityIndicatorView alloc] init];
        act.center=self.view.center;
        [act startAnimating];
        act.hidden=NO;
        act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        [act setColor:[UIColor blackColor]];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=addrequest&id=%@&access_token=%@&device_id=%@",addallstr,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
        
        DebugLog(@"deny url: %@",urlString1);
        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
        
        NSError *error=nil;
        NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                  options:kNilOptions
                                                                    error:&error];
        DebugLog(@"deny json returns: %@",json_deny);
        
        if ([[json_deny objectForKey:@"success"]intValue] == 1)
        {
            if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"searchfriends"] isEqualToString:@"yes"]) {
                ConInviteViewController *con = [[ConInviteViewController alloc]init];
                con.searchfriends= @"yes";
                
//                CATransition* transition = [CATransition animation];
//                
//                transition.duration = 0.4;
//                transition.type = kCATransitionPush;
//                transition.subtype = kCATransitionFade;
//                
//                [[self navigationController].view.layer addAnimation:transition forKey:nil];
                
                [self.navigationController pushViewController:con animated:YES];
                [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"searchafterreg"];
            }
            else
            {
                [SVProgressHUD showSuccessWithStatus:@"Successfully sent Requests!"];
                [act stopAnimating];
            }
            
            
        }
        else
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Sorry!, Failed to Process Request!"
                                               message:[json_deny objectForKey:@"error"]
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
            [act stopAnimating];
        }
    }
    
    else
    {
        NSString *addallstr = [sendarray componentsJoinedByString:@","];
        DebugLog(@"vvvvv %@",addallstr);
        //    int userid = 5;
        act = [[UIActivityIndicatorView alloc] init];
        act.center=self.view.center;
        [act startAnimating];
        act.hidden=NO;
        act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        [act setColor:[UIColor blackColor]];
        //    [mainview addSubview:act];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=addrequest&id=%@&access_token=%@&device_id=%@",addallstr,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
        
        DebugLog(@"deny url: %@",urlString1);
        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
        
        NSError *error=nil;
        NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                  options:kNilOptions
                                                                    error:&error];
        DebugLog(@"deny json returns: %@",json_deny);
        
        if ([[json_deny objectForKey:@"success"]intValue] == 1)
        {
            
            if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"searchfriends"] isEqualToString:@"yes"]) {
                ConInviteViewController *con = [[ConInviteViewController alloc]init];
                con.searchfriends= @"yes";
                
//                CATransition* transition = [CATransition animation];
//                
//                transition.duration = 0.4;
//                transition.type = kCATransitionPush;
//                transition.subtype = kCATransitionFade;
//                
//                [[self navigationController].view.layer addAnimation:transition forKey:nil];
                
                [self.navigationController pushViewController:con animated:YES];
                [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"searchafterreg"];
            }
            else
            {
                [SVProgressHUD showSuccessWithStatus:@"Successfully sent Requests!"];
                [act stopAnimating];
            }
        }
        else
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Sorry, Failed to Process Request!"
                                               message:[json_deny objectForKey:@"error"]
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
            [act stopAnimating];
        }
    }
    
    self.selectedIndexPaths = [NSMutableArray new];
    sendarray = [NSMutableArray new];
    [tab_contact reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
}

- (void)setImage:(UIImage *)iconImage forSearchBarIcon:(UISearchBarIcon)icon state:(UIControlState)state
{
}

- (void)setSearchIconToFavicon
{
    // Really a UISearchBarTextField, but the header is private.
    UITextField *searchField = nil;
    for (UIView *subview in searchBar.subviews) {
        if ([subview isKindOfClass:[UITextField class]]) {
            searchField = (UITextField *)subview;
            break;
        }
    }
    if (searchField) {
        UIImage *image = [UIImage imageNamed: @"icon3.png"];
        UIImageView *iView = [[UIImageView alloc] initWithImage:image];
        searchField.leftView = iView;
    }
}

-(void)goback
{
    backTapped = true;
   // [timer invalidate];
   // [self sendingRequest];
    
    int index;
    NSArray* navarr = [[NSArray alloc] initWithArray:self.navigationController.viewControllers];
    for(int p=0 ; p<[navarr count] ; p++)
    {
        if(![[navarr objectAtIndex:p] isKindOfClass:NSClassFromString(@"ConAddFriendViewController")])
        {
            index = p;
        }
    }
    
//    CATransition *transition = [CATransition animation];
//    
//    transition.duration = 0.4f;
//    
//    transition.type = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:nil];
    [self.navigationController popToViewController:[navarr objectAtIndex:index] animated:YES];
    
    
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [searchBar resignFirstResponder];
}


-(void)addfriendwithbt:(UIButton *)sender
{
    
    DebugLog(@"Tag & Value:%ld %@",(long)sender.tag, [[app_contacts objectAtIndex:sender.tag] objectForKey:@"id"]);
    
    indexReload = (int)sender.tag;
    NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    

    
    [UIView animateWithDuration:0
                     animations:^{
                         
                         tab_contact.userInteractionEnabled = NO;
                          [tab_contact reloadRowsAtIndexPaths:@[indexPath1] withRowAnimation:UITableViewRowAnimationNone];
                         
//                         if ([selected_array count]>0)
//                         {
//                             int count = 0;
//                             for (int indexVal = 0; indexVal<[selected_array count]; indexVal++)
//                             {
//                                 if ([[selected_array objectAtIndex:indexVal] intValue] == [[[app_contacts objectAtIndex:sender.tag] objectForKey:@"id"] intValue])
//                                 {
//                                     count++;
//                                     [selected_array removeObjectAtIndex:(int)indexVal];
//                                     
//                                     [deletedIndex removeObject:[NSString stringWithFormat:@"%d",(int)sender.tag]];
//                                     break;
//                                 }
//                             }
//                             if (count==0)
//                             {
//                                 [deletedIndex addObject:[NSString stringWithFormat:@"%d",(int)sender.tag]];
//                                 [selected_array addObject:[[app_contacts objectAtIndex:sender.tag] objectForKey:@"id"]];
//                             }
//                             
//                         }
//                         else
//                         {
//                             [deletedIndex addObject:[NSString stringWithFormat:@"%d",(int)sender.tag]];
//                             [selected_array addObject:[[app_contacts objectAtIndex:sender.tag] objectForKey:@"id"]];
//                             
//                             
//                         }
//                         
//                         DebugLog(@"Selected Array:%@",selected_array);
//                         
//                         // [tab_contact setUserInteractionEnabled:NO];
////                         [tab_contact reloadRowsAtIndexPaths:@[indexPath1] withRowAnimation:UITableViewRowAnimationNone];
//                         
//                         
//                         [tab_contact beginUpdates];
//                         
//                         NSArray *indexPathsToRemove = [NSArray arrayWithObjects:
//                                                        [NSIndexPath indexPathForRow:sender.tag inSection:0],nil];
//                         [app_contacts removeObjectAtIndex:indexReload];
//                         
//                         
//                         [tab_contact deleteRowsAtIndexPaths:indexPathsToRemove withRowAnimation:UITableViewRowAnimationFade];
//                         
//                         [tab_contact endUpdates];
//                         
//                         [self performSelector:@selector(reloadTheTable) withObject:nil afterDelay:0.2f];
                         
                     }
                     completion:^(BOOL finished){
                         
                         double delayInSeconds = 0.2;
                         dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                         dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                             
                             if ([appDel.selected_array count]>0)
                             {
                                 int count = 0;
                                 for (int indexVal = 0; indexVal<[appDel.selected_array count]; indexVal++)
                                 {
                                     if ([[appDel.selected_array objectAtIndex:indexVal] intValue] == [[[app_contacts objectAtIndex:sender.tag] objectForKey:@"id"] intValue])
                                     {
                                         count++;
                                         [appDel.selected_array removeObjectAtIndex:(int)indexVal];
                                         
                                         [appDel.deletedIndex removeObject:[NSString stringWithFormat:@"%d",(int)sender.tag]];
                                         break;
                                     }
                                 }
                                 if (count==0)
                                 {
                                     [appDel.deletedIndex addObject:[NSString stringWithFormat:@"%d",(int)sender.tag]];
                                     [appDel.selected_array addObject:[[app_contacts objectAtIndex:sender.tag] objectForKey:@"id"]];
                                 }
                                 
                             }
                             else
                             {
                                 [appDel.deletedIndex addObject:[NSString stringWithFormat:@"%d",(int)sender.tag]];
                                 [appDel.selected_array addObject:[[app_contacts objectAtIndex:sender.tag] objectForKey:@"id"]];
                                 
                                 
                             }
                             
                             DebugLog(@"Selected Array:%@",appDel.selected_array);
                             
                             // [tab_contact setUserInteractionEnabled:NO];
                             //                         [tab_contact reloadRowsAtIndexPaths:@[indexPath1] withRowAnimation:UITableViewRowAnimationNone];
                             
                             
                             [tab_contact beginUpdates];
                             
                             NSArray *indexPathsToRemove = [NSArray arrayWithObjects:
                                                            [NSIndexPath indexPathForRow:sender.tag inSection:0],nil];
//                            [app_contacts removeObjectAtIndex:indexReload];
                              [app_contacts removeObjectAtIndex:sender.tag];
                             
                             
                             [tab_contact deleteRowsAtIndexPaths:indexPathsToRemove withRowAnimation:UITableViewRowAnimationFade];
                             
                             [tab_contact endUpdates];
                             
                             indexReload = -1;
                             [tab_contact reloadData];
                             tab_contact.userInteractionEnabled = YES;
                             
                           //  [self performSelector:@selector(reloadTheTable) withObject:nil afterDelay:0.2f];
                             

                            
                         });
                         
                         
                         
                         
                         
                         
                         
                         
                         
//                         NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:30.0f target:self
//                                                                         selector:@selector(sendingRequest)
//                                                                         userInfo:nil
//                                                                          repeats:NO];
                         
//                         DebugLog(@"=========================================================");
//                         
//                         int userid = [[[app_contacts objectAtIndex:sender.tag] objectForKey:@"id"] intValue];
//                         DebugLog(@"user id to be requested: %d",userid);
//                         
//                         NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
//                         NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=addrequest&id=%d&access_token=%@&device_id=%@",userid,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
//                         
//                         DebugLog(@"deny url: %@",urlString1);
//                         NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
//                         
//                         NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
//                         
//                         NSError *error=nil;
//                         NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
//                                                                                   options:kNilOptions
//                                                                                     error:&error];
//                         DebugLog(@"deny json returns: %@",json_deny);
//                         
//                         if ([[json_deny objectForKey:@"success"]intValue] == 1)
//                         {
//                             if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"searchfriends"] isEqualToString:@"yes"]) {
//                                 ConInviteViewController *con = [[ConInviteViewController alloc]init];
//                                 con.searchfriends= @"yes";
//                                 
////                                 CATransition* transition = [CATransition animation];
////                                 
////                                 transition.duration = 0.4;
////                                 transition.type = kCATransitionPush;
////                                 transition.subtype = kCATransitionFade;
////                                 
////                                 [[self navigationController].view.layer addAnimation:transition forKey:nil];
//                                 
//                                 [self.navigationController pushViewController:con animated:YES];
//                                 [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"searchafterreg"];
//                                 
//                                 //[SVProgressHUD dismiss];
//                             }
//                             else
//                             {
//                                 
//                                 [self performSelector:@selector(deletetherow) withObject:nil afterDelay:1];
//                                 
//                                 [act stopAnimating];
//                             }
//                         }
//                         else
//                         {
//                             alert = [[UIAlertView alloc] initWithTitle:@"Sorry!!, Failed to Process Request!"
//                                                                message:[json_deny objectForKey:@"error"]
//                                                               delegate:self
//                                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
//                             [alert show];
//                             [act stopAnimating];
//                             
//                         }
                         
                     }];
    
 }

-(void)sendingRequest
{
    if ([selected_array count]>0)
    {
        DebugLog(@"=========================================================");
        
        NSString *selectedIDs = [selected_array componentsJoinedByString:@","];
        
        //   int userid = [[[app_contacts objectAtIndex:sender.tag] objectForKey:@"id"] intValue];
        DebugLog(@"user id to be requested: %@",selectedIDs);
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *urlString1 =[NSString stringWithFormat:@"https://budnav.com/ext/?action=addrequest&id=%@&access_token=%@&device_id=%@",selectedIDs,[prefs objectForKey:@"access_token"],[prefs objectForKey:@"deviceToken"]]; //&thumb=true
        
        DebugLog(@"deny url: %@",urlString1);
        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
        
        NSError *error=nil;
        NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                  options:kNilOptions
                                                                    error:&error];
        DebugLog(@"deny json returns: %@",json_deny);
        
        if ([[json_deny objectForKey:@"success"]intValue] == 1)
        {
            if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"searchfriends"] isEqualToString:@"yes"]) {
                ConInviteViewController *con = [[ConInviteViewController alloc]init];
                con.searchfriends= @"yes";
                
                //                                 CATransition* transition = [CATransition animation];
                //
                //                                 transition.duration = 0.4;
                //                                 transition.type = kCATransitionPush;
                //                                 transition.subtype = kCATransitionFade;
                //
                //                                 [[self navigationController].view.layer addAnimation:transition forKey:nil];
                
                selected_array = [[NSMutableArray alloc] init];
                deletedIndex = [[NSMutableArray alloc] init];
                selectedIDs = @"";
                
                [self.navigationController pushViewController:con animated:YES];
                [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"searchafterreg"];
                
                //[SVProgressHUD dismiss];
            }
            else
            {
                [self performSelector:@selector(deletetherow) withObject:nil afterDelay:1];
                
                [act stopAnimating];
            }
        }
        else
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Sorry!, Failed to Process Request!"
                                               message:[json_deny objectForKey:@"error"]
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
            [act stopAnimating];
            
        }

    }
    
    
}


-(void)deletetherow
{
//    [tab_contact beginUpdates];
//
//    NSMutableArray *indexPathsToRemove = [[NSMutableArray alloc] init];
//    for (int indexVal =0; indexVal<[selected_array count]; indexVal++)
//    {
//        
//        for (int indVal = 0; indVal<[app_contacts count]; indVal++)
//        {
//            if ([[[app_contacts objectAtIndex:indVal] objectForKey:@"id"] intValue] == [[selected_array objectAtIndex:indexVal] intValue])
//            {
//                [app_contacts removeObjectAtIndex:indVal];
//            }
//        }
//    }
//    
//    
//    DebugLog(@"Deleted Index:%@",deletedIndex);
//    
//    DebugLog(@"App Contacts:%@",indexPathsToRemove);
//    
//     for (int indexVal = 0; indexVal<[deletedIndex count]; indexVal++)
//     {
//         [indexPathsToRemove addObject:[NSIndexPath indexPathForRow:[[deletedIndex objectAtIndex:indexVal] intValue] inSection:0]];
//     }
//    
//    selected_array = [[NSMutableArray alloc] init];
//    deletedIndex = [[NSMutableArray alloc] init];
// //  NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:indexReload inSection:0];
//    
//    
//  //  [app_contacts removeObjectAtIndex:indexReload];
//    
//    
//    [tab_contact deleteRowsAtIndexPaths:indexPathsToRemove withRowAnimation:UITableViewRowAnimationFade];
//    
//    [tab_contact endUpdates];
    
//    [self performSelector:@selector(reloadTheTable) withObject:nil afterDelay:0.2f];
    
    if (!backTapped)
    {
        timer = [NSTimer scheduledTimerWithTimeInterval:30.0f target:self
                                               selector:@selector(sendingRequest)
                                               userInfo:nil
                                                repeats:NO];
    }
    
   
    [tab_contact setUserInteractionEnabled:YES];
}

-(void)reloadTheTable
{
    indexReload = -1;
    [tab_contact reloadData];
    tab_contact.userInteractionEnabled = YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 50.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    TableFooter = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40)];
    [TableFooter setBackgroundColor:[UIColor clearColor]];
    
    actfooter = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    actfooter.frame = CGRectMake(TableFooter.frame.size.width/2 - 20, 0, 40, 40);
    actfooter.color = [UIColor grayColor];
    [TableFooter addSubview:actfooter];
    [actfooter startAnimating];
    
    
    TableFooter.hidden = YES;
    tableView.tableFooterView = TableFooter;
    
    return TableFooter;
}

-(void)noFriend
{
    DebugLog(@"NO FRIEND");
    [act removeFromSuperview];
    searching.text = @"";
    
//    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"" message:@"There are no recommended friends" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//    [alert1 show];

}

-(void)serverError
{
    [act removeFromSuperview];
    searching.text = @"";
    
    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"" message:@"Error in server connection!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert1 show];
    
}



-(void)changecoloragain
{
    backLabel.alpha = 1.0f;
    bck_img.alpha = 1.0f;
}

-(void)changecolor
{
    backLabel.alpha = 0.5f;
    bck_img.alpha = 0.5f;
    
}


-(void)selectFriend:(UIGestureRecognizer *)recognizer
{
    DebugLog(@"Unsorted Array :%@",app_contacts);
    DebugLog(@"Sorted Array :%@",filtered_arr);
    
    if ([selected_array count]>0)
    {
        int count = 0;
        for (int indexVal = 0; indexVal<[selected_array count]; indexVal++)
        {
            if ([[selected_array objectAtIndex:indexVal] intValue] == [[[filtered_arr objectAtIndex:recognizer.view.tag-900] objectForKey:@"id"] intValue])
            {
                NSIndexPath *index=[NSIndexPath indexPathForRow:recognizer.view.tag-900 inSection:0];
                UITableViewCell *cell = [tab_contact cellForRowAtIndexPath:index];
                
               UIImageView *circleImg = (UIImageView *) [cell viewWithTag:recognizer.view.tag];
                circleImg.image = [UIImage imageNamed:@"circle"];
                
               // [tab_contact reloadRowsAtIndexPaths:@[index] withRowAnimation:YES];
                count++;
                [selected_array removeObjectAtIndex:(int)indexVal];
                
                break;
            }
//            else
//            {
//                NSIndexPath *index=[NSIndexPath indexPathForRow:recognizer.view.tag-900 inSection:0];
//                UITableViewCell *cell = [tab_contact cellForRowAtIndexPath:index];
//                
//                UIImageView *circleImg = (UIImageView *) [cell viewWithTag:recognizer.view.tag];
//                circleImg.image = [UIImage imageNamed:@"blue_circle"];
//                
//                //[tab_contact reloadRowsAtIndexPaths:@[index] withRowAnimation:YES];
//                
//                [selected_array addObject:[[filtered_arr objectAtIndex:recognizer.view.tag-900] objectForKey:@"id"]];
//                
//                break;
//
//            }
        }
        if (count==0)
        {
            NSIndexPath *index=[NSIndexPath indexPathForRow:recognizer.view.tag-900 inSection:0];
            UITableViewCell *cell = [tab_contact cellForRowAtIndexPath:index];
            
            UIImageView *circleImg = (UIImageView *) [cell viewWithTag:recognizer.view.tag];
            circleImg.image = [UIImage imageNamed:@"blue_circle"];
            
            //[tab_contact reloadRowsAtIndexPaths:@[index] withRowAnimation:YES];
            
            [selected_array addObject:[[filtered_arr objectAtIndex:recognizer.view.tag-900] objectForKey:@"id"]];
        }
        
    }
    else
    {
        NSIndexPath *index=[NSIndexPath indexPathForRow:recognizer.view.tag-900 inSection:0];
        UITableViewCell *cell = [tab_contact cellForRowAtIndexPath:index];
        
        UIImageView *circleImg = (UIImageView *) [cell viewWithTag:recognizer.view.tag];
        circleImg.image = [UIImage imageNamed:@"blue_circle"];
        
       // [tab_contact reloadRowsAtIndexPaths:@[index] withRowAnimation:YES];
        
        [selected_array addObject:[[filtered_arr objectAtIndex:recognizer.view.tag-900] objectForKey:@"id"]];
        
        
    }
    
    DebugLog(@"Selected Array:%@",selected_array);
}


@end